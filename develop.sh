#!/usr/bin/env bash

UNAMEOUT="$(uname -s)"
case "${UNAMEOUT}" in
    Linux*)             MACHINE=linux;;
    Darwin*)            MACHINE=mac;;
    MINGW64_NT-10.0*)   MACHINE=mingw64;;
    *)                  MACHINE="UNKNOWN"
esac

if [ "$MACHINE" == "UNKNOWN" ]; then
    echo "Unsupported system type"
    echo "System must be a Macintosh, Linux or Windows"
    echo ""
    echo "System detection determined via uname command"
    echo "If the following is empty, could not find uname command: $(which uname)"
    echo "Your reported uname is: $(uname -s)"
fi

export APP_PORT=${APP_PORT:-80}
export MYSQL_PORT=${MYSQL_PORT:-3306}
export WWWUSER=${WWWUSER:-$UID}
export PWD=${PWD:-$PWD}

# Is the environment running
PSRESULT="$(docker-compose ps -q)"
if [ ! -z "$PSRESULT" ]; then
    EXEC="yes"
else
    EXEC="no"
fi

# Create base docker-compose command to run
COMPOSE="docker-compose"


# If we pass any arguments...
if [ $# -gt 0 ]; then

    # Source .env, which can over-ride env vars
    # such as APP_PORT, MYSQL_PORT, and WWWUSER
    if [ -f .env ]; then
        source .env
    fi

    if [ "$1" == "init" ]; then
        if [ ! -f develop ]; then
            echo "No develop file found within current working directory $(pwd)"
            exit 0
        fi
         chmod +x develop

    # Start up containers
    elif [ "$1" == "start" ]; then
        $COMPOSE up -d

    # Stop the containers
    elif [ "$1" == "stop" ]; then
        $COMPOSE down

    # If "php" is used, pass-thru to "php"
    # inside a new container
    elif [ "$1" == "php" ]; then
        shift 1
        if [ "$EXEC" == "yes" ]; then
            $COMPOSE exec \
                -u veracity \
                app \
                php "$@"
        else
            $COMPOSE run --rm \
                app \
                php "$@"
        fi

        # If "ssh" is used, pass-thru to "ssh"
        # inside a new container
        # e.g.: ./develop ssh app
        # e.g.: ./develop ssh mysql
        elif [ "$1" == "ssh" ]; then
            shift 1
            if [ "$EXEC" == "yes" ] && [ "$1" != "node" ]; then
                $COMPOSE exec \
                    -u veracity \
                    $1 \
                    bash
            else
                $COMPOSE run --rm \
                    $1 \
                    bash
            fi

            # Else, pass-thru args to docker-compose
            else
                $COMPOSE "$@"
            fi
else
    # Use the docker-compose ps command if nothing else passed through
    $COMPOSE ps
fi

