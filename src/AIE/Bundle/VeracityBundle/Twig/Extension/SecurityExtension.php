<?php

namespace AIE\Bundle\VeracityBundle\Twig\Extension;

use AIE\Bundle\VeracityBundle\Component\Core\Projects;
use Symfony\Component\HttpFoundation\Request;

class SecurityExtension  extends \Twig_Extension
{

    private    $securityContext;

    public function __construct($securityC){
        $this->securityContext = $securityC;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('isGranted', array($this, 'isGranted')),
        );
    }

    /**
    * Get current controller name
    */
    public function isGranted($role, Projects $entity)
    {
        return $this->securityContext->isGranted($role, $entity);
    }

    public function getName()
    {
        return 'aie_security_extension';
    }
}