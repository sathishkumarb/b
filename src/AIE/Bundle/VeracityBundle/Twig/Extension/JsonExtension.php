<?php
/**
 * User: mokha
 * Date: 2/29/16
 * Time: 2:07 PM
 */

namespace AIE\Bundle\VeracityBundle\Twig\Extension;

class JsonExtension extends \Twig_Extension
{

    public function getName()
    {
        return 'json_twig_extension';
    }

    public function getFunctions()
    {
        return array(
            'json_decode' => new \Twig_SimpleFunction('json_decode', [$this, 'jsonDecode']),
            'json_encode' => new \Twig_SimpleFunction('json_encode', [$this, 'jsonEncode']),
        );
    }

    public function jsonEncode($object)
    {
        return json_encode($object);
    }

    public function jsonDecode($str, $asArray=true)
    {
        return json_decode($str, $asArray);
    }
}