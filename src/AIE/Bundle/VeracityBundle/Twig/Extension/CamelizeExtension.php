<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 07/01/16
 * Time: 12:59
 */

namespace AIE\Bundle\VeracityBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\Request;

class CamelizeExtension extends \Twig_Extension {

	public function getFilters() {
		return [
			'camelize' => new \Twig_Filter_Method($this, 'camelizeFilter'),
		];
	}

	public function camelizeFilter($value) {
		if (! is_string($value)) {
			return $value;
		}

		$chunks = explode(' ', $value);
		$ucfirsted = array_map(function ($s) {
			return ucfirst($s);
		}, $chunks);

		return implode('', $ucfirsted);
	}

	public function getName() {
		return 'aie_camelize_extension';
	}

}