<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 18/10/15
 * Time: 03:46
 */

namespace AIE\Bundle\VeracityBundle\Twig\Extension;


class ClassExtension extends \Twig_Extension {

	public function getFunctions() {
		return [
			'class' => new \Twig_SimpleFunction('class', [$this, 'getClass'])
		];
	}

	public function getName() {
		return 'class_twig_extension';
	}

	public function getClass($object) {
		return (new \ReflectionClass($object))->getShortName();
	}

}