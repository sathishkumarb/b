<?php

namespace AIE\Bundle\VeracityBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\Request;

class ArrayExtension  extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('diff', array($this, 'getDiff')),
        );
    }

    /**
    * Get current controller name
    */
    public function getDiff(array $array1, array $array2)
    {
        if(is_array($array1) && is_array($array2)){
            return array_udiff($array1, $array2, function($a, $b){
                if ($a['name'] === $b['name']) return 0;
                return ($a['name'] > $b['name'])? 1:-1;
            });
        }
    }

    public function getName()
    {
        return 'aie_array_helper_twig_extension';
    }
}