<?php

namespace AIE\Bundle\VeracityBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Security\AuthenticatedController;
use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ProjectConsequencesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AIE\Bundle\IntegrityAssessmentBundle\Security;
use Symfony\Component\HttpFoundation\File\File;

class BaseController extends Controller implements AuthenticatedController {

	use Helper\FormStyleHelper;
	use Security\RoutesRolesMapper;

	protected static $leftTitle = '';
	protected $connection = null;
	protected $folderDirBase = '';

	public static function setLeftTitle($title) {
		static::$leftTitle = $title;
	}

	public static function setLeftTitleFromPipeline($entity) {
		static::setLeftTitle($entity->getName());
	}

	public static function getLeftTitle() {
		return static::$leftTitle;
	}

	private function getUploadDir($folder){
		return $this->folderDirBase . DIRECTORY_SEPARATOR . $folder;
	}
	/**
	 * Upload Files to Storage
	 *
	 * @param string $name Image field name
	 *
	 * @return string
	 */
	protected function uploadFile(File $file, AbstractFile $entity) {
		if ($file === null || $entity === null)
			return false;

		$uploader = $this->get('storage.file_uploader');
		$uploadedPath = $uploader->upload($file, $this->getUploadDir($entity->getUploadDir()), $entity->getAllowedMimeTypes());
		$entity->setFilepath($uploadedPath);
		return $uploadedPath;
	}

	protected function replaceFile(File $file, AbstractFile $entity) {
		if ($entity === null)
			return false;

		if ($file === null) {
			$this->deleteFile($entity);

			return null;
		} else {
			$uploader = $this->get('storage.file_uploader');
			$uploadedPath = $uploader->replace($entity->getFilepath(), $file, $this->getUploadDir($entity->getUploadDir()), $entity->getAllowedMimeTypes());

			$entity->setFilepath($uploadedPath);
			return $uploadedPath;
		}
	}

	protected function deleteFile(AbstractFile $entity) {
		$uploader = $this->get('storage.file_uploader');

		return $uploader->delete($entity->getFilepath());
	}

	protected function readFile(AbstractFile $entity) {
		$uploader = $this->get('storage.file_uploader');

		return $uploader->read($entity->getFilepath());
	}

	protected function getAbsolutePath(AbstractFile $entity) {
		$uploader = $this->get('storage.file_uploader');

		return $uploader->getAbsoluteFilePath($entity->getFilepath());
	}


	protected $_secureObject = null;

	public function setSecureObject($object) {
		$this->_secureObject = $object;
	}

	public function getSecureObject() {
		return $this->_secureObject;
	}

	public function getRoutesRoles() {
		return $this->_routesRoles;
	}

	/**
	 * Shortcut to return the Doctrine Registry service.
	 *
	 * @return Registry
	 *
	 * @throws \LogicException If DoctrineBundle is not available
	 */
	public function getManager() {
		if ($this->connection !== null)
			return $this->getDoctrine()->getManager($this->connection);

		return $this->getDoctrine()->getManager();
	}

}