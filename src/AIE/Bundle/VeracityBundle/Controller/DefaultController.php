<?php

namespace AIE\Bundle\VeracityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     *
     *
     * @Route("/", name="veracity_index")
     * @Method("get")
     * @Template("AIEVeracityBundle:Default:index.html.twig")
     */

    public function indexAction()
    {
        return [];
    }
}
