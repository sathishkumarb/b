<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 11/10/15
 * Time: 03:48
 */

namespace AIE\Bundle\VeracityBundle\Component\Service;


abstract class Manager {

	protected $em = null;
	protected $defem = null;
	protected $securityContext = null;
	protected $jms = null;
	protected $user = null;

	public function getEntityManager() {
		return $this->em;
	}

	public function __construct($em, $defem, $securityContext, $jms) {
		$this->em = $em;
		$this->defem = $defem;
		$this->securityContext = $securityContext;
		$this->jms = $jms;

		$this->user = $this->securityContext->getToken()->getUser() ?: null;
	}
}