<?php

namespace AIE\Bundle\VeracityBundle\Component\Files;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use JMS\Serializer\Annotation as JMS;

/**
 * Files
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
abstract class Files extends AbstractFile {


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Exclude
     */
	protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     * @JMS\Exclude
     */
	protected $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="document_number", type="string", length=2555, nullable=true)
     * @JMS\Exclude
     */
    protected $document_number;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=2555, nullable=true)
     * @JMS\Exclude
     */
    protected $company;

    /**
     * @var string
     *
     * @ORM\Column(name="filereportcategory", type="string", length=100, nullable=true)
     * @JMS\Exclude
     */
    protected $filereportcategory;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     * @JMS\Exclude
     */
    protected $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="document_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     * @JMS\Exclude
     */
    protected $document_date;

    /**
     * @ORM\ManyToOne(targetEntity="FilesCategories", inversedBy="pfc")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=true)
     * @JMS\Exclude
     */
    protected $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     * @JMS\Exclude
     */
    protected $size;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set caption
     *
     * @param string $caption
     * @return Files
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Files
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Files
     */
    public function setFilename($name)
    {
        $this->filename = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set document_number
     *
     * @param string $documentNumber
     * @return Files
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->document_number = $documentNumber;

        return $this;
    }

    /**
     * Get document_number
     *
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->document_number;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Files
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set filereportcategory
     *
     * @param string $filereportcategory
     * @return Files
     */
    public function setFilereportCategory($filereportcategory)
    {
        $this->filereportcategory = $filereportcategory;

        return $this;
    }

    /**
     * Get filereportcategory
     *
     * @return string
     */
    public function getFilereportCategory()
    {
        return $this->filereportcategory;
    }

    /**
     * Set document_date
     *
     * @param \DateTime $documentDate
     * @return Files
     */
    public function setDocumentDate($documentDate)
    {
        $this->document_date = $documentDate;

        return $this;
    }

    /**
     * Get document_date
     *
     * @return \DateTime
     */
    public function getDocumentDate()
    {
        return $this->document_date;
    }


    /***
     * @return string
     */
    function getUniqueName()
    {
        return '<a class="download-attachment" target="_blank" href="#"><span class="glyphicon glyphicon-cloud-download"></span></a><span>' . $this->caption . '</span>';
    }

    function getObject(){
        return $this;
    }
    /**
     * Set category
     *
     * @param FilesCategories $category
     * @return Files
     */
    public function setCategory(FilesCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return FilesCategories
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function getAbsolutePath($fileuploader){
        return $fileuploader->getAbsoluteFilePath($this->getFilepath());
    }


    public function getAsTreeNode($fileuploader)
    {
        $filePath = $fileuploader->getAbsoluteFilePath($this->getFilepath());
        $documentDate = $this->getDocumentDate();
        if($documentDate)
        {
            $documentDate = $documentDate->format('d F Y');
        }

        return [
            'id'             => $this->getId(),
            'key'            => $this->getId(),
            'title'          => $this->getFilename(),
            'caption'        => $this->getCaption(),
            'company'        => $this->getCompany(),
            'documentNumber' => $this->getDocumentNumber(),
            'documentDate'   => $documentDate,
            'date'           => $this->getDate()->format('d F Y'),
            'filepath'       => $this->getFilepath(),
            'absolutepath'   => $filePath,
        ];
    }


    /**
     * Set size
     *
     * @param integer $size
     * @return Files
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    public function __toString(){
        return $this->filename;
    }
}