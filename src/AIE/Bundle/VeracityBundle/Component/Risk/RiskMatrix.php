<?php

namespace AIE\Bundle\VeracityBundle\Component\Risk;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

class RiskMatrix {


	private $_choices = ['L', 'M', 'H', 'VH','VL'];

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="desc_1", type="string", length=255)
	 */
	protected $desc1 = "Failure is not expected\n< 10^-5\n\nonce > 10 years";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="desc_2", type="string", length=255)
	 */
	protected $desc2 = "Never heard of in the industry\n10^-5  -  10^-4\n\nOnce 2 - 10 years";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="desc_3", type="string", length=255)
	 */
	protected $desc3 = "An accident has occured in the industry\n10^-4  -  10^-3\n\nOnce 1 - 2 years";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="desc_4", type="string", length=255)
	 */
	protected $desc4 = "Has been experienced by most operators\n10^-3  - 10^-2\n\nOnce 1 month - 1 year";

	/**
	 * @var string
	 *
	 * @ORM\Column(name="desc_5", type="string", length=255)
	 */
	protected $desc5 = "Occurs several times per year\n10^-2  -  10^-1\n\nOnce in < 1 month";

	/**
	 * @var array
	 *
	 * @ORM\Column(name="matrix", type="json_array")
	 */
	protected $matrix = '[["L","L","L","M","M"],["L","M","M","M","H"],["L","M","M","H","H"],["M","M","H","H","VH"],["M","H","H","VH","VH"]]';

	protected $matrixJson = null;

	/**
	 * @var array
	 *
	 * @JMS\Type("array")
	 * @JMS\Exclude
	 * @JMS\ReadOnly
	 */
	static protected $riskChoices = [
		0 => 1, 1 => 2, 2 => 3, 3 => 4, 4 => 5, 5 => 6
	];

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    static protected $riskChoicesAlpha = [
        0 => 'L', 1 => 'M', 2 => 'H', 3 => 'VH', 4 => 'VL'
    ];

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set desc1
	 *
	 * @param string $desc1
	 * @return RiskMatrix
	 */
	public function setDesc1($desc1) {
		$this->desc1 = $desc1;

		return $this;
	}

	/**
	 * Get desc1
	 *
	 * @return string
	 */
	public function getDesc1() {
		return $this->desc1;
	}

	/**
	 * Set desc2
	 *
	 * @param string $desc2
	 * @return RiskMatrix
	 */
	public function setDesc2($desc2) {
		$this->desc2 = $desc2;

		return $this;
	}

	/**
	 * Get desc2
	 *
	 * @return string
	 */
	public function getDesc2() {
		return $this->desc2;
	}

	/**
	 * Set desc3
	 *
	 * @param string $desc3
	 * @return RiskMatrix
	 */
	public function setDesc3($desc3) {
		$this->desc3 = $desc3;

		return $this;
	}

	/**
	 * Get desc3
	 *
	 * @return string
	 */
	public function getDesc3() {
		return $this->desc3;
	}

	/**
	 * Set desc4
	 *
	 * @param string $desc4
	 * @return RiskMatrix
	 */
	public function setDesc4($desc4) {
		$this->desc4 = $desc4;

		return $this;
	}

	/**
	 * Get desc4
	 *
	 * @return string
	 */
	public function getDesc4() {
		return $this->desc4;
	}

	/**
	 * Set desc5
	 *
	 * @param string $desc5
	 * @return RiskMatrix
	 */
	public function setDesc5($desc5) {
		$this->desc5 = $desc5;

		return $this;
	}

	/**
	 * Get desc5
	 *
	 * @return string
	 */
	public function getDesc5() {
		return $this->desc5;
	}

	/**
	 * Set matrix
	 *
	 * @param array $matrix
	 * @return RiskMatrix
	 */
	public function setMatrix($matrix) {
		$this->matrix = $matrix;

		return $this;
	}

	/**
	 * Get matrix
	 *
	 * @return array
	 */
	public function getMatrix() {
		return $this->matrix;
	}

	public function getChoices() {
		return $this->_choices;
	}

	public function getColorMatrix() {

		$colorMatrix = [];

		$matrix = json_decode($this->matrix, true);

		for ($i = 1; $i <= 5; $i++) {//cons
			for ($j = 1; $j <= 5; $j++) { //pof
				$colorMatrix[ $i ] [ $j ] = $matrix[ $i - 1 ][ $j - 1 ];
			}
		}

		return $colorMatrix;
	}

	public function getRisk($consequence=0, $probability=0, $numerical=false){
		if($this->matrixJson === null){
			$this->matrixJson = json_decode($this->matrix, true);
		}

        if($consequence === null || $probability === null){
            return null;
        }

		$riskValue = $this->matrixJson[$consequence][$probability];

		if($numerical){
			$riskMappings = ['VL' => 4, 'L' => 0, 'M' => 1, 'H' => 2, 'VH' => 3];
			return $riskMappings[$riskValue];
		}

		return $riskValue;
	}

	public static function getRiskChoices($numerical=false) {
        if($numerical){
            return self::$riskChoices;
        }
        else{
            return self::$riskChoicesAlpha;
        }
	}
}