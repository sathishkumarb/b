<?php
/**
 * Created by AIE
 * Date: 18/10/15
 * Time: 04:18
 */

namespace AIE\Bundle\VeracityBundle\Component\Core;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

trait Tags {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="statusTag", type="string", length=255)
	 * @JMS\Type("string")
	 */
	protected $statusTag;

    /**
     * @var string
     *
     * @ORM\Column(name="leveltwostatusTag", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $leveltwostatusTag;

	/**
	 * Set statusTag
	 *
	 * @param string $statusTag
	 * @return AnomalyRegistrar
	 */
	public function setStatusTag($statusTag) {
		$this->statusTag = $statusTag;

		return $this;
	}

	/**
	 * Get statusTag
	 *
	 * @return string
	 */
	public function getStatusTag() {
		return $this->statusTag;
	}

    /**
     * Set leveltwostatusTag
     *
     * @param string $leveltwostatusTag
     * @return AnomalyRegistrar
     */
    public function setleveltwoStatusTag($leveltwostatusTag) {
        $this->leveltwostatusTag = $leveltwostatusTag;

        return $this;
    }

    /**
     * Get leveltwostatusTag
     *
     * @return string
     */
    public function getleveltwoStatusTag() {
        return $this->leveltwostatusTag;
    }


	/**
	 * A sample of what it should return
	 * return [
	 *    'tag-unique-name'       => [
	 *    'name'  => 'name-in-db',
	 *    'label' => 'label-to-show'
	 *    ],
	 * @return mixed
	 */
	public static function getTagChoices() {
		throw new \Exception("Tag Choices must be defined");
	}
}