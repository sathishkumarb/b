<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 11:28
 */

namespace AIE\Bundle\VeracityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ActionOwnersType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('company', 'text', $this->options(array('label' => 'Company')))
            ->add('position', 'text', $this->options(array('label' => 'Position')))
            ->add('email', 'email', $this->options(array('label' => 'Email')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\VeracityBundle\Component\Core\ActionOwners'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_actionowners';
    }

}
