<?php

namespace AIE\Bundle\VeracityBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\VeracityBundle\Component\Files\Files;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class FilesEditType extends AbstractType {

    use Helper\FormStyleHelper;

    protected $entity;

    public function __construct(Files $file)
    {
        $this->entity = $file;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = $this->entity->getDocumentDate();
        if ($date)
        {
            $date = $date->format('d F Y');
        }
        $builder
            ->add('filename', 'text', $this->options(['label' => 'Name']))
            ->add('caption', 'text', $this->options(['label' => 'Caption', 'required' => false]))
            ->add('company', 'text', $this->options(['label' => 'Company', 'required' => true]))
            ->add('document_number', 'text', $this->options(['label' => 'Document Number', 'required' => true]))
            ->add('document_date', new DatePickerType(), $this->options(['mapped' => true, 'label' => 'Document Date', 'data' => $date, 'attr' => ['class' => 'col-md-9'], 'label_attr' => ['class' => 'col-md-3 control-label']], ''));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\VeracityBundle\Component\Files\Files'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_veracitybundle_files_edit';
    }

}
