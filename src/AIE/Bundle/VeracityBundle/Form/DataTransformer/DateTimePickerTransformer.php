<?php
/**
 * User: mokha
 * Date: 3/24/16
 * Time: 3:36 AM
 */
namespace AIE\Bundle\VeracityBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use DateTime;
use Symfony\Component\Validator\Constraints\Date;

class DateTimePickerTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object (date) to a string (date_str).
     *
     * @param  DateTime|null $date
     * @return string
     */
    public function transform($date)
    {
        if (null === $date) {
            return '';
        }

        return $date->format('d F Y');
    }

    /**
     * Transforms a string (date_str) to an object (date).
     *
     * @param  string $dateStr
     * @return DateTime|null
     * @throws TransformationFailedException if object (date_str) cannot be converted to DateTime.
     */
    public function reverseTransform($dateStr)
    {
        // no issue number? It's optional, so that's ok
        if (!$dateStr) {
            return null;
        }

        $date = new DateTime($dateStr);

        if (null === $date) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(
                sprintf(
                    'An issue with date "%s". Cannot be converted to DateTime!',
                    $dateStr
                )
            );
        }

        return $date;
    }
}