<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 22:28
 */

namespace AIE\Bundle\VeracityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\VeracityBundle\Component\Risk\RiskMatrix;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class RiskMatrixType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(RiskMatrix $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('desc1', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc1(),
                'attr' => array('class' => 'liklyhood')
            ), 'textarea')
            )
            ->add('desc2', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc2(),
                'attr' => array('class' => 'liklyhood')
            ), 'textarea')
            )
            ->add('desc3', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc3(),
                'attr' => array('class' => 'liklyhood')
            ), 'textarea')
            )
            ->add('desc4', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc4(),
                'attr' => array('class' => 'liklyhood')
            ), 'textarea')
            )
            ->add('desc5', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc5(),
                'attr' => array('class' => 'liklyhood')
            ), 'textarea')
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\VeracityBundle\Component\Risk\RiskMatrix'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_veracitybundle_riskmatrix';
    }

}
