<?php

namespace AIE\Bundle\VeracityBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class FilesType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('filename', 'text', $this->options(['label' => 'Name']))
            ->add('caption', 'text', $this->options(['label' => 'Caption', 'required' => false]))
            ->add('company', 'text', $this->options(['label' => 'Company', 'required' => true]))
            ->add('document_number', 'text', $this->options(['label' => 'Document Number', 'required' => true]))
            ->add('document_date', new DatePickerType(), $this->options(['mapped' => true, 'label' => 'Document Date', 'attr' => ['class' => 'col-md-9'], 'label_attr' => ['class' => 'col-md-3 control-label']], ''))
            ->add('file', 'file', $this->options(['label' => ' '], 'file'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\VeracityBundle\Component\Files\Files'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_veracitybundle_files';
    }

}
