<?php
/**
 * User: mokha
 * Date: 2/28/16
 * Time: 12:10 AM
 */

namespace AIE\Bundle\VeracityBundle\Helper;


class EntityHelper
{

    public static function getEntityColumnValues($entity, $em)
    {
        $metadata = $em->getClassMetadata(get_class($entity));
        $fields = $metadata->getFieldNames();
        $associations = $metadata->getAssociationNames();

        $properties = array_merge($fields, $associations);
        //$cols = $metadata->getColumnNames();

        $fieldsData = [];
        foreach ($properties as $field => $function) {
            $fieldsData[$function] = $entity->{'get'.ucfirst($function)}();
        }

        return $fieldsData;
    }
}