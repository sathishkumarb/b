<?php
/**
 * Created by AIE LLC
 * Date: 28/12/15
 * Time: 13:25
 */

namespace AIE\Bundle\VeracityBundle\Helper;

class InspectionHelper
{

    public static function calculateNextInspectionDate($last_date, $frequency = 1)
    {
        if ($last_date === null) {
            return null;
        }

        $next_date = clone $last_date;

        if ( $frequency < 1 ) {
            $days = 1;
            switch($frequency)
            {
                case (0.25):
                    $days = 7;
                    break;
                case (0.50):
                    $days = 14;
                    break;
                case (0.75):
                    $days = 21;
                    break;
                default:
                    $days = 1;
                    break;
            }
            //$this->next_inspection_date->add(new \DateInterval("P{$days}D"));
            $next_date->modify("+{$days} day");
        } else {

            $day = 0;

            $months = explode(".", $frequency );
            if ($months[0]) $month = (int) $months[0];
            if (isset($months[1]) && $months[1]) $day = (int) $months[1];

            if ( $day && $month)
            {
                $next_date->modify("+{$month} month");
                switch($day)
                {
                    case (25):
                        $day = 7;
                        break;
                    case (50):
                        $day = 14;
                        break;
                    case (75):
                        $day = 21;
                        break;
                    default:
                        $day = 1;
                        break;
                }
                $next_date->modify("+{$day} day");
            }
            else if ( !$day && $month ) {
                $month = $months[0];
                //$this->next_inspection_date->add(new \DateInterval("P{$months}M"));
                $next_date->modify("+{$month} month");
            }

        }

        return $next_date;
    }

    public static function calculateKpiCompliance($kpiColor, $weightageValue)
    {
        return ($kpiColor * $weightageValue);
    }
}