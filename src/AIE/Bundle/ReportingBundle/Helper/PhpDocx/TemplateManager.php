<?php

namespace AIE\Bundle\ReportingBundle\Helper\PhpDocx;

//use Phpdocx\Phpdocx;

class TemplateManager {

    private $_htmlSettings;
    private $_tableSettings;
    /** @var  Phpdocx */
    private $_phpdocx;
    private $_variables = array(
        'html' => array(), //'ID' => array('block', 'HTML')
        'text' => array(), //'ID' => 'STRING'
        'image' => array(), //'ID' => 
        'table' => array(), //'ID' =>
        'list'  => array(),
    );
    private $_clearVariable = 'DELETE_ME';

    public function __construct($template) {
        $this->initializeDocument($template);
    }

    private function initializeDocument($template) {
        $this->_phpdocx = new \Phpdocx\Create\CreateDocxFromTemplate($template);
        //$this->_phpdocx->setEncodeUTF8(); //Removed because of double encoding -.-
 //       $this->_phpdocx->CreateDocxFromTemplate($template);
    }

    public function setHtmlSettings($settings = array()) {
        $this->_htmlSettings = $settings;
    }

    public function setTableSettings($settings = array()) {
        $this->_tableSettings = $settings;
    }

    public function addText($variable, $text) {
        $this->_variables['text'][$variable] = (is_array($text) && !is_string($text)) ? $text : strval($text);
    }

    public function addHTML($variable, $html, $type = 'block', $settings = null) {

        $settings = (is_null($settings)) ? $this->_htmlSettings : $settings;

        $this->_variables['html'][$variable] = array(
            'html' => nl2br($html),
            'type' => $type,
            'settings' => $settings
        );
    }

    public function replaceHeaderText($variables) {
      $options = array('target' =>header);
      $this->_phpdocx->replaceVariableByText($variables, $options);
    }

    public function addTable($variable, array $data, $settings = null) {
        $settings = (is_null($settings)) ? $this->_tableSettings : $settings;

        $this->_variables['table'][$variable] = array(
            'data' => $data,
            'settings' => $settings
        );
    }

    public function addImage($variable, $image) {
        $this->_variables['image'][$variable] = (is_array($image) && !is_string($image)) ? $image : strval($image);
    }
     public function addList($variable, $list) {
        $this->_variables['list'][$variable] = (is_array($list) && !is_string($list)) ? $list : strval($list);
    } 


    public function addMultipleTexts(array $variables) {
        $flag = 0;
        foreach ($variables as $var => $text) {
            if ( !is_array($text) ) {
                if ($text != strip_tags($text)) {
                    $chunks = explode('<br />', $text);
                    if ($chunks) {
                        $flag = 1;
                        foreach ($chunks as $chunk) {
                            $add = array('text' => $chunk, 'lineBreak' => 'after');
                            $this->addText($var,$add);
                        }
                    }
                    else{
                        $flag = 0;
                    }
                }
                else{
                    $flag = 0;
                }
            }
            if (!$flag) $this->addText($var, $text);
        }

    }

    public function addMultipleHTML(array $variables) {
        foreach ($variables as $var => $param) {
            $html = $param['html'];
            $type = ($param['type'] == 'block' || $param['type'] == 'inline') ? $param['type'] : 'block';
            $settings = $param['settings'];
            $this->addHTML($var, $html, $type, $settings);
        }
    }

    public function addMultipleTables(array $variables) {
        foreach ($variables as $var => $param) {
            $data = $param['data'];
            $settings = $param['settings'];
            $this->addTable($var, $data, $settings);
        }
    }
    
    public function addMultipleLists(array $variables) {
      foreach ($variables as $var => $list) {
            $this->addList($var, $list);
        }
    }

    public function addMultipleImages(array $variables) {
        foreach ($variables as $var => $image) {
            $this->addImage($var, $image);
        }
    }

    public function replaceHeaderImage($var,$text){
        $options = array('target' =>header);
        $this->_phpdocx->replacePlaceHolderImage($var,$text,$options);
    }

    public function replaceVariables() {

        foreach ($this->_variables['text'] as $var => $text) {
            $this->_phpdocx->replaceVariableByText(array($var => $text), array('parseLineBreaks' => true));
        }

        foreach ($this->_variables['html'] as $var => $param) {
            $this->_phpdocx->replaceVariableByHTML($var, $param['type'], $param['html'], $param['settings']);
        }

        foreach ($this->_variables['table'] as $var => $param) {
            $this->_phpdocx->replaceTableVariable($param['data'], $param['settings']);
        }

        foreach ($this->_variables['image'] as $var => $image) {
            $this->_phpdocx->replacePlaceholderImage($var, $image);
        }

        foreach ($this->_variables['list'] as $var => $list) {
            $this->_phpdocx->replaceListVariable($var, $list);
        }

    }

    public function getClearVariable() {
        return '$' . $this->_clearVariable . '$';
    }

    public function clearDocument() {
        $this->_phpdocx->removeTemplateVariable($this->_clearVariable, 'block');
        $this->_phpdocx->clearBlocks();
    }

    public function getDocument() {
        return $this->_phpdocx;
    }

    public function generateDocument() {
        $this->replaceVariables();
        return $this->_phpdocx;
    }

    public function deleteBlock($block){
        $this->_phpdocx->deleteTemplateBlock($block);
    }
}
