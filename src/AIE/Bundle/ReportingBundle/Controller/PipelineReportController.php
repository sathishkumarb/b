<?php

namespace AIE\Bundle\ReportingBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Controller\BaseController;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects;
use AIE\Bundle\IntegrityAssessmentBundle\Form\FormCreators\AssessmentReportCreator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineRiskAssessmentType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsMitigationsFrequencyType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Phpdocx\Phpdocx;
use AIE\Bundle\ReportingBundle\Helper\PhpDocx\TemplateManager;
use AIE\Bundle\IntegrityAssessmentBundle\Form\FormCreators;

/**
 * Report controller.
 *
 * @Route("pipeline/{pipelineId}/report")
 */
class PipelineReportController extends BaseController {

    use FormCreators\AssessmentReportCreator;

    private $_timeoutGeneration = 5000;

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/{id}-pdf", name="assessment_report_pdf")
     * @Method("GET")
     */
    public function reportPDFAction($pipelineId, $id)
    {
        $pageUrl = $this->generateUrl('assessment_report', array('pipelineId' => $pipelineId, 'id' => $id), true); // use absolute path!

        $session = $this->get('session');
        $session->save();
        session_write_close();

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl, array('cookie' => array($session->getName() => $session->getId()))), 200, array(
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="file.pdf"'
            )
        );
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/{id}-docx", name="assessment_report_docx")
     * @Method("POST")
     */
    public function reportDocxAction(Request $request, $pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $workshops = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->findByPipeline($pipelineId);
        $form = $this->createAssesmentReportForm($pipelineId, $id, $workshops);
        $form->handleRequest($request);

        if (! $form->isValid())
        {
            throw $this->createNotFoundException('Unable to generate document.');
        }

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineRiskAssessment entity.');
        }

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        $pipeline = $assessment['pipeline'];
        $project = $assessment['project'];


        $templateTempPath = tempnam(sys_get_temp_dir(), 'AIE').".docx";
        $reportData = $this->readFile($entity);
        $fh = fopen($templateTempPath, 'w'); fwrite($fh, $reportData); fclose($fh);
        $templateManage = new TemplateManager($templateTempPath);

        $textVariables = [];
        $tableVariables = [];

        //remove not selected blocks
        $assessment_report_selected_blocks = $form->getData()['assessment_report'];
        foreach ($assessment_report_selected_blocks as $block => $include)
        {
            if (! $include)
            {
                $templateManage->deleteBlock($block);
            }
        }

        if ($assessment_report_selected_blocks['WORKSHOP'])
        {
            $workshop_report = $form->getData()['workshop_report'];
            $workshop_report = array_filter($workshop_report);
            $workshopId = key($workshop_report);
            $workshopId = explode('_', $workshopId);
            $workshopId = $workshopId[1];

            $workshop = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($workshopId);

            $textVariables['INITIALLY_ASSESSED_BY'] = $workshop->getInitiallyAssessedBy();
            $textVariables['WORKSHOP_DATE'] = $workshop->getDate()->format('jS F Y');
            $attendees = array();
            foreach ($workshop->getWa() as $attendee)
            {
                $attendees[] = [
                    'ATTENDEE_NAME'     => $attendee->getName(),
                    'ATTENDEE_POSITION' => $attendee->getPosition(),
                    'ATTENDEE_COMPANY'  => $attendee->getCompany()
                ];
            }
            $tableVariables['ATTENDEES_TABLE'] = ['data' => $attendees];
        }

        //Generate IMAGES
        $knpImage = $this->get('knp_snappy.image');
//        $knpImage->getInternalGenerator()->setTimeout($this->_timeoutGeneration);

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $sectionGmapURL = $this->generateUrl('assessment_section_gmap', array('pipelineId' => $pipelineId, 'id' => $entity->getId()), true);
        $sectionGmapPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . md5(uniqid(rand(), true)) . '.jpg';
        $snappy_config = $this->container->getParameter('knp_snappy');
        try{
            $knpImage->generate($sectionGmapURL, $sectionGmapPath, array('cookie' => array($session->getName() => $session->getId()), 'javascript-delay' => $snappy_config['delay']));
        }catch (\Exception $e){ }

        $imageVariables = [
            'SECTIONS_MAP' => $sectionGmapPath
        ];


        $templateManage->addMultipleTexts($textVariables);
        $templateManage->addMultipleTables($tableVariables);
        $templateManage->addMultipleImages($imageVariables);
        $templateManage->replaceVariables();
        $templateManage->clearDocument();
        $report = $templateManage->generateDocument();

        //Sending the file to be downloaded
        $bad = array_merge(
            array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

        $name = $project['client'] . ' - ' . $pipeline['name'];
        $name = str_replace($bad, "", $name);
        $name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $name); #remove non-ascii characters
        $fullName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $name;
        //$report->createDocx($fullName);


        $filePath = $fullName . '.' . $report->getExtension();
        $report->createDocx($fullName);
        $content = file_get_contents($filePath);
        $response = new Response();
        $d = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT, "{$name}" . '.' . $report->getExtension()
        );
        $response->headers->set('Content-Disposition', $d);
        $response->setContent($content);

        foreach ($imageVariables as $image)
        {
            unlink($image);
        }
        @unlink($filePath);
        @unlink($templateTempPath);

        return $response;
    }
}
