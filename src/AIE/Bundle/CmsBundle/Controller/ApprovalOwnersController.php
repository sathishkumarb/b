<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\ActionRegistrar;
use AIE\Bundle\CmsBundle\Entity\AnomalyRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\ApprovalOwners;
use AIE\Bundle\CmsBundle\Form\ApprovalOwnersType;

/**
 * ApprovalOwners controller.
 *
 * @Route("projects/{projectId}/approvalowners")
 */
class ApprovalOwnersController extends CmsBaseController
{
    private $layout = 'AIECmsBundle:Default:layout.html.twig';
    private $route_base = 'cms_approvalowners';
    /**
     * Lists all ApprovalOwners entities.
     *
     * @Route("/", name="cms_approvalowners")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIECmsBundle:ApprovalOwners')->findByProject($projectId);
        $users = $this->get('aie_anomaly.user.helper')->getProjectUsers($projectId);
        
        array_walk(
            $entities,
            function (ApprovalOwners $entity) use ($users) {
                $entity->setAnomalyCategory(AnomalyRegistrar::getAnomalyCategories()[$entity->getAnomalyCategory()]);
                //$entity->setActionCategory(ApprovalOwners::getActionsChoices()[$entity->getActionCategory()]);
                $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));
            }
        );

        return [
            'entities' => $entities,
            'projectId' => $projectId,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
        ];
    }

    /**
     * Creates a new ApprovalOwners entity.
     *
     * @Route("/", name="cms_approvalowners_create")
     * @Method("POST")
     * @Template("AIECmsBundle:ApprovalOwners:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new ApprovalOwners();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

            $approvalsSPAPost = $this->get('request')->request->get('aie_bundle_anomalybundle_approvalowners');

            // spa is not loaded on dropdown list
            if(!array_key_exists('actionOwner', $approvalsSPAPost)){
                throw new \Exception("Approval SPA is missing!");
                exit;
            }

            $userActionSpa = $this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($approvalsSPAPost['actionOwner']);

            //$defUserExist = $em->getRepository('AIECmsBundle:ApprovalOwners')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$approvalsSPAPost['anomalyCategory'],'actionCategory'=>$approvalsSPAPost['actionCategory']));
            $defUserExist = $em->getRepository('AIECmsBundle:ApprovalOwners')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$approvalsSPAPost['anomalyCategory']));

            if (!$defUserExist) {
                $entity->setActionOwner($userActionSpa);
                $entity->setProject($project);
                $em->merge($entity);
                $em->flush();
                
            } else{
                $this->get('session')->getFlashBag()->add('error', "Oops! This Approval Owner for the category is already in use.");
            }

            return $this->redirect($this->generateUrl('cms_approvalowners', ['projectId' => $projectId]));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
        ];
    }

    /**
     * Creates a form to create a ApprovalOwners entity.
     *
     * @param ApprovalOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ApprovalOwners $entity, $projectId)
    {

        $em = $this->getManager();
        $actionSpas = array();
        //$actionOwners = $em->getRepository('AIECmsBundle:ActionOwners')->findByProject($projectId);
        //$this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $actionSpas = $this->get('aie_anomaly.user.helper')->getProjectSpas($projectId,'ROLE_APPROVALOWNER_SPA');

        $form = $this->createForm(
            new ApprovalOwnersType($actionSpas),
            $entity,
            [
                'action' => $this->generateUrl('cms_approvalowners_create', ['projectId' => $projectId]),
                'method' => 'POST',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Displays a form to create a new ApprovalOwners entity.
     *
     * @Route("/new", name="cms_approvalowners_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        $entity = new ApprovalOwners();
        $form = $this->createCreateForm($entity, $projectId);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
        ];
    }

    /**
     * Finds and displays a ApprovalOwners entity.
     *
     * @Route("/{id}", name="cms_approvalowners_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ApprovalOwners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);

        $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing ApprovalOwners entity.
     *
     * @Route("/{id}/edit", name="cms_approvalowners_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ApprovalOwners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $deleteForm = $this->createDeleteForm($projectId, $id);

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a ApprovalOwners entity.
     *
     * @param ApprovalOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ApprovalOwners $entity, $projectId)
    {
        $em = $this->getManager();
        $actionOwners = $em->getRepository('AIECmsBundle:ActionOwners')->findByProject($projectId);
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $form = $this->createForm(
            new ApprovalOwnersType($actionOwners),
            $entity,
            [
                'action' => $this->generateUrl(
                    'cms_approvalowners_update',
                    ['id' => $entity->getId(), 'projectId' => $projectId]
                ),
                'method' => 'PUT',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Edits an existing ApprovalOwners entity.
     *
     * @Route("/{id}", name="cms_approvalowners_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:ApprovalOwners:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ApprovalOwners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);
        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cms_approvalowners', ['projectId' => $projectId]));
        }

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a ApprovalOwners entity.
     *
     * @Route("/{id}", name="cms_approvalowners_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:ApprovalOwners')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cms_approvalowners', ['projectId' => $projectId]));
    }

    /**
     * Creates a form to delete a ApprovalOwners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_approvalowners_delete', ['id' => $id, 'projectId' => $projectId]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }
}
