<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase as ChemicalDosageDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;

use AIE\Bundle\CmsBundle\Form\SubProjectChemicalDosageDatabaseType;
use AIE\Bundle\CmsBundle\Form\SubProjectChemicalDosageDatabaseEditType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * SubProjectChemicalDosageDatabaseController controller.
 *
 * @Route("/{projectId}/subprojectchemicaldosagedatabase/{subprojectId}")
 */
class SubProjectChemicalDosageDatabaseController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = array('serviceid','pumptypeid','chemicaltypeid','injectionmethodid');

    Protected $dynamicChemicalDosageSubProjectCols = [];

    Protected $entityCheck = "";

    Protected $addGranted = 0;
    Protected $editGranted = 0;
    Protected $deleteGranted = 0;
    Protected $excelGranted = 0;
    Protected $selectcolumnsGranted = 0;

    /**
     * Lists all SubProjectChemicalDosageDatabase entities.
     *
     * @Route("/", name="cms_subproject_chemicaldosagedatabase")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request,$projectId, $subprojectId)
    {
        $chemicaldosagedatabases = array();
        $subprojectsystems = array();
        $em = $this->getManager();
        if ($request->query->get('stepId')) $stepid = $request->query->get('stepId');
        else $stepid= 1;

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $subprojectsystems = $cmsDatabasesHelper->getSubProjectAllSystemsDatabaseItems($subprojectId,2);

        if (isset($subprojectsystems) && !empty($subprojectsystems)) {
            $chemicaldosagedatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $subprojectsystems[0]['bs_id']);
        }

        $dbcols = $cmsDatabasesHelper->getChemicalDosageColumns($subprojectId);

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $dbcolEntity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->findOneBy(array('subproject'=>$subprojectId));

        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_EXCEL',$this->userRolesMergeToCheck)) {
            $this->excelGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_SELECT_COLUMNS',$this->userRolesMergeToCheck)) {
            $this->selectcolumnsGranted = 1;
        }

        usort($subprojectsystems, function($a, $b) {
            if($a['bs_systemstitle'] == $b['bs_systemstitle']) {
                return 0;
            }
            return ($a['bs_systemstitle'] < $b['bs_systemstitle']) ? -1 : 1;
        });

        return array(
            'systems'                     => $subprojectsystems,
            'chemicaldosagedatabases'     => $chemicaldosagedatabases,
            'dbcols'                      => $dbcols,
            //'form'                        => $form->createView(),
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'project'                     => $project,
            'subproject'                  => $subproject,
            'wizard_title'                => 'Chemical Database',
            'stepId' => $stepid,
            'dbcolEntity' => $dbcolEntity,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,
            'excelGranted' => $this->excelGranted,
            'selectcolumnsGranted' => $this->selectcolumnsGranted,
        );

    }

    /**
     * Lists all SubProjectSystems Headbar for system entities.
     *
     * @Route("/{sid}/loadsystems", name="cms_subproject_chemicaldosagedatabase_loadsystem")
     * @Method("GET")
     * @Template()
     */
    public function loadsystemAction(Request $request,$sid, $projectId, $subprojectId)
    {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $chemicaldosagedatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $sid);

        $dbcols = $cmsDatabasesHelper->getChemicalDosageColumns($subprojectId);

        $systemInfo = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);

        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }

        return $this->render('AIECmsBundle:SubProjectChemicalDosageDatabase:systemchemicaldosagedatabases.html.twig',array(
            'chemicaldosagedatabases'  => $chemicaldosagedatabases,
            'dbcols'=>$dbcols,
            'systemid'=>$sid,
            'systemInfo' => $systemInfo,
            'projectId'=>$projectId,
            'subprojectId'=>$subprojectId,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,));

    }

    /**
     * Create the entity SubProjectChemicalDosageDatabase
     *
     * @Route("/{sid}/{stepid}/create", name="cms_subproject_chemicaldosagedatabase_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $stepid, $sid, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $entity = new ChemicalDosageDatabase();
        $form = $this->createSystemsChemicalDosageDatabaseForm($entity, $projectId, $subprojectId, $sid, $stepid);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $dupFindData = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->findBy(array('subproject'=>$subprojectId, 'location'=>$data->getLocation(), 'activityid' => $data->getActivityid()));

            if (!$dupFindData ) {

                $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                $entity->setSubproject($subproject);

                $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);
                $entity->setSubProjectSystems($subprojectsystems);

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'New entry has been added to chemical management database');
            }
            else
            {
                $this->addFlash('error', 'Location already exists for the activity');
            }

            return $this->redirect($this->generateUrl('cms_subproject_chemicaldosagedatabase',array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'stepId' => $stepid,)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Delete the entity SubProjectChemicalDosageDatabase
     *
     * @Route("/{sid}/delete", name="cms_subproject_chemicaldosagedatabase_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $sid, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        if($request->isXmlHttpRequest()) {

            if (!$this->get('security.token_storage')->getToken()->getUser()) {
                return new Response(json_encode(array('result' => 'nousertoken')));
            }

            $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

            if ($sid){
                $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->find($sid);
            }

            if (!$entity) {
                return new Response(json_encode(array('result' => 'noentity')));
            }

            $em->remove($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }
    }

    /**
     * Creates a form to create a SubProjectChemicalDosageDatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsChemicalDosageDatabaseForm(ChemicalDosageDatabase $entity,$projectId, $subprojectId, $sId, $stepId)
    {

        $em = $this->getDoctrine()->getManager('cms');

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $dbcols = $cmsDatabasesHelper->getChemicalDosageColumns($subprojectId);

        $form = $this->createForm(new SubProjectChemicalDosageDatabaseType($entity, $em, $projectId, $subprojectId, $sId, $dbcols), $entity, [
            'action' => $this->generateURL('cms_subproject_chemicaldosagedatabase_create', ['stepid' => $stepId,'sid' => $sId, 'projectId' =>$projectId, 'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        return $form;
    }
    /**
     * Creates a form to create a ChemicalDosagedatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsChemicalDosagedatabaseEditForm(ChemicalDosageDatabase $entity, $stepId, $sId, $projectId, $subprojectId)
    {

        $em = $this->getManager('cms');
        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $dbcols = $cmsDatabasesHelper->getChemicalDosageColumns($subprojectId);

        $form = $this->createForm(
            new SubProjectChemicalDosageDatabaseEditType($entity, $em, $projectId, $subprojectId, $sId, $dbcols),
            $entity,
            [
                'action' => $this->generateUrl('cms_subproject_chemicaldosagedatabase_edit', ['id' => $entity->getId(), 'stepid' => $stepId, 'sid' => $sId, 'projectId' => $projectId, 'subprojectId' => $subprojectId]),
                'method' => 'POST',
            ]
        );

        return $form;
    }

    /**
     * Update subprojectchemicaldosagedatabase
     *
     * @Route("/{sid}/updatefields", name="cms_subproject_chemicaldosagedatabase_updatefields")
     * @Method("POST")
     * @Template()
     */
    public function updateFieldsAction($sid, Request $request)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest() &&  $request->isMethod( 'POST' )) {

            $title = $request->request->get('title');

            $key = $request->request->get('key');

            if ($title) {
                switch ($key) {
                    case "platformorplant":
                        $entity->setPlatformorplant($title);
                        break;
                    case "pandid":
                        $entity->setPandid($title);
                        break;
                    case "location":
                        $entity->setLocation($title);
                        break;
                    case "stage":
                        $entity->setStage($title);
                        break;
                    case "type":
                        $entity->setType($title);
                        break;
                    case "linedescription":
                        $entity->setLinedescription($title);
                        break;
                    case "chemicaltradename":
                        $entity->setChemicaltradename($title);
                        break;
                    case "chemicalsupplier":
                        $entity->setChemicalsupplier($title);
                        break;
                    case "targetconcentration":
                        $entity->setTargetconcentration($title);
                        break;
                    case "targetdosage":
                        $entity->setTargetdosage($title);
                        break;
                    case "pumprating":
                        $entity->setPumprating($title);
                        break;
                    case "columnone":
                        $entity->setColumnone($title);
                        break;
                    case "columntwo":
                        $entity->setColumntwo($title);
                        break;
                    case "columnthree":
                        $entity->setColumnthree($title);
                        break;
                    case "columnfour":
                        $entity->setColumnfour($title);
                        break;
                    case "columnfive":
                        $entity->setColumnfive($title);
                        break;

                }
            }


            $em->persist($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

    }


    /**
     * Update SubProjectChemicalDosageDatabase
     *
     * @Route("/{stepid}/{sid}/{id}/edit", name="cms_subproject_chemicaldosagedatabase_edit")
     * @Method("POST")
     * @Template()
     */
    public function editAction(Request $request, $stepid, $sid, $projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Project Chemical Management Database entity.');
        }

        $editForm = $this->createSystemsChemicalDosagedatabaseEditForm($entity, $stepid, $sid, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Entry has been updated to chemical management database');

            return $this->redirect($this->generateUrl('cms_subproject_chemicaldosagedatabase', ['projectId' => $projectId, 'subprojectId' => $subprojectId,'stepId' => $stepid,]));
        }

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];

    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}/{savetype}/", name="cms_subproject_chemicaldosagedatabase_corrosionsave")
     * @Method("POST")
     * @Template("")
     */
    public function corrosionfieldsupdateAction(Request $request, $savetype, $projectId, $subprojectId, $id)
    {

        if ($request->query->get('token') !== $this->get('security.csrf.token_manager')->getToken('intention')->getValue()) {
            throw new \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException('Invalid CSRF token');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $entities = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->findBy(array('activityid'=> $id,'subproject'=> $subprojectId));

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        if($request->isMethod( 'POST' )) {

            if ($savetype == 'two') {

                foreach ($entities as $entity) {


                    $from_date_normal_injection = $request->request->get('from_date_normal_injection_' . $entity->getId());
                    $to_date_normal_injection = $request->request->get('to_date_normal_injection_' . $entity->getId());

                    $from_date_normal_concentration = $request->request->get('from_date_normal_concentration_' . $entity->getId());
                    $to_date_normal_concentration = $request->request->get('to_date_normal_concentration_' . $entity->getId());

                    $from_date_normal_availability = $request->request->get('from_date_normal_availability_' . $entity->getId());
                    $to_date_normal_availability = $request->request->get('to_date_normal_availability_' . $entity->getId());

                    $from_date_average = $request->request->get('from_date_average_' . $entity->getId());
                    $to_date_average = $request->request->get('to_date_average_' . $entity->getId());

                    $analysis = $request->request->get('analysis_' . $entity->getId());
                    $recommendation = $request->request->get('recommendation_' . $entity->getId());

                    $entity->setAnalysis($analysis);
                    $entity->setRecommendations($recommendation);

                    if ($from_date_normal_injection && $to_date_normal_injection) {
                        if (!strchr($from_date_normal_injection, "Object") && !strchr($to_date_normal_injection, "Object")) {
                            $from_date_normal_injection = new \DateTime( $from_date_normal_injection);
                            $to_date_normal_injection = new \DateTime( $to_date_normal_injection);

                            $entity->setFromDateNormalInjection($from_date_normal_injection);
                            $entity->setToDateNormalInjection($to_date_normal_injection);
                        }
                    }
                    if ($from_date_normal_concentration && $to_date_normal_concentration) {
                        if (!strchr($from_date_normal_concentration, "Object") && !strchr($to_date_normal_concentration, "Object")) {
                            $from_date_normal_concentration = new \DateTime( $from_date_normal_concentration);
                            $to_date_normal_concentration = new \DateTime( $to_date_normal_concentration);

                            $entity->setFromDateNormalConcentration($from_date_normal_concentration);
                            $entity->setToDateNormalConcentration($to_date_normal_concentration);
                        }
                    }
                    if ($from_date_normal_availability && $to_date_normal_availability) {
                        if (!strchr($from_date_normal_availability, "Object") && !strchr($to_date_normal_availability, "Object")) {
                            $from_date_normal_availability = new \DateTime( $from_date_normal_availability);
                            $to_date_normal_availability = new \DateTime( $to_date_normal_availability);

                            $entity->setFromDateNormalAvailability($from_date_normal_availability);
                            $entity->setToDateNormalAvailability($to_date_normal_availability);
                        }
                    }
                    if ($from_date_average && $to_date_average) {
                        if (!strchr($from_date_average, "Object") && !strchr($to_date_average, "Object")) {

                            $from_date_average = new \DateTime( $from_date_average);
                            $to_date_average = new \DateTime( $to_date_average);

                            $entity->setFromDateAverage($from_date_average);
                            $entity->setToDateAverage($to_date_average);
                        }
                    }
                }

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'Changes saved');
                $systemId = $request->query->get('systemId');
                return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_graph_corrosioninhibitor', ['id' => $id, 'ids' => $request->query->get('ids'), 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => $request->request->get('types'), 'systemId' => $systemId]));
            }
            else if ($savetype == 'one'){

                foreach($entities as $entity) {

                    $from = $request->request->get('from_'.$entity->getId());
                    $to = $request->request->get('to_'.$entity->getId());

                    $analysis = $request->request->get('analysis_'.$entity->getId());
                    $recommendation = $request->request->get('recommendation_'.$entity->getId());

                    $entity->setAnalysis($analysis);
                    $entity->setRecommendations($recommendation);

                    if (!strchr($from,"Object") && !strchr($to,"Object")) {

                        $from = new \DateTime($from);
                        $to = new \DateTime($to);

                        $entity->setFrom($from);
                        $entity->setTo($to);
                    }
                }

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'Your changes have been saved');
                $systemId = $request->query->get('systemId');
                return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_graph', ['id' => $id, 'ids' => $request->query->get('ids'), 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => 'controlactivity', 'systemId' => $systemId]));
            }
        }

        return [
            'entity' => $entity
        ];
    }


}
