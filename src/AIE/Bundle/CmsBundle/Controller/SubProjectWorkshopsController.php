<?php

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops;
use AIE\Bundle\CmsBundle\Form\SubProjectWorkshopsType;

/**
 * SubProjectWorkshops controller.
 *
 * @Route("/{projectId}/{subprojectId}/workshops")
 */
class SubProjectWorkshopsController extends CmsBaseController {

    protected $grantedAdd;

    protected $grantedEdit;

    protected $grantedDelete;

    protected $grantedAttendView;

    protected $grantedAttendAdd;

    protected $grantedAttendEdit;

    /**
     * Lists all SubProjectWorkshops entities.
     *
     * @Route("/", name="cms_workshops")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->findBySubproject($subprojectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
       // $this->setLeftTitleFromPipeline($subproject);

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_ADD',$this->userRolesMergeToCheck)) {
            $this->grantedAdd=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_EDIT',$this->userRolesMergeToCheck)) {
            $this->grantedEdit=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_DELETE',$this->userRolesMergeToCheck)) {
            $this->grantedDelete=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEES_SHOW',$this->userRolesMergeToCheck)) {
            $this->grantedAttendView=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEES_ADD',$this->userRolesMergeToCheck)) {
            $this->grantedAttendAdd=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEES_EDIT',$this->userRolesMergeToCheck)) {
            $this->grantedAttendEdit=1;
        }

        return array(
            'entities'   => $entities,
            'subprojectId' => $subprojectId,
            'project' => $subproject->getProject(),
            'grantedAdd' => $this->grantedAdd,
            'grantedAttendView' => $this->grantedAttendView,
            'grantedAttendAdd' => $this->grantedAttendAdd,
            'grantedAttendEdit' => $this->grantedAttendEdit,
            'grantedEdit' => $this->grantedEdit,
            'grantedDelete' => $this->grantedDelete,
        );
    }

    /**
     * Creates a new SubProjectWorkshops entity.
     *
     * @Route("/", name="cms_workshops_create")
     * @Method("POST")
     * @Template("AIECmsBundle:SubProjectWorkshops:new.html.twig")
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectWorkshops();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getManager();
            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($subproject);
            $data = $form->getData();
            $date = $data->getDate();
            if ($date)
            {
                $entity->setDate(new \DateTime($date));
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId,'subprojectId' => $subprojectId)));
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
       // $this->setLeftTitleFromPipeline($subproject);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'subprojectId' => $subprojectId
        );
    }

    /**
     * Creates a form to create a SubProjectWorkshops entity.
     *
     * @param SubProjectWorkshops $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubProjectWorkshops $entity, $projectId, $subprojectId)
    {
        $form = $this->createForm(new SubProjectWorkshopsType($entity), $entity, array(
            'action' => $this->generateUrl('cms_workshops_create', ['projectId' => $projectId, 'subprojectId' => $subprojectId]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new SubProjectWorkshops entity.
     *
     * @Route("/new", name="cms_workshops_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId, $subprojectId)
    {
        $entity = new SubProjectWorkshops();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId);

        $em = $this->getManager();
        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);
        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'projectId' => $projectId,
            'subprojectId' => $subprojectId
        );
    }

    /**
     * Finds and displays a SubProjectWorkshops entity.
     *
     * @Route("/{id}", name="cms_workshops_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find SubProjectWorkshops entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SubProjectWorkshops entity.
     *
     * @Route("/{id}/edit", name="cms_workshops_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find SubProjectWorkshops entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);

        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'projectId'  => $projectId,
            'subprojectId'  => $subprojectId,
        );
    }

    /**
     * Creates a form to edit a SubProjectWorkshops entity.
     *
     * @param SubProjectWorkshops $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SubProjectWorkshops $entity, $projectId, $subprojectId)
    {
        $form = $this->createForm(new SubProjectWorkshopsType($entity), $entity, array(
            'action' => $this->generateUrl('cms_workshops_update', array('projectId' => $projectId, 'subprojectId' => $subprojectId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing SubProjectWorkshops entity.
     *
     * @Route("/{id}", name="cms_workshops_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:SubProjectWorkshops:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find SubProjectWorkshops entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);
        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {

            $data = $editForm->getData();
            $date = $data->getDate();
            if ($date)
            {
                $entity->setDate(new \DateTime($date));
            }
            $em->flush();

            return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        $this->setLeftTitleFromPipeline($subproject);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a SubProjectWorkshops entity.
     *
     * @Route("/{id}", name="cms_workshops_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id,  $projectId, $subprojectId)
    {
        $form = $this->createDeleteForm($id,  $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find SubProjectWorkshops entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
    }

    /**
     * Creates a form to delete a SubProjectWorkshops entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $projectId, $subprojectId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_workshops_delete', array('projectId' => $projectId,'subprojectId' => $subprojectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm();
    }
}
