<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\ActionMonitor;
use AIE\Bundle\CmsBundle\Entity\ActionRegistrar;
use AIE\Bundle\CmsBundle\Entity\CmsRegistrar;
use AIE\Bundle\CmsBundle\Entity\Monitor;
use AIE\Bundle\CmsBundle\Entity\Registrar;
use AIE\Bundle\CmsBundle\Entity\RiskProject;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\Request as CmsRequest;
use AIE\Bundle\CmsBundle\Form\RequestType;
use AIE\Bundle\CmsBundle\Form\PipelineCmsType;
use AIE\Bundle\CmsBundle\Form\PipingCmsType;
use AIE\Bundle\CmsBundle\Form\StructureCmsType;
use AIE\Bundle\CmsBundle\Form\StaticEquipmentCmsType;
use AIE\Bundle\CmsBundle\Form\RepairOrderRegistrarType;
use AIE\Bundle\CmsBundle\Form\FabricMaintenanceRegistrarType;
use AIE\Bundle\CmsBundle\Form\TemporaryRepairOrderRegistrarType;
use Doctrine\Common\Collections\ArrayCollection;
use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Request controller.
 *
 * @Route("/requests")
 */
class RequestController extends CmsBaseController
{

    /**
     * Lists all Request entities.
     *
     * @Route("/", name="cms_requests")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

    }

    /**
     * Finds and displays a Request entity.
     *
     * @Route("/{id}", name="cms_requests_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        
    }

    /**
     * Creates a form to show Registrar
     *
     * @param Registrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createRegistrarForm($entity, $projectId, $requestId=null)
    {
       
    }

    /**
     * Displays a form to edit an existing Request entity.
     *
     * @Route("/{id}/edit", name="cms_requests_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

    }

    /**
     * Creates a form to edit a Request entity.
     *
     * @param Request $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CmsRequest $entity)
    {


    }

    /**
     * Edits an existing Request entity.
     *
     * @Route("/{id}", name="cms_requests_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:Request:show.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        
    }




    /**
     * Show the request history of an anomaly
     *
     * @Route("/history/{id}", name="cms_request_history")
     * @Method("GET")
     * @Template("AIECmsBundle:Request:history.html.twig")
     */
    public function historyAction($id)
    {
        

    }

    public function mergeUserRequests($approvalOwners,$type)
    {
       
    }
}
