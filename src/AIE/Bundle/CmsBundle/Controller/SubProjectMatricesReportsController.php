<?php

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use AIE\Bundle\CmsBundle\Entity\ActionOwners;

use AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports;
use AIE\Bundle\CmsBundle\Form\ActivityPlanningType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivitiesOwnerType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivityMitigationsType;
use AIE\Bundle\CmsBundle\Form\ArchiveReportType;
use Aws\CloudFront\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\HttpFoundation\File\File;
use AIE\Bundle\ReportingBundle\Helper\PhpDocx\TemplateManager;
use AIE\Bundle\CmsBundle\Form\FormCreators\ArchiveReportCreator;
use AIE\Bundle\CmsBundle\Form\FormCreators;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AIE\Bundle\StorageBundle\Upload\FileUploader;
use Symfony\Component\HttpFoundation\Response;

use Aws\S3\Sync\UploadSyncBuilder;

//use Imagick;

use Symfony\Component\Validator\Constraints\DateTime;

/**
 * SubProjectMatricesReports controller.
 *
 * @Route("/{projectId}/{subprojectId}/matricesreports")
 */
class SubProjectMatricesReportsController extends CmsBaseController {

    use ArchiveReportCreator;

    /**
     * Lists all SubProjectMatricesReports entities.
     *
     * @Route("/archives", name="cms_subproject_archives")
     * @Method("GET")
     * @Template("AIECmsBundle:SubProjectMatricesReports:archives.html.twig")
     */
    public function ArchivesAction(Request $request, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $entities = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject' => $subprojectId, 'isApproved' => false), array('date' => 'DESC'));
        $workshops = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->findBySubproject($subprojectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $from_date = new \DateTime('now');
        $to_date = clone $from_date;

        $currMonth = $from_date->format('m');
        $currYear = $from_date->format('Y');

        $qb = $em->createQueryBuilder();
        $qb->select('f')
            ->from('AIECmsBundle:SubProjectActivity', 'f')//replace friends with the correct entity name
            ->where('f.subproject = :spid')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('f.isflowassurancemanagement', ':uid'),
                $qb->expr()->eq('f.isflowassurancecontrol', ':uid')
            ))

            ->setParameter('uid', 1)
            ->setParameter('spid', $subprojectId)
            ->setMaxResults(1);
        $entityFlow = $qb->getQuery()->getResult();

        $qb = $em->createQueryBuilder();
        $qb->select('f')
            ->from('AIECmsBundle:SubProjectActivity', 'f')//replace friends with the correct entity name
            ->where('f.subproject = :spid')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('f.ismanagement', ':uid'),
                $qb->expr()->eq('f.iscontrol', ':uid')
            ))

            ->setParameter('uid', 1)
            ->setParameter('spid', $subprojectId)
            ->setMaxResults(1);
        $entityCorrosion = $qb->getQuery()->getResult();

        if ($subproject->getReportingperiod() == 'quarterly')
        {

            $quarter = self::getQuarterByMonth($currMonth);

            switch ($quarter) {
                case 1:
                    $startmonth = 1;
                    $endmonth = 3;
                    $endday = 31;
                    break;
                case 2:
                    $startmonth = 4;
                    $endmonth = 6;
                    $endday = 30;
                    break;
                case 3:
                    $startmonth = 7;
                    $endmonth = 10;
                    $endday = 30;
                    break;
                case 4:
                    $startmonth = 10;
                    $endmonth = 12;
                    $endday = 31;
                    break;
            }
        }
        else if ($subproject->getReportingperiod() == 'bi-annual')
        {

            $quarter = self::getHalfByMonth($currMonth);

            switch ($quarter) {
                case 1:
                    $startmonth = 1;
                    $endmonth = 6;
                    $endday = 30;
                    break;
                case 2:
                    $startmonth = 7;
                    $endmonth = 12;
                    $endday = 31;
                    break;
            }
        }
        else
        {
            $endmonth = $to_date->format('m');
            $endday = $to_date->format('d');
        }

        $to_date->setDate($currYear, $endmonth, $endday);

        $form = $this->createArchiveFilterForm($projectId, $subprojectId, $from_date, $to_date);

        $assessment_report_forms = array();
        $data = $form->getData();

        foreach ($entities as $entity)
        {
            $assessment_form = $this->createArchiveReportForm($projectId, $subprojectId, $entity->getId(), $workshops, $entity->getReporttype());
            $assessment_report_forms[$entity->getId()] = $assessment_form->createView();
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);

        $reportarchiveShowGranted = 0;
        $reportarchiveGenerateReportGranted = 0;
        $reportarchiveShowReportOnlineGranted = 0;
        $reportarchiveReportDownloadGranted = 0;
        $reportarchiveReportArchiveApproveGranted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_SHOW',$this->userRolesMergeToCheck)){
            $reportarchiveShowGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_GENERATE_REPORT_MATRICES',$this->userRolesMergeToCheck)){
            $reportarchiveGenerateReportGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_SHOW_REPORT_ONLINE',$this->userRolesMergeToCheck)){
            $reportarchiveShowReportOnlineGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_DOWNLOAD_DOCX',$this->userRolesMergeToCheck)){
            $reportarchiveReportDownloadGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_APPROVE',$this->userRolesMergeToCheck)){
            $reportarchiveReportArchiveApproveGranted = 1;
        }

        if (!$entityCorrosion) $entityCorrosion = 0; else $entityCorrosion = 1;
        if (!$entityFlow) $entityFlow = 0; else $entityFlow = 1;

        return array(
            'entities'                => $entities,
            'subprojectId'            => $subprojectId,
            'projectId'               => $projectId,
            'subproject'              => $subproject,
            'form'                    => $form->createView(),
            'assessment_report_forms' => $assessment_report_forms,
            'corrosionAvailable' => $entityCorrosion,
            'flowAvailable' => $entityFlow,
            'reportarchiveShowGranted' => $reportarchiveShowGranted,
            'reportarchiveGenerateReportGranted' => $reportarchiveGenerateReportGranted,
            'reportarchiveShowReportOnlineGranted' => $reportarchiveShowReportOnlineGranted,
            'reportarchiveReportDownloadGranted' => $reportarchiveReportDownloadGranted,
            'reportarchiveReportArchiveApproveGranted' => $reportarchiveReportArchiveApproveGranted,
        );

    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/approved_archives", name="cms_subproject_approved_archives")
     * @Method("GET")
     * @Template("AIECmsBundle:SubProjectMatricesReports:approvedarchives.html.twig")
     */
    public function approvedAssesmentsAction($projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject' => $subprojectId, 'isApproved' => true), array('date' => 'DESC'));
        $workshops = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->findBySubproject($subprojectId);

        $assessment_report_forms = array();
        foreach ($entities as $entity)
        {
            $assessment_form = $this->createArchiveReportForm($projectId, $subprojectId, $entity->getId(), $workshops, $entity->getReporttype());
            $assessment_report_forms[$entity->getId()] = $assessment_form->createView();
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);

        $reportarchiveReportsApprovedShowGranted = 0;
        $reportarchiveReportsApprovedShowOnlineGranted = 0;
        $reportarchiveReportsApprovedDownloadGranted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_APPROVED_SHOW',$this->userRolesMergeToCheck)){
            $reportarchiveReportsApprovedShowGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_APPROVED_SHOW_REPORT_ONLINE',$this->userRolesMergeToCheck)){
            $reportarchiveReportsApprovedShowOnlineGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_APPROVED_DOWNLOAD_DOCX',$this->userRolesMergeToCheck)){
            $reportarchiveReportsApprovedDownloadGranted = 1;
        }

        return array(
            'entities'                => $entities,
            'subprojectId'            => $subprojectId,
            'projectId'               => $projectId,
            'subproject'              => $subproject,
            'assessment_report_forms' => $assessment_report_forms,
            'reportarchiveReportsApprovedShowGranted' => $reportarchiveReportsApprovedShowGranted,
            'reportarchiveReportsApprovedShowOnlineGranted' => $reportarchiveReportsApprovedShowOnlineGranted,
            'reportarchiveReportsApprovedDownloadGranted' => $reportarchiveReportsApprovedDownloadGranted
        );
    }

    private function createArchiveFilterForm($projectId, $subprojectId, \DateTime $from_date, \DateTime $to_date)
    {

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subproject_archivescreate', array('projectId' => $projectId,'subprojectId' => $subprojectId)))
            ->setMethod('POST');

        $form->add('filter_form', new ActivityPlanningType($from_date, $to_date), ['label' => ' ']);

        $form->add('report_type', 'choice', $this->options(array('required' => true,'choices' => $this->getReportChoices(),
            'attr' => array('class'=>'col-md-10','style'=>'padding:0; width:470px;'),
            'label_attr'=> ['style'=>'padding:0; margin:0;'],
            'label'=> 'Type',
            "empty_value" => '--Select--',
        )));

        $form = $form->add('save', ButtonType::class, $this->options(array('label' => 'Generate Report/Matrices','attr' => array('class'=>'col-md-12 pull-right ajaxgeneratereport'),), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Creates a new archives entity.
     *
     * @Route("/archivescreate", name="cms_subproject_archivescreate")
     * @Method("POST")
     */
    public function ArchivesCreateAction(Request $request, $projectId, $subprojectId)
    {
        return true;
    }

    /**
     * prints html report.
     *
     * @Route("/report/{id}", name="cms_subproject_archivereports")
     * @Method("GET")
     */
    public function reportsAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find Sub Project Matrices Reports entity.');
        }

        return new Response($entity->getHtmlReport());
    }

    /**
     * Generates a doc report from the archives of matrices and reports
     *
     * @Route("/{id}-docx", name="cms_subproject_archivereports_docx")
     * @Method("POST")
     */
    public function reportDocxAction(Request $request, $projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find SubProjectMatricesReports entity.');
        }

        $workshops = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->findBySubproject($subprojectId);
        $form = $this->createArchiveReportForm($projectId,$subprojectId, $id, $workshops, $entity->getReporttype());
        $form->handleRequest($request);

        if (! $form->isValid() && $entity->getReporttype() <=4 )
        {
            throw $this->createNotFoundException('Unable to generate document.');
        }

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        //$pipeline = $assessment['pipeline'];
        $project = $assessment['subproject']['project'];
        $subproject = $assessment['subproject'];

        $templateTempPath = tempnam(sys_get_temp_dir(), 'AIE').'.docx';
        $reportData = $this->readFile($entity);
        $fh = fopen($templateTempPath, 'w'); fwrite($fh, $reportData); fclose($fh);
        $templateManage = new TemplateManager($templateTempPath);

        $textVariables = [];
        $tableVariables = [];
        $listVariables = [];

        switch ($entity->getReporttype())
        {

            case 1:
                $reportName = 'AIE-VeracityCCM-CorrosionManagement-ExceptionReport-';
                break;
            case 2:
                $reportName = 'AIE-VeracityCCM-CorrosionManagement-SupplementaryReport-';
                break;
            case 3:
                $reportName = 'AIE-VeracityCCM-CorrosionManagement-ComprehensiveReport-';
                break;
            case 4:
                $reportName = 'AIE-VeracityCCM-MatrixReport-';
                break;
            case 5:
                $reportName = 'AIE-VeracityCCM-CorrosionManagement-ComplianceReport-';
                break;
            case 6:
                $reportName = 'AIE-VeracityCCM-FlowAssuranceManagement-ComprehensiveReport-';
                break;
            case 7:
                $reportName = 'AIE-VeracityCCM-FlowAssuranceManagement-ComplianceReport-';
                break;

        }

        if ( $entity->getReporttype() <= 4  || $entity->getReporttype() == 6 )
        {
            //remove not selected blocks
            $assessment_report_selected_blocks = $form->getData()['archive_report'];

            foreach ($assessment_report_selected_blocks as $block => $include) {
                if (!$include) {
                    $templateManage->deleteBlock($block);
                }
            }

            //Generate IMAGES
            $knpImage = $this->get('knp_snappy.image');
            //$knpImage->getInternalGenerator()->setTimeout($this->_timeoutGeneration);

            $session = $this->get('session');
            $session->save();
            session_write_close();

            $templateManage->addMultipleTexts($textVariables);
            $templateManage->addMultipleHTML($textVariables);
            $templateManage->addMultipleLists($listVariables);

            $templateManage->clearDocument();
            $report = $templateManage->generateDocument();

            //Sending the file to be downloaded
            $bad = array_merge(
                array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

            $name = $reportName . $project['client'] . ' - ' . $subproject['name'];
            $name = str_replace($bad, "", $name);
            $name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $name); #remove non-ascii characters
            $fullName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $name;
            $report->createDocx($fullName);

            $filePath = $fullName . '.' . $report->getExtension();
            $content = file_get_contents($filePath);
            $response = new Response();
            $d = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, "{$name}" . '.' . $report->getExtension()
            );
            $response->headers->set('Content-Disposition', $d);
            $response->setContent($content);

            @unlink($filePath);
            @unlink($templateTempPath);

            return $response;
        }
        else
        {

            //Sending the file to be downloaded
            $bad = array_merge(
                array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

            $name = $reportName . $project['client'] . ' - ' . $subproject['name'];
            $name = str_replace($bad, "", $name);
            $name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $name); #remove non-ascii characters
            //$content = file_get_contents('../web/uploads/'.$entity->getFilepath());
            $content = file_get_contents($this->container->getParameter('amazon_s3_base_url').$entity->getFilepath());
            $response = new Response();
            $d = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT, "{$name}" . '.docx'
            );
            $response->headers->set('Content-Disposition', $d);
            $response->setContent($content);

            return $response;
        }
    }

    /**
     * Approved archives
     *
     * @Route("/approve/{id}", name="cms_subproject_archivereports_approve")
     * @Method("GET")
     * @Template("AIECmsBundle:Reports:archives.html.twig")
     */
    public function approveAction($projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->find($id);

        $quarterdet = json_decode($entity->getComplianceData());

        $rfrom_date = new \DateTime($quarterdet->from_date->date);
        $rto_date = new \DateTime($quarterdet->to_date->date);

        $subprojectentity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find SubProjectMatricesReports entity.');
        }

        $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->updateApproved($subprojectId, $entity->getReportingFromDate(),$entity->getReportingToDate(),$subprojectentity->getReportingperiod());

        $entity->setIsApproved(true);
        $em->flush();


        //$this->updateArchives($subprojectId, $entity, $entity->getReporttype(), $entity->getReportingFromDate(), $entity->getReportingToDate());

        return $this->redirect($this->generateUrl('cms_subproject_archives', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
    }

    /**
     * updates the reports archives with html in reports tables and stores physical files
     */
    public function updateArchives($subprojectId, $entity, $report_type, $from_date, $to_date, $baseUrl)
    {
        if ( !$report_type )
        {
            $this->addFlash('error', 'Please select a report type before generating report');
            return;
        }

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);

        $subproject = $assessment['subproject'];
        $project = $assessment['subproject']['project'];

        $subprojectEntity = $em->getRepository('AIECmsBundle:SubProjects')->find($subproject['id']);
        $projectEntity = $em->getRepository('AIECmsBundle:Projects')->find($project['id']);

        $subprojectactivityData = $assessment['subproject']['activities'];

        $quarterCompliances = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->getQuarterReports($subproject['id'], $from_date, $to_date);

        $now = new \DateTime("now");
        $currMonth = $now->format('m');
        $currYear = $now->format('Y');

        $quarter = self::getQuarterByMonth($currMonth);

        $currentCompliances = $assessment['subproject']['compliance'];

        $currentCompliances['quarter'] = $quarter;

        $references_list = array();

        if ($subproject['documents']) {

            foreach ($subproject['documents'] as $document) {
                $reference = "{$document['filename']}, {$document['company']}, Document Number {$document['document_number']}";

                if ($document['document_date']) {
                    if (is_array($document['document_date'])) {
                        $date = new \DateTime($document['document_date']['date']);
                    } else
                        $date = $document['document_date'];
                    $date = $date->format('d F Y');
                    $reference .= ", $date";
                }
                $references_list[] = $reference;
            }
        }

        $tempattendee = 0;

        $companyName = "";

        if ($subproject['meeting'])
        {

            foreach ($subproject['meeting'] as $meeting) {
                if (is_array($meeting['date'])) {
                    $meetingDate = new \DateTime($meeting['date']['date']);
                    $meetingDate = $meetingDate->format('d-m-y');
                } else {
                    $meetingDate = $meeting['date']->format('d-m-y');
                }
                if ( isset($meeting['attendees']) && !empty($meeting['attendees'])) {
                    foreach ($meeting['attendees'] as $attendees) {
                        if ($tempattendee != $attendees['id']) {
                            if ($companyName)
                                $companyName = $companyName . ", " . $attendees['company'];
                            else
                                $companyName = $attendees['company'];

                        }
                        $tempattendee = $attendees['id'];
                    }
                }

            }
        }

        $html = $this->renderView('AIECmsBundle:Reports:archives.html.twig', array(
            'archive' => $entity,
            'subprojectId' => $subprojectId,
            'subproject' => $subproject,
            'quarterCompliances' => $quarterCompliances,
            'currentCompliances' => $currentCompliances,
            'project' => $project,
            'reportType' => $report_type,
        ));

        //$html = mb_convert_encoding($html, 'UTF-8', mb_detect_encoding($html, 'UTF-8, ISO-8859-1', true));
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");

//        label:debug_html
//        echo $html;

        //iconv(mb_detect_encoding($html, mb_detect_order(), true), "UTF-8", $html);

        $html_settings = array(
            'isFile' => false,
            'parseDivsAsPs' => true,
            'downloadImages' => true,
            'parseFloats' => true,
            'strictWordStyles' => true,
            'wordStyles' => array(
                '.table' => 'AIETable',
                '.appendix-title' => 'AppendixTitle',
                '.appendix-desc' => 'AppendixDesc',
                '.caption' => 'Caption',
                '' => 'Normal'
            )
        );

        $table_settings = array('header' => true);
        if ($report_type == 1) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-CMR-EXCEPTION-TEMPLATE_rev2.docx");
        } else if ($report_type == 3) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-CMR-COMPREHENSIVE-TEMPLATE_rev2.docx");
        } else if ($report_type == 2) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-SUPPLEMENTARY-TEMPLATE_rev2.docx");
        } else if ($report_type == 4) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-CORROSION-MATRIX_rev2.docx");
        } else if ($report_type == 5) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-COMPLIANCE-TEMPLATE_rev2.docx");
        } else if ($report_type == 6) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-FLOW-ASSURANCE-MANAGEMENT-TEMPLATE_rev1.docx");
        }
        else if ($report_type == 7) {
            $template = $this->get('kernel')->locateResource("@AIECmsBundle/Reports/Templates/AIE-FLOW-ASSURANCE-COMPLIANCE-TEMPLATE_rev1.docx");
        }

        $templateManage = new TemplateManager($template);

        $templateManage->setHtmlSettings($html_settings);
        $templateManage->setTableSettings($table_settings);

        try {
            $parser = $this->container->get('simple_html_dom');
            $parser->load($html);
        } catch (Exception $e) {
            // handle error here
            $this->addFlash('error', 'Report Type Not Selected');
            return;
        }

        if ($subprojectactivityData) $subprojectactivityData = array_filter($subprojectactivityData, function($value) { return $value !== ''; });

        $systems = array();

        foreach ($subprojectactivityData as $key => $activity)
        {

            $id = $activity['id'];

            $systems[] = ucfirst($activity['system']['name']);

            if (!empty($activity['system']['locations']))
            {

                if ($activity['control']['selected']==1 && $activity['management']['selected']==1)
                {
                    $controlTitle= $activity['control']['title'];
                }
                else if ($activity['control']['selected']==0 && $activity['management']['selected']==1)
                {
                    $controlTitle= $activity['management']['title'];
                }
                else if ($activity['control']['selected']==1 && $activity['management']['selected']==0)
                {
                    $controlTitle= $activity['control']['title'];
                }
                else if ($activity['control']['selected']==0 && $activity['management']['selected']==0 && $activity['non_monitoring_kpi']['selected']==1)
                {
                    $controlTitle= $activity['non_monitoring_kpi']['title'];
                }

                $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $activity['system']['id']);

                $avgreenThreshold = $avredThreshold = $volaverageredThreshold = $volaveragegreenThreshold = "";

                if (count($chemicalActivities))
                {
                    foreach ($chemicalActivities as $chemicalActivity)
                    {

                        if ($chemicalActivity['bmc_corrosioninhibitortype'] == 1)
                        {
                            if ($chemicalActivity['bsd_controlgreenrange'] != "") $avgreenThreshold = $chemicalActivity['bsd_controlgreenrange']; else $avgreenThreshold = $chemicalActivity['bsd_controlgreen'];
                            if ($chemicalActivity['bsd_controlredrange'] != "") $avredThreshold = $chemicalActivity['bsd_controlredrange']; else $avredThreshold = $chemicalActivity['bsd_controlred'];
                        }

                        if ($chemicalActivity['bmc_corrosioninhibitortype'] == 2)
                        {
                            if ($chemicalActivity['bsd_controlgreenrange'] != "") $volaveragegreenThreshold = $chemicalActivity['bsd_controlgreenrange']; else $volaveragegreenThreshold = $chemicalActivity['bsd_controlgreen'];
                            if ($chemicalActivity['bsd_controlredrange'] != "") $volaverageredThreshold = $chemicalActivity['bsd_controlredrange']; else $volaverageredThreshold = $chemicalActivity['bsd_controlred'];
                        }

                    }
                }

                if ($report_type <= 3 || $report_type == 6) {

                    $CorrosionGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['corrosion_probe_graphs'], 'corrosion', 'CorrosionProbeGraphs', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);

                    $ChemicalTypeOneGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['chemical_type1_graphs'], 'chemical', 'ChemicalTypeOneGraphs', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);

                    $ChemicalTypeTwoInjectionGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['chemical_type2_injection_graphs'], 'chemical', 'ChemicalTypeTwoInjectionGraphs', 'CI Injection, L/Day', '', '', '');

                    $ChemicalTypeTwoConcentrationGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['chemical_type2_concentration_graphs'], 'chemical', 'ChemicalTypeTwoConcentrationGraphs', 'Corrosion Inhibitor Concentration', '', '', '');

                    $ChemicalTypeTwoAvailabilityGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['chemical_type2_availability_graphs'], 'chemical', 'ChemicalTypeTwoAvailabilityGraphs', 'Corrosion Inhibitor availability %', $avgreenThreshold, $avredThreshold, '');

                    $ChemicalTypeTwoAverageGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['chemical_type2_average_graphs'], 'chemical', 'ChemicalTypeTwoAverageGraphs', 'Corrosion Inhibitor Average Availability', $volaveragegreenThreshold, $volaverageredThreshold, $activity['system']['locations']['averagethresholds']);

                    $SamplingGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['sampling_graphs'], 'sampling', 'SamplingGraphs', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);

                    $CathodicGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['cathodic_protection_graphs'], 'cathodic', 'CathodicProtectionGraphs', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);

                    $CathodicOnOffGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['cathodic_protection_graphs_two'], 'cathodiconoff', 'CathodicProtectionGraphsOnOff', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);

                    if (!empty($CorrosionGraphs)) {
                        $systemLocationGraphReplacement['corrosion'][$activity['system']['id']][$id] = $CorrosionGraphs;
                    }

                    if (!empty($ChemicalTypeOneGraphs)) {
                        $systemLocationGraphReplacement['chemicaltypeone'][$activity['system']['id']][$id] = $ChemicalTypeOneGraphs;
                    }

                    if (!empty($SamplingGraphs)) {
                        $systemLocationGraphReplacement['sampling'][$activity['system']['id']][$id] = $SamplingGraphs;
                    }

                    if (!empty($CathodicGraphs)) {
                        $systemLocationGraphReplacement['cathodic'][$activity['system']['id']][$id] = $CathodicGraphs;
                    }

                    if (!empty($CathodicOnOffGraphs)) {
                        $systemLocationGraphReplacement['cathodiconoff'][$activity['system']['id']][$id] = $CathodicOnOffGraphs;
                    }

                    if (!empty($ChemicalTypeTwoConcentrationGraphs)) {
                        $systemLocationGraphReplacement['chemicaltypetwoconcentration'][$activity['system']['id']][$id] = $ChemicalTypeTwoConcentrationGraphs;
                    }
                    if (!empty($ChemicalTypeTwoInjectionGraphs)) {
                        $systemLocationGraphReplacement['chemicaltypetwoinjection'][$activity['system']['id']][$id] = $ChemicalTypeTwoInjectionGraphs;
                    }
                    if (!empty($ChemicalTypeTwoAvailabilityGraphs)) {
                        $systemLocationGraphReplacement['chemicaltypetwoavailability'][$activity['system']['id']][$id] = $ChemicalTypeTwoAvailabilityGraphs;
                    }
                    if (!empty($ChemicalTypeTwoAverageGraphs)) {
                        $systemLocationGraphReplacement['chemicaltypetwoaverage'][$activity['system']['id']][$id] = $ChemicalTypeTwoAverageGraphs;
                    }

                    $ProductionGraphs = $this->filterGraphArray($id, $activity['system']['id'], $activity['system']['locations']['production_graphs'], 'production', 'Production', $controlTitle, $activity['control']['green'], $activity['control']['red'], $activity['system']['locations']['thresholds']);
                    //$ProductionGraphs = $this->filterProductionGraphArray($activity['id'], $activity['system']['production_graphs']);

                    if (!empty($ProductionGraphs)) {
                        $systemLocationGraphReplacement['production'][$activity['system']['id']][$id] = $ProductionGraphs;
                    }

                }
            }

        }

        $quarter1 = $quarter2 = $quarter3 = $quarter4 = 0;

        $complianceStart = "[";
        $complianceEnd = "]";

        if ($currentCompliances)
        {
            if ($report_type > 5 )
            {
                $quarter4_controlcompliance = round($currentCompliances['flowassuranceoverallcontrolcompliance'], 1);
                $quarter4_managementcompliance = round($currentCompliances['flowassuranceoverallmanagementcompliance'], 1);
            }
            else
            {
                $quarter4_controlcompliance = round($currentCompliances['overallcontrolcompliance'], 1);
                $quarter4_managementcompliance = round($currentCompliances['overallmanagementcompliance'], 1);
            }
            $rfrom_date = new \DateTime($currentCompliances['from_date']->date);
            $rto_date = new \DateTime($currentCompliances['to_date']->date);
            $quarter4_range = $rfrom_date->format('d/m/Y') . "-" . $rto_date->format('d/m/Y');

            if (date('t', strtotime($rto_date->format('Y-m-d'))) != 31)
                $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($rto_date->format('Y-m-d'))));
            else
                $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($rto_date->format('Y-m-d'))));

            $complianceControlData = "[Date.UTC(".$replacedt."),".$quarter4_controlcompliance."]";
            $complianceManagementData = "[Date.UTC(".$replacedt."),".$quarter4_managementcompliance."]";

            $complianceRedData = "[Date.UTC(".$replacedt."),50]";
            $complianceGreenData = "[Date.UTC(".$replacedt."),75]";
        }

        if ($quarterCompliances)
        {
            $inc = 2;
            foreach ($quarterCompliances as $quarter)
            {
                $quarterdet = json_decode($quarter['complianceData']);

                if ($report_type > 5 )
                {
                    $quarter_control = $quarterdet->flowassuranceoverallcontrolcompliance;
                    $quarter_management = $quarterdet->flowassuranceoverallmanagementcompliance;
                }
                else{
                    $quarter_control = $quarterdet->overallcontrolcompliance;
                    $quarter_management = $quarterdet->overallmanagementcompliance;
                }

                if ($inc == 2)
                {
                    $quarter3_controlcompliance = round($quarter_control, 1);
                    $quarter3_managementcompliance = round($quarter_management, 1);

                    $rfrom_date = new \DateTime($quarterdet->from_date->date);
                    $rto_date = new \DateTime($quarterdet->to_date->date);
                    $quarter3_range = $rfrom_date->format('d/m/Y') . "-" . $rto_date->format('d/m/Y');

                    if (date('t', strtotime($rto_date->format('Y-m-d'))) != 31)
                        $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($rto_date->format('Y-m-d'))));
                    else
                        $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($rto_date->format('Y-m-d'))));

                    //echo $replacedt."<br>";

                    $complianceControlData.= ",[Date.UTC(".$replacedt."),".$quarter3_controlcompliance."]";
                    $complianceManagementData.= ",[Date.UTC(".$replacedt."),".$quarter3_managementcompliance."]";
                }
                else if ($inc == 3)
                {
                    $quarter2_controlcompliance = round($quarter_control, 1);
                    $quarter2_managementcompliance = round($quarter_management, 1);

                    $rfrom_date = new \DateTime($quarterdet->from_date->date);
                    $rto_date = new \DateTime($quarterdet->to_date->date);
                    $quarter2_range = $rfrom_date->format('d/m/Y') . "-" . $rto_date->format('d/m/Y');

                    if (date('t', strtotime($rto_date->format('Y-m-d'))) != 31)
                        $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($rto_date->format('Y-m-d'))));
                    else
                        $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($rto_date->format('Y-m-d'))));

                    //echo $replacedt."<br>";

                    $complianceControlData.= ",[Date.UTC(".$replacedt."),".$quarter2_controlcompliance."]";
                    $complianceManagementData.= ",[Date.UTC(".$replacedt."),".$quarter2_managementcompliance."]";
                }
                else if ($inc == 4)
                {
                    $quarter1_controlcompliance = round($quarter_control, 1);
                    $quarter1_managementcompliance = round($quarter_management, 1);

                    $rfrom_date = new \DateTime($quarterdet->from_date->date);
                    $rto_date = new \DateTime($quarterdet->to_date->date);
                    $quarter1_range = $rfrom_date->format('d/m/Y') . "-" . $rto_date->format('d/m/Y');

                    if (date('t', strtotime($rto_date->format('Y-m-d'))) != 31)
                        $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($rto_date->format('Y-m-d'))));
                    else
                        $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($rto_date->format('Y-m-d'))));

                    //echo $replacedt."<br>";

                    $complianceControlData.= ",[Date.UTC(".$replacedt."),".$quarter1_controlcompliance."]";
                    $complianceManagementData.= ",[Date.UTC(".$replacedt."),".$quarter1_managementcompliance."]";
                }
                if (date('t', strtotime($rto_date->format('Y-m-d'))) != 31)
                    $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($rto_date->format('Y-m-d'))));
                else
                    $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($rto_date->format('Y-m-d'))));

                $complianceRedData.= ",[Date.UTC(".$replacedt."),50]";
                $complianceGreenData.= ",[Date.UTC(".$replacedt."),75]";
                $inc++;
            }
        }

        $complianceManagementData = $complianceStart.$complianceManagementData.$complianceEnd;
        $complianceControlData = $complianceStart.$complianceControlData.$complianceEnd;
        $complianceRedData = $complianceStart.$complianceRedData.$complianceEnd;
        $complianceGreenData = $complianceStart.$complianceGreenData.$complianceEnd;

        $graphComplianceData= array('Control Compliance'=>$complianceControlData,'ManagementCompliance'=>$complianceManagementData);
        $graphComplianceThresholdData= array('Thresholds'=>$complianceRedData."|".$complianceGreenData);

        $textVariables = array(
            'SUB-PROJECT_NAME' => $this->br2nl($subproject['name']),
            'SUB-PROJECT_LOCATION' => $this->br2nl($subproject['location']),
            'SUB-PROJECT_DESCRIPTION' => $this->br2nl($subproject['description']),
            'DATE_RANGE' => $project['dateRange'],
            'PROJECT_NAME' => $project['name'],
            'PROJECT_DESCRIPTION' => $this->br2nl($project['description']),
            'YEAR' => $currYear,
            'CLIENT_NAME' => $project['client'],
            'REPORT_ID' => $entity->getId(),
            'REFERENCE_NUMBER' => $project['referenceNo'],
            'CREATED_DATE' => $entity->getDate()->format('j F Y'),
            //'REFERENCES_LIST' => $references_list,
            'SYSTEMS' => array_map(function ($val) {
                return $val;
            }, $systems),
            'RECOMMENDED_SYSTEMS' => array_map(function ($val) {
                return $val;
            }, $systems),
            'Q1_RANGE' => $quarter1_range,
            'Q2_RANGE' => $quarter2_range,
            'Q3_RANGE' => $quarter3_range,
            'Q4_RANGE' => $quarter4_range,
            'Q1_CONTROL' => $quarter1_controlcompliance,
            'Q2_CONTROL' => $quarter2_controlcompliance,
            'Q3_CONTROL' => $quarter3_controlcompliance,
            'Q4_CONTROL' => $quarter4_controlcompliance,
            'Q1_MANAGEMENT' => $quarter1_managementcompliance,
            'Q2_MANAGEMENT' => $quarter2_managementcompliance,
            'Q3_MANAGEMENT' => $quarter3_managementcompliance,
            'Q4_MANAGEMENT' => $quarter4_managementcompliance,
            'MEETING_ATTENDED_BY' => $companyName,
            'MEETING_DATE' => $meetingDate
        );

        $corrosion_control_kpi_tbl = $parser->find("#corrosion_control_kpi", 0);
        $corrosion_management_kpi_tbl = $parser->find("#corrosion_management_kpi", 0);
        $flowassurance_control_kpi_tbl = $parser->find("#flow_assurance_control_kpi", 0);
        $flowassurance_management_kpi_tbl = $parser->find("#flow_assurance_management_kpi", 0);
        $defined_corrosion_control_tbl = $parser->find("#defined_control_kpi", 0);
        $defined_corrosion_management_tbl = $parser->find("#defined_management_kpi", 0);
        $defined_flowassurance_control_tbl = $parser->find("#flow_defined_control_kpi", 0);
        $defined_flowassurance_management_tbl = $parser->find("#flow_defined_management_kpi", 0);
        $meetings_tbl = $parser->find("#meetings", 0);
        $monitoring_activity_tbl = $parser->find("#monitoring_kpi", 0);
        $action_tracker_tbl = $parser->find("#action_tracker", 0);
        $corrosion_management_tbl = $parser->find("#corrision_man_ctrl", 0);
        $flowassurance_management_tbl = $parser->find("#flowassurance_man_ctrl", 0);
        $recommendations_tbl = $parser->find("#recommendations", 0);
        $production_profile_tbl = $parser->find("#production_profile", 0);
        $overallcompliance_graph_tbl = $parser->find("#overallcompliance_graph", 0);
        $documents_tbl = $parser->find("#documents-explorer", 0);
        $corrosion_matrix_management_tbl = $parser->find("#corrosion_matrix_management", 0);
        $corrosion_matrix_control_tbl = $parser->find("#corrosion_matrix_control", 0);
        $flowassurance_matrix_management_tbl = $parser->find("#flowassurance_matrix_management", 0);
        $flowassurance_matrix_control_tbl = $parser->find("#flowassurance_matrix_control", 0);

        foreach ($parser->find('table thead th') as $element) {
            $element->style = 'background: transparent; font-size: 13px;';
            $inner = $element->first_child();
            if ($inner) {
                $inner->style = 'background: transparent; font-weight: bold; text-align: center; font-size: 13px;';
            }
        }
        $wordStyles = array(
            '<table>' => 'Normal PHPDOCX'
        );

        $headerVariables = array(
            'SUB-PROJECT_NAME' => $this->br2nl($subproject['name']),
            'DATE_RANGE' => $project['dateRange'],
            'PROJECT_NAME' => $project['name'],
            'CLIENT_NAME' => $project['client'],
            'YEAR' => $currYear
        );

        $listVariables = array(
            'REFERENCES_LIST' => array('list' => $references_list),
        );

        if ( $report_type == 3 )
        {
            if(is_object($corrosion_control_kpi_tbl) === true && is_object($corrosion_management_kpi_tbl) === true &&
                is_object($defined_corrosion_control_tbl) === true && is_object($defined_corrosion_management_tbl) === true &&
                is_object($meetings_tbl) === true && is_object($monitoring_activity_tbl) === true && is_object($recommendations_tbl) === true &&
                is_object($production_profile_tbl) === true && is_object($overallcompliance_graph_tbl) === true && is_object($documents_tbl) === true &&
                is_object($action_tracker_tbl) === true && is_object($corrosion_management_tbl) === true
            )
            {

                (!empty($corrosion_control_kpi_tbl) ? $corrosion_control_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_management_kpi_tbl) ? $corrosion_management_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_corrosion_control_tbl) ? $defined_corrosion_control_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_corrosion_management_tbl) ? $defined_corrosion_management_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($meetings_tbl) ? $meetings_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($monitoring_activity_tbl) ? $monitoring_activity_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($action_tracker_tbl) ? $action_tracker_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin:0; font-size: 10px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_management_tbl) ? $corrosion_management_tbl->style = 'border-collapse: collapse; border: 1px solid white; width: 800px; padding-left: 0px; text-indent: 0px; font-size: 10px;' : "");
                (!empty($recommendations_tbl) ? $recommendations_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px;' : "");
                (!empty($production_profile_tbl) ? $production_profile_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px;' : "");
                (!empty($overallcompliance_graph_tbl) ? $overallcompliance_graph_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px;' : "");
                (!empty($documents_tbl) ? $documents_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px;' : "");

                $htmlVariables = array(
                    'CORROSION_CONTROL_KPIS_TABLE' => array('html' => $corrosion_control_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_KPIS_TABLE' => array('html' => $corrosion_management_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_CORROSION_CONTROL_TABLE' => array('html' => $defined_corrosion_control_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_CORROSION_MANAGEMENT_TABLE' => array('html' => $defined_corrosion_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'MEETINGS_TABLE' => array('html' => $meetings_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'ACTION_TRACKER_TABLE' => array('html' => $action_tracker_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'MONITORING_ACTIVITY_TABLE' => array('html' => $monitoring_activity_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_TABLE' => array('html' => $corrosion_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'RECOMMENDATIONS_TABLE' => array('html' => $recommendations_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'TRENDS_MONITORING_TABLE' => array('html' => $parser->find("#trends_monitoring_nonkpi_activity", 0), 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'PRODUCTION_PROFILE_TABLE' => array('html' => $production_profile_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'COMPLIANCE_GRAPH_TABLE' => array('html' => $overallcompliance_graph_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'REFERENCES_LIST' => array('html' => $documents_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Corrosion comprehensive online report content generation error');
                return;
            }
        }
        else if ( $report_type == 1 )
        {

            if(is_object($corrosion_control_kpi_tbl) === true && is_object($corrosion_management_kpi_tbl) === true &&
                is_object($defined_corrosion_control_tbl) === true && is_object($defined_corrosion_management_tbl) === true &&
                is_object($action_tracker_tbl) === true && is_object($corrosion_management_tbl) === true && is_object($recommendations_tbl) === true &&
                is_object($production_profile_tbl) === true && is_object($overallcompliance_graph_tbl) === true && is_object($documents_tbl) === true
            )
            {

                (!empty($corrosion_control_kpi_tbl) ? $corrosion_control_kpi_tbl->style = 'border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_management_kpi_tbl) ? $corrosion_management_kpi_tbl->style = 'border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_corrosion_control_tbl) ? $defined_corrosion_control_tbl->style = 'border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_corrosion_management_tbl) ? $defined_corrosion_management_tbl->style = 'border-collapse: collapse;border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($action_tracker_tbl) ? $action_tracker_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_management_tbl) ? $corrosion_management_tbl->style = 'border-collapse: collapse; border: 1px solid white; width: 800px; padding-left: 0px; text-indent: 0px; font-size: 10px; border-spacing: 5px; padding: 5px;' : "");
                (!empty($recommendations_tbl) ? $recommendations_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");
                (!empty($production_profile_tbl) ? $production_profile_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");
                (!empty($overallcompliance_graph_tbl) ? $overallcompliance_graph_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");
                (!empty($documents_tbl) ? $documents_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");

                $htmlVariables = array(
                    'CORROSION_CONTROL_KPIS_TABLE' => array('html' => $corrosion_control_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_KPIS_TABLE' => array('html' => $corrosion_management_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_CORROSION_CONTROL_TABLE' => array('html' => $defined_corrosion_control_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_CORROSION_MANAGEMENT_TABLE' => array('html' => $defined_corrosion_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'ACTION_TRACKER_TABLE' => array('html' => $action_tracker_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_TABLE' => array('html' => $corrosion_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'RECOMMENDATIONS_TABLE' => array('html' => $recommendations_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'PRODUCTION_PROFILE_TABLE' => array('html' => $production_profile_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'COMPLIANCE_GRAPH_TABLE' => array('html' => $overallcompliance_graph_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'REFERENCES_LIST' => array('html' => $documents_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );

            }
            else
            {
                $this->addFlash('error', 'Corrosion exception online report content generation error');
                return;
            }
        }
        else if ( $report_type == 2 )
        {

            if(is_object($meetings_tbl) === true && is_object($monitoring_activity_tbl) === true && is_object($meetings_tbl) === true && is_object($action_tracker_tbl) === true && is_object($corrosion_management_tbl) === true && is_object($monitoring_activity_tbl) === true)
            {
                (!empty($meetings_tbl) ? $meetings_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;': "") ;
                (!empty($monitoring_activity_tbl) ? $monitoring_activity_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;': "") ;
                (!empty($action_tracker_tbl) ? $action_tracker_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;': "") ;
                (!empty($corrosion_management_tbl) ? $corrosion_management_tbl->style = 'border-collapse: collapse; border: 1px solid white; width: 650px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;': "") ;
                (!empty($documents_tbl) ? $documents_tbl->style = 'border-collapse: collapse; width: 600px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;': "") ;

                $htmlVariables = array(
                    'MEETINGS_TABLE' => array('html' => $meetings_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'ACTION_TRACKER_TABLE' =>  array('html' => $action_tracker_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'MONITORING_ACTIVITY_TABLE' =>  array('html' => $monitoring_activity_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_TABLE'=> array('html' => $parser->find("#corrision_man_ctrl", 0), 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'TRENDS_MONITORING_TABLE'=> array('html' => $parser->find("#trends_monitoring_nonkpi_activity", 0), 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'REFERENCES_LIST'=> array('html' => $documents_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Corrosion supplementary online report content generation error');
                return;
            }

        }
        else if ( $report_type == 4 )
        {

            if(is_object($corrosion_matrix_management_tbl) === true && is_object($corrosion_matrix_control_tbl) === true && is_object($flowassurance_matrix_management_tbl) === true && is_object($flowassurance_matrix_control_tbl) === true && is_object($monitoring_activity_tbl) === true) {

                (!empty($corrosion_matrix_management_tbl) ? $corrosion_matrix_management_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 800px; margin:0px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_matrix_control_tbl) ? $corrosion_matrix_control_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 800px; margin:0px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_matrix_management_tbl) ? $flowassurance_matrix_management_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 800px; margin:0px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_matrix_control_tbl) ? $flowassurance_matrix_control_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 800px; margin:0px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($monitoring_activity_tbl) ? $monitoring_activity_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");

                $htmlVariables = array(
                    'CORROSION_MATRIX_MANAGEMENT_TABLE' => array('html' => $corrosion_matrix_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MATRIX_CONTROL_TABLE' => array('html' => $corrosion_matrix_control_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'FLOWASSURANCE_MATRIX_MANAGEMENT_TABLE' => array('html' => $flowassurance_matrix_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'FLOWASSURANCE_MATRIX_CONTROL_TABLE' => array('html' => $flowassurance_matrix_control_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'MONITORING_ACTIVITY_TABLE' => array('html' => $monitoring_activity_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Corrosion and flow assurance online report content generation error');
                return;
            }

        }
        else if ( $report_type == 5 )
        {

            $corrosion_control_kpi_tbl = $parser->find("#corrosion_control_compliance_kpi", 0);
            $corrosion_management_kpi_tbl = $parser->find("#corrosion_management_compliance_kpi", 0);

            if(is_object($corrosion_control_kpi_tbl) === true && is_object($corrosion_management_kpi_tbl) === true && is_object($overallcompliance_graph_tbl) === true) {

                (!empty($overallcompliance_graph_tbl) ? $overallcompliance_graph_tbl->style = 'width: 600px; margin-left:-100px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_control_kpi_tbl) ? $corrosion_control_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($corrosion_management_kpi_tbl) ? $corrosion_management_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");

                $htmlVariables = array(
                    'CORROSION_CONTROL_KPIS_TABLE' => array('html' => html_entity_decode($corrosion_control_kpi_tbl), 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'CORROSION_MANAGEMENT_KPIS_TABLE' => array('html' => html_entity_decode($corrosion_management_kpi_tbl), 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'COMPLIANCE_GRAPH_TABLE' => array('html' => $overallcompliance_graph_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Corrosion compliance online report content generation error');
                return;
            }
        }
        else if ( $report_type == 6 )
        {

            if(is_object($flowassurance_control_kpi_tbl) === true && is_object($flowassurance_management_kpi_tbl) === true &&
                is_object($defined_flowassurance_control_tbl) === true && is_object($defined_flowassurance_management_tbl) === true &&
                is_object($recommendations_tbl) === true && is_object($overallcompliance_graph_tbl) === true &&
                is_object($documents_tbl) === true && is_object($flowassurance_management_tbl) === true
            )
            {
                (!empty($flowassurance_control_kpi_tbl) ? $flowassurance_control_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_management_kpi_tbl) ? $flowassurance_management_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_flowassurance_control_tbl) ? $defined_flowassurance_control_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($defined_flowassurance_management_tbl) ? $defined_flowassurance_management_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_management_tbl) ? $flowassurance_management_tbl->style = 'border-collapse: collapse; border: 1px solid white; width: 800px; padding-left: 0px; text-indent: 0px; font-size: 10px;' : "");
                (!empty($recommendations_tbl) ? $recommendations_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");
                (!empty($overallcompliance_graph_tbl) ? $overallcompliance_graph_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");
                (!empty($documents_tbl) ? $documents_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; font-size: 10px; align:center; border-spacing: 5px; padding: 5px;' : "");

                $htmlVariables = array(
                    'FLOWASSURANCE_CONTROL_KPIS_TABLE' => array('html' => $flowassurance_control_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'FLOWASSURANCE_MANAGEMENT_KPIS_TABLE' => array('html' => $flowassurance_management_kpi_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_FLOWASSURANCE_CONTROL_TABLE' => array('html' => $defined_flowassurance_control_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'DEFINED_KPI_FLOWASSURANCE_MANAGEMENT_TABLE' => array('html' => $defined_flowassurance_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'FLOWASSURANCE_MANAGEMENT_TABLE' => array('html' => $flowassurance_management_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'RECOMMENDATIONS_TABLE' => array('html' => $recommendations_tbl, 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'COMPLIANCE_GRAPH_TABLE' => array('html' => $overallcompliance_graph_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                    'REFERENCES_LIST' => array('html' => $documents_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Flow assurance online report content generation error');
                return;
            }
        }
        else if ( $report_type == 7 )
        {

            $flowassurance_control_kpi_tbl = $parser->find("#flow_assurance_control_compliance_kpi", 0);
            $flowassurance_management_kpi_tbl = $parser->find("#flow_assurance_management_compliance_kpi", 0);

            if(is_object($flowassurance_control_kpi_tbl) === true && is_object($flowassurance_management_kpi_tbl) === true && is_object($overallcompliance_graph_tbl) === true) {

                (!empty($overallcompliance_graph_tbl) ? $overallcompliance_graph_tbl->style = 'width: 600px; margin-left:-100px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_control_kpi_tbl) ? $flowassurance_control_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");
                (!empty($flowassurance_management_kpi_tbl) ? $flowassurance_management_kpi_tbl->style = 'border-collapse: collapse; border: 1px solid black; width: 600px; margin-left:-10px; font-size: 10px; border-spacing: 5px; padding: 5px; align:center;' : "");

                $htmlVariables = array(
                    'FLOWASSURANCE_CONTROL_KPIS_TABLE' => array('html' => html_entity_decode($flowassurance_control_kpi_tbl), 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'FLOWASSURANCE_MANAGEMENT_KPIS_TABLE' => array('html' => html_entity_decode($flowassurance_management_kpi_tbl), 'settings' => array('strictWordStyles' => false) + $html_settings),
                    'COMPLIANCE_GRAPH_TABLE' => array('html' => $overallcompliance_graph_tbl, 'settings' => array('strictWordStyles' => true) + $html_settings),
                );
            }
            else
            {
                $this->addFlash('error', 'Flow assurance compliance online report content generation error');
                return;
            }
        }

        $chartMultiSeriesPath = $this->generateSeriesGraphData( 'comp', $graphComplianceData, $titleString='Overall Compliance %', $xaxisString="Overall Compliance %", $yaxisString="Overall Compliance %", $thresholdMin=null, $thresholdMax=null, $fromDate=null, $toDate=null, $graphComplianceThresholdData, true, null, true);

        if ( $report_type <= 3 || $report_type == 6 )
        {

            if (count($systemLocationGraphReplacement['production']) && !empty($systemLocationGraphReplacement['production'])) {

                foreach ($systemLocationGraphReplacement['production'] as $systemIndex => $locationGraph) {

                    foreach ($locationGraph as $innerdata) {

                        if ($innerdata) {

                            foreach ($innerdata as $innerindex => $dataImg) {

                                if ($dataImg && $innerindex) {

                                    $innerCathodicContainerHtml = $parser->find("#" . $innerindex, 0);

                                    $imgCathodicContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                    if ($innerCathodicContainerHtml) {

                                        $innerCathodicContainerHtml->innertext = $imgCathodicContainerStr;
//                                        $innerCathodicContainerHtml->outertext = "";
                                    }
                                }
                            }

                        }

                    }
                }
            }

            if (count($systemLocationGraphReplacement['cathodic']) && !empty($systemLocationGraphReplacement['cathodic'])) {

                foreach ($systemLocationGraphReplacement['cathodic'] as $systemIndex => $locationGraph) {

                    foreach ($locationGraph as $innerdata) {

                        if ($innerdata) {

                            foreach ($innerdata as $innerindex => $dataImg) {
                                if ($dataImg && $innerindex) {

                                    $innerCathodicContainerHtml = $parser->find("#" . $innerindex, 0);

                                    $imgCathodicContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                    if ($innerCathodicContainerHtml) {

                                        $innerCathodicContainerHtml->innertext = $imgCathodicContainerStr;
//                                        $innerCathodicContainerHtml->outertext = "";
                                    }
                                }
                            }

                        }

                    }
                }
            }

            if (count($systemLocationGraphReplacement['cathodiconoff']) && !empty($systemLocationGraphReplacement['cathodiconoff'])) {

                foreach ($systemLocationGraphReplacement['cathodiconoff'] as $systemIndex => $locationGraph) {

                    foreach ($locationGraph as $innerdata) {

                        if ($innerdata) {

                            foreach ($innerdata as $innerindex => $dataImg) {
                                if ($dataImg && $innerindex) {

                                    $innerCathodicContainerHtml = $parser->find("#" . $innerindex, 0);

                                    $imgCathodicContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                    if ($innerCathodicContainerHtml) {

                                        $innerCathodicContainerHtml->innertext = $imgCathodicContainerStr;
//                                        $innerCathodicContainerHtml->outertext = "";
                                    }
                                }
                            }

                        }

                    }
                }
            }

            if (count($systemLocationGraphReplacement['sampling']) && !empty($systemLocationGraphReplacement['sampling'])) {

                foreach ($systemLocationGraphReplacement['sampling'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerSamplingContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgSamplingContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerSamplingContainerHtml) {

                                            $innerSamplingContainerHtml->innertext = $imgSamplingContainerStr;
//                                            $innerSamplingContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['chemicaltypeone']) && !empty($systemLocationGraphReplacement['chemicaltypeone'])) {

                foreach ($systemLocationGraphReplacement['chemicaltypeone'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerChemicaltypeoneContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgChemicaltypeoneContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerChemicaltypeoneContainerHtml) {

                                            $innerChemicaltypeoneContainerHtml->innertext = $imgChemicaltypeoneContainerStr;
//                                            $innerChemicaltypeoneContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['corrosion']) && !empty($systemLocationGraphReplacement['corrosion'])) {

                foreach ($systemLocationGraphReplacement['corrosion'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerCorrosionContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgCorrosionContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerCorrosionContainerHtml) {

                                            $innerCorrosionContainerHtml->innertext = $imgCorrosionContainerStr;
//                                            $innerCorrosionContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['chemicaltypetwoconcentration']) && !empty($systemLocationGraphReplacement['chemicaltypetwoconcentration'])) {

                foreach ($systemLocationGraphReplacement['chemicaltypetwoconcentration'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerChemicalConcentrationContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgChemicalConcentrationContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerChemicalConcentrationContainerHtml) {

                                            $innerChemicalConcentrationContainerHtml->innertext = $imgChemicalConcentrationContainerStr;
//                                            $innerChemicalConcentrationContainerHtml->outertext = "";

                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['chemicaltypetwoinjection']) && !empty($systemLocationGraphReplacement['chemicaltypetwoinjection'])) {

                foreach ($systemLocationGraphReplacement['chemicaltypetwoinjection'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerChemicalInjectionContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgChemicalInjectionContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerChemicalInjectionContainerHtml) {

                                            $innerChemicalInjectionContainerHtml->innertext = $imgChemicalInjectionContainerStr;
//                                            $innerChemicalInjectionContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['chemicaltypetwoavailability']) && !empty($systemLocationGraphReplacement['chemicaltypetwoavailability'])) {

                foreach ($systemLocationGraphReplacement['chemicaltypetwoavailability'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerChemicalAvailabilityContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgChemicalAvailabilityContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerChemicalAvailabilityContainerHtml) {

                                            $innerChemicalAvailabilityContainerHtml->innertext = $imgChemicalAvailabilityContainerStr;
//                                            $innerChemicalAvailabilityContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }

            if (count($systemLocationGraphReplacement['chemicaltypetwoaverage']) && !empty($systemLocationGraphReplacement['chemicaltypetwoaverage'])) {

                foreach ($systemLocationGraphReplacement['chemicaltypetwoaverage'] as $systemIndex => $locationGraph) {

                    if ($locationGraph) {

                        foreach ($locationGraph as $innerdata) {

                            if ($innerdata) {

                                foreach ($innerdata as $innerindex => $dataImg) {
                                    if ($dataImg && $innerindex) {

                                        $innerChemicalAverageContainerHtml = $parser->find("#" . $innerindex, 0);

                                        $imgChemicalAverageContainerStr = self::parseImageHtml($dataImg,$baseUrl);

                                        if ($innerChemicalAverageContainerHtml) {

                                            $innerChemicalAverageContainerHtml->innertext = $imgChemicalAverageContainerStr;
//                                            $innerChemicalAverageContainerHtml->outertext = "";
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }
        }

        if (!empty($chartMultiSeriesPath))
        {
            $chartMultiSeriesFullPath = $this->get('kernel')->getRootDir() . '/../web'.$chartMultiSeriesPath;

            if (file_exists($chartMultiSeriesFullPath))
            {

                $innerComplianceContainerHtml = $parser->find("#containerCompliance", 0);

                if ($parser->find("#containerCompliance", 0))
                {

                    $imgComplianceContainerStr = self::parseImageHtml($chartMultiSeriesPath,$baseUrl);

                    $innerComplianceContainerHtml->innertext = $imgComplianceContainerStr;

                }
            }

        }

        //Generate IMAGES
        $knpImage = $this->get('knp_snappy.image');
        //$knpImage->getInternalGenerator()->setTimeout($this->_timeoutGeneration);

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $templateManage->addMultipleTexts($textVariables);
        $templateManage->addMultipleHTML($htmlVariables);
        $templateManage->addMultipleLists($listVariables);

        $templateManage->replaceHeaderText($headerVariables);

        $templateManage->replaceVariables();

        $logoContent = $this->readFile($projectEntity);
        //client logo image to word
        if ($projectEntity && $logoContent)
        {
            $tempLogo = tempnam(sys_get_temp_dir(), 'COMPANY_LOGO' . uniqid());
            $logoHandle = fopen($tempLogo, "w");
            fwrite($logoHandle, $logoContent);
            fclose($logoHandle);

            $imageVariables['COMPANY_LOGO'] = $tempLogo;
            $templateManage->addMultipleImages($imageVariables);
            $templateManage->replaceVariables();
        }

        //process flow diagram to word
        if ( $report_type <= 3 )
        {

            if ($subprojectEntity->getFilepath())
            {
                $logoContent = $this->readFile($subprojectEntity);
                if ( $logoContent)
                {
                    $tempLogo = tempnam(sys_get_temp_dir(), 'PFD' . uniqid());
                    $logoHandle = fopen($tempLogo, "w");
                    fwrite($logoHandle, $logoContent);
                    fclose($logoHandle);

                    $imageVariables['PFD'] = $tempLogo;
                    $templateManage->addMultipleImages($imageVariables);
                    $templateManage->replaceVariables();
                }
            }
        }

        if ($projectEntity && $logoContent)
        {
            $tempLogo = tempnam(sys_get_temp_dir(), 'COMPANY_LOGO' . uniqid());
            $logoHandle = fopen($tempLogo, "w");
            fwrite($logoHandle, $logoContent);
            fclose($logoHandle);

            $imageVariables['COMPANY_LOGO'] = $tempLogo;
            $templateManage->addMultipleImages($imageVariables);
            $templateManage->replaceVariables();
        }

        $report = $templateManage->generateDocument();

        //Sending the file to be downloaded
        $bad = array_merge(
            array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

        $name = $project['client'] . ' - ' . $subproject['name'];
        $name = str_replace($bad, "", $name);
        $fullName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $name;
        $report->createDocx($fullName);

        $fileName = $fullName . '.' . $report->getExtension();
        $file = new File($fileName);
        $filePath = $this->uploadFile($file, $entity);

        $entity->setFilepath($filePath);
        $entity->setHtmlReport($html);

        $em->persist($entity);
        $em->flush();

        if ($fileName) @unlink($fileName);

        $masterFileLocation = $this->get('kernel')->getRootDir().'/../web/uploads/cms/reports/';

        if (file_exists($masterFileLocation))
        {

            $files = glob($this->get('kernel')->getRootDir().'/../web/uploads/cms/reports'.'/'.'*.png');
            foreach($files as $file)
            {
                if(is_file($file)) unlink($file);
            }
        }
        if ($report_type == 1)
            $reportMessage = 'Corrosion Management Status Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 2)
            $reportMessage = 'Corrosion Management Supplementary Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 3)
            $reportMessage = 'Corrosion Management Comprehensive Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 4)
            $reportMessage = 'Corrosion and Flow Assurance Matrices Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 5)
            $reportMessage = 'Corrosion Compliance Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 6)
            $reportMessage = 'Flow Assurance Management Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();
        else if ($report_type == 7)
            $reportMessage = 'Flow Assurance Compliance Report No '.$entity->getId().', generated for the sub project ' . $subprojectEntity->getName();

        $this->addFlash('success', $reportMessage);
    }

    /**
     * compare red amber positive thresholds
     */
    public function compareRedAmberThresholdsYaxis($subProjectActivity, $compareValue)
    {
        $flag = 0;

        if ( $subProjectActivity->getControlamber() != "" && $subProjectActivity->getControlamber() != "N/A")
        {

            if ($subProjectActivity->getControlamberexpression() && $subProjectActivity->getControlamberrange() && $subProjectActivity->getControlamberexpressionrange())
            {
                if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == ">="){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue >= $subProjectActivity->getControlamberrange()){
                        $flag = 31;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "="){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue == $subProjectActivity->getControlamberrange()){
                        $flag = 32;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = 33;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<="){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 34;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == ">"){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue > $subProjectActivity->getControlamberrange()){
                        $flag = 35;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = 36;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue == $subProjectActivity->getControlamberrange()){
                        $flag = 37;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 38;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue > $subProjectActivity->getControlamberrange()){
                        $flag = 39;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue >= $subProjectActivity->getControlamberrange()){
                        $flag = 40;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue == $subProjectActivity->getControlamber() && $compareValue > $subProjectActivity->getControlamberrange()){
                        $flag = 41;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue == $subProjectActivity->getControlamber() && $compareValue >= $subProjectActivity->getControlamberrange()){
                        $flag = 42;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue == $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = 43;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue == $subProjectActivity->getControlamber() && $compareValue == $subProjectActivity->getControlamberrange()){
                        $flag = 44;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue == $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 45;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue < $subProjectActivity->getControlamber() && $compareValue > $subProjectActivity->getControlamberrange()){
                        $flag = 46;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue < $subProjectActivity->getControlamber() && $compareValue >= $subProjectActivity->getControlamberrange()){
                        $flag = 47;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue < $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = 48;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue < $subProjectActivity->getControlamber() && $compareValue == $subProjectActivity->getControlamberrange()){
                        $flag = 49;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue < $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 50;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue <= $subProjectActivity->getControlamber() && $compareValue > $subProjectActivity->getControlamberrange()){
                        $flag = 51;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue <= $subProjectActivity->getControlamber() && $compareValue >= $subProjectActivity->getControlamberrange()){
                        $flag = 52;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 53;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue <= $subProjectActivity->getControlamber() && $compareValue == $subProjectActivity->getControlamberrange()){
                        $flag = 54;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = 55;
                    }
                }

            }
            else if ($subProjectActivity->getControlamberexpression() && !$subProjectActivity->getControlamberrange() && !$subProjectActivity->getControlamberexpressionrange())
            {

                if ($subProjectActivity->getControlamberexpression() == ">") {
                    if ($compareValue > $subProjectActivity->getControlamber()) {
                        $flag = 56;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=") {
                    if ($compareValue >= $subProjectActivity->getControlamber()) {
                        $flag = 57;
                    }
                }
                if ($subProjectActivity->getControlamberexpression() == "<") {
                    if ($compareValue < $subProjectActivity->getControlamber()) {
                        $flag = 58;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=") {
                    if ($compareValue <= $subProjectActivity->getControlamber()) {
                        $flag = 59;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=") {
                    if ($compareValue == $subProjectActivity->getControlamber()) {
                        $flag = 60;
                    }
                }

            }
            else if (!$subProjectActivity->getControlamberexpression() && !$subProjectActivity->getControlamberrange() && !$subProjectActivity->getControlamberexpressionrange() && $subProjectActivity->getControlamber()){
                if ($compareValue <= $subProjectActivity->getControlamber()) {
                    $flag = 31;
                }
            }

        }

        if ( $subProjectActivity->getControlred() != "" && $subProjectActivity->getControlred() != "N/A" )
        {

            if ($subProjectActivity->getControlredexpression() && $subProjectActivity->getControlredrange() != "" && $subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == ">="){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue >= $subProjectActivity->getControlredrange()){
                        $flag = 1;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "="){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue == $subProjectActivity->getControlredrange()){
                        $flag = 2;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<"){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = 3;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<="){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 4;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == ">"){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue > $subProjectActivity->getControlredrange()){
                        $flag = 5;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = 6;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue == $subProjectActivity->getControlredrange()){
                        $flag = 7;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 8;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue > $subProjectActivity->getControlredrange()){
                        $flag = 9;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue >= $subProjectActivity->getControlredrange()){
                        $flag = 10;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue == $subProjectActivity->getControlred() && $compareValue > $subProjectActivity->getControlredrange()){
                        $flag = 11;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue == $subProjectActivity->getControlred() && $compareValue >= $subProjectActivity->getControlredrange()){
                        $flag = 12;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue == $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = 13;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue == $subProjectActivity->getControlred() && $compareValue == $subProjectActivity->getControlredrange()){
                        $flag = 14;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue == $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 15;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue < $subProjectActivity->getControlred() && $compareValue > $subProjectActivity->getControlredrange()){
                        $flag = 16;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue < $subProjectActivity->getControlred() && $compareValue >= $subProjectActivity->getControlredrange()){
                        $flag = 17;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue < $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = 18;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue < $subProjectActivity->getControlred() && $compareValue == $subProjectActivity->getControlredrange()){
                        $flag = 19;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue < $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 20;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue <= $subProjectActivity->getControlred() && $compareValue > $subProjectActivity->getControlredrange()){
                        $flag = 21;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue <= $subProjectActivity->getControlred() && $compareValue >= $subProjectActivity->getControlredrange()){
                        $flag = 22;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 23;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue <= $subProjectActivity->getControlred() && $compareValue == $subProjectActivity->getControlredrange()){
                        $flag = 24;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = 25;
                    }
                }


            }
            else if ($subProjectActivity->getControlredexpression() && !$subProjectActivity->getControlredrange() && !$subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">") {
                    if ($compareValue > $subProjectActivity->getControlred()) {
                        $flag = 26;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=") {
                    if ($compareValue >= $subProjectActivity->getControlred()) {
                        $flag = 27;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<") {
                    if ($compareValue < $subProjectActivity->getControlred()) {
                        $flag = 28;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=") {
                    if ($compareValue <= $subProjectActivity->getControlred()) {
                        $flag = 29;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=") {
                    if ($compareValue == $subProjectActivity->getControlred()) {
                        $flag = 30;
                    }
                }

            }
            else if (!$subProjectActivity->getControlredexpression() && !$subProjectActivity->getControlredrange() && !$subProjectActivity->getControlredexpressionrange() && $subProjectActivity->getControlred()){
                if ($compareValue <= $subProjectActivity->getControlred()) {
                    $flag = 31;
                }
            }
            //if ($flag > 0) echo "normal reg flag::".$flag."||Act".$subProjectActivity->getId()."||c".$compareValue."||r".$subProjectActivity->getControlred()."||".$subProjectActivity->getControlredexpression()."||".$subProjectActivity->getControlredrange()."||".$subProjectActivity->getControlredexpressionrange()."<br>";

            //if ($flag > 0) echo "NORMAL AMBER FLAG::".$flag."||Act".$subProjectActivity->getId()."||c".$compareValue."||a".$subProjectActivity->getControlamber()."||".$subProjectActivity->getControlamberexpression()."||".$subProjectActivity->getControlamberrange()."||".$subProjectActivity->getControlamberexpressionrange()."<br>";
        }

        return $flag;
    }

    /**
     * compare red amber negative thresholds
     */
    public function compareRedAmberMinusThresholdsYaxis($subProjectActivity, $compareValue)
    {
        $flag = 0;

        if ( $subProjectActivity->getControlamber() != "" && $subProjectActivity->getControlamber() != "N/A")
        {

            if ($subProjectActivity->getControlamberexpression() && $subProjectActivity->getControlamberrange() && $subProjectActivity->getControlamberexpressionrange())
            {
                if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == ">="){
                    if ($compareValue  > $subProjectActivity->getControlamberrange() && $compareValue >= $subProjectActivity->getControlamber()){
                        $flag = 31;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "="){
                    if ($compareValue  > $subProjectActivity->getControlamberrange() && $compareValue == $subProjectActivity->getControlamber()){
                        $flag = 32;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue  > $subProjectActivity->getControlamberrange() && $compareValue < $subProjectActivity->getControlamber()){
                        $flag = 33;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<="){
                    if ($compareValue  > $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 34;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == ">"){
                    if ($compareValue  > $subProjectActivity->getControlamberrange() && $compareValue > $subProjectActivity->getControlamber()){
                        $flag = 35;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue  >= $subProjectActivity->getControlamberrange() && $compareValue < $subProjectActivity->getControlamber()){
                        $flag = 36;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue  >= $subProjectActivity->getControlamberrange() && $compareValue == $subProjectActivity->getControlamber()){
                        $flag = 37;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue  >= $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 38;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue  >= $subProjectActivity->getControlamberrange() && $compareValue > $subProjectActivity->getControlamber()){
                        $flag = 39;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue  >= $subProjectActivity->getControlamberrange() && $compareValue >= $subProjectActivity->getControlamber()){
                        $flag = 40;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue == $subProjectActivity->getControlamberrange() && $compareValue > $subProjectActivity->getControlamber()){
                        $flag = 41;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue == $subProjectActivity->getControlamberrange() && $compareValue >= $subProjectActivity->getControlamber()){
                        $flag = 42;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue == $subProjectActivity->getControlamberrange() && $compareValue < $subProjectActivity->getControlamber()){
                        $flag = 43;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue == $subProjectActivity->getControlamberrange() && $compareValue == $subProjectActivity->getControlamber()){
                        $flag = 44;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue == $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 45;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue < $subProjectActivity->getControlamberrange() && $compareValue > $subProjectActivity->getControlamber()){
                        $flag = 46;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue < $subProjectActivity->getControlamberrange() && $compareValue >= $subProjectActivity->getControlamber()){
                        $flag = 47;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue < $subProjectActivity->getControlamberrange() && $compareValue < $subProjectActivity->getControlamber()){
                        $flag = 48;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue < $subProjectActivity->getControlamberrange() && $compareValue == $subProjectActivity->getControlamber()){
                        $flag = 49;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue < $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 50;
                    }
                }


                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == ">"){

                    if ($compareValue <= $subProjectActivity->getControlamberrange() && $compareValue > $subProjectActivity->getControlamber()){
                        $flag = 51;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == ">="){

                    if ($compareValue <= $subProjectActivity->getControlamberrange() && $compareValue >= $subProjectActivity->getControlamber()){
                        $flag = 52;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 53;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "="){

                    if ($compareValue <= $subProjectActivity->getControlamberrange() && $compareValue == $subProjectActivity->getControlamber()){
                        $flag = 54;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlamberrange() && $compareValue <= $subProjectActivity->getControlamber()){
                        $flag = 55;
                    }
                }

            }
            else if ($subProjectActivity->getControlamberexpression() && !$subProjectActivity->getControlamberrange() && !$subProjectActivity->getControlamberexpressionrange())
            {

                if ($subProjectActivity->getControlamberexpression() == ">") {
                    if ($compareValue > $subProjectActivity->getControlamber()) {
                        $flag = 56;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=") {
                    if ($compareValue >= $subProjectActivity->getControlamber()) {
                        $flag = 57;
                    }
                }
                if ($subProjectActivity->getControlamberexpression() == "<") {
                    if ($compareValue < $subProjectActivity->getControlamber()) {
                        $flag = 58;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=") {
                    if ($compareValue <= $subProjectActivity->getControlamber()) {
                        $flag = 59;
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "=") {
                    if ($compareValue == $subProjectActivity->getControlamber()) {
                        $flag = 60;
                    }
                }

            }
            else if (!$subProjectActivity->getControlamberexpression() && !$subProjectActivity->getControlamberrange() && !$subProjectActivity->getControlamberexpressionrange() && $subProjectActivity->getControlamber()){
                if ($compareValue >= $subProjectActivity->getControlamber()) {
                    $flag = 31;
                }
            }

        }

        if ( $subProjectActivity->getControlred() != "" && $subProjectActivity->getControlred() != "N/A" )
        {

            if ($subProjectActivity->getControlredexpression() && $subProjectActivity->getControlredrange() != "" && $subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == ">="){
                    if ($compareValue > $subProjectActivity->getControlredrange() && $compareValue >= $subProjectActivity->getControlred()){
                        $flag = 1;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "="){
                    if ($compareValue > $subProjectActivity->getControlredrange() && $compareValue == $subProjectActivity->getControlred()){
                        $flag = 2;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<"){
                    if ($compareValue > $subProjectActivity->getControlredrange() && $compareValue < $subProjectActivity->getControlred()){
                        $flag = 3;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<="){
                    if ($compareValue > $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 4;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == ">"){
                    if ($compareValue > $subProjectActivity->getControlredrange() && $compareValue > $subProjectActivity->getControlred()){
                        $flag = 5;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue >= $subProjectActivity->getControlredrange() && $compareValue < $subProjectActivity->getControlred()){
                        $flag = 6;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue >= $subProjectActivity->getControlredrange() && $compareValue == $subProjectActivity->getControlred()){
                        $flag = 7;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue >= $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 8;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue >= $subProjectActivity->getControlredrange() && $compareValue > $subProjectActivity->getControlred()){
                        $flag = 9;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue >= $subProjectActivity->getControlredrange() && $compareValue >= $subProjectActivity->getControlred()){
                        $flag = 10;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue == $subProjectActivity->getControlredrange() && $compareValue > $subProjectActivity->getControlred()){
                        $flag = 11;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue == $subProjectActivity->getControlredrange() && $compareValue >= $subProjectActivity->getControlred()){
                        $flag = 12;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue == $subProjectActivity->getControlredrange() && $compareValue < $subProjectActivity->getControlred()){
                        $flag = 13;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue == $subProjectActivity->getControlredrange() && $compareValue == $subProjectActivity->getControlred()){
                        $flag = 14;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue == $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 15;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue < $subProjectActivity->getControlredrange() && $compareValue > $subProjectActivity->getControlred()){
                        $flag = 16;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue < $subProjectActivity->getControlredrange() && $compareValue >= $subProjectActivity->getControlred()){
                        $flag = 17;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue < $subProjectActivity->getControlredrange() && $compareValue < $subProjectActivity->getControlred()){
                        $flag = 18;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue < $subProjectActivity->getControlredrange() && $compareValue == $subProjectActivity->getControlred()){
                        $flag = 19;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue < $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 20;
                    }
                }


                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == ">"){

                    if ($compareValue <= $subProjectActivity->getControlredrange() && $compareValue > $subProjectActivity->getControlred()){
                        $flag = 21;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == ">="){

                    if ($compareValue <= $subProjectActivity->getControlredrange() && $compareValue >= $subProjectActivity->getControlred()){
                        $flag = 22;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 23;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "="){

                    if ($compareValue <= $subProjectActivity->getControlredrange() && $compareValue == $subProjectActivity->getControlred()){
                        $flag = 24;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue <= $subProjectActivity->getControlredrange() && $compareValue <= $subProjectActivity->getControlred()){
                        $flag = 25;
                    }
                }


            }
            else if ($subProjectActivity->getControlredexpression() && !$subProjectActivity->getControlredrange() && !$subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">")
                {

                    if ($compareValue > $subProjectActivity->getControlred())
                    {
                        $flag = 26;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=")
                {
                    if ($compareValue >= $subProjectActivity->getControlred())
                    {
                        $flag = 27;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<")
                {
                    if ($compareValue < $subProjectActivity->getControlred())
                    {
                        $flag = 28;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=")
                {
                    if ($compareValue <= $subProjectActivity->getControlred())
                    {
                        $flag = 29;
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "=")
                {
                    if ($compareValue == $subProjectActivity->getControlred())
                    {
                        $flag = 30;
                    }
                }

            }
            else if (!$subProjectActivity->getControlredexpression() && !$subProjectActivity->getControlredrange() && !$subProjectActivity->getControlredexpressionrange() && $subProjectActivity->getControlred()){
                if ($compareValue >= $subProjectActivity->getControlred()) {
                    $flag = 31;
                }
            }
        }

        //echo "flag".$flag."id:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";

        return $flag;
    }

    /**
     * compare red amber chemical db in activites thresholds
     */
    public function compareChemicalRedAmberThresholdsYaxis($act, $compareValue)
    {
        $flag = 0;

        if ($act['bsd_controlred'] != "" && $act['bsd_controlred'] != "N/A" && $act['bsd_controlamber'] != "" && $act['bsd_controlamber'] != "N/A")
        {

            if ($act['bsd_controlredexpression'] && $act['bsd_controlredrange'] && $act['bsd_controlredexpressionrange'])
            {
                if ($act['bsd_controlredexpression'] == ">=" && $act['bsd_controlredexpressionrange'] == "<="){

                    if ($compareValue  >= $act['bsd_controlred'] && $compareValue <= $act['bsd_controlredrange']){
                        $flag = 17;
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">" && $act['bsd_controlredexpressionrange'] == "<"){
                    if ($compareValue  > $act['bsd_controlred'] && $compareValue < $act['bsd_controlredrange']){
                        $flag = 18;
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">=" && $act['bsd_controlredexpressionrange'] == "<"){

                    if ($compareValue  >= $act['bsd_controlred'] && $compareValue < $act['bsd_controlredrange']){
                        $flag = 19;
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">" && $act['bsd_controlredexpressionrange'] == "<="){
                    if ($compareValue  > $act['bsd_controlred'] && $compareValue <= $act['bsd_controlredrange']){
                        $flag = 20;
                    }
                }

            }
            else if ($act['bsd_controlredexpression'] && !$act['bsd_controlredrange'] && !$act['bsd_controlredexpressionrange'])
            {

                if ($act['bsd_controlredexpression'] == ">") {
                    if ($compareValue > $act['bsd_controlred']) {
                        $flag = 21;
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">=") {
                    if ($compareValue >= $act['bsd_controlred']) {
                        $flag = 22;
                    }
                }
                else if ($act['bsd_controlredexpression'] == "<") {
                    if ($compareValue < $act['bsd_controlred']) {
                        $flag = 23;
                    }
                }
                else if ($act['bsd_controlredexpression'] == "<=") {
                    if ($compareValue <= $act['bsd_controlred']) {
                        $flag = 24;
                    }
                }

            }

            //if ($flag > 0) echo "Chemical amber Flag::".$flag."||".$act['bsd_controlred']."||".$act['bsd_controlredexpression']."||".$act['bsd_controlredrange']."||".$act['bsd_controlredexpression']."<br>";

            if ($act['bsd_controlamberexpression'] && $act['bsd_controlamberrange'] && $act['bsd_controlamberexpressionrange'])
            {
                if ($act['bsd_controlamberexpression'] == ">=" && $act['bsd_controlamberexpressionrange'] == "<="){

                    if ($compareValue  >= $act['bsd_controlamber'] && $compareValue <= $act['bsd_controlamberrange']){
                        $flag = 25;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">" && $act['bsd_controlamberexpressionrange'] == "<"){
                    if ($compareValue  > $act['bsd_controlamber'] && $compareValue < $act['bsd_controlamberrange']){
                        $flag = 26;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">=" && $act['bsd_controlamberexpressionrange'] == "<"){

                    if ($compareValue  >= $act['bsd_controlamber'] && $compareValue < $act['bsd_controlamberrange']){
                        $flag = 27;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">" && $act['bsd_controlamberexpressionrange'] == "<="){
                    if ($compareValue  > $act['bsd_controlamber'] && $compareValue <= $act['bsd_controlamberrange']){
                        $flag = 28;
                    }
                }

            }
            else if ($act['bsd_controlamberexpression'] && !$act['bsd_controlamberrange'] && !$act['bsd_controlamberexpressionrange'])
            {

                if ($act['bsd_controlamberexpression'] == ">") {
                    if ($compareValue > $act['bsd_controlamber']) {
                        $flag = 29;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">=") {
                    if ($compareValue >= $act['bsd_controlamber']) {
                        $flag = 30;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == "<") {
                    if ($compareValue < $act['bsd_controlamber']) {
                        $flag = 31;
                    }
                }
                else if ($act['bsd_controlamberexpression'] == "<=") {
                    if ($compareValue <= $act['bsd_controlamber']) {
                        $flag = 32;
                    }
                }

            }

        }
        //if ($flag > 0) echo "Chemical amber Flag::".$flag."||".$act['bsd_controlamber']."||".$act['bsd_controlamberexpression']."||".$act['bsd_controlamberrange']."||".$act['bsd_controlamberexpression']."<br>";

        return $flag;
    }

    /**
     * filters graph for exception, comprehensive, supplementary reports to the system activity and location graphs
     */
    public function filterGraphArray($id, $systemId, $activityGraphArray, $dbtype, $graphType, $activityName, $thresholdMin=null, $thresholdMax=null, $thresholdGraphArray=null)
    {

        if (!empty($activityGraphArray))
        {

            $corrosionFiltered = array_filter($activityGraphArray);

            if (!empty($corrosionFiltered))
            {
                foreach($corrosionFiltered as $index => $corrosion) {

                    if ($dbtype != "production")
                    {
                        if (!empty($corrosion[0]) && $corrosion[0] != "[]" && $corrosion[0] != "[]|[]" && $corrosion[0] != "[]|[]|[]|[]|[]") {

                            if ($dbtype == "chemical") {

                                if ($graphType == "ChemicalTypeTwoConcentrationGraphs") {

                                    $strExplode = explode("|", $corrosion[0]);

                                    if ($strExplode[2]) $fromDate = $strExplode[2]; else $fromDate = null;
                                    if ($strExplode[3]) $toDate = $strExplode[3]; else $toDate = null;

                                    $graphMultiData = array('Actual Corrosion Inhibitor Concentration, ppm' => $strExplode[0], 'Target Corrosion Inhibitor Concentration, ppm' => $strExplode[1]);

                                    $chartMultiSeriesPath = $this->generateSeriesGraphData($id, $graphMultiData, '', $activityName, "Corrosion Inhibitor Concentration", null, null, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                    $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartMultiSeriesPath;
                                } else if ($graphType == "ChemicalTypeTwoAverageGraphs") {

                                    $strExplode = explode("|", $corrosion[0]);

                                    if ($strExplode[2]) $fromDate = $strExplode[2]; else $fromDate = null;
                                    if ($strExplode[3]) $toDate = $strExplode[3]; else $toDate = null;

                                    $graphMultiData = array('Volume Injected Against Volume Required, %' => $strExplode[0], 'Percentage Time Chemical Target Reached, %' => $strExplode[1]);

                                    $chartMultiSeriesPath = $this->generateSeriesGraphData($id, $graphMultiData, $activityName, $activityName, "Corrosion Inhibitor Average", $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                    $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartMultiSeriesPath;
                                } else if ($graphType == "ChemicalTypeTwoAvailabilityGraphs") {

                                    $strExplode = explode("|", $corrosion[0]);

                                    if ($strExplode[1]) $fromDate = $strExplode[1]; else $fromDate = null;
                                    if ($strExplode[2]) $toDate = $strExplode[2]; else $toDate = null;

                                    $chartPath = $this->generateSeriesGraphData($id, $strExplode[0], $activityName, $activityName, $yaxisString = "Corrosion Inhibitor Availability, %", $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                    $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                                } else if ($graphType == "ChemicalTypeTwoInjectionGraphs") {

                                    $strExplode = explode("|", $corrosion[0]);

                                    if ($strExplode[1]) $fromDate = $strExplode[1]; else $fromDate = null;
                                    if ($strExplode[2]) $toDate = $strExplode[2]; else $toDate = null;

                                    $chartPath = $this->generateSeriesGraphData($id, $strExplode[0], '', $activityName, $yaxisString = "CI Injection, L/day ", null, null, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                    $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                                } else if ($graphType == "ChemicalTypeOneGraphs") {

                                    $strExplode = explode("|", $corrosion[0]);

                                    if ($strExplode[2]) $fromDate = $strExplode[2]; else $fromDate = null;
                                    if ($strExplode[3]) $toDate = $strExplode[3]; else $toDate = null;

                                    $graphMultiData = array('% Achievement' => $strExplode[0], 'Target' => $strExplode[1], 'Actual' => $strExplode[2]);

                                    $chartPath = $this->generateSeriesGraphData($id, $graphMultiData, '', $activityName, $activityName, $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null, '% Achievement');

                                    $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                                }
                            }

                            if ($dbtype == "cathodic") {

                                $strExplode = explode("|", $corrosion[0]);

                                if ($strExplode[1]) $fromDate = $strExplode[1]; else $fromDate = null;
                                if ($strExplode[2]) $toDate = $strExplode[2]; else $toDate = null;

                                $chartPath = $this->generateSeriesGraphData($id, $strExplode[0], '', $activityName, $activityName, $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                            }

                            if ($dbtype == "cathodiconoff") {

                                $strExplode = explode("|", $corrosion[0]);

                                if ($strExplode[2]) $fromDate = $strExplode[2]; else $fromDate = null;
                                if ($strExplode[3]) $toDate = $strExplode[3]; else $toDate = null;

                                $graphMultiData = array('ON Potential' => $strExplode[0], 'OFF Potential' => $strExplode[1]);

                                $chartMultiSeriesPath = $this->generateSeriesGraphData($id, $graphMultiData, '', $activityName, $activityName, $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartMultiSeriesPath;
                            }

                            if ($dbtype == "sampling") {

                                $strExplode = explode("|", $corrosion[0]);

                                if ($strExplode[1]) $fromDate = $strExplode[1]; else $fromDate = null;
                                if ($strExplode[2]) $toDate = $strExplode[2]; else $toDate = null;

                                $chartPath = $this->generateSeriesGraphData($id, $strExplode[0], '', $activityName, $activityName, $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                            }

                            if ($dbtype == "corrosion") {

                                $strExplode = explode("|", $corrosion[0]);

                                if ($strExplode[1]) $fromDate = $strExplode[1]; else $fromDate = null;
                                if ($strExplode[2]) $toDate = $strExplode[2]; else $toDate = null;

                                $chartPath = $this->generateSeriesGraphData($id, $strExplode[0], '', $activityName, $activityName, $thresholdMin, $thresholdMax, $fromDate, $toDate, $thresholdGraphArray[$index], null);

                                $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartPath;
                            }

                        }
                    }
                    else
                    {
                        if ($corrosion != "[]|[]|[]|[]|[]|[]|[]")
                        {

                            $strExplode = explode("|", $corrosion);

                            $graphData = $strExplode[0];

                            if ($graphData != "[]") {

                                $chartOnePath = $this->generateSeriesGraphData($id, $graphData, $titleString = 'Gas Production', $xaxisString = "Gas Production", $yaxisString = "Gas Production, mmscf/day", $thresholdMin = null, $thresholdMax = null, $fromDate = null, $toDate = null, null, null);

                                $graphArray['container' . $graphType . $systemId . '_' . $index] = $chartOnePath;
                            }

                            if ($strExplode[1] || $strExplode[2] || $strExplode[3] || $strExplode[4]) {

                                $graphMultiData = array('Water, bbl/d' => $strExplode[1], 'Oil, bbl/d' => $strExplode[3], 'Condensate, bbl/d' => $strExplode[2], 'Total Liquid Production, bbl/d' => $strExplode[4]);

                                $chartMultiSeriesPath = $this->generateSeriesGraphData($id, $graphMultiData, $titleString = 'Total Liquid Production', $xaxisString = "Total Liquid Production", $yaxisString = "Production, bbl per day", $thresholdMin = null, $thresholdMax = null, $fromDate = null, $toDate = null, null, null);

                                $graphArray['containerOne' . $graphType . $systemId . '_' . $index] = $chartMultiSeriesPath;
                            }

                            if ($strExplode[5] && $strExplode[6]) {

                                $graphMultiData = array('H2S( mol %)' => $strExplode[5], 'CO2( mol %)' => $strExplode[6]);

                                $chartMultiSeriesPath = $this->generateSeriesGraphData($id, $graphMultiData, $titleString = 'Acid Gas Content', $xaxisString = "Acid Gas Content", $yaxisString = "Mol%", $thresholdMin = null, $thresholdMax = null, $fromDate = null, $toDate = null, null, null);

                                $graphArray['containerTwo' . $graphType . $systemId . '_' . $index] = $chartMultiSeriesPath;
                            }
                        }

                    }

                }

            }

        }

        return $graphArray;

    }

    /**
     * filters production graph
     */
    public function filterProductionGraphArray($id, $activityGraphArray)
    {

        $productionGraphArray = [];

        if (!empty($activityGraphArray))
        {
            $corrosionFiltered = array_filter($activityGraphArray);

            if (!empty($corrosionFiltered))
            {

                $location = str_replace("/app","",$this->get('kernel')->getRootDir());

                $location.= '/web/assets/img/cms';
                $readlocation = '/assets/img/cms/';

                foreach($corrosionFiltered as $index => $corrosion)
                {

                    if (!empty($corrosion != "[]|[]|[]|[]|[]"))
                    {
                        $strExplode = explode("|", $corrosion);

                        $graphData = $strExplode[0];

                        if ($graphData != "[]") {

                            $chartOnePath = $this->generateSeriesGraphData($id, $graphData, $titleString = 'Gas Production', $xaxisString = "Gas Production", $yaxisString = "Gas Production, mmscf/day", $thresholdMin = null, $thresholdMax = null, $fromDate = null, $toDate = null, null, null);

                            $productionGraphArray['container' . $index] = $chartOnePath;
                        }

                        $graphMultiData= array('Water, bbl/d'=>$strExplode[1],'Oil, bbl/d'=>$strExplode[3],'Condensate, bbl/d'=>$strExplode[2],'Total Liquid Production, bbl/d'=>$strExplode[4]);

                        $chartMultiSeriesPath = $this->generateSeriesGraphData( $id, $graphMultiData, $titleString='Total Liquid Production', $xaxisString="Total Liquid Production", $yaxisString="Production, bbl per day", $thresholdMin=null,$thresholdMax=null,$fromDate=null,$toDate=null,null, null);

                        $productionGraphArray['containerOne'.$index] = $chartMultiSeriesPath;

                    }

                }


            }

        }

        return $productionGraphArray;
    }

    /**
     * generates the sinle or multi series series graph generic function with options
     */
    public function generateSeriesGraphData( $id, $seriesData, $titleString=null, $xaxisString=null, $yaxisString=null, $thresholdMin=null, $thresholdMax=null, $fromDate=null, $toDate=null, $thresholdGraphArray=null, $bands= null, $yseries=null, $background =null )
    {

        $minMax = "";

        $seriesRunData = "";

        $seriesThresholdRedData = "";
        $seriesThresholdGreenData = "";
        $seriesThresholdAmberData = "";

        $fromYear = "";
        $fromMonth = "";
        $fromDay = "";

        $toYear = "";
        $toMonth = "";
        $toDay = "";

        if ($fromDate && $toDate)
        {
            $maxY = 365;
            $date1 = date_create($fromDate);
            $date2 = date_create($toDate);

            $fromSplitDate = explode("-", $fromDate);

            $fromYear = $fromSplitDate[0];
            $fromMonth = $fromSplitDate[1];
            $fromDay = $fromSplitDate[2];

            $toSplitDate = explode("-", $toDate);

            $toYear = $toSplitDate[0];
            $toMonth = $toSplitDate[1];
            $toDay = $toSplitDate[2];

            if ($fromYear && $fromMonth && $fromDay && $fromYear && $toYear && $toMonth && $toDay)
            {
                $minMax = "'min': Date.UTC('$fromYear', '$fromMonth', '$fromDay', 0, 0, 0), 'max': Date.UTC('$toYear', '$toMonth', '$toDay', 0, 0, 0),";
            }
            else
            {
                $minMax = "'minTickInterval': 28 * 24 * 3600 * 1000,";
            }

        }

        if ($thresholdMin && $thresholdMax && $thresholdMin != "N/A" && $thresholdMax != "N/A")
        {
            $thresholdPlot = "";
        }

        if (is_array($seriesData) && !empty($seriesData))
        {
            foreach($seriesData as $index =>$data)
            {
                if ($data != "[]")
                {
                    if ($index == 0 && $yseries)
                    {
                        $seriesRunData .= '{
                        "name": "' . addslashes($index) . '",
                        "data": ' . $data . ',
                        "yAxis": 1,
                        "marker": {
                                    "enabled": true,
                                    "radius": 4
                                },
                        },';
                    }
                    else {
                        $seriesRunData .= '{
                        "name": "' . addslashes($index) . '",
                        "data": ' . $data . ',
                        "marker": {
                                    "enabled": true,
                                    "radius": 4
                                },
                        },';
                    }
                }
            }
        }
        else
        {
            $seriesRunData = '{ 
                    "name": "'.addslashes($yaxisString).'",
                    "data": '.$seriesData.',
                    "marker": {
                                "enabled": true,
                                "radius": 4
                            },
                }';
        }

        if (is_array($thresholdGraphArray) && !empty($thresholdGraphArray))
        {

            foreach($thresholdGraphArray as $index =>$data) {

                $thresholdGraphData = explode("|", $data);

                if ($thresholdGraphData[0] && $thresholdGraphData[0] != "[]")
                {
                    $seriesThresholdRedData = ',{
                    "name": "Red Threshold",
                    "data": ' . $thresholdGraphData[0] . ',
                    "color": "red",
                    "marker": {
                                "enabled": false,
                            },
                    "zones": [ {
                            "dashStyle": "shortdash"
                        }],
                    },';
                }

                if ($thresholdGraphData[1] && $thresholdGraphData[1] != "[]")
                {
                    $seriesThresholdGreenData = '{
                    "name": "Green Threshold",
                    "data": ' . $thresholdGraphData[1] . ',
                    "color": "green",
                    "marker": {
                                "enabled": false,
                            },
                    "zones": [ {
                            "dashStyle": "shortdash"
                        }],
                    },';
                }

                if ($thresholdGraphData[2] && $thresholdGraphData[2] != "[]")
                {
                    $seriesThresholdAmberData = '{
                    "name": "Amber Threshold",
                    "data": ' . $thresholdGraphData[2] . ',
                    "color": "#FFC200",
                    "marker": {
                                "enabled": false,
                            },
                    "zones": [ {
                            "dashStyle": "shortdash"
                        }],
                    },';
                }
            }

        }

        $seriesThresholdRedData = str_replace(',,', ',', $seriesThresholdRedData);

        $gridline = "";
        $yMaxMin = "";
        $linLog = '';
        if ($bands)
        {
            $plotBands = ',
			"plotBands": [],';
            $gridlineWidth = '"gridLineWidth": 0,
                        "minorGridLineWidth": 0,';
            $yMaxMin = '"min":0,"max": 100,';

        }

        if ($titleString == 'Gas Production' || $titleString == 'Total Liquid Production' || $titleString == 'Overall Compliance %')
        {
            $linLog = 'Linear';
            $titleString = '"title": {
                                            "text": "'.addslashes($titleString).'",
                                     },';
        }
        else
        {
            $linLog = 'Linear';
            $titleString = '"title": {
                                         "text": "",
                                     },';
        }

        if ($yseries)
        {
            $yseriesPlot = '
            ,{
                "type": "' . $linLog . '",
                    "opposite": false,
                    "labels" : {
                "useHTML": true,
                    },
                    "title": {
                "text": "' . addslashes($yseries) . '",
                        "useHTML": true,
                    },
                "opposite": true,
            }';
        }

        if ($background)
        {
            $background = '"plotBackgroundColor": {
                                "linearGradient": 
                                            {
                                                "x1": 0,
                                                "x2": 0,
                                                "y1": 0,
                                                "y2": 1
                                            },
                                "stops": 
                                            [                                                
                                                [0, "#92d050"],
                                                [0.40, "#ffc90e"],
                                                [0.75, "#ff4646"],
                                                [1, "#ff4646"],
                                            ]
                            },';
        }

        $jsonString = '{
                            "chart": {
                                "borderColor": "#d2d5d5",
                                "borderWidth": 1,
                                "type": "line",
                                '.$background.'
                            },
                            '.$titleString.'
                            "exporting": {
                                 "enabled": false
                            },
                            "xAxis": {
                                "labels" : {
                                    "useHTML": true,
                                },
                                "type": "datetime",
                                '.$minMax.'
                            },
                            "yAxis": [{
                                "type": "'.$linLog.'",
                                "opposite": false,
                                "labels" : {
                                    "useHTML": true,
                                },
                                "title": {
                                    "text": "'.addslashes($yaxisString).'",
                                    "useHTML": true,
                                }'.$thresholdPlot.$plotBands.$gridlineWidth.$yMaxMin.'
                            }'.$yseriesPlot.'],
                            "legend": {
                                "enabled": true,
                                "align": "center",
                                "verticalAlign": "top",
                                "layout": "horizontal",
                                "x": -20,
                                "useHTML": true,
                                "margin": 5,
                                "itemMarginTop": 10,
                                "itemMarginBottom": 2,
                            },
                            "series": ['.$seriesRunData.$seriesThresholdRedData.$seriesThresholdGreenData.$seriesThresholdAmberData.'],
                            "credits": {
                               "enabled": false
                            },
                    }';

        //FileUploader::upload($this->filesystem, $this->container, $path)

        $chartPath = $id."_".md5(uniqid(rand(), true)) . '.png';

        $masterFileLocation = $this->get('kernel')->getRootDir().'/../web/uploads/cms/reports/';

        $fileLocation = '/uploads/cms/reports/';

        $jsonFilename = 'graph.json';

        $jsonFilename = $masterFileLocation. $jsonFilename;

        $jsonString = str_replace(',,', ',', $jsonString);

//        echo $jsonString."<br>";

        try
        {
            file_put_contents($jsonFilename, $jsonString);

            $storeFilePath = $masterFileLocation. $chartPath;

            $highchartcomand = "highcharts-export-server --width 750 --batch '".$jsonFilename."=".$storeFilePath."'";

            if (!file_exists($storeFilePath)) {
                exec($highchartcomand);
            }

        }
        catch (Exception $e) {
            error_log(e);
        }

        $returnFilePath = $fileLocation.$chartPath;

        return $returnFilePath;
    }

    /*
     * Check the activity mitigation status fro amber red green hold
     */
    public function checkActivityMitigationControlStatus($subProjectActivity)
    {
        $status = 0;
        if ($subProjectActivity->getIsControl())
        {
            if ($subProjectActivity->getKpiControl() == "R")
            {
                $status = 1;
            }

            if ($subProjectActivity->getKpiControl() == "A")
            {
                $status = 2;
            }
        }

        return $status;
    }

    /*
    * Check the activity mitigation status fro amber red green hold
    */
    public function checkActivityMitigationManagementStatus($subProjectActivity)
    {
        $status = 0;

        if ($subProjectActivity->getIsManagement())
        {
            if ($subProjectActivity->getKpiManagement() == "R")
            {
                $status = 3;
            }

            if ($subProjectActivity->getKpiManagement() == "A")
            {
                $status = 4;
            }
        }
        return $status;
    }

    /*
     * Convert Date Format
     */
    public function convertGraphDateString($dateString)
    {

        if($dateString instanceof \DateTime)
        {
            return $dateString->format('Y-m-d');
        }
        //
    }

    /*
     * Check integer
     */
    public function check_numb($Degree)
    {
        if ( $Degree > 0 ) {
            return 'Positive';
        } elseif( $Degree < 0 ) {
            return 'Negative';
        }
    }

    /*
     * Move from break to newline
     */
    public function br2nl( $input )
    {
        $input = nl2br( $input );
        return $input;
    }

    /*
     * get month quarters
     */
    public static function getQuarterByMonth($monthNumber)
    {
        return floor(($monthNumber - 1) / 3) + 1;
    }

    /*
     * get month halfs
     */
    public static function getHalfByMonth($monthNumber)
    {
        return floor(($monthNumber - 1) / 6) + 1;
    }

    /*
     * Parse Image Path
     */
    public static function parseImageHtml($imgPath,$baseUrl)
    {

        return $img = '<img src="' . $baseUrl . $imgPath . '" style="align:middle; display:block; margin: 0 auto;" />';
    }

    /**
     * Creates a new archives entity.
     *
     * @Route("/archivesajax", name="cms_subproject_archivesajax")
     * @Method("POST")
     */
    public function ArchivesAjaxAction(Request $request, $projectId, $subprojectId)
    {

        $userId = $this->get('security.context')->getToken()->getUser()->getId();

        $this->baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        if ($request->isXmlHttpRequest() && $request->request->get('reportType') && $request->request->get('fromDate') && $request->request->get('toDate')  && $userId) {
            ini_set('memory_limit', '-1');
            //error_reporting(0);
            if ($this->container->has('profiler')) {
                $this->container->get('profiler')->disable();
            }

            $em = $this->getManager();
            $em->getConnection()->getConfiguration()->setSQLLogger(null);
            $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

            $entity = new SubProjectMatricesReports();
            $from_date = new \DateTime('now');

            $to_date = clone $from_date;
            $currMonth = $from_date->format('m');
            $currYear = $from_date->format('Y');

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            if ($subproject->getReportingperiod() == 'quarterly') {

                $quarter = self::getQuarterByMonth($currMonth);

                switch ($quarter) {
                    case 1:
                        $startmonth = 1;
                        $endmonth = 3;
                        $endday = 31;
                        break;
                    case 2:
                        $startmonth = 4;
                        $endmonth = 6;
                        $endday = 30;
                        break;
                    case 3:
                        $startmonth = 7;
                        $endmonth = 10;
                        $endday = 30;
                        break;
                    case 4:
                        $startmonth = 10;
                        $endmonth = 12;
                        $endday = 31;
                        break;
                }
            } else if ($subproject->getReportingperiod() == 'bi-annual') {

                $quarter = self::getHalfByMonth($currMonth);

                switch ($quarter) {
                    case 1:
                        $startmonth = 1;
                        $endmonth = 6;
                        $endday = 30;
                        break;
                    case 2:
                        $startmonth = 7;
                        $endmonth = 12;
                        $endday = 31;
                        break;
                }
            } else {
                $endmonth = $to_date->format('m');
                $endday = $to_date->format('d');
            }

            $to_date->setDate($currYear, $endmonth, $endday);

            $form = $this->createArchiveFilterForm($projectId, $subprojectId, $from_date, $to_date);
            $form->handleRequest($request);

            //$data = $form->getData();

            $report_type = $request->request->get('reportType');

            $from_date = new \DateTime($request->request->get('fromDate'));
            $to_date = new \DateTime($request->request->get('toDate'));

            $toYear = $to_date->format('Y');
            $currQuarter = $to_date->format('m');

            $currQuarter = self::getQuarterByMonth($currQuarter);

            $em = $this->getManager();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId, 1);

            if ($subproject && $systems)
            {
                $archivesData = array();
                $subProjectData = array();
                $documentsData = array();
                $actiontrackerData = array();
                $meetingData = array();
                $compliance = array();

                $entity->setSubproject($subproject);
                $entity->setIsApproved(false);
                $entity->setDate(new \DateTime());
                $entity->setReporttype($report_type);

                //projectData
                $project = $subproject->getProject();
                $projectData = array();
                $projectData['id'] = $project->getId();
                $projectData['name'] = $project->getName();
                $projectData['description'] = $project->getDescription();
                $projectData['referenceNo'] = $project->getReferenceNo();
                $projectData['client'] = $project->getClient();
                $projectData['location'] = $project->getLocation();
                $projectData['dateRange'] = date("M Y", strtotime($from_date->format('Y-m-d'))) . " - " . date("M Y", strtotime($to_date->format('Y-m-d')));

                $compliance['from_date'] = new \DateTime($from_date->format('Y-m-d'));
                $compliance['to_date'] = new \DateTime($to_date->format('Y-m-d'));

                //subprojectData
                $subProjectData['id'] = $subproject->getId();
                $subProjectData['name'] = $subproject->getName();
                $subProjectData['client'] = $subproject->getClient();
                $subProjectData['description'] = $subproject->getDescription();
                $subProjectData['location'] = $subproject->getLocation();
                $subProjectData['filepath'] = $subproject->getFilepath();
                $subProjectData['reportingperiod'] = $subproject->getReportingperiod();

                $compliance['control']['redcontrolkpi'] = array();
                $compliance['management']['redmanagementkpi'] = array();

                $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
                $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

                $compliance['control']['totalredkpi'] = 0;
                $compliance['control']['totalgreenkpi'] = 0;
                $compliance['control']['totalamberkpi'] = 0;

                $compliance['flowassurancecontrol']['totalredkpi'] = 0;
                $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
                $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

                $compliance['flowassurancemanagement']['totalredkpi'] = 0;
                $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
                $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

                $compliance['management']['totalredkpi'] = 0;
                $compliance['management']['totalgreenkpi'] = 0;
                $compliance['management']['totalamberkpi'] = 0;

                $compliance['control']['kpi_control_compliance'] = array();
                $compliance['management']['kpi_management_compliance'] = array();

                $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
                $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

                $compliance['overallcontrolcompliance'] = 0;
                $compliance['overallmanagementcompliance'] = 0;

                $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                $compliance['overdue'] = 0;

                $tempsystem = 999999999999999999999999999999999;

                $indeX = 0;

                foreach ($systems as $system)
                {
                    $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId(), false);

                    if ($activitiesAction[$system->getId()])
                    {

                        foreach ($activitiesAction[$system->getId()] as $subProjectActivity)
                        {
                            if ($subProjectActivity->getId())
                            {
                                $id = $subProjectActivity->getId();

                                $subProjectActivitiesData[$id] = array();

                                $subProjectActivitiesData[$id]['id'] = $id;
                                $subProjectActivitiesData[$id]['control']['selected'] = 0;
                                $subProjectActivitiesData[$id]['management']['selected'] = 0;
                                $subProjectActivitiesData[$id]['flowassurancecontrol']['selected'] = 0;
                                $subProjectActivitiesData[$id]['flowassurancemanagement']['selected'] = 0;
                                $subProjectActivitiesData[$id]['non_monitoring_kpi']['selected'] = 0;
                                $subProjectActivitiesData[$id]['datasheet'] = $subProjectActivity->getSubprojectdatasheet()->getId();

                                //control data
                                if ($subProjectActivity->getIsControl())
                                {
                                    $subProjectActivitiesData[$id]['control']['title'] = $subProjectActivity->getControltitle();

                                    $subProjectActivitiesData[$id]['control']['amber_expression_range'] = $subProjectActivity->getControlamberexpressionrange();
                                    $subProjectActivitiesData[$id]['control']['green_expression_range'] = $subProjectActivity->getControlgreenexpressionrange();
                                    $subProjectActivitiesData[$id]['control']['red_expression_range'] = $subProjectActivity->getControlredexpressionrange();

                                    $subProjectActivitiesData[$id]['control']['amber_range'] = $subProjectActivity->getControlamberrange();
                                    $subProjectActivitiesData[$id]['control']['green_range'] = $subProjectActivity->getControlgreenrange();
                                    $subProjectActivitiesData[$id]['control']['red_range'] = $subProjectActivity->getControlredrange();

                                    $subProjectActivitiesData[$id]['control']['amber_expression'] = $subProjectActivity->getControlamberexpression();
                                    $subProjectActivitiesData[$id]['control']['green_expression'] = $subProjectActivity->getControlgreenexpression();
                                    $subProjectActivitiesData[$id]['control']['red_expression'] = $subProjectActivity->getControlredexpression();

                                    $subProjectActivitiesData[$id]['control']['amber'] = $subProjectActivity->getControlamber();
                                    $subProjectActivitiesData[$id]['control']['green'] = $subProjectActivity->getControlgreen();
                                    $subProjectActivitiesData[$id]['control']['red'] = $subProjectActivity->getControlred();

                                    $subProjectActivitiesData[$id]['control']['kpi_control_color'] = $subProjectActivity->getKpiControl();
                                    $subProjectActivitiesData[$id]['control']['comments'] = $this->br2nl($subProjectActivity->getJustificationControl());

                                    $subProjectActivitiesData[$id]['control']['selected'] = $subProjectActivity->getIsControl();

                                    $compliance['control']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                    if ($subProjectActivity->getKpiControl() == "R")
                                    {
                                        $compliance['control']['totalredkpi']++;
                                        $compliance['control']['redcontrolkpi'][] = $subProjectActivity->getId();
                                    }
                                    if ($subProjectActivity->getKpiControl() == "G")
                                        $compliance['control']['totalgreenkpi']++;
                                    if ($subProjectActivity->getKpiControl() == "A")
                                        $compliance['control']['totalamberkpi']++;

                                }

                                //management data
                                if ($subProjectActivity->getIsManagement())
                                {
                                    $subProjectActivitiesData[$id]['management']['title'] = $subProjectActivity->getManagementtitle();
                                    $subProjectActivitiesData[$id]['management']['activity_person'] = $subProjectActivity->getActivityperson();

                                    $subProjectActivitiesData[$id]['management']['amber'] = $subProjectActivity->getManagementamber();
                                    $subProjectActivitiesData[$id]['management']['green'] = $subProjectActivity->getManagementgreen();
                                    $subProjectActivitiesData[$id]['management']['red'] = $subProjectActivity->getManagementred();
                                    $subProjectActivitiesData[$id]['management']['kpi_management_color'] = $subProjectActivity->getKpiManagement();
                                    $subProjectActivitiesData[$id]['management']['selected'] = $subProjectActivity->getIsManagement();
                                    $subProjectActivitiesData[$id]['management']['comments'] = $this->br2nl($subProjectActivity->getJustificationManagement());

                                    $compliance['management']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                    if ($subProjectActivity->getKpiManagement() == "R")
                                    {
                                        $compliance['management']['totalredkpi']++;
                                        $compliance['management']['redmanagementkpi'][] = $subProjectActivity->getId();
                                    }
                                    if ($subProjectActivity->getKpiManagement() == "G")
                                        $compliance['management']['totalgreenkpi']++;
                                    if ($subProjectActivity->getKpiManagement() == "A")
                                        $compliance['management']['totalamberkpi']++;
                                }

                                if ($subProjectActivity->getIsflowassuranceManagement())
                                {

                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['title'] = $subProjectActivity->getManagementtitle();
                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['activity_person'] = $subProjectActivity->getActivityperson();

                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['amber'] = $subProjectActivity->getManagementamber();
                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['green'] = $subProjectActivity->getManagementgreen();
                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['red'] = $subProjectActivity->getManagementred();
                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['kpi_management_color'] = $subProjectActivity->getKpiManagement();
                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['comments'] = $this->br2nl($subProjectActivity->getJustificationManagement());

                                    $subProjectActivitiesData[$id]['flowassurancemanagement']['selected'] = $subProjectActivity->getIsflowassuranceManagement();

                                    $compliance['flowassurancemanagement']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                    if ($subProjectActivity->getKpiControl() == "R") {
                                        $compliance['flowassurancemanagement']['totalredkpi']++;
                                        $compliance['flowassurancemanagement']['redmanagementkpi'][] = $subProjectActivity->getId();
                                    }
                                    if ($subProjectActivity->getKpiControl() == "G")
                                        $compliance['flowassurancemanagement']['totalgreenkpi']++;
                                    if ($subProjectActivity->getKpiControl() == "A")
                                        $compliance['flowassurancemanagement']['totalamberkpi']++;

                                }

                                if ($subProjectActivity->getIsflowassuranceControl())
                                {

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['title'] = $subProjectActivity->getControltitle();

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['amber_expression_range'] = $subProjectActivity->getControlamberexpressionrange();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['green_expression_range'] = $subProjectActivity->getControlgreenexpressionrange();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['red_expression_range'] = $subProjectActivity->getControlredexpressionrange();

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['amber_range'] = $subProjectActivity->getControlamberrange();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['green_range'] = $subProjectActivity->getControlgreenrange();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['red_range'] = $subProjectActivity->getControlredrange();

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['amber_expression'] = $subProjectActivity->getControlamberexpression();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['green_expression'] = $subProjectActivity->getControlgreenexpression();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['red_expression'] = $subProjectActivity->getControlredexpression();

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['amber'] = $subProjectActivity->getControlamber();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['green'] = $subProjectActivity->getControlgreen();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['red'] = $subProjectActivity->getControlred();

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['kpi_control_color'] = $subProjectActivity->getKpiControl();
                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['comments'] = $this->br2nl($subProjectActivity->getJustificationControl());

                                    $subProjectActivitiesData[$id]['flowassurancecontrol']['selected'] = $subProjectActivity->getIsflowassuranceControl();

                                    $compliance['flowassurancecontrol']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                    if ($subProjectActivity->getKpiControl() == "R")
                                    {
                                        $compliance['flowassurancecontrol']['totalredkpi']++;
                                        $compliance['flowassurancecontrol']['redcontrolkpi'][] = $subProjectActivity->getId();
                                    }
                                    if ($subProjectActivity->getKpiControl() == "G")
                                        $compliance['flowassurancecontrol']['totalgreenkpi']++;
                                    if ($subProjectActivity->getKpiControl() == "A")
                                        $compliance['flowassurancecontrol']['totalamberkpi']++;
                                }

                                //check for production datasheet to ignore in report indexing
                                if ($subProjectActivity->getSubprojectmetrics()->getDatabasetype()->getId() != 6)
                                {

                                    //Important Indexing segment as per report type
                                    if ($report_type == 2 || $report_type == 3 || $report_type == 5) {
                                        if ($subProjectActivity->getIsManagement() || $subProjectActivity->getIsControl()) {
                                            if ($tempsystem == $system->getId()) {
                                                $indeX++;
                                            } else {
                                                $indeX = 1;
                                            }
                                            $subProjectActivitiesData[$id]['activity_index'] = $indeX;
                                        }
                                    } else if ($report_type == 6) {
                                        if ($subProjectActivity->getIsflowassuranceManagement() || $subProjectActivity->getIsflowassuranceControl()) {

                                            if ($tempsystem == $system->getId()) {
                                                $indeX++;
                                            } else {
                                                $indeX = 1;
                                            }
                                            $subProjectActivitiesData[$id]['activity_index'] = $indeX;
                                        }
                                    } else if ($report_type == 1) {
                                        if ($subProjectActivity->getKpiManagement() == "A" || $subProjectActivity->getKpiManagement() == "R" || $subProjectActivity->getKpiControl() == "A" || $subProjectActivity->getKpiControl() == "R") {
                                            if ($tempsystem == $system->getId()) {
                                                $indeX++;
                                            } else {
                                                $indeX = 1;
                                            }
                                        }
                                        $subProjectActivitiesData[$id]['activity_index'] = $indeX;
                                    }
                                }
                                else
                                {
                                    $subProjectActivitiesData[$id]['activity_index'] = 1;
                                }

                                //setting inspection
                                if ($subProjectActivity->getIsInspectionDue())
                                {
                                    $compliance['overdue'] += 1;
                                }

                                $subProjectActivitiesData[$id]['activity_person'] = $subProjectActivity->getActivityperson();
                                $subProjectActivitiesData[$id]['activity_location'] = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivity($id);
                                $subProjectActivitiesData[$id]['frequency'] = $subProjectActivity->getFrequency();
                                $subProjectActivitiesData[$id]['weightage'] = $subProjectActivity->getWeightage();
                                $subProjectActivitiesData[$id]['remedial_action'] = $subProjectActivity->getRemedialaction();
                                $subProjectActivitiesData[$id]['consequences_excursion'] = $subProjectActivity->getConsequencesexcursion();

                                $subProjectActivitiesData[$id]['comments'] = $this->br2nl($subProjectActivity->getComments());

                                if ($subProjectActivity->getIsnonmonitoringkpi())
                                {
                                    $subProjectActivitiesData[$id]['non_monitoring_kpi']['selected'] = $subProjectActivity->getIsnonmonitoringkpi();
                                    $subProjectActivitiesData[$id]['non_monitoring_kpi']['title'] = $subProjectActivity->getManagementtitle();
                                    $subProjectActivitiesData[$id]['non_monitoring_kpi']['comments'] = $this->br2nl($subProjectActivity->getComments());
                                    $subProjectActivitiesData[$id]['control']['green'] = (!empty($subProjectActivity->getControlgreen()) ? $subProjectActivity->getControlgreen() : "");
                                    $subProjectActivitiesData[$id]['control']['red'] = (!empty($subProjectActivity->getControlred()) ? $subProjectActivity->getControlred() : "");
                                }

                                $subProjectActivitiesData[$id]['system']['name'] = $system->getSystemsTitle();
                                $subProjectActivitiesData[$id]['system']['id'] = $system->getId();
                                $subProjectActivitiesData[$id]['system']['description'] = $this->br2nl($subProjectActivity->getDescription());

                                //generating production graphs as per scope version 1
                                //$productiongraphentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getProductionDatasheet('production', $id, $subprojectId );

                                $locations = $cmsDatabasesHelper->getLocationsFromDatabasesPerSystemSubProjectActivity($system->getId(), $subprojectId, $id);

                                //system data, location data, generating datasheet graphs
                                if (count($locations))
                                {

                                    foreach ($locations as $location)
                                    {

                                        $corrosiondatasheetlocationEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByLocation($location['type'], $location['id'], $subprojectId, $id);

                                        $latestEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocation($subProjectActivity->getId(), $location['type'], $location['id'], $subprojectId, 1);

                                        $latestAvailableEntities = array();

                                        if ($location['type'] == 2)
                                        {
                                            $latestAvailableEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getAverageLatestDatasheetLocation($location['id'], $subprojectId, 1);
                                        }

                                        if (!empty($corrosiondatasheetlocationEntities))
                                        {

                                            if ($location['type'] == 2)
                                            {
                                                $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                                                $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $system->getId());
                                            }

                                            $gasDetails = "[";
                                            $waterDetails = "[";
                                            $condensateDetails = "[";
                                            $oilDetails = "[";
                                            $totalDetails = "[";
                                            $h2sDetails = "[";
                                            $co2Details = "[";

                                            $corrosionProbeGraphData = "[";
                                            $chemicalType1GraphData = "[";
                                            $chemicalType1GraphActualData = "[";
                                            $chemicalType1GraphAchievementData = "[";
                                            $samplingGraphData = "[";
                                            $cathodicprotectionGraphData = "[";
                                            $cathodicprotectionGraphDataOnOffPotential = "[";

                                            $chemicalType2InjectionGraphData = "[";
                                            $chemicalType2ConcentrationTargetGraphData = "[";
                                            $chemicalType2ConcentrationActualGraphData = "[";
                                            $chemicalType2AvailabilityGraphData = "[";

                                            $chemicalType2AverageGraphVolData = "[";
                                            $chemicalType2AverageGraphReachData = "[";

                                            $redGraphData = "[";
                                            $greenGraphData = "[";
                                            $amberGraphData = "[";

                                            $redVolThresholdGraphData = "[";
                                            $greenVolThresholdGraphData = "[";
                                            $amberVolThresholdGraphData = "[";

                                            if (isset($corrosiondatasheetlocationEntities[0]['from_date']) && $corrosiondatasheetlocationEntities[0]['from_date']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['from_date']) ? $corrosiondatasheetlocationEntities[0]['from_date']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['to_date']) && $corrosiondatasheetlocationEntities[0]['to_date']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['to_date']) ? $corrosiondatasheetlocationEntities[0]['to_date']->format('Y-m-d') : "");

                                            if (isset($corrosiondatasheetlocationEntities[0]['from_date_normal_injection']) && $corrosiondatasheetlocationEntities[0]['from_date_normal_injection']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_injection'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['from_date_normal_injection']) ? $corrosiondatasheetlocationEntities[0]['from_date_normal_injection']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['to_date_normal_injection']) && $corrosiondatasheetlocationEntities[0]['to_date_normal_injection']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_injection'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['to_date_normal_injection']) ? $corrosiondatasheetlocationEntities[0]['to_date_normal_injection']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['from_date_normal_concentration']) && $corrosiondatasheetlocationEntities[0]['from_date_normal_concentration']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_concentration'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['from_date_normal_concentration']) ? $corrosiondatasheetlocationEntities[0]['from_date_normal_concentration']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['to_date_normal_concentration']) && $corrosiondatasheetlocationEntities[0]['to_date_normal_concentration']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_concentration'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['to_date_normal_concentration']) ? $corrosiondatasheetlocationEntities[0]['to_date_normal_concentration']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['from_date_normal_availability']) && $corrosiondatasheetlocationEntities[0]['from_date_normal_availability']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_availability'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['from_date_normal_availability']) ? $corrosiondatasheetlocationEntities[0]['from_date_normal_availability']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['to_date_normal_availability']) && $corrosiondatasheetlocationEntities[0]['to_date_normal_availability']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_availability'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['to_date_normal_availability']) ? $corrosiondatasheetlocationEntities[0]['to_date_normal_availability']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['from_date_average']) && $corrosiondatasheetlocationEntities[0]['from_date_average']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_average'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['from_date_average']) ? $corrosiondatasheetlocationEntities[0]['from_date_average']->format('Y-m-d') : "");
                                            if (isset($corrosiondatasheetlocationEntities[0]['to_date_average']) && $corrosiondatasheetlocationEntities[0]['to_date_average']->format("Y-m-d") != '-0001-11-30') $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_average'][$location['id']] = (!empty($corrosiondatasheetlocationEntities[0]['to_date_average']) ? $corrosiondatasheetlocationEntities[0]['to_date_average']->format('Y-m-d') : "");

                                            $showCorrosionGraph = 0;
                                            $showChemicalGraph = 0;
                                            $showChemicalAverageGraph = 0;
                                            $showChemicalAvailabilityGraph = 0;
                                            $showCathodicGraph = 0;
                                            $showSamplingGraph = 0;

                                            foreach ($corrosiondatasheetlocationEntities as $index => $datasheetLocationDetails)
                                            {

                                                if (!empty($subProjectActivity->getControlgreenrange()) && $subProjectActivity->getControlgreenrange() != "N/A") {
                                                    $greenThreshold = $subProjectActivity->getControlgreenrange();
                                                } elseif (!empty($subProjectActivity->getControlgreen()) && $subProjectActivity->getControlgreen() != "N/A") {
                                                    $greenThreshold = $subProjectActivity->getControlgreen();
                                                } else {
                                                    $greenThreshold = "";
                                                }

                                                if (!empty($subProjectActivity->getControlredrange()) && $subProjectActivity->getControlredrange() != "N/A") {
                                                    $redThreshold = $subProjectActivity->getControlredrange();
                                                } elseif (!empty($subProjectActivity->getControlred()) && $subProjectActivity->getControlred() != "N/A") {
                                                    $redThreshold = $subProjectActivity->getControlred();
                                                } else {
                                                    $redThreshold = "";
                                                }

                                                if (!empty($subProjectActivity->getControlamberrange()) && $subProjectActivity->getControlamberrange() != "N/A") {
                                                    $amberThreshold = $subProjectActivity->getControlamberrange();
                                                } elseif (!empty($subProjectActivity->getControlamber()) && $subProjectActivity->getControlamber() != "N/A") {
                                                    $amberThreshold = $subProjectActivity->getControlamber();
                                                } else {
                                                    $amberThreshold = "";
                                                }

                                                $showRedFlag = 0;
                                                $showGreenFlag = 0;
                                                $showAmberFlag = 0;

                                                if ($greenThreshold != "")
                                                {
                                                    $showGreenFlag = 1;

                                                    if ($redThreshold != "")
                                                    {
                                                        $showRedFlag = 1;
                                                    }

                                                    if ($greenThreshold == $redThreshold)
                                                    {
                                                        $showRedFlag = 1;
                                                        $showGreenFlag = 0;
                                                    }

                                                }

                                                if ($amberThreshold != "")
                                                {
                                                    $showAmberFlag = 1;

                                                    if ($redThreshold != "")
                                                    {
                                                        $showRedFlag = 1;
                                                    }

                                                    if ($amberThreshold == $redThreshold)
                                                    {
                                                        $showRedFlag = 2;
                                                        $showAmberFlag = 0;
                                                    }

                                                    if ($amberThreshold == $greenThreshold)
                                                    {
                                                        $showAmberFlag = 0;
                                                        $showGreenFlag = 1;
                                                    }

                                                }

                                                if ($redThreshold == "")
                                                {
                                                    $showRedFlag = 0;
                                                }

                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 0;
                                                $newdate = "";
                                                if ($subProjectActivity->getIsManagement() == "A" || $subProjectActivity->getIsManagement() == "R")
                                                {
                                                    $managementAmberActivitiesPerformed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityLocationPerformedManagementKPI($subprojectId, $subProjectActivity->getId(), $location['id'], 'A');
                                                    $managementRedActivitiesPerformed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityLocationPerformedManagementKPI($subprojectId, $subProjectActivity->getId(), $location['id'], 'R');
                                                }

                                                if (isset($datasheetLocationDetails[0]['retrieval_date']) && !empty($datasheetLocationDetails[0]['retrieval_date']))
                                                {
                                                    //managementperformed

                                                    $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                                    if ($location['type'] == 3)
                                                    {

                                                        if (!empty($latestEntities[0][0]['average_rate_mmpy']) && $latestEntities[0][0]['average_rate_mmpy'] == $datasheetLocationDetails[0]['average_rate_mmpy']) {

                                                            if ($this->check_numb($datasheetLocationDetails[0]['average_rate_mmpy']) == 'Positive') {
                                                                if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['average_rate_mmpy'])) {
                                                                    $showCorrosionGraph = 1;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_corrosion_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['average_rate_mmpy']);
//                                                                    echo "Control Second Case corr1:".$subProjectActivity->getId()."==".$location['id']."==".$datasheetLocationDetails[0]['average_rate_mmpy']."<br>";
                                                                }
                                                            } else if ($this->check_numb($datasheetLocationDetails[0]['average_rate_mmpy']) == 'Negative') {

                                                                if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['average_rate_mmpy'])) {
                                                                    $showCorrosionGraph = 2;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 2;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_corrosion_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['average_rate_mmpy']);
//                                                                    echo "Control Second Case corr2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                }
                                                            }

                                                        }

                                                        if ($datasheetLocationDetails[0]['average_rate_mmpy']) $corrosionProbeGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['average_rate_mmpy'] . "],";

                                                    } //production
                                                    else if ($location['type'] == 6) {

                                                        if (!empty($datasheetLocationDetails[0]['retrieval_date'])) {
                                                            $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                                            if ($datasheetLocationDetails[0]['gas_production_mmscf_per_day'] != NULL) $gasDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['gas_production_mmscf_per_day'] . "],";
                                                            if ($datasheetLocationDetails[0]['water_production_bbl_per_day'] != NULL) $waterDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['water_production_bbl_per_day'] . "],";
                                                            if ($datasheetLocationDetails[0]['condensate_production_bbl_per_day'] != NULL) $condensateDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['condensate_production_bbl_per_day'] . "],";
                                                            if ($datasheetLocationDetails[0]['oil_production_bbl_per_day'] != NULL) $oilDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['oil_production_bbl_per_day'] . "],";
                                                            if ($datasheetLocationDetails[0]['total_production_bbl_per_day'] != NULL) $totalDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['total_production_bbl_per_day'] . "],";
                                                            if ($datasheetLocationDetails[0]['h2s_mol_percentage'] != NULL) $h2sDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['h2s_mol_percentage'] . "],";
                                                            if ($datasheetLocationDetails[0]['co2_mol_percentage'] != NULL) $co2Details .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['co2_mol_percentage'] . "],";

                                                        }

                                                    } else if ($location['type'] == 2) {

                                                        if (!empty($datasheetLocationDetails[0]['control_activity_achievement_value'])) {

                                                            if (!empty($latestEntities[0][0]['control_activity_achievement_value']) && $latestEntities[0][0]['control_activity_achievement_value'] == $datasheetLocationDetails[0]['control_activity_achievement_value']) {

                                                                if ($this->check_numb($datasheetLocationDetails[0]['control_activity_achievement_value']) == 'Positive') {
                                                                    if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_achievement_value'])) {
                                                                        $showChemicalGraph = 1;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 3;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_achievement_value']);
//                                                                        echo "Control Second Case chem1:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                } else if ($this->check_numb($datasheetLocationDetails[0]['control_activity_achievement_value']) == 'Negative') {
                                                                    if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_achievement_value'])) {
                                                                        $showChemicalGraph = 2;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 4;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_achievement_value']);
//                                                                        echo "Control Second Case chem2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                }
                                                            }

                                                            if ($datasheetLocationDetails[0]['control_activity_value']) $chemicalType1GraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['control_activity_value'] . "],";
                                                            if ($datasheetLocationDetails[0]['control_activity_actual_value']) $chemicalType1GraphActualData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['control_activity_actual_value'] . "],";
                                                            if ($datasheetLocationDetails[0]['control_activity_achievement_value']) $chemicalType1GraphAchievementData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['control_activity_achievement_value'] . "],";
                                                        }


                                                        if (!empty($datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'])) {
                                                            $chemicalType2InjectionGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'] . "],";
                                                        }

                                                        if (!empty($datasheetLocationDetails[0]['target_ci_concentration_ppm']) && !empty($datasheetLocationDetails[0]['actual_ci_concentration_ppm'])) {
                                                            $chemicalType2ConcentrationTargetGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['target_ci_concentration_ppm'] . "],";                                                        //multi
                                                            $chemicalType2ConcentrationActualGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['actual_ci_concentration_ppm'] . "],";
                                                        }

                                                        if (!empty($datasheetLocationDetails[0]['ci_percentage_availability'])) {

                                                            if (!empty($latestEntities[0][0]['ci_percentage_availability']) && $latestEntities[0][0]['ci_percentage_availability'] == $datasheetLocationDetails[0]['ci_percentage_availability']) {

                                                                if ($this->check_numb($datasheetLocationDetails[0]['ci_percentage_availability']) == 'Positive') {
                                                                    if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['ci_percentage_availability'])) {
                                                                        $showChemicalAvailabilityGraph = 1;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 5;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['ci_percentage_availability']);
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph_threshold'][$location['id']][] = true;
//                                                                        echo "Control Second Case chagh1:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                } else if ($this->check_numb($datasheetLocationDetails[0]['ci_percentage_availability']) == 'Negative') {
                                                                    if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['ci_percentage_availability'])) {
                                                                        $showChemicalAvailabilityGraph = 2;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 6;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['ci_percentage_availability']);
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph_threshold'][$location['id']][] = true;
//                                                                        echo "Control Second Case chagh2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                }
                                                            }

                                                            $chemicalType2AvailabilityGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['ci_percentage_availability'] . "],";

                                                        }

                                                    } else if ($location['type'] == 4) {

                                                        if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value']) {

                                                            if ($this->check_numb($datasheetLocationDetails[0]['control_activity_value']) == 'Positive') {

                                                                if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value'])) {
                                                                    $showSamplingGraph = 1;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 7;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_sampling_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value']);
//                                                                    echo "Control Second Case sa1:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                }
                                                            } else if ($this->check_numb($datasheetLocationDetails[0]['control_activity_value']) == 'Negative') {

                                                                if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value'])) {
                                                                    $showSamplingGraph = 2;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 8;
                                                                    $subProjectActivitiesData[$id]['system']['locations']['show_sampling_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value']);
//                                                                    echo "Control Second Case sa2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                }
                                                            }
                                                        }

                                                        if ($datasheetLocationDetails[0]['control_activity_value']) $samplingGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['control_activity_value'] . "],";

                                                    } else if ($location['type'] == 1) {

                                                        if ($subProjectActivitiesData[$id]['datasheet'] != 8) {

                                                            if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value']) {
                                                                if ($this->check_numb($datasheetLocationDetails[0]['control_activity_value']) == 'Positive') {
                                                                    if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value'])) {
                                                                        $showCathodicGraph = 1;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 9;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value']);
//                                                                        echo "Control Second Case cg1:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                } else if ($this->check_numb($datasheetLocationDetails[0]['control_activity_value']) == 'Negative') {
                                                                    if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value'])) {
                                                                        $showCathodicGraph = 2;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 10;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['control_activity_value']);
//                                                                        echo "Control Second Case cg2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                }
                                                            }

                                                            if ($datasheetLocationDetails[0]['control_activity_value']) $cathodicprotectionGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['control_activity_value'] . "],";

                                                        } else {

                                                            if (!empty($latestEntities[0][0]['on_potential']) && $latestEntities[0][0]['on_potential'] == $datasheetLocationDetails[0]['on_potential']) {
                                                                if ($this->check_numb($datasheetLocationDetails[0]['on_potential']) == 'Positive') {
                                                                    if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['on_potential'])) {
                                                                        $showCathodicGraph = 1;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 9;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['on_potential']);
//                                                                        echo "Control Second Case cg3:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                } else if ($this->check_numb($datasheetLocationDetails[0]['on_potential']) == 'Negative') {
                                                                    if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['on_potential'])) {
                                                                        $showCathodicGraph = 2;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 10;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['on_potential']);
//                                                                        echo "Control Second Case cg4:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                }

                                                            }
                                                            if (!empty($latestEntities[0][0]['off_potential']) && $latestEntities[0][0]['off_potential'] == $datasheetLocationDetails[0]['off_potential']) {
                                                                if ($this->check_numb($datasheetLocationDetails[0]['off_potential']) == 'Positive') {
                                                                    if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['off_potential'])) {
                                                                        $showCathodicGraph = 1;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 11;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['off_potential']);
//                                                                        echo "Control Second Case cg5:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                } else if ($this->check_numb($datasheetLocationDetails[0]['off_potential']) == 'Negative') {

                                                                    if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['off_potential'])) {
                                                                        $showCathodicGraph = 2;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 12;
                                                                        $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['off_potential']);
//                                                                        echo "Control Second Case cg6:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                                    }
                                                                }
                                                            }
                                                            if ($datasheetLocationDetails[0]['on_potential']) $cathodicprotectionGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['on_potential'] . "],";
                                                            if ($datasheetLocationDetails[0]['off_potential']) $cathodicprotectionGraphDataOnOffPotential .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['off_potential'] . "],";
                                                        }

                                                    }

                                                    if ($showRedFlag) $redGraphData .= '[Date.UTC(' . $newdate . '),' . $redThreshold . "],";
                                                    if ($showGreenFlag) $greenGraphData .= '[Date.UTC(' . $newdate . '),' . $greenThreshold . "],";
                                                    if ($showAmberFlag) $amberGraphData .= '[Date.UTC(' . $newdate . '),' . $amberThreshold . "],";

                                                }

                                                if (isset($datasheetLocationDetails[0]['to_date']) && !empty($datasheetLocationDetails[0]['to_date']) && $location['type'] == 2) {
                                                    //$chemicalType2AvailabilityGraphData .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability'] . "],";

                                                    if (!empty($latestAvailableEntities[0][0]['corrosion_inhibitor_percentage_availability']) && $latestAvailableEntities[0][0]['corrosion_inhibitor_percentage_availability'] == $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability']) {

                                                        if ($this->check_numb($datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability']) == 'Positive') {
                                                            if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability'])) {
                                                                $showChemicalAverageGraph = 1;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag1:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        } else if ($this->check_numb($datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability']) == 'Negative') {
                                                            if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability'])) {
                                                                $showChemicalAverageGraph = 2;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['corrosion_inhibitor_percentage_availability']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag2:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        }
                                                    }

                                                    if (!empty($latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']) && $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage'] == $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage']) {
                                                        if ($this->check_numb($datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage']) == 'Positive') {
                                                            if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])) {
                                                                $showChemicalAverageGraph = 3;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 13;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag3:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        } else if ($this->check_numb($datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage']) == 'Negative') {
                                                            if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])) {
                                                                $showChemicalAverageGraph = 4;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 14;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag4:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        }
                                                    }

                                                    if (!empty($latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']) && $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage'] == $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage']) {
                                                        if ($this->check_numb($datasheetLocationDetails[0]['per_time_ci_target_reach_percentage']) == 'Positive') {
                                                            if ($this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])) {
                                                                $showChemicalAverageGraph = 5;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 15;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag5:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        } else if ($this->check_numb($datasheetLocationDetails[0]['per_time_ci_target_reach_percentage']) == 'Negative') {
                                                            if ($this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])) {
                                                                $showChemicalAverageGraph = 6;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 16;
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = $this->compareRedAmberMinusThresholdsYaxis($subProjectActivity, $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage']);
                                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                                echo "Control Second Case cag6:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                            }
                                                        }
                                                    }

                                                    if (!$subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
                                                        $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 17;
                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = 1;
                                                        $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph_threshold'][$location['id']][] = true;
//                                                        echo "Control Second Case cag7:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                    }

                                                    $avdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['to_date']->format('Y-m-d'))));

                                                    $chemicalType2AverageGraphVolData .= '[Date.UTC(' . $avdate . '),' . $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'] . "],";
                                                    $chemicalType2AverageGraphReachData .= '[Date.UTC(' . $avdate . '),' . $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'] . "],";

                                                    if ($showRedFlag) $redVolThresholdGraphData .= '[Date.UTC(' . $avdate . '),' . $redThreshold . "],";
                                                    if ($showGreenFlag) $greenVolThresholdGraphData .= '[Date.UTC(' . $avdate . '),' . $greenThreshold . "],";
                                                    if ($showAmberFlag) $amberVolThresholdGraphData .= '[Date.UTC(' . $avdate . '),' . $amberThreshold . "],";
                                                }

                                            }

                                            // Exception logic based on over all and location KPi specific starts here
                                            $checkMitigationFlag = 0;

                                            // management overall kpi high priority
                                            if (!empty($managementAmberActivitiesPerformed) || !empty($managementRedActivitiesPerformed)) {
//                                                echo "Management Second Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $checkMitigationFlag = 18;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_corrosion_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_corrosion_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_corrosion_graph'][$location['id']][] = 1;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_chemical_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_graph'][$location['id']][] = 1;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_availability_graph'][$location['id']][] = 1;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_cathodic_graph'][$location['id']][] = 1;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_sampling_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_sampling_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_sampling_graph'][$location['id']][] = 1;
                                            }

                                            if ($checkMitigationFlag) {
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = $checkMitigationFlag;
                                            } else if (empty($subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph']) && $this->checkActivityMitigationControlStatus($subProjectActivity)) {
//                                                echo "Control First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                                $subProjectActivitiesData[$id]['system']['locations']['show_graph_exception'] = 1;
                                                $subProjectActivitiesData[$id]['system']['locations']['show_chemical_average_graph'][$location['id']][] = 1;
                                            }

                                            // Exception logic end here

                                            $redGraphData .= "]";
                                            $greenGraphData .= "]";
                                            $amberGraphData .= "]";

                                            $redVolThresholdGraphData .= "]";
                                            $greenVolThresholdGraphData .= "]";
                                            $amberVolThresholdGraphData .= "]";

                                            $subProjectActivitiesData[$id]['system']['locations']['averagethresholds'][$location['id']][] = $redVolThresholdGraphData . "|" . $greenVolThresholdGraphData . "|" . $amberVolThresholdGraphData;
                                            $subProjectActivitiesData[$id]['system']['locations']['thresholds'][$location['id']][] = $redGraphData . "|" . $greenGraphData . "|" . $amberGraphData;

                                            $corrosionProbeGraphData .= "]";
                                            $chemicalType1GraphData .= "]";
                                            $chemicalType1GraphActualData .= "]";
                                            $chemicalType1GraphAchievementData .= "]";
                                            $samplingGraphData .= "]";
                                            $cathodicprotectionGraphData .= "]";
                                            $cathodicprotectionGraphDataOnOffPotential .= "]";

                                            $chemicalType2InjectionGraphData .= "]";
                                            $chemicalType2ConcentrationTargetGraphData .= "]";
                                            $chemicalType2ConcentrationActualGraphData .= "]";
                                            $chemicalType2AvailabilityGraphData .= "]";

                                            $chemicalType2AverageGraphVolData .= "]";
                                            $chemicalType2AverageGraphReachData .= "]";

                                            $gasDetails .= "]";
                                            $waterDetails .= "]";
                                            $oilDetails .= "]";
                                            $condensateDetails .= "]";
                                            $totalDetails .= "]";
                                            $h2sDetails .= "]";
                                            $co2Details .= "]";

                                            $subProjectActivitiesData[$id]['system']['locations']['production_graphs'][$location['id']] = $gasDetails . "|" . $waterDetails . "|" . $condensateDetails . "|" . $oilDetails . "|" . $totalDetails . "|" . $h2sDetails . "|" . $co2Details;

                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']]) && $corrosionProbeGraphData != "[]") {
                                                $corrosionProbeGraphData = $corrosionProbeGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
                                            }
                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']]) && $corrosionProbeGraphData != "[]") {
                                                $corrosionProbeGraphData = $corrosionProbeGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
                                            }

                                            if ($corrosionProbeGraphData != "[]") $subProjectActivitiesData[$id]['system']['locations']['corrosion_probe_graphs'][$location['id']][] = $corrosionProbeGraphData;

//                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']]) && $chemicalType1GraphData != "[]")
//                                            {
//                                                $chemicalType1GraphData = $chemicalType1GraphData."|".$subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
//                                            }
//                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']]) && $chemicalType1GraphData != "[]")
//                                            {
//                                                $chemicalType1GraphData = $chemicalType1GraphData."|".$subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
//                                            }
//
//                                            if ($chemicalType1GraphData != "[]") $subProjectActivitiesData[$id]['system']['locations']['chemical_type1_graphs'][$location['id']][] = $chemicalType1GraphData;

                                            if ($chemicalType1GraphAchievementData != "[]") {
                                                $tempChemicalGraphOneData = $chemicalType1GraphAchievementData . "|" . $chemicalType1GraphData . "|" . $chemicalType1GraphActualData;

                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']])) {
                                                    $tempChemicalGraphOneData = $tempChemicalGraphOneData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
                                                }
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']])) {
                                                    $tempChemicalGraphOneData = $tempChemicalGraphOneData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
                                                }

                                                $subProjectActivitiesData[$id]['system']['locations']['chemical_type1_graphs'][$location['id']][] = $tempChemicalGraphOneData;
                                            }


                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_injection'][$location['id']]) && $chemicalType2InjectionGraphData != "[]") {
                                                $chemicalType2InjectionGraphData = $chemicalType2InjectionGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_injection'][$location['id']];
                                            }
                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_injection'][$location['id']]) && $chemicalType2InjectionGraphData != "[]") {
                                                $chemicalType2InjectionGraphData = $chemicalType2InjectionGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_injection'][$location['id']];
                                            }

                                            if ($chemicalType2InjectionGraphData != "[]") $subProjectActivitiesData[$id]['system']['locations']['chemical_type2_injection_graphs'][$location['id']][] = $chemicalType2InjectionGraphData;

                                            if ($chemicalType2ConcentrationTargetGraphData != "[]" && $chemicalType2ConcentrationActualGraphData != "[]") {

                                                $tempConData = $chemicalType2ConcentrationTargetGraphData . "|" . $chemicalType2ConcentrationActualGraphData;

                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_concentration'][$location['id']])) {
                                                    $tempConData = $tempConData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_concentration'][$location['id']];
                                                }
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_concentration'][$location['id']])) {
                                                    $tempConData = $tempConData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_concentration'][$location['id']];
                                                }
                                                $subProjectActivitiesData[$id]['system']['locations']['chemical_type2_concentration_graphs'][$location['id']][] = $tempConData;
                                            }

                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_availability'][$location['id']]) && $chemicalType2AvailabilityGraphData != "[]") {
                                                $chemicalType2AvailabilityGraphData = $chemicalType2AvailabilityGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_normal_availability'][$location['id']];
                                            }
                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_availability'][$location['id']]) && $chemicalType2AvailabilityGraphData != "[]") {
                                                $chemicalType2AvailabilityGraphData = $chemicalType2AvailabilityGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_normal_availability'][$location['id']];
                                            }

                                            if ($chemicalType2AvailabilityGraphData != "[]") $subProjectActivitiesData[$id]['system']['locations']['chemical_type2_availability_graphs'][$location['id']][] = $chemicalType2AvailabilityGraphData;

                                            if ($chemicalType2AverageGraphVolData != "[]" && $chemicalType2AverageGraphReachData != "[]") {
                                                $tempAverageData = $chemicalType2AverageGraphVolData . "|" . $chemicalType2AverageGraphReachData;
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_average'][$location['id']])) {
                                                    $tempAverageData = $tempAverageData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date_average'][$location['id']];
                                                }
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_average'][$location['id']])) {
                                                    $tempAverageData = $tempAverageData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date_average'][$location['id']];
                                                }

                                                $subProjectActivitiesData[$id]['system']['locations']['chemical_type2_average_graphs'][$location['id']][] = $tempAverageData;
                                            }

                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']]) && $samplingGraphData != "[]") {
                                                $samplingGraphData = $samplingGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
                                            }
                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']]) && $samplingGraphData != "[]") {
                                                $samplingGraphData = $samplingGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
                                            }

                                            if ($samplingGraphData != "[]") $subProjectActivitiesData[$id]['system']['locations']['sampling_graphs'][$location['id']][] = $samplingGraphData;

                                            if ($cathodicprotectionGraphDataOnOffPotential != "[]" && $cathodicprotectionGraphData != "[]") {

                                                $tempCPGraphData = $cathodicprotectionGraphData . "|" . $cathodicprotectionGraphDataOnOffPotential;
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']])) {
                                                    $tempCPGraphData = $tempCPGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
                                                }
                                                if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']])) {
                                                    $tempCPGraphData = $tempCPGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
                                                }

                                                $subProjectActivitiesData[$id]['system']['locations']['cathodic_protection_graphs_two'][$location['id']][] = $tempCPGraphData;
                                            }

                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']]) && $cathodicprotectionGraphData != "[]") {
                                                $cathodicprotectionGraphData = $cathodicprotectionGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['from_date'][$location['id']];
                                            }

                                            if (isset($subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']]) && $cathodicprotectionGraphData != "[]") {
                                                $cathodicprotectionGraphData = $cathodicprotectionGraphData . "|" . $subProjectActivitiesData[$id]['system']['locations']['graphdates']['to_date'][$location['id']];
                                            }

                                            if ($cathodicprotectionGraphData != "[]" && $cathodicprotectionGraphDataOnOffPotential == "[]") $subProjectActivitiesData[$id]['system']['locations']['cathodic_protection_graphs'][$location['id']][] = $cathodicprotectionGraphData;

                                        }

                                        $subProjectActivitiesData[$id]['system']['locations']['names'][$location['id']] = $location['location'];
                                        $subProjectActivitiesData[$id]['system']['locations']['type'][$location['id']] = $location['type'];
                                        $subProjectActivitiesData[$id]['system']['locations']['ids'][$location['id']] = $location['id'];
                                        $subProjectActivitiesData[$id]['system']['locations']['analysis'][$location['id']] = $location['analysis'];
                                        $subProjectActivitiesData[$id]['system']['locations']['recommendations'][$location['id']] = $this->br2nl($location['recommendations']);

                                    }
                                } else {
                                    // Exception logic based on over all and location KPi specific starts here
                                    $checkMitigationFlag = 0;

                                    // management overall kpi high priority
                                    if ($this->checkActivityMitigationManagementStatus($subProjectActivity)) {
                                        //echo "Management First Case:".$subProjectActivity->getId()."==".$subProjectActivity->getControltitle()."==".$subProjectActivity->getManagementtitle()."<br>";
                                        $checkMitigationFlag = 2;
                                        $subProjectActivitiesData[$id]['system']['show_management'] = $checkMitigationFlag;

                                    }

                                }

                            }


                            if ($report_type == 2 || $report_type == 3 || $report_type == 5)
                            {
                                if ($subProjectActivity->getIsManagement() || $subProjectActivity->getIsControl())
                                {
                                    $tempsystem = $system->getId();
                                }
                            }
                            else if ($report_type == 6)
                            {
                                if ($subProjectActivity->getIsflowassuranceManagement() || $subProjectActivity->getIsflowassuranceControl())
                                {
                                    $tempsystem = $system->getId();
                                }
                            }
                            else if ($report_type == 1)
                            {
                                if ($subProjectActivity->getKpiManagement() == "A" || $subProjectActivity->getKpiManagement() == "R" || $subProjectActivity->getKpiControl() == "A" || $subProjectActivity->getKpiControl() == "R")
                                {
                                    $tempsystem = $system->getId();
                                }
                            }

                        }
                    }

                }

//              label:debug_var_dump_all
//              echo "<pre>";
//              print_r($subProjectActivitiesData);
//              echo "</pre>";
//              exit;

                $files = $subproject->getFiles();

                foreach ($files as $document) {
                    $id = $document->getId();

                    if (isSet($documentsData[$id])) {
                        continue;
                    }

                    $documentsData[$id] = array();
                    $documentsData[$id]['id'] = $id;
                    $documentsData[$id]['filename'] = $document->getFilename();
                    $documentsData[$id]['document_number'] = $document->getDocumentNumber();
                    $documentsData[$id]['caption'] = $document->getCaption();
                    $documentsData[$id]['company'] = $document->getCompany();
                    $documentsData[$id]['document_date'] = $document->getDocumentDate();
                    $documentsData[$id]['date'] = $document->getDate();
                }

                $subprojectActiontracker = $subproject->getActiontracker();

                foreach ($subprojectActiontracker as $actiontracker) {
                    $id = $actiontracker->getId();

                    if (isSet($actiontrackerData[$id])) {
                        continue;
                    }

                    $actiontrackerData[$id] = array();

                    $actionStatement = "SELECT e,
						CASE WHEN e.status = 'Live' THEN 1
							 WHEN e.status = 'Close' THEN 2
							 WHEN e.status = 'Hold' THEN 3
							 WHEN e.status = 'Cancelled' THEN 4 ELSE 0 END AS HIDDEN sortCondition
				  FROM
						AIECmsBundle:ActionTrackerItems e
						Where e.actionTracker = :aid
				  ORDER BY
						sortCondition, e.duedate DESC";

                    $actionQuery = $em->createQuery($actionStatement)->setParameter('aid', $id);

                    $actionTrackerItems = $actionQuery->getResult();

                    //foreach($actiontracker->getActiontrackeritems() as $actiontrackeritems)
                    foreach ($actionTrackerItems as $actiontrackeritems) {
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['id'] = $actiontrackeritems->getId();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['item'] = $actiontrackeritems->getItem();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['details'] = $actiontrackeritems->getDetails();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['responsiblegroup'] = $actiontrackeritems->getResponsibleGroup();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['persons'] = $actiontrackeritems->getPersons();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['choices'] = $actiontrackeritems->getChoices();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['status'] = $actiontrackeritems->getStatus();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['duedatestring'] = $actiontrackeritems->getDueDateSting();
                        $actiontrackerData[$id]['actiontrackeritems'][$actiontrackeritems->getId()]['comments'] = $actiontrackeritems->getComments();
                    }

                    $actiontrackerData[$id]['id'] = $id;
                    $actiontrackerData[$id]['title'] = $actiontracker->getTitle();
                    $actiontrackerData[$id]['date'] = $actiontracker->getDate();
                }

                $subprojectMeeting = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->getLatestWorkshop($subprojectId, 1, 0);

                foreach ($subprojectMeeting as $meeting) {
                    $id = $meeting->getId();

                    if (isSet($meetingData[$id])) {
                        continue;
                    }

                    $meetingData[$id] = array();

                    foreach ($meeting->getSpwa() as $attendees) {
                        $meetingData[$id]['attendees'][$attendees->getId()]['id'] = $attendees->getId();
                        $meetingData[$id]['attendees'][$attendees->getId()]['name'] = $attendees->getName();
                        $meetingData[$id]['attendees'][$attendees->getId()]['position'] = $attendees->getPosition();
                        $meetingData[$id]['attendees'][$attendees->getId()]['company'] = $attendees->getCompany();
                    }

                    $meetingData[$id]['id'] = $id;
                    $meetingData[$id]['initiallyassessedby'] = $meeting->getInitiallyAssessedBy();
                    $meetingData[$id]['date'] = $meeting->getDate();
                }

                $subData['project'] = $projectData;
                $subData['documents'] = $documentsData;
                $subData['activities'] = $subProjectActivitiesData;

                $compliance['overallcontrolcompliance'] = 0;
                $compliance['overallmanagementcompliance'] = 0;

                $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                $compliance['control']['kpi_control_compliance'] = array_filter($compliance['control']['kpi_control_compliance'], function ($value) {
                    return !is_null($value);
                });
                $compliance['management']['kpi_management_compliance'] = array_filter($compliance['management']['kpi_management_compliance'], function ($value) {
                    return !is_null($value);
                });

                $compliance['flowassurancecontrol']['kpi_control_compliance'] = array_filter($compliance['flowassurancecontrol']['kpi_control_compliance'], function ($value) {
                    return !is_null($value);
                });
                $compliance['flowassurancemanagement']['kpi_management_compliance'] = array_filter($compliance['flowassurancemanagement']['kpi_management_compliance'], function ($value) {
                    return !is_null($value);
                });

                if (!empty($compliance['control']['kpi_control_compliance'])) $compliance['overallcontrolcompliance'] = (array_sum($compliance['control']['kpi_control_compliance']) / count($compliance['control']['kpi_control_compliance'])) * 100;
                if (!empty($compliance['management']['kpi_management_compliance'])) $compliance['overallmanagementcompliance'] = (array_sum($compliance['management']['kpi_management_compliance']) / count($compliance['management']['kpi_management_compliance'])) * 100;

                if (!empty($compliance['flowassurancecontrol']['kpi_control_compliance'])) $compliance['flowassuranceoverallcontrolcompliance'] = (array_sum($compliance['flowassurancecontrol']['kpi_control_compliance']) / count($compliance['flowassurancecontrol']['kpi_control_compliance'])) * 100;
                if (!empty($compliance['flowassurancemanagement']['kpi_management_compliance'])) $compliance['flowassuranceoverallmanagementcompliance'] = (array_sum($compliance['flowassurancemanagement']['kpi_management_compliance']) / count($compliance['flowassurancemanagement']['kpi_management_compliance'])) * 100;

                $subData['compliance'] = $compliance;
                $subData['actiontracker'] = $actiontrackerData;
                $subData['meeting'] = $meetingData;

                $subData = array_merge($subData, $subProjectData);

                $archivesData['subproject'] = $subData;

                $entity->setAssessmentData($archivesData);

                $entity->setReportingFromDate($compliance['from_date']);

                $entity->setReportingToDate($compliance['to_date']);

                $entity->setComplianceData(json_encode($compliance));

                $em->persist($entity);
                $em->flush();

                //call update report
                $this->updateArchives($subprojectId, $entity, $report_type, $from_date, $to_date, $this->baseUrl);

                return new Response(json_encode(array('result' => 'ok', 'reportType' => $report_type)));

            }
        } else {
            return new Response(json_encode(array('result' => 'notok')));
        }
    }

}
