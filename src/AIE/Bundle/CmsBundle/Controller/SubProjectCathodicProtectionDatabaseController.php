<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase as CathodicProtectionDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;

use AIE\Bundle\CmsBundle\Form\SubProjectCathodicProtectionDatabaseType;
use AIE\Bundle\CmsBundle\Form\SubProjectCathodicProtectionDatabaseEditType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * SubProjectCathodicProtectionDatabaseController controller.
 *
 * @Route("/{projectId}/subprojectcathodicprotectiondatabase/{subprojectId}")
 */
class SubProjectCathodicProtectionDatabaseController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = array('serviceid','cptypeid','equipmenttypeid','anodetypeid','referenceelectrodeid','monitoringmethodid');

    Protected $entityCheck = "";

    Protected $addGranted = 0;
    Protected $editGranted = 0;
    Protected $deleteGranted = 0;
    Protected $excelGranted = 0;
    Protected $selectcolumnsGranted = 0;

    /**
     * Lists all SubProjectCathodicProtectionDatabase entities.
     *
     * @Route("/", name="cms_subproject_cathodicprotectiondatabase")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $projectId, $subprojectId)
    {
        $cathodicprotectiondatabases = array();
        $subprojectsystems = array();
        $em = $this->getManager();
        if ($request->query->get('stepId')) $stepid = $request->query->get('stepId');
        else $stepid= 1;

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $subprojectsystems = $cmsDatabasesHelper->getSubProjectAllSystemsDatabaseItems($subprojectId,1);

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (isset($subprojectsystems) && !empty($subprojectsystems)) {
            $cathodicprotectiondatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 1, $subprojectsystems[0]['bs_id']);
        }

        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_EXCEL',$this->userRolesMergeToCheck)) {
            $this->excelGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_SELECT_COLUMNS',$this->userRolesMergeToCheck)) {
            $this->selectcolumnsGranted = 1;
        }

        usort($subprojectsystems, function($a, $b) {
            if($a['bs_systemstitle'] == $b['bs_systemstitle']) {
                return 0;
            }
            return ($a['bs_systemstitle'] < $b['bs_systemstitle']) ? -1 : 1;
        });

        return array(
            'systems'                     => $subprojectsystems,
            'cathodicprotectiondatabases' => $cathodicprotectiondatabases,
            //'form'                        => $form->createView(),
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'project'                     => $project,
            'subproject'                  => $subproject,
            'wizard_title'                => 'Cathodic Protection Database',
            'stepId' => $stepid,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,
            'excelGranted' => $this->excelGranted,
            'selectcolumnsGranted' => $this->selectcolumnsGranted,
        );

    }

    /**
     * Lists all SubProjectSystems Headbar for system entities.
     *
     * @Route("/{sid}/loadsystems", name="cms_subproject_cathodicprotectiondatabase_loadsystem")
     * @Method("GET")
     * @Template()
     */
    public function loadsystemAction(Request $request,$sid, $projectId, $subprojectId)
    {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $cathodicprotectiondatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 1, $sid);

        $systemInfo = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);

        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }

        return $this->render('AIECmsBundle:SubProjectCathodicProtectionDatabase:systemcathodicprotectiondatabases.html.twig',array(
            'cathodicprotectiondatabases'  => $cathodicprotectiondatabases,
            'systemid'=>$sid,
            'systemInfo' => $systemInfo,
            'projectId'=>$projectId,
            'subprojectId'=>$subprojectId,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,));

    }

    /**
     * Create the entity SubProjectCathodicProtectionDatabase
     *
     * @Route("/{sid}/{stepid}/create", name="cms_subproject_cathodicprotectiondatabase_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $sid, $stepid, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $entity = new CathodicProtectionDatabase();
        $form = $this->createSystemsCathodicProtectionDatabaseForm($entity, $projectId, $subprojectId, $sid, $stepid);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $dupFindData = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->findBy(array('subproject'=>$subprojectId, 'location'=>$data->getLocation(), 'activityid' => $data->getActivityid()));

            if (!$dupFindData ) {

                $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                $entity->setSubproject($subproject);

                $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);
                $entity->setSubProjectSystems($subprojectsystems);
                $file = $data->getFile();
                if ($file) {
                    $uploadedPath = $this->uploadFile($file, $entity);
                    $entity->setFilepath($uploadedPath);
                }

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'New entry has been added to Cathodic Protection Database');
            }
            else
            {
                $this->addFlash('warning', 'Location already exists for the activity');
            }

            return $this->redirect($this->generateUrl('cms_subproject_cathodicprotectiondatabase',array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'stepId' => $stepid)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Delete the entity SubProjectCathodicProtectionDatabase
     *
     * @Route("/{sid}/delete", name="cms_subproject_cathodicprotectiondatabase_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $sid, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        if($request->isXmlHttpRequest()) {

            if (!$this->get('security.token_storage')->getToken()->getUser()) {
                return new Response(json_encode(array('result' => 'nousertoken')));
            }

            $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

            if ($sid){
                $entity = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->find($sid);
            }

            if (!$entity) {
                return new Response(json_encode(array('result' => 'noentity')));
            }

            $em->remove($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }
    }

    /**
     * Creates a form to create a SubProjectCathodicProtectionDatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsCathodicProtectionDatabaseForm(CathodicProtectionDatabase $entity, $projectId, $subprojectId, $sId, $stepId)
    {

        $em = $this->getDoctrine()->getManager('cms');

        $form = $this->createForm(new SubProjectCathodicProtectionDatabaseType($entity, $em, $subprojectId, $sId), $entity, [
            'action' => $this->generateURL('cms_subproject_cathodicprotectiondatabase_create', ['stepid' => $stepId, 'sid' => $sId, 'projectId' =>$projectId, 'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Creates a form to create a CathodicProtectiondatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsCathodicProtectionDatabaseEditForm(CathodicProtectionDatabase $entity, $stepId, $sId, $projectId, $subprojectId)
    {

        $em = $this->getManager('cms');

        $form = $this->createForm(
            new SubProjectCathodicProtectionDatabaseEditType($entity, $em, $subprojectId, $sId),
            $entity,
            [
                'action' => $this->generateUrl('cms_subproject_cathodicprotectiondatabase_edit', ['id' => $entity->getId(), 'stepid' => $stepId, 'sid' => $sId, 'projectId' => $projectId, 'subprojectId' => $subprojectId]),
                'method' => 'POST',
            ]
        );

        return $form;
    }

    /**
     * Update subprojectcathodicprotectiondatabase
     *
     * @Route("/{sid}/updatefields", name="cms_subproject_cathodicprotectiondatabase_updatefields")
     * @Method("POST")
     * @Template()
     */
    public function updateFieldsAction($sid, Request $request)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest() &&  $request->isMethod( 'POST' )) {

            $title = $request->request->get('title');

            $key = $request->request->get('key');

            if ($title) {
                switch ($key) {
                    case "platformorplant":
                        $entity->setPlatformorplant($title);
                        break;
                    case "pandid":
                        $entity->setPandid($title);
                        break;
                    case "location":
                        $entity->setLocation($title);
                        break;
                    case "tag":
                        $entity->setTag($title);
                        break;
                }
            }


            $em->persist($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

    }


    /**
     * Update subprojectcathodicprotectiondatabase
     *
     * @Route("/{stepid}/{sid}/{id}/edit", name="cms_subproject_cathodicprotectiondatabase_edit")
     * @Method("POST")
     * @Template()
     */
    public function editAction(Request $request, $stepid, $sid, $projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Project Cathodic Protection Database entity.');
        }

        $editForm = $this->createSystemsCathodicProtectionDatabaseEditForm($entity, $stepid, $sid, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            /* Handle Uploaded File */
            $data = $editForm->getData();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($subproject);

            $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);
            $entity->setSubProjectSystems($subprojectsystems);
            $file = $data->getFile();
            if ($file)
            {
                $uploadedPath = $this->uploadFile($file, $entity);
                $entity->setFilepath($uploadedPath);
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Entry has been updated to cathodic protection database');

            return $this->redirect($this->generateUrl('cms_subproject_cathodicprotectiondatabase', ['projectId' => $projectId, 'subprojectId' => $subprojectId, 'stepId' => $stepid,]));
        }

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];

    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="cms_subproject_cathodicprotectiondatabase_corrosionsave")
     * @Method("POST")
     * @Template("")
     */
    public function corrosionfieldsupdateAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->query->get('token') !== $this->get('security.csrf.token_manager')->getToken('intention')->getValue()) {
            throw new \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException('Invalid CSRF token');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $entities = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->findBy(array('activityid'=> $id,'subproject'=> $subprojectId));

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        if($request->isMethod( 'POST' ))
        {

            foreach($entities as $entity) {

                $from = $request->request->get('from_'.$entity->getId());
                $to = $request->request->get('to_'.$entity->getId());

                $analysis = $request->request->get('analysis_'.$entity->getId());
                $recommendation = $request->request->get('recommendation_'.$entity->getId());

                $entity->setAnalysis($analysis);
                $entity->setRecommendations($recommendation);

                if (!strchr($from,"Object") && !strchr($to,"Object")) {

                    $from = new \DateTime($from);
                    $to = new \DateTime($to);

                    $entity->setFrom($from);
                    $entity->setTo($to);
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Changes saved');
            $systemId = $request->query->get('systemId');
            return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_graph', ['id' => $id, 'ids' => html_entity_decode($request->query->get('ids')), 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => 'normal', 'systemId' => $systemId]));
        }

        return [
            'entity' => $entity
        ];
    }

}
