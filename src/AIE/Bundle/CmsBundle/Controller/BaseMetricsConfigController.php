<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig;
use AIE\Bundle\CmsBundle\Form\BaseMetricsConfigType;
use AIE\Bundle\CmsBundle\Form\BaseMetricsConfigEditType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * BaseMetricsConfig controller.
 *
 * @Route("/basemetrics")
 */
class BaseMetricsConfigController extends CmsBaseController
{

    /**
     * Lists all BaseMetricsConfig entities.
     *
     * @Route("/", name="cms_basemetrics")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        //$entities = $this->getGrantedBaseMetricsConfig();

        $entities = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->findBy([], array('title' => 'ASC'));


        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');

        foreach ($entities as $entity) {
            $deleteForm = $this->createDeleteForm($entity->getId());
            $entity->delete_form = $deleteForm->createView();
        }

        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );

        return [
            'entities' => $entities,
            'pagination' => $pagination,
        ];
    }

    /**
     * Lists all BaseMetricsConfig entities.
     *
     * @Route("/{id}/show", name="cms_basemetrics_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Base Metrics Config entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a new BaseMetricsConfig entity.
     *
     * @Route("/", name="cms_basemetrics_create")
     * @Method("POST")
     * @Template("AIECmsBundle:BaseMetrics:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getManager();
        $entity = new BaseMetricsConfig();
        //$type = $request->request->get('aie_bundle_cmsbundle_projects')['type'];

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $formData = $form->getData();

            $pData = $request->request->get('aie_bundle_cmsbundle_basemetrics');

            /* Handle Uploaded File */

            $datasheetconfig = $em->getRepository('AIECmsBundle:BaseDatasheetConfig')->findOneBy(array('databasetype' => $pData['databasetype'],'datasheettype' => $pData['datasheettype']));

            $cmsHelper = $this->get('aie_cms.cms.helper');

            $isUsed = $cmsHelper->isMetricsUsed($formData->getTitle(),$formData->getDatabasetype());

            if (!$datasheetconfig){
                $this->addFlash('error', 'Database and datasheet mismatch');
            }

            if ($isUsed){
                $this->get('session')->getFlashBag()->add('error', "Metric/Database combination already in use.");
            } else {

                /*  */
                $em = $this->getManager();

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'New metric ' . $entity->getTitle() . ' created');

                return $this->redirect($this->generateUrl('cms_basemetrics'));
            }
        }

        return $this->render('AIECmsBundle:BaseMetricsConfig:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Base Metrics entity.
     *
     * @param BaseMetricsConfig $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BaseMetricsConfig $entity)
    {
        $em = $this->getManager();
 
        $form = $this->createForm(
            new BaseMetricsConfigType($entity),
            $entity,
            [
                'action' => $this->generateUrl('cms_basemetrics_create'),
                'method' => 'POST',
            ]
        );

        //$form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));
        //$form->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Base Metrics entity.
     *
     * @Route("/new", name="cms_basemetrics_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new BaseMetricsConfig();
        $form = $this->createCreateForm($entity);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing BaseMetricsConfig entity.
     *
     * @Route("/{id}/edit", name="cms_basemetrics_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BaseMetricsConfig entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a BaseMetricsConfig entity.
     *
     * @param BaseMetricsConfig $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(BaseMetricsConfig $entity)
    {
        $form = $this->createForm(
            new BaseMetricsConfigEditType($entity),
            $entity,
            [
                'action' => $this->generateUrl('cms_basemetrics_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        return $form;
    }

    /**
     * Edit an existing BaseMetricsConfig entity.
     *
     * @Route("/{id}", name="cms_basemetrics_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:BaseMetricsConfig:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Base Metrics entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid() && $editForm->isSubmitted()) {

            $formData = $editForm->getData();

            $pData = $request->request->get('aie_bundle_cmsbundle_basemetrics_edit');

            $datasheetconfig = $em->getRepository('AIECmsBundle:BaseDatasheetConfig')->findOneBy(array('databasetype' => $pData['databasetype'],'datasheettype' => $pData['datasheettype']));

            $cmsHelper = $this->get('aie_cms.cms.helper');

            if (!$datasheetconfig){

                $this->addFlash('error', 'Database and datasheet mismatch!');

                return $this->redirect($this->generateUrl('cms_basemetrics_edit',array('id'=>$id)));

            }

            $posts = $em->getRepository('AIECmsBundle:BaseActivity')->findBy(array('basemetrics' => $entity->getId()));

            foreach ($posts as $post) {
                $post->setBasedatasheet($em->getRepository('AIECmsBundle:BaseDatasheetType')->find($pData['datasheettype']));
                $em->persist($post);

                $subposts = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('baseactivityid' => $post->getId()));
                foreach ($subposts as $subpost) {
                    $subpost->setSubprojectdatasheet($em->getRepository('AIECmsBundle:BaseDatasheetType')->find($pData['datasheettype']));
                    $em->persist($subpost);
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Metric updated');

            return $this->redirect($this->generateUrl('cms_basemetrics'));

        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a BaseMetricsConfig entity.
     *
     * @Route("/{id}/delete", name="cms_basemetrics_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($id);

        if ($form->isValid()) {

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Base Metrics entity.');
            }

            $basemetricsentity = $em->getRepository('AIECmsBundle:BaseActivity')->findOneBy(array('basemetrics'=>$id));

            $subprojectmetricsentity = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOneBy(array('subprojectmetrics'=>$id));

            //$securityContext = $this->container->get('security.authorization_checker');
            if (!$basemetricsentity && !$subprojectmetricsentity) {
                $em->remove($entity);
                $em->flush();
                $this->addFlash('success', 'Metric deleted');
            }
            else{
                $this->addFlash('warning', 'Metric cannot be deleted, it is used in matrix configuration!');
            }

        }
        return $this->redirect($this->generateUrl('cms_basemetrics'));

    }

    /**
     * Creates a form to delete a BaseMetricsConfig entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_basemetrics_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }
}
