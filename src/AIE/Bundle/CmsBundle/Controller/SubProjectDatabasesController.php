<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\CmsBundle\Entity\SubProjects;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectSystems;
use AIE\Bundle\CmsBundle\Entity\SubProjectThreats;
use AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques;
use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use AIE\Bundle\CmsBundle\Form\SubProjectsType;
use AIE\Bundle\CmsBundle\Form\SubProjectsEditType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * SubProjects Databses controller.
 *
 * @Route("/{projectId}/{subprojectId}/databases")
 */
class SubProjectDatabasesController extends CmsBaseController
{

    /**
     * Lists all SubProjects Databses entities.
     *
     * @Route("/", name="cms_subproject_databaseslist")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $alldatabases = $cmsDatabasesHelper->getSubProjectAllSystemsDatabasesMetrics($subprojectId,1);

        return array(
            'alldatabases' => $alldatabases,
            'projectId'    => $projectId,
            'subprojectId' => $subprojectId,
        );
    }

}
