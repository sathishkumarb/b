<?php

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;


/**
 * Sections controller.
 *
 * @Route("/ajax")
 */
class AjaxController extends CmsBaseController {

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/report/chart_img", name="cms_chart")
     * @Method("POST")
     * @Template("AIECmsBundle:Reports:chart_img.html.twig")
     */
    public function chartAction(Request $request)
    {
    }

    /**
     * Lists all ApprovalOwners entities.
     *
     * @Route("/sendmail", name="cms_overdueemails")
     * @Method("GET")
     * @Template()
     */
    public function sendcmsmailAction(Request $request)
    {
        error_reporting(-1);
// Replace path_to_sdk_inclusion with the path to the SDK as described in 
// http://docs.aws.amazon.com/aws-sdk-php/v3/guide/getting-started/basic-usage.html
//define('REQUIRED_FILE','path_to_sdk_inclusion'); 
                                                  
// Replace sender@example.com with your "From" address. 
// This address must be verified with Amazon SES.
define('SENDER', 'veracity@aiegroup.org');           

// Replace recipient@example.com with a "To" address. If your account 
// is still in the sandbox, this address must be verified.
define('RECIPIENT', 'meet.sathish@gmail.com');    

// Specify a configuration set. If you do not want to use a configuration
// set, comment the following variable, and the 
// 'ConfigurationSetName' => CONFIGSET argument below.
define('CONFIGSET','ConfigSet');

// Replace us-west-2 with the AWS Region you're using for Amazon SES.
define('REGION','eu-west-1'); 

define('SUBJECT','Amazon SES test (AWS SDK for PHP)');

define('HTMLBODY','<h1>AWS Amazon Simple Email Service Test Email</h1>'.
                  '<p>This email was sent with <a href="https://aws.amazon.com/ses/">'.
                  'Amazon SES</a> using the <a href="https://aws.amazon.com/sdk-for-php/">'.
                  'AWS SDK for PHP</a>.</p>');
define('TEXTBODY','This email was send with Amazon SES using the AWS SDK for PHP.');

define('CHARSET','UTF-8');


//use Aws\Ses\SesClient;
//use Aws\Ses\Exception\SesException;

$client = SesClient::factory(array(
    'version'=> 'latest',     
    'region' => REGION
));

try {
     $result = $client->sendEmail([
    'Destination' => [
        'ToAddresses' => [
            RECIPIENT,
        ],
    ],
    'Message' => [
        'Body' => [
            'Html' => [
                'Charset' => CHARSET,
                'Data' => HTMLBODY,
            ],
			'Text' => [
                'Charset' => CHARSET,
                'Data' => TEXTBODY,
            ],
        ],
        'Subject' => [
            'Charset' => CHARSET,
            'Data' => SUBJECT,
        ],
    ],
    'Source' => SENDER,
    // If you are not using a configuration set, comment or delete the
    // following line
    'ConfigurationSetName' => CONFIGSET,
]);
     $messageId = $result->get('MessageId');
     echo("Email sent! Message ID: $messageId"."\n");

} catch (SesException $error) {
     echo("The email was not sent. Error message: ".$error->getAwsErrorCode()."\n");
}
        $em = $this->getManager();

        $mailer=$this->container->get('mailer');

//GET ALL OVERDUE ACTIONS
        $overdueActions = null;

        //if ($input->getOption('month'))
        //{
        //  $overdueActions = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOverDue(true);

        // echo "<pre>";
        // print_r($overdueActions);
        // echo "</pre>";

         $message = (new \Swift_Message());
         //$message->setContentType("text/html");
         $message->setSubject('test from program')
                    ->setFrom('veracity@aiegroup.org')
                    ->setTo(['meet.sathish@gmail.com','sathish.kumar@aiegroup.org'])
                    ->setBody(
                        'test'
                    );

if (!$mailer->send($message, $failures))
{
  echo "Failures:";
  print_r($failures);
}
        exit;
        //}
        //else
        //{
         //$overdueActions = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOverDue();
        //}

        $subprojectOwnersActions = [];

        $systemActions = [];


        foreach ($overdueActions as $key => $action)
        {

            $action = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($action['id']);

            $currDate_U = new \DateTime();

            $repeatedAction = clone $action;

            if($repeatedAction->getFrequency() == 0){
                continue;
            }

            $inc=0;

            $nextDate = $repeatedAction->getNextInspectionDate(true);
            $overdueActions[$key]['child'][$inc] = $nextDate->format('d F Y');
            $repeatedAction->setLastInspectionDate($nextDate);
            $nextDate = $repeatedAction->getNextInspectionDate(true);
            //echo $nextDate->format('d/m/y')."<br>";
            $nextDate_U = (int)$nextDate->format('U');

            while ($nextDate_U <= (int) $currDate_U->format('U'))
            {
                $inc++;
                $repeatedActions[] = clone $repeatedAction;

                $overdueActions[$key]['child'][$inc] = $nextDate->format('d F Y');
                //set repeated action last time to next date
                $repeatedAction->setLastInspectionDate($nextDate);
                //set next date from repeated action
                $nextDate = $repeatedAction->getNextInspectionDate(true);

                $nextDate_U = (int)$nextDate->format('U');
            }

        }


        foreach ($overdueActions as $key => $actions)
        {

            $action = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($actions['id']);

            if (! $action)
                continue;

            $mitigation = $action->getControltitle();
            $mitigationTechnique = $action->getSubProjectMitigationTechniques();

            $systemThreat = $mitigationTechnique->getSubProjectThreats();
            $systems = $systemThreat->getSubProjectSystems();

            $subproject = $systems->getSubProject();
            $dueDate = $action->getNextInspectionDate();
            $actionOwner = $action->getActionOwner();

            if (! $actionOwner)
                continue;

            if (! isSet($subprojectOwnersActions[$subproject->getId()]))
            {
                $subprojectOwnersActions[$subproject->getId()] = [
                    'subproject' => $subproject,
                    'owners'   => []
                ];
            }

            if (! isSet($subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]))
            {
                $subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()] = [
                    'owner'   => $actionOwner,
                    'actions' => []
                ];
            }

            $actionName = $mitigation;
            if (isSet($systemActions[$systems->getId()]))
            {
                if (isSet($systemActions[$systems->getId()][$actionName]))
                {
                    $index = $systemActions[$actionName];
                    if ($systemActions[$index]->getFrequency() > $action->getFrequency())
                    {
                        unset($subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]['actions'][$index]);
                    } else
                    {
                        continue;
                    }
                } else
                {
                    $systemActions[$systems->getId()][$actionName] = $key;
                }
            } else
            {
                $systemActions[$systems->getId()] = [];
                $systemActions[$systems->getId()][$actionName] = $key;
            }

            $actions['next_inspection_date']=$action->getNextInspectionDate();
            $actions['is_inspection_due']=$action->getIsInspectionDue();
            $actions['action_owner_name']=$action->getActionOwner()->getName();

            $subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]['actions'][$key] = $actions;
        }

        echo "<pre>";
        print_r($subprojectOwnersActions);
        echo "</pre>";


    }

}
