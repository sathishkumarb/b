<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageAddColumnsDatabase;

use AIE\Bundle\CmsBundle\Form\SubProjectChemicalDosageDatabaseEditColumnsType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * SubProjectChemicalDosageAddColumnsDatabaseController controller.
 *
 * @Route("/{projectId}/{subprojectId}/subprojectschemicaldosageaddcolumns")
 */
class SubProjectChemicalDosageAddColumnsController extends CmsBaseController
{
    Protected $addGranted = 0;

    /**
     * Lists all subprojectschemicaldosageaddcolumns entities.
     *
     * @Route("/", name="cms_subprojectschemicaldosageaddcolumns")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId,$subprojectId)
    {
        $em = $this->getManager();

        //$entities = $this->getGrantedProjects();

        $entities = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->findBy(array('subproject'=>$subprojectId));
        $currentEntity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->findOneBy(array('subproject'=>$subprojectId));

        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );

        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }

        return [
            'entities' => $entities,
            'pagination' => $pagination,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'currentEntity' => $currentEntity,
            'addGranted' => $this->addGranted,
        ];
    }

    /**
     * Creates a new Projects entity.
     *
     * @Route("/", name="cms_subprojectschemicaldosageaddcolumns_create")
     * @Method("POST")
     * @Template("AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase:new.html.twig")
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectChemicalDosageAddColumnsDatabase();
        $em = $this->getManager();
        //$type = $request->request->get('aie_bundle_cmsbundle_projects')['type'];

        $form = $this->createCreateForm($entity, $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $project = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($project);

            $em->persist($entity);

            $em->flush();

            $this->addFlash('success', 'New column(s) added / edited');

            return $this->redirect($this->generateUrl('cms_subproject_chemicaldosagedatabase', ['projectId' => $projectId,'subprojectId' => $subprojectId]));

        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubProjectChemicalDosageAddColumnsDatabase $entity, $projectId, $subprojectId)
    {
        $form = $this->createForm(
            new SubProjectChemicalDosageDatabaseEditColumnsType(),
            $entity,
            [
                'action' => $this->generateUrl('cms_subprojectschemicaldosageaddcolumns_create', array('projectId' => $projectId,'subprojectId' => $subprojectId)),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }


    /**
     * Displays a form to create a new Projects entity.
     *
     * @Route("/new", name="cms_subprojectschemicaldosageaddcolumns_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId, $subprojectId)
    {
        $entity = new SubProjectChemicalDosageAddColumnsDatabase();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}", name="cms_subprojectschemicaldosageaddcolumns_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $projectId, $subprojectId)
    {
//        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
//            throw $this->createAccessDeniedException('You do not have permission to view the project.');
//        }

        $em = $this->getManager();

        /** @var Projects $entity */
        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);
        return [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ];
    }


    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/dashboard", name="cms_subprojectschemicaldosageaddcolumns_dashboard")
     * @Method("GET")
     * @Template()
     */
    public function dashboardAction($id, $projectId, $subprojectId)
    {
//        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
//            throw $this->createAccessDeniedException('You do not have permission to view the project.');
//        }

        $em = $this->getManager();

        /** @var Projects $entity */
        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Projects Chemical Dosage Columns entity.');
        }

        return [
            'entity' => $entity,
        ];
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/edit", name="cms_subprojectschemicaldosageaddcolumns_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Projects Chemical Dosage Columns  entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SubProjectChemicalDosageAddColumnsDatabase $entity, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $form = $this->createForm(
            new SubProjectChemicalDosageDatabaseEditColumnsType($entity),
            $entity,
            [
                'action' => $this->generateUrl('cms_subprojectschemicaldosageaddcolumns_update', ['id' => $entity->getId(), 'projectId' => $projectId,'subprojectId' => $subprojectId]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="cms_subprojectschemicaldosageaddcolumns_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Chemical Dosage Columns  entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);
        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            /* Handle Uploaded File */
            $data = $editForm->getData();

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'New column(s) added / edited');

            return $this->redirect($this->generateUrl('cms_subproject_chemicaldosagedatabase', ['projectId' => $projectId,'subprojectId' => $subprojectId]));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a Projects entity.
     *
     * @Route("/{id}", name="cms_subprojectschemicaldosageaddcolumns_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $subprojectId, $id)
    {

        $form = $this->createDeleteForm($projectId, $subprojectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Chemical Dosage Columns  entity.');
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cms_subproject_chemicaldosagedatabase',['projectId' => $projectId,'subprojectId' => $subprojectId]));
    }

    /**
     * Creates a form to delete a Projects entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $projectId, $subprojectId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subprojectschemicaldosageaddcolumns_delete', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }
}
