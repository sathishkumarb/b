<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Security\AuthenticatedController;
use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AIE\Bundle\CmsBundle\Security;
use Symfony\Component\HttpFoundation\File\File;
use AIE\Bundle\VeracityBundle\Controller\BaseController as VeracityBaseController;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class CmsBaseController extends VeracityBaseController
{

    protected $connection = 'cms';
    protected $folderDirBase = 'cms';
    protected $registrarClassName;

    protected $cmsUserGroupRoles;
    protected $userRolesMergeToCheck;
    protected $securityHelper;
    protected $baseUrl;

    protected $userRoles;

    protected $reportChoices = [
        1 => 'Corrosion Management Report With Exception',
        2 => 'Corrosion Management Supplementary Report',
        3 => 'Corrosion Management Comprehensive Report',
        4 => 'Corrosion Management and Flow Assurance Management Matrices',
        5 => 'Corrosion Management Compliance Report',
        6 => 'Flow Assurance Management Comprehensive Report',
        7 => 'Flow Assurance Management Compliance Report',
    ];

    public function __construct() {

        global $kernel;

        $securityContext = $kernel->getContainer()->get('security.authorization_checker');

        /* Set Roles based on anomaly project groups user associated */
        $token = $kernel->getContainer()->get('security.context')->getToken();

        $loggedInUser = $token->getUser();

        /* Set Roles based on anomaly project groups user associated */

        $this->ccmUserGroupRoles = $kernel->getContainer()->get('aie_cms.user.helper')->getCcmGroupRoles($loggedInUser->getId());

        $this->userRolesMergeToCheck = [];

        if ($this->ccmUserGroupRoles && count($this->ccmUserGroupRoles) > 1){
            foreach($this->ccmUserGroupRoles as $roles){
                $this->userRolesMergeToCheck = array_merge( $this->userRolesMergeToCheck, unserialize($roles->getRoles()) );
            }
        }
        else{
            if($this->ccmUserGroupRoles)
                $this->userRolesMergeToCheck = (count($this->ccmUserGroupRoles) ? unserialize($this->ccmUserGroupRoles[0]->getRoles()) : []);
        }

        $this->securityHelper = $kernel->getContainer()->get('aie_cms.user.helper');

        if (!$this->userRolesMergeToCheck && !$securityContext->isGranted('ROLE_SUPER_ADMIN')){


                return $this->redirect('accessDeniedException');
   }

        // /* End Roles set for ccm groups*/


    }

    /**
     * @param $em
     * @return array
     */
    public function getGrantedProjects() {
        $em = $this->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $projects = [];

        //if ($securityContext->isGranted('ROLE_PROJECTS')) {
        $rolesExist = null;

        if ($this->userRolesMergeToCheck){
            $rolesExist = $this->securityHelper->isRoleGranted('ROLE_PROJECTS',$this->userRolesMergeToCheck);
        }

        if( $securityContext->isGranted('ROLE_SUPER_ADMIN') ) {
            $projects = $em->getRepository('AIECmsBundle:Projects')->findBy(array(), array('name' => 'ASC'));
        } else {
            $user = $this->getUser();
            $projects = [];
            $assignedProjects = $em->getRepository('AIECmsBundle:UserProjectGroup')->findBy(['user' => $user->getId()]);
            $temp = [];
            foreach ($assignedProjects as $p) {
                if (!in_array($p->getProject()->getId(),$temp) ) $projects[ ] = $p->getProject();
                $temp[] = $p->getProject()->getId();
            }
        }

        return $projects;
    }

    public function getReportChoices()
    {
        return $this->reportChoices;
    }
}
