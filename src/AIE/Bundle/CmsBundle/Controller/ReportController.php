<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\CmsBundle\Entity\Registrar;
use AIE\Bundle\CmsBundle\Entity\Request;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Request controller.
 *
 * @Route("/reports")
 */
class ReportController extends CmsBaseController {

	/**
	 * Finds and displays a Request entity.
	 *
	 * @Route("/{id}", name="cms_report_download")
	 * @Method("GET")
	 * @Template()
	 */
	public function downloadAction($id) {


		return $response;
	}
}