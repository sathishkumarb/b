<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use AIE\Bundle\CmsBundle\Entity\SubProjectSystems;
use AIE\Bundle\CmsBundle\Entity\SubProjectThreats;
use AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques;
use AIE\Bundle\CmsBundle\Entity\subprojectmatrixConfig;
use AIE\Bundle\CmsBundle\Form\SubProjectSystemsType;
use AIE\Bundle\CmsBundle\Form\SubProjectThreatsType;
use AIE\Bundle\CmsBundle\Form\SubProjectMitigationTechniquesType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivityType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivityEditType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * subprojectmatrixConfig controller.
 *
 * @Route("/{projectId}/subprojectmatrixconfig/{subprojectId}")
 */
class SubProjectMatrixConfigController extends CmsBaseController
{

    Protected $subprojectdataId = "";

    Protected $entityDuplicateCheck = "";

    Protected $showRoleMatrixConfig = 0;

    Protected $showRoleMatrixConfigEdit = 0;

    Protected $showRoleMatrixConfigSystemsAdd = 0;

    Protected $showRoleMatrixConfigThreatsAdd = 0;

    Protected $showRoleMatrixConfigMitigationAdd = 0;

    Protected $showRoleMatrixConfigActivityAdd = 0;

    Protected $showRoleMatrixConfigMitigationDelete = 0;

    Protected $showRoleMatrixConfigThreatsDelete = 0;

    Protected $showRoleMatrixConfigSystemsDelete = 0;

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/", name="cms_subprojectmatrix")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId,$subprojectId)
    {
        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $dbthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(array('subproject' => $subprojectId));

        if (!empty($dbthreats)) {
            foreach ($dbthreats as $dbthreat) {
                $subsystemid = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $dbthreat->getSubproject()->getId(), 'basesystemid' => $dbthreat->getBasesystemid()));
                if ($dbthreat->getBasesystemid() != NULL && !empty($subsystemid)) {
                    $dbthreat->setSubProjectSystems($em->getRepository('AIECmsBundle:SubProjectSystems')->find($subsystemid->getId()));
                    $em->persist($dbthreat);
                }
            }
        }

        $dbmitigationtechniques = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findBy(array('subproject' => $subprojectId));
        if (!empty($dbmitigationtechniques)) {
            foreach ($dbmitigationtechniques as $dbmitigationtechnique) {
                $subthreatid = $em->getRepository('AIECmsBundle:SubProjectThreats')->findOneBy(array('subproject' => $dbmitigationtechnique->getSubproject()->getId(), 'basethreatid' => $dbmitigationtechnique->getBasethreatid()));
                if ($dbmitigationtechnique->getBasethreatid() != NULL && !empty($subthreatid)) {
                    $dbmitigationtechnique->setSubProjectThreats($em->getRepository('AIECmsBundle:SubProjectThreats')->find($subthreatid->getId()));
                    $em->persist($dbmitigationtechnique);
                }
            }
        }

        $dbactivities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('subproject' => $subprojectId));
        if (!empty($dbactivities)) {
            foreach ($dbactivities as $dbactivity) {
                $submitigationtechniqueid = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findOneBy(array('subproject' => $dbactivity->getSubproject()->getId(), 'basemitigationtechniqueid' => $dbactivity->getBasemitigationtechniqueid()));
                if ($dbactivity->getBasemitigationtechniqueid() != NULL && !empty($submitigationtechniqueid)) {
                    $dbactivity->setSubProjectMitigationTechniques($em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($submitigationtechniqueid->getId()));
                    $em->persist($dbactivity);
                }
            }
        }

        $em->flush();
        $em->clear();

        $statement = 'SELECT bs,bt,bmt,ba,bsa,bmc,bdt FROM AIECmsBundle:SubProjectSystems bs left join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems left join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats left join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques left join AIECmsBundle:BaseDatasheetType bsa WITH bsa.id = ba.subprojectdatasheet left join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.subprojectmetrics left join AIECmsBundle:BaseDatabaseType bdt WITH bdt.id = bmc.databasetype WHERE bs.subproject=:subprojectid or bt.subproject=:subprojectid or bmt.subproject=:subprojectid and ba.subproject=:subprojectid ORDER BY bs.systemstitle ASC';

        $query = $em->createQuery($statement)->setParameter('subprojectid', $subprojectId);

        $subprojectmatrixProperties = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $entity = new SubProjectSystems();
        $form   = $this->createCreateSubProjectSystemsForm($entity, $projectId, $subprojectId);

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION_SHOW',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfig = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION_EDIT',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigEdit = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_SYSTEMS_ADD',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigSystemsAdd = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_THREATS_ADD',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigThreatsAdd = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_MITIGATION_ADD',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigMitigationAdd = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_ACTIVITY_ADD',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigActivityAdd = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_MITIGATION_DELETE',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigMitigationDelete = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_SYSTEMS_DELETE',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigSystemsDelete = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck) || $this->securityHelper->isRoleGranted('ROLE_SUB_PROJECT_MATRIX_THREATS_DELETE',$this->userRolesMergeToCheck)){
            $this->showRoleMatrixConfigThreatsDelete = 1;
        }

        return array(
            'subprojectmatrixproperties' => $subprojectmatrixProperties,
            'form' =>$form->createView(),
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'showRoleMatrixConfig' => $this->showRoleMatrixConfig,
            'showRoleMatrixConfigEdit' => $this->showRoleMatrixConfigEdit,
            'showRoleMatrixConfigSystemsAdd' => $this->showRoleMatrixConfigSystemsAdd,
            'showRoleMatrixConfigThreatsAdd' => $this->showRoleMatrixConfigThreatsAdd,
            'showRoleMatrixConfigMitigationAdd' => $this->showRoleMatrixConfigMitigationAdd,
            'showRoleMatrixConfigActivityAdd' => $this->showRoleMatrixConfigActivityAdd,
            'showRoleMatrixConfigMitigationDelete' => $this->showRoleMatrixConfigMitigationDelete,
            'showRoleMatrixConfigSystemsDelete' => $this->showRoleMatrixConfigSystemsDelete,
            'showRoleMatrixConfigThreatsDelete' => $this->showRoleMatrixConfigThreatsDelete,
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/refer", name="cms_subprojectmatrix_refer")
     * @Method("GET")
     * @Template()
     */
    public function referAction($projectId,$subprojectId)
    {
        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $dbthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(array('subproject' => $subprojectId));

        if (!empty($dbthreats)) {
            foreach ($dbthreats as $dbthreat) {
                $subsystemid = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $dbthreat->getSubproject()->getId(), 'basesystemid' => $dbthreat->getBasesystemid()));
                if ($dbthreat->getBasesystemid() != NULL && !empty($subsystemid)) {
                    $dbthreat->setSubProjectSystems($em->getRepository('AIECmsBundle:SubProjectSystems')->find($subsystemid->getId()));
                    $em->persist($dbthreat);
                }
            }
        }

        $dbmitigationtechniques = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findBy(array('subproject' => $subprojectId));
        if (!empty($dbmitigationtechniques)) {
            foreach ($dbmitigationtechniques as $dbmitigationtechnique) {
                $subthreatid = $em->getRepository('AIECmsBundle:SubProjectThreats')->findOneBy(array('subproject' => $dbmitigationtechnique->getSubproject()->getId(), 'basethreatid' => $dbmitigationtechnique->getBasethreatid()));
                if ($dbmitigationtechnique->getBasethreatid() != NULL && !empty($subthreatid)) {
                    $dbmitigationtechnique->setSubProjectThreats($em->getRepository('AIECmsBundle:SubProjectThreats')->find($subthreatid->getId()));
                    $em->persist($dbmitigationtechnique);
                }
            }
        }

        $dbactivities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('subproject' => $subprojectId));
        if (!empty($dbactivities)) {
            foreach ($dbactivities as $dbactivity) {
                $submitigationtechniqueid = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findOneBy(array('subproject' => $dbactivity->getSubproject()->getId(), 'basemitigationtechniqueid' => $dbactivity->getBasemitigationtechniqueid()));
                if ($dbactivity->getBasemitigationtechniqueid() != NULL && !empty($submitigationtechniqueid)) {
                    $dbactivity->setSubProjectMitigationTechniques($em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($submitigationtechniqueid->getId()));
                    $em->persist($dbactivity);
                }
            }
        }

        $em->flush();
        $em->clear();

        $statement = 'SELECT bs,bt,bmt,ba,bsa,bmc,bdt FROM AIECmsBundle:SubProjectSystems bs left join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems left join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats left join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques left join AIECmsBundle:BaseDatasheetType bsa WITH bsa.id = ba.subprojectdatasheet left join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.subprojectmetrics left join AIECmsBundle:BaseDatabaseType bdt WITH bdt.id = bmc.databasetype WHERE bs.subproject=:subprojectid ORDER BY bs.systemstitle ASC';

        $query = $em->createQuery($statement)->setParameter('subprojectid', $subprojectId);

        $subprojectmatrixProperties = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return array(
            'subprojectmatrixproperties' => $subprojectmatrixProperties,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/ajaxloaddatasheet", name="cms_subprojectmatrix_ajaxloaddatasheet")
     * @Method("POST")
     * @Template()
     */

    public function ajaxAction(Request $request)
    {

        if (! $request->isXmlHttpRequest())
        {
            throw new NotFoundHttpException();
        }

        $em = $this->getManager();

        $em->clear();

        $id = $request->query->get('metrics_id');

        $result = array();

        $qb = $em->createQueryBuilder();

        $query = $qb->select('bs')

            ->from('AIECmsBundle:BaseMetricsConfig', 'bm')
            ->innerJoin('AIECmsBundle:BaseDatabaseType', 'bd', 'WITH', 'bd.id = bm.databasetype')
            ->innerJoin('AIECmsBundle:BaseDatasheetType', 'bs', 'WITH', 'bs.id = bm.datasheettype');

        $qb->andWhere('bm.id = :uid')
            ->setParameter('uid', $id);

        $cities = $query->getQuery()->getResult();

        foreach ($cities as $city)
        {
            $result[$city->getTitle()] = $city->getId();
        }

        return new JsonResponse($result);

    }

    /**
     * Displays a form to create a new sub project Systems entity.
     *
     * @Route("/subprojectsystemsnew", name="cms_subprojectmatrix_subprojectsystems_new")
     * @Method("GET")
     * @Template()
     */
    public function subprojectsystemsAction()
    {
        $entity = new SubProjectSystems();
        $form   = $this->createCreateSubProjectSystemsForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/subprojectsytemscreate", name="cms_subprojectmatrix_systems_create")
     * @Method("POST")
     * @Template()
     */
    public function createsubprojectsystemsAction(Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectSystems();
        $form = $this->createCreateSubProjectSystemsForm($entity, $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
                $em = $this->getManager();

                $entity->setSubproject($em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId));

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
            }
            else
            {
                $this->addFlash('warning', 'Not permitted to perform action');
                return $this->redirect($this->generateUrl('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Update subproject systems
     *
     * @Route("/subprojectsystems/{id}/update", name="cms_subprojectmatrix_systems_update")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectsystemsAction(Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isXmlHttpRequest()) {

                $title = $request->request->get('title');

                $entity->setSystemsTitle($title);

                $em->persist($entity);
                $em->flush();

                return new Response(json_encode(array('result' => 'ok')));

            }
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

        return [
            'entity'    => $entity,
            'systemsId' => $id,
        ];
    }

    /**
     * Displays a form to create a new sub project Systems entity.
     *
     * @Route("/subprojectthreatsnew", name="cms_subprojectmatrix_threats_new")
     * @Method("GET")
     * @Template()
     */
    public function subprojectthreatsAction($systemId, $projectId, $subprojectId)
    {
        $entity = new SubProjectThreats();
        $form   = $this->createCreateSubProjectThreatsForm($systemId,$entity, $projectId, $subprojectId);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/{systemId}/subprojectthreatscreate", name="cms_subprojectmatrix_threats_create")
     * @Method("POST")
     * @Template()
     */
    public function createsubprojectthreatsAction($systemId, Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectThreats();
        $form = $this->createCreateSubProjectThreatsForm($systemId, $entity, $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

                $em = $this->getManager();
                $code = $form->getData()->getThreatsTitle();
                $cmsHelper = $this->get('aie_cms.cms.helper');
                $isUsed = $cmsHelper->isSubProjectThreatUsed($systemId, $code);
//            if ($isUsed) {
//                $this->get('session')->getFlashBag()->add('error', "Oops! This SubProject Threat ({$code}) is already in use.");
//            } else {
                $entity->setSubproject($em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId));

                $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($systemId);

                $entity->setSubProjectSystems($subprojectsystems);

                $em->persist($entity);
                $em->flush();

                // }

                return $this->redirect($this->generateURL('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
            }
            else
            {
                $this->addFlash('warning', 'Not permitted to perform action');
                return $this->redirect($this->generateURL('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
            }
        }

        return [
            'entity'    => $entity,
            'systemId' => $systemId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Update subproject threats
     *
     * @Route("/{systemId}/subprojectthreats/{id}/update", name="cms_subprojectmatrix_threats_update")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectthreatsAction($systemId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isXmlHttpRequest()) {

                $title = $request->request->get('title');

                $entity->setThreatsTitle($title);

                $em->persist($entity);
                $em->flush();

                return new Response(json_encode(array('result' => 'ok')));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

        return [
            'entity'    => $entity,
            'threatsId' => $id,
        ];
    }

    /**
     * Displays a form to create a new mitigation techniques entity.
     *
     * @Route("/subprojectmitigationtechniquesnew", name="cms_subprojectmatrix_mitigationtechniques_new")
     * @Method("GET")
     * @Template()
     */
    public function subprojectmitigationtechniquesAction($threatId, $projectId, $subprojectId)
    {
        $entity = new SubProjectMitigationTechniques();
        $form   = $this->createCreateSubProjectMitigationTechniquesForm($threatId, $entity, $projectId, $subprojectId);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/{threatId}/subprojectmitigationtechniquescreate", name="cms_subprojectmatrix_mitigationtechniques_create")
     * @Method("POST")
     * @Template()
     */
    public function createsubprojectmitigationtechniquesAction($threatId, Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectMitigationTechniques();
        $form = $this->createCreateSubProjectMitigationTechniquesForm($threatId, $entity, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
                $em = $this->getManager();

                $code = $form->getData()->getMitigationTechniquesTitle();
                $cmsHelper = $this->get('aie_cms.cms.helper');
                //$isUsed = $cmsHelper->isSubProjectMitigationTechniqueUsed($threatId, $code);

                $entity->setSubproject($em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId));

                $subprojectthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($threatId);
                $entity->setSubProjectThreats($subprojectthreats);
                $em->persist($entity);
                $em->flush();


                return new Response(json_encode(array('result' => $entity->getId())));
            }

        }

        return [
            'entity'    => $entity,
            'threatId' => $threatId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Update subproject mitigationtechniques
     *
     * @Route("/{threatId}/subprojectmitigationtechniques/{id}/update", name="cms_subprojectmatrix_mitigationtechniques_update")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectmitigationtechniquesAction($threatId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isXmlHttpRequest()) {

                $title = $request->request->get('title');

                $entity->setMitigationTechniquesTitle($title);

                $em->persist($entity);
                $em->flush();

                return new Response(json_encode(array('result' => 'ok')));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

        return [
            'entity'    => $entity,
            'mitigationtechniquesId' => $id,
        ];
    }

    /**
     * Displays a form to create a new SubProject Activity entity.
     *
     * @Route("/subprojectactivitynew", name="cms_subprojectmatrix_activity_new")
     * @Method("GET")
     * @Template()
     */
    public function subprojectactivityAction($threatId, $projectId, $subprojectId)
    {
        $entity = new SubProjectActivity();
        $form   = $this->createCreateActivityForm($threatId,$entity, $projectId, $subprojectId);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all SubProjectMatrixConfig entities.
     *
     * @Route("/{threatId}/subprojectactivitycreate", name="cms_subprojectmatrix_activity_create")
     * @Method("POST")
     * @Template()
     */
    public function createsubprojectactivityAction($threatId, Request $request, $projectId, $subprojectId)
    {
        $entity = new SubProjectActivity();
        $form = $this->createCreateActivityForm($threatId, $entity, $projectId, $subprojectId);
        $form->handleRequest($request);
        $em = $this->getManager();
        $params = $request->request->all();

        if ($form->isValid()) {
            if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
                if (!$form->isSubmitted()) {
                    $this->addFlash('error', 'Activity cannot be added, Please input valid data ');
                    return $this->redirect($this->generateURL('cms_subprojectmatrix'));
                }
                $data = $form->getData();

                if (!empty($form->get('mitigationtechniquestitle')->getViewData())) {
                    $entity = new SubProjectMitigationTechniques();
                    $code = $form->get('mitigationtechniquestitle')->getViewData();
                    $cmsHelper = $this->get('aie_cms.cms.helper');
                    //$isUsed = $cmsHelper->isSubProjectMitigationTechniqueUsed($threatId, $code);

                    $entity->setSubproject($em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId));

                    $subprojectthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($threatId);
                    $entity->setSubProjectThreats($subprojectthreats);
                    $entity->setMitigationTechniquesTitle($code);
                    $em->persist($entity);
                    $em->flush();
                    $em->refresh($entity);

                    $mitigationtechniqueId = $entity->getId();


                    $entity = new SubProjectActivity();

                    $datasheetid = $form->get('subprojectmetrics')->getData()->getId();

                    $subprojectdatasheet = $em->getRepository('AIECmsBundle:BaseDataSheetType')->find($data->getSubProjectdatasheet()->getId());
                    $entity->setSubProjectdatasheet($subprojectdatasheet);

                    $subprojectmetrics = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($datasheetid);

                    $entity->setSubProjectmetrics($subprojectmetrics);
                    $subprojectmitigations = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($mitigationtechniqueId);
                    $entity->setFrequency($data->getFrequency());
                    $entity->setActivityperson($data->getActivityperson());

                    $entity->setWeightage($data->getWeightage());
                    $entity->setConsequencesexcursion($data->getConsequencesexcursion());
                    $entity->setRemedialaction($data->getRemedialaction());
                    $entity->setManagementtitle($data->getManagementtitle());
                    $entity->setManagementgreen($data->getManagementgreen());
                    $entity->setManagementamber($data->getManagementamber());
                    $entity->setManagementred($data->getManagementred());
                    $entity->setControltitle($data->getControltitle());

                    $entity->setControlgreenexpression($data->getControlgreenexpression());
                    $entity->setControlgreen($data->getControlgreen());

                    $entity->setControlgreenexpressionrange($data->getControlgreenexpressionrange());
                    $entity->setControlgreenrange($data->getControlgreenrange());

                    $entity->setControlamberexpression($data->getControlamberexpression());
                    $entity->setControlamber($data->getControlamber());

                    $entity->setControlamberexpressionrange($data->getControlamberexpressionrange());
                    $entity->setControlamberrange($data->getControlamberrange());

                    $entity->setControlredexpression($data->getControlredexpression());
                    $entity->setControlred($data->getControlred());

                    $entity->setControlredexpressionrange($data->getControlredexpressionrange());
                    $entity->setControlredrange($data->getControlredrange());

                    $entity->setSubProjectMitigationTechniques($subprojectmitigations);
                    $entity->setSubproject($em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId));

                    if ($data->getControltitle() == "N/A") {
                        $entity->setControlgreenexpression("");
                        $entity->setControlgreen("N/A");
                        $entity->setControlgreenexpressionrange("");
                        $entity->setControlgreenrange("");

                        $entity->setControlamberexpression("");
                        $entity->setControlamber("N/A");
                        $entity->setControlamberexpressionrange("");
                        $entity->setControlamberrange("");

                        $entity->setControlredexpression("");
                        $entity->setControlred("N/A");
                        $entity->setControlredexpressionrange("");
                        $entity->setControlredrange("");
                    }
                    if (empty($data->getControlgreenrange())) {
                        $entity->setControlgreenexpressionrange("");
                    }
                    if (empty($data->getControlamberrange())) {
                        $entity->setControlamberexpressionrange("");
                    }
                    if (empty($data->getControlredrange())) {
                        $entity->setControlredexpressionrange("");
                    }
                    if ($data->getControlgreen() == "N/A") {
                        $entity->setControlgreenexpression("");
                        $entity->setControlgreenexpressionrange("");
                        $entity->setControlgreenrange("");
                    }
                    if ($data->getControlamber() == "N/A") {
                        $entity->setControlamberexpression("");
                        $entity->setControlamberexpressionrange("");
                        $entity->setControlamberrange("");
                    }
                    if ($data->getControlred() == "N/A") {
                        $entity->setControlredexpression("");
                        $entity->setControlredexpressionrange("");
                        $entity->setControlredrange("");
                    }
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateURL('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
                }
            }
            else
            {
                $this->addFlash('warning', 'Not permitted to perform action');
            }
        }

        return [
            'entity'    => $entity,
            'threatId'  => $threatId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Displays a editform to create a new SubProject Activity entity.
     *
     * @Route("/{mitigationtechniqueId}/subprojectactivity/{id}/edit", name="cms_subprojectmatrix_activity_edit")
     * @Method("GET")
     * @Template()
     */
    public function subprojectactivityeditAction($mitigationtechniqueId, $id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubProject Activity entity.');
        }
        $editform   = $this->createCreateActivityEditform($mitigationtechniqueId, $entity, $id, $projectId, $subprojectId);

        return array(
            'entity' => $entity,
            'editform'   => $editform->createView(),
        );
    }

    /**
     * Creates a editform to edit a SubProject Activity entity.
     *
     * @param SubProjectSystems $entity The entity
     *
     * @return \Symfony\Component\form\editform The editform
     */
    private function createCreateActivityEditform($mitigationtechniqueId, SubProjectActivity $entity, $id, $projectId, $subprojectId)
    {
        $em = $this->getManager();
        $editform = $this->createform(new SubProjectActivityEditType($em,$entity->getSubProjectmetrics()->getId()), $entity, array(
            'action' => $this->generateURL('cms_subprojectmatrix_activity_update',['mitigationtechniqueId' => $mitigationtechniqueId,'id' => $id, 'projectId' =>$projectId, 'subprojectId' =>$subprojectId]),
            'method' => 'POST',
        ));

        $editform->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'btn btn-primary btn-sm']], 'btn'));
        $editform->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm']], 'btn'));

        return $editform;
    }

    /**
     * Update subproject activity
     *
     * @Route("/{mitigationtechniqueId}/subprojectactivity/{id}/update", name="cms_subprojectmatrix_activity_update")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectactivityAction($mitigationtechniqueId, Request $request, $id, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubProject Activity entity.');
        }

        $editform = $this->createCreateActivityEditform($mitigationtechniqueId, $entity, $id, $projectId, $subprojectId);
        $editform->handleRequest($request);

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isMethod('POST')) {

                if ($editform->isSubmitted() && $editform->isValid()) {

                    $data = $editform->getData();

                    $subprojectdatasheet = $em->getRepository('AIECmsBundle:BaseDataSheetType')->find($data->getSubProjectdatasheet()->getId());

                    $entity->setSubProjectdatasheet($subprojectdatasheet);

                    $datasheetid = $editform->get('subprojectmetrics')->getData()->getId();

                    $subprojectmetrics = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($datasheetid);

                    $entity->setSubProjectmetrics($subprojectmetrics);
                    $subprojectmitigations = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($mitigationtechniqueId);
                    $entity->setSubProjectMitigationTechniques($subprojectmitigations);
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateURL('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
                }
            }
        }
        else
        {
            $this->addFlash('warning', 'No permission to perform the action');
            return $this->redirect($this->generateURL('cms_subprojectmatrix', ['projectId' => $projectId, 'subprojectId' => $subprojectId,]));
        }

        return [
            'entity'    => $entity,
            'activityId' => $id,
        ];
    }

    /**
     * Update subproject activity
     *
     * @Route("/{mitigationtechniqueId}/subprojectactivity/{id}/updatefields", name="cms_subprojectmatrix_activity_updatefields")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectactivityfieldsAction($mitigationtechniqueId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {

                $title = $request->request->get('title');

                $key = $request->request->get('key');

                if ($title) {
                    switch ($key) {
                        case "threshold_controltitle":
                            $entity->setControltitle($title);
                            break;
                        case "threshold_managementtitle":
                            $entity->setManagementtitle($title);
                            break;
                        case "threshold_frequency":
                            $entity->setFrequency($title);
                            break;
                        case "threshold_activityperson":
                            $entity->setActivityperson($title);
                            break;
                        case "threshold_weightage":
                            $entity->setWeightage($title);
                            break;
                        case "threshold_consequencesexcursion":
                            $entity->setConsequencesexcursion($title);
                            break;
                        case "threshold_remedialaction":
                            $entity->setRemedialaction($title);
                            break;
                        case "threshold_managementgreen":
                            $entity->setManagementgreen($title);
                            break;
                        case "threshold_managementamber":
                            $entity->setManagementamber($title);
                            break;
                        case "threshold_managementred":
                            $entity->setManagementred($title);
                            break;

                    }
                }


                $em->persist($entity);
                $em->flush();

                return new Response(json_encode(array('result' => 'ok','text' => $title)));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Update subproject activity
     *
     * @Route("/{mitigationtechniqueId}/subprojectactivity/{id}/updateexpressionfields", name="cms_subprojectmatrix_activity_updateexpressionfields")
     * @Method("POST")
     * @Template()
     */
    public function updatesubprojectactivityexpressionfieldsAction($mitigationtechniqueId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {

                $title = (!empty($request->request->get('title'))) ? $request->request->get('title') : "";
                $titleexpression = (!empty($request->request->get('titleexpression'))) ? $request->request->get('titleexpression') : "";

                $titlerange = (!empty($request->request->get('titlerange'))) ? $request->request->get('titlerange') : "";
                $titlerangeexpression = (!empty($request->request->get('titlerangeexpression'))) ? $request->request->get('titlerangeexpression') : "";

                $key = $request->request->get('key');

                if ($entity->getControltitle() == "N/A") {
                    if (is_numeric($title) || $title != "N/A") {
                        return new Response(json_encode(array('result' => 'notok')));
                    }
                }

                if (!$titlerange) $titlerangeexpression = "";
                if (!$title) $titleexpression = "";

                if (!is_numeric($title)) {
                    $title = strtoupper($title);
                }

                if (!is_numeric($titlerange)) {
                    $titlerange = strtoupper($titlerange);
                }

                // no control thresholds N/A then all is null
                if ($title == "N/A" && $titlerange != "N/A") {
                    $titleexpression = "";
                    $titlerangeexpression = "";
                    $titlerange = "";
                } else if ($title != "N/A" && $titlerange == "N/A") {
                    $titlerangeexpression = "";
                    $titlerange = "";
                }

                switch ($key) {
                    case "controlgreen":
                        $entity->setControlgreenexpression($titleexpression);
                        $entity->setControlgreen($title);
                        $entity->setControlgreenexpressionrange($titlerangeexpression);
                        $entity->setControlgreenrange($titlerange);
                        break;
                    case "controlamber":
                        $entity->setControlamberexpression($titleexpression);
                        $entity->setControlamber($title);
                        $entity->setControlamberexpressionrange($titlerangeexpression);
                        $entity->setControlamberrange($titlerange);
                        break;
                    case "controlred":
                        $entity->setControlredexpression($titleexpression);
                        $entity->setControlred($title);
                        $entity->setControlredexpressionrange($titlerangeexpression);
                        $entity->setControlredrange($titlerange);
                        break;
                }

//                if ($entity->getControltitle() == "N/A") {
//                    $entity->setControlgreenexpression("");
//                    $entity->setControlgreen("N/A");
//                    $entity->setControlgreenexpressionrange("");
//                    $entity->setControlgreenrange("");
//                    $entity->setControlamberexpression("");
//                    $entity->setControlamber("N/A");
//                    $entity->setControlamberexpressionrange("");
//                    $entity->setControlamberrange("");
//                    $entity->setControlredexpression("");
//                    $entity->setControlred("N/A");
//                    $entity->setControlredexpressionrange("");
//                    $entity->setControlredrange("");
//                }

                $em->persist($entity);
                $em->flush();
                $title = $title."<br/>";
                return new Response(json_encode(array('result' => 'ok','passid'=>$request->request->get('passid'), 'text' => $title,'textExpression' => $titleexpression,'textRange' => $titlerange,'textRangeExpression' => $titlerangeexpression)));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Creates a form to create a SubProject Systems entity.
     *
     * @param SubProjectSystems $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateSubProjectSystemsForm(SubProjectSystems $entity, $projectId, $subprojectId) {
        $form = $this->createForm(new SubProjectSystemsType(), $entity, array(
            'action' => $this->generateURL('cms_subprojectmatrix_systems_create',['projectId' =>$projectId,'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a SubProject Threats entity.
     *
     * @param SubProjectThreats $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateSubProjectThreatsForm($systemId, SubProjectThreats $entity, $projectId, $subprojectId)
    {
        $form = $this->createForm(new SubProjectThreatsType(), $entity, array(
            'action' => $this->generateURL('cms_subprojectmatrix_threats_create', ['systemId' => $systemId,'projectId' =>$projectId,'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a SubProject MitigationTechniques entity.
     *
     * @param SubProjectMitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateSubProjectMitigationTechniquesForm($threatId, SubProjectMitigationTechniques $entity, $projectId, $subprojectId)
    {
        $em = $this->getManager();
        $form = $this->createForm(new SubProjectMitigationTechniquesType($em), $entity, [
            'action' => $this->generateURL('cms_subprojectmatrix_mitigationtechniques_create', ['threatId' => $threatId,'projectId' =>$projectId,'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a SubProject Activity entity.
     *
     * @param SubProjectActivity $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateActivityForm($threatId, SubProjectActivity $entity, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $form = $this->createForm(new SubProjectActivityType($em), $entity, [
            'action' => $this->generateURL('cms_subprojectmatrix_activity_create', ['threatId' => $threatId,'projectId' =>$projectId,'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        //$form->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Update subproject systems
     *
     * @Route("/{id}/subprojectsystems/deleteconfirm", name="cms_subprojectmatrix_systems_deleteconfirm")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectsystemsconfirmAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($id);

            $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $systemsdata = $cmsDatabasesHelper->getSelectedSystemsExistOnDatabase($id);

            foreach($systemsdata as $data){
                if (isset($data[1]) && $data[1] ==1) $dbExistData[]='Cathodic Protection';
                if (isset($data[1]) && $data[1] ==2) $dbExistData[]='Chemical Dosage';
                if (isset($data[1]) && $data[1] ==3) $dbExistData[]='Corrosion Monitoring';
                if (isset($data[1]) && $data[1] ==4) $dbExistData[]='Sampling';
            }
            $arrSystems = array(
                'entity' => $entity,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'systemId'  => $id,
                'systemTitle'  => $entity->getSystemsTitle(),
            );

            if ($cmsDatabasesHelper->getSelectedSystemsExistOnDatabase($id)) {
                $arrSystems['dbExistData']=array_unique($dbExistData, SORT_REGULAR);
            }

            return $this->render('AIECmsBundle:SubProjectMatrixConfig:deletesystemsconfirm.html.twig', $arrSystems);
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Update subproject systems
     *
     * @Route("/{id}/subprojectsystems/delete", name="cms_subprojectmatrix_systems_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectsystemsAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {

            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($id);

            $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            if (empty($entity->getBasesystemid()))
            {
                $em->remove($entity);
                $em->flush();
                return new Response(json_encode(array('result' => 'ok')));
            }
            else
            {
                return new Response(json_encode(array('result' => 'notok')));
            }
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Update subproject threats
     *
     * @Route("/{id}/subprojectthreats/deleteconfirm", name="cms_subprojectmatrix_threats_deleteconfirm")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectthreatsconfirmAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $securityContext = $this->container->get('security.authorization_checker');
            return $this->render('AIECmsBundle:SubProjectMatrixConfig:deletethreatsconfirm.html.twig', array(
                'entity' => $entity,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'systemId'  => $entity->getSubProjectSystems()->getId(),
                'systemTitle'  => $entity->getSubProjectSystems()->getSystemsTitle(),
                'threatTitle'  => $entity->getThreatsTitle(),
                'threatId'  => $id
            ));
        }else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }


    /**
     * Update subproject threats
     *
     * @Route("/{id}/subprojectthreats/delete", name="cms_subprojectmatrix_threats_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectthreatsAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            if (empty($entity->getBasethreatid()))
            {
                $em->remove($entity);
                $em->flush();
                return new Response(json_encode(array('result' => 'ok')));
            }
            else
            {
                return new Response(json_encode(array('result' => 'notok')));
            }
        }else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Update subproject mitigationtechniques
     *
     * @Route("/{id}/subprojectmitigationtechniques/deleteconfirm", name="cms_subprojectmatrix_mitigationtechniques_deleteconfirm")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectmitigationtechniquesconfirmAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $activityentity = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOneBy(array('subprojectmitigationtechniques'=>$id));
            $selected =0;
            if ($entity->getIsselected() == 1 || $activityentity->getIsmanagement() == 1 || $activityentity->getIscontrol() ==1 || $activityentity->getIsnonmonitoringkpi()==1)
            {
                $selected = 1;
            }
            return $this->render('AIECmsBundle:SubProjectMatrixConfig:deletemitigationtechniquesconfirm.html.twig', array(
                'entity' => $entity,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'threatId'  => $entity->getSubProjectThreats()->getId(),
                'threatTitle'  => $entity->getSubProjectThreats()->getThreatsTitle(),
                'mitigationtechniqueId'  => $id,
                'mitigationtechniqueTitle'  => $entity->getMitigationTechniquesTitle(),
                'selected' => $selected,
            ));
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Update subproject mitigationtechniques
     *
     * @Route("/{id}/subprojectmitigationtechniques/delete", name="cms_subprojectmatrix_mitigationtechniques_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletesubprojectmitigationtechniquesAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest() && $this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }
            if (empty($entity->getBasemitigationtechniqueid()) && empty($entity->getBasethreatid()))
            {
                $em->remove($entity);
                $em->flush();
                return new Response(json_encode(array('result' => 'ok')));
            }
            else
            {
                return new Response(json_encode(array('result' => 'notok')));
            }
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    function rutime($ru, $rus, $index)
    {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }

}
