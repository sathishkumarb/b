<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\CathodicProtectionDatasheet;
use AIE\Bundle\CmsBundle\Entity\ControlActivity;
use AIE\Bundle\CmsBundle\Entity\CorrosionInhibitor;
use AIE\Bundle\CmsBundle\Entity\CorrosionInhibitorAvailability;
use AIE\Bundle\CmsBundle\Entity\GeneralCorrosion;
use AIE\Bundle\CmsBundle\Entity\PittingCorrosion;
use AIE\Bundle\CmsBundle\Entity\Production;
use AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet as CorrosionDatasheet;
use Aws\CloudFront\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * SubProjectCorrosionDatasheetController controller.
 *
 * @Route("/{projectId}/subprojectcorrosiondatasheet/{subprojectId}")
 */
class SubProjectCorrosionDatasheetController extends CmsBaseController
{
    Protected $showMitStatTrendDatasheetsBack = 0;
    Protected $showMitStatTrendDatasheetsDesc = 0;
    Protected $showMitStatTrendDatasheetsEdHiSh = 0;
    Protected $showMitStatTrendDatasheetsEdHiAdd = 0;
    Protected $showMitStatTrendDatasheetsEdHiEdit = 0;
    Protected $showMitStatTrendDatasheetsEdHiExcel = 0;
    Protected $showMitStatTrendDatasheetsEdHiDelete = 0;
    Protected $showMitStatTrendDatasheetsGrDetEdit = 0;
    Protected $showMitStatTrendDatasheetsGrSh = 0;
    /**
     * Lists all SubProjectCorrosionDatasheet entities.
     *
     * @Route("/{id}", name="cms_subproject_corrosion_datasheet")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $id, $projectId, $subprojectId)
    {
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();

        $em->flush();
        $em->clear();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $successMessage = $request->query->get('message');

        if($successMessage) $this->get('session')->getFlashBag()->add('success', 'Data saved');

        $corrosion_datasheet_locations = null;

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        $activityTitle = $this->activityTitle($activity);

        if (!$activity){
            $this->addFlash('error', 'No activity');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

        if (!$database){
            $this->addFlash('error', 'No metric linked to the activity database');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

        $chemicalActivities = null;
        $distype = null;
		$type = null;

        if (!$corrosion_type_entities) {
            $this->addFlash('error', 'No location[s] found for the activity');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        $nomax = 0;
        if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5 ) {
            $type = "g";
            $distype= "generalcorrosion";
        }
        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6 ) {
            $type = "p";
            $distype= "pittingcorrosion";
        }
		else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $distype= "controlactivity";
            $nomax=1;
        }
		else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $distype= "controlactivity";
            $nomax=1;
        }
        else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] == 8 ) {
            $type = "cp";
            $distype= "cathodicprotectiondatasheet";
        }
        else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] != 8 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 4 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
            $type = "ca-cd-ch";
            $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
            $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, $corrosion_type_entities['bdt_id'], $dbSystems['id']);
            $distype= "corrosioninhibitor";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 6) {
            $type = "pr";
            $distype= "production";
        }

        $corrosion_datasheet_all_locations = $cmsDatabasesHelper->getAllActivityLocations($id, $database->getDatabasetype()->getId(), $subprojectId);

        $corrosion_datasheet_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getDatasheetLocations($id, $database->getDatabasetype()->getId(), $subprojectId, $distype, $nomax);

        $datasheetLocationDetails = array();
        $datasheetAllLocationDetails = array();

        $templocation = null;
        $tempcouponholder = null;
        $tempplatform = null;
        $tempid = null;

        if (!empty($corrosion_datasheet_all_locations)) {

            foreach ($corrosion_datasheet_all_locations as $index => $datasheetAllLocationDetailstions) {

                $datasheetAllLocationDetails[$index]['actid'] = $id;
                $datasheetAllLocationDetails[$index]['id'] = $datasheetAllLocationDetailstions['id'];
                $datasheetAllLocationDetails[$index]['location'] = $datasheetAllLocationDetailstions['location'];
                if (isset($datasheetAllLocationDetailstions['platformorplant']) && !empty($datasheetAllLocationDetailstions['platformorplant'])) $datasheetAllLocationDetails[$index]['platformplant'] = $datasheetAllLocationDetailstions['platformorplant'];
                if ($datasheetAllLocationDetailstions['id']) $datasheetAllLocationDetails[$index]['locationid'] = $datasheetAllLocationDetailstions['id'];

                $datasheetAllLocationDetails[$index]['dateretrieval'] = "-";
                $datasheetAllLocationDetails[$index]['datasheet_id'] = 0;
                $datasheetAllLocationDetails[$index]['gasproductionmmscfday'] = "-";
                $datasheetAllLocationDetails[$index]['oilproductionbblday'] = "-";
                $datasheetAllLocationDetails[$index]['condensateproductionbblday'] = "-";
                $datasheetAllLocationDetails[$index]['waterproductionbblday'] = "-";
                $datasheetAllLocationDetails[$index]['totalproductionbblday'] = "-";
                $datasheetAllLocationDetails[$index]['watercutpercentage'] = "-";
                $datasheetAllLocationDetails[$index]['gorscfstb'] = "-";
                $datasheetAllLocationDetails[$index]['salinityppm'] = "-";
                $datasheetAllLocationDetails[$index]['htwosmolpercentage'] = "-";
                $datasheetAllLocationDetails[$index]['cotwomolpercentage'] = "-";
                $datasheetAllLocationDetails[$index]['usppsig'] = "-";
                $datasheetAllLocationDetails[$index]['dsppsig'] = "-";

                $datasheetAllLocationDetails[$index]['train'] = "-";
                $datasheetAllLocationDetails[$index]['fieldmanifold'] = "-";
                $datasheetAllLocationDetails[$index]['stationmanifold'] = "-";

                $datasheetAllLocationDetails[$index]['ustdegreecelsius'] = "-";
                $datasheetAllLocationDetails[$index]['espopsfrequencyhz'] = "-";
                $datasheetAllLocationDetails[$index]['espintakepressurepsig'] = "-";
                $datasheetAllLocationDetails[$index]['espdischargepressurepsig'] = "-";

                $datasheetAllLocationDetails[$index]['gas_production_mmscf_per_day'] = "-";
                $datasheetAllLocationDetails[$index]['total_production_bbl_per_day'] = "-";
                $datasheetAllLocationDetails[$index]['recommended_gas_dosage_litre_per_mmscf'] = "-";
                $datasheetAllLocationDetails[$index]['target_ci_dosage_litre_per_day'] = "-";
                $datasheetAllLocationDetails[$index]['target_ci_concentration_ppm'] = "-";
                $datasheetAllLocationDetails[$index]['actual_ci_dosage_litre_per_day'] = "-";
                $datasheetAllLocationDetails[$index]['actual_ci_concentration_ppm'] = "-";
                $datasheetAllLocationDetails[$index]['ci_percentage_availability'] = "-";
                $datasheetAllLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = "-";
                $datasheetAllLocationDetails[$index]['per_time_ci_target_reach_percentage'] = "-";

                $datasheetAllLocationDetails[$index]['control_activity_value'] = "-";
                $datasheetAllLocationDetails[$index]['controlactivityvalue'] = "-";
                $datasheetAllLocationDetails[$index]['control_activity_actual_value'] = "-";
                $datasheetAllLocationDetails[$index]['control_activity_achievement_value'] = "-";

                $datasheetAllLocationDetails[$index]['installationweight'] = "-";
                $datasheetAllLocationDetails[$index]['retrievalweight'] = "-";
                $datasheetAllLocationDetails[$index]['couponserialno'] = "-";
                $datasheetAllLocationDetails[$index]['dateretrieval'] = "-";
                $datasheetAllLocationDetails[$index]['dateinstalled'] = "-";

                $datasheetAllLocationDetails[$index]['generalcorrosionratemmpy'] = "-";
                $datasheetAllLocationDetails[$index]['averagecorrosionratemmpy'] = "-";
                $datasheetAllLocationDetails[$index]['averagecorrosionratempy'] = "-";

                $datasheetAllLocationDetails[$index]['depthpit'] = "-";
                $datasheetAllLocationDetails[$index]['couponserialno'] = "-";

                $datasheetAllLocationDetails[$index]['dateinstalled'] = "-";

                $datasheetAllLocationDetails[$index]['pittingratemmpy'] = "-";
                $datasheetAllLocationDetails[$index]['averagepittingratemmpy'] = "-";
                $datasheetAllLocationDetails[$index]['averagepittingratempy'] = "-";
                $datasheetAllLocationDetails[$index]['onpotential'] = "-";
                $datasheetAllLocationDetails[$index]['offpotential'] = "-";

                $datasheetAllLocationDetails[$index]['couponholdertagno'] = "-";
                $datasheetAllLocationDetails[$index]['coupontype'] = "-";


            }
        }

        if (!empty($corrosion_datasheet_locations))
        {
            $corrosion_datasheet_locations = array_merge($corrosion_datasheet_locations,$datasheetLocationDetails);

            //echo $corrosion_type_entities['bdt_id']."==".$corrosion_type_entities['bst_id'];

            foreach ($corrosion_datasheet_locations as $index => $datasheetLocationDetailstions)
            {
                $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions['datasheet_id']);

                if ( $corrosion_type_entities['bdt_id'] <= 6 )
                {
                    $datasheetLocationDetails[$index]['id'] = $datasheetLocationDetailstions['id'];
                    $datasheetLocationDetails[$index]['dbactivitylocationid'] = $datasheetLocationDetailstions['dbactivitylocationid'];
                    if (!empty($corrosion_locations)) {
                        if ($tempid != $corrosion_locations->getDbactivities())
                            $datasheetLocationDetails[$index]['id'] = $corrosion_locations->getDbactivities();
                        else
                            $datasheetLocationDetails[$index]['id'] = 0;
                    }


                    $datasheetLocationDetails[$index]['actid'] = $id;
                    $datasheetLocationDetails[$index]['platformplant'] = (!empty($datasheetLocationDetailstions['platformorplant']) ? $datasheetLocationDetailstions['platformorplant']: "");
                    $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];

                    if ($corrosion_type_entities['bdt_id'] == 3) {
                        $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                    }

                    $datasheetLocationDetails[$index]['state'] = true;

                    $datasheetLocationDetails[$index]['activityperformed'] = (!empty($datasheetLocationDetailstions['activityperformed']) ? $datasheetLocationDetailstions['activityperformed']: "");
                    $datasheetLocationDetails[$index]['datasheet_id'] = $datasheetLocationDetailstions['datasheet_id'];
                    $datasheetLocationDetails[$index]['locationid'] = $datasheetLocationDetailstions['dbactivitylocationid'];


                    if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'generalcorrosion', $id, $subprojectId);

                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetailstions['installed_date'] = new \DateTime($datasheetLocationDetailstions['installed_date']);

                        $datasheetLocationDetails[$index]['installationweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['installation_weight']);
                        $datasheetLocationDetails[$index]['retrievalweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['retrieval_weight']);
                        $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions['coupon_serial_no'];
                        $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions['retrieval_date']) ? $datasheetLocationDetailstions['retrieval_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions['installed_date']) ? $datasheetLocationDetailstions['installed_date']->format('d M Y') : "");

                        $datasheetLocationDetails[$index]['generalcorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['general_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagecorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['average_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagecorrosionratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['average_rate_mpy']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'pittingcorrosion', $id, $subprojectId);

                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetailstions['installed_date'] = new \DateTime($datasheetLocationDetailstions['installed_date']);

                        $datasheetLocationDetails[$index]['depthpit'] = $datasheetLocationDetailstions['depth_pit'];
                        $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions['coupon_serial_no'];
                        $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions['retrieval_date']) ? $datasheetLocationDetailstions['retrieval_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions['installed_date']) ? $datasheetLocationDetailstions['installed_date']->format('d M Y') : "");

                        $datasheetLocationDetails[$index]['pittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['pitting_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagepittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['average_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagepittingratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['average_rate_mpy']);

                    }
					else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);

                        $datasheetLocationDetailstions['date_retrieval'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['date_retrieval']->format('d M Y');
                        $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_achievement_value']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);
                        $datasheetLocationDetailstions['date_retrieval'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['date_retrieval']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_achievement_value']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'corrosioninhibitor', $id, $subprojectId);

                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['gas_production_mmscf_per_day'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['total_production_bbl_per_day'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['total_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['recommended_gas_dosage_litre_per_mmscf'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['recommended_gas_dosage_litre_per_mmscf']);
                        $datasheetLocationDetails[$index]['target_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['target_ci_dosage_litre_per_day']);
                        $datasheetLocationDetails[$index]['target_ci_concentration_ppm'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['target_ci_concentration_ppm']);
                        $datasheetLocationDetails[$index]['actual_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['actual_ci_dosage_litre_per_day']);
                        $datasheetLocationDetails[$index]['actual_ci_concentration_ppm'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['actual_ci_concentration_ppm']);
                        $datasheetLocationDetails[$index]['ci_percentage_availability'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['ci_percentage_availability']);
                        $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['vol_inject_vs_vol_req_percentage']);
                        $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['per_time_ci_target_reach_percentage']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);
                        $datasheetLocationDetailstions['date_retrieval'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['date_retrieval']->format('d M Y');
                        $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_achievement_value']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);
                        $datasheetLocationDetailstions['date_retrieval'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['date_retrieval']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_achievement_value']);

                    }
                    else if ($corrosion_type_entities['bdt_id'] == 4)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);
                        $datasheetLocationDetailstions['date_retrieval'] = new \DateTime($datasheetLocationDetailstions['date_retrieval']);

                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['date_retrieval']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] != 8 )
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'controlactivity', $id, $subprojectId);
                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['control_activity_value']);
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] == 8 )
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'cathodicprotectiondatasheet', $id, $subprojectId);
                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['onpotential'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['on_potential']);
                        $datasheetLocationDetails[$index]['offpotential'] = $this->removeTrailingAddZero($dateRetrievalExistsEntity[0]['off_potential']);
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 6)
                    {
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocationDetailstions['dbactivitylocationid'], $datasheetLocationDetailstions['date_retrieval'], 'production', $id, $subprojectId);
                        $datasheetLocationDetails[$index]['actid'] = $id;
                        $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                        $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['train'];
                        $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                        $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                        $datasheetLocationDetailstions['retrieval_date'] = new \DateTime($datasheetLocationDetailstions['retrieval_date']);
                        $datasheetLocationDetails[$index]['locationid'] = $datasheetLocationDetailstions['id'];
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['oil_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['condensate_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['water_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['total_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['watercutpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['water_cut_percentage']);
                        $datasheetLocationDetails[$index]['gorscfstb'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['gor_scf_stb']);
                        $datasheetLocationDetails[$index]['salinityppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['salinity_ppm']);
                        $datasheetLocationDetails[$index]['htwosmolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['h2s_mol_percentage']);
                        $datasheetLocationDetails[$index]['cotwomolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['co2_mol_percentage']);
                        $datasheetLocationDetails[$index]['usppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['usp_psig']);
                        $datasheetLocationDetails[$index]['dsppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['dsp_psig']);
                        $datasheetLocationDetails[$index]['ustdegreecelsius'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['ust_degree_celsius']);
                        $datasheetLocationDetails[$index]['espopsfrequencyhz'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['esp_ops_frequency_hz']);
                        $datasheetLocationDetails[$index]['espintakepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['esp_intake_pressure_psig']);
                        $datasheetLocationDetails[$index]['espdischargepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['esp_discharge_pressure_psig']);
                    }

                    if ($corrosion_type_entities['bdt_id'] == 3) {
                        if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                            $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                            $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                            $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                        } else $datasheetLocationDetails[$index]['coupontype'] = "";
                    }
                }

            }
        }

        $corrosion_datasheet_inhibitor_locations = array();

        if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3)
        {
            $corrosionDatasheetInhibitorLocations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestCorrosionAverageLocations($id, $subprojectId);

            foreach($corrosionDatasheetInhibitorLocations as $index => $locations)
            {

                $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetAverageByFilter($locations['dbactivitylocationid'], $locations['date_retrieval'], 'corrosioninhibitoravailability', $id, $subprojectId);
//                print_r($dateRetrievalExistsEntity);
                if ( $locations['from_date'] && $locations['from_date'] != '0000-00-00 00:00:00') {
                    $locations['from_date'] = new \DateTime($locations['from_date']);
                    $locations['from_date'] = $locations['from_date']->format('d M Y');
                }
                else{
                    $locations['from_date'] = "-";
                }
                $locations['to_date'] = new \DateTime($locations['to_date']);
                $corrosion_datasheet_inhibitor_locations[$index]['id'] = $locations['dbactivitylocationid'];
                $corrosion_datasheet_inhibitor_locations[$index]['from_date'] = $locations['from_date'];
                $corrosion_datasheet_inhibitor_locations[$index]['to_date'] = $locations['to_date']->format('d M Y');
                $corrosion_datasheet_inhibitor_locations[$index]['corrosion_inhibitor_percentage_availability'] =  $this->removeTrailingAddZero($locations["corrosion_inhibitor_percentage_availability"]);
                $corrosion_datasheet_inhibitor_locations[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($locations["vol_inject_vs_vol_req_percentage"]);
                $corrosion_datasheet_inhibitor_locations[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($locations["per_time_ci_target_reach_percentage"]);

                $corrosion_datasheet_inhibitor_locations[$index]['location'] = $locations['location'];
                $corrosion_datasheet_inhibitor_locations[$index]['platformplant'] = $locations['platformorplant'];

            }
        }

        if ($corrosion_type_entities['bdt_id'] == 5 || $corrosion_type_entities['bdt_id'] == 3) rsort($datasheetLocationDetails); //else rsort($datasheetLocationDetails);

        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_BACK',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsBack = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DESCRIPTION_SAVE',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsDesc = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DESCRIPTION_SHOW',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsDesc = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_SHOW',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsEdHiSh = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_ADD',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsEdHiAdd = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EDIT',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsEdHiEdit = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EXCEL',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsEdHiExcel = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_DELETE',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsEdHiDelete = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_GRAPH_EDIT',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsGrDetEdit = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_GRAPH_SHOW',$this->userRolesMergeToCheck)){
            $this->showMitStatTrendDatasheetsGrSh = 1;
        }

        return array(
            //'corrosion_datasheet_locations' => json_encode($datasheetLocationDetails, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
            'corrosion_datasheet_locations'                => array_merge($datasheetAllLocationDetails,$datasheetLocationDetails),
            'corrosion_datasheet_inhibitor_locations'      => $corrosion_datasheet_inhibitor_locations,
            'projectId'                                    => $projectId,
            'subprojectId'                                 => $subprojectId,
            'project'                                      => $project,
            'subproject'                                   => $subproject,
            'activityId'                                   => $id,
            'corrosionType'                                => $type,
            'corrosion_activity_entities'                  => $corrosion_type_entities,
            'chemicalActivities'                           => $chemicalActivities,
            'activityTitle'                                => $activityTitle,
            'showMitStatTrendDatasheetsBack'               => $this->showMitStatTrendDatasheetsBack,
            'showMitStatTrendDatasheetsDesc'               => $this->showMitStatTrendDatasheetsDesc,
            'showMitStatTrendDatasheetsEdHiSh'             => $this->showMitStatTrendDatasheetsEdHiSh,
            'showMitStatTrendDatasheetsEdHiAdd'            => $this->showMitStatTrendDatasheetsEdHiAdd,
            'showMitStatTrendDatasheetsEdHiEdit'           => $this->showMitStatTrendDatasheetsEdHiEdit,
            'showMitStatTrendDatasheetsEdHiExcel'          => $this->showMitStatTrendDatasheetsEdHiExcel,
            'showMitStatTrendDatasheetsEdHiDelete'         => $this->showMitStatTrendDatasheetsEdHiDelete,
            'showMitStatTrendDatasheetsGrDetEdit'          => $this->showMitStatTrendDatasheetsGrDetEdit,
            'showMitStatTrendDatasheetsGrSh'               => $this->showMitStatTrendDatasheetsGrSh,
            'systemId'                                     => $request->query->get('systemId'),
            'pageType'                                     => $distype
        );

    }

    /**
     * Lists all SubProjectCorrosionDatasheet entities.
     *
     * @Route("/{id}/{types}/new", name="cms_subproject_corrosion_datasheet_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request, $id, $types, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $datasheetLocationsToRetrieve = $request->query->get('dataRetrieve');

        if (strstr($datasheetLocationsToRetrieve, ',', true)) $datasheetLocationsToRetrieve = explode(",",$datasheetLocationsToRetrieve);
        else {
            $datasheetLocationsToRetrieve = array(0=>$datasheetLocationsToRetrieve);
        }

        $corrosion_datasheet_locations = null;

        $mergedLocationsData = array();
        $datasheetLocationDetails = array();
        $datasheetLocationDetailst = array();

        $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        $activityTitle = "";

        $activityTitle = $this->activityTitle($activity);

        $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

        $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

        if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5 ) {
            $type = "g";
            $dbname = "SubProjectCorrosionMonitoringDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6 ) {
            $type = "p";
            $dbname = "SubProjectCorrosionMonitoringDatabase";
        }
		else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $dbname = "SubProjectCorrosionMonitoringDatabase";
        }
		else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $dbname = "SubProjectCorrosionMonitoringDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
            $type = "ca-cd-ch";
            $dbname = "SubProjectChemicalDosageDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $dbname = "SubProjectChemicalDosageDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $dbname = "SubProjectChemicalDosageDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 4) {
            $type = "ca";
            $dbname = "SubProjectSamplingDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 1 ) {
            if ($corrosion_type_entities['bst_id'] == 8) {
                $type = "cp";
            }
            else{
                $type = "ca";
            }
            $dbname = "SubProjectCathodicProtectionDatabase";
        }
        else if ($corrosion_type_entities['bdt_id'] == 6) {
            $type = "pr";
            $dbname = "";
        }

        if ($corrosion_type_entities['bdt_id'] <=4 )
        {
            if (!empty($datasheetLocationsToRetrieve[0])) {
                $corrosion_datasheet_locations = $em->getRepository('AIECmsBundle:' . $dbname)->findById($datasheetLocationsToRetrieve);
            }
            else
                $corrosion_datasheet_locations = $em->getRepository('AIECmsBundle:' . $dbname)->findBy(array('activityid' => $id, 'subproject' => $subprojectId));

            $datasheetLocationDetails[0]['platformplant'] = "";
            $datasheetLocationDetails[0]['id'] = "";
            $datasheetLocationDetails[0]['location'] = "";
            $datasheetLocationDetails[0]['couponholdertagnumber'] = "";

            if (!empty($corrosion_datasheet_locations))
            {

                foreach ($corrosion_datasheet_locations as $index => $datasheetLocationDetailstions)
                {
                    //$datasheetLocationDetails[$index]['dateretrieval'] = 'DD-MM-YYYY';
                    $datasheetLocationDetails[$index]['platformplant'] = $datasheetLocationDetailstions->getPlatformorplant();
                    $datasheetLocationDetails[$index]['id'] = $datasheetLocationDetailstions->getId();
                    $datasheetLocationDetails[$index]['actid'] = $datasheetLocationDetailstions->getId();
                    $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions->getLocation();
                    if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] > 4 && $corrosion_type_entities['bst_id'] < 7 ) {

                        //$datasheetLocationDetails[$index]['dateinstalled'] = 'DD-MM-YYYY';
                        $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions->getCouponholdertagno();
                        if ($datasheetLocationDetailstions->getCoupontypeandareaid()) {
                            $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions->getCoupontypeandareaid());
                            $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                            $datasheetLocationDetails[$index]['coupontypearea'] = $corrosion_coupon_data->getColIdTwoValue();
                            $coupontypearea = $corrosion_coupon_data->getColIdTwoValue();
                        }
                        else
                        {
                            $datasheetLocationDetails[$index]['coupontype'] = "";
                            $datasheetLocationDetails[$index]['coupontypearea'] = "";
                            $coupontypearea = "";
                        }
                        if ($datasheetLocationDetailstions->getCouponmaterialanddensityid()) {
                            $corrosion_coupon_density_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions->getCouponmaterialanddensityid());
                            $datasheetLocationDetails[$index]['coupontypedensity'] = $corrosion_coupon_density_data->getColIdTwoValue();
                            $coupontypedensity = $corrosion_coupon_density_data->getColIdTwoValue();
                        }
                        else
                        {
                            $datasheetLocationDetails[$index]['coupontypedensity'] = "";
                            $coupontypedensity = "";
                        }
                    }

                }
                if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] > 4 &&  $corrosion_type_entities['bst_id'] < 7 )
                {

                    foreach ($corrosion_datasheet_locations as $index => $datasheetLocationDetailstions)
                    {

                        $datasheetLocationDetailst[$index]['platformplant'] = $datasheetLocationDetailstions->getPlatformorplant();
                        $datasheetLocationDetailst[$index]['id'] = $datasheetLocationDetailstions->getId();
                        $datasheetLocationDetailst[$index]['actid'] = $datasheetLocationDetailstions->getId();
                        $datasheetLocationDetailst[$index]['location'] = $datasheetLocationDetailstions->getLocation();

                        $datasheetLocationDetailst[$index]['couponholdertagno'] = $datasheetLocationDetailstions->getCouponholdertagno();

                        if ($datasheetLocationDetailstions->getCoupontypeandareaid()) {
                            $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions->getCoupontypeandareaid());
                            $datasheetLocationDetailst[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                            $datasheetLocationDetails[$index]['coupontypearea'] = $corrosion_coupon_data->getColIdTwoValue();
                            $coupontypearea = $corrosion_coupon_data->getColIdTwoValue();
                        }
                        else {
                            $datasheetLocationDetailst[$index]['coupontype'] = "";
                            $datasheetLocationDetails[$index]['coupontypearea'] = "";
                        }

                        if ($datasheetLocationDetailstions->getCouponmaterialanddensityid()) {
                            $corrosion_coupon_density_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions->getCouponmaterialanddensityid());
                            $datasheetLocationDetails[$index]['coupontypedensity'] = $corrosion_coupon_density_data->getColIdTwoValue();
                            $coupontypedensity = $corrosion_coupon_density_data->getColIdTwoValue();
                        }
                        else {
                            $datasheetLocationDetails[$index]['coupontypedensity'] = "";
                        }

                    }


                }
                $mergedLocationsData = array_merge($datasheetLocationDetails, $datasheetLocationDetailst);

                usort($mergedLocationsData, function ($a, $b)
                {
                    return strcmp($a["location"], $b["location"]);
                });
            }
        }

        return array(
            'corrosion_datasheet_locations' => json_encode($mergedLocationsData, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
            //'form'                        => $form->createView(),
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'activityId'                  => $id,
            'corrosionType'               => $type,
            'activity'                    => $activity,
            'pageType'                    => $types,
            'activityTitle'               => $activityTitle,
            'productionLocation'          => $datasheetLocationsToRetrieve[0],
            'systemId'                    => $request->query->get('systemId'),
        );

    }

    /**
     * Update SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/edit", name="cms_subproject_corrosion_datasheet_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $id, $types, $projectId, $subprojectId)
    {
            set_time_limit(0);
            if ($this->container->has('profiler')) {
                $this->container->get('profiler')->disable();
            }

            $em = $this->getManager();
            $em->flush();
            $em->clear();

            $em->getConnection()->getConfiguration()->setSQLLogger(null);

            $datasheetLocationsToRetrieve = $request->query->get('dataRetrieve');

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            $successMessage = $request->query->get('message');

            if ($successMessage) $this->get('session')->getFlashBag()->add('success', 'Data saved');

            if (strstr($datasheetLocationsToRetrieve, ',', true)) $datasheetLocationsToRetrieve = explode(",", $datasheetLocationsToRetrieve);
            else {
                $datasheetLocationsToRetrieve = array(0 => $datasheetLocationsToRetrieve);
            }

            $activityTitle = "";

            if ($request && $datasheetLocationsToRetrieve) {

                $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

                $activityTitle = $this->activityTitle($activity);

                $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

                $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

                if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5) {
                    $type = "g";
                    $dbname = "SubProjectCorrosionMonitoringDatabase";
                } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6) {
                    $type = "p";
                    $dbname = "SubProjectCorrosionMonitoringDatabase";
                } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
					$type = "ca-cd-oh";
					$dbname = "SubProjectCorrosionMonitoringDatabase";
				} else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
					$type = "ca";
					$dbname = "SubProjectCorrosionMonitoringDatabase";
				} else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3) {
                    $type = "ca-cd-ch";
                    $dbname = "SubProjectChemicalDosageDatabase";
                } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {
                    $type = "ca-cd-oh";
                    $dbname = "SubProjectChemicalDosageDatabase";
                    $types = "controlactivity";
                } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {
                    $type = "ca";
                    $dbname = "SubProjectChemicalDosageDatabase";
                    $types = "controlactivity";
                } else if ($corrosion_type_entities['bdt_id'] == 4) {
                    $type = "ca";
                    $dbname = "SubProjectSamplingDatabase";
                } else if ($corrosion_type_entities['bdt_id'] == 1) {
                    if ($corrosion_type_entities['bst_id'] == 8) {
                        $type = "cp";
                    } else {
                        $type = "ca";
                    }
                    $dbname = "SubProjectCathodicProtectionDatabase";
                } else if ($corrosion_type_entities['bdt_id'] == 6) {
                    $type = "pr";
                    $dbname = "";
                }

                $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

                $datasheetLocationDetails = array();

                if (!empty($corrosionentities)) {

                    foreach ($corrosionentities as $index => $datasheetLocationDetailstions) {

                        if (isset($datasheetLocationDetailstions['platformorplant']) && !empty($datasheetLocationDetailstions['platformorplant']))
                        {
                            $datasheetLocationDetails[$index]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        }
                        $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                        $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions[0]['id']);

                        $datasheetLocationDetails[$index]['id'] = $corrosion_locations->getId();
                        $datasheetLocationDetails[$index]['actlocationid'] = $corrosion_locations->getDbactivities();
                        $datasheetLocationDetails[$index]['actid'] = $id;

                        if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5) {
                            $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                            if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                                $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                                $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                            } else $datasheetLocationDetails[$index]['coupontype'] = "";
                            $datasheetLocationDetails[$index]['installationweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['installation_weight']);
                            $datasheetLocationDetails[$index]['retrievalweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['retrieval_weight']);
                            $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                            $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                            $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                            $datasheetLocationDetails[$index]['generalcorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['general_rate_mmpy']);
                            $datasheetLocationDetails[$index]['averagecorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                            $datasheetLocationDetails[$index]['averagecorrosionratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);
                        } 
						elseif ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6) {
                            $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                            if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                                $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                                $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                            } else $datasheetLocationDetails[$index]['coupontype'] = "";
                            $datasheetLocationDetails[$index]['depthpit'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['depth_pit']);
                            $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                            $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                            $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                            $datasheetLocationDetails[$index]['pittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['pitting_rate_mmpy']);
                            $datasheetLocationDetails[$index]['averagepittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                            $datasheetLocationDetails[$index]['averagepittingratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);
                        } 
						else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3) {

                            if ($types == "average") {
                                $datasheetLocationDetails[$index]['from_date'] = (!empty($datasheetLocationDetailstions[0]['from_date']) ? $datasheetLocationDetailstions[0]['from_date']->format('d M Y') : "");
                                $datasheetLocationDetails[$index]['to_date'] = $datasheetLocationDetailstions[0]['to_date']->format('d M Y');
                                $datasheetLocationDetails[$index]['corrosion_inhibitor_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["corrosion_inhibitor_percentage_availability"]);
                                $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["vol_inject_vs_vol_req_percentage"]);
                                $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["per_time_ci_target_reach_percentage"]);

                            } else {

                                $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                                $datasheetLocationDetails[$index]['gas_production_mmscf_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gas_production_mmscf_per_day']);
                                $datasheetLocationDetails[$index]['total_production_bbl_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['total_production_bbl_per_day']);
                                $datasheetLocationDetails[$index]['recommended_gas_dosage_litre_per_mmscf'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['recommended_gas_dosage_litre_per_mmscf']);
                                $datasheetLocationDetails[$index]['target_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_dosage_litre_per_day']);
                                $datasheetLocationDetails[$index]['target_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_concentration_ppm']);
                                $datasheetLocationDetails[$index]['actual_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_dosage_litre_per_day']);
                                $datasheetLocationDetails[$index]['actual_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_concentration_ppm']);
                                $datasheetLocationDetails[$index]['ci_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['ci_percentage_availability']);
                                $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['vol_inject_vs_vol_req_percentage']);
                                $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['per_time_ci_target_reach_percentage']);
                            }

                        } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4) {

                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                            $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                            $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                        } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2) {

                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                            $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                            $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                        } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {

                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                            $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                            $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                        } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {

                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                            $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                            $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                        } elseif ($corrosion_type_entities['bdt_id'] == 4) {
                            // $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        } else if ($corrosion_type_entities['bst_id'] != 8 && $corrosion_type_entities['bdt_id'] == 1) {
                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        } else if ($corrosion_type_entities['bst_id'] == 8 && $corrosion_type_entities['bdt_id'] == 1) {
                            $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['onpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['on_potential']);
                            $datasheetLocationDetails[$index]['offpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['off_potential']);
                        } else if ($corrosion_type_entities['bdt_id'] == 6) {

                            if (isset($datasheetLocationDetailstions['retrieval_date']) && !empty($datasheetLocationDetailstions['retrieval_date'])) {
                                $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                            }
                            $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['train'];
                            $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                            $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                            if (isset($datasheetLocationDetailstions['gasproductionmmscfday']) && !empty($datasheetLocationDetailstions['gasproductionmmscfday'])) {
                                $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['gas_production_mmscf_per_day']);
                            }
                            if (isset($datasheetLocationDetailstions['oilproductionbblday']) && !empty($datasheetLocationDetailstions['oilproductionbblday'])) {
                                $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['oilproductionbblday']);
                            }
                            if (isset($datasheetLocationDetailstions['condensateproductionbblday']) && !empty($datasheetLocationDetailstions['condensateproductionbblday'])) {
                                $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['condensateproductionbblday']);
                            }
                            if (isset($datasheetLocationDetailstions['waterproductionbblday']) && !empty($datasheetLocationDetailstions['waterproductionbblday'])) {
                                $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['waterproductionbblday']);
                            }
                            if (isset($datasheetLocationDetailstions['totalproductionbblday']) && !empty($datasheetLocationDetailstions['totalproductionbblday'])) {
                                $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['totalproductionbblday']);
                            }

                        }

                    }
                }

                if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_DELETE',$this->userRolesMergeToCheck)){
                    $this->showMitStatTrendDatasheetsEdHiDelete = 1;
                }

                if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EDIT',$this->userRolesMergeToCheck)){
                    $this->showMitStatTrendDatasheetsEdHiEdit = 1;
                }

                if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EXCEL',$this->userRolesMergeToCheck)){
                    $this->showMitStatTrendDatasheetsEdHiExcel = 1;
                }

                return [
                    'corrosion_datasheet_locations' => $datasheetLocationDetails,
                    'projectId' => $projectId,
                    'subprojectId' => $subprojectId,
                    'subproject' => $subproject,
                    'activityId' => $id,
                    'activityTitle' => $activityTitle,
                    'corrosionType' => $type,
                    'pageType' => $types,
                    'dataRetrieve' => $request->query->get('dataRetrieve'),
                    'showMitStatTrendDatasheetsEdHiDelete' => $this->showMitStatTrendDatasheetsEdHiDelete,
                    'showMitStatTrendDatasheetsEdHiEdit' => $this->showMitStatTrendDatasheetsEdHiEdit,
                    'showMitStatTrendDatasheetsEdHiExcel' => $this->showMitStatTrendDatasheetsEdHiExcel,
                    'systemId' => $request->query->get('systemId'),
                ];
            }

    }
    
    /**
     * Update SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/exportexcelrecent", name="cms_subproject_corrosion_datasheet_exportexcelrecent")
     * @Method("POST")
     * @Template()
     */
    public function exportexcelrecentAction(Request $request, $id, $types, $projectId, $subprojectId)
    {

        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();

        $em->flush();
        $em->clear();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $corrosion_datasheet_locations = null;

        $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        $activityTitle = $this->activityTitle($activity);

        if (!$activity){
            $this->addFlash('error', 'No activity');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

        if (!$database){
            $this->addFlash('error', 'No metric linked to the activity database');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

        $chemicalActivities = null;
        $distype = null;
        $type = null;

        if (!$corrosion_type_entities){
            $this->addFlash('error', 'No location[s] found for the activity');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        $datasheetLocationsToRetrieve = $request->query->get('dataRetrieve');

        if (strstr($datasheetLocationsToRetrieve, ',', true))
        {
            $datasheetLocationsToRetrieve = explode(",", $datasheetLocationsToRetrieve);
        }
        else
        {
            $datasheetLocationsToRetrieve = array(0 => $datasheetLocationsToRetrieve);
        }

        $nomax = 0;
        if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5 ) {
            $type = "g";
            $distype= "generalcorrosion";
        }
        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6 ) {
            $type = "p";
            $distype= "pittingcorrosion";
        }
        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $distype= "controlactivity";
            $nomax=1;
        }
        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $distype= "controlactivity";
            $nomax=1;
        }
        else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] == 8 ) {
            $type = "cp";
            $distype= "cathodicprotectiondatasheet";
        }
        else if ($corrosion_type_entities['bdt_id'] == 1 && $corrosion_type_entities['bst_id'] != 8 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 4 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
            $type = "ca-cd-ch";
            $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
            $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, $corrosion_type_entities['bdt_id'], $dbSystems['id']);
            $distype= "corrosioninhibitor";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
            $type = "ca-cd-oh";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2 ) {
            $type = "ca";
            $distype= "controlactivity";
        }
        else if ($corrosion_type_entities['bdt_id'] == 6) {
            $type = "pr";
            $distype= "production";
        }

        $corrosion_datasheet_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);
        //$corrosion_datasheet_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocationByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

        $datasheetLocationDetails = array();

        $templocation = null;
        $tempcouponholder = null;
        $tempplatform = null;
        $tempid = null;

        $datasheetLocationDetails = array();

        if (!empty($corrosion_datasheet_locations)) {

            foreach ($corrosion_datasheet_locations as $index => $datasheetLocationDetailstions) {

                if (isset($datasheetLocationDetailstions['platformorplant']) && !empty($datasheetLocationDetailstions['platformorplant']))
                {
                    $datasheetLocationDetails[$index]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                }
                $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions[0]['id']);

                $datasheetLocationDetails[$index]['id'] = $corrosion_locations->getId();
                $datasheetLocationDetails[$index]['actlocationid'] = $corrosion_locations->getDbactivities();
                $datasheetLocationDetails[$index]['actid'] = $id;
                $datasheetLocationDetails[$index]['activityperformed'] = $corrosion_locations->getActivityPerformed();

                if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5) {
                    $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                    if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                        $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                        $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                    } else $datasheetLocationDetails[$index]['coupontype'] = "";
                    $datasheetLocationDetails[$index]['installationweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['installation_weight']);
                    $datasheetLocationDetails[$index]['retrievalweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['retrieval_weight']);
                    $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                    $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                    $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                    $datasheetLocationDetails[$index]['generalcorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['general_rate_mmpy']);
                    $datasheetLocationDetails[$index]['averagecorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                    $datasheetLocationDetails[$index]['averagecorrosionratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);
                }
                elseif ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6) {
                    $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                    if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                        $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                        $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                    } else $datasheetLocationDetails[$index]['coupontype'] = "";
                    $datasheetLocationDetails[$index]['depthpit'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['depth_pit']);
                    $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                    $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                    $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                    $datasheetLocationDetails[$index]['pittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['pitting_rate_mmpy']);
                    $datasheetLocationDetails[$index]['averagepittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                    $datasheetLocationDetails[$index]['averagepittingratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);
                }
                else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3) {

                    if ($types == "average") {
                        $datasheetLocationDetails[$index]['from_date'] = (!empty($datasheetLocationDetailstions[0]['from_date']) ? $datasheetLocationDetailstions[0]['from_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['to_date'] = $datasheetLocationDetailstions[0]['to_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['corrosion_inhibitor_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["corrosion_inhibitor_percentage_availability"]);
                        $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["vol_inject_vs_vol_req_percentage"]);
                        $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["per_time_ci_target_reach_percentage"]);

                    } else {

                        $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['gas_production_mmscf_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['total_production_bbl_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['total_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['recommended_gas_dosage_litre_per_mmscf'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['recommended_gas_dosage_litre_per_mmscf']);
                        $datasheetLocationDetails[$index]['target_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_dosage_litre_per_day']);
                        $datasheetLocationDetails[$index]['target_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_concentration_ppm']);
                        $datasheetLocationDetails[$index]['actual_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_dosage_litre_per_day']);
                        $datasheetLocationDetails[$index]['actual_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_concentration_ppm']);
                        $datasheetLocationDetails[$index]['ci_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['ci_percentage_availability']);
                        $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['vol_inject_vs_vol_req_percentage']);
                        $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['per_time_ci_target_reach_percentage']);
                    }

                } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4) {

                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                    $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                    $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2) {

                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                    $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                    $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {

                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                    $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                    $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {

                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                    $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                    $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                } elseif ($corrosion_type_entities['bdt_id'] == 4) {
                    // $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                } else if ($corrosion_type_entities['bst_id'] != 8 && $corrosion_type_entities['bdt_id'] == 1) {
                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                } else if ($corrosion_type_entities['bst_id'] == 8 && $corrosion_type_entities['bdt_id'] == 1) {
                    $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                    $datasheetLocationDetails[$index]['onpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['on_potential']);
                    $datasheetLocationDetails[$index]['offpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['off_potential']);
                } else if ($corrosion_type_entities['bdt_id'] == 6) {

                    if (isset($datasheetLocationDetailstions['retrieval_date']) && !empty($datasheetLocationDetailstions['retrieval_date'])) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                    }
                    $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['train'];
                    $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                    $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                    if (isset($datasheetLocationDetailstions['gasproductionmmscfday']) && !empty($datasheetLocationDetailstions['gasproductionmmscfday'])) {
                        $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['gas_production_mmscf_per_day']);
                    }
                    if (isset($datasheetLocationDetailstions['oilproductionbblday']) && !empty($datasheetLocationDetailstions['oilproductionbblday'])) {
                        $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['oilproductionbblday']);
                    }
                    if (isset($datasheetLocationDetailstions['condensateproductionbblday']) && !empty($datasheetLocationDetailstions['condensateproductionbblday'])) {
                        $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['condensateproductionbblday']);
                    }
                    if (isset($datasheetLocationDetailstions['waterproductionbblday']) && !empty($datasheetLocationDetailstions['waterproductionbblday'])) {
                        $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['waterproductionbblday']);
                    }
                    if (isset($datasheetLocationDetailstions['totalproductionbblday']) && !empty($datasheetLocationDetailstions['totalproductionbblday'])) {
                        $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['totalproductionbblday']);
                    }

                }

            }
        }

        $corrosion_datasheet_inhibitor_locations = array();

        if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3)
        {
            $corrosionDatasheetInhibitorLocations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestCorrosionAverageLocations($id, $subprojectId);

            if (count($corrosionDatasheetInhibitorLocations)) {
                foreach ($corrosionDatasheetInhibitorLocations as $index => $locations) {
                    //$corrosioDatasheetInhibitorLocations = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->find($locations->getDbactivities());
                    if ($locations['from_date'] && $locations['from_date'] != '0000-00-00 00:00:00') {
                        $locations['from_date'] = new \DateTime($locations['from_date']);
                        $locations['from_date'] = $locations['from_date']->format('d M Y');
                    } else {
                        $locations['from_date'] = "-";
                    }

                    $locations['to_date'] = new \DateTime($locations['to_date']);
                    $corrosion_datasheet_inhibitor_locations[$index]['id'] = $locations['dbactivitylocationid'];
                    $corrosion_datasheet_inhibitor_locations[$index]['from_date'] = $locations['from_date'];
                    $corrosion_datasheet_inhibitor_locations[$index]['to_date'] = $locations['to_date']->format('d M Y');
                    $corrosion_datasheet_inhibitor_locations[$index]['corrosion_inhibitor_percentage_availability'] = $this->removeTrailingAddZero($locations["corrosion_inhibitor_percentage_availability"]);
                    $corrosion_datasheet_inhibitor_locations[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($locations["vol_inject_vs_vol_req_percentage"]);
                    $corrosion_datasheet_inhibitor_locations[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($locations["per_time_ci_target_reach_percentage"]);

                    $corrosion_datasheet_inhibitor_locations[$index]['location'] = $locations['location'];
                    $corrosion_datasheet_inhibitor_locations[$index]['platformplant'] = $locations['platformorplant'];

                }

            }
            if ($types=="average") $datasheetLocationDetails = $corrosion_datasheet_inhibitor_locations;
        }

        if ($corrosion_type_entities['bdt_id'] == 5 || $corrosion_type_entities['bdt_id'] == 3) rsort($datasheetLocationDetails); //else rsort($datasheetLocationDetails);


        return array(
            'corrosion_datasheet_locations'                => $datasheetLocationDetails,
            'projectId'                                    => $projectId,
            'subprojectId'                                 => $subprojectId,
            'project'                                      => $project,
            'subproject'                                   => $subproject,
            'activityId'                                   => $id,
            'corrosionType'                                => $type,
            'corrosion_activity_entities'                  => $corrosion_type_entities,
            'chemicalActivities'                           => $chemicalActivities,
            'activityTitle'                                => $activityTitle,
            'systemId'                                     => $request->query->get('systemId'),
            'pageType'                                     => $types,
        );
    }

    /**
     * Update SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/exportexcel", name="cms_subproject_corrosion_datasheet_exportexcel")
     * @Method("POST")
     * @Template()
     */
    public function exportexcelAction(Request $request, $id, $types, $projectId, $subprojectId)
    {
        set_time_limit(0);
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();
        $em->flush();
        $em->clear();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $datasheetLocationsToRetrieve = $request->query->get('dataRetrieve');

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $successMessage = $request->query->get('message');

        if ($successMessage) $this->get('session')->getFlashBag()->add('success', 'Data saved');

        if (strstr($datasheetLocationsToRetrieve, ',', true))
        {
            $datasheetLocationsToRetrieve = explode(",", $datasheetLocationsToRetrieve);
        }
        else
        {
            $datasheetLocationsToRetrieve = array(0 => $datasheetLocationsToRetrieve);
        }

        $activityTitle = "";

        if ($request && $datasheetLocationsToRetrieve) {

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = $this->activityTitle($activity);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5) {
                $type = "g";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6) {
                $type = "p";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
                $type = "ca-cd-oh";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
                $type = "ca";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3) {
                $type = "ca-cd-ch";
                $dbname = "SubProjectChemicalDosageDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {
                $type = "ca-cd-oh";
                $dbname = "SubProjectChemicalDosageDatabase";
                $types = "controlactivity";
            } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {
                $type = "ca";
                $dbname = "SubProjectChemicalDosageDatabase";
                $types = "controlactivity";
            } else if ($corrosion_type_entities['bdt_id'] == 4) {
                $type = "ca";
                $dbname = "SubProjectSamplingDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 1) {
                if ($corrosion_type_entities['bst_id'] == 8) {
                    $type = "cp";
                } else {
                    $type = "ca";
                }
                $dbname = "SubProjectCathodicProtectionDatabase";
            } else if ($corrosion_type_entities['bdt_id'] == 5) {
                $type = "pr";
                $dbname = "";
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            $datasheetLocationDetails = array();

            if (!empty($corrosionentities)) {

                foreach ($corrosionentities as $index => $datasheetLocationDetailstions) {

                    $datasheetLocationDetails[$index]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                    $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                    $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions[0]['id']);

                    $datasheetLocationDetails[$index]['id'] = $corrosion_locations->getId();
                    $datasheetLocationDetails[$index]['actlocationid'] = $corrosion_locations->getDbactivities();
                    $datasheetLocationDetails[$index]['actid'] = $id;
                    $datasheetLocationDetails[$index]['activityPerformed'] = $corrosion_locations->getActivityPerformed();

                    if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5) {
                        $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                        if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                            $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                            $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                        } else $datasheetLocationDetails[$index]['coupontype'] = "";
                        $datasheetLocationDetails[$index]['installationweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['installation_weight']);
                        $datasheetLocationDetails[$index]['retrievalweight'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['retrieval_weight']);
                        $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                        $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                        $datasheetLocationDetails[$index]['generalcorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['general_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagecorrosionratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagecorrosionratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);

                    } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6) {
                        $datasheetLocationDetails[$index]['couponholdertagno'] = $datasheetLocationDetailstions['couponholdertagno'];
                        if ($datasheetLocationDetailstions['coupontypeandareaid']) {
                            $corrosion_coupon_data = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($datasheetLocationDetailstions['coupontypeandareaid']);
                            $datasheetLocationDetails[$index]['coupontype'] = $corrosion_coupon_data->getColIdOneValue() . " and " . $corrosion_coupon_data->getColIdTwoValue();
                        } else $datasheetLocationDetails[$index]['coupontype'] = "";
                        $datasheetLocationDetails[$index]['depthpit'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['depth_pit']);
                        $datasheetLocationDetails[$index]['couponserialno'] = $datasheetLocationDetailstions[0]['coupon_serial_no'];

                        $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                        $datasheetLocationDetails[$index]['dateinstalled'] = (!empty($datasheetLocationDetailstions[0]['installed_date']) ? $datasheetLocationDetailstions[0]['installed_date']->format('d M Y') : "");

                        $datasheetLocationDetails[$index]['pittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['pitting_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagepittingratemmpy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mmpy']);
                        $datasheetLocationDetails[$index]['averagepittingratempy'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['average_rate_mpy']);

                    } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3) {
                        if ($types == "average") {
                            $datasheetLocationDetails[$index]['from_date'] = (!empty($datasheetLocationDetailstions[0]['from_date']) ? $datasheetLocationDetailstions[0]['from_date']->format('d M Y') : "");
                            $datasheetLocationDetails[$index]['to_date'] = $datasheetLocationDetailstions[0]['to_date']->format('d M Y');
                            $datasheetLocationDetails[$index]['corrosion_inhibitor_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["corrosion_inhibitor_percentage_availability"]);
                            $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["vol_inject_vs_vol_req_percentage"]);
                            $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]["per_time_ci_target_reach_percentage"]);

                        } else {

                            $datasheetLocationDetails[$index]['dateretrieval'] = (!empty($datasheetLocationDetailstions[0]['retrieval_date']) ? $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y') : "");
                            $datasheetLocationDetails[$index]['gas_production_mmscf_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gas_production_mmscf_per_day']);
                            $datasheetLocationDetails[$index]['total_production_bbl_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['total_production_bbl_per_day']);
                            $datasheetLocationDetails[$index]['recommended_gas_dosage_litre_per_mmscf'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['recommended_gas_dosage_litre_per_mmscf']);
                            $datasheetLocationDetails[$index]['target_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_dosage_litre_per_day']);
                            $datasheetLocationDetails[$index]['target_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['target_ci_concentration_ppm']);
                            $datasheetLocationDetails[$index]['actual_ci_dosage_litre_per_day'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_dosage_litre_per_day']);
                            $datasheetLocationDetails[$index]['actual_ci_concentration_ppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['actual_ci_concentration_ppm']);
                            $datasheetLocationDetails[$index]['ci_percentage_availability'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['ci_percentage_availability']);
                            $datasheetLocationDetails[$index]['vol_inject_vs_vol_req_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['vol_inject_vs_vol_req_percentage']);
                            $datasheetLocationDetails[$index]['per_time_ci_target_reach_percentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['per_time_ci_target_reach_percentage']);
                        }

                    } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                    } else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                    } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['control_activity_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                    } else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);
                        $datasheetLocationDetails[$index]['control_activity_actual_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_actual_value']);
                        $datasheetLocationDetails[$index]['control_activity_achievement_value'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_achievement_value']);

                    } else if ($corrosion_type_entities['bdt_id'] == 4) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);

                    } else if ($corrosion_type_entities['bst_id'] != 8 && $corrosion_type_entities['bdt_id'] == 1) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['controlactivityvalue'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['control_activity_value']);

                    } else if ($corrosion_type_entities['bst_id'] == 8 && $corrosion_type_entities['bdt_id'] == 1) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['onpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['on_potential']);
                        $datasheetLocationDetails[$index]['offpotential'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['off_potential']);

                    } else if ($corrosion_type_entities['bdt_id'] == 6) {
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['train'];
                        $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                        $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                        $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['oil_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['condensate_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['water_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions['total_production_bbl_per_day']);

                    }

                }
            }

            return [
                'corrosion_datasheet_locations' => $datasheetLocationDetails,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'project'      => $project,
                'subproject'   => $subproject,
                'activityId' => $id,
                'activityTitle' => $activityTitle,
                'corrosionType' => $type,
                'pageType' => $types,
                'dataRetrieve' => $request->query->get('dataRetrieve')
            ];
        }

    }

    /**
     * Update SubProjectCorrosionProductionDatasheet
     *
     * @Route("/{id}/{types}/productionedit", name="cms_subproject_corrosion_datasheet_production")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function productioneditAction(Request $request, $id, $types, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $successMessage = $request->query->get('message');
        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if ($successMessage) $this->get('session')->getFlashBag()->add('success', 'Data saved');

        if ($request->request->get('hidGroupId') != "") {
            $ids = $request->request->get('hidGroupId');
        }
        else if ($request->query->get('dataRetrieve') != ""){
            $ids = $request->query->get('dataRetrieve');
        }

        $datasheetLocationsToRetrieve = $ids;

        if (strstr($datasheetLocationsToRetrieve, ',', true)) $datasheetLocationsToRetrieve = explode(",", $datasheetLocationsToRetrieve);
        else {
            $datasheetLocationsToRetrieve = array(0 => $datasheetLocationsToRetrieve);
        }

        $activityTitle = "";

        if ($request && $datasheetLocationsToRetrieve) {

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = $this->activityTitle($activity);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if ($corrosion_type_entities['bdt_id'] == 6) {
                $type = "pr";
                $dbname = "";
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            $datasheetLocationDetails = array();

            if (!empty($corrosionentities)) {

                foreach ($corrosionentities as $index => $datasheetLocationDetailstions) {
                    $datasheetLocationDetails[$index]['actid'] = $id;
                    $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                    $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['location'];
                    $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                    $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                    $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions[0]['id']);
                    $datasheetLocationDetails[$index]['actlocationid'] = $corrosion_locations->getDbactivities();
                    $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions['id']);

                    if ($corrosion_type_entities['bdt_id'] == 6) {
                        $datasheetLocationDetails[$index]['id'] = $datasheetLocationDetailstions[0]['id'];
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['oil_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['condensate_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['water_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['total_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['watercutpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['water_cut_percentage']);
                        $datasheetLocationDetails[$index]['gorscfstb'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gor_scf_stb']);
                        $datasheetLocationDetails[$index]['salinityppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['salinity_ppm']);
                        $datasheetLocationDetails[$index]['htwosmolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['h2s_mol_percentage']);
                        $datasheetLocationDetails[$index]['cotwomolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['co2_mol_percentage']);
                        $datasheetLocationDetails[$index]['usppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['usp_psig']);
                        $datasheetLocationDetails[$index]['dsppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['dsp_psig']);
                        $datasheetLocationDetails[$index]['ustdegreecelsius'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['ust_degree_celsius']);
                        $datasheetLocationDetails[$index]['espopsfrequencyhz'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_ops_frequency_hz']);
                        $datasheetLocationDetails[$index]['espintakepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_intake_pressure_psig']);
                        $datasheetLocationDetails[$index]['espdischargepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_discharge_pressure_psig']);
                    }

                }
            }
            //sort($datasheetLocationDetails);

            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_DELETE',$this->userRolesMergeToCheck)){
                $this->showMitStatTrendDatasheetsEdHiDelete = 1;
            }

            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EDIT',$this->userRolesMergeToCheck)){
                $this->showMitStatTrendDatasheetsEdHiEdit = 1;
            }

            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EXCEL',$this->userRolesMergeToCheck)){
                $this->showMitStatTrendDatasheetsEdHiExcel = 1;
            }

            return [
                'corrosion_datasheet_locations' => $datasheetLocationDetails,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'subproject' => $subproject,
                'activityId' => $id,
                'activityTitle' => $activityTitle,
                'corrosionType' => $type,
                'pageType' => $types,
                'dataRetrieve' => $ids,
                'showMitStatTrendDatasheetsEdHiDelete' => $this->showMitStatTrendDatasheetsEdHiDelete,
                'showMitStatTrendDatasheetsEdHiEdit' => $this->showMitStatTrendDatasheetsEdHiEdit,
                'showMitStatTrendDatasheetsEdHiExcel' => $this->showMitStatTrendDatasheetsEdHiExcel,
                'systemId' => $request->query->get('systemId'),
            ];
        }

    }


    /**
     * Update SubProjectCorrosionProductionDatasheet
     *
     * @Route("/{id}/{types}/productionexcel", name="cms_subproject_corrosion_datasheet_production_excel")
     * @Method("POST")
     * @Template()
     */
    public function productionexcelAction(Request $request, $id, $types, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $successMessage = $request->query->get('message');

        if ($successMessage) $this->get('session')->getFlashBag()->add('success', 'Data saved');

        if ($request->request->get('hidGroupId') != "") {
            $ids = $request->request->get('hidGroupId');
        }

        $datasheetLocationsToRetrieve = $ids;

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (strstr($datasheetLocationsToRetrieve, ',', true)) $datasheetLocationsToRetrieve = explode(",", $datasheetLocationsToRetrieve);
        else {
            $datasheetLocationsToRetrieve = array(0 => $datasheetLocationsToRetrieve);
        }

        $activityTitle = "";

        if ($request && $datasheetLocationsToRetrieve) {

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = $this->activityTitle($activity);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if ($corrosion_type_entities['bdt_id'] == 6) {
                $type = "pr";
                $dbname = "";
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            $datasheetLocationDetails = array();

            if (!empty($corrosionentities)) {

                foreach ($corrosionentities as $index => $datasheetLocationDetailstions) {
                    $datasheetLocationDetails[$index]['actid'] = $id;
                    $datasheetLocationDetails[$index]['location'] = $datasheetLocationDetailstions['location'];
                    $datasheetLocationDetails[$index]['train'] = $datasheetLocationDetailstions['location'];
                    $datasheetLocationDetails[$index]['fieldmanifold'] = $datasheetLocationDetailstions['fieldmanifold'];
                    $datasheetLocationDetails[$index]['stationmanifold'] = $datasheetLocationDetailstions['stationmanifold'];
                    $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions[0]['id']);
                    $datasheetLocationDetails[$index]['actlocationid'] = $corrosion_locations->getDbactivities();
                    $corrosion_locations = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($datasheetLocationDetailstions['id']);

                    if ($corrosion_type_entities['bdt_id'] == 6) {
                        $datasheetLocationDetails[$index]['id'] = $datasheetLocationDetailstions[0]['id'];
                        $datasheetLocationDetails[$index]['dateretrieval'] = $datasheetLocationDetailstions[0]['retrieval_date']->format('d M Y');
                        $datasheetLocationDetails[$index]['gasproductionmmscfday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gas_production_mmscf_per_day']);
                        $datasheetLocationDetails[$index]['oilproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['oil_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['condensateproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['condensate_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['waterproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['water_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['totalproductionbblday'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['total_production_bbl_per_day']);
                        $datasheetLocationDetails[$index]['watercutpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['water_cut_percentage']);
                        $datasheetLocationDetails[$index]['gorscfstb'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['gor_scf_stb']);
                        $datasheetLocationDetails[$index]['salinityppm'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['salinity_ppm']);
                        $datasheetLocationDetails[$index]['htwosmolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['h2s_mol_percentage']);
                        $datasheetLocationDetails[$index]['cotwomolpercentage'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['co2_mol_percentage']);
                        $datasheetLocationDetails[$index]['usppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['usp_psig']);
                        $datasheetLocationDetails[$index]['dsppsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['dsp_psig']);
                        $datasheetLocationDetails[$index]['ustdegreecelsius'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['ust_degree_celsius']);
                        $datasheetLocationDetails[$index]['espopsfrequencyhz'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_ops_frequency_hz']);
                        $datasheetLocationDetails[$index]['espintakepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_intake_pressure_psig']);
                        $datasheetLocationDetails[$index]['espdischargepressurepsig'] = $this->removeTrailingAddZero($datasheetLocationDetailstions[0]['esp_discharge_pressure_psig']);
                    }

                }
            }
            //sort($datasheetLocationDetails);

            return [
                'corrosion_datasheet_locations' => $datasheetLocationDetails,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId,
                'project'      => $project,
                'subproject'   => $subproject,
                'activityId' => $id,
                'activityTitle' => $activityTitle,
                'corrosionType' => $type,
                'pageType' => $types,
                'dataRetrieve' => $ids
            ];
        }

    }

    /**
     * Create the entity SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/create", name="cms_subproject_corrosion_datasheet_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $id, $projectId, $subprojectId)
    {

        $em = $this->getDoctrine()->getManager('cms');
        $flag = 0;
        $pageType = "";

        if( $request->isXmlHttpRequest() &&  $request->isMethod( 'POST' ) )
        {

            $datasheetLocationsToStore = $request->request->get('dataStore');

            $corrosionType = $request->request->get('corrosionType');

            $pageType = $request->request->get('pageType');

            $datasheetLocationsToRetrieve = $request->request->get('locationId');

            if (empty($datasheetLocationsToStore))
            {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $retrievalDate = null;

            $checkflag = 0;

            $inc = 0;

            foreach ( $datasheetLocationsToStore as $datasheetLocations )
            {

                if ( !empty($datasheetLocations) )
                {

                    if ($corrosionType == "g" && empty($datasheetLocations['averagecorrosionratemmpy']) )
                    {
                        $checkflag=1;
                    }
                    else if ($corrosionType == "p" && empty($datasheetLocations['averagepittingratemmpy']) )
                    {
                        $checkflag=2;
                    }
                    else if ($corrosionType == "cp" && empty($datasheetLocations['onpotential']))
                    {
                        $checkflag=5;
                    }
                    else if ($corrosionType == "ca" && empty($datasheetLocations['controlactivityvalue']) )
                    {
                        $checkflag=3;
                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="normal" && empty($datasheetLocations['target_ci_dosage_litre_per_day']))
                    {
                        $checkflag=1;
                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="normal" && empty($datasheetLocations['target_ci_concentration_ppm']))
                    {
                        $checkflag=4;
                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="normal" && empty($datasheetLocations['actual_ci_concentration_ppm']))
                    {
                        $checkflag=5;
                    }
                    else if (($corrosionType == "ca-cd-ch" || $corrosionType == "g" || $corrosionType == "p" || $corrosionType == "ca" || $corrosionType == "cp") && empty($datasheetLocations['dateretrieval']) && $pageType=="normal")
                    {
                        $checkflag=29;
                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="average" && empty($datasheetLocations['corrosion_inhibitor_availability_average']))
                    {
                        $fromDate = new \DateTime($datasheetLocations['from_date']);

                        if (empty($fromDate) && empty($datasheetLocations['corrosion_inhibitor_availability_average'])) {
                            $checkflag=8;
                        }

                        $toDate = new \DateTime($datasheetLocations['to_date']);

                        if (empty($toDate) && empty($datasheetLocations['corrosion_inhibitor_availability_average'])) {
                            $checkflag=9;
                        }
                    }
                    else if ($corrosionType == "ca-cd-oh" && empty($datasheetLocations['control_activity_achievement_value']))
                    {
                        $checkflag=19;
                    }
                    else if ($corrosionType == "pr" && empty($datasheetLocations['dateretrieval']))
                    {
                        $checkflag=15;
                    }
                }
                $inc++;

            }

            foreach ( $datasheetLocationsToStore as $datasheetLocations )
            {

                if ( !empty($datasheetLocations) )
                {
                    if (isset($datasheetLocations['dateretrieval']) && !empty($datasheetLocations['dateretrieval']) && $pageType != "average")
                    {
                        try
                        {
                            if (strpos($datasheetLocations['dateretrieval'], '/') !== false) {
                                $retrievalDate = date('Y-m-d', strtotime(str_replace('/', '-', $datasheetLocations['dateretrieval'])));
                            }
                            elseif (strpos($datasheetLocations['dateretrieval'], '-') !== false) {
                                $retrievalDate = $datasheetLocations['dateretrieval'];
                            }

                            $retrievalDate = new \DateTime($retrievalDate);

                        }
                        catch(Exception $e){
                            return new Response(json_encode(array('result' => 'formaterror')));
                        }
                    }

                    $corrosionDatasheetEntity = new CorrosionDatasheet();
                    if ($corrosionType == "g" && !$checkflag )
                    {
                        if (!empty($datasheetLocations['dateinstalled']) && $datasheetLocations['dateinstalled'] != "DD-MM-YYYY") $installedDate = new \DateTime($datasheetLocations['dateinstalled']);
                        $generalCorrosionEntity = new GeneralCorrosion();
                        //$generalCorrosionHistoryEntity = new GeneralCorrosionHistory();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'generalcorrosion', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $couponserialno = (!empty($datasheetLocations['couponserialno']) ? $datasheetLocations['couponserialno']: "");
                            $retrievalweight = (!empty($datasheetLocations['retrievalweight']) ? $datasheetLocations['retrievalweight']: "");
                            $installationweight = (!empty($datasheetLocations['installationweight']) ? $datasheetLocations['installationweight']: "");
                            $generalcorrosionratemmpy = (!empty($datasheetLocations['generalcorrosionratemmpy']) ? $datasheetLocations['generalcorrosionratemmpy']: "");
                            $averagecorrosionratempy = (!empty($datasheetLocations['averagecorrosionratempy']) ? $datasheetLocations['averagecorrosionratempy']: "");
                            $generalCorrosionEntity->setSubproject($subproject);
                            $generalCorrosionEntity->setDbactivities($datasheetLocations['actid']);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $generalCorrosionEntity->setActivityId($corrosionActEntity);
                            $generalCorrosionEntity->setCouponSerialNo($couponserialno);
                            $generalCorrosionEntity->setRetrievalDate($retrievalDate);
                            if (!empty($datasheetLocations['dateinstalled']) && $datasheetLocations['dateinstalled'] != "DD-MM-YYYY") $generalCorrosionEntity->setInstalledDate($installedDate);
                            $generalCorrosionEntity->setAverageRateMmpy($datasheetLocations['averagecorrosionratemmpy']);
                            $generalCorrosionEntity->setAverageRateMpy($averagecorrosionratempy);
                            $generalCorrosionEntity->setRetrievalWeight($retrievalweight);
                            $generalCorrosionEntity->setInstallationWeight($installationweight);
                            $generalCorrosionEntity->setGeneralRateMmpy($generalcorrosionratemmpy);
                            $em->persist($generalCorrosionEntity);
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "p" && !$checkflag)
                    {
                        if (!empty($datasheetLocations['dateinstalled']) && $datasheetLocations['dateinstalled'] != "DD-MM-YYYY") $installedDate = new \DateTime($datasheetLocations['dateinstalled']);
                        $pittingCorrosionEntity = new PittingCorrosion();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'pittingcorrosion', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $couponserialno = (!empty($datasheetLocations['couponserialno']) ? $datasheetLocations['couponserialno']: "");
                            $averagepittingratempy = (!empty($datasheetLocations['averagepittingratempy']) ? $datasheetLocations['averagepittingratempy']: "");
                            $depthpit = (!empty($datasheetLocations['depthpit']) ? $datasheetLocations['depthpit']: "");
                            $pittingratemmpy = (!empty($datasheetLocations['pittingratemmpy']) ? $datasheetLocations['pittingratemmpy']: "");
                            $pittingCorrosionEntity->setSubproject($subproject);
                            $pittingCorrosionEntity->setDbactivities($datasheetLocations['actid']);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $pittingCorrosionEntity->setActivityId($corrosionActEntity);
                            $pittingCorrosionEntity->setCouponSerialNo($couponserialno);
                            $pittingCorrosionEntity->setRetrievalDate($retrievalDate);
                            if (!empty($datasheetLocations['dateinstalled']) && $datasheetLocations['dateinstalled'] != "DD-MM-YYYY") $pittingCorrosionEntity->setInstalledDate($installedDate);
                            $pittingCorrosionEntity->setAverageRateMmpy($datasheetLocations['averagepittingratemmpy']);
                            $pittingCorrosionEntity->setAverageRateMpy($averagepittingratempy);
                            $pittingCorrosionEntity->setDepthPit($depthpit);
                            $pittingCorrosionEntity->setPittingRateMmpy($pittingratemmpy);
                            $em->persist($pittingCorrosionEntity);
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "ca" && !$checkflag)
                    {
                        $controlactivityEntity = new ControlActivity();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'controlactivity', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $controlactivityEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityEntity->setActivityId($corrosionActEntity);
                            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($corrosionActEntity->getSubprojectmetrics()->getId());
                            $controlactivityEntity->setDbactivities($datasheetLocations['actid']);
                            $controlactivityEntity->setRetrievalDate($retrievalDate);
                            $controlactivityEntity->setControlActivityValue($datasheetLocations['controlactivityvalue']);
                            $em->persist($controlactivityEntity);
                            $flag = 1;
                        }
                        else
                        {
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "cp" && !$checkflag)
                    {
                        $controlactivityEntity = new CathodicProtectionDatasheet();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'cathodicprotectiondatasheet', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $controlactivityEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityEntity->setActivityId($corrosionActEntity);
                            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($corrosionActEntity->getSubprojectmetrics()->getId());
                            $controlactivityEntity->setDbactivities($datasheetLocations['actid']);
                            $controlactivityEntity->setRetrievalDate($retrievalDate);
                            $controlactivityEntity->setOnPotential($datasheetLocations['onpotential']);
                            if ( isset($datasheetLocations['offpotential']) && $datasheetLocations['offpotential'] != "" ) $controlactivityEntity->setOffPotential($datasheetLocations['offpotential']);
                            $em->persist($controlactivityEntity);
                            $flag = 1;
                        }
                        else
                        {
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="normal" && !$checkflag)
                    {

                        $controlactivityEntity = new CorrosionInhibitor();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'corrosioninhibitor', $id, $subprojectId );

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $gas_production_mmscf_day = (!empty($datasheetLocations['gas_production_mmscf_day']) ? $datasheetLocations['gas_production_mmscf_day']: "");
                            $total_production_bbl_per_day = (!empty($datasheetLocations['total_production_bbl_per_day']) ? $datasheetLocations['total_production_bbl_per_day']: "");
                            $recommended_gas_dosage_litre_per_mmscf = (!empty($datasheetLocations['recommended_gas_dosage_litre_per_mmscf']) ? $datasheetLocations['recommended_gas_dosage_litre_per_mmscf']: "");
                            $actual_ci_dosage_litre_per_day = (!empty($datasheetLocations['actual_ci_dosage_litre_per_day']) ? $datasheetLocations['actual_ci_dosage_litre_per_day']: "");
                            $ci_percentage_availability = (!empty($datasheetLocations['ci_percentage_availability']) ? $datasheetLocations['ci_percentage_availability']: "");
                            $vol_inject_vs_vol_req_percentage = (!empty($datasheetLocations['vol_inject_vs_vol_req_percentage']) ? $datasheetLocations['vol_inject_vs_vol_req_percentage']: "");
                            $per_time_ci_target_reach_percentage = (!empty($datasheetLocations['per_time_ci_target_reach_percentage']) ? $datasheetLocations['per_time_ci_target_reach_percentage']: "");
                            $controlactivityEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityEntity->setActivityId($corrosionActEntity);
                            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($corrosionActEntity->getSubprojectmetrics()->getId());
                            $controlactivityEntity->setDbactivities($datasheetLocations['actid']);
                            $controlactivityEntity->setRetrievalDate($retrievalDate);
                            $controlactivityEntity->setGasProductionMmscfPerDay($gas_production_mmscf_day);
                            $controlactivityEntity->setTotalProductionBblPerDay($total_production_bbl_per_day);
                            $controlactivityEntity->setRecommendedGasDosageLitrePerMmscf($recommended_gas_dosage_litre_per_mmscf);
                            $controlactivityEntity->setTargetCiDosageLitrePerDay($datasheetLocations['target_ci_dosage_litre_per_day']);
                            $controlactivityEntity->setTargetCiConcentrationPpm($datasheetLocations['target_ci_concentration_ppm']);
                            $controlactivityEntity->setActualCiDosageLitrePerDay($actual_ci_dosage_litre_per_day);
                            $controlactivityEntity->setActualCiConcentrationPpm($datasheetLocations['actual_ci_concentration_ppm']);
                            $controlactivityEntity->setCiPercentageAvailability($ci_percentage_availability);
                            $controlactivityEntity->setVolInjectVsVolReqPercentage($vol_inject_vs_vol_req_percentage);
                            $controlactivityEntity->setPerTimeCiTargetReachPercentage($per_time_ci_target_reach_percentage);

                            $em->persist($controlactivityEntity);
                            $em->flush();
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }

                    }
                    else if ($corrosionType == "ca-cd-ch" && $pageType=="average" && !$checkflag)
                    {

                        $controlactivityInhEntity = new CorrosionInhibitorAvailability();
                        if (isset($datasheetLocations['from_date']) && !empty($datasheetLocations['from_date']))
                        {
                            if (strpos($datasheetLocations['from_date'], '/') !== false) {
                                $fromDate = date('Y-m-d', strtotime(str_replace('/', '-', $datasheetLocations['from_date'])));
                            }
                            elseif (strpos($datasheetLocations['from_date'], '-') !== false) {
                                $fromDate = date('Y-m-d', strtotime($datasheetLocations['from_date']));
                            }

                            $fromDate = new \DateTime($fromDate);
                        }
                        if (strpos($datasheetLocations['to_date'], '/') !== false) {
                            $toDate = date('Y-m-d', strtotime(str_replace('/', '-', $datasheetLocations['to_date'])));
                        }
                        elseif (strpos($datasheetLocations['to_date'], '-') !== false) {
                            $toDate = date('Y-m-d', strtotime($datasheetLocations['to_date']));
                        }

                        $toDate = new \DateTime($toDate);

                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetAverageByFilter( $datasheetLocations['actid'], $toDate,'corrosioninhibitoravailability', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity)) {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $corrosion_inhibitor_availability_average = (!empty($datasheetLocations['corrosion_inhibitor_availability_average']) ? $datasheetLocations['corrosion_inhibitor_availability_average']: NULL);
                            $vol_inject_vs_vol_req_percentage_average = (!empty($datasheetLocations['vol_inject_vs_vol_req_percentage_average']) ? $datasheetLocations['vol_inject_vs_vol_req_percentage_average']: NULL);
                            $per_time_ci_target_reach_percentage_average = (!empty($datasheetLocations['per_time_ci_target_reach_percentage_average']) ? $datasheetLocations['per_time_ci_target_reach_percentage_average']: NULL);
                            $controlactivityInhEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityInhEntity->setActivityId($corrosionActEntity);
                            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($corrosionActEntity->getSubprojectmetrics()->getId());
                            $controlactivityInhEntity->setDbactivities($datasheetLocations['actid']);
                            if (isset($datasheetLocations['from_date']) && !empty($datasheetLocations['from_date'])) $controlactivityInhEntity->setFromDate($fromDate);
                            $controlactivityInhEntity->setToDate($toDate);
                            $controlactivityInhEntity->setCorrosionInhibitorPercentageAvailability($corrosion_inhibitor_availability_average);
                            $controlactivityInhEntity->setVolInjectVsVolReqPercentage($vol_inject_vs_vol_req_percentage_average);
                            $controlactivityInhEntity->setPerTimeCiTargetReachPercentage($per_time_ci_target_reach_percentage_average);
                            $em->persist($controlactivityInhEntity);
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "ca-cd-oh" && !$checkflag)
                    {
                        $controlactivityEntity = new ControlActivity();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionDatasheetByFilter($datasheetLocations['actid'], $retrievalDate, 'controlactivity', $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $control_activity_value = (!empty($datasheetLocations['control_activity_value']) ? $datasheetLocations['control_activity_value']: NULL);
                            $control_activity_actual_value = (!empty($datasheetLocations['control_activity_actual_value']) ? $datasheetLocations['control_activity_actual_value']: NULL);
                            $controlactivityEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityEntity->setActivityId($corrosionActEntity);
                            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($corrosionActEntity->getSubprojectmetrics()->getId());
                            $controlactivityEntity->setDbactivities($datasheetLocations['actid']);
                            $controlactivityEntity->setRetrievalDate($retrievalDate);
                            $controlactivityEntity->setControlActivityValue($control_activity_value);
                            $controlactivityEntity->setControlActivityActualValue($control_activity_actual_value);
                            $controlactivityEntity->setControlActivityAchievementValue($datasheetLocations['control_activity_achievement_value']);
                            $em->persist($controlactivityEntity);
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }
                    }
                    else if ($corrosionType == "pr" && !$checkflag)
                    {

                        $controlactivityEntity = new Production();
                        $dateRetrievalExistsEntity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getProductionCorrosionDatasheetByFilter($retrievalDate, 'production', $datasheetLocationsToRetrieve, $id, $subprojectId);

                        if (empty($dateRetrievalExistsEntity))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                            $controlactivityEntity->setSubproject($subproject);
                            $corrosionActEntity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);
                            $controlactivityEntity->setActivityId($corrosionActEntity);
                            $controlactivityEntity->setRetrievalDate($retrievalDate);

                            $controlactivityEntity->setDbactivities($request->request->get('locationId'));

                            $controlactivityEntity->setGasProductionMmscfPerDay(($datasheetLocations['gasproductionmmscfday'] != "" ? $datasheetLocations['gasproductionmmscfday'] : NULL));
                            $controlactivityEntity->setOilProductionBblPerDay(($datasheetLocations['oilproductionbblday'] != "" ? $datasheetLocations['oilproductionbblday'] : NULL));
                            $controlactivityEntity->setCondensateProductionBblPerDay(($datasheetLocations['condensateproductionbblday'] != "" ? $datasheetLocations['condensateproductionbblday'] : NULL));
                            $controlactivityEntity->setWaterProductionBblPerDay(($datasheetLocations['waterproductionbblday'] != "" ? $datasheetLocations['waterproductionbblday'] : NULL));
                            $controlactivityEntity->setTotalProductionBblPerDay(($datasheetLocations['totalproductionbblday'] != "" ? $datasheetLocations['totalproductionbblday'] : NULL));

                            $controlactivityEntity->setWaterCutPercentage(($datasheetLocations['watercutpercentage'] != "" ? $datasheetLocations['watercutpercentage'] : NULL));
                            $controlactivityEntity->setGorScfStb(($datasheetLocations['gorscfstb'] != "" ? $datasheetLocations['gorscfstb'] : NULL));
                            $controlactivityEntity->setSalinityPpm(($datasheetLocations['salinityppm'] != "" ? $datasheetLocations['salinityppm'] : NULL));
                            $controlactivityEntity->setH2sMolPercentage(($datasheetLocations['htwosmolpercentage'] != "" ? $datasheetLocations['htwosmolpercentage'] : NULL));
                            $controlactivityEntity->setCo2MolPercentage(($datasheetLocations['cotwomolpercentage'] != "" ? $datasheetLocations['cotwomolpercentage'] : NULL));

                            $controlactivityEntity->setUspPsig(($datasheetLocations['usppsig'] != "" ? $datasheetLocations['usppsig'] : NULL));
                            $controlactivityEntity->setDspPsig(($datasheetLocations['dsppsig'] != "" ? $datasheetLocations['dsppsig'] : NULL));
                            $controlactivityEntity->setUstDegreeCelsius(($datasheetLocations['ustdegreecelsius'] != "" ? $datasheetLocations['ustdegreecelsius'] : NULL));
                            $controlactivityEntity->setEspOpsFrequencyHz(($datasheetLocations['espopsfrequencyhz'] != "" ? $datasheetLocations['espopsfrequencyhz'] : NULL));
                            $controlactivityEntity->setEspIntakePressurePsig(($datasheetLocations['espintakepressurepsig'] != "" ? $datasheetLocations['espintakepressurepsig'] : NULL));
                            $controlactivityEntity->setEspDischargePressurePsig(($datasheetLocations['espdischargepressurepsig'] != "" ? $datasheetLocations['espdischargepressurepsig'] : NULL));

                            $em->persist($controlactivityEntity);
                            $flag = 1;
                        }
                        else{
                            $flag = -1;
                        }
                    }
                }

            }

            $em->flush();
            $em->clear();

            if ( $flag == 1 ) return new Response(json_encode(array('result' => 'ok')));
            else if ( $flag == -1 ) return new Response(json_encode(array('result' => 'exists')));
            else return new Response(json_encode(array('result' => 'notok')));
            //return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet',array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id' => $id)));
        }
        else
        {
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Delete the entity SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{sid}/delete", name="cms_subproject_corrosion_datasheet_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id, $sid, $projectId, $subprojectId)
    {
        $type = $request->query->get('type');
        $datasheetLocationsToRetrieve = $request->query->get('dataRetrieve');

        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_DELETE',$this->userRolesMergeToCheck)) {

            $em = $this->getManager();

            if ($request->isMethod('GET')) {

                $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

                if ($sid) {
                    $entity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($sid);
                }

                if (!$entity) {
                    return new Response(json_encode(array('result' => 'noentity')));
                }

                $em->remove($entity);
                $em->flush();

                $this->addFlash('success', 'Data point deleted');

                if ($entity instanceof Production) {
                    return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_production', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => $type, 'dataRetrieve' => $datasheetLocationsToRetrieve, 'systemId' => $request->query->get('systemId')]));
                }
                else
                    return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_edit', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => $type, 'dataRetrieve' => $datasheetLocationsToRetrieve, 'systemId' => $request->query->get('systemId')]));

            }
        }
        else {
            $this->addFlash('warning', 'You are not permitted to perform this action');
            return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_edit', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'types' => $type, 'dataRetrieve' => $datasheetLocationsToRetrieve, 'systemId' => $request->query->get('systemId')]));
        }

    }

    /**
     * Update subprojectcorrosion_datasheet
     *
     * @Route("/{id}/update", name="cms_subproject_corrosion_datasheet_update")
     * @Method("POST")
     * @Template()
     */
    public function updatefieldsAction($id, Request $request)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_EDIT_HISTORY_EDIT',$this->userRolesMergeToCheck))
        {

            $em = $this->getManager();

            $passId = $request->request->get('passid');

            $entity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->find($passId);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notoks')));
            }

            if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {

                $returnVal = 0;

                if ( $request->request->get('title') != "" && $request->request->get('title') != 0 )
                {
                    $title = $request->request->get('title');
                }
                if ( $request->request->get('title') != "" && $request->request->get('title') == 0 )
                {
                    $title = 0;
                }
                if ( $request->request->get('title') == "" )
                {
                    $title = NULL;
                }

                //$title = (!empty($request->request->get('title')) ? $request->request->get('title') : NULL);
                $key = $request->request->get('key');

                if ($key) {
                    switch ($key) {
                        case "dateretrieval":
                            if ($title) {
                                $title = new \DateTime($title);
                                $entity->setRetrievalDate($title);
                            }
                            break;
                        case "dateinstalled":
                            if ($title) {
                                $title = new \DateTime($title);
                                $entity->setInstalledDate($title);
                            }
                            break;
                        case "depthpit":
                            $entity->setDepthPit($title);
                            break;
                        case "generalcorrosionratemmpy":
                            $entity->setGeneralRateMmpy($title);
                            break;
                        case "pittingratemmpy":
                            $entity->setPittingRateMmpy($title);
                            break;
                        case "averagecorrosionratemmpy":
                            $entity->setAverageRateMmpy($title);
                            break;
                        case "averagecorrosionratempy":
                            $entity->setAverageRateMpy($title);
                            break;
                        case "averagepittingratemmpy":
                            $entity->setAverageRateMmpy($title);
                            break;
                        case "averagepittingratempy":
                            $entity->setAverageRateMpy($title);
                            break;
                        case "retrievalweight":
                            $entity->setRetrievalWeight($title);
                            break;
                        case "installationweight":
                            $entity->setInstallationWeight($title);
                            break;
                        case "controlactivityvalue":
                            $entity->setControlActivityValue($title);
                            break;
                        case "controlactivityactualvalue":
                            $entity->setControlActivityActualValue($title);
                            break;
                        case "controlactivityachievementvalue":
                            $entity->setControlActivityAchievementValue($title);
                            break;
                        case "gasproductionmmscfday":
                            $entity->setGasProductionMmscfPerDay($title);
                            break;
                        case "oilproductionbblday":
                            $entity->setOilProductionBblPerDay($title);
                            break;
                        case "condensateproductionbblday":
                            $entity->setCondensateProductionBblPerDay($title);
                            break;
                        case "waterproductionbblday":
                            $entity->setWaterProductionBblPerDay($title);
                            break;
                        case "totalproductionbblday":
                            $entity->setTotalProductionBblPerDay($title);
                            break;
                        case "gasproductionmmscfperday":
                            $entity->setGasProductionMmscfPerDay($title);
                            break;
                        case "totalproductionbblperday":
                            $entity->setTotalProductionBblPerDay($title);
                            break;
                        case "watercutpercentage":
                            $entity->setWaterCutPercentage($title);
                            break;
                        case "gorscfstb":
                            $entity->setGorScfStb($title);
                            break;
                        case "salinityppm":
                            $entity->setSalinityPpm($title);
                            break;
                        case "htwosmolpercentage":
                            $entity->setH2sMolPercentage($title);
                            break;
                        case "cotwomolpercentage":
                            $entity->setCo2MolPercentage($title);
                            break;
                        case "usppsig":
                            $entity->setUspPsig($title);
                            break;
                        case "dsppsig":
                            $entity->setDspPsig($title);
                            break;
                        case "ustdegreecelsius":
                            $entity->setUstDegreeCelsius($title);
                            break;
                        case "espopsfrequencyhz":
                            $entity->setEspOpsFrequencyHz($title);
                            break;
                        case "espintakepressurepsig":
                            $entity->setEspIntakePressurePsig($title);
                            break;
                        case "espdischargepressurepsig":
                            $entity->setEspDischargePressurePsig($title);
                            break;
                        case "recommendedgasdosagelitrepermmscf":
                            $entity->setRecommendedGasDosageLitrePerMmscf($title);
                            break;
                        case "targetcidosagelitreperday":
                            $entity->setTargetCiDosageLitrePerDay($title);
                            break;
                        case "targetciconcentrationppm":
                            $entity->setTargetCiConcentrationPpm($title);
                            break;
                        case "actualcidosagelitreperday":
                            $entity->setActualCiDosageLitrePerDay($title);
                            break;
                        case "actualciconcentrationppm":
                            $entity->setActualCiConcentrationPpm($title);
                            break;
                        case "cipercentageavailability":
                            $entity->setCiPercentageAvailability($title);
                            break;
                        case "volinjectvsvolreqpercentage":
                            $entity->setVolInjectVsVolReqPercentage($title);
                            break;
                        case "pertimecitargetreachpercentage":
                            $entity->setPerTimeCiTargetReachPercentage($title);
                            break;
                        case "fromdateaverage":
                            if ($title) {
                                $title = new \DateTime($title);
                                $entity->setFromDate($title);
                            }
                            break;
                        case "todateaverage":
                            if ($title) {
                                $title = new \DateTime($title);
                                $entity->setToDate($title);
                            }
                            break;
                        case "corrosioninhibitorpercentageavailability":
                            $entity->setCorrosionInhibitorPercentageAvailability($title);
                            break;
                        case "volinjectvsvolreqpercentageaverage":
                            $entity->setVolInjectVsVolReqPercentage($title);
                            break;
                        case "pertimecitargetreachpercentage":
                            $entity->setPerTimeCiTargetReachPercentage($title);
                            break;
                        case "onpotential":
                            $entity->setOnPotential($title);
                            break;
                        case "offpotential":
                            $entity->setOffPotential($title);
                            break;
                        default:
                            break;

                    }
                }

                $em->persist($entity);
                $em->flush();

                if ($title) {
                    switch ($key) {
                        case "dateretrieval":
                            $returnVal = $entity->getRetrievalDate();
                            break;
                        case "dateinstalled":
                            $returnVal = $entity->getInstalledDate();
                            break;
                        case "depthpit":
                            $returnVal = $entity->getDepthPit();
                            break;
                        case "generalcorrosionratemmpy":
                            $returnVal = $entity->getGeneralRateMmpy();
                            break;
                        case "pittingratemmpy":
                            $returnVal = $entity->getPittingRateMmpy();
                            break;
                        case "averagecorrosionratemmpy":
                            $returnVal = $entity->getAverageRateMmpy();
                            break;
                        case "averagecorrosionratempy":
                            $returnVal = $entity->getAverageRateMpy();
                            break;
                        case "averagepittingratemmpy":
                            $returnVal = $entity->getAverageRateMmpy();
                            break;
                        case "averagepittingratempy":
                            $returnVal = $entity->getAverageRateMpy();
                            break;
                        case "retrievalweight":
                            $returnVal = $entity->getRetrievalWeight();
                            break;
                        case "installationweight":
                            $returnVal = $entity->getInstallationWeight();
                            break;
                        case "controlactivityvalue":
                            $returnVal = $entity->getControlActivityValue();
                            break;
                        case "controlactivityactualvalue":
                            $returnVal = $entity->getControlActivityActualValue();
                            break;
                        case "controlactivityachievementvalue":
                            $returnVal = $entity->getControlActivityAchievementValue();
                            break;
                        case "gasproductionmmscfday":
                            $returnVal = $entity->getGasProductionMmscfPerDay();
                            break;
                        case "oilproductionbblday":
                            $returnVal = $entity->getOilProductionBblPerDay();
                            break;
                        case "condensateproductionbblday":
                            $returnVal = $entity->getCondensateProductionBblPerDay();
                            break;
                        case "waterproductionbblday":
                            $returnVal = $entity->getWaterProductionBblPerDay();
                            break;
                        case "totalproductionbblday":
                            $returnVal = $entity->getTotalProductionBblPerDay();
                            break;
                        case "gasproductionmmscfperday":
                            $returnVal = $entity->getGasProductionMmscfPerDay();
                            break;
                        case "totalproductionbblperday":
                            $returnVal = $entity->getTotalProductionBblPerDay();
                            break;
                        case "recommendedgasdosagelitrepermmscf":
                            $returnVal = $entity->getRecommendedGasDosageLitrePerMmscf();
                            break;
                        case "targetcidosagelitreperday":
                            $returnVal = $entity->getTargetCiDosageLitrePerDay();
                            break;
                        case "targetciconcentrationppm":
                            $returnVal = $entity->getTargetCiConcentrationPpm();
                            break;
                        case "actualcidosagelitreperday":
                            $returnVal = $entity->getActualCiDosageLitrePerDay();
                            break;
                        case "actualciconcentrationppm":
                            $returnVal = $entity->getActualCiConcentrationPpm();
                            break;
                        case "cipercentageavailability":
                            $returnVal = $entity->getCiPercentageAvailability();
                            break;
                        case "volinjectvsvolreqpercentage":
                            $returnVal = $entity->getVolInjectVsVolReqPercentage();
                            break;
                        case "pertimecitargetreachpercentage":
                            $returnVal = $entity->getPerTimeCiTargetReachPercentage();
                            break;
                        case "fromdateaverage":
                            $returnVal = $entity->getFromDate();
                            break;
                        case "todateaverage":
                            $returnVal = $entity->getToDate();
                            break;
                        case "corrosioninhibitorpercentageavailability":
                            $returnVal = $entity->getCorrosionInhibitorPercentageAvailability();
                            break;
                        case "volinjectvsvolreqpercentageaverage":
                            $returnVal = $entity->getVolInjectVsVolReqPercentage();
                            break;
                        case "pertimecitargetreachpercentage":
                            $returnVal = $entity->getPerTimeCiTargetReachPercentage();
                            break;
                        case "onpotential":
                            $returnVal = $entity->getOnPotential();
                            break;
                        case "offpotential":
                            $returnVal = $entity->getOffPotential();
                            break;
                        default:
                            break;
                    }
                }

                return new Response(json_encode(array('result' => 'ok', 'output' => $returnVal)));

            } else {
                return new Response(json_encode(array('result' => 'notok')));
            }
        }
        else {
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Graph all except production  SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/{ids}/graph", name="cms_subproject_corrosion_datasheet_graph")
     * @Method("GET")
     * @Template()
     */
    public function graphAction(Request $request, $id, $types, $ids, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $passids = $ids;

        $datasheetLocationsToRetrieve = \GuzzleHttp\json_decode($passids);

        if ($request && $projectId && $subprojectId)
        {
            $ids = str_replace('false',0,$ids);

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = "";

            $activityTitle = $this->activityTitle($activity);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5 )
            {
                $type = "g";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            }
            else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6 )
            {
                $type = "p";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
            }
			else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 )
            {
                $type = "ca-cd-oh";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
				$types = "controlactivity";
            }
			else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 )
            {
                $type = "ca";
                $dbname = "SubProjectCorrosionMonitoringDatabase";
				$types = "controlactivity";
            }
            else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 )
            {
                $type = "ca-cd-ch";
                $dbname = "SubProjectChemicalDosageDatabase";
            }
            else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 )
            {
                $type = "ca-cd-oh";
                $dbname = "SubProjectChemicalDosageDatabase";
                $types = "controlactivity";
            }
            else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2 )
            {
                $type = "ca";
                $dbname = "SubProjectChemicalDosageDatabase";
                $types = "controlactivity";
            }
            else if ($corrosion_type_entities['bdt_id'] == 4)
            {
                $type = "ca";
                $dbname = "SubProjectSamplingDatabase";
            }
            else if ($corrosion_type_entities['bdt_id'] == 1)
            {
                if ($corrosion_type_entities['bst_id'] == 8) {
                    $type = "cp";
                }
                else{
                    $type = "ca";
                }
                $dbname = "SubProjectCathodicProtectionDatabase";
            }
            else if ($corrosion_type_entities['bdt_id'] == 5)
            {
                $type = "pr";
                $dbname = "";
            }

            if ($corrosion_type_entities['bdt_id'] <= 4)
            {

                $corrosion_monitoring_entity = $em->getRepository('AIECmsBundle:'.$dbname)->findOneBy(array('activityid'=> $id,'subproject'=> $subprojectId));

                if (!$corrosion_type_entities)
                {
                    //throw new \Exception("Not found Sub Project Activity Entity!");
                    return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                        'message' => 'Not found Sub Project Activity Entity!'
                    ]);
                }
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            if (!empty($corrosionentities))
            {
                $passGraphdata = array();

                foreach ($corrosionentities as $index => $datasheetLocationDetailstions)
                {
                    $passGraphdata[$datasheetLocationDetailstions['id']]['id'] = $datasheetLocationDetailstions['id'];
                    $passGraphdata[$datasheetLocationDetailstions['id']]['analysis'] = $datasheetLocationDetailstions['analysis'];
                    $passGraphdata[$datasheetLocationDetailstions['id']]['recommendations'] = $datasheetLocationDetailstions['recommendations'];

//                    $passGraphdata[$datasheetLocationDetailstions['id']]['from_date'] = $datasheetLocationDetailstions['from_date']->format('Y-m-d');
//                    $passGraphdata[$datasheetLocationDetailstions['id']]['to_date'] = $datasheetLocationDetailstions['to_date']->format('Y-m-d');

                    if (!empty($datasheetLocationDetailstions['from_date']) && $datasheetLocationDetailstions['from_date']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['from_date']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date'] = null;
                    }
                    if (!empty($datasheetLocationDetailstions['to_date']) && $datasheetLocationDetailstions['to_date']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['to_date']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date'] = null;
                    }


                    if ( $corrosion_type_entities['bdt_id'] == 1 || $corrosion_type_entities['bdt_id'] == 3 || $corrosion_type_entities['bdt_id'] == 4 )
                    {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id']), true);
                    }
                    else if ( $corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true);
                    }
                    else if ( $corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true); 
                    }
					else if ( $corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true);
                    }
                    else if ( $corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true); 
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_corrosioninhibitorparse', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true);
                    }
                    else if ( $corrosion_type_entities['bdt_id'] == 5 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_production', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'ids'=>$passids), true);
                    }

                }
                if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_GRAPH_EDIT',$this->userRolesMergeToCheck)){
                    $this->showMitStatTrendDatasheetsGrDetEdit = 1;
                }
                
                //print_r($passGraphdata);

                if ( $corrosion_type_entities['bdt_id'] <= 4 )
                {
                    return [
                        'corrosionDatasheetLocations' => $passGraphdata,
                        'projectId' => $projectId,
                        'subprojectId' => $subprojectId,
                        'activityId' => $id,
                        'corrosionType' => $type,
                        'corrosionDetails' => $corrosion_type_entities,
                        'corrosion_monitoring_entity' => $corrosion_monitoring_entity,
                        'passids' => $passids,
                        'activityTitle' => $activityTitle,
                        'showMitStatTrendDatasheetsGrDetEdit'          => $this->showMitStatTrendDatasheetsGrDetEdit,
                        'systemId' => $request->query->get('systemId'),
                    ];
                }
                else
                {
                    return [
                        'corrosionDatasheetLocations' => $passGraphdata,
                        'projectId' => $projectId,
                        'subprojectId' => $subprojectId,
                        'activityId' => $id,
                        'corrosionType' => $type,
                        'corrosionDetails' => $corrosion_type_entities,
                        'passids' => $passids,
                        'activityTitle' => $activityTitle,
                        'showMitStatTrendDatasheetsGrDetEdit'          => $this->showMitStatTrendDatasheetsGrDetEdit,
                        'systemId' => $request->query->get('systemId'),
                    ];
                }

            }
            else
            {
                //throw new \Exception("No data to plot graph!", 404);

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'No data to plot graph'
                ]);
            }

        }

    }

    /**
     * Graph parse all except production SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{datasheetlocationid}/graphparse", name="cms_subproject_corrosion_datasheet_graph_controlactivity")
     * @Method("POST")
     * @Template()
     */
    public function graphParseAction(Request $request, $id, $datasheetlocationid, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        if( $request->isXmlHttpRequest() )
        {
            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if (!$corrosion_type_entities)
            {
                //throw new \Exception("Not Found Sub Project Activity Entity!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'Not Found Sub Project Activity Entity!'
                ]);
            }

            if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 5 )
            {
                $type = "g";
            }
            else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 6 )
            {
                $type = "p";
            }
			else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2 )
            {
                $type = "ca";
            }
			else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4 )
            {
                $type = "ca";
            }
            else if ($corrosion_type_entities['bdt_id'] == 4)
            {
                $type = "ca";
            }
            else if ($corrosion_type_entities['bdt_id'] == 1)
            {
                if ($corrosion_type_entities['bst_id'] == 8) {
                    $type = "cp";
                }
                else{
                    $type = "ca";
                }
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryById($database->getDatabasetype()->getId(), $id, $datasheetlocationid, $subprojectId);

            $redGraphData = "[";
            $greenGraphData = "[";
            $amberGraphData = "[";

            if (!empty($corrosionentities))
            {
                $datasheetLocationDetails = $datasheetlocationid."|".$corrosion_type_entities['ba_controltitle']."|"."[";
                $datasheetLocationDetails2 = "[";
                $datasheetLocationDetails3 = "[";

                    foreach ($corrosionentities as $index => $datasheetLocationDetailstions)
                    {

                        $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetailstions[0]['retrieval_date']->format('Y-m-d'))));

                        if ( $corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] > 4  && $corrosion_type_entities['bst_id'] < 7 ) {
                            if ($datasheetLocationDetailstions[0]['average_rate_mmpy']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['average_rate_mmpy'] . "],";
                        }
                        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4) {
                            if ($datasheetLocationDetailstions[0]['control_activity_achievement_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_achievement_value'] . "],";
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails2 .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] ."],";
                            if ($datasheetLocationDetailstions[0]['control_activity_actual_value']) $datasheetLocationDetails3 .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_actual_value'] . "],";
                        }
                        else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 2) {
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] ."],";
                        }
			            else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 4) {
                            if ($datasheetLocationDetailstions[0]['control_activity_achievement_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_achievement_value'] . "],";
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails2 .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] ."],";
                            if ($datasheetLocationDetailstions[0]['control_activity_actual_value']) $datasheetLocationDetails3 .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_actual_value'] ."],";

                        }
                        else if ($corrosion_type_entities['bdt_id'] == 3 && $corrosion_type_entities['bst_id'] == 2) {
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] ."],";
                        }
                        else if ($corrosion_type_entities['bdt_id'] == 4) {
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] . "],";
                        }
                        else if ($corrosion_type_entities['bst_id'] != 8 && $corrosion_type_entities['bdt_id'] == 1) {
                            if ($datasheetLocationDetailstions[0]['control_activity_value']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['control_activity_value'] . "],";
                        }
                        else if ($corrosion_type_entities['bst_id'] == 8 && $corrosion_type_entities['bdt_id'] == 1) {
                            if ($datasheetLocationDetailstions[0]['on_potential']) $datasheetLocationDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['on_potential'] . "],";
                            if ($datasheetLocationDetailstions[0]['off_potential']) $datasheetLocationDetails2 .= '[Date.UTC(' . $newdate . '),' . $datasheetLocationDetailstions[0]['off_potential'] . "],";
                        }

                        if ($corrosion_type_entities['ba_controlgreenrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlgreenrange'])) {
                            $greenThreshold = $corrosion_type_entities['ba_controlgreenrange'];
                        }
                        elseif ( $corrosion_type_entities['ba_controlgreen'] != "N/A" && !empty($corrosion_type_entities['ba_controlgreen'])) {
                            $greenThreshold = $corrosion_type_entities['ba_controlgreen'];
                        }
                        else {
                            $greenThreshold = "";
                        }

                        if ($corrosion_type_entities['ba_controlredrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlredrange'])) {
                            $redThreshold = $corrosion_type_entities['ba_controlredrange'];
                        }
                        elseif ( $corrosion_type_entities['ba_controlred'] != "N/A" && !empty($corrosion_type_entities['ba_controlred'])) {
                            $redThreshold = $corrosion_type_entities['ba_controlred'];
                        }
                        else {
                            $redThreshold = "";
                        }

                        if ($corrosion_type_entities['ba_controlamberrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlamberrange'])) {
                            $amberThreshold = $corrosion_type_entities['ba_controlamberrange'];
                        }
                        elseif ( $corrosion_type_entities['ba_controlamber'] != "N/A" && !empty($corrosion_type_entities['ba_controlamber'])) {
                            $amberThreshold = $corrosion_type_entities['ba_controlamber'];
                        }
                        else {
                            $amberThreshold = "";
                        }

                        $showRedFlag = 0;
                        $showGreenFlag = 0;
                        $showAmberFlag = 0;
                        if ($greenThreshold != "")
                        {
                            $showGreenFlag = 1;

                            if ($redThreshold != "") {
                                $showRedFlag = 1;
                            }

                            if ( $greenThreshold == $redThreshold )
                            {
                                $showRedFlag = 2;
                                $showGreenFlag = 0;
                            }

                        }

                        if ($amberThreshold != "")
                        {
                            $showAmberFlag = 1;

                            if ($redThreshold != "") {
                                $showRedFlag = 1;
                            }

                            if ( $amberThreshold == $redThreshold )
                            {
                                $showRedFlag = 2;
                                $showAmberFlag = 0;
                            }

                            if ( $amberThreshold == $greenThreshold )
                            {
                                $showAmberFlag = 0;
                                $showGreenFlag = 1;
                            }

                        }

                        if($showRedFlag) $redGraphData .= '[Date.UTC(' . $newdate . '),' . $redThreshold . "],";
                        if($showGreenFlag) $greenGraphData .= '[Date.UTC(' . $newdate . '),' . $greenThreshold . "],";
                        if($showAmberFlag) $amberGraphData .= '[Date.UTC(' . $newdate . '),' . $amberThreshold . "],";
                    }

                $datasheetLocationDetails.= "]";
                $datasheetLocationDetails2.= "]";
                $datasheetLocationDetails3.= "]";
                $redGraphData.= "]";
                $greenGraphData.= "]";
                $amberGraphData.= "]";

                $datasheetLocationDetails.= "|".$redGraphData."|".$greenGraphData."|".$amberGraphData."|".$datasheetLocationDetails2."|".$datasheetLocationDetails3;

                return new Response( $datasheetLocationDetails);

            }
            else
            {
                //throw new \Exception("No data to plot graph!");
                return $this->render('AIECmsBundle:Default:custom_error.html.twig', array(
                    'message' => 'No data to plot graph'
                ));
            }

        }

    }

    /**
     * Graph the production datasheet SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/{ids}/graphcorrosioninhibitor", name="cms_subproject_corrosion_datasheet_graph_corrosioninhibitor")
     * @Method("GET")
     * @Template()
     */
    public function graphcorrosioninhibitorAction(Request $request, $id, $types, $ids, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $passids = $ids;

        $datasheetLocationsToRetrieve = \GuzzleHttp\json_decode($ids);

        if ($request && $projectId && $subprojectId)
        {
            $ids = str_replace('false',0,$ids);

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = "";

            if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 1)
            {
                $activityTitle = $activity->getControltitle();
            }
            else if ($activity->getIsManagement() == 0 && $activity->getIsControl() == 1)
            {
                $activityTitle = $activity->getControltitle();
            }
            else if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 0)
            {
                $activityTitle = $activity->getManagementtitle();
            }
            else if ($activity->getIsManagement() == 0 && $activity->getIsnonmonitoringkpi() == 1 && $activity->getIsControl() == 0)
            {
                $activityTitle = $activity->getManagementtitle();
            }

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
                $type = "ca-cd-ch";
                $dbname = "SubProjectChemicalDosageDatabase";
            }
            else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
                $type = "ca-cd-oh";
                $dbname = "SubProjectChemicalDosageDatabase";
            }

            if ($corrosion_type_entities['bdt_id'] == 2) {

                $corrosion_monitoring_entity = $em->getRepository('AIECmsBundle:'.$dbname)->findOneBy(array('activityid'=> $id,'subproject'=> $subprojectId));

                if (!$corrosion_type_entities)
                {
                    //throw new \Exception("Not found corrosion type entity!");
                    return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                        'message' => 'Not found corrosion type entity!'
                    ]);
                }
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            if (!empty($corrosionentities))
            {
                $chemicalActivities = array();

                $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $dbSystems['id']);

                $passGraphdata = array();

                foreach ($corrosionentities as $index => $datasheetLocationDetailstions)
                {
                    $passGraphdata[$datasheetLocationDetailstions['id']]['analysis'] = $datasheetLocationDetailstions['analysis'];
                    $passGraphdata[$datasheetLocationDetailstions['id']]['recommendations'] = $datasheetLocationDetailstions['recommendations'];

                    if (!empty($datasheetLocationDetailstions['from_date_normal_injection']) && $datasheetLocationDetailstions['from_date_normal_injection']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_injection'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['from_date_normal_injection']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_injection'] = null;
                    }
                    if (!empty($datasheetLocationDetailstions['to_date_normal_injection']) && $datasheetLocationDetailstions['to_date_normal_injection']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_injection'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['to_date_normal_injection']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_injection'] = null;
                    }

                    if (!empty($datasheetLocationDetailstions['from_date_normal_concentration']) && $datasheetLocationDetailstions['from_date_normal_concentration']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_concentration'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['from_date_normal_concentration']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_concentration'] = null;
                    }
                    if (!empty($datasheetLocationDetailstions['to_date_normal_concentration']) && $datasheetLocationDetailstions['to_date_normal_concentration']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_concentration'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['to_date_normal_concentration']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_concentration'] = null;
                    }

                    if (!empty($datasheetLocationDetailstions['from_date_normal_availability']) && $datasheetLocationDetailstions['from_date_normal_availability']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_availability'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['from_date_normal_availability']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_normal_availability'] = null;
                    }
                    if (!empty($datasheetLocationDetailstions['to_date_normal_availability']) && $datasheetLocationDetailstions['to_date_normal_availability']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_availability'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['to_date_normal_availability']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_normal_availability'] = null;
                    }


                    if (!empty($datasheetLocationDetailstions['from_date_average']) && $datasheetLocationDetailstions['from_date_average']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_average'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['from_date_average']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['from_date_average'] = null;
                    }
                    if (!empty($datasheetLocationDetailstions['to_date_average']) && $datasheetLocationDetailstions['to_date_average']->format("Y-m-d") != '-0001-11-30') {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_average'] = date('Y-m-d', strtotime('-1 months', strtotime($datasheetLocationDetailstions['to_date_average']->format('Y-m-d'))));
                    }else{
                        $passGraphdata[$datasheetLocationDetailstions['id']]['to_date_average'] = null;
                    }

                    if ( $corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 4 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_controlactivity', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true);
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $corrosion_type_entities['bst_id'] == 3 ) {
                        $passGraphdata[$datasheetLocationDetailstions['id']]['platformorplant'] = $datasheetLocationDetailstions['platformorplant'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['location'] = $datasheetLocationDetailstions['location'];
                        $passGraphdata[$datasheetLocationDetailstions['id']]['callback_url']= $this->generateUrl('cms_subproject_corrosion_datasheet_graph_corrosioninhibitorparse', array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'id'=>$id,'datasheetlocationid'=>$datasheetLocationDetailstions['id'],'ids'=>$ids,'types'=>$types), true);
                    }

                }
                if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TREND_DATASHEETS_GRAPH_DETAILS_EDIT',$this->userRolesMergeToCheck)){
                    $this->showMitStatTrendDatasheetsGrDetEdit = 1;
                }

                return [
                    'corrosionDatasheetLocations' => $passGraphdata,
                    'projectId' => $projectId,
                    'subprojectId' => $subprojectId,
                    'activityId' => $id,
                    'corrosionType' => $type,
                    'corrosionDetails' => $corrosion_type_entities,
                    'corrosion_monitoring_entity' => $corrosion_monitoring_entity,
                    'ids' => $passids,
                    'pageType' => $types,
                    'chemicalActivities' => $chemicalActivities,
                    'activityTitle' => $activityTitle,
                    'showMitStatTrendDatasheetsGrDetEdit' => $this->showMitStatTrendDatasheetsGrDetEdit,
                    'systemId' => $request->query->get('systemId'),
                ];
            }
            else
            {
                //throw new \Exception("No data to plot graph!");
                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'No data to plot graph!'
                ]);
            }

        }

    }

    /**
     * Graph the production datasheet SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/{datasheetlocationid}/graphcorrosioninhibitorparse", name="cms_subproject_corrosion_datasheet_graph_corrosioninhibitorparse")
     * @Method("POST")
     * @Template()
     */
    public function graphcorrosioninhibitorparseAction(Request $request, $id, $types, $datasheetlocationid, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        if( $request)
        {

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if (!$corrosion_type_entities)
            {
                //throw new \Exception("Not Found Sub Project Activity Entity!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'Not Found Sub Project Activity Entity!'
                ]);
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetlocationid, $subprojectId);

            if (!empty($corrosionentities))
            {

                $chemicalActivities = array();

                $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                //$chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $dbSystems['id']);

                $greenVolThreshold = "";
                $amberVolThreshold = "";
                $redVolThreshold = "";

                if ($corrosion_type_entities['ba_controlgreenrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlgreenrange'])) {
                    $greenThreshold = $corrosion_type_entities['ba_controlgreenrange'];
                    $greenVolThreshold = $corrosion_type_entities['ba_controlgreenrange'];
                }
                elseif ( $corrosion_type_entities['ba_controlgreen'] != "N/A" && !empty($corrosion_type_entities['ba_controlgreen'])) {
                    $greenThreshold = $corrosion_type_entities['ba_controlgreen'];
                    $greenVolThreshold = $corrosion_type_entities['ba_controlgreen'];
                }
                else {
                    $greenThreshold = "";
                }

                if ($corrosion_type_entities['ba_controlredrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlredrange'])) {
                    $redThreshold = $corrosion_type_entities['ba_controlredrange'];
                    $redVolThreshold = $corrosion_type_entities['ba_controlredrange'];
                }
                elseif ( $corrosion_type_entities['ba_controlred'] != "N/A" && !empty($corrosion_type_entities['ba_controlred'])) {
                    $redThreshold = $corrosion_type_entities['ba_controlred'];
                    $redVolThreshold = $corrosion_type_entities['ba_controlred'];
                }
                else {
                    $redThreshold = "";
                }

                if ($corrosion_type_entities['ba_controlamberrange'] != "N/A" && !empty($corrosion_type_entities['ba_controlamberrange'])) {
                    $amberThreshold = $corrosion_type_entities['ba_controlamberrange'];
                    $amberVolThreshold = $corrosion_type_entities['ba_controlred'];
                }
                elseif ( $corrosion_type_entities['ba_controlamber'] != "N/A" && !empty($corrosion_type_entities['ba_controlamber'])) {
                    $amberThreshold = $corrosion_type_entities['ba_controlamber'];
                    $amberVolThreshold = $corrosion_type_entities['ba_controlred'];
                }
                else {
                    $amberThreshold = "";
                }

                $ciPercentageAvailability = "[";
                $ciInjectionLitre = "[";

                //multi
                $targetCiConcentrationPpm = "[";
                $actualCiConcentrationPpm = "[";
                $volInjectedVsVolReqPercentage = "[";
                $perTimeCiTargetReachPercentage = "[";

                $redVolThresholdGraphData = "[";
                $greenVolThresholdGraphData = "[";
                $amberVolThresholdGraphData = "[";

                $redThresholdGraphData = "[";
                $greenThresholdGraphData = "[";
                $amberThresholdGraphData = "[";

                $datasheetLocationDetails = $types."|".$datasheetlocationid."|".$corrosion_type_entities['ba_controltitle']."|";

                foreach ($corrosionentities as $index => $datasheetProductionDetails) {
                    if ($corrosion_type_entities['bdt_id'] == 2 && $types == "normal") {
                        $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetProductionDetails[0]['retrieval_date']->format('Y-m-d'))));

                        if ($datasheetProductionDetails[0]['target_ci_dosage_litre_per_day'] != 0) $ciInjectionLitre.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['target_ci_dosage_litre_per_day'] . "],";
                        if ($datasheetProductionDetails[0]['target_ci_concentration_ppm'] != 0) $targetCiConcentrationPpm.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['target_ci_concentration_ppm'] . "],";
                        //multi
                        if ($datasheetProductionDetails[0]['actual_ci_concentration_ppm'] != 0) $actualCiConcentrationPpm.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['actual_ci_concentration_ppm'] . "],";
                        if ($datasheetProductionDetails[0]['ci_percentage_availability'] != 0) $ciPercentageAvailability.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['ci_percentage_availability'] . "],";
                    }
                    else if ($corrosion_type_entities['bdt_id'] == 2 && $types == "average") {
                        $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetProductionDetails[0]['to_date']->format('Y-m-d'))));

                        if ($datasheetProductionDetails[0]['vol_inject_vs_vol_req_percentage'] != 0) $volInjectedVsVolReqPercentage.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['vol_inject_vs_vol_req_percentage'] . "],";
                        if ($datasheetProductionDetails[0]['per_time_ci_target_reach_percentage'] != 0) $perTimeCiTargetReachPercentage.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails[0]['per_time_ci_target_reach_percentage'] . "],";
                    }
                    $showGreenFlag = 0;
                    $showAmberFlag = 0;
                    $showRedFlag = 0;
                    if ($greenThreshold != "")
                    {
                        $showGreenFlag = 1;

                        if ($redThreshold != "") {
                            $showRedFlag = 1;
                        }

                        if ( $greenThreshold == $redThreshold )
                        {
                            $showRedFlag = 2;
                            $showGreenFlag = 0;
                        }

                    }

                    if ($amberThreshold != "")
                    {
                        $showAmberFlag = 1;
                        if ($redThreshold != "") {
                            $showRedFlag = 1;
                        }

                        if ( $amberThreshold == $redThreshold )
                        {
                            $showRedFlag = 2;
                            $showAmberFlag = 0;
                        }

                        if ( $amberThreshold == $greenThreshold )
                        {
                            $showAmberFlag = 0;
                            $showGreenFlag = 1;
                        }

                    }

                    $showGreenVolFlag = 0;
                    $showAmberVolFlag = 0;
                    $showRedVolFlag=0;
                    if ($greenVolThreshold != "")
                    {
                        $showGreenVolFlag = 1;

                        if ($redVolThreshold != "") {
                            $showRedVolFlag = 1;
                        }

                        if ( $greenVolThreshold == $redVolThreshold )
                        {
                            $showRedVolFlag = 2;
                            $showGreenVolFlag = 0;
                        }

                    }

                    if ($amberVolThreshold != "")
                    {
                        $showAmberVolFlag = 1;
                        if ($redVolThreshold != "") {
                            $showRedVolFlag = 1;
                        }

                        if ( $amberVolThreshold == $redVolThreshold )
                        {
                            $showRedVolFlag = 2;
                            $showAmberVolFlag = 0;
                        }

                        if ( $amberVolThreshold == $greenVolThreshold )
                        {
                            $showAmberVolFlag = 0;
                            $showGreenVolFlag = 1;
                        }

                    }

                    if($showRedFlag) $redThresholdGraphData.= '[Date.UTC(' . $newdate . '),' . $redThreshold . "],";

                    if($showGreenFlag) $greenThresholdGraphData .= '[Date.UTC(' . $newdate . '),' . $greenThreshold . "],";

                    if ($showAmberFlag) $amberThresholdGraphData.= '[Date.UTC(' . $newdate . '),' . $amberThreshold . "],";

                    if ($showRedVolFlag) $redVolThresholdGraphData.= '[Date.UTC(' . $newdate . '),' . $redVolThreshold . "],";
                    if ($showGreenVolFlag) $greenVolThresholdGraphData.= '[Date.UTC(' . $newdate . '),' . $greenVolThreshold . "],";
                    if ($showAmberVolFlag) $amberVolThresholdGraphData.= '[Date.UTC(' . $newdate . '),' . $amberVolThreshold . "],";
                }

                $targetCiConcentrationPpm.= "]";
                $actualCiConcentrationPpm.= "]";
                $ciPercentageAvailability.= "]";
                $ciInjectionLitre.= "]";
                $volInjectedVsVolReqPercentage.= "]";
                $perTimeCiTargetReachPercentage.= "]";

                $redVolThresholdGraphData.= "]";
                $greenVolThresholdGraphData.= "]";
                $amberVolThresholdGraphData.= "]";

                $redThresholdGraphData.= "]";
                $greenThresholdGraphData.= "]";
                $amberThresholdGraphData.= "]";

                if ($corrosion_type_entities['bdt_id'] == 2 && $types == "normal") $datasheetLocationDetails.= $actualCiConcentrationPpm."|".$targetCiConcentrationPpm."|".$ciPercentageAvailability."|".$ciInjectionLitre."|".$redThresholdGraphData."|".$greenThresholdGraphData."|".$amberThresholdGraphData;
                else if ($corrosion_type_entities['bdt_id'] == 2 && $types == "average") $datasheetLocationDetails.= $volInjectedVsVolReqPercentage."|".$perTimeCiTargetReachPercentage."|".$redVolThresholdGraphData."|".$greenVolThresholdGraphData."|".$amberVolThresholdGraphData;

                return new Response( $datasheetLocationDetails);

            }
            else
            {
                //throw new \Exception("No data to plot graph!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'No data to plot graph!'
                ]);
            }

        }

    }

    /**
     * Graph the production datasheet SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/{ids}/graphcorrosioninhibitoraverage", name="cms_subproject_corrosion_datasheet_graph_four")
     * @Method("GET")
     * @Template()
     */
    public function graphcorrosioninhibitoraverageAction(Request $request, $id, $types, $ids, $projectId, $subprojectId)
    {

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        if( $request)
        {
            $datasheetLocationsToRetrieve = \GuzzleHttp\json_decode($ids);

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if (!$corrosion_type_entities)
            {
                //throw new \Exception("Not Found Sub Project Activity Entity!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'Not Found Sub Project Activity Entity!'
                ]);
            }

            $corrosionentities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByIds($types, $database->getDatabasetype()->getId(), $id, $datasheetLocationsToRetrieve, $subprojectId);

            if (!empty($corrosionentities))
            {
                $chemicalActivities = array();

                $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $dbSystems['id']);

                $volInjection = "[";
                $perTimeCi = "[";

                foreach ($corrosionentities as $index => $datasheetProductionDetails) {
                    if ($corrosion_type_entities['bdt_id'] == 2) {
                        $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetProductionDetails['retrieval_date']->format('Y-m-d'))));
                        $volInjection.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['vol_inject_vs_vol_req_percentaga'] . "],";
                        $perTimeCi.= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['per_time_ci_target_reach_percentage'] . "],";
                    }
                }

                $volInjection.= "]";
                $perTimeCi.= "]";

                return [
                    'volInjectionData' => $volInjection,
                    'perTimeCiData' => $perTimeCi,
                    'projectId' => $projectId,
                    'subprojectId' => $subprojectId,
                    'activityId' => $id,
                    'corrosionType' => "ca-cd-ch",
                    'corrosionDetails' => $corrosion_type_entities,
                    'ids' => $ids,
                    'chemicalActivities' => $chemicalActivities,
                    'systemId' => $request->query->get('systemId'),
                ];

            }
            else
            {
                //throw new \Exception("No data to plot graph!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'No data to plot graph!'
                ]);
            }

        }

    }

    /**
     * Graph the production datasheet SubProjectCorrosionDatasheet
     *
     * @Route("/{id}/{types}/graphproduction", name="cms_subproject_corrosion_datasheet_graph_production")
     * @Method("POST")
     * @Template()
     */
    public function graphproductionAction(Request $request, $id, $types, $projectId, $subprojectId)
    {
        ini_set('memory_limit','-1');
        $em = $this->getManager();

        if ($request->isMethod('POST') )
        {

            $ids = $request->request->get('hidGroupId');

            $datasheetLocationsToRetrieve = \GuzzleHttp\json_decode($ids);

            $activity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

            $activityTitle = "";

            if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 1)
            {
                $activityTitle = $activity->getControltitle();
            }
            else if ($activity->getIsManagement() == 0 && $activity->getIsControl() == 1)
            {
                $activityTitle = $activity->getControltitle();
            }
            else if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 0)
            {
                $activityTitle = $activity->getManagementtitle();
            }
            else if ($activity->getIsManagement() == 0 && $activity->getIsnonmonitoringkpi() == 1 && $activity->getIsControl() == 0)
            {
                $activityTitle = $activity->getManagementtitle();
            }

            $database = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($activity->getSubprojectmetrics()->getId());

            $corrosion_type_entities = $em->getRepository('AIECmsBundle:SubProjectActivity')->getActivityMetrics($subprojectId, $database->getDatabasetype()->getId(), $id);

            if (!$corrosion_type_entities)
            {
                //throw new \Exception("Not Found Sub Project Activity Entity!");
                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'Not Found Sub Project Activity Entity!'
                ]);
            }

            foreach($datasheetLocationsToRetrieve as $index =>$ids)
            {
                $corrosionentities[$ids] = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getProductionHistoryByIds('production', $id, $ids, $subprojectId);
            }




//            $newArray = array();
//            foreach($corrosionentities as $array) {
//                foreach($array as $k=>$v) {
//                    $newArray[$k] = $v;
//                }
//            }
//            $corrosionentities = $newArray;

            $datasheetProductionDetailsPass = array();

            if (!empty($corrosionentities))
            {
                $templocationid = 0;

                $tempdate= 0;

                $gasDetails = $waterDetails =  $condensateDetails = $oilDetails = $totalDetails = $h2sDetails = $co2Details = "";

                foreach ($corrosionentities as $index => $inner)
                {

                    if ($templocationid != $index)
                    {
                        $gasDetails = $waterDetails =  $condensateDetails = $oilDetails = $totalDetails = $h2sDetails = $co2Details = "";
                    }
                    foreach($inner as $inneridx => $datasheetProductionDetails)
                    {

                            // …
                            if (!empty($datasheetProductionDetails['retrieval_date']) && isset($datasheetProductionDetails['retrieval_date'])) {

                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['retrieval_date'] = new \DateTime($datasheetProductionDetails['retrieval_date']);
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['location'] = $datasheetProductionDetails['location'];
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['train'] = $datasheetProductionDetails['train'];
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['fieldmanifold'] = $datasheetProductionDetails['fieldmanifold'];
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['stationmanifold'] = $datasheetProductionDetails['stationmanifold'];
                                $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['retrieval_date']->format('Y-m-d'))));
                                if ($datasheetProductionDetails['gas_production_mmscf_per_day'] != NULL) $gasDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['gas_production_mmscf_per_day'] . "],";
                                if ($datasheetProductionDetails['water_production_bbl_per_day'] != NULL) $waterDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['water_production_bbl_per_day'] . "],";
                                if ($datasheetProductionDetails['condensate_production_bbl_per_day'] != NULL) $condensateDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['condensate_production_bbl_per_day'] . "],";
                                if ($datasheetProductionDetails['oil_production_bbl_per_day'] != NULL) $oilDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['oil_production_bbl_per_day'] . "],";
                                if ($datasheetProductionDetails['total_production_bbl_per_day'] != NULL) $totalDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['total_production_bbl_per_day'] . "],";

                                if ($datasheetProductionDetails['h2s_mol_percentage'] != NULL) $h2sDetails .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['h2s_mol_percentage'] . "],";
                                if ($datasheetProductionDetails['co2_mol_percentage'] != NULL) $co2Details .= '[Date.UTC(' . $newdate . '),' . $datasheetProductionDetails['co2_mol_percentage'] . "],";

                                //$datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['graphData'] =  $gasDetails . "|" . $waterDetails . "|" . $condensateDetails . "|". $oilDetails . "|". $totalDetails. "|". $h2sDetails . "|". $co2Details."]";
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['gasData'] = $gasDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['waterData'] = $waterDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['condensateData'] = $condensateDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['oilData'] = $oilDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['totalData'] = $totalDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['h2sData'] = $h2sDetails;
                                $datasheetProductionDetailsPass[$datasheetProductionDetails['dbactivitylocationid']]['co2Data'] = $co2Details;



                            }


                    }
                    $templocationid = $index;

                }

                foreach ($datasheetProductionDetailsPass as $index => $datasheetProductionDetails)
                {
                    $datasheetProductionDetailsPass[$index]['graphData'] =  "[". $datasheetProductionDetails['gasData']."]" . "|" . "[". $datasheetProductionDetails['waterData'] . "]" . "|" . "[". $datasheetProductionDetails['condensateData'] . "]" . "|" . "[". $datasheetProductionDetails['oilData'] . "]" . "|" . "[". $datasheetProductionDetails['totalData'] . "]" . "|" . "[". $datasheetProductionDetails['h2sData'] . "]" . "|" . "[". $datasheetProductionDetails['co2Data'] . "]";
                }

                return [
                    'projectId' => $projectId,
                    'subprojectId' => $subprojectId,
                    'activityId' => $id,
                    'corrosionType' => "pr",
                    'corrosionDetails' => $corrosion_type_entities,
                    'ids' => $ids,
                    'activityTitle' => $activityTitle,
                    'datasheetProductionDetails' => $datasheetProductionDetailsPass,
                    'systemId' => $request->query->get('systemId'),
                ];

            }
            else
            {
                //throw new \Exception("No data to plot graph!");

                return $this->render('AIECmsBundle:Default:custom_error.html.twig', [
                    'message' => 'No data to plot graph!'
                ]);
            }

        }

    }


    function array_flatten($array) {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            }
            else {
                $result[$key] = $value;
            }
        }
        return $result;
    }


    /**
     * Edits an existing SubProjectCorrosionDatasheet entity.
     *
     * @Route("/{id}", name="cms_subproject_corrosiondatasheet_save")
     * @Method("POST")
     * @Template("")
     */
    public function corrosionactivityupdateAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->query->get('token') !== $this->get('security.csrf.token_manager')->getToken('intention')->getValue()) {
            throw new \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException('Invalid CSRF token');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $entity = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        if($request->isMethod( 'POST' )) {

            $description = $request->request->get('description');
            $selectedperformedactivity = $request->request->get('selectedperformedactivity');

            if ($selectedperformedactivity) {

                $performVeryOuter = explode(",",$selectedperformedactivity);

                if (count($performVeryOuter))
                {
                    foreach ($performVeryOuter as $perform)
                    {
                        if (!empty($perform) && strstr($perform, '_'))
                        {
                            $performOuter = explode("_", $perform);
                            if ($performOuter[0] && $performOuter[1])
                            {
                                $subentity = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->findOneBy(array('subproject' => $subprojectId, 'activityid' => $id, 'dbactivitylocations' => $performOuter[1], 'id' => $performOuter[2]));
                                //echo $subentity->getId();
                                $subentity->setActivityPerformed($performOuter[0]);
                                $em->persist($subentity);
                            }
                        }
                    }
                }
            }

            $entity->setDescription($description);

            $em->persist($entity);

            $em->flush();
            $em->clear();

            $this->addFlash('success', 'Your changes have been saved');

            return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId, 'systemId' => $request->query->get('systemId'),]));
        }

        return [
            'entity' => $entity
        ];
    }

    public function activityTitle($activity)
    {
        if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 1) {
            $activityTitle = $activity->getControltitle();
        } else if ($activity->getIsManagement() == 0 && $activity->getIsControl() == 1) {
            $activityTitle = $activity->getControltitle();
        } else if ($activity->getIsManagement() == 1 && $activity->getIsControl() == 0) {
            $activityTitle = $activity->getManagementtitle();
        }
        else if ($activity->getIsflowassuranceManagement() == 1 && $activity->getIsflowassuranceControl() == 1) {
            $activityTitle = $activity->getControltitle();
        }
        else if ($activity->getIsflowassuranceManagement() == 0 && $activity->getIsflowassuranceControl() == 1) {
            $activityTitle = $activity->getControltitle();
        }
        else if ($activity->getIsflowassuranceManagement() == 1 && $activity->getIsflowassuranceControl() == 0) {
            $activityTitle = $activity->getManagementtitle();
        }
        else if ($activity->getIsManagement() == 0 && $activity->getIsnonmonitoringkpi() == 1 && $activity->getIsControl() == 0) {

            if ($activity->getControltitle() != 'N/A') {
                $activityTitle = $activity->getControltitle();
            }
            else
            {
                $activityTitle = $activity->getManagementtitle();
            }
        }

        return $activityTitle;
    }

    public function removeTrailingAddZero($rate)
    {
        $rate = rtrim($rate, '0');
        if (strrpos($rate, '.') == strlen($rate) - 1) {
            return $rate = rtrim($rate, '.');
        }
        return $rate;
    }

    public function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
        $reference_array = array();

        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
    }

}
