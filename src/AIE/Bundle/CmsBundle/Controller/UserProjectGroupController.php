<?php

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\UserProjectGroup;
use AIE\Bundle\CmsBundle\Form\UserProjectGroupEditType;
use AIE\Bundle\CmsBundle\Form\UserProjectGroupType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * UserProjectGroup controller.
 *
 * @Route("user/{userId}/projects")
 */
class UserProjectGroupController extends CmsBaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all UserProjectGroup entities.
     *
     * @Route("/", name="ccm_userprojectgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($userId) {

        $em = $this->getManager();

        $anomalyEm = $this->get('doctrine')->getManager('cms');

        //$entities = $anomalyEm->getRepository('AIECmsBundle:UserProjectGroup')->findByUser($userId);

        $entities = $this->get('aie_ccm.user.helper')->getUserCcmGroupProjects($userId);

        $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->find($userId);
       
        $pasdata = [];

        $loop =0;


        //$entities = $this->array_unique_multidimensional($entities);
        $tempGidPid = [];

        if ($entities){
            foreach ( $entities as $index => $entity ) {

                $findByAvailability = $anomalyEm->getRepository('AIECmsBundle:UserProjectGroup')->findOneBy(array('user' => $userId, 'group' => $entities[$index]['g_gid'], 'project' => $entities[$index]['p_id']));

                if ($findByAvailability) {

                    $chkDuplicateExists = $entities[$index]['g_gid']."_".$entities[$index]['p_id'];

                    if (  !in_array($chkDuplicateExists,$tempGidPid)){

                        $pasdata[$loop]['id'] = $entities[$index]['ug_id'];
                        $pasdata[$loop]['groupname'] = $entities[$index]['g_name'];
                        $pasdata[$loop]['projectname'] = $entities[$index]['p_name'];
                        $pasdata[$loop]['roles'] = $entities[$index]['g_roles'];
                       
                        $pasdata[$loop]['form'] = $this->createDeleteForm($pasdata[$loop]['id'], $userId)->createView();
                        $tempGidPid[] = $entities[$index]['g_gid']."_".$entities[$index]['p_id'];

                        $loop++;
                      
                    }
                   
                }
                
            }
        }
        

        return array(
            'entities' => array_unique($pasdata,SORT_REGULAR),
            'userId' => $userId,
            'user' => $user
        );
        
    }

    /**
     * Creates a form to delete a UserProjectGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $userId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ccm_userprojectgroup_delete', array('userId' => $userId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')), 'btn'))
            ->getForm();
    }

    function array_unique_multidimensional($input)
    {
        $serialized = array_map('serialize', $input);
        $unique = array_unique($serialized);
        return array_intersect_key($input, $unique);
    }

    /**
     * Lists all UserProjectGroup entities.
     *
     * @Route("/", name="ccm_userprojectgroup_ajax")
     * @Method("POST")
     * @Template()
     */
    public function ajaxAction(Request $request, $userId) {
        $em = $this->getManager();

        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        // Get the province ID
        $id = $request->query->get('project_id');
        $result = array();
        // Return a list of cities, based on the selected province
        $projects = $em->getRepository('AIECmsBundle:Projects')->find($id);
        foreach ($projects->getSubprojects() as $subproject) {
            $result[$subproject->getName()] = $subproject->getId();
        }

        return new JsonResponse($result);
    }

    /**
     * Creates a new UserProjectGroup entity.
     *
     * @Route("/create", name="ccm_userprojectgroup_create")
     * @Method("POST")
     * @Template("AIECmsBundle:UserProjectGroup:new.html.twig")
     */
    public function createAction(Request $request, $userId) {
        $entity = new UserProjectGroup();


        $form = $this->createCreateForm($entity, $userId);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $cmsEm = $this->get('doctrine')->getManager('cms');
            $user = $em->getRepository('UserBundle:User')->find($userId);

            $entity->setUser($user);

            $cmsEm->merge($entity);
            $cmsEm->flush();

            return $this->redirect($this->generateUrl('ccm_userprojectgroup', array('userId' => $userId)));
        }
        else{
            return array(
                'entity' => $entity,
                'form' => $form->createView(),
                'userId' => $userId
            );
        }


    }

    /**
     * Creates a form to create a UserProjectGroup entity.
     *
     * @param UserProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UserProjectGroup $entity, $userId) {
        $cmsEm = $this->get('doctrine')->getManager('cms');
        $form = $this->createForm(new UserProjectGroupType($cmsEm), $entity, array(
            'action' => $this->generateUrl('ccm_userprojectgroup_create', array('userId' => $userId)),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new UserProjectGroup entity.
     *
     * @Route("/new", name="ccm_userprojectgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($userId) {
        $entity = new UserProjectGroup();
        $form = $this->createCreateForm($entity, $userId);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'userId' => $userId
        );
    }

    /**
     * Finds and displays a UserProjectGroup entity.
     *
     * @Route("/{id}", name="ccm_userprojectgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $userId) {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $userId);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UserProjectGroup entity.
     *
     * @Route("/{id}/edit", name="ccm_userprojectgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $userId) {
        $em = $this->get('doctrine')->getManager('cms');

        $entity = $em->getRepository('AIECmsBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $user = $em->getRepository('UserBundle:User')->find($userId);
        $group = $em->getRepository('UserBundle:User')->find($userId);

        $editForm = $this->createEditForm($entity, $userId);
        $deleteForm = $this->createDeleteForm($id, $userId);
        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'userId' => $userId
        );
    }

    /**
     * Creates a form to edit a UserProjectGroup entity.
     *
     * @param UserProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(UserProjectGroup $entity, $userId) {
        $em = $this->getManager();
        $form = $this->createForm(new UserProjectGroupEditType($entity, $em), $entity, array(
            'action' => $this->generateUrl('ccm_userprojectgroup_update', array('userId' => $userId, 'id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing UserProjectGroup entity.
     *
     * @Route("/{id}", name="ccm_userprojectgroup_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:UserProjectGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $userId) {
        $em = $this->getManager('cms');

        $entity = $em->getRepository('AIECmsBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $userId);
        $editForm = $this->createEditForm($entity, $userId);


        if ($request->request->get('aie_bundle_ccmbundle_userprojectgroup')['group'] && $request->request->get('aie_bundle_ccmbundle_userprojectgroup')['_token']) {

            $groupSelected = $em->getRepository('AIECmsBundle:ReflectionCcmGroup')->find($request->request->get('aie_bundle_ccmbundle_userprojectgroup')['group']);

            $qb = $em->createQueryBuilder();

            $userGroupExistcheck = $em->getRepository('AIECmsBundle:UserProjectGroup')->findOneBy(array('user' => $userId, 'project' => $entity->getProject()->getId(), 'group' => $entity->getGroup()->getId()));

            if ($userGroupExistcheck && $groupSelected){

                $q = $qb->update('AIECmsBundle:UserProjectGroup', 'ug')
                        ->set('ug.group', $groupSelected->getGid())
                        ->where('ug.user = ?1')
                        ->andWhere('ug.project = ?2')
                        ->andWhere('ug.group = ?3')
                        ->setParameter(1, $userId)
                        ->setParameter(2, $entity->getProject()->getId())
                        ->setParameter(3, $entity->getGroup()->getId())
                        ->getQuery();

                $p = $q->execute();
            } else{
                throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
            }

            return $this->redirect($this->generateUrl('ccm_userprojectgroup', array('userId' => $userId)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'userId' => $userId,
        );
    }

    /**
     * Deletes a UserProjectGroup entity.
     *
     * @Route("/{id}", name="ccm_userprojectgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $userId) {
        $form = $this->createDeleteForm($id, $userId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:UserProjectGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ccm_userprojectgroup', array('userId' => $userId)));
    }

}
