<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 07/10/2018
 * Time: 11:29 AM
 */

namespace AIE\Bundle\CmsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Exception controller.
 *
 * @Route("/exception")
 */
class ExceptionController extends CmsBaseController
{
    /**
     * Lists all Request entities.
     *
     * @Route("/", name="accessDeniedException")
     * @Method("GET")
     * @Template("AIECmsBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
//        echo("Hello");
        return $this->render(
            'AIEVeracityBundle:Default:accessdenied.html.twig'

        );

    }
    }