<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase as CorrosionMonitoringDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;

use AIE\Bundle\CmsBundle\Form\SubProjectCorrosionMonitoringDatabaseType;
use AIE\Bundle\CmsBundle\Form\SubProjectCorrosionMonitoringDatabaseEditType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * SubProjectCorrosionMonitoringDatabaseController controller.
 *
 * @Route("/{projectId}/subprojectcorrosionmonitoringdatabase/{subprojectId}")
 */
class SubProjectCorrosionMonitoringDatabaseController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = array('serviceid','installeddeviceid','corrosionorientationid','accessfittingtypeid','holderplugid','teefittingid','insulationid','surfacefinishid','onlineid','scaffoldid','accessid');

    Protected $findduplicatelinkedproperties = array('probetechnologyid|dataloggerid','couponmaterialid|densityid','coupontypeid|areaid');

    Protected $entityCheck = "";

    Protected $addGranted = 0;

    Protected $editGranted = 0;

    Protected $deleteGranted = 0;

    Protected $excelGranted = 0;

    Protected $selectcolumnsGranted = 0;

    /**
     * Lists all SubProjectCorrosionMonitoringDatabase entities.
     *
     * @Route("/", name="cms_subproject_corrosionmonitoringdatabase")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request, $projectId, $subprojectId)
    {
        $corrosionmonitoringdatabases = array();
        $subprojectsystems = array();
        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        if ($request->query->get('stepId')) $stepid = $request->query->get('stepId'); else $stepid = 1;

        $subprojectsystems = $cmsDatabasesHelper->getSubProjectAllSystemsDatabaseItems($subprojectId,3);

        $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (isset($subprojectsystems) && !empty($subprojectsystems)) {
            $corrosionmonitoringdatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 3, $subprojectsystems[0]['bs_id']);
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_EXCEL',$this->userRolesMergeToCheck)) {
            $this->excelGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_SELECT_COLUMNS',$this->userRolesMergeToCheck)) {
            $this->selectcolumnsGranted = 1;
        }

        usort($subprojectsystems, function($a, $b) {
            if($a['bs_systemstitle'] == $b['bs_systemstitle']) {
                return 0;
            }
            return ($a['bs_systemstitle'] < $b['bs_systemstitle']) ? -1 : 1;
        });

        return array(
            'systems'                     => $subprojectsystems,
            'corrosionmonitoringdatabases'     => $corrosionmonitoringdatabases,
            //'form'                        => $form->createView(),
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'project'                     => $project,
            'subproject'                  => $subproject,
            'wizard_title'                => 'Coupon/Probe Database',
            'stepId' => $stepid,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,
            'excelGranted' => $this->excelGranted,
            'selectcolumnsGranted' => $this->selectcolumnsGranted,
        );

    }

    /**
     * Lists all SubProjectSystems Headbar for system entities.
     *
     * @Route("/{sid}/loadsystems", name="cms_subproject_corrosionmonitoringdatabase_loadsystem")
     * @Method("GET")
     * @Template()
     */
    public function loadsystemAction(Request $request,$sid, $projectId, $subprojectId)
    {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $systemInfo = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);

        $corrosionmonitoringdatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 3, $sid);

        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_ADD',$this->userRolesMergeToCheck)) {
            $this->addGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_EDIT',$this->userRolesMergeToCheck)) {
            $this->editGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_DELETE',$this->userRolesMergeToCheck)) {
            $this->deleteGranted = 1;
        }

        return $this->render('AIECmsBundle:SubProjectCorrosionMonitoringDatabase:systemcorrosionmonitoringdatabases.html.twig',array(
            'corrosionmonitoringdatabases'  => $corrosionmonitoringdatabases,
            'systemid'=>$sid,
            'systemInfo' => $systemInfo,
            'projectId'=>$projectId,
            'subprojectId'=>$subprojectId,
            'addGranted' => $this->addGranted,
            'editGranted' => $this->editGranted,
            'deleteGranted' => $this->deleteGranted,));

    }

    /**
     * Create the entity SubProjectCorrosionMonitoringDatabase
     *
     * @Route("/{sid}/{stepid}/create", name="cms_subproject_corrosionmonitoringdatabase_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $stepid, $sid, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $entity = new CorrosionMonitoringDatabase();
        $form = $this->createSystemsCorrosionMonitoringDatabaseForm($entity, $projectId, $subprojectId, $sid, $stepid);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $dupFindData = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->findBy(array('subproject'=>$subprojectId, 'location'=>$data->getLocation(), 'activityid' => $data->getActivityid()));

            if (!$dupFindData )
            {

                $file = $data->getFile();
                if ($file) {
                    $uploadedPath = $this->uploadFile($file, $entity);
                    $entity->setFilepath($uploadedPath);
                }

                $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                $entity->setSubproject($subproject);

                $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sid);
                $entity->setSubProjectSystems($subprojectsystems);

                $em->persist($entity);
                $em->flush();

                $this->addFlash('success', 'New entry added to corrosion monitoring database');
            }
            else
            {
                $this->addFlash('warning', 'Location already exists for the activity');
            }

            return $this->redirect($this->generateUrl('cms_subproject_corrosionmonitoringdatabase',array('projectId'=>$projectId,'subprojectId'=>$subprojectId,'stepId' => $stepid,)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Delete the entity SubProjectCorrosionMonitoringDatabase
     *
     * @Route("/{sid}/delete", name="cms_subproject_corrosionmonitoringdatabase_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $sid, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        if($request->isXmlHttpRequest()) {

            if (!$this->get('security.token_storage')->getToken()->getUser()) {
                return new Response(json_encode(array('result' => 'nousertoken')));
            }

            $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

            if ($sid){
                $entity = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->find($sid);
            }

            if (!$entity) {
                return new Response(json_encode(array('result' => 'noentity')));
            }

            $em->remove($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }
    }

    /**
     * Creates a form to create a SubProjectCorrosionMonitoringDatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsCorrosionMonitoringDatabaseForm(CorrosionMonitoringDatabase $entity,$projectId, $subprojectId, $sId, $stepId)
    {

        $em = $this->getDoctrine()->getManager('cms');

        $form = $this->createForm(new SubProjectCorrosionMonitoringDatabaseType($entity, $em, $projectId, $subprojectId, $sId), $entity, [
            'action' => $this->generateURL('cms_subproject_corrosionmonitoringdatabase_create', ['stepid' => $stepId,'sid' => $sId, 'projectId' =>$projectId, 'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Creates a form to create a CorrosionMonitoringDatabasedatabase entity.
     *
     * @param int $projectId, $subprojectId, $sId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSystemsCorrosionMonitoringDatabaseEditForm(CorrosionMonitoringDatabase $entity, $stepId, $sId, $projectId, $subprojectId)
    {

        $em = $this->getManager('cms');

        $form = $this->createForm(
            new SubProjectCorrosionMonitoringDatabaseEditType($entity, $em, $projectId, $subprojectId, $sId),
            $entity,
            [
                'action' => $this->generateUrl('cms_subproject_corrosionmonitoringdatabase_edit', ['id' => $entity->getId(), 'stepid' => $stepId, 'sid' => $sId, 'projectId' => $projectId, 'subprojectId' => $subprojectId]),
                'method' => 'POST',
            ]
        );

        return $form;
    }

    /**
     * Update subprojectcorrosionmonitoringdatabase
     *
     * @Route("/{sid}/updatefields", name="cms_subproject_corrosionmonitoringdatabase_updatefields")
     * @Method("POST")
     * @Template()
     */
    public function updateFieldsAction($sid, Request $request)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest() &&  $request->isMethod( 'POST' )) {

            $title = $request->request->get('title');

            $key = $request->request->get('key');

            if ($title) {
                switch ($key) {
                    case "platformorplant":
                        $entity->setPlatformorplant($title);
                        break;
                    case "pandid":
                        $entity->setPandid($title);
                        break;
                    case "location":
                        $entity->setLocation($title);
                        break;
                    case "stage":
                        $entity->setStage($title);
                        break;
                    case "type":
                        $entity->setType($title);
                        break;
                    case "linedescription":
                        $entity->setLinenumber($title);
                        break;
                    case "linediameterinches":
                        $entity->setLinediameterinches($title);
                        break;
                    case "standoffinches":
                        $entity->setStandoffinches($title);
                        break;
                    case "linematerial":
                        $entity->setLinematerial($title);
                        break;
                    case "operatingpressure":
                        $entity->setOperatingpressure($title);
                        break;
                    case "operatingtemperature":
                        $entity->setOperatingtemperature($title);
                        break;
                    case "installedcap":
                        $entity->setInstalledcap($title);
                        break;
                    case "installedplug":
                        $entity->setInstalledplug($title);
                        break;
                    case "instrumentation":
                        $entity->setInstrumentation($title);
                        break;
                    case "probeextensionadapter":
                        $entity->setProbeextensionadapter($title);
                        break;
                    case "probetypeandmodel":
                        $entity->setProbetypeandmodel($title);
                        break;
                    case "logginginterval":
                        $entity->setLogginginterval($title);
                        break;
                    case "probeinstallationdate":
                        $entity->setProbeinstallationdate($title);
                        break;
                    case "couponholdertagno":
                        $entity->setCouponholdertagno($title);
                        break;
                    case "tool":
                        $entity->setTool($title);
                        break;
                }
            }

            $em->persist($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

    }


    /**
     * Update SubProjectCorrosionMonitoringDatabase
     *
     * @Route("/{stepid}/{sid}/{id}/edit", name="cms_subproject_corrosionmonitoringdatabase_edit")
     * @Method("POST")
     * @Template()
     */
    public function editAction(Request $request, $stepid, $sid, $projectId, $subprojectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Project Corrosion Monitoring Database entity.');
        }

        $editForm = $this->createSystemsCorrosionMonitoringDatabaseEditForm($entity, $stepid, $sid, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Entry has been updated to corrosion monitoring database');

            return $this->redirect($this->generateUrl('cms_subproject_corrosionmonitoringdatabase', ['projectId' => $projectId, 'subprojectId' => $subprojectId,'stepId' => $stepid,]));
        }

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];

    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="cms_subproject_corrosionmonitoringdatabase_corrosionsave")
     * @Method("POST")
     * @Template("")
     */
    public function corrosionfieldsupdateAction(Request $request, $projectId, $subprojectId, $id)
    {

        if ($request->query->get('token') !== $this->get('security.csrf.token_manager')->getToken('intention')->getValue()) {
            throw new \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException('Invalid CSRF token');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $entities = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->findBy(array('activityid'=> $id,'subproject'=> $subprojectId));

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        if($request->isMethod( 'POST' ))
        {

            foreach($entities as $entity) {

                $from = $request->request->get('from_'.$entity->getId());
                $to = $request->request->get('to_'.$entity->getId());

                $analysis = $request->request->get('analysis_'.$entity->getId());
                $recommendation = $request->request->get('recommendation_'.$entity->getId());

                $entity->setAnalysis($analysis);
                $entity->setRecommendations($recommendation);

                if (!strchr($from,"Object") && !strchr($to,"Object")) {

                    $from = new \DateTime($from);
                    $to = new \DateTime($to);

                    $entity->setFrom($from);
                    $entity->setTo($to);
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Changes saved');
            $systemId = $request->query->get('systemId');
            return $this->redirect($this->generateUrl('cms_subproject_corrosion_datasheet_graph', ['id' => $id, 'ids' => $request->query->get('ids'), 'projectId' => $projectId, 'subprojectId' => $subprojectId,'types' => $request->request->get('types'), 'systemId' => $systemId]));
        }

        return [
            'entity' => $entity
        ];
    }

}
