<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\CmsBundle\Entity\Projects;
use AIE\Bundle\CmsBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\CmsBundle\Form\ManagementFilterType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * PipelineAnomaly controller.
 *
 * @Route("projects/{projectId}/management")
 */
class ManagementController extends CmsBaseController {

	public function planningHandler(Request $request, $projectId, $anomalyType, $submitForm) {
		
	}

	private function createFilterForm($projectId, \DateTime $fromDate, \DateTime $toDate, $submitForm) {
		
	}

	/**
	 *
	 * @Route("/planning/master", name="cms_planning_master")
	 * @Method({"GET", "POST"})
	 * @Template("AIECmsBundle:Management:planning.html.twig")
	 */
	public function masterPlanningAction(Request $request, $projectId) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Planning'];
	}

	/**
	 *
	 * @Route("/planning/{anomalyType}", name="cms_planning", requirements={"anomalyType"="(pl|pi|se|st|mr)?"})
	 * @Method({"GET", "POST"})
	 * @Template("AIECmsBundle:Management:planning.html.twig")
	 */
	public function planningAction(Request $request, $projectId) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Planning'];
	}

	/**
	 *
	 * @Route("/reminders/master", name="cms_reminders_master")
	 * @Method({"GET", "POST"})
	 * @Template("AIECmsBundle:Management:reminders.html.twig")
	 */
	public function masterRemindersAction(Request $request, $projectId) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Reminders'];
	}


	/**
	 *
	 * @Route("/reminders/{anomalyType}", name="cms_reminders", requirements={"anomalyType"="(pl|pi|se|st|mr)?"})
	 * @Method({"GET", "POST"})
	 * @Template("AIECmsBundle:Management:reminders.html.twig")
	 */
	public function remindersAction(Request $request, $projectId) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Reminders'];
	}


  
	public function getCmsIsDue()
    {
       
    }

}