<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\ActionTracker;
use AIE\Bundle\CmsBundle\Form\SubProjectActionTrackerType;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * SubProjectSamplingDatabase controller.
 *
 * @Route("/{projectId}/subprojectactiontracker/{subprojectId}")
 */
class SubProjectActionTrackerController extends CmsBaseController
{

    /**
     * Lists all SubProjectActionTrackers entities.
     *
     * @Route("/", name="cms_subproject_actiontracker")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId, $subprojectId)
    {

        $em = $this->getManager();

        $actionTrackers = $em->getRepository('AIECmsBundle:ActionTracker')->findBy(array('subproject'=>$subprojectId));

        $actionShowGranted = 0;
        $actionAddGranted = 0;
        $actionEditGranted = 0;
        $actionSaveGranted = 0;
        $actionCancelGranted = 0;
        $actionDeleteGranted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_SHOW',$this->userRolesMergeToCheck)){
            $actionShowGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ADD',$this->userRolesMergeToCheck)){
            $actionAddGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_EDIT',$this->userRolesMergeToCheck)){
            $actionEditGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_SAVE',$this->userRolesMergeToCheck)){
            $actionSaveGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_CANCEL',$this->userRolesMergeToCheck)){
            $actionCancelGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_DELETE',$this->userRolesMergeToCheck)){
            $actionDeleteGranted = 1;
        }

        return array(
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'actionTrackers'              => $actionTrackers,
            'wizard_title'                => 'Action Tracker',
            'actionShowGranted'           => $actionShowGranted,
            'actionAddGranted'            => $actionAddGranted,
            'actionEditGranted'           => $actionEditGranted,
            'actionSaveGranted'           => $actionSaveGranted,
            'actionCancelGranted'         => $actionCancelGranted,
            'actionDeleteGranted'         => $actionDeleteGranted,
        );

    }

    /**
     * Create
     *
     * @Route("/create", name="cms_subproject_actiontracker_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $entity = new ActionTracker();
        $form = $this->createActionTrackerForm($projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($subproject);

            $entity->setTitle($data->getTitle());
            $entity->setDate(new \DateTime($data->getDate()));


            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'New action tracker created');

            return $this->redirect($this->generateUrl('cms_subproject_actiontracker',array('projectId'=>$projectId,'subprojectId'=>$subprojectId)));
        }

        return array(
            'entity' => $entity, //why this is needed?
            'form'   => $form->createView(),
        );
    }

    /**
     * Delete
     *
     * @Route("/delete/{aid}", name="cms_subproject_actiontracker_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction(Request $request, $aid, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $actiontracker = $em->getRepository('AIECmsBundle:ActionTracker')->find($aid);
        if (!$actiontracker) {
            throw $this->createNotFoundException(
                'No Action Tracker found for id ' . $aid
            );
        }

        $actionTrackerItems =$em->getRepository('AIECmsBundle:ActionTrackerItems')->findBy(array('actionTracker'=>$aid));

        foreach($actionTrackerItems as $item){
            $em->remove($item);
        }
        $em->remove($actiontracker);
        $em->flush();
        $this->addFlash('success', 'Action tracker deleted');

        return $this->redirect($this->generateUrl('cms_subproject_actiontracker',array('projectId'=>$projectId,'subprojectId'=>$subprojectId)));
    }
    /**
     * edit
     *
     * @Route("/{id}/edit", name="cms_subproject_actiontracker_edit")
     * @Method("POST")
     * @Template()
     */
    public function editAction(Request $request, $id, $projectId, $subprojectId)
    {
        $aid = $id;
        $em = $this->getDoctrine()->getManager('cms');
        $entity = $em->getRepository('AIECmsBundle:ActionTracker')->find($aid);
        if (!$entity) {
            throw $this->createNotFoundException(
                'No Action Tracker found for id ' . $aid
            );
        }
        $form = $this->updateActionTrackerForm($projectId, $subprojectId, $entity);
//        $request->attributes->set('aid', $aid);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($subproject);

            $entity->setTitle($data->getTitle());
            $entity->setDate(new \DateTime($data->getDate()));
            $em->flush();

            $this->addFlash('success', ' Action tracker updated');

            return $this->redirect($this->generateUrl('cms_subproject_actiontracker',array('projectId'=>$projectId,'subprojectId'=>$subprojectId)));
        }

        return array(
            'entity' => $entity, //why this is needed?
            'aid' => $aid,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create action tracker entity.
     *
     * @param int $subprojectId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createActionTrackerForm($projectId, $subprojectId)
    {

        $em = $this->getDoctrine()->getManager('cms');

        $entity = new ActionTracker();

        $form = $this->createForm(new SubProjectActionTrackerType($entity), $entity, [
            'action' => $this->generateURL('cms_subproject_actiontracker_create', ['projectId' =>$projectId,'subprojectId' => $subprojectId,]),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Creates a form to create action tracker entity.
     *
     * @param int $subprojectId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function updateActionTrackerForm($projectId, $subprojectId, $entity)
    {

        $em = $this->getDoctrine()->getManager('cms');
        $form = $this->createForm(new SubProjectActionTrackerType($entity), $entity, [
            'action' => $this->generateURL('cms_subproject_actiontracker_edit', ['id' =>$entity->getId(),'projectId' =>$projectId,'subprojectId' => $subprojectId]),
            'method' => 'POST',
        ]);

        return $form;
    }


}
