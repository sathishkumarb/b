<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\Files;
use AIE\Bundle\CmsBundle\Form\FilesType;
use AIE\Bundle\CmsBundle\Form\FilesEditType;
use AIE\Bundle\CmsBundle\Helper;

/**
 * Files controller.
 *
 * @Route("/{projectId}/cms/{subprojectId}/files")
 */
class FilesController extends CmsBaseController {

    Protected $documentShowSearchGranted = 0;
    Protected $documentViewFolderGranted = 0;
    Protected $documentViewDocumentGranted = 0;
    Protected $documentAddFolderGranted = 0;
    Protected $documentAddDocumentGranted = 0;
    Protected $documentEditFolderGranted = 0;
    Protected $documentEditDocumentGranted = 0;
    Protected $documentDeleteFolderGranted = 0;
    Protected $documentDeleteDocumentGranted = 0;

	/**
	 * Lists all Files entities.
	 *
	 * @Route("/", name="cms_files")
	 * @Method("GET")
	 * @Template("AIECmsBundle:Files:index.html.twig")
	 */
	public function indexAction($projectId, $subprojectId) {
		$em = $this->getManager();

		$entities = $em->getRepository('AIECmsBundle:Files')->findBySubproject($subprojectId);

        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_SHOW_SEARCH',$this->userRolesMergeToCheck)) {
            $this->documentShowSearchGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_VIEW_FOLDER',$this->userRolesMergeToCheck)) {
            $this->documentViewFolderGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_VIEW_DOCUMENT',$this->userRolesMergeToCheck)) {
            $this->documentViewDocumentGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_ADD_FOLDER',$this->userRolesMergeToCheck)) {
            $this->documentAddFolderGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_ADD_DOCUMENT',$this->userRolesMergeToCheck)) {
            $this->documentAddDocumentGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_EDIT_FOLDER',$this->userRolesMergeToCheck)) {
            $this->documentEditFolderGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_EDIT_DOCUMENT',$this->userRolesMergeToCheck)) {
            $this->documentEditDocumentGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_DELETE_FOLDER',$this->userRolesMergeToCheck)) {
            $this->documentDeleteFolderGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_DELETE_DOCUMENT',$this->userRolesMergeToCheck)) {
            $this->documentDeleteDocumentGranted = 1;
        }

        return array(
            'entities'   => $entities,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'documentShowSearchGranted'   => $this->documentShowSearchGranted,
            'documentViewFolderGranted' => $this->documentViewFolderGranted,
            'documentViewDocumentGranted' => $this->documentViewDocumentGranted,
            'documentAddFolderGranted'   => $this->documentAddFolderGranted,
            'documentAddDocumentGranted' => $this->documentAddDocumentGranted,
            'documentEditFolderGranted' => $this->documentEditFolderGranted,
            'documentEditDocumentGranted'   => $this->documentEditDocumentGranted,
            'documentDeleteFolderGranted' => $this->documentDeleteFolderGranted,
            'documentDeleteDocumentGranted' => $this->documentDeleteDocumentGranted,
        );
	}

	/**
	 * Creates a new Files entity.
	 *
	 * @Route("/", name="cms_files_create")
	 * @Method("POST")
	 * @Template("AIECmsBundle:Files:new.html.twig")
	 */
	public function createAction(Request $request, $projectId, $subprojectId) {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_ADD_DOCUMENT',$this->userRolesMergeToCheck)) {
            $entity = new Files();

            $form = $this->createCreateForm($entity, $projectId, $subprojectId);
            $form->handleRequest($request);

            $em = $this->getManager();

            if ($form->isValid()) {
                $cms = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
                $entity->setSubproject($cms);
                $data = $form->getData();
                /* Handle Uploaded File */
                $file = $data->getFile();
                $uploadedPath = $this->uploadFile($file, $entity);
                $entity->setFilepath($uploadedPath);
                $entity->setSize($file->getSize());

                $date = $data->getDocumentDate();
                if ($date) {
                    $entity->setDocumentDate(new \DateTime($date));
                }
                $entity->setDate(new \DateTime());

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('cms_files', ['projectId' => $projectId, 'subprojectId' => $subprojectId]));
                //return $this->redirect($this->generateUrl('cms_files_show', array('id' => $entity->getId(), 'refId' => $subprojectId)));
            }

            return [
                'entity' => $entity,
                'route_base' => $this->route_base,
                'layout' => $this->layout,
                'form' => $form->createView(),
            ];
        }
	}

	/**
	 * Creates a form to create a Files entity.
	 *
	 * @param Files $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Files $entity, $projectId, $subprojectId) {
		$form = $this->createForm(new FilesType(), $entity, [
			'action' => $this->generateUrl('cms_files_create', ['projectId' => $projectId, 'subprojectId' => $subprojectId]),
			'method' => 'POST',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Displays a form to create a new Files entity.
	 *
	 * @Route("/new", name="cms_files_new")
	 * @Method("GET")
	 * @Template("AIECmsBundle:Files:new.html.twig")
	 */
	public function newAction($projectId, $subprojectId) {
		$entity = new Files();
		$form = $this->createCreateForm($entity, $projectId, $subprojectId);

		return [
			'entity' => $entity,
			'form'   => $form->createView(),
            'projectId' => $projectId,
			'subprojectId'  => $subprojectId
		];
	}

	/**
	 * Finds and displays a Files entity.
	 *
	 * @Route("/{id}", name="cms_files_show")
	 * @Method("GET")
	 * @Template("AIECmsBundle:Files:show.html.twig")
	 */
	public function showAction($projectId, $subprojectId, $id) {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_VIEW_DOCUMENT',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();

            $entity = $em->getRepository('AIECmsBundle:Files')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Files entity.');
            }

            // $deleteForm = $this->createDeleteForm($id);

            return [
                'entity' => $entity,
                'projectId' => $projectId,
                'subprojectId' => $subprojectId
                //'delete_form' => $deleteForm->createView(),
            ];
        }
        else{
            $this->addFlash('warning', ' You are not permitted to perform this action.');
        }
	}

	/**
	 * Displays a form to edit an existing Files entity.
	 *
	 * @Route("/{id}/edit", name="cms_files_edit", options={"expose"=true})
	 * @Method("GET")
	 * @Template("AIECmsBundle:Files:edit.html.twig")
	 */
	public function editAction($id, $projectId, $subprojectId) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIECmsBundle:Files')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Files entity.');
		}

		$editForm = $this->createEditForm($entity, $projectId, $subprojectId);

		return [
			'entity'      => $entity,
            'projectId' => $projectId,
			'subprojectId'       => $subprojectId,
			'edit_form'   => $editForm->createView(),
		];
	}

	/**
	 * Creates a form to edit a Files entity.
	 *
	 * @param Files $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Files $entity, $projectId, $subprojectId) {
		$form = $this->createForm(new FilesEditType($entity), $entity, [
			'action' => $this->generateUrl('cms_files_update', ['id' => $entity->getId(), 'projectId' => $projectId, 'subprojectId' => $subprojectId]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Edits an existing Files entity.
	 *
	 * @Route("/{id}", name="cms_files_update")
	 * @Method("PUT")
	 * @Template("AIECmsBundle:Files:edit.html.twig")
	 */
	public function updateAction(Request $request, $id, $projectId, $subprojectId) {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_EDIT_DOCUMENT',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();

            $entity = $em->getRepository('AIECmsBundle:Files')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Files entity.');
            }

            $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);
            $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {

                /* Handle Uploaded File */
                $data = $editForm->getData();
                /*$file = $data->getFile();
                $uploadedPath = $this->replaceFile($file, $entity);
                $entity->setFilepath($uploadedPath);
                $entity->setSize($file->getSize());*/

                $date = $data->getDocumentDate();
                if ($date) {
                    $entity->setDocumentDate(new \DateTime($date));
                }

                $em->flush();

                return $this->redirect($this->generateUrl('cms_files', ['projectId' => $projectId, 'subprojectId' => $subprojectId]));
                //return $this->redirect($this->generateUrl('cms_files_edit', array('id' => $id, 'refId' => $subprojectId)));
            }

            return [
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ];
        }
        else
        {
            $this->addFlash('warning', ' You are not permitted to perform this action.');
            return $this->redirect($this->generateUrl('cms_files', ['projectId' => $projectId, 'subprojectId' => $subprojectId]));
        }
	}

	/**
	 * Deletes a Files entity.
	 *
	 * @Route("/{id}/delete", name="cms_files_delete", options={"expose"=true})
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, $projectId, $subprojectId, $id) {
        $response = false;
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_DELETE_DOCUMENT',$this->userRolesMergeToCheck)) {
            //if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:Files')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Files entity.');
            }

            $this->deleteFile($entity);
            $em->remove($entity);
            $em->flush();
            $response = true;

            return new JsonResponse(['status' => 200, 'success' => $response]);
        }
        else{
            return new JsonResponse(['status' => 404, 'success' => $response]);
        }
	}

	/**
	 * Creates a form to delete a Files entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id, $projectId, $subprojectId) {
		return $this->createFormBuilder()
			->setAction($this->generateUrl('cms_files_delete', ['id' => $id, 'projectId' => $projectId, 'subprojectId' => $subprojectId]))
			->setMethod('DELETE')
			->add('submit', 'submit', $this->options(['label' => 'Delete', 'attr' => ['class' => 'right btn-danger']], 'btn'))
			->getForm();
	}

}
