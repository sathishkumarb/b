<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\BaseActivity;
use AIE\Bundle\CmsBundle\Entity\BaseSystems;
use AIE\Bundle\CmsBundle\Entity\BaseThreats;
use AIE\Bundle\CmsBundle\Entity\BaseMitigationTechniques;
use AIE\Bundle\CmsBundle\Entity\basematrixConfig;
use AIE\Bundle\CmsBundle\Form\BaseSystemsType;
use AIE\Bundle\CmsBundle\Form\BaseThreatsType;
use AIE\Bundle\CmsBundle\Form\BaseMitigationTechniquesType;
use AIE\Bundle\CmsBundle\Form\BaseActivityType;
use AIE\Bundle\CmsBundle\Form\BaseActivityEditType;

use AIE\Bundle\CmsBundle\Entity\SubProjectSystems;
use AIE\Bundle\CmsBundle\Entity\SubProjectThreats;
use AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques;
use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * basematrixConfig controller.
 *
 * @Route("/basematrix")
 */
class BaseMatrixConfigController extends CmsBaseController
{

    protected $basedataId = "";

    protected $entityDuplicateCheck = "";

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/", name="cms_basematrix")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $query = $em->createQuery('SELECT bs,bt,bmt,ba,bsa,bmc,bdt FROM AIECmsBundle:BaseSystems bs left join AIECmsBundle:BaseThreats bt WITH bs.id= bt.basesystems left join AIECmsBundle:BaseMitigationTechniques bmt WITH bt.id = bmt.basethreats left join AIECmsBundle:BaseActivity ba WITH bmt.id = ba.basemitigationtechniques left join AIECmsBundle:BaseDatasheetType bsa WITH bsa.id = ba.basedatasheet left join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.basemetrics left join AIECmsBundle:BaseDatabaseType bdt WITH bdt.id = bmc.databasetype ORDER BY bs.systemstitle ASC'
        );

        $basematrixProperties = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $entity = new BaseSystems();
        $form   = $this->createCreateBaseSystemsForm($entity);

        return array(
            'basematrixproperties' => $basematrixProperties,
            'form' =>$form->createView(),
        );  
    }

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/ajaxloaddatasheet", name="cms_basematrix_ajaxloaddatasheet")
     * @Method("POST")
     * @Template()
     */

    public function ajaxAction(Request $request) {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getManager();

        $id = $request->query->get('metrics_id');

        $result = array();

        $qb = $em->createQueryBuilder();
        $query = $qb->select('bs')

            ->from('AIECmsBundle:BaseMetricsConfig', 'bm')
            ->innerJoin('AIECmsBundle:BaseDatabaseType', 'bd', 'WITH', 'bd.id = bm.databasetype')
            ->innerJoin('AIECmsBundle:BaseDatasheetType', 'bs', 'WITH', 'bs.id = bm.datasheettype');

        $qb->andWhere('bm.id = :uid')
            ->setParameter('uid', $id);

        $cities = $query->getQuery()->getResult();

        foreach ($cities as $city) {
            $result[$city->getTitle()] = $city->getId();
        }
        return new JsonResponse($result);
    }

    /**
     * Displays a form to create a new Basy Systems entity.
     *
     * @Route("/basesystemsnew", name="cms_basematrix_basesystems_new")
     * @Method("GET")
     * @Template()
     */
    public function basesystemsAction()
    {
        $entity = new BaseSystems();
        $form   = $this->createCreateBaseSystemsForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/basesytemscreate", name="cms_basematrix_basesystems_create")
     * @Method("POST")
     * @Template()
     */
    public function createbasesystemsAction(Request $request)
    {
        $entity = new BaseSystems();
        $form = $this->createCreateBaseSystemsForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $em->persist($entity);
            $em->flush();
            $em->refresh($entity);

            $dbprojects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

            foreach ($dbprojects as $dbproject) {

                $subProjectSystems = new SubProjectSystems();

                $oldReflection = new \ReflectionObject($entity);
                $newReflection = new \ReflectionObject($subProjectSystems);

                foreach ($oldReflection->getProperties() as $property) {
                    if ($newReflection->hasProperty($property->getName())) {
                        $newProperty = $newReflection->getProperty($property->getName());
                        $property->setAccessible(true);
                        $newProperty->setAccessible(true);
                        $newProperty->setValue($subProjectSystems, $property->getValue($entity));
                    }
                    $subProjectSystems->setBasesystemid($entity->getId());
                    $subProjectSystems->setSubproject($dbproject);

                    $em->persist($subProjectSystems);
                }
            }

            $em->flush();

            return $this->redirect($this->generateUrl('cms_basematrix'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Update base systems
     *
     * @Route("/basesystems/{id}/update", name="cms_basematrix_basesystems_update")
     * @Method("POST")
     * @Template()
     */
    public function updatebasesystemsAction(Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:BaseSystems')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest()) {

            $title = $request->request->get('title');

            $entity->setSystemsTitle($title);

            $em->persist($entity);

            $subentities = $em->getRepository('AIECmsBundle:SubProjectSystems')->findBy(array('basesystemid'=>$passid));

            foreach ($subentities as $subentity) {
                $subentity->setSystemsTitle($title);
                $em->persist($subentity);
            }

            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

        return [
            'entity'    => $entity,
            'systemsId' => $id,
        ];
    }

    /**
     * Displays a form to create a new Basy Systems entity.
     *
     * @Route("/basethreatsnew", name="cms_basematrix_basethreats_new")
     * @Method("GET")
     * @Template()
     */
    public function basethreatsAction($systemId)
    {
        $entity = new BaseThreats();
        $form   = $this->createCreateBaseThreatsForm($systemId,$entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/{systemId}/basethreatscreate", name="cms_basematrix_basethreats_create")
     * @Method("POST")
     * @Template()
     */
    public function createbasethreatsAction($systemId, Request $request)
    {
        $entity = new BaseThreats();
        $form = $this->createCreateBaseThreatsForm($systemId, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();

            $code = $form->getData()->getThreatsTitle();
            $cmsHelper = $this->get('aie_cms.cms.helper');
            $isUsed = $cmsHelper->isBaseThreatUsed($systemId, $code);
//            if ($isUsed) {
//                $this->get('session')->getFlashBag()->add('error', "Oops! This Base Threat ({$code}) is already in use.");
//            } else {
                $basesystems = $em->getRepository('AIECmsBundle:BaseSystems')->find($systemId);
                $entity->setBaseSystems($basesystems);
                $em->persist($entity);
                $em->flush();
                $em->refresh($entity);

                $dbprojects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

                foreach ($dbprojects as $dbproject) {

                    $subProjectThreats = new SubProjectThreats();

                    $oldReflection = new \ReflectionObject($entity);
                    $newReflection = new \ReflectionObject($subProjectThreats);

                    foreach ($oldReflection->getProperties() as $property) {
                        if ($newReflection->hasProperty($property->getName())) {
                            $newProperty = $newReflection->getProperty($property->getName());
                            $property->setAccessible(true);
                            $newProperty->setAccessible(true);
                            $newProperty->setValue($subProjectThreats, $property->getValue($entity));
                        }
                        $subProjectThreats->setBasesystemid($systemId);
                        $subProjectThreats->setBasethreatid($entity->getId());
                        $subProjectThreats->setSubproject($dbproject);

                        $em->persist($subProjectThreats);
                    }
                }

                $em->flush();
           // }

            return $this->redirect($this->generateURL('cms_basematrix'));
        }

        return [
            'entity'    => $entity,
            'systemId' => $systemId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Update base threats
     *
     * @Route("/{systemId}/basethreats/{id}/update", name="cms_basematrix_basethreats_update")
     * @Method("POST")
     * @Template()
     */
    public function updatebasethreatsAction($systemId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:BaseThreats')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest()) {

            $title = $request->request->get('title');

            $entity->setThreatsTitle($title);

            $em->persist($entity);

            $subentities = $em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(array('basesystemid'=>$systemId,'basethreatid'=>$passid));

            foreach ($subentities as $subentity) {
                $subentity->setThreatsTitle($title);
                $em->persist($subentity);
            }

            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

        return [
            'entity'    => $entity,
            'threatsId' => $id,
        ];
    }

    /**
     * Displays a form to create a new Basy Systems entity.
     *
     * @Route("/basemitigationtechniquesnew", name="cms_basematrix_basemitigationtechniques_new")
     * @Method("GET")
     * @Template()
     */
    public function basemitigationtechniquesAction($threatId)
    {
        $entity = new BaseMitigationTechniques();
        $form   = $this->createCreateBaseMitigationTechniquesForm($threatId,$entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/{threatId}/basemitigationtechniquescreate", name="cms_basematrix_basemitigationtechniques_create")
     * @Method("POST")
     * @Template()
     */
    public function createbasemitigationtechniquesAction($threatId, Request $request)
    {
        $entity = new BaseMitigationTechniques();
        $form = $this->createCreateBaseMitigationTechniquesForm($threatId, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();

            $code = $form->getData()->getMitigationTechniquesTitle();
            $cmsHelper = $this->get('aie_cms.cms.helper');
            $isUsed = $cmsHelper->isBaseMitigationTechniqueUsed($threatId, $code);
            if ($isUsed) {
                $this->get('session')->getFlashBag()->add('error', "Oops! This Base Mitigation Techniques ({$code}) is already in use.");
            } else {
                $basethreats = $em->getRepository('AIECmsBundle:BaseThreats')->find($threatId);
                $entity->setBaseThreats($basethreats);
                $em->persist($entity);
                $em->flush();
            }

            return new Response(json_encode(array('result' => $entity->getId())));
        }

        return [
            'entity'    => $entity,
            'threatId' => $threatId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Update base mitigationtechniques
     *
     * @Route("/{threatId}/basemitigationtechniques/{id}/update", name="cms_basematrix_basemitigationtechniques_update")
     * @Method("POST")
     * @Template()
     */
    public function updatebasemitigationtechniquesAction($threatId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:BaseMitigationTechniques')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest()) {

            $title = $request->request->get('title');

            $entity->setMitigationTechniquesTitle($title);

            $em->persist($entity);

            $subentities = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findBy(array('basethreatid'=>$threatId, 'basemitigationtechniqueid'=>$passid));

            foreach ($subentities as $subentity) {
                $subentity->setMitigationTechniquesTitle($title);
                $em->persist($subentity);
            }

            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

        return [
            'entity'    => $entity,
            'mitigationtechniquesId' => $id,
        ];
    }

    /**
     * Displays a form to create a new Base Activity entity.
     *
     * @Route("/baseactivitynew", name="cms_basematrix_baseactivity_new")
     * @Method("GET")
     * @Template()
     */
    public function baseactivityAction($threatId)
    {
        $entity = new BaseActivity();
        $form   = $this->createCreateActivityForm($threatId,$entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Lists all BaseMatrixConfig entities.
     *
     * @Route("/{threatId}/baseactivitycreate", name="cms_basematrix_baseactivity_create")
     * @Method("POST")
     * @Template()
     */
    public function createbaseactivityAction($threatId, Request $request)
    {
        $entity = new BaseActivity();
        $form = $this->createCreateActivityForm($threatId, $entity);
        $form->handleRequest($request);
        $em = $this->getManager();
        $params = $request->request->all();

        if ($form->isValid()) {

            if (!$form->isSubmitted()){
                 $this->addFlash('error', 'Activity cannot be added, Please input valid data ');
                 return $this->redirect($this->generateURL('cms_basematrix'));
            }
            $data = $form->getData();

            if (!empty($form->get('mitigationtechniquestitle')->getViewData()))
            {
                $entity = new BaseMitigationTechniques();
                $code = $form->get('mitigationtechniquestitle')->getViewData();
                $cmsHelper = $this->get('aie_cms.cms.helper');

                $basethreats = $em->getRepository('AIECmsBundle:BaseThreats')->find($threatId);
                $entity->setBaseThreats($basethreats);
                $entity->setMitigationTechniquesTitle($code);
                $em->persist($entity);
                $em->flush();
                $em->refresh($entity);

                $dbprojects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

                foreach ($dbprojects as $dbproject) {

                    $subProjectMitigationTechniques = new SubProjectMitigationTechniques();

                    $oldReflection = new \ReflectionObject($entity);
                    $newReflection = new \ReflectionObject($subProjectMitigationTechniques);

                    foreach ($oldReflection->getProperties() as $property) {
                        if ($newReflection->hasProperty($property->getName())) {
                            $newProperty = $newReflection->getProperty($property->getName());
                            $property->setAccessible(true);
                            $newProperty->setAccessible(true);
                            $newProperty->setValue($subProjectMitigationTechniques, $property->getValue($entity));
                        }
                        $subProjectMitigationTechniques->setBasethreatid($threatId);
                        $subProjectMitigationTechniques->setBasemitigationtechniqueid($entity->getId());
                        $subProjectMitigationTechniques->setSubproject($dbproject);

                        $em->persist($subProjectMitigationTechniques);
                    }
                }

                $em->flush();

                $mitigationtechniqueId = $entity->getId();

                $entity = new BaseActivity();

                $datasheetid = $form->get('basemetrics')->getData()->getId();

                $basedatasheet = $em->getRepository('AIECmsBundle:BaseDataSheetType')->find($data->getBasedatasheet()->getId());
                $entity->setBasedatasheet($basedatasheet);

                $basemetrics = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($datasheetid);

                $entity->setBasemetrics($basemetrics);
                $basemitigations = $em->getRepository('AIECmsBundle:BaseMitigationTechniques')->find($mitigationtechniqueId);
                $entity->setFrequency($data->getFrequency());
                $entity->setActivityperson($data->getActivityperson());

                $entity->setWeightage($data->getWeightage());
                $entity->setConsequencesexcursion($data->getConsequencesexcursion());
                $entity->setRemedialaction($data->getRemedialaction());
                $entity->setManagementtitle($data->getManagementtitle());
                $entity->setManagementgreen($data->getManagementgreen());
                $entity->setManagementamber($data->getManagementamber());
                $entity->setManagementred($data->getManagementred());
                $entity->setControltitle($data->getControltitle());

                $entity->setControlgreenexpression($data->getControlgreenexpression());
                $entity->setControlgreen($data->getControlgreen());

                $entity->setControlgreenexpressionrange($data->getControlgreenexpressionrange());
                $entity->setControlgreenrange($data->getControlgreenrange());

                $entity->setControlamberexpression($data->getControlamberexpression());
                $entity->setControlamber($data->getControlamber());

                $entity->setControlamberexpressionrange($data->getControlamberexpressionrange());
                $entity->setControlamberrange($data->getControlamberrange());

                $entity->setControlredexpression($data->getControlredexpression());
                $entity->setControlred($data->getControlred());

                $entity->setControlredexpressionrange($data->getControlredexpressionrange());
                $entity->setControlredrange($data->getControlredrange());

                $entity->setBaseMitigationTechniques($basemitigations);
                if ($data->getControltitle() == "N/A")
                {
                    $entity->setControlgreenexpression("");
                    $entity->setControlgreen("N/A");
                    $entity->setControlgreenexpressionrange("");
                    $entity->setControlgreenrange("");

                    $entity->setControlamberexpression("");
                    $entity->setControlamber("N/A");
                    $entity->setControlamberexpressionrange("");
                    $entity->setControlamberrange("");

                    $entity->setControlredexpression("");
                    $entity->setControlred("N/A");
                    $entity->setControlredexpressionrange("");
                    $entity->setControlredrange("");
                }
                if (empty($data->getControlgreenrange()))
                {
                    $entity->setControlgreenexpressionrange("");
                }
                if (empty($data->getControlamberrange()))
                {
                    $entity->setControlamberexpressionrange("");
                }
                if (empty($data->getControlredrange()))
                {
                    $entity->setControlredexpressionrange("");
                }
                if ($data->getControlgreen() == "N/A")
                {
                    $entity->setControlgreenexpression("");
                    $entity->setControlgreenexpressionrange("");
                    $entity->setControlgreenrange("");
                }
                if ($data->getControlamber() == "N/A")
                {
                    $entity->setControlamberexpression("");
                    $entity->setControlamberexpressionrange("");
                    $entity->setControlamberrange("");
                }
                if ($data->getControlred() == "N/A")
                {
                    $entity->setControlredexpression("");
                    $entity->setControlredexpressionrange("");
                    $entity->setControlredrange("");
                }
                $em->persist($entity);
                $em->flush();
                $em->refresh($entity);

                $dbprojects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

                foreach ($dbprojects as $dbproject) {

                    $subProjectActivity = new SubProjectActivity();

                    $oldReflection = new \ReflectionObject($entity);
                    $newReflection = new \ReflectionObject($subProjectActivity);

                    foreach ($oldReflection->getProperties() as $property) {
                        if ($newReflection->hasProperty($property->getName())) {
                            $newProperty = $newReflection->getProperty($property->getName());
                            $property->setAccessible(true);
                            $newProperty->setAccessible(true);
                            $newProperty->setValue($subProjectActivity, $property->getValue($entity));
                        }
                        $subProjectActivity->setSubprojectmetrics($entity->getBasemetrics());
                        $subProjectActivity->setSubprojectdatasheet($entity->getBasedatasheet());
                        $subProjectActivity->setBasemitigationtechniqueid($mitigationtechniqueId);
                        $subProjectActivity->setBaseactivityid($entity->getId());
                        $subProjectActivity->setSubproject($dbproject);

                        $em->persist($subProjectActivity);
                    }
                }

                $em->flush();

                return $this->redirect($this->generateURL('cms_basematrix'));
            }
        }

        return [
            'entity'    => $entity,
            'threatId' => $threatId,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Displays a editform to create a new Base Activity entity.
     *
     * @Route("/{mitigationtechniqueId}/baseactivity/{id}/edit", name="cms_basematrix_baseactivity_edit")
     * @Method("GET")
     * @Template()
     */
    public function baseactivityeditAction($mitigationtechniqueId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseActivity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Base Activity entity.');
        }
        $editform   = $this->createCreateActivityEditform($mitigationtechniqueId, $entity, $id);

        return array(
            'entity' => $entity,
            'editform'   => $editform->createView(),
        );
    }

    /**
     * Creates a editform to edit a Base Activity entity.
     *
     * @param BaseSystems $entity The entity
     *
     * @return \Symfony\Component\form\editform The editform
     */
    private function createCreateActivityEditform($mitigationtechniqueId, BaseActivity $entity, $id) {
        $em = $this->getManager();
        $editform = $this->createform(new BaseActivityEditType($em,$entity->getBasemetrics()->getId()), $entity, array(
            'action' => $this->generateURL('cms_basematrix_baseactivity_update',['mitigationtechniqueId' => $mitigationtechniqueId,'id' => $id]),
            'method' => 'POST',
        ));

        $editform->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'btn btn-primary btn-sm']], 'btn'));
        $editform->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm']], 'btn'));

        return $editform;
    }

    /**
     * Update base activity
     *
     * @Route("/{mitigationtechniqueId}/baseactivity/{id}/update", name="cms_basematrix_baseactivity_update")
     * @Method("POST")
     * @Template()
     */
    public function updatebaseactivityAction($mitigationtechniqueId, Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:BaseActivity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Base Activity entity.');
        }

        $editform = $this->createCreateActivityEditform($mitigationtechniqueId, $entity, $id);
        $editform->handleRequest($request);

        if ($request->isMethod('POST')) {

            if ($editform->isSubmitted() && $editform->isValid()) {

                $data = $editform->getData();

                $basedatasheet = $em->getRepository('AIECmsBundle:BaseDataSheetType')->find($data->getBasedatasheet()->getId());
                $entity->setBasedatasheet($basedatasheet);

                $datasheetid = $editform->get('basemetrics')->getData()->getId();

                $basemetrics = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($datasheetid);

                $entity->setBasemetrics($basemetrics);

                $basemitigations = $em->getRepository('AIECmsBundle:BaseMitigationTechniques')->find($mitigationtechniqueId);
                $entity->setBaseMitigationTechniques($basemitigations);
                $em->persist($entity);
                $em->flush();
                $em->refresh($entity);

                $subentities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('basemitigationtechniqueid'=>$mitigationtechniqueId, 'baseactivityid'=>$id));

                foreach ($subentities as $subentity) {
                    //$subprojectdatasheet = $em->getRepository('AIECmsBundle:BaseDataSheetType')->find($entity->getBasedatasheet()->getId());
                    $subentity->setSubprojectdatasheet($entity->getBasedatasheet());
                    //$subprojectmetrics = $em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($entity->getBasemetrics()->getId());
                    $subentity->setSubprojectmetrics($entity->getBasemetrics());
                    $em->persist($subentity);
                }

                $em->flush();

                return $this->redirect($this->generateURL('cms_basematrix'));
            }
        }

        return [
            'entity'    => $entity,
            'activityId' => $id,
        ];
    }

    /**
     * Update base activity
     *
     * @Route("/{mitigationtechniqueId}/baseactivity/{id}/updatefields", name="cms_basematrix_baseactivity_updatefields")
     * @Method("POST")
     * @Template()
     */
    public function updatebaseactivityfieldsAction($mitigationtechniqueId, Request $request, $id)
    {

        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:BaseActivity')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest() &&  $request->isMethod( 'POST' )) {

            $title = $request->request->get('title');

            $key = $request->request->get('key');

            $mitigationtechniqueIdTemp = explode("_",$key);
            $mitigationtechniqueIdTemp = $mitigationtechniqueIdTemp[1];

            $mitigationtechniqueId = str_replace($mitigationtechniqueIdTemp,"",$mitigationtechniqueId);

            if ($title) {

                switch ($key) {
                    case "threshold_controltitle":
                        $entity->setControltitle($title);
                        break;
                    case "threshold_managementtitle":
                        $entity->setManagementtitle($title);
                        break;
                    case "threshold_frequency":
                        $entity->setFrequency($title);
                        break;
                    case "threshold_activityperson":
                        $entity->setActivityperson($title);
                        break;
                    case "threshold_weightage":
                        $entity->setWeightage($title);
                        break;
                    case "threshold_consequencesexcursion":
                        $entity->setConsequencesexcursion($title);
                        break;
                    case "threshold_remedialaction":
                        $entity->setRemedialaction($title);
                        break;
                    case "threshold_managementgreen":
                        $entity->setManagementgreen($title);
                        break;
                    case "threshold_managementamber":
                        $entity->setManagementamber($title);
                        break;
                    case "threshold_managementred":
                        $entity->setManagementred($title);
                        break;

                }
            }

            $em->persist($entity);
            $em->flush();
            $em->refresh($entity);

            $subentities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('basemitigationtechniqueid'=>$mitigationtechniqueId, 'baseactivityid'=>$passid));


            foreach ($subentities as $subentity) {

                switch ($key) {
                    case "threshold_controltitle":
                        $subentity->setControltitle($entity->getControltitle());
                        break;
                    case "threshold_managementtitle":
                        $subentity->setManagementtitle($entity->getManagementtitle());
                        break;
                    case "threshold_frequency":
                        $subentity->setFrequency($entity->getFrequency());
                        break;
                    case "threshold_activityperson":
                        $subentity->setActivityperson($entity->getActivityperson());
                        break;
                    case "threshold_weightage":
                        $subentity->setWeightage($entity->getWeightage());
                        break;
                    case "threshold_consequencesexcursion":
                        $subentity->setConsequencesexcursion($entity->getConsequencesexcursion());
                        break;
                    case "threshold_remedialaction":
                        $subentity->setRemedialaction($entity->getRemedialaction());
                        break;
                    case "threshold_managementgreen":
                        $subentity->setManagementgreen($entity->getManagementgreen());
                        break;
                    case "threshold_managementamber":
                        $subentity->setManagementamber($entity->getManagementamber());
                        break;
                    case "threshold_managementred":
                        $subentity->setManagementred($entity->getManagementred());
                        break;

                }

                $em->persist($subentity);
            }
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

    }

    /**
     * Update base activity
     *
     * @Route("/{mitigationtechniqueId}/baseactivity/{id}/updateexpressionfields", name="cms_basematrix_baseactivity_updateexpressionfields")
     * @Method("POST")
     * @Template()
     */
    public function updatebaseactivityexpressionfieldsAction($mitigationtechniqueId, Request $request, $id)
    {
        $em = $this->getManager();

        $passid = $request->request->get('passid');

        $entity = $em->getRepository('AIECmsBundle:BaseActivity')->find($passid);

        if (!$entity) {
            return new Response(json_encode(array('result' => 'notok')));
        }

        if($request->isXmlHttpRequest() &&  $request->isMethod( 'POST' )) {

            $title = (!empty($request->request->get('title'))) ? $request->request->get('title') : "";
            $titleexpression = (!empty($request->request->get('titleexpression'))) ? $request->request->get('titleexpression') : "";

            $titlerange = (!empty($request->request->get('titlerange'))) ? $request->request->get('titlerange') : "";
            $titlerangeexpression = (!empty($request->request->get('titlerangeexpression'))) ? $request->request->get('titlerangeexpression') : "";

            $key = $request->request->get('key');

            if ($entity->getControltitle() == "N/A") {
                if (is_numeric($title) || $title != "N/A") {
                    return new Response(json_encode(array('result' => 'notok')));
                }
            }
            // no value for range or control empression then null
            if (!$titlerange) $titlerangeexpression = "";
            if (!$title) $titleexpression = "";

            if (!is_numeric($title)) {
                $title = strtoupper($title);
            }

            if (!is_numeric($titlerange)) {
                $titlerange = strtoupper($titlerange);
            }

            // no control thresholds N/A then all is null
            if ( $title == "N/A" && $titlerange != "N/A" )
            {
                $titleexpression = "";
                $titlerangeexpression = "";
                $titlerange = "";
            }
            else if ( $title != "N/A" && $titlerange == "N/A")
            {
                $titlerangeexpression = "";
                $titlerange = "";
            }

            switch ($key){
                case "controlgreen":
                    $entity->setControlgreenexpression($titleexpression);
                    $entity->setControlgreen($title);
                    $entity->setControlgreenexpressionrange($titlerangeexpression);
                    $entity->setControlgreenrange($titlerange);
                    break;
                case "controlamber":
                    $entity->setControlamberexpression($titleexpression);
                    $entity->setControlamber($title);
                    $entity->setControlamberexpressionrange($titlerangeexpression);
                    $entity->setControlamberrange($titlerange);
                    break;
                case "controlred":
                    $entity->setControlredexpression($titleexpression);
                    $entity->setControlred($title);
                    $entity->setControlredexpressionrange($titlerangeexpression);
                    $entity->setControlredrange($titlerange);
                    break;
            }

            $em->persist($entity);
            $em->flush();
            $em->refresh($entity);

            $subentities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('basemitigationtechniqueid'=>$mitigationtechniqueId, 'baseactivityid'=>$passid));

            foreach ($subentities as $subentity) {
                switch ($key){
                    case "controlgreen":
                        $subentity->setControlgreenexpression($entity->getControlgreenexpression());
                        $subentity->setControlgreen($entity->getControlgreen());
                        $subentity->setControlgreenexpressionrange($entity->getControlgreenexpressionrange());
                        $subentity->setControlgreenrange($entity->getControlgreenrange());
                        break;
                    case "controlamber":
                        $subentity->setControlamberexpression($entity->getControlamberexpression());
                        $subentity->setControlamber($entity->getControlamber());
                        $subentity->setControlamberexpressionrange($entity->getControlamberexpressionrange());
                        $subentity->setControlamberrange($entity->getControlamberrange());
                        break;
                    case "controlred":
                        $subentity->setControlredexpression($entity->getControlredexpression());
                        $subentity->setControlred($entity->getControlred());
                        $subentity->setControlredexpressionrange($entity->getControlredexpressionrange());
                        $subentity->setControlredrange($entity->getControlredrange());
                        break;
                }
                $em->persist($subentity);
            }

            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }

    }

    /**
     * Creates a form to create a Base Systems entity.
     *
     * @param BaseSystems $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateBaseSystemsForm(BaseSystems $entity) {
        $form = $this->createForm(new BaseSystemsType(), $entity, array(
            'action' => $this->generateURL('cms_basematrix_basesystems_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a Base Threats entity.
     *
     * @param BaseThreats $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateBaseThreatsForm($systemId, BaseThreats $entity) {
        $form = $this->createForm(new BaseThreatsType(), $entity, array(
            'action' => $this->generateURL('cms_basematrix_basethreats_create', ['systemId' => $systemId,]),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a Base MitigationTechniques entity.
     *
     * @param BaseMitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateBaseMitigationTechniquesForm($threatId, BaseMitigationTechniques $entity) {
        $em = $this->getManager();
        $form = $this->createForm(new BaseMitigationTechniquesType($em), $entity, [
            'action' => $this->generateURL('cms_basematrix_basemitigationtechniques_create', ['threatId' => $threatId,]),
            'method' => 'POST',
        ]);

        //$form->add('submit', 'submit', $this->options(['label' => 'Add', 'attr' => ['class' => 'btn btn-primary btn-sm','style' => 'float: left']], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to create a Base Activity entity.
     *
     * @param BaseActivity $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateActivityForm($threatId, BaseActivity $entity) {
        $em = $this->getManager();

        $form = $this->createForm(new BaseActivityType($em), $entity, [
            'action' => $this->generateURL('cms_basematrix_baseactivity_create', ['threatId' => $threatId,]),
            'method' => 'POST',
        ]);

        //$form->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton']], 'btn'));

        return $form;
    }

    /**
     * Delete base systems
     *
     * @Route("/{id}/basesystems/delete", name="cms_basematrix_basesystems_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletebasesystemsAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:BaseSystems')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);

            $dbprojectsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->findBy(array('basesystemid'=>$id));

            foreach ($dbprojectsystems as $dbprojectsystem) {

                $em->remove($dbprojectsystem);
            }

            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Delete base threats
     *
     * @Route("/{id}/basethreats/delete", name="cms_basematrix_basethreats_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletebasethreatsAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:BaseThreats')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);

            $dbprojectthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(array('basethreatid'=>$id));

            foreach ($dbprojectthreats as $dbprojectthreat) {

                $em->remove($dbprojectthreat);
            }

            $em->flush();
            return new Response(json_encode(array('result' => 'ok')));
        }else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

    /**
     * Delete base mitigation
     *
     * @Route("/{id}/basemitigationtechniques/delete", name="cms_basematrix_basemitigationtechniques_delete")
     * @Method("POST")
     * @Template()
     */
    public function deletebasemitigationtechniquesAction(Request $request, $id)
    {

        if ($request->isXmlHttpRequest()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:BaseMitigationTechniques')->find($id);

            if (!$entity) {
                return new Response(json_encode(array('result' => 'notok')));
            }

            $em->remove($entity);

            $dbprojectmitigations = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findBy(array('basemitigationtechniqueid'=>$id));

            foreach ($dbprojectmitigations as $dbprojectmitigation) {

                $em->remove($dbprojectmitigation);
            }

            $em->flush();
            return new Response(json_encode(array('result' => 'ok')));
        }
        else{
            return new Response(json_encode(array('result' => 'notok')));
        }

    }

}
