<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Controller\AnomalyBaseController;
use AIE\Bundle\CmsBundle\Entity\ActionOwners;
use AIE\Bundle\CmsBundle\Form\ActionOwnersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * SubProjectActionOwnersController controller.
 *
 * @Route("/{projectId}/{subprojectId}/ao")
 */
class SubProjectActionOwnersController extends CmsBaseController {

    protected $grantedShow =0;
    protected $grantedEdit =0;
    protected $grantedDelete =0;
    protected $grantedAdd =0;

    /**
     * Lists all ActionOwners entities.
     *
     * @Route("/", name="cms_subproject_actionowners")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId,$subprojectId)
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIECmsBundle:ActionOwners')->findBySubproject($subprojectId);

        foreach ($entities as $entity)
        {
            $deleteForm = $this->createDeleteForm($entity->getId(), $projectId, $subprojectId);
            $entity->delete_form = $deleteForm->createView();
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_SHOW',$this->userRolesMergeToCheck)) {
            $this->grantedShow=1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_EDIT',$this->userRolesMergeToCheck)) {
            $this->grantedEdit=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_DELETE',$this->userRolesMergeToCheck)) {
            $this->grantedDelete=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_ADD',$this->userRolesMergeToCheck)) {
            $this->grantedAdd =1;
        }

        //$subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);

        return array(
            'entities'   => $entities,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'grantedShow' => $this->grantedShow,
            'grantedEdit' => $this->grantedEdit,
            'grantedDelete' => $this->grantedDelete,
            'grantedAdd' => $this->grantedAdd,
        );
    }

    /**
     * Creates a new ActionOwners entity.
     *
     * @Route("/", name="cms_subproject_actionowners_create")
     * @Method("POST")
     * @Template("AIECmsBundle:ActionOwners:new.html.twig")
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        $entity = new ActionOwners();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getManager();
            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $entity->setSubproject($subproject);
            if ($entity->getEmail() == "") $entity->setEmail(" ");
            if ($entity->getCompany() == "") $entity->setCompany(" ");
            if ($entity->getPosition() == "") $entity->setPosition(" ");
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cms_subproject_actionowners', array('projectId' => $projectId,'subprojectId' => $subprojectId)));
        }

        return $this->redirect($this->generateUrl('cms_subproject_actionowners_new', array('projectId' => $projectId,'subprojectId' => $subprojectId)));
    }

    /**
     * Creates a form to create a ActionOwners entity.
     *
     * @param ActionOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ActionOwners $entity, $projectId,$subprojectId)
    {
        $form = $this->createForm(new ActionOwnersType(), $entity, array(
            'action' => $this->generateUrl('cms_subproject_actionowners_create', array('projectId' => $projectId,'subprojectId' => $subprojectId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new ActionOwners entity.
     *
     * @Route("/new", name="cms_subproject_actionowners_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId, $subprojectId)
    {
        $entity = new ActionOwners();
        $em = $this->getManager();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId);
        //$subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);
        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'projectId' => $projectId,
            'subprojectId'  => $subprojectId,
        );
    }

    /**
     * Finds and displays a ActionOwners entity.
     *
     * @Route("/{id}", name="cms_subproject_actionowners_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ActionOwners')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find ActionOwners entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'projectId'  => $projectId,
            'subprojectId'  => $subprojectId,
        );
    }

    /**
     * Displays a form to edit an existing ActionOwners entity.
     *
     * @Route("/{id}/edit", name="cms_subproject_actionowners_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ActionOwners')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find ActionOwners entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);
        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'subprojectId'  => $subprojectId,
            'projectId'   => $projectId
        );
    }

    /**
     * Creates a form to edit a ActionOwners entity.
     *
     * @param ActionOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ActionOwners $entity, $projectId, $subprojectId)
    {
        $form = $this->createForm(new ActionOwnersType(), $entity, array(
            'action' => $this->generateUrl('cms_subproject_actionowners_update', array('projectId' => $projectId,'subprojectId' => $subprojectId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing ActionOwners entity.
     *
     * @Route("/{id}", name="cms_subproject_actionowners_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:ActionOwners:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $projectId, $subprojectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:ActionOwners')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find ActionOwners entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        //$this->setLeftTitleFromPipeline($subproject);

        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId);
        $editForm = $this->createEditForm($entity, $projectId, $subprojectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();
            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            return $this->redirect($this->generateUrl('cms_subproject_actionowners', array('projectId' => $projectId,'subprojectId' => $subprojectId)));
        }

        return $this->redirect($this->generateUrl('cms_subproject_actionowners_edit', array('projectId' => $projectId,'subprojectId' => $subprojectId, 'id' => $id)));
    }

    /**
     * Deletes a ActionOwners entity.
     *
     * @Route("/{id}", name="cms_subproject_actionowners_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $projectId, $subprojectId)
    {
        $form = $this->createDeleteForm($id, $projectId, $subprojectId);
        $form->handleRequest($request);

        $em = $this->getManager();
        if ($form->isValid())
        {
            $entity = $em->getRepository('AIECmsBundle:ActionOwners')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find ActionOwners entity.');
            }

            $assignedActivities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(['action_owner' => $id ]);
            foreach($assignedActivities as $activity){
                $activity->setActionOwner(null);
                //$mitigation->setRemindedOn(null);
                $em->persist($activity);
            }

            $em->remove($entity);
            $em->flush();
            $this->addFlash('success', 'Action owner deleted');
        }

        return $this->redirect($this->generateUrl('cms_subproject_actionowners', array('projectId' => $projectId,'subprojectId' => $subprojectId)));
    }

    /**
     * Creates a form to delete a ActionOwners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $projectId, $subprojectId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subproject_actionowners_delete', array('projectId' => $projectId,'subprojectId' => $subprojectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'btn btn-primary right btn-danger')), 'btn')
            ->getForm();
    }

}
