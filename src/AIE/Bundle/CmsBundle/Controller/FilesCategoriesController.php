<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\StorageBundle\Upload\FileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\FilesCategories;

/**
 * FilesCategories controller.
 *
 * @Route("/{projectId}/cms/{subprojectId}/files/categories")
 */
class FilesCategoriesController extends CmsBaseController {

    private $fileuploader;

    private function generateTree($categories, $files)
    {

        $tree = [];
        $_catFiles = $files;

        foreach ($_catFiles as $key => $file)
        {
            if ($file->getCategory() === null)
            {
                $tree[] = $file->getAsTreeNode($this->fileuploader);
                unset($_catFiles[$key]);
            }
        }

        foreach ($categories as $category)
        {
            $categoryNode = $category->getAsTreeNode($_catFiles, $this->fileuploader);
            $tree[] = $categoryNode;
        }

        return $tree;
    }

    /**
     * Lists all FilesCategories entities.
     *
     * @Route("/", name="cms_files_categories", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function getFilesTreeAction($subprojectId)
    {
        $em = $this->getManager();
        $folders = $em->getRepository('AIECmsBundle:FilesCategories')->findBy(['subproject' => $subprojectId, 'parent' => null]);
        $files = $em->getRepository('AIECmsBundle:Files')->findBy(['subproject' => $subprojectId]);

        $this->fileuploader = $this->get('storage.file_uploader');

        $tree = $this->generateTree($folders, $files);

        return new JsonResponse($tree);
    }

    /**
     * Creates a new FilesCategories entity.
     *
     * @Route("/", name="cms_files_categories_create", options={"expose"=true})
     * @Method("POST")
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_ADD_FOLDER',$this->userRolesMergeToCheck))
        {
            $entity = new FilesCategories();
            $name = $request->get('name');
            $parentId = $request->get('parent');
            $response = false;

            if ($name) {
                $em = $this->getManager();
                $cms = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if ($cms) {
                    $entity->setName($name);
                    $entity->setSubproject($cms);
                    if ($parentId) {
                        $parent = $em->getRepository('AIECmsBundle:FilesCategories')->find($parentId);
                        $entity->setParent($parent);
                    }

                    $em->persist($entity);
                    $em->flush();

                    $response = true;
                }
            }

            return new JsonResponse(['success' => $response, 'id' => $entity->getId()]);
        }
    }

    /**
     * Edits an existing FilesCategories entity.
     *
     * @Route("/{id}", name="cms_files_categories_update", options={"expose"=true})
     * @Method("PUT")
     */
    public function updateAction(Request $request, $projectId, $subprojectId, $id)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_EDIT_FOLDER',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();

            $category = $em->getRepository('AIECmsBundle:FilesCategories')->find($id);

            if (!$category) {
                throw $this->createNotFoundException('Unable to find FilesCategories entity.');
            }

            $name = $request->get('name');
            $response = false;

            if ($name) {
                $category->setName($name);
                $em->flush();
                $response = true;
            }

            return new JsonResponse(['success' => $response]);
        }
        else{
            return new JsonResponse(['status' => 404, 'success' => false]);
        }
    }

    /**
     * Deletes a FilesCategories entity.
     *
     * @Route("/{id}", name="cms_files_categories_delete", options={"expose"=true})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $subprojectId, $id)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_DELETE_FOLDER',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $response = false;
            $entity = $em->getRepository('AIECmsBundle:FilesCategories')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FilesCategories entity.');
            }

            if ($em->getRepository('AIECmsBundle:FilesCategories')->deleteCategory($id)) {
                $response = true;
            }

            return new JsonResponse(['success' => $response]);
        }
        else{
            return new JsonResponse(['status' => 404, 'success' => false]);
        }
    }


    /**
     * Creates a new FilesCategories entity.
     *
     * @Route("/move/{id}", name="cms_files_categories_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveAction(Request $request, $projectId, $subprojectId, $id)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_MOVE_FOLDER',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $response = false;
            $category = $em->getRepository('AIECmsBundle:FilesCategories')->find($id);

            if ($category) {
                $parentId = $request->get('parent');
                if ($parentId) {
                    $parent = $em->getRepository('AIECmsBundle:FilesCategories')->find($parentId);
                    if ($parent) {
                        $category->setParent($parent);
                        $response = true;
                    }
                } else {
                    $category->setParent(NULL);
                    $response = true;
                }

                $em->flush();
            }

            return new JsonResponse(['success' => $response]);
        }
        else{
            return new JsonResponse(['status' => 404, 'success' => false]);
        }
    }


	/**
     * Creates a new FilesCategories entity.
     *
     * @Route("/move_file/{id}", name="cms_files_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveFileAction(Request $request, $projectId, $subprojectId, $id)
    {
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_MOVE_DOCUMENT',$this->userRolesMergeToCheck)) {
            $em = $this->getManager();
            $response = false;
            $file = $em->getRepository('AIECmsBundle:Files')->find($id);

            if ($file) {
                $parentId = $request->get('parent');
                if ($parentId) {
                    $parent = $em->getRepository('AIECmsBundle:FilesCategories')->find($parentId);
                    if ($parent) {
                        $file->setCategory($parent);
                        $response = true;
                    }
                } else {
                    $file->setCategory(NULL);
                    $response = true;
                }

                $em->flush();
            }

            return new JsonResponse(['success' => $response]);
        }
        else{
            return new JsonResponse(['status' => 404, 'success' => false]);
        }
    }
}
