<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\BaseDatabaseProperties as DatabaseProperties;
use AIE\Bundle\CmsBundle\Entity\BaseDatabaseLinkedProperties as DatabaseLinkedProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjects;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * basedatabaseConfig controller.
 *
 * @Route("/basedatabase")
 */
class BaseDatabaseConfigController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = [];

    Protected $entityCheck = "";

    public function __construct(){
        $this->findduplicateproperties = array('serviceid','samplingorientationid','corrosionorientationid','accessfittingtypeid','holderplugid','probetechnologyid','couponmaterialid','coupontypeid','insulationid','surfacefinishid','chemicaltypeid','pumptypeid','injectionmethodid');
    }

    /**
     * Lists all BaseDatabaseConfig entities.
     *
     * @Route("/", name="cms_basedatabase")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $databaseProperties = $em->getRepository('AIECmsBundle:BaseDatabase')->findAll();
  
        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.serviceid= bsp.colid'
        );

        $serviceEntities = $query->getResult();
       
        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.samplingorientationid= bsp.colid'
        );

        $samplingorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.activitypersonid= bsp.colid'
        );

        $activitypersonEntities = $query->getResult();


        //corrosion

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.installeddeviceid= bsp.colid'
        );

        $installeddeviceEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.corrosionorientationid= bsp.colid'
        );

        $corrosionorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.accessfittingtypeid= bsp.colid'
        );

        $accessfittingtypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.teefittingid= bsp.colid'
        );

        $teefittingEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.holderplugid= bsp.colid'
        );

        $holderplugEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseLinkedProperties bsp WITH bs.probetechnologyid= bsp.colidone AND bs.dataloggerid= bsp.colidtwo'
        );

        $probetechnologyEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseLinkedProperties bsp WITH bs.couponmaterialid= bsp.colidone where bs.densityid= bsp.colidtwo'
        );

        $couponmaterialEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseLinkedProperties bsp WITH bs.coupontypeid= bsp.colidone where bs.areaid= bsp.colidtwo'
        );

        $coupontypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.insulationid= bsp.colid'
        );

        $insulationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.surfacefinishid= bsp.colid'
        );

        $surfacefinishEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.onlineid= bsp.colid'
        );

        $onlineEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.scaffoldid= bsp.colid'
        );

        $scaffoldEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.accessid= bsp.colid'
        );

        $accessEntities = $query->getResult();


        // chemical

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.chemicaltypeid= bsp.colid'
        );

        $chemicaltypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.pumptypeid= bsp.colid'
        );

        $pumptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.injectionmethodid= bsp.colid'
        );

        $injectionmethodEntities = $query->getResult();


        // Cathodic

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.locationid= bsp.colid'
        );

        $locationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.cptypeid= bsp.colid'
        );

        $cptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.equipmenttypeid= bsp.colid'
        );

        $equipmenttypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.anodetypeid= bsp.colid'
        );

        $anodetypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.monitoringmethodid= bsp.colid'
        );

        $monitoringmethodEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:BaseDatabaseProperties bsp WITH bs.referenceelectrodeid= bsp.colid'
        );

        $referenceelectrodeEntities = $query->getResult();


        return [
            'databaseproperties'=>$databaseProperties,
            'serviceentities' => $serviceEntities,

            'samplingorientationentities' => $samplingorientationEntities,
            'activitypersonentities' => $activitypersonEntities,
            
            'installeddeviceentities' => $installeddeviceEntities,
            'corrosionorientationentities' => $corrosionorientationEntities,
            'accessfittingtypeentities' => $accessfittingtypeEntities,
            'teefittingentities' => $teefittingEntities,
            'holderplugentities' => $holderplugEntities,
            'probetechnologyentities' => $probetechnologyEntities,
            'couponmaterialentities' => $couponmaterialEntities,
            'coupontypeentities' => $coupontypeEntities,
            'insulationentities' => $insulationEntities,
            'surfacefinishentities' => $surfacefinishEntities,
            'onlineentities' => $onlineEntities,
            'scaffoldentities' => $scaffoldEntities,
            'accessentities' => $accessEntities,

            'chemicaltypeentities' => $chemicaltypeEntities,
            'pumptypeentities' => $pumptypeEntities,
            'injectionmethodentities' => $injectionmethodEntities,

            'locationentities' => $locationEntities,
            'cptypeentities' => $cptypeEntities,
            'equipmenttypeentities' => $equipmenttypeEntities,
            'anodetypeentities' => $anodetypeEntities,
            'monitoringmethodentities' => $monitoringmethodEntities,
            'referenceelectrodeentities' => $referenceelectrodeEntities,

        ];
    }


    /**
     * Create BaseDatabaseConfig entities.
     *
     * @Route("/{sid}/create", name="cms_basedatabase_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $sid)
    {
        $title = $request->request->get('title');
        $id = $request->request->get('id');
        $em = $this->getManager();

        $response = new JsonResponse();

        $entity = new DatabaseProperties();

        if($request->isXmlHttpRequest()) {
            
            if (!$title) return new Response(json_encode(array('result' => 'error::empty')));

            $this->basedataId = $em->getRepository('AIECmsBundle:BaseDatabase')->find(1);

            if ($this->findduplicateproperties) {
                foreach ($this->findduplicateproperties as $properties) {
                    if ($sid == $this->basedataId[0][$properties]) {
                        $this->entityCheck = $em->getRepository('AIECmsBundle:BaseDatabaseProperties')->findBy(['colvalue' => $title, 'colid' => $sid]);
                    }
                }
            }

            if ($this->entityCheck) return new Response(json_encode(array('result' => 'error::duplicate')));

            if ($id){
                $entity = $em->getRepository('AIECmsBundle:BaseDatabaseProperties')->find($id);
            } else{
                $entity->setColId($sid);
            }

            $entity->setColValue($title);
            $em->persist($entity);
            $em->flush();

            if ($id){
                $subProjectDbProperties = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')
                    ->findBy(array('basedatabasepropertyid'
                    => $id));
                foreach ($subProjectDbProperties as $subProjectDbProperty) {
                    $subProjectDb = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')
                        ->find($subProjectDbProperty->getId());
                    $subProjectDb->setColValue($title);
                    $em->persist($subProjectDb);
                }
                $em->flush();
            }
            else {

                $projects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

                foreach ($projects as $project) {

                    $subProjectDbProperties = new SubProjectDatabaseProperties();
                    $oldReflection = new \ReflectionObject($entity);
                    $newReflection = new \ReflectionObject($subProjectDbProperties);
                    foreach ($oldReflection->getProperties() as $property) {
                        if ($newReflection->hasProperty($property->getName())) {
                            $newProperty = $newReflection->getProperty($property->getName());
                            $property->setAccessible(true);
                            $newProperty->setAccessible(true);
                            $newProperty->setValue($subProjectDbProperties, $property->getValue($entity));
                        }
                    }
                    $subProjectDbProperties->setBasedatabasepropertyid($entity->getId());
                    $subProjectDbProperties->setSubproject($project);

                    $em->persist($subProjectDbProperties);
                }

                $em->flush();
            }

            return new Response(json_encode(array('result' => 'ok')));
           
        } 
    }

    /**
     * Create BaseDatabaseConfig entities.
     *
     * @Route("/{sid}/{lid}/linkedcreate", name="cms_basedatabase_linkedcreate")
     * @Method("POST")
     * @Template()
     */
    public function createlinkedAction(Request $request, $sid, $lid)
    {
        $titlecolone = $request->request->get('titlecolone');
        $titlecoltwo = $request->request->get('titlecoltwo');
        $id = $request->request->get('id');
        $em = $this->getManager();

        $entity = new DatabaseLinkedProperties();

        if($request->isXmlHttpRequest()) {

            if (!$titlecolone && !$titlecoltwo) return new Response(json_encode(array('result' => 'error::empty')));

            $this->basedataId = $em->getRepository('AIECmsBundle:BaseDatabase')->find(1);
            if (!$id) {
                foreach ($this->findduplicateproperties as $properties) {
                    if ($sid == $this->basedataId[0][$properties]) {
                        $this->entityCheck = $em->getRepository('AIECmsBundle:BaseDatabaseLinkedProperties')->findBy(['colidonevalue' => $titlecolone]);
                    }
                }

                if ($this->entityCheck) {
                    return new Response(json_encode(array('result' => 'error::duplicate')));
                }
            }

            if ($lid == $this->basedataId[0]['densityid'] || $lid == $this->basedataId[0]['areaid'] )
            {
                if (!is_numeric($titlecoltwo)){
                    return new Response(json_encode(array('result' => 'error::nointeger')));
                }
            }

            if ($id){
                $entity = $em->getRepository('AIECmsBundle:BaseDatabaseLinkedProperties')->find($id);
            } else{
                $entity->setColIdOne($sid);
                $entity->setColIdTwo($lid);
            }

            $entity->setColIdOneValue($titlecolone);
            $entity->setColIdTwoValue($titlecoltwo);

            $em->persist($entity);

            $em->flush();

            $projects = $em->getRepository('AIECmsBundle:SubProjects')->findAll();
            if ($id){
                $subProjectDbLinkedProperties = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')
                    ->findBy(array('basedatabaselinkedpropertyid'
                    => $id));
                foreach ($subProjectDbLinkedProperties as $subProjectDbLinkedProperty) {
                    $subProjectLinkedDb = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')
                        ->find($subProjectDbLinkedProperty->getId());

                    $subProjectLinkedDb->setColIdOneValue($titlecolone);
                    $subProjectLinkedDb->setColIdTwoValue($titlecoltwo);

                    $em->persist($subProjectLinkedDb);
                }
                $em->flush();
            }
            else {
                foreach ($projects as $project) {

                    $subProjectDbLinkedProperties = new SubProjectDatabaseLinkedProperties();
                    $oldReflection = new \ReflectionObject($entity);
                    $newReflection = new \ReflectionObject($subProjectDbLinkedProperties);
                    foreach ($oldReflection->getProperties() as $property) {
                        if ($newReflection->hasProperty($property->getName())) {
                            $newProperty = $newReflection->getProperty($property->getName());
                            $property->setAccessible(true);
                            $newProperty->setAccessible(true);
                            $newProperty->setValue($subProjectDbLinkedProperties, $property->getValue($entity));
                        }
                    }
                    $subProjectDbLinkedProperties->setSubproject($project);
                    $subProjectDbLinkedProperties->setBasedatabaselinkedpropertyid($entity->getId());

                    $em->persist($subProjectDbLinkedProperties);
                }
                $em->flush();
            }


            return new Response(json_encode(array('result' => 'ok')));
           
        } 
    }

    /**
     * Delete BaseDatabaseConfig entities.
     *
     * @Route("/{sid}/delete", name="cms_basedatabase_delete")
     * @Method("POST")
     * @Template()
     */
    public function deleteAction(Request $request, $sid)
    {
        $id = $request->request->get('id');
        $em = $this->getManager();

        $entity = new DatabaseProperties();

        if($request->isXmlHttpRequest()) {

            if ($id){
                $entity = $em->getRepository('AIECmsBundle:BaseDatabaseProperties')->find($id);
                $subProjectDbProperties = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')
                    ->findBy(array('basedatabasepropertyid'
                    => $id));
                foreach ($subProjectDbProperties as $subProjectDbProperty) {
                    $subProjectDb = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')
                        ->find($subProjectDbProperty->getId());

                    $em->remove($subProjectDb);
                }
                $em->flush();
            } 

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BaseDatabaseProperties entity.');
            }

            $em->remove($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));
           
        } 
    }


    /**
     * Delete BaseDatabaseConfig entities.
     *
     * @Route("/{sid}/{lid}/linkeddelete", name="cms_basedatabase_linkeddelete")
     * @Method("POST")
     * @Template()
     */
    public function deletelinkedAction(Request $request, $sid)
    {
        $id = $request->request->get('id');
        $em = $this->getManager();

        $entity = new DatabaseLinkedProperties();

        if($request->isXmlHttpRequest()) {

            if ($id){
                $entity = $em->getRepository('AIECmsBundle:BaseDatabaseLinkedProperties')->find($id);

                $subProjectDbLinkedProperties = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')
                    ->findBy(array('basedatabaselinkedpropertyid'
                    => $id));
                foreach ($subProjectDbLinkedProperties as $subProjectDbLinkedProperty) {
                    $subProjectLinkedDb = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')
                        ->find($subProjectDbLinkedProperty->getId());

                    $em->remove($subProjectLinkedDb);
                }
                $em->flush();
            } 

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BaseDatabaseLinkedProperties entity.');
            }

            $em->remove($entity);
            $em->flush();

            return new Response(json_encode(array('result' => 'ok')));

        }
    }

}
