<?php

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\CmsBundle\Entity\WorkshopAttendee;
use AIE\Bundle\CmsBundle\Form\WorkshopAttendeeType;

/**
 * WorkshopAttendee controller.
 *
 * @Route("/subproject/{projectId}/{subprojectId}/workshops/{workshopId}/attendee")
 */
class WorkshopAttendeeController extends CmsBaseController {

    /**
     * Creates a new WorkshopAttendee entity.
     *
     * @Route("/", name="cms_workshops_attendee_create")
     * @Method("POST")
     * @Template("AIECmsBundle:WorkshopAttendee:new.html.twig")
     */
    public function createAction(Request $request, $projectId, $subprojectId, $workshopId)
    {
        $entity = new WorkshopAttendee();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId, $workshopId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getManager();

            $workshop = $em->getRepository('AIECmsBundle:SubProjectWorkshops')->find($workshopId);
            $entity->setWorkshop($workshop);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a WorkshopAttendee entity.
     *
     * @param WorkshopAttendee $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(WorkshopAttendee $entity, $projectId, $subprojectId, $workshopId)
    {
        $form = $this->createForm(new WorkshopAttendeeType(), $entity, array(
            'action' => $this->generateUrl('cms_workshops_attendee_create', ['projectId' => $projectId, 'subprojectId' => $subprojectId, 'workshopId' => $workshopId]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new WorkshopAttendee entity.
     *
     * @Route("/new", name="cms_workshops_attendee_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId, $subprojectId, $workshopId)
    {
        $entity = new WorkshopAttendee();
        $form = $this->createCreateForm($entity, $projectId, $subprojectId, $workshopId);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'workshopId' => $workshopId,
        );
    }

    /**
     * Displays a form to edit an existing WorkshopAttendee entity.
     *
     * @Route("/{id}/edit", name="cms_workshops_attendee_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId, $subprojectId, $workshopId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:WorkshopAttendee')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId, $subprojectId, $workshopId);
        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId, $workshopId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,
            'workshopId' => $workshopId,
        );
    }

    /**
     * Creates a form to edit a WorkshopAttendee entity.
     *
     * @param WorkshopAttendee $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(WorkshopAttendee $entity, $projectId, $subprojectId, $workshopId)
    {
        $form = $this->createForm(new WorkshopAttendeeType(), $entity, array(
            'action' => $this->generateUrl('cms_workshops_attendee_update', array('projectId' => $projectId, 'subprojectId' => $subprojectId, 'workshopId' => $workshopId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing WorkshopAttendee entity.
     *
     * @Route("/{id}", name="cms_workshops_attendee_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:WorkshopAttendee:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $projectId, $subprojectId, $workshopId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:WorkshopAttendee')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId, $subprojectId, $workshopId);
        $editForm = $this->createEditForm($entity, $projectId, $subprojectId, $workshopId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();

            return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a WorkshopAttendee entity.
     *
     * @Route("/{id}", name="cms_workshops_attendee_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $projectId, $subprojectId, $workshopId)
    {
        $form = $this->createDeleteForm($id, $projectId, $subprojectId, $workshopId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:WorkshopAttendee')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cms_workshops', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
    }

    /**
     * Creates a form to delete a WorkshopAttendee entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id,  $projectId, $subprojectId, $workshopId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_workshops_attendee_delete', array('projectId' => $projectId, 'subprojectId' => $subprojectId, 'workshopId' => $workshopId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm();
    }
}
