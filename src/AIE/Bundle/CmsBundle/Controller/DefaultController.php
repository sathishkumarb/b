<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends CmsBaseController
{
    /**
     *
     *
     * @Route("/", name="cms_index")
     * @Method("get")
     * @Template("AIECmsBundle:Default:index.html.twig")
     */

    public function indexAction()
    {

        if ($this->securityHelper->isRoleGranted('ROLE_VERACITY_CCM_DASHBOARD',$this->userRolesMergeToCheck))
        {
            return $this->render(
                'AIECmsBundle:Default:index.html.twig'
            );
        }
        else
        {
            return $this->render(
                'AIEVeracityBundle:Default:accessdenied.html.twig'
            );
        }
    }

    /**
     *
     *
     * @Route("/userdashboard", name="cms_userdashboard")
     * @Method("get")
     * @Template("AIECmsBundle:Default:userdashbaord.html.twig")
     */
    public function usergrantedprojectsAction()
    {
        ini_set('memory_limit', '-1');

        $session = $this->get('session');

        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }

        $em = $this->getManager();

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $projects = $this->getGrantedProjects();

        $subprojects = array();

        $project = array();

        $activities = array();

        $spmr = array();

        $projectInfo = array();

        $emptyprojectInfo = array();

        $finalData=[];
        $projectData=[];
        $checkData=[];

        $kpiRedData = [];

        //variables for total project level counts
        $projCorrOD = 0;
        $projCorrRedCntrlKpi = 0;
        $projCorrRedMngmntKpi = 0;
        $projFAOD = 0;
        $projFARedCntrlKpi = 0;
        $projFARedMngmntKpi= 0;

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $userId = $this->get('security.context')->getToken()->getUser()->getId();

        if (!empty($projects)) {

            foreach ($projects as $index => $project)
            {
                $kpiRedData[$project->getId()] = ['corrosion' => ['mgmt' => 0, 'ctrl' => 0], 'flow' => ['mgmt' => 0, 'ctrl' => 0]];
                $projectInfo[$index]['id'] = $project->getId();
                $projectInfo[$index]['name'] = $project->getName();
                $emptyprojectInfo[$index]['id'] = $project->getId();
                $emptyprojectInfo[$index]['name'] = $project->getName();

                if (!$this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN', $this->userRolesMergeToCheck))
                {
                    $usergroupentities = $em->getRepository('AIECmsBundle:UserProjectGroup')->findBy(array('user' => $userId, 'project' => $project->getId()));
                    foreach ($usergroupentities as $usergroup)
                    {
                        $ids[] = $usergroup->getSubProject()->getId();
                    }
                    $subprojectEntities = $em->getRepository('AIECmsBundle:SubProjects')->findBy(array('id' => $ids));
                }
                else
                {

                    $subprojectEntities = $project->getSubProjects();
                }

                if (!empty($subprojectEntities))
                {
                    foreach ($subprojectEntities as $innerindex => $subprojects)
                    {
                        if (!empty($subprojects))
                        {
                            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojects->getId());
                            $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojects->getId(), 1);

                            if ($subproject && $systems)
                            {
                                $compliance = array();

                                $compliance['control']['redcontrolkpi'] = array();
                                $compliance['management']['redmanagementkpi'] = array();

                                $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
                                $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

                                $compliance['control']['totalredkpi'] = 0;
                                $compliance['control']['totalgreenkpi'] = 0;
                                $compliance['control']['totalamberkpi'] = 0;

                                $compliance['flowassurancecontrol']['totalredkpi'] = 0;
                                $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
                                $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

                                $compliance['flowassurancemanagement']['totalredkpi'] = 0;
                                $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
                                $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

                                $compliance['management']['totalredkpi'] = 0;
                                $compliance['management']['totalgreenkpi'] = 0;
                                $compliance['management']['totalamberkpi'] = 0;

                                $compliance['control']['kpi_control_compliance'] = array();
                                $compliance['management']['kpi_management_compliance'] = array();

                                $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
                                $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

                                $compliance['overallcontrolcompliance'] = 0;
                                $compliance['overallmanagementcompliance'] = 0;

                                $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                                $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                                $compliance['corrOverdue'] = 0;
                                $compliance['faOverdue'] = 0;

                                $indeX = 0;

                                $compliance['totalprojectleveloverallcontrolcompliance']=array();
                                $compliance['totalprojectleveloverallmanagementcompliance']=array();
                                $compliance['totalprojectlevelcontrolcompliance']=array();
                                $compliance['totalprojectlevelmanagementcompliance']=array();
                                $tempControl= 0;
                                $tempManagement= 0;

                                $entityReports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->getMaxGroupedReports($subprojects->getId());

                                foreach ($systems as $system)
                                {
                                    $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojects->getId(), $system->getId(), false);

                                    if ($activitiesAction[$system->getId()])
                                    {
                                        foreach ($activitiesAction[$system->getId()] as $subProjectActivity)
                                        {
                                            if ($subProjectActivity->getId())
                                            {
                                                $id = $subProjectActivity->getId();

                                                $subProjectActivitiesData[$id] = array();

                                                $subProjectActivitiesData[$id]['id'] = $id;
                                                $subProjectActivitiesData[$id]['control']['selected'] = 0;
                                                $subProjectActivitiesData[$id]['management']['selected'] = 0;
                                                $subProjectActivitiesData[$id]['flowassurancecontrol']['selected'] = 0;
                                                $subProjectActivitiesData[$id]['flowassurancemanagement']['selected'] = 0;
                                                $subProjectActivitiesData[$id]['non_monitoring_kpi']['selected'] = 0;
                                                $subProjectActivitiesData[$id]['datasheet'] = $subProjectActivity->getSubprojectdatasheet()->getId();

                                                //control data
                                                if ($subProjectActivity->getIsControl())
                                                {
                                                    $compliance['control']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                                    if ($subProjectActivity->getKpiControl() == "R")
                                                    {
                                                        $compliance['control']['totalredkpi']++;
                                                        $compliance['control']['redcontrolkpi'][] = $subProjectActivity->getId();
                                                    }
                                                    if ($subProjectActivity->getKpiControl() == "G")
                                                        $compliance['control']['totalgreenkpi']++;
                                                    if ($subProjectActivity->getKpiControl() == "A")
                                                        $compliance['control']['totalamberkpi']++;

                                                }

                                                //management data
                                                if ($subProjectActivity->getIsManagement())
                                                {
                                                    $compliance['management']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                                    if ($subProjectActivity->getKpiManagement() == "R") {
                                                        $compliance['management']['totalredkpi']++;
                                                        $compliance['management']['redmanagementkpi'][] = $subProjectActivity->getId();
                                                    }
                                                    if ($subProjectActivity->getKpiManagement() == "G")
                                                        $compliance['management']['totalgreenkpi']++;
                                                    if ($subProjectActivity->getKpiManagement() == "A")
                                                        $compliance['management']['totalamberkpi']++;

                                                }

                                                if ($subProjectActivity->getIsflowassuranceControl())
                                                {
                                                    $compliance['flowassurancecontrol']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                                    if ($subProjectActivity->getKpiControl() == "R") {
                                                        $compliance['flowassurancecontrol']['totalredkpi']++;
                                                        $compliance['flowassurancecontrol']['redcontrolkpi'][] = $subProjectActivity->getId();
                                                    }
                                                    if ($subProjectActivity->getKpiControl() == "G")
                                                        $compliance['flowassurancecontrol']['totalgreenkpi']++;
                                                    if ($subProjectActivity->getKpiControl() == "A")
                                                        $compliance['flowassurancecontrol']['totalamberkpi']++;

                                                }

                                                if ($subProjectActivity->getIsflowassuranceManagement())
                                                {
                                                    $compliance['flowassurancemanagement']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                                    if ($subProjectActivity->getKpiControl() == "R")
                                                    {
                                                        $compliance['flowassurancemanagement']['totalredkpi']++;
                                                        $compliance['flowassurancemanagement']['redmanagementkpi'][] = $subProjectActivity->getId();
                                                    }
                                                    if ($subProjectActivity->getKpiControl() == "G")
                                                        $compliance['flowassurancemanagement']['totalgreenkpi']++;
                                                    if ($subProjectActivity->getKpiControl() == "A")
                                                        $compliance['flowassurancemanagement']['totalamberkpi']++;

                                                }

                                                $subData['activities'] = $subProjectActivitiesData;

                                                $compliance['overallcontrolcompliance'] = 0;
                                                $compliance['overallmanagementcompliance'] = 0;

                                                $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                                                $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                                                $compliance['control']['kpi_control_compliance'] = array_filter($compliance['control']['kpi_control_compliance'], function ($value) {
                                                    return !is_null($value);
                                                });
                                                $compliance['management']['kpi_management_compliance'] = array_filter($compliance['management']['kpi_management_compliance'], function ($value) {
                                                    return !is_null($value);
                                                });

                                                $compliance['flowassurancecontrol']['kpi_control_compliance'] = array_filter($compliance['flowassurancecontrol']['kpi_control_compliance'], function ($value) {
                                                    return !is_null($value);
                                                });
                                                $compliance['flowassurancemanagement']['kpi_management_compliance'] = array_filter($compliance['flowassurancemanagement']['kpi_management_compliance'], function ($value) {
                                                    return !is_null($value);
                                                });

                                                if (!empty($compliance['control']['kpi_control_compliance'])) $compliance['overallcontrolcompliance'] = (array_sum($compliance['control']['kpi_control_compliance']) / count($compliance['control']['kpi_control_compliance'])) * 100;
                                                if (!empty($compliance['management']['kpi_management_compliance'])) $compliance['overallmanagementcompliance'] = (array_sum($compliance['management']['kpi_management_compliance']) / count($compliance['management']['kpi_management_compliance'])) * 100;

                                                if (!empty($compliance['flowassurancecontrol']['kpi_control_compliance'])) $compliance['flowassuranceoverallcontrolcompliance'] = (array_sum($compliance['flowassurancecontrol']['kpi_control_compliance']) / count($compliance['flowassurancecontrol']['kpi_control_compliance'])) * 100;
                                                if (!empty($compliance['flowassurancemanagement']['kpi_management_compliance'])) $compliance['flowassuranceoverallmanagementcompliance'] = (array_sum($compliance['flowassurancemanagement']['kpi_management_compliance']) / count($compliance['flowassurancemanagement']['kpi_management_compliance'])) * 100;

                                                $tempControl = ((float) $compliance['overallcontrolcompliance']) * ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);
                                                $compliance['subprojectcurrentoverallcontrolcompliance'] = $tempControl;
                                                $compliance['subprojectcurrentcontrolkpi']= ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);

                                                $tempManagement = ((float) $compliance['overallmanagementcompliance']) * ($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);
                                                $compliance['subprojectcurrentoverallmanagementcompliance']= $tempManagement;
                                                $compliance['subprojectcurrentmanagementkpi']=($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);

                                                $tempFlowControl = ((float) $compliance['flowassuranceoverallcontrolcompliance']) * ($compliance['flowassurancecontrol']['totalredkpi'] + $compliance['flowassurancecontrol']['totalgreenkpi'] + $compliance['flowassurancecontrol']['totalamberkpi']);
                                                $compliance['subprojectcurrentflowoverallcontrolcompliance'] = $tempFlowControl;
                                                $compliance['subprojectcurrentflowcontrolkpi']= ($compliance['flowassurancecontrol']['totalredkpi'] + $compliance['flowassurancecontrol']['totalgreenkpi'] + $compliance['flowassurancecontrol']['totalamberkpi']);

                                                $tempFlowManagement = ((float) $compliance['flowassuranceoverallmanagementcompliance']) * ($compliance['flowassurancemanagement']['totalredkpi'] + $compliance['flowassurancemanagement']['totalgreenkpi'] + $compliance['flowassurancemanagement']['totalamberkpi']);
                                                $compliance['subprojectcurrentflowoverallmanagementcompliance']= $tempFlowManagement;
                                                $compliance['subprojectcurrentflowmanagementkpi']=($compliance['flowassurancemanagement']['totalredkpi'] + $compliance['flowassurancemanagement']['totalgreenkpi'] + $compliance['flowassurancemanagement']['totalamberkpi']);

                                            }
                                        }
                                    }
                                }

                                $projectInfo[$index]['subprojects'][$innerindex]['id'] = $subprojects->getId();
                                $projectInfo[$index]['subprojects'][$innerindex]['name'] = $subprojects->getName();

                                // latest approved data

                                $latestApprovedDate = null;
                                $latestApprovedArray[$subprojects->getId()] = ['exist'=>false, 'CM'=>null, 'CC'=>null, 'FAM'=>null, 'FAC'=>null];

//                                foreach ($subprojects->getSpmr() as $deepinnerdex => $spmr)
                                foreach ($entityReports as $deepinnerdex => $spmr)
                                {
                                    if ($spmr->getIsApproved())
                                    {
                                        if ($latestApprovedDate == null)
                                        {
                                            $latestApprovedDate = $spmr->getDate();
                                        }
                                        $projectInfo[$index]['subprojects'][$innerindex]['compliance'][$deepinnerdex] = $spmr->getCompliancedata();
                                        $projectInfo[$index]['subprojects'][$innerindex]['complianceDate'][$deepinnerdex] = $spmr->getDate();
                                        $cd = json_decode($spmr->getCompliancedata(),true);
                                        $toDate = new \DateTime($cd['to_date']['date']);
                                        if($latestApprovedDate <= $spmr->getDate())
                                        {
                                            $latestApprovedArray[$subprojects->getId()] = [
                                                'exist'=>true,
                                                'CM'=>$cd['overallmanagementcompliance'],
                                                'CC'=>$cd['overallcontrolcompliance'],
                                                'FAM'=>(isset($cd['flowassuranceoverallmanagementcompliance']) ? $cd['flowassuranceoverallmanagementcompliance'] : null),
                                                'FAC'=>(isset($cd['flowassuranceoverallcontrolcompliance']) ? $cd['flowassuranceoverallcontrolcompliance'] : null)
                                            ];
                                            $latestApprovedDate = $spmr->getDate();
                                        }
                                        //$toDate = date('Y-m-d', strtotime($rto_date->format('Y-m-d')));
                                        if(!isset($finalData[$project->getId()][$toDate->format('Ymd')]))
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')] = ['date'=>null, 'mgmt'=>0, 'ctrl'=>0, 'mgmtkpi'=>0, 'ctrlkpi'=>0, 'flowmgmtkpi'=>0, 'flowctrlkpi'=>0, 'fa_ctrl' =>0, 'fa_mgmt'=>0 , 'overdue' =>0 ,'count'=>0, 'corrOverdue'=>0, 'faOverdue'=>0, 'totalRedControlKpi' =>0, 'totalRedManagementKpi' =>0,];
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['date'] = $toDate->format('Y-m-d');
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['mgmt'] +=  ((float) $cd['overallmanagementcompliance']) * ($cd['management']['totalredkpi'] + $cd['management']['totalgreenkpi'] + $cd['management']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['ctrl'] +=  ((float) $cd['overallcontrolcompliance']) * ($cd['control']['totalredkpi'] + $cd['control']['totalgreenkpi'] + $cd['control']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['mgmtkpi'] +=  ($cd['management']['totalredkpi']+$cd['management']['totalgreenkpi']+$cd['management']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['ctrlkpi'] +=  ($cd['control']['totalredkpi']+$cd['control']['totalgreenkpi']+$cd['control']['totalamberkpi']);

                                            if (isset($cd['flowassurancemanagement']['totalredkpi']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['flowmgmtkpi'] += ($cd['flowassurancemanagement']['totalredkpi'] + $cd['flowassurancemanagement']['totalgreenkpi'] + $cd['flowassurancemanagement']['totalamberkpi']);
                                            }
                                            if (isset($cd['flowassurancecontrol']['totalredkpi']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['flowctrlkpi'] +=  ($cd['flowassurancecontrol']['totalredkpi'] + $cd['flowassurancecontrol']['totalgreenkpi'] + $cd['flowassurancecontrol']['totalamberkpi']);
                                            }

                                            if (isset($cd['flowassuranceoverallcontrolcompliance']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  ((float) $cd['flowassuranceoverallcontrolcompliance']) * ($cd['flowassurancecontrol']['totalredkpi'] + $cd['flowassurancecontrol']['totalgreenkpi'] + $cd['flowassurancecontrol']['totalamberkpi']);
                                            }
                                            else
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  0;
                                            }
                                            if (isset($cd['flowassuranceoverallmanagementcompliance']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  ((float) $cd['flowassuranceoverallmanagementcompliance']) * ($cd['flowassurancemanagement']['totalredkpi'] + $cd['flowassurancemanagement']['totalgreenkpi'] + $cd['flowassurancemanagement']['totalamberkpi']);
                                            }
                                            else
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  0;
                                            }
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['overdue'] +=  $cd['overdue'];

                                            $finalData[$project->getId()][$toDate->format('Ymd')]['count'] +=  1;
                                        }
                                        else
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['mgmt'] +=  ((float) $cd['overallmanagementcompliance']) * ($cd['management']['totalredkpi'] + $cd['management']['totalgreenkpi'] + $cd['management']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['ctrl'] +=  ((float) $cd['overallcontrolcompliance']) * ($cd['control']['totalredkpi'] + $cd['control']['totalgreenkpi'] + $cd['control']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['mgmtkpi'] +=  ($cd['management']['totalredkpi']+$cd['management']['totalgreenkpi']+$cd['management']['totalamberkpi']);
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['ctrlkpi'] +=  ($cd['control']['totalredkpi']+$cd['control']['totalgreenkpi']+$cd['control']['totalamberkpi']);

                                            if (isset($cd['flowassurancemanagement']['totalredkpi']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['flowmgmtkpi'] += ($cd['flowassurancemanagement']['totalredkpi'] + $cd['flowassurancemanagement']['totalgreenkpi'] + $cd['flowassurancemanagement']['totalamberkpi']);
                                            }
                                            if (isset($cd['flowassurancecontrol']['totalredkpi']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['flowctrlkpi'] +=  ($cd['flowassurancecontrol']['totalredkpi'] + $cd['flowassurancecontrol']['totalgreenkpi'] + $cd['flowassurancecontrol']['totalamberkpi']);
                                            }

                                            if (isset($cd['flowassuranceoverallcontrolcompliance']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  ((float) $cd['flowassuranceoverallcontrolcompliance']) * ($cd['flowassurancecontrol']['totalredkpi'] + $cd['flowassurancecontrol']['totalgreenkpi'] + $cd['flowassurancecontrol']['totalamberkpi']);
                                            }
                                            else
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  0;
                                            }
                                            if (isset($cd['flowassuranceoverallmanagementcompliance']))
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  ((float) $cd['flowassuranceoverallmanagementcompliance']) * ($cd['flowassurancemanagement']['totalredkpi'] + $cd['flowassurancemanagement']['totalgreenkpi'] + $cd['flowassurancemanagement']['totalamberkpi']);
                                            }
                                            else
                                            {
                                                $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  0;
                                            }
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['overdue'] +=  $cd['overdue'];
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['count'] +=  1;
                                        }
                                        $kpiRedData[$project->getId()]['corrosion']['mgmt']+= $cd['management']['totalredkpi'];
                                        $kpiRedData[$project->getId()]['corrosion']['ctrl']+= $cd['control']['totalredkpi'];
                                        if (isset($cd['flowassurancemanagement']['totalredkpi']))
                                        {
                                            $kpiRedData[$project->getId()]['flow']['mgmt']+= $cd['flowassurancemanagement']['totalredkpi'];
                                        }
                                        if (isset($cd['flowassurancecontrol']['totalredkpi']))
                                        {
                                            $kpiRedData[$project->getId()]['flow']['ctrl']+= $cd['flowassurancecontrol']['totalredkpi'];
                                        }
                                        if(!in_array($project->getId(),$checkData))
                                        {
                                            array_push($checkData,$project->getId());
                                        }
                                    }
                                }
                                //current data
                                $latestCurrentDate = null;
                                $latestCurrentArray[$subprojects->getId()] = ['exist'=>false, 'CM'=>null, 'CC'=>null, 'FAM'=>null, 'FAC'=>null];

                                $currentCM = 0;
                                $currentCC = 0;
                                $currentFAM = 0;
                                $currentFAC = 0;

                                if($latestCurrentDate == null)
                                {
                                    //$cd = json_decode($spmr->getCompliancedata(),true);
                                    $latestCurrentArray[$subprojects->getId()] = [
                                        'exist'=>true,
                                        'CM'=>$compliance['overallmanagementcompliance'],
                                        'CC'=>$compliance['overallcontrolcompliance'],
                                        'FAM'=>(isset($compliance['flowassuranceoverallmanagementcompliance']) ? $compliance['flowassuranceoverallmanagementcompliance'] : null),
                                        'FAC'=>(isset($compliance['flowassuranceoverallcontrolcompliance']) ? $compliance['flowassuranceoverallcontrolcompliance'] : null)
                                    ];

                                    $currentCM = $compliance['overallmanagementcompliance'];
                                    $currentCC = $compliance['overallcontrolcompliance'];
                                    if (isset($compliance['flowassuranceoverallmanagementcompliance']))
                                    {
                                        $currentFAM = $compliance['flowassuranceoverallmanagementcompliance'];
                                    }
                                    if (isset($compliance['flowassuranceoverallmanagementcompliance']))
                                    {
                                        $currentFAC = $compliance['flowassuranceoverallcontrolcompliance'];
                                    }
                                    $currentCount = 0;
                                    $latestCurrentDate = new \DateTime("now");
                                }

                                // To calculate the overdue count for Corrosion Management, Flow Assurance and Total count
                                if (!empty($subprojects->getActivity()))
                                {
                                    foreach ($subprojects->getActivity() as $deepinnerdex => $activities)
                                    {
                                        $checkActivity = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                                        if (!empty($checkActivity) && $activities->getNextInspectionDate() && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1)
                                        {
                                            if ($activities->getNextInspectionDate())
                                            {
                                                $nextDate = $activities->getNextInspectionDate(true);
                                                $nextDate_U = (int)$nextDate->format('U');
                                                $current_date = new \DateTime("now");
                                                $current_date_U = (int)$current_date->format('U');
                                                //set repeated action last time to next date

                                                while ($nextDate_U <= $current_date_U)
                                                {
                                                    if (($activities->getIsControl()) || ($activities->getIsManagement()))
                                                    {
                                                        $compliance['corrOverdue'] +=  1;
                                                    }
                                                    elseif (($activities->getIsflowassuranceControl()) || ($activities->getIsflowassuranceManagement()))
                                                    {
                                                        $compliance['faOverdue']+=1;
                                                    }
                                                    //set next date from repeated action
                                                    $activities->setLastInspectionDate($nextDate);
                                                    $nextDate = $activities->getNextInspectionDate(true);
                                                    $nextDate_U = (int)$nextDate->format('U');
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    $emptyprojectInfo[$index]['subprojects'][$innerindex]['id'] = $subprojects->getId();
                                    $emptyprojectInfo[$index]['subprojects'][$innerindex]['name'] = $subprojects->getName();
                                }
                                // end of overdue calculation on main dashboard

                                if($latestCurrentDate != null)
                                {
                                    $toDate = new \DateTime();
                                    if(!isset($finalData[$project->getId()][$toDate->format('Ymd')]))
                                    {
                                        $finalData[$project->getId()][$toDate->format('Ymd')] = ['date'=>null,'mgmt'=>0,'ctrl'=>0, 'mgmtkpi'=>0, 'ctrlkpi'=>0, 'flowmgmtkpi'=>0, 'flowctrlkpi'=>0, 'fa_ctrl' =>0, 'fa_mgmt'=>0 , 'overdue' =>0 ,'count'=>0, 'corrOverdue'=>0, 'faOverdue'=>0, 'totalRedControlKpi' =>0, 'totalRedManagementKpi' =>0,];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['date'] = 'Current';
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['mgmt']+=  $compliance['subprojectcurrentoverallmanagementcompliance'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['ctrl']+=  $compliance['subprojectcurrentoverallcontrolcompliance'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['mgmtkpi']+=  $compliance['subprojectcurrentmanagementkpi'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['ctrlkpi']+=  $compliance['subprojectcurrentcontrolkpi'];

                                        $finalData[$project->getId()][$toDate->format('Ymd')]['flowmgmtkpi']+=  $compliance['subprojectcurrentflowmanagementkpi'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['flowctrlkpi']+=  $compliance['subprojectcurrentflowcontrolkpi'];

                                        if (isset($compliance['subprojectcurrentflowoverallcontrolcompliance']))
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  $compliance['subprojectcurrentflowoverallcontrolcompliance'];
                                        }
                                        if (isset($compliance['subprojectcurrentflowoverallmanagementcompliance']))
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  $compliance['subprojectcurrentflowoverallmanagementcompliance'];
                                        }

                                        $finalData[$project->getId()][$toDate->format('Ymd')]['count'] +=  1;
                                    }
                                    else
                                    {
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['mgmt']+=  $compliance['subprojectcurrentoverallmanagementcompliance'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['ctrl']+=  $compliance['subprojectcurrentoverallcontrolcompliance'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['mgmtkpi']+=  $compliance['subprojectcurrentmanagementkpi'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['ctrlkpi']+=  $compliance['subprojectcurrentcontrolkpi'];

                                        $finalData[$project->getId()][$toDate->format('Ymd')]['flowmgmtkpi']+=  $compliance['subprojectcurrentflowmanagementkpi'];
                                        $finalData[$project->getId()][$toDate->format('Ymd')]['flowctrlkpi']+=  $compliance['subprojectcurrentflowcontrolkpi'];

                                        if (isset($compliance['subprojectcurrentflowoverallcontrolcompliance']))
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['fa_ctrl'] +=  $compliance['subprojectcurrentflowoverallcontrolcompliance'];
                                        }
                                        if (isset($compliance['subprojectcurrentflowoverallmanagementcompliance']))
                                        {
                                            $finalData[$project->getId()][$toDate->format('Ymd')]['fa_mgmt'] +=  $compliance['subprojectcurrentflowoverallmanagementcompliance'];
                                        }

                                        $finalData[$project->getId()][$toDate->format('Ymd')]['count'] +=  1;
                                    }
                                    if(!in_array($project->getId(),$checkData)){
                                        array_push($checkData,$project->getId());
                                    }
                                }
                                else
                                {
                                    $toDate = new \DateTime();
                                    $finalData[$project->getId()][$toDate->format('Ymd')] = ['date'=>null,'mgmt'=>0,'ctrl'=>0, 'mgmtkpi'=>0, 'ctrlkpi'=>0, 'fa_ctrl' =>0, 'fa_mgmt'=>0 , 'overdue' =>0 ,'count'=>0, 'corrOverdue'=>0, 'faOverdue'=>0, 'totalRedControlKpi' =>0, 'totalRedManagementKpi' =>0,];
                                }

                                if (!empty($subprojects->getActivity()))
                                {
                                    foreach ($subprojects->getActivity() as $deepinnerdex => $activities)
                                    {
                                        $checkActivity = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                                        if (!empty($checkActivity) && $activities->getNextInspectionDate() && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1)
                                        {
                                            if ($activities->getNextInspectionDate())
                                            {
                                                $nextDate = $activities->getNextInspectionDate(true);
                                                $nextDate_U = (int)$nextDate->format('U');
                                                $current_date = new \DateTime("now");
                                                $current_date_U = (int)$current_date->format('U');
                                                //set repeated action last time to next date
                                                while ($nextDate_U <= $current_date_U)
                                                {
                                                    $activities->setLastInspectionDate($nextDate);
                                                    if (($subProjectActivity->getIsControl()) || ($subProjectActivity->getIsManagement()))
                                                    {
                                                        $compliance['corrOverdue']++;
                                                    }
                                                    elseif (($subProjectActivity->getIsflowassuranceControl()) || ($subProjectActivity->getIsflowassuranceManagement()))
                                                    {
                                                        $compliance['faOverdue']++;
                                                    }
                                                    //set next date from repeated action
                                                    $nextDate = $activities->getNextInspectionDate(true);
                                                    $nextDate_U = (int)$nextDate->format('U');
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    $emptyprojectInfo[$index]['subprojects'][$innerindex]['id'] = $subprojects->getId();
                                    $emptyprojectInfo[$index]['subprojects'][$innerindex]['name'] = $subprojects->getName();
                                }
                                $projectInfo[$index]['subprojects'][$innerindex]['corrOverdue'] = $compliance['corrOverdue'];
                                $projectInfo[$index]['subprojects'][$innerindex]['faOverdue'] = $compliance['faOverdue'];
                                $projectInfo[$index]['subprojects'][$innerindex]['totalRedControlKpi'] = $compliance['control']['totalredkpi'];
                                $projectInfo[$index]['subprojects'][$innerindex]['totalRedManagementKpi'] = $compliance['management']['totalredkpi'];
                                $projectInfo[$index]['subprojects'][$innerindex]['flowtotalRedControlKpi'] = $compliance['flowassurancecontrol']['totalredkpi'];
                                $projectInfo[$index]['subprojects'][$innerindex]['flowtotalRedManagementKpi'] = $compliance['flowassurancemanagement']['totalredkpi'];
                            }
                        }
                        else
                        {
                            $emptyprojectInfo[$index]['subprojects'][$innerindex]['id'] = $subprojects->getId();
                            $emptyprojectInfo[$index]['subprojects'][$innerindex]['name'] = $subprojects->getName();

                            $projectInfo[$index]['subprojects'][$innerindex]['corrOverdue'] = 0;
                            $projectInfo[$index]['subprojects'][$innerindex]['faOverdue'] = 0;
                            $projectInfo[$index]['subprojects'][$innerindex]['totalRedControlKpi'] = 0;
                            $projectInfo[$index]['subprojects'][$innerindex]['totalRedManagementKpi'] = 0;
                            $projectInfo[$index]['subprojects'][$innerindex]['flowtotalRedControlKpi'] = 0;
                            $projectInfo[$index]['subprojects'][$innerindex]['flowtotalRedManagementKpi'] = 0;
                        }

                        // logic for accumulating the sub project counts into variables for project level counts
                        $projCorrOD += $compliance['corrOverdue'];
                        $projCorrRedCntrlKpi +=  $compliance['control']['totalredkpi'];
                        $projCorrRedMngmntKpi +=  $compliance['management']['totalredkpi'];

                        $projFAOD +=  $compliance['faOverdue'];
                        $projFARedCntrlKpi +=  $compliance['flowassurancecontrol']['totalredkpi'];
                        $projFARedMngmntKpi +=  $compliance['flowassurancemanagement']['totalredkpi'];

                        //Adding data to session to reuse on the subproject dashboard - Sub project level counts

                        //At sub project level
                        $session->set('CorrOD_' . $project->getId() . '_' . $subprojects->getId(),isset($compliance['corrOverdue'])? $compliance['corrOverdue']: 0);
                        $session->set('FAOD_' . $project->getId() . '_' . $subprojects->getId(),isset($compliance['faOverdue'])? $compliance['faOverdue']:0);

                        $compliance['corrOverdue']=0;
                        $compliance['faOverdue']=0;
                    }
                }

                //Adding data to session to reuse on the subproject dashboard at project level
                $session->set('ProjCorrOD_'. $project->getId(),isset($projCorrOD)? $projCorrOD : 0);
                $session->set('ProjFAOD_'. $project->getId(),isset($projFAOD)? $projFAOD : 0);
                $session->set('CorrRedControlKpi_'. $project->getId(),isset($projCorrRedCntrlKpi)? $projCorrRedCntrlKpi : 0);
                $session->set('CorrRedMngmntKpi_'. $project->getId() ,isset($projCorrRedMngmntKpi)? $projCorrRedMngmntKpi: 0);
                $session->set('FARedControlKpi_'. $project->getId() ,isset($projFARedCntrlKpi)? $projFARedCntrlKpi: 0);
                $session->set('FARedMngmntKpi_'. $project->getId() ,isset($projFARedMngmntKpi)? $projFARedMngmntKpi: 0);

                // end of session logic

                //clearing project level counts so that they are reset and counted correctly for next project
                $projCorrOD =0;
                $projCorrRedCntrlKpi =0;
                $projCorrRedMngmntKpi =0;

                $projFAOD =0;
                $projFARedCntrlKpi=0;
                $projFARedMngmntKpi =0;
            }
        }

        foreach ($finalData as $pId=>$data)
        {
            ksort($finalData[$pId]);
        }

        $checkDate = new \DateTime();

        foreach ($finalData as $pId=>$data)
        {
            foreach ($data as $dId=>$items)
            {

                if (!empty( $items['mgmt']) && !empty($items['mgmtkpi']) )
                {
                    $items['mgmt'] = ($items['mgmt'] / $items['mgmtkpi']);

                }
                else{
                    $items['mgmt'] =0;
                }

                if (!empty( $items['ctrl']) && !empty($items['ctrlkpi']) )
                {
                    $items['ctrl'] = ($items['ctrl'] / $items['ctrlkpi']);

                }
                else
                {
                    $items['ctrl'] =0;
                }


                if (!empty( $items['fa_mgmt']) && !empty($items['flowmgmtkpi']) )
                {
                    $items['fa_mgmt'] = ($items['fa_mgmt'] / $items['flowmgmtkpi']);

                }
                else{
                    $items['fa_mgmt'] =0;
                }

                if (!empty( $items['fa_ctrl']) && !empty($items['flowctrlkpi']) )
                {
                    $items['fa_ctrl'] = ($items['fa_ctrl'] / $items['flowctrlkpi']);

                }
                else
                {
                    $items['fa_ctrl'] =0;
                }

                $projectData[$pId][$dId] = [
                    'date' => ($items['date']),
                    'mgmt' => $items['mgmt'],
                    'ctrl' => $items['ctrl'],
                    'fa_mgmt' => $items['fa_mgmt'],
                    'fa_ctrl' => $items['fa_ctrl'],
//                    'overdue' => $items['overdue'],
                ];

            }

        }

        if ($this->securityHelper->isRoleGranted('ROLE_VERACITY_CCM_DASHBOARD',$this->userRolesMergeToCheck))
        {
            return [
                'projects' => $projectInfo,
                'projects_trend'=>$projectData,
                'validate_data'=>$checkData,
                'approved_data'=>$latestApprovedArray,
                'current_data'=>$latestCurrentArray,
                'emptyprojects' =>$emptyprojectInfo,
            ];
        }
        else
        {

            return $this->render(
                'AIEVeracityBundle:Default:accessdenied.html.twig'
            );
        }
    }
}
