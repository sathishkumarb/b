<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase as SamplingDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;

use AIE\Bundle\CmsBundle\Form\SubProjectSamplingDatabaseType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * SubProjectSamplingDatabase controller.
 *
 * @Route("/{projectId}/subprojectoutstandingactions/{subprojectId}")
 */
class SubProjectOutstandingActionsController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = [];

    Protected $entityCheck = "";

    public function __construct(){
        $this->findduplicateproperties = array('serviceid','samplingorientationid','activitypersonid');
    }

    /**
     * Lists all SubProjectOutstandingActions entities.
     *
     * @Route("/", name="cms_subproject_outstandingactions")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId, $subprojectId)
    {

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $subprojectsystems = $cmsDatabasesHelper->getSubProjectAllSystemsDatabaseItems($subprojectId,4);

        $samplingdatabases = $cmsDatabasesHelper->getSubProjectAllSystemsDatabaseItemsPerDb($subprojectId,4);

        return array(
            'systems'                     => $subprojectsystems,
            'samplingdatabases'           => $samplingdatabases,
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'wizard_title'                => 'Outstanding Actions and Recommendations'
        );

    }
    /**
     * Lists all databases related to subproject entities.
     *
     * @Route("/{sid}/loadsystems", name="cms_subproject_systemoutstandingactions")
     * @Method("GET")
     * @Template()
     */
    public function loadsystemAction(Request $request,$sid, $projectId, $subprojectId)
    {
        if (! $request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        $em = $this->getManager();

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        $cathodicdatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 1, $sid);
        $chemicaldatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 2, $sid);
        $corrosiondatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 3, $sid);
        $samplingdatabases = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($subprojectId, 4, $sid);
        $alldatabases = array_merge( $cathodicdatabases, $chemicaldatabases, $corrosiondatabases, $samplingdatabases);
        return $this->render('AIECmsBundle:SubProjectOutstandingActions:systemsoutstandingactions.html.twig',array(
            'alldatabases'  => $alldatabases,'systemid'=>$sid,'projectId'=>$projectId,'projectId'=>$subprojectId));

    }

}
