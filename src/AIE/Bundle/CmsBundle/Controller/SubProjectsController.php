<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\CathodicProtectionDatasheet;
use AIE\Bundle\CmsBundle\Entity\ControlActivity;
use AIE\Bundle\CmsBundle\Entity\CorrosionInhibitor;
use AIE\Bundle\CmsBundle\Entity\CorrosionInhibitorAvailability;
use AIE\Bundle\CmsBundle\Entity\GeneralCorrosion;
use AIE\Bundle\CmsBundle\Entity\PittingCorrosion;
use AIE\Bundle\CmsBundle\Entity\SubProjectDashboardActivity;
use AIE\Bundle\CmsBundle\Entity\UserProjectGroup;
use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\CmsBundle\Entity\SubProjects;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectSystems;
use AIE\Bundle\CmsBundle\Entity\SubProjectThreats;
use AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques;
use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet;
use AIE\Bundle\CmsBundle\Entity\ActionTracker;
use AIE\Bundle\CmsBundle\Entity\ActionTrackerItems;
use AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops;
use AIE\Bundle\CmsBundle\Entity\WorkshopAttendee;
use AIE\Bundle\CmsBundle\Entity\ActionOwners;
use AIE\Bundle\CmsBundle\Entity\Files;
use AIE\Bundle\CmsBundle\Entity\FilesCategories;
use AIE\Bundle\CmsBundle\Form\SubProjectsType;
use AIE\Bundle\CmsBundle\Form\SubProjectsEditType;

use AIE\Bundle\CmsBundle\Form\ActivityPlanningType;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Projects controller.
 *
 * @Route("/{projectId}/subprojects")
 */
class SubProjectsController extends CmsBaseController
{

    /**
     * Lists all Projects entities.
     *
     * @Route("/", name="cms_subprojects")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($projectId);

        //$entities = $this->getGrantedProjects();

        //$entities = $em->getRepository('AIECmsBundle:SubProjects')->findAll();

        //$entities = $this->getGrantedProjects();
        $userId = $this->get('security.context')->getToken()->getUser()->getId();

        $usergroupentities = $em->getRepository('AIECmsBundle:UserProjectGroup')->findBy(array('user'=>$userId,'project'=>$projectId));
        foreach($usergroupentities as $usergroup){
            $ids[]= $usergroup->getSubProject()->getId();
        }
        $entities = $em->getRepository('AIECmsBundle:SubProjects')->findBy(array('id' => $ids));

        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );

        return [
            'entity' => $entity,
            'entities' => $entities,
            'pagination' => $pagination,
        ];
    }

    /**
     * @param $projectId, $subprojectId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createActivityFilterForm($projectId, $subprojectId, \DateTime $from_date, \DateTime $to_date, $submit_form)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($submit_form, array('projectId' => $projectId, 'id' => $subprojectId)))
            ->setMethod('POST');

        $form->add('filter_form', new ActivityPlanningType($from_date, $to_date));

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate', 'attr' => ['class' => 'btn btn-primary btn-block']), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Creates a new Projects entity.
     *
     * @Route("/", name="cms_subprojects_create")
     * @Method("POST")
     * @Template("AIECmsBundle:SubProjects:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new SubProjects();
        $em = $this->getManager();
        //$type = $request->request->get('aie_bundle_cmsbundle_projects')['type'];

        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();

            if ($data->getFile()) {
                $file = $data->getFile();
                $uploadedPath = $this->uploadFile($file, $entity);
                $entity->setFilepath($uploadedPath);
                $entity->setFolder(md5(uniqid(rand(), true)));
            }

            $project = $em->getRepository('AIECmsBundle:Projects')->find($projectId);
            $entity->setProject($project);

            /*  */
            $entity->setDateCreate(new \DateTime("now"));

            $securityContext = $this->get('security.context');
            $token = $securityContext->getToken();
            $user = $token->getUser();
            $userId = $user->getId();
            $entity->setCreatedBy($userId);

            /* Add default Class, threats and design codes */
            $dbproperties = $em->getRepository('AIECmsBundle:BaseDatabaseProperties')->findAll();
            $dblinkedproperties = $em->getRepository('AIECmsBundle:BaseDatabaseLinkedProperties')->findAll();

            foreach ($dbproperties as $c) {
                $dbproperties_e = new SubProjectDatabaseProperties();
                $dbproperties_e->setColId($c->getColId());
                $dbproperties_e->setColValue($c->getColValue());
                $dbproperties_e->setSubProject($entity);
                $dbproperties_e->setBasedatabasepropertyid($c->getId());
                $entity->addBasedatabaseproperties($dbproperties_e);
                $em->persist($dbproperties_e);
            }

            foreach ($dblinkedproperties as $t) {
                $dblinkproperties_e = new SubProjectDatabaseLinkedProperties();
                $dblinkproperties_e->setColIdOne($t->getColIdOne());
                $dblinkproperties_e->setColIdTwo($t->getColIdTwo());
                $dblinkproperties_e->setColIdOneValue($t->getColIdOneValue());
                $dblinkproperties_e->setColIdTwoValue($t->getColIdTwoValue());
                $dblinkproperties_e->setSubProject($entity);
                $dblinkproperties_e->setBasedatabaselinkedpropertyid($c->getId());
                $entity->addBasedatabaselinkedproperties($dblinkproperties_e);
                $em->persist($dblinkproperties_e);
            }

            $dbsystems = $em->getRepository('AIECmsBundle:BaseSystems')->findAll();

            foreach ($dbsystems as $dbsystem) {

                $subProjectSystems = new SubProjectSystems();

                $oldReflection = new \ReflectionObject($dbsystem);
                $newReflection = new \ReflectionObject($subProjectSystems);

                foreach ($oldReflection->getProperties() as $property) {
                    if ($newReflection->hasProperty($property->getName())) {
                        $newProperty = $newReflection->getProperty($property->getName());
                        $property->setAccessible(true);
                        $newProperty->setAccessible(true);
                        $newProperty->setValue($subProjectSystems, $property->getValue($dbsystem));
                    }
                    $subProjectSystems->setBasesystemid($dbsystem->getId());
                    $subProjectSystems->setSubproject($entity);

                    $em->persist($subProjectSystems);
                }
            }


            $dbthreats = $em->getRepository('AIECmsBundle:BaseThreats')->findAll();

            foreach ($dbthreats as $dbthreat) {

                $subProjectThreats = new SubProjectThreats();
                $oldReflection = new \ReflectionObject($dbthreat);
                $newReflection = new \ReflectionObject($subProjectThreats);

                foreach ($oldReflection->getProperties() as $property) {
                    if ($newReflection->hasProperty($property->getName())) {
                        $newProperty = $newReflection->getProperty($property->getName());
                        $property->setAccessible(true);
                        $newProperty->setAccessible(true);
                        $newProperty->setValue($subProjectThreats, $property->getValue($dbthreat));
                    }

                    //$subProjectThreats->setSubProjectSystems($em->getRepository('AIECmsBundle:SubProjectSystems')->find($dbsystem->getId()));
                    $subProjectThreats->setBasesystemid($dbthreat->getBaseSystems()->getId());
                    $subProjectThreats->setBasethreatid($dbthreat->getId());
                    $subProjectThreats->setSubproject($entity);
                    $em->persist($subProjectThreats);
                }

            }


            $dbmitigationtechniques = $em->getRepository('AIECmsBundle:BaseMitigationTechniques')->findAll();
            foreach ($dbmitigationtechniques as $dbmitigationtechnique) {

                $subProjectMitigationTechniques = new SubProjectMitigationTechniques();

                $oldReflection = new \ReflectionObject($dbmitigationtechnique);
                $newReflection = new \ReflectionObject($subProjectMitigationTechniques);

                foreach ($oldReflection->getProperties() as $property) {
                    if ($newReflection->hasProperty($property->getName())) {
                        $newProperty = $newReflection->getProperty($property->getName());
                        $property->setAccessible(true);
                        $newProperty->setAccessible(true);
                        $newProperty->setValue($subProjectMitigationTechniques, $property->getValue($dbmitigationtechnique));
                    }

                    //$subProjectMitigationTechniques->setSubProjectThreats($em->getRepository('AIECmsBundle:SubProjectThreats')->find($dbthreat->getId()));
                    $subProjectMitigationTechniques->setBasethreatid($dbmitigationtechnique->getBaseThreats()->getId());

                    $subProjectMitigationTechniques->setBasemitigationtechniqueid($dbmitigationtechnique->getId());
                    $subProjectMitigationTechniques->setSubproject($entity);
                    $em->persist($subProjectMitigationTechniques);
                }
            }


            $dbactivities = $em->getRepository('AIECmsBundle:BaseActivity')->findAll();

            foreach ($dbactivities as $dbactivity) {

                $subProjectActivity = new SubProjectActivity();

                $subProjectActivity->setCreatedAt($dbactivity->getCreatedAt());
                $subProjectActivity->setModifiedAt($dbactivity->getModifiedAt());
                $subProjectActivity->setFrequency($dbactivity->getFrequency());
                $subProjectActivity->setActivityperson($dbactivity->getActivityperson());
                $subProjectActivity->setWeightage($dbactivity->getWeightage());
                $subProjectActivity->setConsequencesexcursion($dbactivity->getConsequencesexcursion());
                $subProjectActivity->setRemedialaction($dbactivity->getRemedialaction());
                $subProjectActivity->setSubProjectmetrics($dbactivity->getBasemetrics());
                $subProjectActivity->setSubProjectdatasheet($dbactivity->getBasedatasheet());
                $subProjectActivity->setControltitle($dbactivity->getControltitle());
                $subProjectActivity->setControlgreenexpression($dbactivity->getControlgreenexpression());
                $subProjectActivity->setControlgreen($dbactivity->getControlgreen());
                $subProjectActivity->setControlamberexpression($dbactivity->getControlamberexpression());
                $subProjectActivity->setControlamber($dbactivity->getControlamber());
                $subProjectActivity->setControlredexpression($dbactivity->getControlredexpression());
                $subProjectActivity->setControlred($dbactivity->getControlred());
                $subProjectActivity->setControlgreenexpressionrange($dbactivity->getControlgreenexpressionrange());
                $subProjectActivity->setControlgreenrange($dbactivity->getControlgreenrange());
                $subProjectActivity->setControlamberexpressionrange($dbactivity->getControlamberexpression());
                $subProjectActivity->setControlamberrange($dbactivity->getControlamberrange());
                $subProjectActivity->setControlredexpressionrange($dbactivity->getControlredexpressionrange());
                $subProjectActivity->setControlredrange($dbactivity->getControlredrange());
                $subProjectActivity->setManagementtitle($dbactivity->getManagementtitle());
                $subProjectActivity->setManagementgreen($dbactivity->getManagementgreen());
                $subProjectActivity->setManagementamber($dbactivity->getManagementamber());
                $subProjectActivity->setManagementred($dbactivity->getManagementred());

                //$subProjectActivity->setSubProjectMitigationTechniques($em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($dbmitigationtechnique->getId()));
                $subProjectActivity->setBasemitigationtechniqueid($dbactivity->getBaseMitigationTechniques()->getId());
                $subProjectActivity->setBaseactivityid($dbactivity->getId());
                $subProjectActivity->setSubproject($entity);

                $em->persist($subProjectActivity);
            }

            $em->persist($entity);

            $em->flush();
            $em->clear();

            $this->addFlash('success', 'New Sub-Project '.$entity->getName().' created');

            return $this->redirect($this->generateUrl('cms_list_subprojects', ['id' => $projectId]));

        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubProjects $entity, $projectId)
    {
        $form = $this->createForm(
            new SubProjectsType(),
            $entity,
            [
                'action' => $this->generateUrl('cms_subprojects_create', array('projectId' => $projectId)),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Projects entity.
     *
     * @Route("/new", name="cms_subprojects_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        $entity = new SubProjects();
        $form = $this->createCreateForm($entity,$projectId);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}", name="cms_subprojects_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $projectId)
    {
//        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
//            throw $this->createAccessDeniedException('You do not have permission to view the project.');
//        }

        $em = $this->getManager();

        /** @var Projects $entity */
        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sub Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId);
        return [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView()
        ];
    }

    public function sortFunction( $a, $b ) {
        return strtotime($a["date"]) - strtotime($b["date"]);
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/ajaxdashboard/{type}/{fromdate}/{todate}", name="cms_subprojects_ajaxdashboard")
     * @Method({"GET", "POST"})
     * @Template("AIECmsBundle:SubProjects:ajaxdashboard.html.twig")
     */
    public function ajaxdashboardAction(Request $request, $projectId, $id, $type, $fromdate, $todate)
    {
        ini_set('memory_limit', '-1');

        //Sub-project level counts passed from defaultController
        $corrOD = $this->get('session')->get('CorrOD_'.$projectId.'_'.$id);
        $faOD = $this->get('session')->get('FAOD_'.$projectId.'_'.$id);

        //project level counts

        $ProjCorrOD = $this->get('session')->get('ProjCorrOD_'.$projectId);
        $CorrRedControlKpi = $this->get('session')->get('CorrRedControlKpi_'.$projectId);
        $CorrRedMngmntKpi = $this->get('session')->get('CorrRedMngmntKpi_'.$projectId);

        $ProjFAOD = $this->get('session')->get('ProjFAOD_'.$projectId);
        $FARedControlKpi = $this->get('session')->get('FARedControlKpi_'.$projectId);
        $FARedMngmntKpi = $this->get('session')->get('FARedMngmntKpi_'.$projectId);

        //end of project level counts
        $RedKPI_ODCounts = array('CorrOD'=>$ProjCorrOD,'CorrCntrl'=>$CorrRedControlKpi,'CorrMngmt'=>$CorrRedMngmntKpi,'FAOD'=>$ProjFAOD,'FACntrl'=>$FARedControlKpi,'FAMgmt'=>$FARedMngmntKpi,'SP_CorrOD'=>$corrOD,'SP_FAOD'=>$faOD);

        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }
        $tab = $type;
        $chartHasPrediction = null;
        $ctrlPrediction = null;
        $mgmtPrediction = null;
        $redcontrolCompliance = array();
        $redmanagementCompliance = array();
        $subRedControl = array();
        $subRedManagement = array();
        $overdueActivities = array();
        $systemCompliance =[];
        $enabledSystems = ['CM'=>true,'FA'=>true];
        $compliance = array();

        $compliance['control']['redcontrolkpi'] = array();
        $compliance['management']['redmanagementkpi'] = array();

        $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
        $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

        $compliance['control']['totalredkpi'] = 0;
        $compliance['control']['totalgreenkpi'] = 0;
        $compliance['control']['totalamberkpi'] = 0;

        $compliance['flowassurancecontrol']['totalredkpi'] = 0;
        $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
        $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

        $compliance['flowassurancemanagement']['totalredkpi'] = 0;
        $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
        $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

        $compliance['management']['totalredkpi'] = 0;
        $compliance['management']['totalgreenkpi'] = 0;
        $compliance['management']['totalamberkpi'] = 0;

        $compliance['control']['kpi_control_compliance'] = array();
        $compliance['management']['kpi_management_compliance'] = array();

        $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
        $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

        $compliance['overallcontrolcompliance'] = 0;
        $compliance['overallmanagementcompliance'] = 0;

        $compliance['flowassuranceoverallcontrolcompliance'] = 0;
        $compliance['flowassuranceoverallmanagementcompliance'] = 0;

        $compliance['overdue'] = 0;
        $compliance['corrOverdue'] = 0;
        $compliance['faOverdue'] = 0;

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');
        $cmsHelper = $this->get('aie_cms.cms.helper');
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

        $entityactivities = $entity->getActivity();
        $from_date = \DateTime::createFromFormat('Ymd', $fromdate);
        $to_date = \DateTime::createFromFormat('Ymd', $todate);
        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Sub Projects entity.');
        }

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($id);
        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

        if ($subproject && $systems)
        {

            $compliance = array();

            $compliance['control']['redcontrolkpi'] = array();
            $compliance['management']['redmanagementkpi'] = array();

            $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
            $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

            $compliance['control']['totalredkpi'] = 0;
            $compliance['control']['totalgreenkpi'] = 0;
            $compliance['control']['totalamberkpi'] = 0;

            $compliance['flowassurancecontrol']['totalredkpi'] = 0;
            $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
            $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

            $compliance['flowassurancemanagement']['totalredkpi'] = 0;
            $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
            $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

            $compliance['management']['totalredkpi'] = 0;
            $compliance['management']['totalgreenkpi'] = 0;
            $compliance['management']['totalamberkpi'] = 0;

            $compliance['control']['kpi_control_compliance'] = array();
            $compliance['management']['kpi_management_compliance'] = array();

            $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
            $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

            $compliance['overallcontrolcompliance'] = 0;
            $compliance['overallmanagementcompliance'] = 0;

            $compliance['flowassuranceoverallcontrolcompliance'] = 0;
            $compliance['flowassuranceoverallmanagementcompliance'] = 0;

            $compliance['overdue'] = 0;
            $compliance['corrOverdue'] = 0;
            $compliance['faOverdue'] = 0;

            $indeX = 0;

            $compliance['totalprojectleveloverallcontrolcompliance'] = array();
            $compliance['totalprojectleveloverallmanagementcompliance'] = array();
            $compliance['totalprojectlevelcontrolcompliance'] = array();
            $compliance['totalprojectlevelmanagementcompliance'] = array();

            foreach ($systems as $system)
            {

                $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($id, $system->getId(), false);

                if ($activitiesAction[$system->getId()])
                {

                    foreach ($activitiesAction[$system->getId()] as $subProjectActivity)
                    {

                        if ($subProjectActivity->getId())
                        {

                            //control data
                            if ($subProjectActivity->getIsControl())
                            {

                                $compliance['control']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                if ($subProjectActivity->getKpiControl() == "R") {
                                    $compliance['control']['totalredkpi']++;
                                    $compliance['control']['redcontrolkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['control']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['control']['totalamberkpi']++;

                            }

                            //management data
                            if ($subProjectActivity->getIsManagement())
                            {

                                $compliance['management']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                if ($subProjectActivity->getKpiManagement() == "R") {
                                    $compliance['management']['totalredkpi']++;
                                    $compliance['management']['redmanagementkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiManagement() == "G")
                                    $compliance['management']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiManagement() == "A")
                                    $compliance['management']['totalamberkpi']++;

                            }

                            if ($subProjectActivity->getIsflowassuranceControl())
                            {

                                $compliance['flowassurancecontrol']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                if ($subProjectActivity->getKpiControl() == "R") {
                                    $compliance['flowassurancecontrol']['totalredkpi']++;
                                    $compliance['flowassurancecontrol']['redcontrolkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['flowassurancecontrol']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['flowassurancecontrol']['totalamberkpi']++;

                            }

                            if ($subProjectActivity->getIsflowassuranceManagement())
                            {

                                $compliance['flowassurancemanagement']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                if ($subProjectActivity->getKpiControl() == "R") {
                                    $compliance['flowassurancemanagement']['totalredkpi']++;
                                    $compliance['flowassurancemanagement']['redmanagementkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['flowassurancemanagement']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['flowassurancemanagement']['totalamberkpi']++;

                            }

                            //setting inspection
                            if ($subProjectActivity->getIsInspectionDue()) {
                                $compliance['overdue'] += 1;
                            }

                            $compliance['overallcontrolcompliance'] = 0;
                            $compliance['overallmanagementcompliance'] = 0;

                            $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                            $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                            $compliance['control']['kpi_control_compliance'] = array_filter($compliance['control']['kpi_control_compliance'], function ($value) {
                                return !is_null($value);
                            });
                            $compliance['management']['kpi_management_compliance'] = array_filter($compliance['management']['kpi_management_compliance'], function ($value) {
                                return !is_null($value);
                            });

                            $compliance['flowassurancecontrol']['kpi_control_compliance'] = array_filter($compliance['flowassurancecontrol']['kpi_control_compliance'], function ($value) {
                                return !is_null($value);
                            });
                            $compliance['flowassurancemanagement']['kpi_management_compliance'] = array_filter($compliance['flowassurancemanagement']['kpi_management_compliance'], function ($value) {
                                return !is_null($value);
                            });

                            if (!empty($compliance['control']['kpi_control_compliance'])) $compliance['overallcontrolcompliance'] = (array_sum($compliance['control']['kpi_control_compliance']) / count($compliance['control']['kpi_control_compliance'])) * 100;
                            if (!empty($compliance['management']['kpi_management_compliance'])) $compliance['overallmanagementcompliance'] = (array_sum($compliance['management']['kpi_management_compliance']) / count($compliance['management']['kpi_management_compliance'])) * 100;

                            if (!empty($compliance['flowassurancecontrol']['kpi_control_compliance'])) $compliance['flowassuranceoverallcontrolcompliance'] = (array_sum($compliance['flowassurancecontrol']['kpi_control_compliance']) / count($compliance['flowassurancecontrol']['kpi_control_compliance'])) * 100;
                            if (!empty($compliance['flowassurancemanagement']['kpi_management_compliance'])) $compliance['flowassuranceoverallmanagementcompliance'] = (array_sum($compliance['flowassurancemanagement']['kpi_management_compliance']) / count($compliance['flowassurancemanagement']['kpi_management_compliance'])) * 100;

                            $tempControl = ((float)$compliance['overallcontrolcompliance']) * ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);
                            $compliance['subprojectcurrentoverallcontrolcompliance'] = $tempControl;
                            $compliance['subprojectcurrentcontrolkpi'] = ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);

                            $tempManagement = ((float)$compliance['overallmanagementcompliance']) * ($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);
                            $compliance['subprojectcurrentoverallmanagementcompliance'] = $tempManagement;
                            $compliance['subprojectcurrentmanagementkpi'] = ($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);

                        }
                    }
                }
            }
        }

        //$entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject'=>$id, 'reporttype' => array(1,2,3,5,6,7)), array('date' => 'DESC'));

        $entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->getMaxGroupedReports($id);

        $managementGraphData = "[";
        $controlGraphData = "[";

        $redGraphData = "[";
        $greenGraphData = "[";

        $newdate = [];

        $lastApprovedDate = "";

        if (!empty($entityreports))
        {
            foreach ($entityreports as $index => $entities)
            {
                $redkpiCompliance = \GuzzleHttp\json_decode($entities->getComplianceData());

                if ($entities->getIsApproved())
                {
                    $rto_date = new \DateTime($redkpiCompliance->to_date->date);
                    if ($tab == 1)
                    {
                        $newdate[$index]['dategraph'] = date('Y-m-d', strtotime($rto_date->format('Y-m-d')));
                        $newdate[$index]['control'] = $redkpiCompliance->overallcontrolcompliance;
                        $newdate[$index]['management'] = $redkpiCompliance->overallmanagementcompliance;
                    }
                    if ($tab == 2)
                    {
                        if (isset($redkpiCompliance->flowassuranceoverallcontrolcompliance) && isset($redkpiCompliance->flowassuranceoverallmanagementcompliance)) {
                            $newdate[$index]['dategraph'] = date('Y-m-d', strtotime($rto_date->format('Y-m-d')));
                            $newdate[$index]['control'] = $redkpiCompliance->flowassuranceoverallcontrolcompliance;
                            $newdate[$index]['management'] = $redkpiCompliance->flowassuranceoverallmanagementcompliance;
                        }
                    }

                }

            }

            if ($newdate && is_array($newdate) && count($newdate))
            {

                usort($newdate, function ($a, $b) {
                    $t1 = strtotime($a['dategraph']);
                    $t2 = strtotime($b['dategraph']);
                    return $t1 - $t2;
                });

                foreach ($newdate as $nwdt)
                {
                    if (date('t', strtotime($nwdt['dategraph'])) != 31)
                        $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($nwdt['dategraph'])));
                    else
                        $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($nwdt['dategraph'])));

                    $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($nwdt['management']) . "],";
                    $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($nwdt['control']) . "],";

                    $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
                    $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";

                    $lastApprovedDate = date('Y-m-d', strtotime($nwdt['dategraph']));

                }
            }

        }

        if ($tab == 1)
        {

            $replacedt = date('Y,m,d', strtotime('-31 days'));

            $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['overallmanagementcompliance']) . "],";
            $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['overallcontrolcompliance']) . "],";

            $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
            $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";

        }
        if ($tab == 2)
        {
            $replacedt = date('Y,m,d', strtotime('-31 days'));

            $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['flowassuranceoverallmanagementcompliance']) . "],";
            $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['flowassuranceoverallcontrolcompliance']) . "],";

            $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
            $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";
        }

        if ($tab == 1 && ($compliance['overallcontrolcompliance'] || $compliance['overallmanagementcompliance']))
        {
            $ctrlPrediction = round($compliance['overallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['overallmanagementcompliance'], 2);
        }
        if ($tab == 2 && ($compliance['flowassuranceoverallcontrolcompliance'] || $compliance['flowassuranceoverallmanagementcompliance']))
        {
            $ctrlPrediction = round($compliance['flowassuranceoverallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['flowassuranceoverallmanagementcompliance'], 2);
        }

        if ($ctrlPrediction || $mgmtPrediction)
        {
            if ($lastApprovedDate)
                $chartHasPrediction = $lastApprovedDate;
            else
                $chartHasPrediction = date('Y-m-d');
        }

        $managementGraphData.= "]";
        $controlGraphData.= "]";

        $redGraphData.= "]";
        $greenGraphData.= "]";

        $entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject'=>$id, 'reporttype' => array(1,2,3,5,6,7), 'isApproved'=>1),array('date' => 'DESC'),1);

        $overdues = [];

        $complianceKPIData = [];
        $complianceKPIData['control']['red']=0;
        $complianceKPIData['management']['red']=0;
        $complianceKPIData['control']['amber']=0;
        $complianceKPIData['management']['amber']=0;
        $complianceKPIData['control']['green']=0;
        $complianceKPIData['management']['green']=0;
        $complianceKPIData['control']['hold']=0;
        $complianceKPIData['management']['hold']=0;

        if (!empty($entityactivities))
        {
            $corrOverdueCount = 0;
            $faOverdueCount = 0;
            $overdueActivities = array();

            foreach ($entityactivities as $activities)
            {

                $checkActivity = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                if ( $tab == 1 && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1 && !$checkActivity['isnonmonitoringkpi'] && ($activities->getIsManagement() == 1 || $activities->getIsControl() == 1))
                {

                    if ($activities->getIsManagement() == 1 && $activities->getIsControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsManagement() == 1 && $activities->getIsControl() == 0)
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsnonmonitoringkpi() == 1 && $activities->getIsControl() == 0 && $activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 0  )
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }

                    if (!empty($activities->getKpiManagement())) {
                        $managementActivitiesRed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'R');
                        $managementActivitiesAmber = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'A');
                    }

                    if (!empty($activities->getKpiManagement())) {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());
                        if(!isset($systemCompliance[$tempsys['id']])){
                            $systemCompliance[$tempsys['id']] = ['name' => $tempsys['title'], 'mgmt'=>[], 'ctrl'=>[],'justification'=>''];
                        }
                        array_push($systemCompliance[$tempsys['id']]['mgmt'], $activities->getKpiManagement());
                    }
                    if (!empty($activities->getKpiControl())) {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());
                        if(!isset($systemCompliance[$tempsys['id']])){
                            $systemCompliance[$tempsys['id']] = ['name' => $tempsys['title'], 'mgmt'=>[], 'ctrl'=>[],'justification'=>''];
                        }
                        array_push($systemCompliance[$tempsys['id']]['ctrl'], $activities->getKpiControl());
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "R")
                    {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation) {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'red';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'red';
                        }
                        $complianceKPIData['control']['red']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "R")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['red']++;
                    }
                    else if(!empty($managementActivitiesRed))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['red']++;
                    }


                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "A")
                    {
                        //$redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation) {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'amber';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'amber';
                        }

                        $complianceKPIData['control']['amber']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "A")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['amber']++;
                    }
                    else if(!empty($managementActivitiesAmber))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['amber']++;
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "G")
                    {
                        $complianceKPIData['control']['green']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "G")
                    {
                        $complianceKPIData['management']['green']++;
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "H")
                    {
                        $complianceKPIData['control']['hold']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "H")
                    {
                        $complianceKPIData['management']['hold']++;
                    }

                    if ($activities->getNextInspectionDate())
                    {
                        $nextDate = $activities->getNextInspectionDate(true);
                        $nextDate_U = (int)$nextDate->format('U');

                        if ( (int)$to_date->format('U') &&  $nextDate_U <= (int)$to_date->format('U')  )
                        {
                            //set repeated action last time to next date

                            $overdues[] = $activities->getId();
                            $overdueActivities[$activities->getId()]['id'] =  $activities->getId();
                            $overdueActivities[$activities->getId()]['title'] = $activitiesTitle;
                            $overdueActivities[$activities->getId()]['duedate'] = $nextDate->format('d F Y');
                            $overdueActivities[$activities->getId()]['frequency'] = $activities->getFrequency();
                            $overdueActivities[$activities->getId()]['actionowner'] = (!empty($activities->getActionOwner()) ? $activities->getActionOwner()->getName() : "");
                            $overdues++;

                        }

                        //logic to get the count of overdue activities uptil the "To date"
                        $nextDate1 = $activities->getNextInspectionDate(true);
                        $nextDate_U1 = (int)$nextDate1->format('U');

                        while ($nextDate_U1 <= (int)$to_date->format('U'))
                        {

                            $corrOverdueCount++;

                            $activities->setLastInspectionDate($nextDate1);

                            //set next date from repeated action
                            $nextDate1 = $activities->getNextInspectionDate(true);
                            $nextDate_U1 = (int)$nextDate1->format('U');

                        }

                    }

                    $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

                    foreach ($systems as $system)
                    {
                        $actid = $activities->getId();

                        $activitiesKpiControl = $activities->getKpiControl();

                        $locations = $cmsDatabasesHelper->getLocationsFromDatabasesPerSystemSubProjectActivity($system->getId(), $id, $actid);

                        $corrosiondatasheetlocationEntities = array();

                        //system data, location data, generating datasheet graphs
                        if (count($locations))
                        {

                            foreach ($locations as $location)
                            {

                                $corrosiondatasheetlocationEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByLocation($location['type'], $location['id'], $id, $activities->getId());

                                $latestEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocation($actid, $location['type'], $location['id'], $id, 1);

                                $latestAvailableEntities = array();

                                if ($location['type'] == 2)
                                {
                                    $latestAvailableEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getAverageLatestDatasheetLocation($location['id'], $id, 1);
                                }

                                if (!empty($corrosiondatasheetlocationEntities))
                                {

                                    if ($location['type'] == 2)
                                    {
                                        $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                                        $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($id, 2, $system->getId());
                                    }

                                    foreach ($corrosiondatasheetlocationEntities as $index => $datasheetLocationDetails)
                                    {

                                        if ($datasheetLocationDetails[0]['retrieval_date'])
                                        {

                                            $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                            if ($location['type'] == 3)
                                            {
                                                if (!empty($latestEntities[0][0]['average_rate_mmpy']) && $latestEntities[0][0]['average_rate_mmpy'] == $datasheetLocationDetails[0]['average_rate_mmpy'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 2)
                                            {

                                                if (!empty($datasheetLocationDetails[0]['control_activity_achievement_value']))
                                                {

                                                    if (!empty($latestEntities[0][0]['control_activity_achievement_value']) && $latestEntities[0][0]['control_activity_achievement_value'] == $datasheetLocationDetails[0]['control_activity_achievement_value'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['target_ci_dosage_litre_per_day']))
                                                {

                                                    if (!empty($latestEntities[0][0]['target_ci_dosage_litre_per_day']) && $latestEntities[0][0]['target_ci_dosage_litre_per_day'] == $datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['ci_percentage_availability']))
                                                {

                                                    if (!empty($latestEntities[0][0]['ci_percentage_availability']) && $latestEntities[0][0]['ci_percentage_availability'] == $datasheetLocationDetails[0]['ci_percentage_availability'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']);
                                                        }
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 4)
                                            {

                                                if ( !empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'] )
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {

                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 1)
                                            {

                                                if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                        }

                                        if ($datasheetLocationDetails[0]['to_date'] && $location['type'] == 2)
                                        {

                                            foreach ($chemicalActivities as $chemicalActivity)
                                            {

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 2)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']) && $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage'] == $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']))
                                                        {
                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']);
                                                        }
                                                    }
                                                }

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 3)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']) && $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage'] == $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']))
                                                        {
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
                elseif ( $tab == 2 && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1 && !$checkActivity['isnonmonitoringkpi'] && ($activities->getIsflowassuranceManagement() == 1 || $activities->getIsflowassuranceControl() == 1))
                {

                    if ($activities->getIsflowassuranceManagement() == 1 && $activities->getIsflowassuranceControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsflowassuranceManagement() == 1 && $activities->getIsflowassuranceControl() == 0)
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsnonmonitoringkpi() == 1 && $activities->getIsControl() == 0 && $activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 0  )
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }

                    if (!empty($activities->getKpiManagement())) {
                        $managementActivitiesRed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'R');
                        $managementActivitiesAmber = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'A');
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "R")
                    {
//                        $redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation)
                            {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'red';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'red';
                        }
                        $complianceKPIData['control']['red']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "R")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['red']++;
                    }
                    else if(!empty($managementActivitiesRed))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['red']++;
                    }


                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "A")
                    {
                        //$redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation)
                            {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'amber';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'amber';
                        }
                        $complianceKPIData['control']['amber']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "A")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['amber']++;
                    }
                    else if(!empty($managementActivitiesAmber))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['amber']++;
                    }


                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "G")
                    {
                        $complianceKPIData['control']['green']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "G")
                    {
                        $complianceKPIData['management']['green']++;
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "H")
                    {
                        $complianceKPIData['control']['hold']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "H")
                    {
                        $complianceKPIData['management']['hold']++;
                    }

                    if ($activities->getNextInspectionDate())
                    {
                        $nextDate = $activities->getNextInspectionDate(true);
                        $nextDate_U = (int)$nextDate->format('U');
//                        $current_date = new \DateTime("now");
//                        $current_date_U = (int)$current_date->format('U');

                        if (  $to_date->format('U')  && $nextDate_U <= (int)$to_date->format('U') )
                        {
                            //set repeated action last time to next date
//                            $activities->setLastInspectionDate($nextDate);
                            $overdues[] = $activities->getId();
                            $overdueActivities[$activities->getId()]['id'] =  $activities->getId();
                            $overdueActivities[$activities->getId()]['title'] = $activitiesTitle;
                            $overdueActivities[$activities->getId()]['duedate'] = $nextDate->format('d F Y');
                            $overdueActivities[$activities->getId()]['frequency'] = $activities->getFrequency();
                            $overdueActivities[$activities->getId()]['actionowner'] = (!empty($activities->getActionOwner()) ? $activities->getActionOwner()->getName() : "");
                            $overdues++;

                        }

                        //logic to get the count of overdue activities uptil the "To date"
                        $nextDate1 = $activities->getNextInspectionDate(true);
                        $nextDate_U1 = (int)$nextDate1->format('U');


                        while ($nextDate_U1 <= (int)$to_date->format('U')) {

//                            if ($nextDate_U1 >= (int)$from_date->format('U')) {
                            $faOverdueCount++;
//                            }

                            $activities->setLastInspectionDate($nextDate1);

                            //set next date from repeated action
                            $nextDate1 = $activities->getNextInspectionDate(true);
                            $nextDate_U1 = (int)$nextDate1->format('U');

                        }

                    }

                    $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

                    foreach ($systems as $system)
                    {
                        $actid = $activities->getId();

                        $activitiesKpiControl = $activities->getKpiControl();

                        $locations = $cmsDatabasesHelper->getLocationsFromDatabasesPerSystemSubProjectActivity($system->getId(), $id, $actid);

                        $corrosiondatasheetlocationEntities = array();

                        //system data, location data, generating datasheet graphs
                        if (count($locations))
                        {

                            foreach ($locations as $location)
                            {

                                $corrosiondatasheetlocationEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByLocation($location['type'], $location['id'], $id, $activities->getId());

                                $latestEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocation($actid, $location['type'], $location['id'], $id, 1);

                                $latestAvailableEntities = array();

                                if ($location['type'] == 2)
                                {
                                    $latestAvailableEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getAverageLatestDatasheetLocation($location['id'], $id, 1);
                                }

                                if (!empty($corrosiondatasheetlocationEntities))
                                {

                                    if ($location['type'] == 2)
                                    {
                                        $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                                        $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($id, 2, $system->getId());
                                    }

                                    foreach ($corrosiondatasheetlocationEntities as $index => $datasheetLocationDetails)
                                    {

                                        if ($datasheetLocationDetails[0]['retrieval_date'])
                                        {

                                            $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                            if ($location['type'] == 3)
                                            {
                                                if (!empty($latestEntities[0][0]['average_rate_mmpy']) && $latestEntities[0][0]['average_rate_mmpy'] == $datasheetLocationDetails[0]['average_rate_mmpy'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 2)
                                            {

                                                if (!empty($datasheetLocationDetails[0]['control_activity_achievement_value']))
                                                {

                                                    if (!empty($latestEntities[0][0]['control_activity_achievement_value']) && $latestEntities[0][0]['control_activity_achievement_value'] == $datasheetLocationDetails[0]['control_activity_achievement_value'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['target_ci_dosage_litre_per_day']))
                                                {

                                                    if (!empty($latestEntities[0][0]['target_ci_dosage_litre_per_day']) && $latestEntities[0][0]['target_ci_dosage_litre_per_day'] == $datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['ci_percentage_availability']))
                                                {

                                                    if (!empty($latestEntities[0][0]['ci_percentage_availability']) && $latestEntities[0][0]['ci_percentage_availability'] == $datasheetLocationDetails[0]['ci_percentage_availability'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']);
                                                        }
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 4)
                                            {

                                                if ( !empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'] )
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {

                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 1)
                                            {

                                                if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                        }

                                        if ($datasheetLocationDetails[0]['to_date'] && $location['type'] == 2)
                                        {

                                            foreach ($chemicalActivities as $chemicalActivity)
                                            {

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 2)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']) && $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage'] == $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']))
                                                        {
                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']);
                                                        }
                                                    }
                                                }

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 3)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']) && $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage'] == $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']))
                                                        {
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }

            }
        }

        $subRedRealControls = array();

        if (!empty($redmanagementCompliance))
        {

            foreach ($redmanagementCompliance as $index => $activities)
            {

                $subRedManagement[$activities]['activity_locations']['id'] = $activities;
                $subRedManagement[$activities]['activity_locations']['locations'] = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities);
                $subRedManagement[$activities]['activity_locations']['systems'] = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities);

                $managementRedActivities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities,'R');
                $managementAmberActivities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities,'A');

                if (!empty($activities))
                {

                    foreach($managementRedActivities as $innerindex => $managementRedActivity)
                    {
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['management_performed'] = 'R';
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['id'] = $managementRedActivity['dbactivitylocationid'];
                    }

                    foreach($managementAmberActivities as $innerindex => $managementAberActivity)
                    {

                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['management_performed'] = 'A';
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['id'] = $managementRedActivity['dbactivitylocationid'];

                    }

                }

            }
        }

        $level1Granted = 0;
        $level2Granted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_SUB-PROJECT_SPECIFIC_DASHBOARD_SHOW',$this->userRolesMergeToCheck)){
            $level1Granted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_SUB-PROJECT_SPECIFIC_DASHBOARD_SHOW',$this->userRolesMergeToCheck)){
            $level2Granted = 1;
        }
        $subRedRealControls = array();

        $corrosionPrediction = ['ctrl'=>$ctrlPrediction,'mgmt'=>$mgmtPrediction];

        $finalSystemCompliance = [];
        foreach ($systemCompliance as $key=>$pair){
            if (count($pair['ctrl']) > 0 || count($pair['mgmt']) > 0){
                $finalSystemCompliance[$key] = [
                    'name'=>$pair['name'],
                    'mgmt'=>'N/A',
                    'ctrl'=>'N/A',
                    'justification'=>$pair['justification'],
                ];
                $counter =0;
                $mgmtCount = 0;
                foreach($pair['mgmt'] as $item){
                    if ($item == "R"){
                        $counter += 1;
                        $mgmtCount += 0;
                    }
                    if ($item == "A"){
                        $counter += 1;
                        $mgmtCount += 50;
                    }
                    if ($item == "G"){
                        $counter += 1;
                        $mgmtCount += 100;
                    }
                }
                if ($counter > 0){
                    $finalSystemCompliance[$key]['mgmt'] = $mgmtCount / $counter;
                }else{
                    $finalSystemCompliance[$key]['mgmt'] = "N/A";
                }
                $counter =0;
                $ctrlCount = 0;
                foreach($pair['ctrl'] as $item){
                    if ($item == "R"){
                        $counter += 1;
                        $mgmtCount += 0;
                    }
                    if ($item == "A"){
                        $counter += 1;
                        $mgmtCount += 50;
                    }
                    if ($item == "G"){
                        $counter += 1;
                        $mgmtCount += 100;
                    }
                }
                if ($counter > 0){
                    $finalSystemCompliance[$key]['ctrl'] = $ctrlCount / $counter;
                }else{
                    $finalSystemCompliance[$key]['ctrl'] = "N/A";
                }
            }

        }

        //echo "<pre>";print_r($systemCompliance);echo "</pre>";

        return [
            'projectId' => $projectId,
            'subprojectId' => $id,
            'entity' => $entity,
            'entityreports' => $entityreports,
            'chart_has_prediction'=>$chartHasPrediction,
            'corrosion_prediction'=>$corrosionPrediction,
            'redmanagementkpis' => $subRedManagement,
            'redcontrolkpis' => $subRedControl,
            'redrealcontrolkpis' => $subRedRealControls,
            'overdues' => count(array_unique($overdueActivities,SORT_REGULAR)),
            'managementComplianceData' => $managementGraphData,
            'controlComplianceData' => $controlGraphData,
            'redComplianceData' => $redGraphData,
            'greenComplianceData' => $greenGraphData,
            'complianceKPIData' => $complianceKPIData,
            'overdueActivities' =>array_unique($overdueActivities,SORT_REGULAR),
            'level1Granted' => $level1Granted,
            'level2Granted' => $level2Granted,
            'enabled_systems'=>$enabledSystems,
            'category'=>$tab,
            'systems_compliance'=>$finalSystemCompliance,
            'RedKPI_ODCounts'=>$RedKPI_ODCounts,
            'corrOverdueCount'=> $corrOverdueCount,
            'faOverdueCount'=>$faOverdueCount,
            'toDate'=>$to_date,
        ];
    }
    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/dashboard", name="cms_subprojects_dashboard")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function dashboardAction(Request $request, $id, $projectId)
    {

//        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
//            throw $this->createAccessDeniedException('You do not have permission to view the project.');
//        }

        ini_set('memory_limit', '-1');

        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }

        $enabledSystems = ['CM'=>true,'FA'=>true];
        $em = $this->getManager();
        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);
        $to_date = new \DateTime('now');
        $from_date = clone $to_date;
        $from_date->modify('-1 month');
        $from_date->modify('-1 day');
        $filter_form = $this->createActivityFilterForm($projectId, $id, $from_date, $to_date, 'cms_subprojects_dashboard');
        $filter_form->handleRequest($request);
        if ($filter_form->isValid())
        {
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];
            $from_date = new \DateTime($filter_data['from_date']);
            $to_date = new \DateTime($filter_data['to_date']);

        }

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Sub Projects entity.');
        }

        return [
            'projectId' => $projectId,
            'subprojectId' => $id,
            'entity' => $entity,
            'filter_form'            => $filter_form->createView(),
            'fromDate' => $from_date->format('Y-m-d'),
            'toDate' => $to_date->format('Y-m-d'),
            'enabled_systems'=>$enabledSystems
        ];
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/{type}/{fromDate}/{toDate}/dashboardreport", name="cms_subprojects_dashboard_report")
     * @Method({"GET","POST"})
     * @Template()
     */
    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/{type}/{fromDate}/{toDate}/dashboardreport", name="cms_subprojects_dashboard_report")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function dashboardreportAction(Request $request, $id, $projectId, $fromDate, $toDate, $type)
    {

        ini_set('memory_limit', '-1');

        $corrOD = $this->get('session')->get('CorrOD_'.$projectId.'_'.$id);
        $faOD = $this->get('session')->get('FAOD_'.$projectId.'_'.$id);

        //project level counts

        $ProjCorrOD = $this->get('session')->get('ProjCorrOD_'.$projectId);
        $CorrRedControlKpi = $this->get('session')->get('CorrRedControlKpi_'.$projectId);
        $CorrRedMngmntKpi = $this->get('session')->get('CorrRedMngmntKpi_'.$projectId);

        $ProjFAOD = $this->get('session')->get('ProjFAOD_'.$projectId);
        $FARedControlKpi = $this->get('session')->get('FARedControlKpi_'.$projectId);
        $FARedMngmntKpi = $this->get('session')->get('FARedMngmntKpi_'.$projectId);

        //end of project level counts
        $RedKPI_ODCounts = array('CorrOD'=>$ProjCorrOD,'CorrCntrl'=>$CorrRedControlKpi,'CorrMngmt'=>$CorrRedMngmntKpi,'FAOD'=>$ProjFAOD,'FACntrl'=>$FARedControlKpi,'FAMgmt'=>$FARedMngmntKpi,'SP_CorrOD'=>$corrOD,'SP_FAOD'=>$faOD);

        $corrOverdueCount = 0;
        $faOverdueCount = 0;

        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }
        $tab = $type;
        $chartHasPrediction = null;
        $ctrlPrediction = null;
        $mgmtPrediction = null;
        $redcontrolCompliance = array();
        $redmanagementCompliance = array();
        $subRedControl = array();
        $subRedManagement = array();
        $overdueActivities = array();
        $systemCompliance = [];
        $enabledSystems = ['CM'=>true,'FA'=>true];
        $compliance = array();

        $compliance['control']['redcontrolkpi'] = array();
        $compliance['management']['redmanagementkpi'] = array();

        $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
        $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

        $compliance['control']['totalredkpi'] = 0;
        $compliance['control']['totalgreenkpi'] = 0;
        $compliance['control']['totalamberkpi'] = 0;

        $compliance['flowassurancecontrol']['totalredkpi'] = 0;
        $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
        $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

        $compliance['flowassurancemanagement']['totalredkpi'] = 0;
        $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
        $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

        $compliance['management']['totalredkpi'] = 0;
        $compliance['management']['totalgreenkpi'] = 0;
        $compliance['management']['totalamberkpi'] = 0;

        $compliance['control']['kpi_control_compliance'] = array();
        $compliance['management']['kpi_management_compliance'] = array();

        $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
        $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

        $compliance['overallcontrolcompliance'] = 0;
        $compliance['overallmanagementcompliance'] = 0;

        $compliance['flowassuranceoverallcontrolcompliance'] = 0;
        $compliance['flowassuranceoverallmanagementcompliance'] = 0;

        $compliance['overdue'] = 0;
        $compliance['corrOverdue'] = 0;
        $compliance['faOverdue'] = 0;

        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');
        $cmsHelper = $this->get('aie_cms.cms.helper');
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

        $entityactivities = $entity->getActivity();
        $from_date = new \DateTime($fromDate);
        $to_date = new \DateTime($toDate);

        if (!$entity)
        {
            throw $this->createNotFoundException('Unable to find Sub Projects entity.');
        }

        $entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject'=>$id),array('date' => 'DESC'));
        $managementGraphData = "[";
        $controlGraphData = "[";

        $redGraphData = "[";
        $greenGraphData = "[";

        $chartHasPrediction = null;
        $ctrlPrediction = null;
        $mgmtPrediction = null;
        $newdate = [];
        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($id);
        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

        if ($subproject && $systems)
        {

            $compliance = array();

            $compliance['control']['redcontrolkpi'] = array();
            $compliance['management']['redmanagementkpi'] = array();

            $compliance['flowassurancecontrol']['redcontrolkpi'] = array();
            $compliance['flowassurancemanagement']['redmanagementkpi'] = array();

            $compliance['control']['totalredkpi'] = 0;
            $compliance['control']['totalgreenkpi'] = 0;
            $compliance['control']['totalamberkpi'] = 0;

            $compliance['flowassurancecontrol']['totalredkpi'] = 0;
            $compliance['flowassurancecontrol']['totalgreenkpi'] = 0;
            $compliance['flowassurancecontrol']['totalamberkpi'] = 0;

            $compliance['flowassurancemanagement']['totalredkpi'] = 0;
            $compliance['flowassurancemanagement']['totalgreenkpi'] = 0;
            $compliance['flowassurancemanagement']['totalamberkpi'] = 0;

            $compliance['management']['totalredkpi'] = 0;
            $compliance['management']['totalgreenkpi'] = 0;
            $compliance['management']['totalamberkpi'] = 0;

            $compliance['control']['kpi_control_compliance'] = array();
            $compliance['management']['kpi_management_compliance'] = array();

            $compliance['flowassurancecontrol']['kpi_control_compliance'] = array();
            $compliance['flowassurancemanagement']['kpi_management_compliance'] = array();

            $compliance['overallcontrolcompliance'] = 0;
            $compliance['overallmanagementcompliance'] = 0;

            $compliance['flowassuranceoverallcontrolcompliance'] = 0;
            $compliance['flowassuranceoverallmanagementcompliance'] = 0;

            $compliance['overdue'] = 0;
            $compliance['corrOverdue'] = 0;
            $compliance['faOverdue'] = 0;

            $indeX = 0;

            $compliance['totalprojectleveloverallcontrolcompliance'] = array();
            $compliance['totalprojectleveloverallmanagementcompliance'] = array();
            $compliance['totalprojectlevelcontrolcompliance'] = array();
            $compliance['totalprojectlevelmanagementcompliance'] = array();

            foreach ($systems as $system)
            {
                $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($id, $system->getId(), false);

                if ($activitiesAction[$system->getId()])
                {

                    foreach ($activitiesAction[$system->getId()] as $subProjectActivity)
                    {
                        if ($subProjectActivity->getId())
                        {

                            //control data
                            if ($subProjectActivity->getIsControl())
                            {

                                $compliance['control']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                if ($subProjectActivity->getKpiControl() == "R")
                                {
                                    $compliance['control']['totalredkpi']++;
                                    $compliance['control']['redcontrolkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['control']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['control']['totalamberkpi']++;

                            }

                            //management data
                            if ($subProjectActivity->getIsManagement())
                            {

                                $compliance['management']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                if ($subProjectActivity->getKpiManagement() == "R") {
                                    $compliance['management']['totalredkpi']++;
                                    $compliance['management']['redmanagementkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiManagement() == "G")
                                    $compliance['management']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiManagement() == "A")
                                    $compliance['management']['totalamberkpi']++;

                            }

                            if ($subProjectActivity->getIsflowassuranceManagement())
                            {

                                $compliance['flowassurancemanagement']['kpi_management_compliance'][] = $subProjectActivity->getCompliance(2);

                                if ($subProjectActivity->getKpiControl() == "R") {
                                    $compliance['flowassurancemanagement']['totalredkpi']++;
                                    $compliance['flowassurancemanagement']['redmanagementkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['flowassurancemanagement']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['flowassurancemanagement']['totalamberkpi']++;

                            }

                            if ($subProjectActivity->getIsflowassuranceControl())
                            {

                                $compliance['flowassurancecontrol']['kpi_control_compliance'][] = $subProjectActivity->getCompliance(1);

                                if ($subProjectActivity->getKpiControl() == "R") {
                                    $compliance['flowassurancecontrol']['totalredkpi']++;
                                    $compliance['flowassurancecontrol']['redcontrolkpi'][] = $subProjectActivity->getId();
                                }
                                if ($subProjectActivity->getKpiControl() == "G")
                                    $compliance['flowassurancecontrol']['totalgreenkpi']++;
                                if ($subProjectActivity->getKpiControl() == "A")
                                    $compliance['flowassurancecontrol']['totalamberkpi']++;

                            }

                            //setting inspection
                            if ($subProjectActivity->getIsInspectionDue())
                            {
                                $compliance['overdue'] += 1;
                            }

                            $compliance['overallcontrolcompliance'] = 0;
                            $compliance['overallmanagementcompliance'] = 0;

                            $compliance['flowassuranceoverallcontrolcompliance'] = 0;
                            $compliance['flowassuranceoverallmanagementcompliance'] = 0;

                            $compliance['control']['kpi_control_compliance'] = array_filter($compliance['control']['kpi_control_compliance'], function ($value) {
                                return !is_null($value);
                            });
                            $compliance['management']['kpi_management_compliance'] = array_filter($compliance['management']['kpi_management_compliance'], function ($value) {
                                return !is_null($value);
                            });

                            $compliance['flowassurancecontrol']['kpi_control_compliance'] = array_filter($compliance['flowassurancecontrol']['kpi_control_compliance'], function ($value) {
                                return !is_null($value);
                            });
                            $compliance['flowassurancemanagement']['kpi_management_compliance'] = array_filter($compliance['flowassurancemanagement']['kpi_management_compliance'], function ($value) {
                                return !is_null($value);
                            });

                            if (!empty($compliance['control']['kpi_control_compliance'])) $compliance['overallcontrolcompliance'] = (array_sum($compliance['control']['kpi_control_compliance']) / count($compliance['control']['kpi_control_compliance'])) * 100;
                            if (!empty($compliance['management']['kpi_management_compliance'])) $compliance['overallmanagementcompliance'] = (array_sum($compliance['management']['kpi_management_compliance']) / count($compliance['management']['kpi_management_compliance'])) * 100;

                            if (!empty($compliance['flowassurancecontrol']['kpi_control_compliance'])) $compliance['flowassuranceoverallcontrolcompliance'] = (array_sum($compliance['flowassurancecontrol']['kpi_control_compliance']) / count($compliance['flowassurancecontrol']['kpi_control_compliance'])) * 100;
                            if (!empty($compliance['flowassurancemanagement']['kpi_management_compliance'])) $compliance['flowassuranceoverallmanagementcompliance'] = (array_sum($compliance['flowassurancemanagement']['kpi_management_compliance']) / count($compliance['flowassurancemanagement']['kpi_management_compliance'])) * 100;

                            $tempControl = ((float)$compliance['overallcontrolcompliance']) * ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);
                            $compliance['subprojectcurrentoverallcontrolcompliance'] = $tempControl;
                            $compliance['subprojectcurrentcontrolkpi'] = ($compliance['control']['totalredkpi'] + $compliance['control']['totalgreenkpi'] + $compliance['control']['totalamberkpi']);

                            $tempManagement = ((float)$compliance['overallmanagementcompliance']) * ($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);
                            $compliance['subprojectcurrentoverallmanagementcompliance'] = $tempManagement;
                            $compliance['subprojectcurrentmanagementkpi'] = ($compliance['management']['totalredkpi'] + $compliance['management']['totalgreenkpi'] + $compliance['management']['totalamberkpi']);

                        }
                    }
                }
            }
        }

        //$entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject'=>$id, 'reporttype' => array(1,2,3,5,6,7)), array('date' => 'DESC'));

        $entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->getMaxGroupedReports($id);

        $managementGraphData = "[";
        $controlGraphData = "[";

        $redGraphData = "[";
        $greenGraphData = "[";

        $newdate = [];

        $lastApprovedDate = "";

        if (!empty($entityreports))
        {
            foreach ($entityreports as $index => $entities)
            {
                $redkpiCompliance = \GuzzleHttp\json_decode($entities->getComplianceData());

                if ($entities->getIsApproved())
                {
                    $rto_date = new \DateTime($redkpiCompliance->to_date->date);
                    if ($tab == 1)
                    {
                        $newdate[$index]['dategraph'] = date('Y-m-d', strtotime($rto_date->format('Y-m-d')));
                        $newdate[$index]['control'] = $redkpiCompliance->overallcontrolcompliance;
                        $newdate[$index]['management'] = $redkpiCompliance->overallmanagementcompliance;
                    }
                    if ($tab == 2)
                    {
                        if (isset($redkpiCompliance->flowassuranceoverallcontrolcompliance) && isset($redkpiCompliance->flowassuranceoverallmanagementcompliance)) {
                            $newdate[$index]['dategraph'] = date('Y-m-d', strtotime($rto_date->format('Y-m-d')));
                            $newdate[$index]['control'] = $redkpiCompliance->flowassuranceoverallcontrolcompliance;
                            $newdate[$index]['management'] = $redkpiCompliance->flowassuranceoverallmanagementcompliance;
                        }
                    }

                }

            }

            if ($newdate && is_array($newdate) && count($newdate))
            {

                usort($newdate, function ($a, $b) {
                    $t1 = strtotime($a['dategraph']);
                    $t2 = strtotime($b['dategraph']);
                    return $t1 - $t2;
                });

                foreach ($newdate as $nwdt)
                {
                    if (date('t', strtotime($nwdt['dategraph'])) != 31)
                        $replacedt = date('Y,m,d', strtotime('-1 months', strtotime($nwdt['dategraph'])));
                    else
                        $replacedt = date('Y,m,d', strtotime('-31 days', strtotime($nwdt['dategraph'])));

                    $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($nwdt['management']) . "],";
                    $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($nwdt['control']) . "],";

                    $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
                    $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";

                    $lastApprovedDate = date('Y-m-d', strtotime($nwdt['dategraph']));

                }
            }

        }

        if ($tab == 1)
        {
            $replacedt = date('Y,m,d', strtotime('-31 days'));

            $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['overallmanagementcompliance']) . "],";
            $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['overallcontrolcompliance']) . "],";

            $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
            $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";

        }
        if ($tab == 2)
        {
            $replacedt = date('Y,m,d', strtotime('-31 days'));

            $managementGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['flowassuranceoverallmanagementcompliance']) . "],";
            $controlGraphData .= '[Date.UTC(' . $replacedt . '),' . round($compliance['flowassuranceoverallcontrolcompliance']) . "],";

            $redGraphData .= '[Date.UTC(' . $replacedt . '),' . 50 . "],";
            $greenGraphData .= '[Date.UTC(' . $replacedt . '),' . 75 . "],";
        }

        if ($tab == 1 && ($compliance['overallcontrolcompliance'] || $compliance['overallmanagementcompliance']))
        {
            $ctrlPrediction = round($compliance['overallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['overallmanagementcompliance'], 2);
        }
        if ($tab == 2 && ($compliance['flowassuranceoverallcontrolcompliance'] || $compliance['flowassuranceoverallmanagementcompliance']))
        {
            $ctrlPrediction = round($compliance['flowassuranceoverallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['flowassuranceoverallmanagementcompliance'], 2);
        }

        if ($ctrlPrediction || $mgmtPrediction)
        {
            if ($lastApprovedDate)
                $chartHasPrediction = $lastApprovedDate;
            else
                $chartHasPrediction = date('Y-m-d');
        }

        $managementGraphData.= "]";
        $controlGraphData.= "]";

        $redGraphData.= "]";
        $greenGraphData.= "]";

        $entityreports = $em->getRepository('AIECmsBundle:SubProjectMatricesReports')->findBy(array('subproject'=>$id, 'reporttype' => array(1,2,3,5,6,7), 'isApproved'=>1),array('date' => 'DESC'),1);

        $overdues = [];

        $complianceKPIData = [];
        $complianceKPIData['control']['red']=0;
        $complianceKPIData['management']['red']=0;
        $complianceKPIData['control']['amber']=0;
        $complianceKPIData['management']['amber']=0;
        $complianceKPIData['control']['green']=0;
        $complianceKPIData['management']['green']=0;
        $complianceKPIData['control']['hold']=0;
        $complianceKPIData['management']['hold']=0;

        if (!empty($entityactivities))
        {
            $corrOverdueCount = 0;
            $faOverdueCount = 0;
            $overdueActivities = array();

            foreach ($entityactivities as $activities)
            {

                $checkActivity = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                if ( $tab == 1 && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1 && !$checkActivity['isnonmonitoringkpi'] && ($activities->getIsManagement() == 1 || $activities->getIsControl() == 1))
                {

                    if ($activities->getIsManagement() == 1 && $activities->getIsControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsManagement() == 1 && $activities->getIsControl() == 0)
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsnonmonitoringkpi() == 1 && $activities->getIsControl() == 0 && $activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 0  )
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }

                    if (!empty($activities->getKpiManagement())) {
                        $managementActivitiesRed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'R');
                        $managementActivitiesAmber = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'A');
                    }

                    if (!empty($activities->getKpiManagement())) {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());
                        if(!isset($systemCompliance[$tempsys['id']])){
                            $systemCompliance[$tempsys['id']] = ['name' => $tempsys['title'], 'mgmt'=>[], 'ctrl'=>[],'justification'=>''];
                        }
                        array_push($systemCompliance[$tempsys['id']]['mgmt'], $activities->getKpiManagement());
                    }
                    if (!empty($activities->getKpiControl())) {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());
                        if(!isset($systemCompliance[$tempsys['id']])){
                            $systemCompliance[$tempsys['id']] = ['name' => $tempsys['title'], 'mgmt'=>[], 'ctrl'=>[],'justification'=>''];
                        }
                        array_push($systemCompliance[$tempsys['id']]['ctrl'], $activities->getKpiControl());
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "R")
                    {

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation) {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'red';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'red';
                        }
                        $complianceKPIData['control']['red']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "R")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['red']++;
                    }
                    else if(!empty($managementActivitiesRed))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "A")
                    {
                        //$redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation) {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'amber';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'amber';
                        }
                        $complianceKPIData['control']['amber']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "A")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['amber']++;
                    }
                    else if(!empty($managementActivitiesAmber))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "G")
                    {
                        $complianceKPIData['control']['green']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "G")
                    {
                        $complianceKPIData['management']['green']++;
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "H")
                    {
                        $complianceKPIData['control']['hold']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "H")
                    {
                        $complianceKPIData['management']['hold']++;
                    }

                    if ($activities->getNextInspectionDate())
                    {
                        $nextDate = $activities->getNextInspectionDate(true);
                        $nextDate_U = (int)$nextDate->format('U');

                        if ( (int)$to_date->format('U') &&  $nextDate_U <= (int)$to_date->format('U')  )
                        {
                            //set repeated action last time to next date

                            $overdues[] = $activities->getId();
                            $overdueActivities[$activities->getId()]['id'] =  $activities->getId();
                            $overdueActivities[$activities->getId()]['title'] = $activitiesTitle;
                            $overdueActivities[$activities->getId()]['duedate'] = $nextDate->format('d M Y');
                            $overdueActivities[$activities->getId()]['frequency'] =  $activities->getfrequency();
                            $overdueActivities[$activities->getId()]['actionowner'] = (!empty($activities->getActionOwner()) ? $activities->getActionOwner()->getName() : "");
                            $overdues++;

                        }

                        //logic to get the count of overdue activities uptil the "To date"
                        $nextDate1 = $activities->getNextInspectionDate(true);
                        $nextDate_U1 = (int)$nextDate1->format('U');

                        while ($nextDate_U1 <= (int)$to_date->format('U'))
                        {

                            $corrOverdueCount++;

                            $activities->setLastInspectionDate($nextDate1);

                            //set next date from repeated action
                            $nextDate1 = $activities->getNextInspectionDate(true);
                            $nextDate_U1 = (int)$nextDate1->format('U');

                        }

                    }

                    $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

                    foreach ($systems as $system)
                    {
                        $actid = $activities->getId();

                        $activitiesKpiControl = $activities->getKpiControl();

                        $locations = $cmsDatabasesHelper->getLocationsFromDatabasesPerSystemSubProjectActivity($system->getId(), $id, $actid);

                        $corrosiondatasheetlocationEntities = array();

                        //system data, location data, generating datasheet graphs
                        if (count($locations))
                        {

                            foreach ($locations as $location)
                            {

                                $corrosiondatasheetlocationEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByLocation($location['type'], $location['id'], $id, $activities->getId());

                                $latestEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocation($actid, $location['type'], $location['id'], $id, 1);

                                $latestAvailableEntities = array();

                                if ($location['type'] == 2)
                                {
                                    $latestAvailableEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getAverageLatestDatasheetLocation($location['id'], $id, 1);
                                }

                                if (!empty($corrosiondatasheetlocationEntities))
                                {

                                    if ($location['type'] == 2)
                                    {
                                        $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                                        $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($id, 2, $system->getId());
                                    }

                                    foreach ($corrosiondatasheetlocationEntities as $index => $datasheetLocationDetails)
                                    {

                                        if ($datasheetLocationDetails[0]['retrieval_date'])
                                        {

                                            $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                            if ($location['type'] == 3)
                                            {
                                                if (!empty($latestEntities[0][0]['average_rate_mmpy']) && $latestEntities[0][0]['average_rate_mmpy'] == $datasheetLocationDetails[0]['average_rate_mmpy'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 2)
                                            {

                                                if (!empty($datasheetLocationDetails[0]['control_activity_achievement_value']))
                                                {

                                                    if (!empty($latestEntities[0][0]['control_activity_achievement_value']) && $latestEntities[0][0]['control_activity_achievement_value'] == $datasheetLocationDetails[0]['control_activity_achievement_value'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['target_ci_dosage_litre_per_day']))
                                                {

                                                    if (!empty($latestEntities[0][0]['target_ci_dosage_litre_per_day']) && $latestEntities[0][0]['target_ci_dosage_litre_per_day'] == $datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['ci_percentage_availability']))
                                                {

                                                    if (!empty($latestEntities[0][0]['ci_percentage_availability']) && $latestEntities[0][0]['ci_percentage_availability'] == $datasheetLocationDetails[0]['ci_percentage_availability'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']);
                                                        }
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 4)
                                            {

                                                if ( !empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'] )
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {

                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 1)
                                            {

                                                if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                        }

                                        if ($datasheetLocationDetails[0]['to_date'] && $location['type'] == 2)
                                        {

                                            foreach ($chemicalActivities as $chemicalActivity)
                                            {

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 2)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']) && $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage'] == $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']))
                                                        {
                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']);
                                                        }
                                                    }
                                                }

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 3)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']) && $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage'] == $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']))
                                                        {
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
                elseif ( $tab == 2 && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1 && !$checkActivity['isnonmonitoringkpi'] && ($activities->getIsflowassuranceManagement() == 1 || $activities->getIsflowassuranceControl() == 1))
                {

                    if ($activities->getIsflowassuranceManagement() == 1 && $activities->getIsflowassuranceControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 1)
                    {
                        $activitiesTitle = $activities->getControltitle();
                    }
                    else if ($activities->getIsflowassuranceManagement() == 1 && $activities->getIsflowassuranceControl() == 0)
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }
                    else if ($activities->getIsManagement() == 0 && $activities->getIsnonmonitoringkpi() == 1 && $activities->getIsControl() == 0 && $activities->getIsflowassuranceManagement() == 0 && $activities->getIsflowassuranceControl() == 0  )
                    {
                        $activitiesTitle = $activities->getManagementtitle();
                    }

                    if (!empty($activities->getKpiManagement())) {
                        $managementActivitiesRed = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'R');
                        $managementActivitiesAmber = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities->getId(), 'A');
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "R")
                    {
//                        $redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation)
                            {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'red';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'red';
                        }
                        $complianceKPIData['control']['red']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "R")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['red']++;
                    }
                    else if(!empty($managementActivitiesRed))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['red']++;
                    }


                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "A")
                    {
                        //$redcontrolCompliance[] = $activities->getId();

                        $tempsys = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities->getId());

                        $tempLocations = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities->getId());

                        $subRedControl['activity_locations']['systems'][$tempsys['id']]['title'] = $tempsys['title'];

                        if (count($tempLocations))
                        {
                            foreach ($tempLocations as $innerIndex => $tempLocation)
                            {

                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['title'] = $tempLocation['location'];
                                $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][$tempLocation['id']]['color'] = 'amber';
                            }
                        }
                        else
                        {
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['id'] = $activities->getId();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['kpicontrol'] = $activities->getKpiControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['title'] = $activitiesTitle;
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['justification'] = $activities->getJustificationControl();
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['title'] = "";
                            $subRedControl['activity_locations']['systems'][$tempsys['id']]['activity'][$activities->getId()]['location'][0]['color'] = 'amber';
                        }
                        $complianceKPIData['control']['amber']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "A")
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        $complianceKPIData['management']['amber']++;
                    }
                    else if(!empty($managementActivitiesAmber))
                    {
                        $redmanagementCompliance[] = $activities->getId();
                        //$complianceKPIData['management']['amber']++;
                    }


                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "G")
                    {
                        $complianceKPIData['control']['green']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "G")
                    {
                        $complianceKPIData['management']['green']++;
                    }

                    if (!empty($activities->getKpiControl()) && $activities->getKpiControl() == "H")
                    {
                        $complianceKPIData['control']['hold']++;
                    }

                    if (!empty($activities->getKpiManagement()) && $activities->getKpiManagement() == "H")
                    {
                        $complianceKPIData['management']['hold']++;
                    }

                    if ($activities->getNextInspectionDate())
                    {
                        $nextDate = $activities->getNextInspectionDate(true);
                        $nextDate_U = (int)$nextDate->format('U');

                        if ( $to_date->format('U') &&  $nextDate_U <= (int)$to_date->format('U') )
                        {
                            //set repeated action last time to next date

                            $overdues[] = $activities->getId();
                            $overdueActivities[$activities->getId()]['id'] =  $activities->getId();
                            $overdueActivities[$activities->getId()]['title'] = $activitiesTitle;
                            $overdueActivities[$activities->getId()]['duedate'] = $nextDate->format('d M Y');
                            $overdueActivities[$activities->getId()]['frequency'] = $activities->getfrequency();
                            $overdueActivities[$activities->getId()]['actionowner'] = (!empty($activities->getActionOwner()) ? $activities->getActionOwner()->getName() : "");
                            $overdues++;

                        }

                        //logic to get the count of overdue activities uptil the "To date"
                        $nextDate1 = $activities->getNextInspectionDate(true);
                        $nextDate_U1 = (int)$nextDate1->format('U');

                        while ($nextDate_U1 <= (int)$to_date->format('U'))
                        {

                            $faOverdueCount++;

                            $activities->setLastInspectionDate($nextDate1);

                            //set next date from repeated action
                            $nextDate1 = $activities->getNextInspectionDate(true);
                            $nextDate_U1 = (int)$nextDate1->format('U');

                        }

                    }

                    $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($id, 1);

                    foreach ($systems as $system)
                    {
                        $actid = $activities->getId();

                        $activitiesKpiControl = $activities->getKpiControl();

                        $locations = $cmsDatabasesHelper->getLocationsFromDatabasesPerSystemSubProjectActivity($system->getId(), $id, $actid);

                        $corrosiondatasheetlocationEntities = array();

                        //system data, location data, generating datasheet graphs
                        if (count($locations))
                        {

                            foreach ($locations as $location)
                            {

                                $corrosiondatasheetlocationEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getCorrosionHistoryByLocation($location['type'], $location['id'], $id, $activities->getId());

                                $latestEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getLatestDatasheetLocation($actid, $location['type'], $location['id'], $id, 1);

                                $latestAvailableEntities = array();

                                if ($location['type'] == 2)
                                {
                                    $latestAvailableEntities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getAverageLatestDatasheetLocation($location['id'], $id, 1);
                                }

                                if (!empty($corrosiondatasheetlocationEntities))
                                {

                                    if ($location['type'] == 2)
                                    {
                                        $dbSystems = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($id);
                                        $chemicalActivities = $cmsDatabasesHelper->getSubProjectSelectedSystemsDatabaseItems($id, 2, $system->getId());
                                    }

                                    foreach ($corrosiondatasheetlocationEntities as $index => $datasheetLocationDetails)
                                    {

                                        if ($datasheetLocationDetails[0]['retrieval_date'])
                                        {

                                            $newdate = date('Y,m,d', strtotime('-1 months', strtotime($datasheetLocationDetails[0]['retrieval_date']->format('Y-m-d'))));

                                            if ($location['type'] == 3)
                                            {
                                                if (!empty($latestEntities[0][0]['average_rate_mmpy']) && $latestEntities[0][0]['average_rate_mmpy'] == $datasheetLocationDetails[0]['average_rate_mmpy'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['average_rate_mmpy']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 2)
                                            {

                                                if (!empty($datasheetLocationDetails[0]['control_activity_achievement_value']))
                                                {

                                                    if (!empty($latestEntities[0][0]['control_activity_achievement_value']) && $latestEntities[0][0]['control_activity_achievement_value'] == $datasheetLocationDetails[0]['control_activity_achievement_value'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_achievement_value']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['target_ci_dosage_litre_per_day']))
                                                {

                                                    if (!empty($latestEntities[0][0]['target_ci_dosage_litre_per_day']) && $latestEntities[0][0]['target_ci_dosage_litre_per_day'] == $datasheetLocationDetails[0]['target_ci_dosage_litre_per_day'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['target_ci_dosage_litre_per_day']);
                                                        }
                                                    }

                                                }

                                                if (!empty($datasheetLocationDetails[0]['ci_percentage_availability']))
                                                {

                                                    if (!empty($latestEntities[0][0]['ci_percentage_availability']) && $latestEntities[0][0]['ci_percentage_availability'] == $datasheetLocationDetails[0]['ci_percentage_availability'])
                                                    {

                                                        if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']))
                                                        {

                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['ci_percentage_availability']);
                                                        }
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 4)
                                            {

                                                if ( !empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'] )
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {

                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                            else if ($location['type'] == 1)
                                            {

                                                if (!empty($latestEntities[0][0]['control_activity_value']) && $latestEntities[0][0]['control_activity_value'] == $datasheetLocationDetails[0]['control_activity_value'])
                                                {

                                                    if ($cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']))
                                                    {
                                                        //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $activities->getJustificationControl();
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                        $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareRedAmberThresholdsYaxis($activities, $latestEntities[0][0]['control_activity_value']);
                                                    }
                                                }

                                            }
                                        }

                                        if ($datasheetLocationDetails[0]['to_date'] && $location['type'] == 2)
                                        {

                                            foreach ($chemicalActivities as $chemicalActivity)
                                            {

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 2)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']) && $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage'] == $datasheetLocationDetails[0]['vol_inject_vs_vol_req_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']))
                                                        {
                                                            //$subRedControl['activity_locations']['systems'][$system->getId()][$actid][$location['id']] = true;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['vol_inject_vs_vol_req_percentage']);
                                                        }
                                                    }
                                                }

                                                if ($chemicalActivity['bmc_corrosioninhibitortype'] == 3)
                                                {
                                                    if (!empty($latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']) && $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage'] == $datasheetLocationDetails[0]['per_time_ci_target_reach_percentage'])
                                                    {

                                                        if (isset($chemicalActivity) && $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']))
                                                        {
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['title'] = $system->getSystemsTitle();
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['id'] = $actid;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['kpicontrol'] = $activitiesKpiControl;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['title'] = $activitiesTitle;
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['justification'] = $chemicalActivity['bsd_justificationcontrol'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['title'] = $location['location'];
                                                            $subRedControl['activity_locations']['systems'][$system->getId()]['activity'][$actid]['location'][$location['id']]['color'] = $cmsHelper->compareChemicalRedAmberThresholdsYaxis($chemicalActivity, $latestAvailableEntities[0][0]['per_time_ci_target_reach_percentage']);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }

            }
        }

        $subRedRealControls = array();

        if (!empty($redmanagementCompliance))
        {

            foreach ($redmanagementCompliance as $index => $activities)
            {

                $subRedManagement[$activities]['activity_locations']['id'] = $activities;
                $subRedManagement[$activities]['activity_locations']['locations'] = $cmsDatabasesHelper->getLocationsFromDatabasesPerActivityAsRows($activities);
                $subRedManagement[$activities]['activity_locations']['systems'] = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($activities);

                $managementRedActivities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities,'R');
                $managementAmberActivities = $em->getRepository('AIECmsBundle:SubProjectCorrosionDatasheet')->getActivityPerformedManagementKPI($id, $activities,'A');

                if (!empty($activities))
                {

                    foreach($managementRedActivities as $innerindex => $managementRedActivity)
                    {
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['management_performed'] = 'R';
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['id'] = $managementRedActivity['dbactivitylocationid'];
                    }

                    foreach($managementAmberActivities as $innerindex => $managementAberActivity)
                    {

                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['management_performed'] = 'A';
                        $subRedManagement[$activities]['activity_locations']['performedlocations'][$innerindex]['id'] = $managementRedActivity['dbactivitylocationid'];

                    }

                }

            }
        }

        $level1Granted = 0;
        $level2Granted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_SUB-PROJECT_SPECIFIC_DASHBOARD_SHOW',$this->userRolesMergeToCheck)){
            $level1Granted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_SUB-PROJECT_SPECIFIC_DASHBOARD_SHOW',$this->userRolesMergeToCheck)){
            $level2Granted = 1;
        }
        $subRedRealControls = array();

        if ($tab == 1) {
            $ctrlPrediction = round($compliance['overallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['overallmanagementcompliance'], 2);

        }
        if ($tab == 2 && ($compliance['flowassuranceoverallcontrolcompliance'] || $compliance['flowassuranceoverallmanagementcompliance'])) {
            $ctrlPrediction = round($compliance['flowassuranceoverallcontrolcompliance'], 2);
            $mgmtPrediction = round($compliance['flowassuranceoverallmanagementcompliance'], 2);
        }

        if ($ctrlPrediction || $mgmtPrediction)
        {
            $chartHasPrediction = date('Y-m-d');
        }

        $corrosionPrediction = ['ctrl'=>$ctrlPrediction,'mgmt'=>$mgmtPrediction];

        $finalSystemCompliance = [];

        foreach ($systemCompliance as $key=>$pair)
        {
            if (count($pair['ctrl']) > 0 || count($pair['mgmt']) > 0)
            {
                $finalSystemCompliance[$key] = [
                    'name'=>$pair['name'],
                    'mgmt'=>'N/A',
                    'ctrl'=>'N/A',
                    'justification'=>$pair['justification'],
                ];
                $counter =0;
                $mgmtCount = 0;
                foreach($pair['mgmt'] as $item){
                    if ($item == "R"){
                        $counter += 1;
                        $mgmtCount += 0;
                    }
                    if ($item == "A"){
                        $counter += 1;
                        $mgmtCount += 50;
                    }
                    if ($item == "G"){
                        $counter += 1;
                        $mgmtCount += 100;
                    }
                }
                if ($counter > 0){
                    $finalSystemCompliance[$key]['mgmt'] = $mgmtCount / $counter;
                }else{
                    $finalSystemCompliance[$key]['mgmt'] = "N/A";
                }
                $counter =0;
                $ctrlCount = 0;
                foreach($pair['ctrl'] as $item){
                    if ($item == "R"){
                        $counter += 1;
                        $mgmtCount += 0;
                    }
                    if ($item == "A"){
                        $counter += 1;
                        $mgmtCount += 50;
                    }
                    if ($item == "G"){
                        $counter += 1;
                        $mgmtCount += 100;
                    }
                }
                if ($counter > 0){
                    $finalSystemCompliance[$key]['ctrl'] = $ctrlCount / $counter;
                }else{
                    $finalSystemCompliance[$key]['ctrl'] = "N/A";
                }
            }

        }

        return [
            'projectId' => $projectId,
            'subprojectId' => $id,
            'entity' => $entity,
            'entityreports' => $entityreports,
            'chart_has_prediction'=>$chartHasPrediction,
            'corrosion_prediction'=>$corrosionPrediction,
            'redmanagementkpis' => $subRedManagement,
            'redcontrolkpis' => $subRedControl,
            'redrealcontrolkpis' => $subRedRealControls,
            'overdues' => count(array_unique($overdueActivities,SORT_REGULAR)),
            'managementComplianceData' => $managementGraphData,
            'controlComplianceData' => $controlGraphData,
            'redComplianceData' => $redGraphData,
            'greenComplianceData' => $greenGraphData,
            'complianceKPIData' => $complianceKPIData,
            'overdueActivities' =>array_unique($overdueActivities,SORT_REGULAR),
            'level1Granted' => $level1Granted,
            'level2Granted' => $level2Granted,
            'enabled_systems'=>$enabledSystems,
            'category' => $tab,
            'systems_compliance'=>$finalSystemCompliance,
            'level1Granted' => $level1Granted,
            'level2Granted' => $level2Granted,
            'fromDate' => $from_date,
            'toDate' => $to_date,
            'RedKPI_ODCounts' => $RedKPI_ODCounts,
            'corrOverdueCount' =>$corrOverdueCount,
            'faOverdueCount' =>$faOverdueCount,
        ];
    }

    /**
     * Generate pdf
     *
     * @Route("/{id}/{type}/{fromDate}/{toDate}/report-pdf", name="cms_dashboard_report_pdf")
     * @Method("GET")
     */
    public function reportGeneratePDFAction(Request $request, $id, $projectId, $fromDate, $toDate, $type)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($projectId);
        $subentity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $pageUrl = $this->generateUrl('cms_subprojects_dashboard_report', array('projectId' => $projectId, 'id' => $id, 'type' => $type, 'fromDate' => $fromDate, 'toDate' => $toDate), true); // use absolute path!

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $dashboardentity = new SubProjectDashboardActivity();
        $dashboardentity->setSubProject($subentity);
        $em->persist($dashboardentity);
        $em->flush();


        $headerHtml = $this->renderView(
            'AIECmsBundle:SubProjects:header.html.twig',array( 'entity' => $entity, ));

        $footerHtml = $this->renderView(
            'AIECmsBundle:SubProjects:footer.html.twig',array( 'entity' => $entity, 'subentity' => $subentity, 'reportId' => $dashboardentity->getId()));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl, array('cookie' => array($session->getName() => $session->getId()),
                'encoding' => 'utf-8',
                'images' => true,
                'enable-javascript' => true,
                'header-html'=> $headerHtml,
                'footer-html'=> $footerHtml
            )), 200, array(
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="AIE-VeracityCCM-DashboardReport.pdf"'
            )
        );
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/edit", name="cms_subprojects_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity, $projectId);
        $deleteForm = $this->createDeleteForm($id, $projectId);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SubProjects $entity, $projectId)
    {
        $em = $this->getManager();

        $form = $this->createForm(
            new SubProjectsEditType($entity),
            $entity,
            [
                'action' => $this->generateUrl('cms_subprojects_update', ['id' => $entity->getId(), 'projectId' => $projectId]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="cms_subprojects_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:SubProjects:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $projectId);
        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            /* Handle Uploaded File */
            $data = $editForm->getData();
            if ($data->getFile()){
                $file = $data->getFile();
                $uploadedPath = $this->replaceFile($file, $entity);
                $entity->setFilepath($uploadedPath);
            }

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Sub-Project '.$entity->getName().' updated');

            return $this->redirect($this->generateUrl('cms_list_subprojects', ['id' => $projectId]));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a Projects entity.
     *
     * @Route("/{id}", name="cms_subprojects_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {

        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sub Projects entity.');
            }

            $subentities = $em->getRepository('AIECmsBundle:SubProjectSystems')->findBy(array('subproject'=>$id));
            $selected = false;
            foreach($subentities as $subentity){
                if ($subentity->getIsselected()==1){
                    $selected= true;
                }
            }

            if ($selected == true){
                $this->addFlash('warning', 'Sub project cannot be deleted, since the matrix has been associated!');
                return $this->redirect($this->generateUrl('cms_list_subprojects',['id' => $projectId]));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);
            $em->flush();
            $this->addFlash('success', 'Sub-project deleted');
        }

        return $this->redirect($this->generateUrl('cms_list_subprojects',['id' => $projectId]));
    }

    /**
     * Creates a form to delete a Projects entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $projectId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subprojects_delete', ['id' => $id, 'projectId' => $projectId]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }

    public function erase($databases,$id,$em,$type){

        switch($type){
            case 1:
                $statement= 'AIECmsBundle:SubProjectCathodicProtectionDatabase';
                break;
            case 2:
                $statement= 'AIECmsBundle:SubProjectChemicalDosageDatabase';
                break;
            case 3:
                $statement= 'AIECmsBundle:SubProjectCorrosionMonitoringDatabase';
                break;
            case 4:
                $statement= 'AIECmsBundle:SubProjectSamplingDatabase';
                break;
            default:
                break;
        }
        foreach ($databases as $index => $database) {
            if (!empty($database)) {

                $entities = $em->getRepository($statement)->findOneBy(array('subprojectsystems'=>$database['bs_id'],'subproject'=>$id));
                if (!empty($entities)){
                    $entitiesA = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($entities->getActivityid());

                    if (!empty($entitiesA) ) {
                        //echo $entitiesA->getIsControl()."==".$entitiesA->getIsManagement()."==".$entitiesA->getIsnonmonitoringkpi()."<br>";
                        if ($entitiesA->getIsControl() || $entitiesA->getIsManagement() || $entitiesA->getIsnonmonitoringkpi()) {

                        }
                        else{
                            unset($databases[$index]);
                        }
                    }
                    else{
                        unset($databases[$index]);
                    }
                }

            }
        }

        return $databases;

    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}/clone", name="cms_subprojects_clone")
     * @Method("GET")
     */
    public function cloneAction( Request $request, $projectId, $id )
    {
        if ($this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN',$this->userRolesMergeToCheck)) {

            $rustart = getrusage();
            $em = $this->getManager();

            $mainEntity = $em->getRepository('AIECmsBundle:SubProjects')->find($id);

            $entity = new SubProjects();

            $oldReflection = new \ReflectionObject($mainEntity);
            $newReflection = new \ReflectionObject($entity);

            foreach ($oldReflection->getProperties() as $property) {
                if ($newReflection->hasProperty($property->getName())) {
                    $newProperty = $newReflection->getProperty($property->getName());
                    $property->setAccessible(true);
                    $newProperty->setAccessible(true);
                    $newProperty->setValue($entity, $property->getValue($mainEntity));
                }
            }

            if ($mainEntity->getUg()) {
                foreach ($mainEntity->getUg() as $userProjectGroup) {
                    $newUserProjectGroup = clone $userProjectGroup;
                    $newUserProjectGroup->setSubproject($entity);
                    $em->persist($newUserProjectGroup);
                    echo "User project groups copied::" . $userProjectGroup->getId() . "<br>";
                }
            }

            if ($mainEntity->getDatabaseproperties()) {
                foreach ($mainEntity->getDatabaseproperties() as $dp) {
                    $newSdp = clone $dp;
                    $newSdp->setSubproject($entity);
                    $em->persist($newSdp);
                    echo "Database Configuration one column copied::" . $dp->getId() . "<br>";
                }
            }

            if ($mainEntity->getDatabaselinkedproperties()) {
                foreach ($mainEntity->getDatabaselinkedproperties() as $dlp) {
                    $newDlp = clone $dlp;
                    $newDlp->setSubproject($entity);
                    $em->persist($newDlp);
                    echo "Database Configuration two column copied :: " . $dlp->getId() . "<br>";
                }
            }

            if ($mainEntity->getSystems()) {
                foreach ($mainEntity->getSystems() as $system) {
                    $newSystem = clone $system;
                    $newSystem->setSubproject($entity);

                    $em->persist($newSystem);
                    echo "Systems copied::" . $system->getId() . "<br>";

                }
            }

            if ($mainEntity->getThreats()) {
                foreach ($mainEntity->getThreats() as $threat) {
                    //$subProjectSystem = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject'=>$entity->getId(),'basesystemid' =>$threat->getBasesystemid()));
                    $newThreat = clone $threat;
                    $newThreat->setSubproject($entity);
                    //$newThreat->setSubProjectSystems($subProjectSystem);
                    $em->persist($newThreat);
                    echo "Threats copied::" . $system->getId() . "<br>";

                }
            }

            if ($mainEntity->getMitigationtechniques()) {
                foreach ($mainEntity->getMitigationtechniques() as $mitigationtechnique) {

                    $newMitigationtechnique = clone $mitigationtechnique;
                    $newMitigationtechnique->setSubproject($entity);

                    $em->persist($newMitigationtechnique);
                    echo "Mitigations Techniques copied::" . $system->getId() . "<br>";

                }
            }

            if ($mainEntity->getActivity()) {
                foreach ($mainEntity->getActivity() as $activity) {

                    $newActivity = clone $activity;
                    $newActivity->setSubproject($entity);

                    $em->persist($newActivity);
                    echo "Activity copied::" . $system->getId() . "<br>";
                }
            }

//            if ($mainEntity->getSpcpd()) {
//                foreach ($mainEntity->getSpcpd() as $sPCPD) {
//
//                    $newSPCPD = clone $sPCPD;
//                    $newSPCPD->setSubproject($entity);
//                    $em->persist($newSPCPD);
//                    echo "SubProjectCathodicProtectionDatabase copied::" . $system->getId() . "<br>";
//                }
//
//            }
//
//            if ($mainEntity->getSpcmd()) {
//
//                foreach ($mainEntity->getSpcmd() as $sPCMD) {
//                    $newSPCMD = clone $sPCMD;
//                    $newSPCMD->setSubproject($entity);
//
//                    $em->persist($newSPCMD);
//                    echo "SubProjectCorrosionMonitoringDatabase copied::" . $system->getId() . "<br>";
//                }
//
//            }
//
//            if ($mainEntity->getSpsd()) {
//
//                foreach ($mainEntity->getSpsd() as $sPSD) {
//                    $newSPSD = clone $sPSD;
//                    $newSPSD->setSubproject($entity);
//                    $em->persist($newSPSD);
//                    echo "SubProjectSamplingDatabase copied::" . $system->getId() . "<br>";
//                }
//
//            }
//
//            if ($mainEntity->getSpcdd()) {
//
//                foreach ($mainEntity->getSpcdd() as $sPCDD) {
//                    $newSPCDD = clone $sPCDD;
//                    $newSPCDD->setSubproject($entity);
//                    $em->persist($newSPCDD);
//                    echo "SubProjectChemicalDosageDatabase copied::" . $system->getId() . "<br>";
//                }
//
//            }
//
//            if ($mainEntity->getSpprd()) {
//
//                foreach ($mainEntity->getSpprd() as $sPPRD) {
//                    $newSPPRD = clone $sPPRD;
//                    $newSPPRD->setSubproject($entity);
//                    $em->persist($newSPPRD);
//                    echo "SubProjectProductionDatabase copied::" . $system->getId() . "<br>";
//                }
//
//            }

            // if ($mainEntity->getFilescategories()) {

            //     foreach ($mainEntity->getFilescategories() as $fileCat) {

            //         $newFileCat = clone $fileCat;
            //         $newFileCat->setSubproject($entity);
            //         $em->persist($newFileCat);
            //         echo "File categories copied::" . $fileCat->getId() . "<br>";
            //     }

            // }

            // if ($mainEntity->getFiles()) {

            //     foreach ($mainEntity->getFiles() as $file) {
            //         $newFile = clone $file;
            //         $newFile->setSubproject($entity);
            //         $em->persist($newFile);
            //         echo "File ::" . $file->getId() . "<br>";
            //     }

            // }

            // if ($mainEntity->getActionOwners()) {

            //     foreach ($mainEntity->getActionOwners() as $actionOwner) {
            //         $newActionOwner = clone $actionOwner;
            //         $newActionOwner->setSubproject($entity);
            //         $em->persist($newActionOwner);
            //         echo "Action Owner ::" . $actionOwner->getId() . "<br>";
            //     }

            // }

            // if ($mainEntity->getWorkshop()) {

            //     foreach ($mainEntity->getWorkshop() as $workshop) {

            //         $newWorkshop = new SubProjectWorkshops();

            //         $oldReflectionWorkshop = new \ReflectionObject($workshop);
            //         $newReflectionWorkshop = new \ReflectionObject($newWorkshop);

            //         foreach ($oldReflectionWorkshop->getProperties() as $property) {
            //             if ($newReflectionWorkshop->hasProperty($property->getName())) {
            //                 $newProperty = $newReflectionWorkshop->getProperty($property->getName());
            //                 $property->setAccessible(true);
            //                 $newProperty->setAccessible(true);
            //                 $newProperty->setValue($newWorkshop, $property->getValue($workshop));
            //                 echo "Workshop ::" . $workshop->getId() . "<br>";
            //             }
            //             $newWorkshop->setSubproject($entity);
            //             $em->persist($newWorkshop);
            //         }

            //         foreach ($workshop->getSpwa() as $workshopAttendee) {

            //             $newWorkshopAttendee = clone $workshopAttendee;
            //             $newWorkshopAttendee->setWorkshop($newWorkshop);
            //             $em->persist($newWorkshopAttendee);
            //         }
            //     }

            // }

            // if ($mainEntity->getActiontracker()) {

            //     foreach ($mainEntity->getActiontracker() as $actionTracker) {

            //         $newActionTracker = new ActionTracker();

            //         $oldReflectionAT = new \ReflectionObject($actionTracker);
            //         $newReflectionAT = new \ReflectionObject($newActionTracker);

            //         foreach ($oldReflectionAT->getProperties() as $property) {
            //             if ($newReflectionAT->hasProperty($property->getName())) {
            //                 $newProperty = $newReflectionAT->getProperty($property->getName());
            //                 $property->setAccessible(true);
            //                 $newProperty->setAccessible(true);
            //                 $newProperty->setValue($newActionTracker, $property->getValue($actionTracker));
            //                 echo "Action Tracker ::" . $actionTracker->getId() . "<br>";
            //             }
            //             $newActionTracker->setSubproject($entity);
            //             $em->persist($newActionTracker);
            //         }

            //         foreach ($actionTracker->getActiontrackeritems() as $actionTrackerItems) {
            //             $newActionTrackerItems = clone $actionTrackerItems;
            //             $newActionTrackerItems->setActionTracker($newActionTracker);
            //             $em->persist($newActionTrackerItems);

            //         }
            //     }

            // }

            try {
                $em->persist($entity);
                $em->flush($entity);
                $em->clear();
                echo "Sub Project Id:: " . $mainEntity->getId() . " copied to " . $entity->getId() . "<br>";

                $dbthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(array('subproject' => $entity->getId()));

                if (!empty($dbthreats)) {
                    foreach ($dbthreats as $dbthreat) {
                        $subsystemid = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $dbthreat->getSubproject()->getId(), 'basesystemid' => $dbthreat->getBasesystemid()));
                        if ($dbthreat->getBasesystemid() != NULL && !empty($subsystemid)) {

                            $dbthreat->setSubProjectSystems($em->getRepository('AIECmsBundle:SubProjectSystems')->find($subsystemid->getId()));
                            $em->persist($dbthreat);
                        } else {
                            if ($dbthreat->getSubProjectSystems() != NULL) {
                                $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($dbthreat->getSubProjectSystems()->getId());

                                echo "Base Systems" . $dbthreat->getSubProjectSystems()->getId() . "==" . $subsystems->getSystemsTitle() . "<br>";

                                $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));

                                $dbthreat->setSubProjectSystems($subsystemsreal);
                                $em->persist($dbthreat);

                            }
                        }
                    }
                }

                $dbmitigationtechniques = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findBy(array('subproject' => $entity->getId()));
                if (!empty($dbmitigationtechniques)) {
                    foreach ($dbmitigationtechniques as $dbmitigationtechnique) {
                        $subthreatid = $em->getRepository('AIECmsBundle:SubProjectThreats')->findOneBy(array('subproject' => $dbmitigationtechnique->getSubproject()->getId(), 'basethreatid' => $dbmitigationtechnique->getBasethreatid()));
                        if ($dbmitigationtechnique->getBasethreatid() != NULL && !empty($subthreatid)) {

                            $dbmitigationtechnique->setSubProjectThreats($em->getRepository('AIECmsBundle:SubProjectThreats')->find($subthreatid->getId()));
                            $em->persist($dbmitigationtechnique);
                        } else {
                            if ($dbmitigationtechnique->getSubProjectThreats() != NULL) {

                                $subthreats = $em->getRepository('AIECmsBundle:SubProjectThreats')->find($dbmitigationtechnique->getSubProjectThreats()->getId());

                                echo "Base Threats" . $dbmitigationtechnique->getSubProjectThreats()->getId() . "==" . $subthreats->getThreatsTitle() . "<br>";

                                $subthreatsreal = $em->getRepository('AIECmsBundle:SubProjectThreats')->findOneBy(array('subproject' => $entity->getId(), 'subprojectsystems' => $subthreats->getSubProjectSystems()->getId(), 'threatstitle' => $subthreats->getThreatsTitle()));

                                $dbmitigationtechnique->setSubProjectThreats($subthreatsreal);
                                $em->persist($dbmitigationtechnique);

                            }
                        }
                    }
                }

                $dbactivities = $em->getRepository('AIECmsBundle:SubProjectActivity')->findBy(array('subproject' => $entity->getId()));
                if (!empty($dbactivities)) {
                    foreach ($dbactivities as $dbactivity) {
                        $submitigationtechniqueid = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findOneBy(array('subproject' => $dbactivity->getSubproject()->getId(), 'basemitigationtechniqueid' => $dbactivity->getBasemitigationtechniqueid()));
                        if ($dbactivity->getBasemitigationtechniqueid() != NULL && !empty($submitigationtechniqueid)) {
//                        echo $submitigationtechniqueid->getId()."<br>";
                            $dbactivity->setSubProjectMitigationTechniques($em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($submitigationtechniqueid->getId()));
                            $em->persist($dbactivity);
                        } else {
                            if ($dbactivity->getSubProjectMitigationTechniques() != NULL) {
                                $submitigationtechniques = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->find($dbactivity->getSubProjectMitigationTechniques()->getId());

                                echo "Base Mitigations" . $dbactivity->getSubProjectMitigationTechniques()->getId() . "==" . $submitigationtechniques->getMitigationTechniquesTitle() . "<br>";

                                $subrealmitigationtechniques = $em->getRepository('AIECmsBundle:SubProjectMitigationTechniques')->findOneBy(array('subproject' => $entity->getId(), 'subprojectthreats' => $submitigationtechniques->getSubProjectThreats()->getId(), 'mitigationtechniquestitle' => $submitigationtechniques->getMitigationTechniquesTitle()));

                                $dbactivity->setSubProjectMitigationTechniques($subrealmitigationtechniques);
                                $em->persist($dbactivity);

                            }
                        }
                    }
                }

//                $dbcpd = $em->getRepository('AIECmsBundle:SubProjectCathodicProtectionDatabase')->findBy(array('subproject' => $entity->getId()));
//
//                if ($dbcpd) {
//                    foreach ($dbcpd as $sPCPD) {
//                        $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sPCPD->getSubProjectSystems()->getId());
//
//                        $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));
//
//                        $sPCPD->setSubProjectSystems($subsystemsreal);
//                        //$sPCPD->setSubproject($entity);
//                        $em->persist($sPCPD);
//                        echo "SubProjectCathodicProtectionDatabase updated::" . $system->getId() . "<br>";
//                    }
//
//                }
//
//                $dbcmd = $em->getRepository('AIECmsBundle:SubProjectCorrosionMonitoringDatabase')->findBy(array('subproject' => $entity->getId()));
//                if ($dbcpd) {
//
//                    foreach ($dbcmd as $sPCMD) {
//                        $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sPCMD->getSubProjectSystems()->getId());
//
//                        $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));
//
//                        $sPCMD->setSubProjectSystems($subsystemsreal);
//                        //$sPCMD->setSubproject($entity);
//                        $em->persist($sPCMD);
//                        echo "SubProjectCorrosionMonitoringDatabase updated::" . $system->getId() . "<br>";
//                    }
//
//                }
//
//                $dbcmd = $em->getRepository('AIECmsBundle:SubProjectSamplingDatabase')->findBy(array('subproject' => $entity->getId()));
//                if ($dbcmd) {
//
//                    foreach ($dbcmd as $sPSD) {
//
//                        $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sPSD->getSubProjectSystems()->getId());
//
//                        $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));
//
//                        $sPSD->setSubProjectSystems($subsystemsreal);
//                        //$sPSD->setSubproject($entity);
//                        $em->persist($sPSD);
//                        echo "SubProjectSamplingDatabase updated::" . $system->getId() . "<br>";
//                    }
//
//                }
//
//                $dbcdd = $em->getRepository('AIECmsBundle:SubProjectChemicalDosageDatabase')->findBy(array('subproject' => $entity->getId()));
//                if ($dbcdd) {
//
//                    foreach ($dbcdd as $sPCDD) {
//                        $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sPCDD->getSubProjectSystems()->getId());
//                        $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));
//                        $sPCDD->setSubProjectSystems($subsystemsreal);
//                        //$sPCDD->setSubproject($entity);
//                        $em->persist($sPCDD);
//                        echo "SubProjectChemicalDosageDatabase updated::" . $system->getId() . "<br>";
//                    }
//
//                }
//
//
//                $dbprd = $em->getRepository('AIECmsBundle:SubProjectProductionDatabase')->findBy(array('subproject' => $entity->getId()));
//                if ($dbprd) {
//
//                    foreach ($dbprd as $sPRD) {
//                        $subsystems = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($sPRD->getSubProjectSystems()->getId());
//                        $subsystemsreal = $em->getRepository('AIECmsBundle:SubProjectSystems')->findOneBy(array('subproject' => $entity->getId(), 'systemstitle' => $subsystems->getSystemsTitle()));
//                        $sPRD->setSubProjectSystems($subsystemsreal);
//                        //$sPCDD->setSubproject($entity);
//                        $em->persist($sPRD);
//                        echo "SubProjectProductionDatabase updated::" . $system->getId() . "<br>";
//                    }
//
//                }

                $em->flush();
                $em->clear();
            } catch (\Doctrine\DBAL\DBALException $e) {
                die($e);
            }

            $ru = getrusage();
            $computations = $this->rutime($ru, $rustart, "utime");
            $sustems = $this->rutime($ru, $rustart, "stime");
            $html = <<<HTML
Duplicate process used  $computations  ms for its computations \n
It spent  $sustems ms in system calls \n
HTML;
            die($html);
        }
        else
        {
            die('You are not permitted to clone');
        }
        return true;
    }

    function rutime($ru, $rus, $index)
    {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
            -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }
}
