<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties as DatabaseLinkedProperties;
use AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties as DatabaseProperties;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * SubProjectDatabaseConfig controller.
 *
 * @Route("/{projectId}/subprojectdatabaseconfig/{subprojectId}")
 */
class SubProjectDatabaseConfigController extends CmsBaseController
{

    Protected $basedataId = "";

    Protected $findduplicateproperties = array('serviceid','samplingorientationid','corrosionorientationid','accessfittingtypeid','holderplugid','probetechnologyid','couponmaterialid','coupontypeid','insulationid','surfacefinishid','chemicaltypeid','pumptypeid','injectionmethodid');

    Protected $entityCheck = "";

    /**
     * Lists all SubProjectDatabaseConfig entities.
     *
     * @Route("/", name="cms_subprojectdatabase")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId,$subprojectId)
    {

        $em = $this->getManager();

        $databaseProperties = $em->getRepository('AIECmsBundle:BaseDatabase')->findAll();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.serviceid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);


        $serviceEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.samplingorientationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $samplingorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.activitypersonid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $activitypersonEntities = $query->getResult();


        //corrosion

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.installeddeviceid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $installeddeviceEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.corrosionorientationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $corrosionorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.accessfittingtypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $accessfittingtypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.teefittingid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $teefittingEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.holderplugid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $holderplugEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.probetechnologyid= bsp.colidone AND bs.dataloggerid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $probetechnologyEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.couponmaterialid= bsp.colidone where bs.densityid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $couponmaterialEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.coupontypeid= bsp.colidone where bs.areaid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $coupontypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.insulationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $insulationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.surfacefinishid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $surfacefinishEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.onlineid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $onlineEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.scaffoldid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $scaffoldEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.accessid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $accessEntities = $query->getResult();


        // chemical

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.chemicaltypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $chemicaltypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.pumptypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $pumptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.injectionmethodid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $injectionmethodEntities = $query->getResult();


        // Cathodic

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.locationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $locationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.cptypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $cptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.equipmenttypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $equipmenttypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.anodetypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $anodetypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.monitoringmethodid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $monitoringmethodEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.referenceelectrodeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $referenceelectrodeEntities = $query->getResult();


        return [
            'databaseproperties' => $databaseProperties,
            'serviceentities' => $serviceEntities,

            'samplingorientationentities' => $samplingorientationEntities,
            'activitypersonentities' => $activitypersonEntities,

            'installeddeviceentities' => $installeddeviceEntities,
            'corrosionorientationentities' => $corrosionorientationEntities,
            'accessfittingtypeentities' => $accessfittingtypeEntities,
            'teefittingentities' => $teefittingEntities,
            'holderplugentities' => $holderplugEntities,
            'probetechnologyentities' => $probetechnologyEntities,
            'couponmaterialentities' => $couponmaterialEntities,
            'coupontypeentities' => $coupontypeEntities,
            'insulationentities' => $insulationEntities,
            'surfacefinishentities' => $surfacefinishEntities,
            'onlineentities' => $onlineEntities,
            'scaffoldentities' => $scaffoldEntities,
            'accessentities' => $accessEntities,

            'chemicaltypeentities' => $chemicaltypeEntities,
            'pumptypeentities' => $pumptypeEntities,
            'injectionmethodentities' => $injectionmethodEntities,

            'locationentities' => $locationEntities,
            'cptypeentities' => $cptypeEntities,
            'equipmenttypeentities' => $equipmenttypeEntities,
            'anodetypeentities' => $anodetypeEntities,
            'monitoringmethodentities' => $monitoringmethodEntities,
            'referenceelectrodeentities' => $referenceelectrodeEntities,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,

        ];

    }


    /**
     * Lists all SubProjectDatabaseConfig entities.
     *
     * @Route("/refer", name="cms_subprojectdatabase_refer")
     * @Method("GET")
     * @Template()
     */
    public function referAction($projectId, $subprojectId)
    {

        $em = $this->getManager();

        $databaseProperties = $em->getRepository('AIECmsBundle:BaseDatabase')->findAll();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.serviceid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);


        $serviceEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.samplingorientationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $samplingorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.activitypersonid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $activitypersonEntities = $query->getResult();


        //corrosion

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.installeddeviceid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $installeddeviceEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.corrosionorientationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $corrosionorientationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.accessfittingtypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $accessfittingtypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.teefittingid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $teefittingEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.holderplugid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $holderplugEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.probetechnologyid= bsp.colidone AND bs.dataloggerid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $probetechnologyEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.couponmaterialid= bsp.colidone where bs.densityid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $couponmaterialEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseLinkedProperties bsp WITH bs.coupontypeid= bsp.colidone where bs.areaid= bsp.colidtwo AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $coupontypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.insulationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $insulationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.surfacefinishid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $surfacefinishEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.onlineid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $onlineEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.scaffoldid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $scaffoldEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.accessid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $accessEntities = $query->getResult();


        // chemical

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.chemicaltypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $chemicaltypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.pumptypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $pumptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.injectionmethodid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $injectionmethodEntities = $query->getResult();


        // Cathodic

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.locationid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $locationEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.cptypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $cptypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.equipmenttypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $equipmenttypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.anodetypeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $anodetypeEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.monitoringmethodid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $monitoringmethodEntities = $query->getResult();

        $query = $em->createQuery(
            'SELECT bs,bsp FROM AIECmsBundle:BaseDatabase bs join AIECmsBundle:SubProjectDatabaseProperties bsp WITH bs.referenceelectrodeid= bsp.colid AND bsp.subproject=:subprojectid'
        )
            ->setParameter('subprojectid', $subprojectId);

        $referenceelectrodeEntities = $query->getResult();


        return [
            'databaseproperties'=>$databaseProperties,
            'serviceentities' => $serviceEntities,

            'samplingorientationentities' => $samplingorientationEntities,
            'activitypersonentities' => $activitypersonEntities,

            'installeddeviceentities' => $installeddeviceEntities,
            'corrosionorientationentities' => $corrosionorientationEntities,
            'accessfittingtypeentities' => $accessfittingtypeEntities,
            'teefittingentities' => $teefittingEntities,
            'holderplugentities' => $holderplugEntities,
            'probetechnologyentities' => $probetechnologyEntities,
            'couponmaterialentities' => $couponmaterialEntities,
            'coupontypeentities' => $coupontypeEntities,
            'insulationentities' => $insulationEntities,
            'surfacefinishentities' => $surfacefinishEntities,
            'onlineentities' => $onlineEntities,
            'scaffoldentities' => $scaffoldEntities,
            'accessentities' => $accessEntities,

            'chemicaltypeentities' => $chemicaltypeEntities,
            'pumptypeentities' => $pumptypeEntities,
            'injectionmethodentities' => $injectionmethodEntities,

            'locationentities' => $locationEntities,
            'cptypeentities' => $cptypeEntities,
            'equipmenttypeentities' => $equipmenttypeEntities,
            'anodetypeentities' => $anodetypeEntities,
            'monitoringmethodentities' => $monitoringmethodEntities,
            'referenceelectrodeentities' => $referenceelectrodeEntities,
            'projectId' => $projectId,
            'subprojectId' => $subprojectId,

        ];

    }

    /**
     * Create
     *
     * @Route("/{sid}/create", name="cms_subprojectdatabase_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $sid, $subprojectId)
    {
        $title = $request->request->get('title');
        $id = $request->request->get('id');
        $em = $this->getManager();

        $response = new JsonResponse();

        $entity = new DatabaseProperties();

        if ($this->securityHelper->isRoleGranted('ROLE_DATABASES',$this->userRolesMergeToCheck)) {
            if ($request->isXmlHttpRequest()) {
                $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

                if (!$title) return new Response(json_encode(array('result' => 'error::empty')));

                $this->basedataId = $em->getRepository('AIECmsBundle:BaseDatabase')->find(1);

                if ($this->findduplicateproperties) {
                    foreach ($this->findduplicateproperties as $properties) {
                        if ($sid == $this->basedataId[0][$properties]) {
                            $this->entityCheck = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->findBy(['subproject' => $subprojectId, 'colvalue' => $title, 'colid' => $sid]);
                        }
                    }
                }

                if ($this->entityCheck) return new Response(json_encode(array('result' => 'error::duplicate')));

                if ($id) {
                    $entity = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($id);
                } else {
                    $entity->setColId($sid);
                }

                $entity->setSubproject($subprojectDetails);
                $entity->setColValue($title);

                $em->persist($entity);

                $em->flush();

                return new Response(json_encode(array('result' => 'ok')));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'error::denied')));
        }
    }

    /**
     * Create Linked
     *
     * @Route("/{sid}/{lid}/linkedcreate", name="cms_subprojectdatabase_linkedcreate")
     * @Method("POST")
     * @Template()
     */
    public function createlinkedAction(Request $request, $sid, $lid, $subprojectId)
    {
        $titlecolone = $request->request->get('titlecolone');
        $titlecoltwo = $request->request->get('titlecoltwo');
        $id = $request->request->get('id');
        $em = $this->getManager();

        $response = new JsonResponse();

        $entity = new DatabaseLinkedProperties();

        if ($this->securityHelper->isRoleGranted('ROLE_DATABASES',$this->userRolesMergeToCheck))
        {
            if ($request->isXmlHttpRequest()) {

                $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

                if (!$titlecolone && !$titlecoltwo) return new Response(json_encode(array('result' => 'error::empty')));

                $this->basedataId = $em->getRepository('AIECmsBundle:BaseDatabase')->find(1);

                if (!$id) {

                    foreach ($this->findduplicateproperties as $properties) {
                        if ($sid == $this->basedataId[0][$properties]) {
                            $this->entityCheck = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->findBy(['subproject' => $subprojectId, 'colidonevalue' => $titlecolone]);
                        }
                    }

                    if ($this->entityCheck) {
                        return new Response(json_encode(array('result' => 'error::duplicate')));
                    }
                }

                if ($lid == $this->basedataId[0]['densityid'] || $lid == $this->basedataId[0]['areaid']) {
                    if (!is_numeric($titlecoltwo)) {
                        return new Response(json_encode(array('result' => 'error::nointeger')));
                    }
                }

                if ($id) {
                    $entity = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($id);
                } else {
                    $entity->setColIdOne($sid);
                    $entity->setColIdTwo($lid);
                }
                $entity->setSubproject($subprojectDetails);
                $entity->setColIdOneValue($titlecolone);
                $entity->setColIdTwoValue($titlecoltwo);

                $em->persist($entity);

                $em->flush();

                $entities = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->findAll();

                return new Response(json_encode(array('result' => 'ok')));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'error::denied')));
        }
    }

    /**
     * Delete
     *
     * @Route("/{sid}/delete", name="cms_subprojectdatabase_delete")
     * @Method("POST")
     * @Template()
     */
    public function deleteAction(Request $request, $sid, $subprojectId)
    {
        $id = $request->request->get('id');
        $em = $this->getManager();

        $response = new JsonResponse();

        $entity = new DatabaseProperties();

        if ($this->securityHelper->isRoleGranted('ROLE_DATABASES_CONFIGURATION_DELETE',$this->userRolesMergeToCheck))
        {

            if ($request->isXmlHttpRequest()) {

                $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

                if ($id) {
                    $entity = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($id);
                }

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find SubProjectDatabaseProperties entity.');
                }

                $em->remove($entity);
                $em->flush();

                $entities = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->findAll();
                $response = array();
                foreach ($entities as $index => $entity) {
                    $response[$index]['title'] = $entity->getColValue();
                    $response[$index]['id'] = $entity->getId();
                }
                return new Response(json_encode(array('result' => 'ok', 'response' => $response)));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'error::denied')));
        }
    }


    /**
     * Lists all SubProjectDatabaseConfig entities.
     *
     * @Route("/{sid}/{lid}/linkeddelete", name="cms_subprojectdatabase_linkeddelete")
     * @Method("POST")
     * @Template()
     */
    public function deletelinkedAction(Request $request, $sid, $subprojectId)
    {
        $id = $request->request->get('id');
        $em = $this->getManager();

        $response = new JsonResponse();

        $entity = new DatabaseLinkedProperties();

        if ($this->securityHelper->isRoleGranted('ROLE_DATABASES_CONFIGURATION_DELETE',$this->userRolesMergeToCheck))
        {
            if ($request->isXmlHttpRequest())
            {

                $subprojectDetails = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

                if (!$subprojectDetails) return new Response(json_encode(array('result' => 'error::noentity')));

                if ($id) {
                    $entity = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($id);
                }

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find SubProjectDatabaseLinkedProperties entity.');
                }

                $em->remove($entity);
                $em->flush();

                $entities = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->findAll();

                return new Response(json_encode(array('result' => 'ok', 'response' => $response)));

            }
        }
        else
        {
            return new Response(json_encode(array('result' => 'error::denied')));
        }
    }

}
