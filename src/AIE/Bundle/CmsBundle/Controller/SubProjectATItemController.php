<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\ActionTrackerItems;
use AIE\Bundle\CmsBundle\Form\SubProjectATItemType;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * SubProjectATItemController controller.
 *
 * @Route("/{projectId}/subprojectactiontrackeritems/{subprojectId}")
 */
class SubProjectATItemController extends CmsBaseController
{

    /**
     * Lists all SubProjectActionTrackers entities.
     *
     * @Route("/{aid}", name="cms_subproject_actiontrackeritems")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId, $subprojectId, $aid)
    {

        $em = $this->getManager();

        $statement = "SELECT e,
                            CASE WHEN e.status = 'Live' THEN 1 
                                 WHEN e.status = 'Close' THEN 2 
                                 WHEN e.status = 'Hold' THEN 3
                                 WHEN e.status = 'Cancelled' THEN 4 ELSE 0 END AS HIDDEN sortCondition
                      FROM
                            AIECmsBundle:ActionTrackerItems e
                            Where e.actionTracker = :aid
                      ORDER BY
                            sortCondition, e.duedate DESC";

        $query = $em->createQuery($statement)->setParameter('aid', $aid);

        $actionTrackerItems = $query->getResult();

        $actionTracker = $em->getRepository('AIECmsBundle:ActionTracker')->find($aid);

        $actionItemEditGranted = 0;
        $actionItemDeleteGranted = 0;
        $actionItemAddGranted = 0;

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_EDIT',$this->userRolesMergeToCheck)){
            $actionItemEditGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_DELETE',$this->userRolesMergeToCheck)){
            $actionItemDeleteGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_ADD',$this->userRolesMergeToCheck)){
            $actionItemAddGranted = 1;
        }

        return array(
            'projectId'                   => $projectId,
            'subprojectId'                => $subprojectId,
            'aid'                         => $aid,
            'actionTrackerItems'          => $actionTrackerItems,
            'wizard_title'                => $actionTracker->getTitle(),
            'actionItemEditGranted'       => $actionItemEditGranted,
            'actionItemDeleteGranted'     => $actionItemDeleteGranted,
            'actionItemAddGranted'        => $actionItemAddGranted,
        );

    }

    /**
     * Create
     *
     * @Route("/create", name="cms_subproject_actiontrackeritems_create")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request, $projectId, $subprojectId)
    {
        $aid = $request->get("aid");
        $em = $this->getDoctrine()->getManager('cms');
        $entity = new ActionTrackerItems();
        $form = $this->createActionTrackerItemForm($projectId, $subprojectId,$aid);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $actionTracker = $em->getRepository('AIECmsBundle:ActionTracker')->find($aid);
            $entity->setActionTracker($actionTracker);
            $entity->setItem($data->getItem());
            $entity->setComments($data->getComments());
            $entity->setDetails($data->getDetails());
            $entity->setPersons($data->getPersons());
            $entity->setResponsibleGroup($data->getResponsibleGroup());
            $entity->setDuedate(new \DateTime($data->getDuedate()));
            $entity->setFirstDeferral(new \DateTime($data->getDuedate()));
            $entity->setSeconddeferral(new \DateTime($data->getDuedate()));
            $entity->setStatus($data->getStatus());
       //     $entity->setTitle($data->getTitle());
          //  $entity->setDate(new \DateTime($data->getDate()));


            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', ' Action tracker item created');

            return $this->redirect($this->generateUrl('cms_subproject_actiontrackeritems',array('projectId'=>$projectId,'subprojectId'=>$subprojectId, 'aid'=>$aid,)));
        }

        return array(
            'entity' => $entity, //why this is needed?
            'aid' => $aid,
            'form'   => $form->createView(),
        );
    }
    /**
     * edit
     *
     * @Route("/edit/{aitemid}", name="cms_subproject_actiontrackeritems_edit")
     * @Method("POST")
     * @Template()
     */
    public function editAction(Request $request, $projectId, $subprojectId, $aitemid)
    {
        $aid = $request->request->get('aid');
        $em = $this->getDoctrine()->getManager('cms');
        $entity = $em->getRepository('AIECmsBundle:ActionTrackerItems')->find($aitemid);
        if (!$entity) {
            throw $this->createNotFoundException(
                'No Action Tracker item found for id ' . $aitemid
            );
        }
        $form = $this->updateActionTrackerItemForm($projectId, $subprojectId, $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $actionTracker = $em->getRepository('AIECmsBundle:ActionTracker')->find($entity->getActionTracker()->getId());
            $entity->setActionTracker($actionTracker);
            $entity->setItem($data->getItem());
            $entity->setComments($data->getComments());
            $entity->setDetails($data->getDetails());
            $entity->setPersons($data->getPersons());
            $entity->setResponsibleGroup($data->getResponsibleGroup());
            $entity->setDuedate(new \DateTime($data->getDuedate()));
            $entity->setFirstDeferral(new \DateTime());
            $entity->setSeconddeferral(new \DateTime());
            $entity->setStatus($data->getStatus());
            $em->flush();

            $this->addFlash('success', ' Action Tracker Item updated');

            return $this->redirect($this->generateUrl('cms_subproject_actiontrackeritems',array('projectId'=>$projectId,'subprojectId'=>$subprojectId, 'aid'=>$entity->getActionTracker()->getId(),)));
        }

        return array(
            'entity' => $entity, //why this is needed?
            'aid' => $entity->getActionTracker()->getId(),
            'aitemid' => $aitemid,
            'form'   => $form->createView(),
        );
    }


    /**
     * Creates a form to create action tracker item entity.
     *
     * @param int $subprojectId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createActionTrackerItemForm($projectId, $subprojectId, $aid)
    {

        $em = $this->getDoctrine()->getManager('cms');

        $entity = new ActionTrackerItems();

        $form = $this->createForm(new SubProjectATItemType($entity), $entity, [
            'action' => $this->generateURL('cms_subproject_actiontrackeritems_create', ['projectId' =>$projectId,'subprojectId' => $subprojectId,'aid'=>$aid,]),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Creates a form to update action tracker item entity.
     *
     * @param int $subprojectId
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function updateActionTrackerItemForm($projectId, $subprojectId, $entity)
    {

        $em = $this->getDoctrine()->getManager('cms');
        $form = $this->createForm(new SubProjectATItemType($entity), $entity, [
            'action' => $this->generateURL('cms_subproject_actiontrackeritems_edit', ['projectId' =>$projectId,'subprojectId' => $subprojectId,'aid'=>$entity->getActionTracker()->getId(),'aitemid'=>$entity->getId()]),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Delete
     *
     * @Route("/delete/{aitemid}", name="cms_subproject_actiontrackeritems_delete")
     * @Method("GET")
     * @Template()
      */
    public function deleteAction(Request $request, $aitemid, $projectId, $subprojectId)
    {
        $em = $this->getDoctrine()->getManager('cms');
        $actiontrackeritem = $em->getRepository('AIECmsBundle:ActionTrackerItems')->find($aitemid);
        if (!$actiontrackeritem) {
            throw $this->createNotFoundException(
                'No Action Tracker found for id ' . $aitemid
            );
        }
       ;
        $em->remove($actiontrackeritem);
        $em->flush();
        $this->addFlash('success', 'Action tracker item deleted');
       return $this->redirect($this->generateUrl('cms_subproject_actiontrackeritems', array('projectId' => $projectId, 'subprojectId' => $subprojectId, 'aid' =>  $actiontrackeritem->getActionTracker()->getId(),)));
    }

}
