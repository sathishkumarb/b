<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\Projects;
use AIE\Bundle\CmsBundle\Form\ProjectsEditType;
use AIE\Bundle\CmsBundle\Form\ProjectsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Projects controller.
 *
 * @Route("/projects")
 */
class ProjectsController extends CmsBaseController
{
    protected $grantedShow =0;
    protected $grantedEdit =0;
    protected $grantedDelete =0;
    /**
     * Lists all Projects entities.
     *
     * @Route("/", name="cms_projects")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        //$entities = $this->getGrantedProjects();

        //$entities = $em->getRepository('AIECmsBundle:Projects')->findBy(array(), array('name' => 'ASC'));

        $entities = $this->getGrantedProjects();

        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );



        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
            $this->grantedShow=1;
        }
        else{
            return $this->render(
                'AIEVeracityBundle:Default:accessdenied.html.twig'
            );
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_EDIT',$this->userRolesMergeToCheck)) {
            $this->grantedEdit=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_DELETE',$this->userRolesMergeToCheck)) {
            $this->grantedDelete=1;
        }

        return [
            'entities' => $entities,
            'pagination' => $pagination,
            'grantedShow' => $this->grantedShow,
            'grantedEdit' => $this->grantedEdit,
            'grantedDelete' => $this->grantedDelete,

        ];
    }

    /**
     * Creates a new Projects entity.
     *
     * @Route("/", name="cms_projects_create")
     * @Method("POST")
     * @Template("AIECmsBundle:Projects:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Projects();
        //$type = $request->request->get('aie_bundle_cmsbundle_projects')['type'];

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();
            $file = $data->getFile();
            $uploadedPath = $this->uploadFile($file, $entity);
            $entity->setFilepath($uploadedPath);
            $entity->setFolder(md5(uniqid(rand(), true)));

            /*  */
            $entity->setDateCreate(new \DateTime("now"));

            $securityContext = $this->get('security.context');
            $token = $securityContext->getToken();
            $user = $token->getUser();
            $userId = $user->getId();
            $entity->setCreatedBy($userId);


            /*  */
            $em = $this->getManager();


            /* Add default Class, threats and design codes */
            // $class = $em->getRepository('AIECmsBundle:Classifications')->findAll();
            // $threats = $em->getRepository('AIECmsBundle:Threat')->findAll();
            // $designCodes = $em->getRepository('AIECmsBundle:DesignCode')->findAll();


            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'New project '.$entity->getName().' created');

            return $this->redirect($this->generateUrl('cms_projects'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Projects $entity)
    {
        $form = $this->createForm(
            new ProjectsType(),
            $entity,
            [
                'action' => $this->generateUrl('cms_projects_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Projects entity.
     *
     * @Route("/new", name="cms_projects_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Projects();
        $form = $this->createCreateForm($entity);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}", name="cms_list_subprojects")
     * @Method("GET")
     * @Template()
     */
    public function listsubprojectsAction($id)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($id);

        if (!$this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN',$this->userRolesMergeToCheck))
        {
            //$entities = $this->getGrantedProjects();
            $userId = $this->get('security.context')->getToken()->getUser()->getId();

            $usergroupentities = $em->getRepository('AIECmsBundle:UserProjectGroup')->findBy(array('user' => $userId, 'project' => $id));
            foreach ($usergroupentities as $usergroup) {
                $ids[] = $usergroup->getSubProject()->getId();
            }
            $entities = $em->getRepository('AIECmsBundle:SubProjects')->findBy(array('id' => $ids));
        }
        else
        {
            $entities = $em->getRepository('AIECmsBundle:SubProjects')->findByProject($id);
        }

        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );

        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
            $this->grantedShow=1;
        }
        else{
            return $this->render(
                'AIEVeracityBundle:Default:accessdenied.html.twig'
            );
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_EDIT',$this->userRolesMergeToCheck)) {
            $this->grantedEdit=1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_DELETE',$this->userRolesMergeToCheck)) {
            $this->grantedDelete=1;
        }

        return [
            'entity' => $entity,
            'entities' => $entities,
            'pagination' => $pagination,
            'grantedShow' => $this->grantedShow,
            'grantedEdit' => $this->grantedEdit,
            'grantedDelete' => $this->grantedDelete,
        ];

    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/edit", name="cms_projects_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Projects $entity)
    {
        $form = $this->createForm(
            new ProjectsEditType(),
            $entity,
            [
                'action' => $this->generateUrl('cms_projects_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to delete a Projects entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_projects_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/show", name="cms_projects_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="cms_projects_update")
     * @Method("PUT")
     * @Template("AIECmsBundle:Projects:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIECmsBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            /* Handle Uploaded File */
            $data = $editForm->getData();
            $file = $data->getFile();
            $uploadedPath = $this->replaceFile($file, $entity);
            $entity->setFilepath($uploadedPath);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Project '.$entity->getName().' updated');

            return $this->redirect($this->generateUrl('cms_projects'));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a Projects entity.
     *
     * @Route("/{id}/delete", name="cms_projects_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIECmsBundle:Projects')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Projects entity.');
            }
            $subentities = $em->getRepository('AIECmsBundle:SubProjects')->findBy(array('project'=>$id));
            $selected = false;
            foreach($subentities as $subentity){
                $selected = true;
            }

            if ($selected == true){
                $this->addFlash('warning', 'Project cannot be deleted, since the sub project has been associated!');
                return $this->redirect($this->generateUrl('cms_projects',['id' => $id]));
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);
            $em->flush();
            $this->addFlash('success', 'Project deleted');
        }

        return $this->redirect($this->generateUrl('cms_projects'));
    }
}
