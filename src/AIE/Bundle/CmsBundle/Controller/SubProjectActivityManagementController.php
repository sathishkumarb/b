<?php

namespace AIE\Bundle\CmsBundle\Controller;

use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use AIE\Bundle\CmsBundle\Entity\ActionOwners;

use AIE\Bundle\CmsBundle\Form\ActivityPlanningType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivitiesOwnerType;
use AIE\Bundle\CmsBundle\Form\SubProjectActivityMitigationsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints\DateTime;

/**
 * SubProjectActivities controller.
 *
 * @Route("/{projectId}/{subprojectId}/activitymanagement")
 */
class SubProjectActivityManagementController extends CmsBaseController {

    Protected $mitigationStatusSaveGranted = 0;
    Protected $mitigationStatusFilesGranted = 0;
    Protected $mitigationStatusTrendGranted = 0;
    Protected $mitigationStatusManagementKpiGranted = 0;
    Protected $mitigationStatusControlKpiGranted = 0;
    Protected $mitigationStatusLastDueDateGranted = 0;
    Protected $activityManagementPlanningGenerateGranted = 0;
    Protected $activityManagementRemindersGenerateGranted = 0;

    /**
     * Lists all Sub Project Activity Management entities.
     *
     * @Route("", name="cms_subproject_activitymanagement")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(){
        return true;
    }

    /**
     * Finds and displays a activityinspectionowners entity.
     *
     * @Route("/inspectionowners", name="cms_subproject_activityinspectionowners")
     * @Method("GET")
     * @Template("AIECmsBundle:SubProjectActivityManagement:activityinspectionowners.html.twig")
     */
    public function activityinspectionownersAction($projectId, $subprojectId)
    {
        $em = $this->getManager();

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId,1);

        $actionowners = $subproject->getActionOwners();
        $activitiesAction = array();
        $databaseactivitiesCounter = array();

        foreach ($systems as $system)
        {
            $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId());
            if ($activitiesAction[$system->getId()]) {

                foreach ($activitiesAction[$system->getId()] as $s) {
                    if ($s->getId()) {
                        if (isset($databaseactivitiesCounter[$system->getId() . '_' . $s->getId()])) {
                            $databaseactivitiesCounter[$system->getId() . '_' . $s->getId()]++;
                        } else {
                            $databaseactivitiesCounter[$system->getId() . '_' . $s->getId()] = 1;
                        }
                    }

                }
            }
        }

        $form = $this->createInspectionOwnerForm($projectId, $subprojectId, $systems, $activitiesAction, $actionowners);

        return array(
            'systems'                => $systems,
            'wizard_title'           => 'Activities',
            'activitiesAction'       => $activitiesAction,
            'databaseactivitiesCounter' => $databaseactivitiesCounter,
            'projectId'           => $projectId,
            'subprojectId'           => $subprojectId,
            'form'                   => $form->createView()
        );
    }

    /**
     *
     * @Route("/inspectionsupdate", name="cms_subproject_activityinspectionowners_update")
     * @Method("POST")
     */
    public function updateActivityInspections(Request $request, $projectId, $subprojectId)
    {

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITIES_EDIT',$this->userRolesMergeToCheck))
        {
            $em = $this->getManager();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId, 1);
            $actionowners = $subproject->getActionOwners();
            $activitiesAction = array();

            foreach ($systems as $system) {
                $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId());

            }

            $form = $this->createInspectionOwnerForm($projectId, $subprojectId, $systems, $activitiesAction, $actionowners);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                foreach ($data as $k => $d) {
                    $attr = explode('_', $k); //1=>SubProjectActivity

                    $stm = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($attr[1]);
                    if ($stm) {
                        $stm->setActionOwner($d->getActionOwner());
                    }
                }
                $em->flush();
            }

            return $this->redirect($this->generateUrl('cms_subproject_activityinspectionowners', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }
        else
        {
            $this->addFlash('warning', ' You are not permitted to perform this action.');
            return $this->redirect($this->generateUrl('cms_subproject_activityinspectionowners', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }
    }

    /**
     * @param $activitiesAction
     * @param $projectId
     * @param $subprojectId
     * @param $systems
     * @param $activitiesAction
     * @param $actionowners
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInspectionOwnerForm($projectId, $subprojectId, $systems, $activitiesAction, $actionowners)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subproject_activityinspectionowners_update', array('projectId' => $projectId,'subprojectId' => $subprojectId)))
            ->setMethod('POST');

        foreach ($systems as $system)
        {
            foreach ($activitiesAction[$system->getId()] as $sma)
            {
                $form->add('spa_' . $sma->getId(), new SubProjectActivitiesOwnerType($sma, $subprojectId, $actionowners));
            }
        }
        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Lists all Activity Mitigation Status entities.
     *
     * @Route("/mitigationstatus", name="cms_subproject_activitymitigationstatus")
     * @Method("GET")
     * @Template()
     */
    public function mitigationstatusAction(Request $request, $projectId, $subprojectId) {
        $em = $this->getManager();

        $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId,1);

        $files = $subproject->getFiles();
        $activitiesAction = array();
        $filterssystems = array();
        $databaseactivitiesCounter = array();

        foreach ($systems as $system)
        {

            $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId());
            if ($activitiesAction[$system->getId()]) {

                foreach ($activitiesAction[$system->getId()] as $s) {
                    if ($s->getId()) {
//                        if ($s->getIsManagement() == 1 && $s->getIsControl() ==1 && $s->getIsFlowAssuranceManagement() ==1 && $s->getIsFlowAssuranceControl() ==1 ) {

                        if (isset($databaseactivitiesCounter[$system->getId() . '_' . $s->getId()])) {
                            $databaseactivitiesCounter[$system->getId() . '_' . $s->getId()]++;
                        } else {
                            $databaseactivitiesCounter[$system->getId() . '_' . $s->getId()] = 1;
                        }
//                        }
                    }

                }

                $filterssystems[] = $em->getRepository('AIECmsBundle:SubProjectSystems')->find($system->getId());
            }
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_SAVE',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusSaveGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_FILES',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusFilesGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_TRENDS_SHOW',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusTrendGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_MANAGEMENT_KPI',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusManagementKpiGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_CONTROL_KPI',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusControlKpiGranted = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_LAST_DUE_DATE',$this->userRolesMergeToCheck)) {
            $this->mitigationStatusLastDueDateGranted = 1;
        }

        $elementPermissions = array(
            'mitigationStatusFilesGranted'   => $this->mitigationStatusFilesGranted,
            'mitigationStatusManagementKpiGranted'   => $this->mitigationStatusManagementKpiGranted,
            'mitigationStatusControlKpiGranted'   => $this->mitigationStatusControlKpiGranted,
            'mitigationStatusLastDueDateGranted'   => $this->mitigationStatusLastDueDateGranted,
        );

        $form = $this->createMitigationStatusForm($projectId, $subprojectId, $systems, $activitiesAction, $files, $elementPermissions);


        usort($filterssystems, function($a, $b) {
            if($a->getSystemstitle() == $b->getSystemstitle()) {
                return 0;
            }
            return ($a->getSystemstitle() < $b->getSystemstitle()) ? -1 : 1;
        });

        return array(
            'systems'                => $filterssystems,
            'wizard_title'           => 'Mitigation Status',
            'activitiesAction'       => $activitiesAction,
            'databaseactivitiesCounter' => $databaseactivitiesCounter,
            'projectId'              => $projectId,
            'subprojectId'           => $subprojectId,
            'form'                   => $form->createView(),
            'mitigationStatusTrendGranted'   => $this->mitigationStatusTrendGranted,
            'mitigationStatusSaveGranted'   => $this->mitigationStatusSaveGranted,
            'systemId' => $request->query->get('systemId'),
        );
    }

    /**
     * Creates a new Sub Project Activity Management entity.
     *
     * @Route("/activitymitigationstatus_update", name="cms_subproject_activitymitigationstatus_update")
     * @Method("POST")
     * @Template("AIECmsBundle:SubProjectActivityManagement:mitigationstatus.html.twig")
     */
    public function updateMitigationstatusAction(Request $request, $projectId, $subprojectId) {

        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_SAVE',$this->userRolesMergeToCheck)) {

            $em = $this->getManager();

            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);

            $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId, 1);

            $files = $subproject->getFiles();

            $activitiesAction = array();
            foreach ($systems as $system) {
                $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId());
            }

            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_FILES', $this->userRolesMergeToCheck)) {
                $this->mitigationStatusFilesGranted = 1;
            }

            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_MANAGEMENT_KPI', $this->userRolesMergeToCheck)) {
                $this->mitigationStatusManagementKpiGranted = 1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_CONTROL_KPI', $this->userRolesMergeToCheck)) {
                $this->mitigationStatusControlKpiGranted = 1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_LAST_DUE_DATE', $this->userRolesMergeToCheck)) {
                $this->mitigationStatusLastDueDateGranted = 1;
            }

            $elementPermissions = array(
                'mitigationStatusFilesGranted' => $this->mitigationStatusFilesGranted,
                'mitigationStatusManagementKpiGranted' => $this->mitigationStatusManagementKpiGranted,
                'mitigationStatusControlKpiGranted' => $this->mitigationStatusControlKpiGranted,
                'mitigationStatusLastDueDateGranted' => $this->mitigationStatusLastDueDateGranted,
            );

            $form = $this->createMitigationStatusForm($projectId, $subprojectId, $systems, $activitiesAction, $files, $elementPermissions);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                foreach ($data as $k => $d) {
                    $attr = explode('_', $k); //1=>sectionsThreatsMitigation

                    $stm = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($attr[1]);
                    if ($stm) {

                        $stm->setJustificationManagement($d->getJustificationManagement());
                        $stm->setJustificationControl($d->getJustificationControl());
                        $stm->setKpiManagement($d->getKpiManagement());
                        $stm->setKpiControl($d->getKpiControl());
                        $date = new \DateTime();
                        if ($d->getLastInspectionDate()) {
                            $date = \DateTime::createFromFormat('d M Y', $d->getLastInspectionDate());
                            $date->setTime(0, 0, 0);
                        }
                        $stm->setLastInspectionDate($date);
                    }
                }
                $em->flush();
            }

            return $this->redirect($this->generateUrl('cms_subproject_activitymitigationstatus', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }
        else
        {
            $this->addFlash('warning', ' You are not permitted to perform this action.');
            return $this->redirect($this->generateUrl('cms_subproject_activitymitigationstatus', array('projectId' => $projectId, 'subprojectId' => $subprojectId)));
        }
    }

    /**
     * @param $activitiesAction
     * @param $subprojectId
     * @param $systems
     * @param $activitiesAction
     * @param $actionowners
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createMitigationStatusForm($projectId, $subprojectId, $systems, $activitiesAction, $files, $permissions)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_subproject_activitymitigationstatus_update', array('projectId' => $projectId,'subprojectId' => $subprojectId)))
            ->setMethod('POST');

        foreach ($systems as $system)
        {
            foreach ($activitiesAction[$system->getId()] as $sma)
            {
                $form->add('spa_' . $sma->getId(), new SubProjectActivityMitigationsType($sma, $files, $permissions));
            }
        }
        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Activity Planning Listing.
     *
     * @Route("/planning", name="cms_subproject_activityplanning")
     * @Method({"GET", "POST"})
     * @Template("AIECmsBundle:SubProjectActivityManagement:activity_planning.html.twig")
     */
    public function activityPlanningAction(Request $request, $projectId, $subprojectId)
    {
        $em = $this->getManager();
        $from_date = new \DateTime('now');
        $to_date = clone $from_date;
        $to_date->modify('+1 month');
        $to_date->modify('+1 day');

        $filter_form = $this->createActivityFilterForm($projectId, $subprojectId, $from_date, $to_date, 'cms_subproject_activityplanning');

        $filter_form->handleRequest($request);

        if ($filter_form->isValid())
        {
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];

            $from_date = new \DateTime($filter_data['from_date']);
            $to_date = new \DateTime($filter_data['to_date']);
            $to_date->modify('+1 day');
        }

        $subprojects = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId,1);

        $activitiesAction = array();
        $activityInfo= array();
        //        $systemsCounter = array();
        //        $systemActivitiesCounter = array();
        //        $mitigationAction = array();
        $cmsDatabasesHelper = $this->get('aie_cms.cmsdatabases.helper');

        if ($subprojects && $systems) {

        foreach ($systems as $system) {
            $activitiesAction[$system->getId()] = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojects->getId(), $system->getId(), false);

            if ($activitiesAction[$system->getId()]) {

                foreach ($activitiesAction[$system->getId()] as $index => $subProjectActivity) {
                    if ($subProjectActivity->getId()) {
                        $checkActivity = $cmsDatabasesHelper->getSystemsFromDatabasesPerActivity($subProjectActivity->getId());

                        if (!empty($checkActivity) && $subProjectActivity->getNextInspectionDate() && $checkActivity['bs_isselected'] == 1 && $checkActivity['bt_isselected'] == 1 && $checkActivity['bmt_isselected'] == 1) {

                            if ($subProjectActivity->getNextInspectionDate()) {
                                $nextDate = $subProjectActivity->getNextInspectionDate(true);
                                $nextDate_U = (int)$nextDate->format('U');

                                $current_date = new \DateTime("now");
                                $current_date_U = (int)$current_date->format('U');
                                //set repeated action last time to next date
                                $i = 0;

                                while ($nextDate_U <= (int)$to_date->format('U')) {

                                    if ($nextDate_U >= (int)$from_date->format('U')) {

                                        $activityInfo[$system->getId()][$subProjectActivity->getId()][$index]['activityId'] = $subProjectActivity->getId();
                                        $activityInfo[$system->getId()][$subProjectActivity->getId()][$index]['systemId'] = $system->getId();
                                        $activityInfo[$system->getId()][$subProjectActivity->getId()]['systems'][$i] = $system->getSystemsTitle();

                                        if ($subProjectActivity->getIsmanagement() || $subProjectActivity->getIsflowassuranceManagement()) {
                                            $activityInfo[$system->getId()][$subProjectActivity->getId()]['activityname'][$i] = $subProjectActivity->getManagementtitle();
                                        } elseif ($subProjectActivity->getIsControl() || $subProjectActivity->getIsflowassuranceControl()) {
                                            $activityInfo[$system->getId()][$subProjectActivity->getId()]['activityname'][$i] = $subProjectActivity->getControltitle();
                                        } else
                                            $activityInfo[$system->getId()][$subProjectActivity->getId()]['activityname'][$i] = $subProjectActivity->getControltitle();

//                                                $subProjectActivity->getIsmanagement() ? $subProjectActivity->getManagementtitle() : $subProjectActivity->getControltitle();
//                                            $subProjectActivity->getControltitle().'='.$subProjectActivity->getManagementtitle();

                                        $activityInfo[$system->getId()][$subProjectActivity->getId()]['frequency'][$i] = $subProjectActivity->getFrequency();
                                        $activityInfo[$system->getId()][$subProjectActivity->getId()]['actionowner'][$i] = (!empty($subProjectActivity->getActionOwner()) ? $subProjectActivity->getActionOwner()->getName() : "");


                                        $activityInfo[$system->getId()][$subProjectActivity->getId()]['duedate'][$i] = $nextDate;

                                    }

                                    $subProjectActivity->setLastInspectionDate($nextDate);

                                    //set next date from repeated action
                                    $nextDate = $subProjectActivity->getNextInspectionDate(true);
                                    $nextDate_U = (int)$nextDate->format('U');
                                    $i++;


                                }
                            }

                        }
                    }
                }
            }
        }

        }


        $to_date->modify('-1 day');
        $filter_form = $this->createActivityFilterForm($projectId, $subprojectId, $from_date, $to_date, 'cms_subproject_activityplanning');

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_PLANNING_GENERATE', $this->userRolesMergeToCheck)) {
            $this->activityManagementPlanningGenerateGranted = 1;
        }

        return array(
            'systems'                => $systems,

            'subprojectId'           => $subprojectId,
            'to_date'                   => $to_date,
//            'period'                 => $period,
            'systemActions'                => $activityInfo,
            'filter_form'            => $filter_form->createView(),
            'activityManagementPlanningGenerateGranted' => $this->activityManagementPlanningGenerateGranted,
        );
    }

    /**
     * @param $projectId, $subprojectId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createActivityFilterForm($projectId, $subprojectId, \DateTime $from_date, \DateTime $to_date, $submit_form)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($submit_form, array('projectId' => $projectId, 'subprojectId' => $subprojectId)))
            ->setMethod('POST');

        $form->add('filter_form', new ActivityPlanningType($from_date, $to_date), ['label' => ' ']);

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate', 'attr' => ['class' => 'right', 'style' => 'margin-right: 30px;']), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Reminders Listing.
     *
     * @Route("/reminders", name="cms_subproject_activity_reminders")
     * @Method({"GET", "POST"})
     * @Template("AIECmsBundle:SubProjectActivityManagement:activity_reminders.html.twig")
     */
    public function remindersAction(Request $request, $projectId, $subprojectId)
    {
        return $this->filterActions($request, $projectId, $subprojectId, 'cms_subproject_activity_reminders') + ['wizard_title' => 'Reminders'];
    }

    public function filterActions(Request $request, $projectId, $subprojectId, $submitForm)
    {

        $em = $this->getManager();
        $from_date = new \DateTime('now');
        $to_date = clone $from_date;
        $to_date->modify('+1 month');

        $filter_form = $this->createActivityFilterForm($projectId, $subprojectId, $from_date, $to_date, $submitForm);

        $filter_form->handleRequest($request);

        if ($filter_form->isValid())
        {
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];

            $from_date = new \DateTime($filter_data['from_date']);
            $to_date = new \DateTime($filter_data['to_date']);
            $to_date->modify('+1 day');
        }

        $subprojects = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
        $systems = $em->getRepository('AIECmsBundle:SubProjectSystems')->getSubProjectSelectedSystems($subprojectId,1);

        $activitiesAction = array();
        $systemsCounter = array();
        $systemActivitiesCounter = array();
        $mitigationAction = array();

        foreach ($systems as $system)
        {
            $stm = $em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivitiesForSystem($subprojectId, $system->getId());
            $systemsCounter[$system->getId()] = 0;

            $activitiesAction = [];
            $next_inspection_date_U = 0;

            foreach ($stm as $key => $s)
            {
                if ($s->getNextInspectionDate()) $next_inspection_date_U = (int)$s->getNextInspectionDate()->format('U');

                if($s->getFrequency() == 0){
                    //if continues action
                    unset($stm[$key]);
                    continue;
                }

                if (
                    ((int)$next_inspection_date_U < $from_date->format('U') || $next_inspection_date_U > (int)$to_date->format('U')) //filter between from and to dates
                    &&
                    (! $s->getIsInspectionDue() || (int)$next_inspection_date_U > $to_date->format('U')) //overdue activities up to "to_date"
                )
                {
                    unset($stm[$key]);
                    continue;
                }

                $action = $s->getControltitle();

                //echo $action;
                if (isset($activitiesAction[$action])) //if mitigation action is duplicated in same section
                    //take lowest freq
                {

                    $index = $activitiesAction[$action];
                    if (isset($stm[$index]) && $stm[$index]->getFrequency() < $s->getFrequency())
                    {
                        unset($stm[$key]);
                    } else
                    {
                        unset($stm[$index]);
                    }

                    continue;

                } else
                {
                    $activitiesAction[$action] = $key;
                }

                if (isset($systemActivitiesCounter[$system->getId() . '_' . $s->getId()]))
                {
                    $systemActivitiesCounter[$system->getId() . '_' . $s->getId()] ++;
                } else
                {
                    $systemActivitiesCounter[$system->getId() . '_' . $s->getId()] = 1;
                }

                $stm[$key]->setSystems($system->getSystemstitle());

                $systemsCounter[$system->getId()] ++;
            }


            $mitigationAction[$system->getId()] = $stm;
        }
        $repeatedActions = [];
        foreach ($mitigationAction as $systemId => $actions)
        {
            foreach ($actions as $action)
            {
                $repeatedAction = clone $action;

                if($repeatedAction->getFrequency() == 0){
                    continue;
                }

                $nextDate = $repeatedAction->getNextInspectionDate(true);
                $repeatedAction->setLastInspectionDate($nextDate);
                $nextDate = $repeatedAction->getNextInspectionDate(true);
                if ($nextDate) $nextDate_U = (int)$nextDate->format('U');

                while ($nextDate_U >= (int)$from_date->format('U') && $nextDate_U <= (int)$to_date->format('U'))
                {
                    //add the repeated action to actions list
                    if (! isSet($repeatedActions[$systemId]))
                    {
                        $repeatedActions[$systemId] = [];
                    }
                    $repeatedActions[$systemId][] = clone $repeatedAction;

                    //set repeated action last time to next date
                    $repeatedAction->setLastInspectionDate($nextDate);
                    //set next date from repeated action
                    $nextDate = $repeatedAction->getNextInspectionDate(true);
                    $nextDate_U = (int)$nextDate->format('U');
                }
            }
        }


        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($from_date, $interval, $to_date);

        $sortedActions = Helper\ArrayHelper::array_flatten($mitigationAction);

        $repeatedActions = Helper\ArrayHelper::array_flatten($repeatedActions);

        $sortedActions = array_merge($sortedActions, $repeatedActions);
        //echo count($sortedActions);exit();
        usort($sortedActions, array('AIE\Bundle\CmsBundle\Entity\SubProjectActivity', 'nextDateSort'));

        $actions = [];

        /*foreach ($period as $dt)
        {   $actions[$dt->format('d F Y')] = [];
        }*/

        foreach ($sortedActions as $action)
        {

            $nextDate = $action->getNextInspectionDate();
            if ($nextDate)
            {
                if ((int)$nextDate->format('U') > (int)$to_date->format('U')) {
                    continue;
                }

                $date = $nextDate->format('d F Y');

                if (! isSet($actions[$date]))
                {
                    $actions[$date] = [];
                }

                $actions[$date][] = $action;
            }
        }

        $to_date->modify('-1 day');
        $filter_form = $this->createActivityFilterForm($projectId, $subprojectId, $from_date, $to_date, 'cms_subproject_activity_reminders');

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_REMINDERS_GENERATE', $this->userRolesMergeToCheck)) {
            $this->activityManagementRemindersGenerateGranted = 1;
        }

        return array(
            // 'entities' => $entities,
            'systems'               => $systems,
            'activitiesAction'       => $activitiesAction,
            'systemActivitiesCounter' => $systemActivitiesCounter,
            'systemsCounter'         => $systemsCounter,
            'subprojectId'             => $subprojectId,
            'period'                 => $period,
            'actions'                => $actions,
            'filter_form'            => $filter_form->createView(),
            'activityManagementRemindersGenerateGranted' => $this->activityManagementRemindersGenerateGranted,
        );
    }

}
