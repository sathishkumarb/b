<?php

namespace AIE\Bundle\CmsBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class MenuBuilder
{

    protected $ccmUserGroupRoles;
    protected $userGroupRoles;
    protected $securityHelper;
    protected $loggedInUser;

    protected $pID;
    protected $spID;

    private $factory;
    private $securityContext;
    private $securityAuthorizationChecker;
    private $entityManager;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, $em, $securityTokenStorage, $securityAuthorizationChecker)
    {
        $this->factory = $factory;
        $this->securityContext = $securityTokenStorage;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
        $this->entityManager = $em;

        global $kernel;

        /* Set Roles based on anomaly project groups user associated */
        $token = $kernel->getContainer()->get('security.context')->getToken();

        $this->loggedInUser = $token->getUser();


        if ($kernel->getContainer()->has('request')) {
            $request = $kernel->getContainer()->get('request');
        } else {
            $request = Request::createFromGlobals();
        }
        $pID = null;
        $spID = null;

        $request = $kernel->getContainer()->get('request');
        $routeName = $request->get('_route');

        if ($request->get('projectId') && $request->get('subprojectId', $request->get('id'))){
            $pID = $request->get('projectId');
            $spID = $request->get('subprojectId', $request->get('id'));
        }  else if ($request->get('projectId', $request->get('id')) && (substr_count($routeName, 'cms_projects') || substr_count($routeName, 'cms_subprojects') || substr_count($routeName, 'cms_list_subprojects'))){
            $pID = $request->get('projectId', $request->get('id'));
        }
    
         $this->ccmUserGroupRoles = $kernel->getContainer()->get('aie_cms.user.helper')->getCcmGroupRoles($this->loggedInUser->getId(), $pID);
        
         $this->userGroupRoles = [];

         //if no user role selected user role from users
         if (!$this->ccmUserGroupRoles)
         {
            $this->ccmUserGroupRoles = $kernel->getContainer()->get('aie_cms.user.helper')->getSuperAdmin($this->loggedInUser->getId());
            $this->userGroupRoles = unserialize($this->ccmUserGroupRoles['roles']);
         }
         else
         {

             if (count($this->ccmUserGroupRoles) > 1)
             {
                 foreach( $this->ccmUserGroupRoles as $roles)
                 {
                     $this->userGroupRoles = array_merge( $this->userGroupRoles, unserialize($roles->getRoles()) );
                 }
             }
             else
             {
                 $this->userGroupRoles = (count($this->ccmUserGroupRoles) ? unserialize($this->ccmUserGroupRoles[0]->getRoles()) : []);
             }
         }
 
         $this->securityHelper = $kernel->getContainer()->get('aie_cms.user.helper');

    }

    public function createMainLeftMenu(Request $request)
    {

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu->setChildrenAttribute('id', 'top-nav');

		$menu->addChild('Home', array('route' => "index"));
        $menu->addChild('Dashboard', ['route' => "cms_index"]);
        $menu->addChild('Projects', ['route' => 'cms_projects']);

        $routeName = $request->get('_route');

        if ($request->get('projectId') && $request->get('subprojectId', $request->get('id'))){
            $this->pID = $request->get('projectId');
            $this->spID = $request->get('subprojectId', $request->get('id'));
            $this->createSubProjectTopMenu($menu, $this->pID, $this->spID, $dropdown = false);
        }  else if ($request->get('projectId', $request->get('id')) && (substr_count($routeName, 'cms_projects') || substr_count($routeName, 'cms_subprojects') || substr_count($routeName, 'cms_list_subprojects'))){
            $this->pID = $request->get('projectId', $request->get('id'));
            $this->createProjectTopMenu($menu, $this->pID, $dropdown = false);
        }

        return $menu;
    }

    private function createSubProjectTopMenu(&$menu, $projectId, $subprojectId, $dropdown = false)
    {
        $project = $this->entityManager->getRepository('AIECmsBundle:Projects')->find($projectId);
        $subproject = $this->entityManager->getRepository('AIECmsBundle:SubProjects')->findOneBy(array('project' => $projectId, 'id' => $subprojectId));

        $projectMenu = $menu->addChild(
            'Project',
            [
                'label' => $project->getName(),
                'route' => 'cms_list_subprojects',
                'routeParameters' => ['id' => $projectId]
            ]
        );
        if ($subproject) {

            $projectMenu = $menu->addChild(
                'Sub Project',
                [
                    'label' => $subproject->getName(),
                    'route' => 'cms_subprojects_dashboard',
                    'routeParameters' => ['projectId' => $projectId, 'id' => $subproject->getId()]
                ]
            );
        }

        if ($dropdown) {
            $projectMenu->setAttribute('dropdown', true)->setAttribute('class', 'dropdown');
            $this->createSubProjectConfigMenu($projectMenu, $projectId, $subprojectId);
        }
    }

    private function createSubProjectConfigMenu(&$menu, $projectId, $subprojectId)
    {

        $subproject = $this->entityManager->getRepository('AIECmsBundle:SubProjects')->find(array('project'=>$projectId,'id'=>$subprojectId));

        $datachild = $menu->addChild(
            'Setup',
            array('uri' => '#'))
            ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $datachild->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_CONFIGURATION_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Matrix Configuration',
                ['route' => 'cms_subprojectmatrix', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRIX_SETUP_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Matrix Setup',
                ['route' => 'cms_subprojectmatrixsetup', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        $childdatabases = $datachild->addChild(
            'Databases',
            array('uri' => '#'))
            ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false');
        $childdatabases->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityHelper->isRoleGranted('ROLE_DATABASES',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Database Configuration',
                ['route' => 'cms_subprojectdatabase', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_SAMPLING_DATABASE_SHOW',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Sampling/Testing',
                ['route' => 'cms_subproject_samplingdatabase', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_CATHODIC_PROTECTION_DATABASE_SHOW',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Cathodic Protection',
                ['route' => 'cms_subproject_cathodicprotectiondatabase','routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_CHEMICAL_DATABASE_SHOW',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Chemical',
                ['route' => 'cms_subproject_chemicaldosagedatabase', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }
        if ($this->securityHelper->isRoleGranted('ROLE_COUPON_PROBE_DATABASE_SHOW',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Coupon/Probe',
                ['route' => 'cms_subproject_corrosionmonitoringdatabase', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }
		if ($this->securityHelper->isRoleGranted('ROLE_PRODUCTION_DATABASE_SHOW',$this->userGroupRoles)) {
            $childdatabasesSubmenu = $childdatabases->addChild(
                'Production',
                ['route' => 'cms_subproject_productiondatabase', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

//        $childdatabases = $datachild->addChild(
//            'Datasheets',
//            array('uri' => '#', 'extras' => array('icon' => 'globe'))
//        )->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item subnavheading hidden-xs hidden-sm');
//        $childdatabases->setChildrenAttribute('class', 'subnav')->setAttribute('dropdown', true);
//
//        $datachild->addChild(
//            'Pitting Corrosion',
//            ['route' => 'cms_subprojectdatabase', 'extras' => ['icon' => 'wrench'],'routeParameters' => ['projectId' => $projectId ,'subprojectId' => $subproject->getId()]]
//        )->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
//
//        $datachild->addChild(
//            'General Corrosion',
//            ['route' => 'cms_subprojectdatabase', 'extras' => ['icon' => 'wrench'],'routeParameters' => ['projectId' => $projectId ,'subprojectId' => $subproject->getId()]]
//        )->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');

        $childdatabases = $datachild->addChild(
            'Activities and Owners',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false');
        $childdatabases->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_SHOW',$this->userGroupRoles)) {
            $childactivitiesSubmenu = $childdatabases->addChild(
                'Action Owners',
                ['route' => 'cms_subproject_actionowners', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITIES_SHOW',$this->userGroupRoles)) {
            $childactivitiesSubmenu = $childdatabases->addChild(
                'Activities',
                ['route' => 'cms_subproject_activityinspectionowners', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        $datachild = $menu->addChild(
            'Operational Management',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $datachild->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityHelper->isRoleGranted('ROLE_DOCUMENTS_SHOW_SEARCH',$this->userGroupRoles)) {
            $datachild->addChild(
                'Documents',
                ['route' => 'cms_files', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MITIGATION_STATUS_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Mitigation Status',
                ['route' => 'cms_subproject_activitymitigationstatus', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        $datachild = $menu->addChild(
            'Activity Management',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $datachild->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_PLANNING_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Planning',
                ['route' => 'cms_subproject_activityplanning', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_REMINDERS_SHOW',$this->userGroupRoles)) {
            $datachild->addChild('Reminders', array('route' => 'cms_subproject_activity_reminders', 'routeParameters' => array('projectId' => $projectId, 'subprojectId' => $subproject->getId())));
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_MEETINGS_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Meetings',
                ['route' => 'cms_workshops','routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_SHOW',$this->userGroupRoles)) {
            $datachild->addChild(
                'Action Tracker',
                ['route' => 'cms_subproject_actiontracker', 'routeParameters' => ['projectId' => $projectId, 'subprojectId' => $subproject->getId()]]
            );
        }

        if ($this->securityHelper->isRoleGranted('ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_SHOW',$this->userGroupRoles)) {
            $datachild = $menu->addChild('Matrices and Reports', array('uri' => '#'))->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle', 'dropdown')
                ->setLinkAttribute('role', 'button')->setLinkAttribute('aria-haspopup', 'true')
                ->setLinkAttribute('aria-expanded', 'false')
                ->setLabelAttribute('class', 'caret');
            $datachild->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        }
        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_ARCHIVE_SHOW',$this->userGroupRoles)) {
            $datachild->addChild('Archive', array('route' => 'cms_subproject_archives', 'routeParameters' => array('projectId' => $projectId, 'subprojectId' => $subproject->getId())));
        }

        if ($this->securityHelper->isRoleGranted('ROLE_MATRICES_AND_REPORTS_APPROVED_SHOW',$this->userGroupRoles)) {
            $datachild->addChild('Approved', array('route' => 'cms_subproject_approved_archives', 'routeParameters' => array('projectId' => $projectId, 'subprojectId' => $subproject->getId())));
        }

        $datachild = $menu->addChild(
            'Real-time Data',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $datachild->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        $datachild->addChild(
            'Thickness Data',
            ['uri' => '#']
        );

        $datachild->addChild(
            'Probe Data',
            ['uri' => '#']
        );

        $datachild->addChild(
            'CP Data',
            ['uri' => '#']
        );

        $datachild->addChild(
            'Chemical Data',
            ['uri' => '#']
        );
        $datachild->addChild(
            'Production Data',
            ['uri' => '#']
        );

    }

    private function createProjectTopMenu(&$menu, $projectId, $dropdown = false)
    {

        $project = $this->entityManager->getRepository('AIECmsBundle:Projects')->find($projectId);

        $projectMenu = $menu->addChild(
            'Project',
            [
                'label' => $project->getName(),
                'route' => 'cms_list_subprojects',
                'routeParameters' => ['id' => $projectId]
            ]
        );

        if ($dropdown) {
            $projectMenu->setAttribute('dropdown', true)->setAttribute('class', 'dropdown');
            $this->createProjectLeftMenu($projectMenu, $projectId);
        }
    }

    private function createProjectLeftMenu(&$menu)
    {

        $menu->addChild(
            'List All',
            ['route' => 'cms_projects']
        );
        //if( $this->securityHelper->isRoleGranted('ROLE_PROJECT_ADD',$this->userGroupRoles) ){
        $menu->addChild(
            'Add New',
            ['route' => 'cms_projects_new']
        );

    }

    public function createMainRightMenu(Request $request)
    {

        $user = $this->securityContext->getToken()->getUser();

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        //$requests = $this->entityManager->getRepository('AIEAnomalyBundle:Request')->findByStatus(-1);

        $reqEntitiesApprovalOwners = [];
        $reqEntitiesDeferralOwners = [];

        $entities = [];

        $userId = $this->loggedInUser->getId();

        //$entities = $em->getRepository('AIEAnomalyBundle:Request')->findByStatus(-1);

        //         $deferralOwners = $this->entityManager->getRepository('AIEAnomalyBundle:Deferrals')->findBy(
        //     array('actionOwner' => $userId)
        // );

        //         $approvalOwners = $this->entityManager->getRepository('AIEAnomalyBundle:ApprovalOwners')->findBy(
        //     array('actionOwner' => $userId)
        // );

        //         if (!empty($approvalOwners)){

        //             $reqEntitiesApprovalOwners = $this->mergeUserRequests($approvalOwners,"A");
        //         }


        //         if (!empty($deferralOwners)){
        //              //$entities = $em->getRepository('AIEAnomalyBundle:Request')->findBy(array('status'=>-1,'project_id'=>$deferralOwner->getProject()->getId(),'requeststatus'=>'deferral'));

        //             $reqEntitiesDeferralOwners = $this->mergeUserRequests($deferralOwners,"D");

        //         }

        //         $requests = array_merge(
        //                 $reqEntitiesApprovalOwners,
        //                 $reqEntitiesDeferralOwners
        //             );

        //         $notificationExtras = [];
        //         if (count($requests) > 0) {
        //             $notificationExtras += ['badge' => count($requests)];
        //         }

        //         $menu->addChild(
        //             'Notifications',
        //             ['label' => 'Pending Requests', 'route' => 'cms_requests', 'extras' => $notificationExtras]
        //         );

        /* User Security */
        if ($this->securityAuthorizationChecker->isGranted('ROLE_SUPER_ADMIN') || $this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN',$this->userGroupRoles)) {
            $menu->addChild('Admin', ['label' => 'Admin Panel', 'route' => 'cms_basedatabase']);
        }

        $menu->addChild('User', ['label' => $user->getUsername()])
            ->setAttribute('dropdown', true)
            ->setAttribute('class', 'dropdown');

        $menu['User']->addChild('Profile', ['route' => 'fos_user_profile_show']);
        $menu['User']->addChild('Change Password', ['route' => 'fos_user_change_password'])->setAttribute(
            'divider_append',
            true
        );

        $menu['User']->addChild('Logout', ['route' => 'fos_user_security_logout']);

        return $menu;
    }

    public function createLeftNav(Request $request)
    {

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $projectId = $request->get('projectId', $request->get('id'));

        $subprojectId = $request->get('subprojectId', $request->get('id'));

        switch ($request->get('_route')) {
            case 'ccm_user_group_list':
            case 'ccm_user_group_show':
            case 'ccm_user_group_new':
            case 'ccm_user_group_edit':
            case 'ccm_user_group_delete':
            case 'ccm_user_group':
            case 'ccm_userprojectgroup_show':
            case 'ccm_userprojectgroup_edit':
            case 'ccm_userprojectgroup_new':
            case 'ccm_userprojectgroup_delete':
            case 'ccm_userprojectgroup':
            case 'cms_planning':
            case 'cms_admin_dashboard':
            case 'cms_users':
            case 'cms_user_edit':
            case 'cms_basedatabase':
            case 'cms_basemetrics':
            case 'cms_basemetrics_new':
            case 'cms_basemetrics_edit':
            case 'cms_basemetrics_show':
            case 'cms_basemetrics_delete':
            case 'ccm_users':
            case 'cms_basematrix':
                $this->createAdminMenu($menu);
                break;
            case 'cms_index':
            case 'cms_projects':
            case 'cms_projects_new':
            case 'cms_projects_edit':
            case 'cms_projects_show':
                $this->createProjectLeftMenu($menu, true);
                break;
            case 'cms_subprojects':
            case 'cms_subprojects_new':
            case 'cms_subprojects_edit':
            case 'cms_list_subprojects':
                $this->createSubProjectLeftMenu($menu, $projectId, true);
                break;
            case 'cms_planning_master':
            case 'cms_reminders_master':
            case 'cms_subprojects_show':
            case 'cms_subprojects_dashboard':
            case 'cms_subprojectdatabase':
            case 'cms_subprojectmatrixsetup':
            case 'cms_subprojectmatrix':
            case 'cms_files':
            case 'cms_files_new':
            case 'cms_files_edit':
            case 'cms_subproject_samplingdatabase':
            case 'cms_subproject_cathodicprotectiondatabase':
            case 'cms_subproject_chemicaldosagedatabase':
            case 'cms_subproject_corrosionmonitoringdatabase':
            case 'cms_subprojectschemicaldosageaddcolumns':
            case 'cms_subprojectschemicaldosageaddcolumns_new':
            case 'cms_subprojectschemicaldosageaddcolumns_edit':
			case 'cms_subproject_productiondatabase':
            case 'cms_subprojectsproductionaddcolumns':
            case 'cms_subprojectsproductionaddcolumns_new':
            case 'cms_subprojectsproductionaddcolumns_edit':
            case 'cms_subproject_corrosion_datasheet':
            case 'cms_subproject_corrosion_datasheet_new':
            case 'cms_subproject_corrosion_datasheet_edit':
            case 'cms_subproject_corrosion_datasheet_production':
            case 'cms_subproject_corrosion_datasheet_graph':
            case 'cms_subproject_corrosion_datasheet_graph_corrosioninhibitor':
            case 'cms_subproject_corrosion_datasheet_graph_production':
            case 'cms_subproject_actiontracker':
            case 'cms_subproject_actiontrackeritems':
            case 'cms_subproject_actionowners':
            case 'cms_subproject_actionowners_new':
            case 'cms_subproject_actionowners_edit':
            case 'cms_subproject_actionowners_show':
            case 'cms_subproject_activityinspectionowners':
            case 'cms_subproject_activitymitigationstatus':
            case 'cms_subproject_activityplanning':
            case 'cms_subproject_activity_reminders':
            case 'cms_subproject_archives':
            case 'cms_subproject_approved_archives':
            case 'cms_workshops':
            case 'cms_workshops_new':
            case 'cms_workshops_edit':
            case 'cms_workshops_attendee_new':
            case 'cms_workshops_attendee_edit':
                $this->createSubProjectConfigMenu($menu, $projectId, $subprojectId, true);
                break;
        }

        return $menu;
    }

    public function createAdminMenu(&$menu)
    {
        $basedata = $menu->addChild(
            'Base Data',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $basedata->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityAuthorizationChecker->isGranted('ROLE_POSITIONS') || $this->securityHelper->isRoleGranted('ROLE_POSITIONS',$this->userGroupRoles)) {
            $menu->addChild(
                'Positions',
                array('route' => 'ccm_user_group_list')
            );
        }
        if ($this->securityAuthorizationChecker->isGranted('ROLE_USERS') || $this->securityHelper->isRoleGranted('ROLE_USERS', $this->userGroupRoles)) {
            $menu->addChild('Users', array('route' => 'ccm_users')
            );
        }

        $basedata->addChild('Database Configuration', array('route' => "cms_basedatabase"));

        $basedata->addChild('Metrics', array('route' => "cms_basemetrics"));

        $basedata->addChild('Matrix Configuration', array('route' => "cms_basematrix"));

    }

    private function createSubProjectLeftMenu(&$menu, $projectId)
    {
        //$project = $this->entityManager->getRepository('AIECmsBundle:Projects')->find($projectId);
        $menu->addChild(
            'List All',
            ['route' => 'cms_list_subprojects', 'routeParameters' => ['id' => $projectId]]
        );
        //if( $this->securityHelper->isRoleGranted('ROLE_PROJECT_ADD',$this->userGroupRoles) ){
        $menu->addChild(
            'Add New',
            ['route' => 'cms_subprojects_new','routeParameters' => ['projectId' => $projectId]]
        );

    }

    public function createBreadcrumbMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'breadcrumb');
        // this item will always be displayed
        return $menu;
    }

    public function mergeUserRequests($approvalOwners,$type){
        $em = $this->entityManager;

        global $kernel;

        /* Set Roles based on anomaly project groups user associated */
       
        $userHelper = $kernel->getContainer()->get('aie_cms.user.helper');
        $serializer = $kernel->getContainer()->get('jms_serializer');
        $anomalyHelper = $kernel->getContainer()->get('aie_cms.anomaly.helper');

        $reqEntitiesApprovalOwners = [];
        $reqEntitiesDeferralOwners = [];
        $entitiesPass = [];
        $registrarEntityCheck = null;
        $actionCodeArr = array('TRO','RO','FM');

        if (!empty($approvalOwners))
        {
            //$entities = $em->getRepository('AIEAnomalyBundle:Request')->findBy(array('status'=>-1,'project_id'=>,'requeststatus'=>'new'));
            if ($type == "A")
                $reqEntitiesApprovalOwners = $userHelper->getRequestsByStatusType(-1,'A');
            else if ($type == "D")
                $reqEntitiesApprovalOwners = $userHelper->getRequestsByStatusType(-1,'D');

            if (count($approvalOwners))
            {
                foreach($approvalOwners as $approvalOwner)
                {

                    $aprrovalOwnerAnomalyCategories[] = $approvalOwner->getAnomalyCategory();
                    $projectIds[] = $approvalOwner->getProject()->getId();

                    foreach($reqEntitiesApprovalOwners as $index => $entity)
                    {

                        if ($reqEntitiesApprovalOwners[$index]['id'])
                        {
                            $requestData = $anomalyHelper->formatAnomaly(
                            $serializer->deserialize($reqEntitiesApprovalOwners[$index]['data'], $reqEntitiesApprovalOwners[$index]['type'], 'json'),
                            json_decode($reqEntitiesApprovalOwners[$index]['data'], true)
                        );
                            $registrarEntityCheck = $em->getRepository('AIEAnomalyBundle:Registrar')->findOneBy(array('project'=>$approvalOwner->getProject()->getId(),'id'=>$requestData['Id']));


                            if (isset($registrarEntityCheck) && !empty($registrarEntityCheck) && ($registrarEntityCheck->getCode() == $approvalOwner->getAnomalyCategory() || in_array($registrarEntityCheck->getCode(),$actionCodeArr)))
                            {

                                $entitiesPass[$index] = $reqEntitiesApprovalOwners[$index];

                                $entitiesPass[$index]['registrarcode'] = (!empty($registrarEntityCheck->getCode()) ? $registrarEntityCheck->getCode() : "");

                                $entitiesPass[$index]['registrarprojectid'] = (!empty($registrarEntityCheck->getProject()->getId()) ? $registrarEntityCheck->getProject()->getId() : "");

                                $entitiesPass[$index]['registrarfullid'] = (!empty($registrarEntityCheck->getFullId()) ? $registrarEntityCheck->getFullId() : "");

                                 $entitiesPass[$index]['registrarid'] = (!empty($registrarEntityCheck->getId()) ? $registrarEntityCheck->getId() : "");
                            }
                            
                        }
                    }
                }
            }

        }

        return $entitiesPass;
    }
}
