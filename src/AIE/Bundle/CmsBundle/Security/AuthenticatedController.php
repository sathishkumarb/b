<?php
namespace AIE\Bundle\CmsBundle\Security;

use AIE\Bundle\CmsBundle\Security\SecureObjectInterface;

interface AuthenticatedController {

    public function getSecureObject();

    /*
     * [
     * 'ROUTE' => 'ROLE_NAME'
     * ]
     */
    public function getRoutesRoles();
} 