<?php
/**
 * User: Sathish
 * Date: 04/10/2016
 */

namespace AIE\Bundle\CmsBundle\Security;


trait RoutesRolesMapper {

    protected $_routesRoles = [
        'admin_dashboard'                           => 'ROLE_ADMIN',
        /* Project */
        'pipeline_index'                            => 'ROLE_VERACITY_PIPELINE',
        'anomaly_index'                             => 'ROLE_VERACITY_ANOMALY',
        'ccm_index'                                 => 'ROLE_VERACITY_CCM',

        /* Project */
        'cms_projects_create'                       => 'ROLE_PROJECT_LIST',
        'cms_projects_create'                       => 'ROLE_PROJECT_ADD',
        'cms_projects_new'                          => 'ROLE_PROJECT_ADD',
        'cms_projects_show'                         => 'ROLE_PROJECT_SHOW',
        'cms_projects_edit'                         => 'ROLE_PROJECT_EDIT',
        'cms_projects_update'                       => 'ROLE_PROJECT_EDIT',
        'cms_projects_delete'                       => 'ROLE_PROJECT_DELETE',
        'cms_projects_post_delete'                  => 'ROLE_PROJECT_DELETE',

        /* Admin Metrics */
        'cms_basemetrics'                           => 'ROLE_BASE_METRICS_SHOW',
        'cms_basemetrics_show'                      => 'ROLE_BASE_METRICS_SHOW',
        'cms_basemetrics_create'                    => 'ROLE_BASE_METRICS_ADD',
        'cms_basemetrics_new'                       => 'ROLE_BASE_METRICS_ADD',
        'cms_basemetrics_edit'                      => 'ROLE_BASE_METRICS_EDIT',
        'cms_basemetrics_update'                    => 'ROLE_BASE_METRICS_EDIT',
        'cms_basemetrics_delete'                    => 'ROLE_BASE_METRICS_DELETE',

        /* Admin BaseDatabase */
        'cms_basedatabase'                          => 'ROLE_BASE_DATABASE_SHOW',
        'cms_basedatabase_create'                   => 'ROLE_BASE_DATABASE_ADD',
        'cms_basedatabase_linkedcreate'             => 'ROLE_BASE_DATABASE_ADD',
        'cms_basedatabase_delete'                   => 'ROLE_BASE_DATABASE_DELETE',
        'cms_basedatabase_linkeddelete'             => 'ROLE_BASE_DATABASE_DELETE',

        /* Admin Base Matrix */
        'cms_basematrix'                            => 'ROLE_BASE_MATRIX_SYSTEMS_SHOW',
        'cms_basematrix_ajaxloaddatasheet'          => 'ROLE_BASE_MATRIX_SYSTEMS_SHOW',
        'cms_basematrix_basesystems_new'            => 'ROLE_BASE_MATRIX_SYSTEMS_ADD',
        'cms_basematrix_basesystems_create'         => 'ROLE_BASE_MATRIX_SYSTEMS_ADD',
        'cms_basematrix_basesystems_update'         => 'ROLE_BASE_MATRIX_SYSTEMS_EDIT',
        'cms_basematrix_basesystems_delete'         => 'ROLE_BASE_MATRIX_SYSTEMS_DELETE',

        'cms_basematrix_basethreats_new'            => 'ROLE_BASE_MATRIX_THREAT_ADD',
        'cms_basematrix_basethreats_create'         => 'ROLE_BASE_MATRIX_THREAT_ADD',
        'cms_basematrix_basethreats_update'         => 'ROLE_BASE_MATRIX_THREAT_EDIT',
        'cms_basematrix_basethreats_delete'         => 'ROLE_BASE_MATRIX_THREAT_DELETE',

        'cms_basematrix_basemitigationtechniques_new'       => 'ROLE_BASE_MATRIX_MITIGATION_ADD',
        'cms_basematrix_basemitigationtechniques_create'    => 'ROLE_BASE_MATRIX_MITIGATION_ADD',
        'cms_basematrix_basemitigationtechniques_update'    => 'ROLE_BASE_MATRIX_MITIGATION_EDIT',
        'cms_basematrix_basemitigationtechniques_delete'    => 'ROLE_BASE_MATRIX_MITIGATION_DELETE',

        'cms_basematrix_baseactivity_new'                       => 'ROLE_BASE_MATRIX_ACTIVITY_ADD',
        'cms_basematrix_baseactivity_create'                    => 'ROLE_BASE_MATRIX_ACTIVITY_ADD',
        'cms_basematrix_baseactivity_edit'                      => 'ROLE_BASE_MATRIX_ACTIVITY_EDIT',
        'cms_basematrix_baseactivity_update'                    => 'ROLE_BASE_MATRIX_ACTIVITY_EDIT',
        'cms_basematrix_baseactivity_updatefields'              => 'ROLE_BASE_MATRIX_ACTIVITY_EDIT',
        'cms_basematrix_baseactivity_updateexpressionfields'    => 'ROLE_BASE_MATRIX_ACTIVITY_EDIT',
        'cms_basematrix_baseactivity_delete'                    => 'ROLE_BASE_MATRIX_ACTIVITY_DELETE',

        /* ccm Document */
        'cms_files'                                         => 'ROLE_DOCUMENTS',
        'cms_files_create'                                  => 'ROLE_DOCUMENTS',
        'cms_files_new'                                     => 'ROLE_DOCUMENTS',
        'cms_files_show'                                    => 'ROLE_DOCUMENTS',
        'cms_files_edit'                                    => 'ROLE_DOCUMENTS',
        'cms_files_update'                                  => 'ROLE_DOCUMENTS',
        'cms_files_delete'                                  => 'ROLE_DOCUMENTS',
        'cms_files_categories'                              => 'ROLE_DOCUMENTS',
        'cms_files_categories_create'                       => 'ROLE_DOCUMENTS',
        'cms_files_categories_update'                       => 'ROLE_DOCUMENTS',
        'cms_files_categories_delete'                       => 'ROLE_DOCUMENTS',
        'cms_files_categories_move'                         => 'ROLE_DOCUMENTS',
        'cms_files_move'                                    => 'ROLE_DOCUMENTS',
        'ajax_pipeline_files'                               => 'ROLE_DOCUMENTS',

        'cms_subprojects'                                   => 'ROLE_SUB_PROJECT_SHOW',
        'cms_subprojects_create'                            => 'ROLE_SUB_PROJECT_ADD',
        'cms_subprojects_new'                               => 'ROLE_SUB_PROJECT_ADD',
        'cms_subprojects_delete'                            => 'ROLE_SUB_PROJECT_DELETE',

        'cms_subprojects_show'                              => 'ROLE_SUB_PROJECT_SHOW',
        'cms_subprojects_dashboard'                         => 'ROLE_SUB_PROJECT_SHOW',
        'cms_subprojects_dashboard_report'                  => 'ROLE_SUB_PROJECT_SHOW',
        'cms_dashboard_report_pdf'                          => 'ROLE_SUB_PROJECT_SHOW',
        'cms_subprojects_edit'                              => 'ROLE_SUB_PROJECT_EDIT',
        'cms_subprojects_update'                            => 'ROLE_SUB_PROJECT_EDIT',

        'cms_subprojectdatabase'                            => 'ROLE_SUB_PROJECT_DATABASE_SHOW',
        'cms_subprojectdatabase_refer'                      => 'ROLE_SUB_PROJECT_DATABASE_SHOW',
        'cms_subprojectdatabase_create'                     => 'ROLE_SUB_PROJECT_DATABASE_ADD',
        'cms_subprojectdatabase_linkedcreate'               => 'ROLE_SUB_PROJECT_DATABASE_ADD',
        'cms_subprojectdatabase_delete'                     => 'ROLE_SUB_PROJECT_DATABASE_DELETE',
        'cms_subprojectdatabase_linkeddelete'               => 'ROLE_SUB_PROJECT_DATABASE_DELETE',

        'cms_subprojectmatrix'                              => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_SHOW',
        'cms_subprojectmatrix_ajaxloaddatasheet'            => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_SHOW',
        'cms_subprojectmatrix_subprojectsystems_new'        => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_ADD',
        'cms_subprojectmatrix_systems_create'               => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_ADD',
        'cms_subprojectmatrix_systems_update'               => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_EDIT',
        'cms_subprojectmatrix_systems_deleteconfirm'        => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_DELETE',
        'cms_subprojectmatrix_systems_delete'               => 'ROLE_SUB_PROJECT_MATRIX_SYSTEMS_DELETE',

        'cms_subprojectmatrix_threats_new'                              => 'ROLE_SUB_PROJECT_MATRIX_THREATS_ADD',
        'cms_subprojectmatrix_threats_create'                           => 'ROLE_SUB_PROJECT_MATRIX_THREATS_ADD',
        'cms_subprojectmatrix_threats_update'                           => 'ROLE_SUB_PROJECT_MATRIX_THREATS_EDIT',
        'cms_subprojectmatrix_threats_deleteconfirm'                    => 'ROLE_SUB_PROJECT_MATRIX_THREATS_DELETE',
        'cms_subprojectmatrix_threats_delete'                           => 'ROLE_SUB_PROJECT_MATRIX_THREATS_DELETE',

        'cms_subprojectmatrix_mitigationtechniques_new'                 => 'ROLE_SUB_PROJECT_MATRIX_MITIGATION_ADD',
        'cms_subprojectmatrix_mitigationtechniques_create'              => 'ROLE_SUB_PROJECT_MATRIX_MITIGATION_ADD',
        'cms_subprojectmatrix_mitigationtechniques_update'              => 'ROLE_SUB_PROJECT_MATRIX_MITIGATION_EDIT',
        'cms_subprojectmatrix_mitigationtechniques_deleteconfirm'       => 'ROLE_SUB_PROJECT_MATRIX_MITIGATION_DELETE',
        'cms_subprojectmatrix_mitigationtechniques_delete'              => 'ROLE_SUB_PROJECT_MATRIX_MITIGATION_DELETE',

        'cms_subprojectmatrix_activity_new'                             => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_ADD',
        'cms_subprojectmatrix_activity_create'                          => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_ADD',
        'cms_subprojectmatrix_activity_edit'                            => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_EDIT',
        'cms_subprojectmatrix_activity_update'                          => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_EDIT',
        'cms_subprojectmatrix_activity_updatefields'                    => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_EDIT',
        'cms_subprojectmatrix_activity_updateexpressionfields'          => 'ROLE_SUB_PROJECT_MATRIX_ACTIVITY_EDIT',

        'cms_subprojectmatrixsetup'                                     => 'ROLE_SUB_PROJECT_MATRIX_SETUP_SHOW',
        'cms_subprojectmatrixsetup_update'                              => 'ROLE_SUB_PROJECT_MATRIX_SETUP_EDIT',

        'cms_subproject_samplingdatabase'                               => 'ROLE_SAMPLING_DATABASE_SHOW',
        'cms_subproject_samplingdatabase_loadsystem'                    => 'ROLE_SAMPLING_DATABASE_SHOW',
        'cms_subproject_samplingdatabase_create'                        => 'ROLE_SAMPLING_DATABASE_ADD',
        'cms_subproject_samplingdatabase_edit'                          => 'ROLE_SAMPLING_DATABASE_EDIT',
        'cms_subproject_samplingdatabase_updatefields'                  => 'ROLE_SAMPLING_DATABASE_EDIT',
        'cms_subproject_samplingdatabase_corrosionsave'                 => 'ROLE_SAMPLING_DATABASE_EDIT',
        'cms_subproject_samplingdatabase_delete'                        => 'ROLE_SAMPLING_DATABASE_DELETE',

        'cms_subproject_cathodicprotectiondatabase'                     => 'ROLE_CATHODIC_PROTECTION_DATABASE_SHOW',
        'cms_subproject_cathodicprotectiondatabase_loadsystem'          => 'ROLE_CATHODIC_PROTECTION_DATABASE_SHOW',
        'cms_subproject_cathodicprotectiondatabase_create'              => 'ROLE_CATHODIC_PROTECTION_DATABASE_ADD',
        'cms_subproject_cathodicprotectiondatabase_updatefields'        => 'ROLE_CATHODIC_PROTECTION_DATABASE_EDIT',
        'cms_subproject_cathodicprotectiondatabase_edit'                => 'ROLE_CATHODIC_PROTECTION_DATABASE_EDIT',
        'cms_subproject_cathodicprotectiondatabase_corrosionsave'       => 'ROLE_CATHODIC_PROTECTION_DATABASE_EDIT',
        'cms_subproject_cathodicprotectiondatabase_delete'              => 'ROLE_CATHODIC_PROTECTION_DATABASE_DELETE',

        'cms_subproject_chemicaldosagedatabase'                     => 'ROLE_CHEMICAL_DATABASE_SHOW',
        'cms_subproject_chemicaldosagedatabase_loadsystem'          => 'ROLE_CHEMICAL_DATABASE_SHOW',
        'cms_subproject_chemicaldosagedatabase_create'              => 'ROLE_CHEMICAL_DATABASE_ADD',
        'cms_subproject_chemicaldosagedatabase_updatefields'        => 'ROLE_CHEMICAL_DATABASE_EDIT',
        'cms_subproject_chemicaldosagedatabase_edit'                => 'ROLE_CHEMICAL_DATABASE_EDIT',
        'cms_subproject_chemicaldosagedatabase_corrosionsave'       => 'ROLE_CHEMICAL_DATABASE_EDIT',
        'cms_subproject_chemicaldosagedatabase_delete'              => 'ROLE_CHEMICAL_DATABASE_DELETE',

        'cms_subproject_corrosionmonitoringdatabase'                => 'ROLE_COUPON_PROBE_DATABASE_SHOW',
        'cms_subproject_corrosionmonitoringdatabase_loadsystem'     => 'ROLE_COUPON_PROBE_DATABASE_SHOW',
        'cms_subproject_corrosionmonitoringdatabase_create'         => 'ROLE_COUPON_PROBE_DATABASE_ADD',
        'cms_subproject_corrosionmonitoringdatabase_updatefields'   => 'ROLE_COUPON_PROBE_DATABASE_ADD',
        'cms_subproject_corrosionmonitoringdatabase_edit'           => 'ROLE_COUPON_PROBE_DATABASE_EDIT',
        'cms_subproject_corrosionmonitoringdatabase_corrosionsave'  => 'ROLE_COUPON_PROBE_DATABASE_EDIT',
        'cms_subproject_corrosionmonitoringdatabase_delete'         => 'ROLE_COUPON_PROBE_DATABASE_DELETE',

        'cms_subprojectschemicaldosageaddcolumns'               => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_create'        => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_new'           => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_show'          => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_edit'          => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_update'        => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',
        'cms_subprojectschemicaldosageaddcolumns_delete'        => 'ROLE_CHEMICAL_DATABASE_ADD_NEW_COLUMNS',

        'cms_subproject_corrosion_datasheet'                                    => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_production'                         => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph'                              => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph_controlactivity'              => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph_corrosioninhibitor'           => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph_corrosioninhibitorparse'      => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph_four'                         => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_graph_production'                   => 'ROLE_MITIGATION_STATUS_SHOW',
        'cms_subproject_corrosion_datasheet_new'                                => 'ROLE_MITIGATION_STATUS_ADD',
        'cms_subproject_corrosion_datasheet_create'                             => 'ROLE_MITIGATION_STATUS_ADD',
        'cms_subproject_corrosion_datasheet_edit'                               => 'ROLE_MITIGATION_STATUS_EDIT',
        'cms_subproject_corrosiondatasheet_save'                                => 'ROLE_MITIGATION_STATUS_EDIT',
        'cms_subproject_corrosion_datasheet_update'                             => 'ROLE_MITIGATION_STATUS_EDIT',
        'cms_subproject_corrosion_datasheet_delete'                             => 'ROLE_MITIGATION_STATUS_DELETE',

        'cms_subproject_actionowners'           => 'ROLE_ACTIONOWNERS_SHOW',
        'cms_subproject_actionowners_create'    => 'ROLE_ACTIONOWNERS_ADD',
        'cms_subproject_actionowners_new'       => 'ROLE_ACTIONOWNERS_ADD',
        'cms_subproject_actionowners_show'      => 'ROLE_ACTIONOWNERS_SHOW',
        'cms_subproject_actionowners_edit'      => 'ROLE_ACTIONOWNERS_EDIT',
        'cms_subproject_actionowners_update'    => 'ROLE_ACTIONOWNERS_EDIT',
        'cms_subproject_actionowners_delete'    => 'ROLE_ACTIONOWNERS_DELETE',

        'cms_subproject_activitymanagement'                 => 'ROLE_ACTIVITIES_SHOW',
        'cms_subproject_activityinspectionowners'           => 'ROLE_ACTIVITIES_SHOW',
        'cms_subproject_activityinspectionowners_update'    => 'ROLE_ACTIVITIES_EDIT',
        'cms_subproject_activitymitigationstatus'           => 'ROLE_ACTIVITIES_SHOW',
        'cms_subproject_activitymitigationstatus_update'    => 'ROLE_ACTIVITIES_EDIT',

        'cms_subproject_archives'                               => 'ROLE_MATRICES_AND_REPORTS_ARCHIVE',
        'cms_subproject_approved_archives'                      => 'ROLE_MATRICES_AND_REPORTS_APPROVED',
        'cms_subproject_archivescreate'                         => 'ROLE_MATRICES_AND_REPORTS_ARCHIVE',
        'cms_subproject_archivereports'                         => 'ROLE_MATRICES_AND_REPORTS_ARCHIVE',
        'cms_subproject_archivereports_docx'                    => 'ROLE_MATRICES_AND_REPORTS_ARCHIVE',
        'cms_subproject_archivereports_approve'                 => 'ROLE_MATRICES_AND_REPORTS_ARCHIVE',

        'cms_subproject_outstandingactions' => 'ROLE_SUB_PROJECT_OUTSTANDINGS_SHOW',
        'cms_subproject_systemoutstandingactions'=> 'ROLE_SUB_PROJECT_OUTSTANDINGS_SHOW',

        'cms_subproject_actiontracker'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_SHOW',
        'cms_subproject_actiontracker_create'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ADD',
        'cms_subproject_actiontracker_delete'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_DELETE',
        'cms_subproject_actiontracker_edit'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_EDIT',
        'cms_subproject_actiontrackeritems'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_SHOW',
        'cms_subproject_actiontrackeritems_create'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_ADD',
        'cms_subproject_actiontrackeritems_edit'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_EDIT',
        'cms_subproject_actiontrackeritems_delete'=> 'ROLE_ACTIVITY_MANAGEMENT_ACTIONTRACKER_ITEMS_DELETE',

        'cms_workshops'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_SHOW',
        'cms_workshops_create'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_ADD',
        'cms_workshops_new'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_ADD',
        'cms_workshops_show'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_SHOW',
        'cms_workshops_edit'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_EDIT',
        'cms_workshops_update'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_EDIT',
        'cms_workshops_delete'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_DELETE',

        'cms_workshops_attendee_create'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEE_ADD',
        'cms_workshops_attendee_new'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEE_ADD',
        'cms_workshops_attendee_edit'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEE_EDIT',
        'cms_workshops_attendee_update'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEE_EDIT',
        'cms_workshops_attendee_delete'=> 'ROLE_ACTIVITY_MANAGEMENT_MEETINGS_VIEW_ATTENDEE_DELETE',

        /* Plannig ccm */
        'cms_subproject_activityplanning'                               => 'ROLE_ACTIVITY_MANAGEMENT_PLANNING',
        'cms_subproject_activity_reminders'                             => 'ROLE_ACTIVITY_MANAGEMENT_REMINDERS',

        /* ccm Positions */
        'ccm_userprojectgroup' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_create' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_new' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_show' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_edit' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_update' => 'ROLE_POSITIONS',
        'ccm_userprojectgroup_delete' => 'ROLE_POSITIONS',

        /* CCM Users */
        'ccm_users' => 'ROLE_USERS',
        'ccm_user_doadmin' => 'ROLE_USERS',
    ];


    public function detectSecureObject($request, $em)
    {

        //routes that will contain object
        $projectRoutes = [

            /* Projects */
            'cms_projects',
            'cms_projects_create',
            'cms_projects_new',
            'cms_list_subprojects',
            'cms_projects_edit  ',
            'cms_projects_show',
            'cms_projects_update',
            'cms_projects_delete',

            /* Admin Metrics */
            'cms_basemetrics',
            'cms_basemetrics_show',
            'cms_basemetrics_create',
            'cms_basemetrics_new',
            'cms_basemetrics_edit',
            'cms_basemetrics_update',
            'cms_basemetrics_delete',

            /* Admin BaseDatabase */
            'cms_basedatabase',
            'cms_basedatabase_create',
            'cms_basedatabase_linkedcreate',
            'cms_basedatabase_delete',
            'cms_basedatabase_linkeddelete',

            /* Admin Base Mattrix */
            'cms_basematrix',
            'cms_basematrix_ajaxloaddatasheet',
            'cms_basematrix_basesystems_new',
            'cms_basematrix_basesystems_create',
            'cms_basematrix_basesystems_update',

            'cms_basematrix_basethreats_new',
            'cms_basematrix_basethreats_create',
            'cms_basematrix_basethreats_update',

            'cms_basematrix_basemitigationtechniques_new',
            'cms_basematrix_basemitigationtechniques_create',
            'cms_basematrix_basemitigationtechniques_update',

            'cms_basematrix_baseactivity_new',
            'cms_basematrix_baseactivity_create',
            'cms_basematrix_baseactivity_edit',
            'cms_basematrix_baseactivity_update',
            'cms_basematrix_baseactivity_updatefields',
            'cms_basematrix_baseactivity_updateexpressionfields',

            'cms_basematrix_basesystems_delete',
            'cms_basematrix_basethreats_delete',
            'cms_basematrix_basemitigationtechniques_delete',

            'cms_subprojects',
            'cms_subprojects_create',
            'cms_subprojects_new',
            'cms_subprojects_delete',
        ];

        $subprojectRoutes = [

            'cms_subprojects_show',
            'cms_subprojects_dashboard',
            'cms_subprojects_dashboard_report',
            'cms_dashboard_report_pdf',
            'cms_subprojects_edit',
            'cms_subprojects_update',

            'cms_subprojectdatabase',
            'cms_subprojectdatabase_refer',
            'cms_subprojectdatabase_create,',
            'cms_subprojectdatabase_linkedcreate',
            'cms_subprojectdatabase_delete',
            'cms_subprojectdatabase_linkeddelete',

            'cms_subprojectmatrix',
            'cms_subprojectmatrix_ajaxloaddatasheet',
            'cms_subprojectmatrix_subprojectsystems_new',
            'cms_subprojectmatrix_systems_create',
            'cms_subprojectmatrix_systems_update',
            'cms_subprojectmatrix_threats_new',
            'cms_subprojectmatrix_threats_create',
            'cms_subprojectmatrix_threats_update',

            'cms_subprojectmatrix_mitigationtechniques_new',
            'cms_subprojectmatrix_mitigationtechniques_create',
            'cms_subprojectmatrix_mitigationtechniques_update',

            'cms_subprojectmatrix_activity_new',
            'cms_subprojectmatrix_activity_create',
            'cms_subprojectmatrix_activity_edit',
            'cms_subprojectmatrix_activity_update',
            'cms_subprojectmatrix_activity_updatefields',
            'cms_subprojectmatrix_activity_updateexpressionfields',

            'cms_subprojectmatrix_systems_deleteconfirm',
            'cms_subprojectmatrix_systems_delete',

            'cms_subprojectmatrix_threats_deleteconfirm',
            'cms_subprojectmatrix_threats_delete',

            'cms_subprojectmatrix_mitigationtechniques_deleteconfirm',
            'cms_subprojectmatrix_mitigationtechniques_delete',

            'cms_subprojectmatrixsetup',
            'cms_subprojectmatrixsetup_update',

            'cms_subproject_samplingdatabase',
            'cms_subproject_samplingdatabase_loadsystem',
            'cms_subproject_samplingdatabase_create',
            'cms_subproject_samplingdatabase_edit',
            'cms_subproject_samplingdatabase_delete',
            'cms_subproject_samplingdatabase_updatefields',
            'cms_subproject_samplingdatabase_corrosionsave',

            'cms_subproject_cathodicprotectiondatabase',
            'cms_subproject_cathodicprotectiondatabase_loadsystem',
            'cms_subproject_cathodicprotectiondatabase_create',
            'cms_subproject_cathodicprotectiondatabase_delete',
            'cms_subproject_cathodicprotectiondatabase_updatefields',
            'cms_subproject_cathodicprotectiondatabase_edit',
            'cms_subproject_cathodicprotectiondatabase_corrosionsave',

            'cms_subproject_chemicaldosagedatabase',
            'cms_subproject_chemicaldosagedatabase_loadsystem',
            'cms_subproject_chemicaldosagedatabase_create',
            'cms_subproject_chemicaldosagedatabase_delete',
            'cms_subproject_chemicaldosagedatabase_updatefields',
            'cms_subproject_chemicaldosagedatabase_edit',
            'cms_subproject_chemicaldosagedatabase_corrosionsave',

            'cms_subproject_corrosion_datasheet',
            'cms_subproject_corrosion_datasheet_new',
            'cms_subproject_corrosion_datasheet_edit',
            'cms_subproject_corrosion_datasheet_production',
            'cms_subproject_corrosion_datasheet_create',
            'cms_subproject_corrosion_datasheet_delete',
            'cms_subproject_corrosion_datasheet_update',
            'cms_subproject_corrosion_datasheet_graph',
            'cms_subproject_corrosion_datasheet_graph_controlactivity',
            'cms_subproject_corrosion_datasheet_graph_corrosioninhibitor',
            'cms_subproject_corrosion_datasheet_graph_corrosioninhibitorparse',
            'cms_subproject_corrosion_datasheet_graph_four',
            'cms_subproject_corrosion_datasheet_graph_production',
            'cms_subproject_corrosiondatasheet_save',

            'cms_subproject_corrosionmonitoringdatabase',
            'cms_subproject_corrosionmonitoringdatabase_loadsystem',
            'cms_subproject_corrosionmonitoringdatabase_create',
            'cms_subproject_corrosionmonitoringdatabase_delete',
            'cms_subproject_corrosionmonitoringdatabase_updatefields',
            'cms_subproject_corrosionmonitoringdatabase_edit',
            'cms_subproject_corrosionmonitoringdatabase_corrosionsave',

            'cms_subprojectschemicaldosageaddcolumns',
            'cms_subprojectschemicaldosageaddcolumns_create',
            'cms_subprojectschemicaldosageaddcolumns_new',
            'cms_subprojectschemicaldosageaddcolumns_show',
            'cms_subprojectschemicaldosageaddcolumns_dashboard',
            'cms_subprojectschemicaldosageaddcolumns_edit',
            'cms_subprojectschemicaldosageaddcolumns_update',
            'cms_subprojectschemicaldosageaddcolumns_delete',

            'cms_subproject_actionowners',
            'cms_subproject_actionowners_create',
            'cms_subproject_actionowners_new',
            'cms_subproject_actionowners_show',
            'cms_subproject_actionowners_edit',
            'cms_subproject_actionowners_update',
            'cms_subproject_actionowners_delete',

            'cms_subproject_activitymanagement',
            'cms_subproject_activityinspectionowners',
            'cms_subproject_activityinspectionowners_update',
            'cms_subproject_activitymitigationstatus',
            'cms_subproject_activitymitigationstatus_update',

            'cms_subproject_activityplanning',
            'cms_subproject_activity_reminders',

            'cms_subproject_archives',
            'cms_subproject_approved_archives',
            'cms_subproject_archivescreate',
            'cms_subproject_archivereports',
            'cms_subproject_archivereports_docx',
            'cms_subproject_archivereports_approve',

            'cms_subproject_outstandingactions',
            'cms_subproject_systemoutstandingactions',
            'cms_subproject_actiontracker',
            'cms_subproject_actiontracker_create',
            'cms_subproject_actiontracker_delete',
            'cms_subproject_actiontracker_edit',
            'cms_subproject_actiontrackeritems',
            'cms_subproject_actiontrackeritems_create',
            'cms_subproject_actiontrackeritems_edit',
            'cms_subproject_actiontrackeritems_delete',

            'cms_files',
            'cms_files_create',
            'cms_files_new',
            'cms_files_show',
            'cms_files_edit',
            'cms_files_update',
            'cms_files_delete',

            'cms_files_categories',
            'cms_files_categories_create',
            'cms_files_categories_update',
            'cms_files_categories_delete',
            'cms_files_categories_move',

            'cms_files_move',
            'cms_workshops',
            'cms_workshops_create',
            'cms_workshops_new',
            'cms_workshops_show',
            'cms_workshops_edit',
            'cms_workshops_update',
            'cms_workshops_delete',

            'cms_workshops_attendee_create',
            'cms_workshops_attendee_new',
            'cms_workshops_attendee_edit',
            'cms_workshops_attendee_update',
            'cms_workshops_attendee_delete',

        ];

        $projectId = null;
        //choose first parameter if project


        //if pipeline, get pipeline then get project
        $route = $request->get('_route');
        if (in_array($route, $projectRoutes))
        {
            $projectId = $request->get('projectId', $request->get('id'));
        } else if (in_array($route, $subprojectRoutes))
        {
            $subprojectId = $request->get('subprojectId', $request->get('id'));
            $subproject = $em->getRepository('AIECmsBundle:SubProjects')->find($subprojectId);
            $projectId = $subproject->getProject()->getId();
        } else
        {
            return null;
        }

        //return project else null
        return $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);;

    }
} 