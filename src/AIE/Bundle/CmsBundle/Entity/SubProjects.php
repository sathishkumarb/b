<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use AIE\Bundle\VeracityBundle\Component\Core\Projects as ProjectsBase;
use AIE\Bundle\CmsBundle\Security\SecureObjectInterface;
use AIE\Bundle\UserBundle\Entity\User;

/**
 * SubProjects
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SubProjects extends ProjectsBase
{

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="folder", type="string", length=32, nullable=true)
     */
    private $folder;

    /**
     * @var string
     *
     * @ORM\Column(name="reportingperiod", type="string", length=100)
     */
    private $reportingperiod;

    /**
     * @ORM\OneToMany(targetEntity="ActionOwners" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $actionOwners;

	
	/**
     * @ORM\OneToMany(targetEntity="UserProjectGroup" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ug;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="SubProjects")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectDatabaseProperties" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $databaseproperties;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectDatabaseLinkedProperties" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $databaselinkedproperties;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectSystems" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $systems;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectThreats" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $threats;

    /**
     * @ORM\OneToMany(targetEntity="Files" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $files;

    /**
     * @ORM\OneToMany(targetEntity="FilesCategories" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $filescategories;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectMitigationTechniques" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $mitigationtechniques;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectMatricesReports" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spmr;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectActivity" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $activity;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectWorkshops" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $workshop;

    /**
     * @ORM\OneToMany(targetEntity="ActionTracker" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $actiontracker;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCathodicProtectionDatabase" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spcpd;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCorrosionMonitoringDatabase" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spcmd;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectSamplingDatabase" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spsd;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectChemicalDosageDatabase" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spcdd;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectProductionDatabase" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spprd;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCorrosionDatasheet" , mappedBy="subproject", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spcd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actionOwners = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userProjectGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->databaseproperties = new \Doctrine\Common\Collections\ArrayCollection();
        $this->databaselinkedproperties = new \Doctrine\Common\Collections\ArrayCollection();

        $this->systems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activity = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workshop = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actiontracker = new \Doctrine\Common\Collections\ArrayCollection();
        $this->spmr = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjects
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set reportingperiod
     *
     * @param string $reportingperiod
     * @return SubProjects
     */
    public function setReportingperiod($reportingperiod)
    {
        $this->reportingperiod = $reportingperiod;

        return $this;
    }

    /**
     * Get reportingperiod
     *
     * @return string
     */
    public function getReportingperiod()
    {
        return $this->reportingperiod;
    }


      /**
     * Set project
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Projects $project
     * @return AnomalyClass
     */
    public function setProject(\AIE\Bundle\CmsBundle\Entity\Projects $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * Add actionOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners
     * @return SubProjects
     */
    public function addActionOwner(\AIE\Bundle\CmsBundle\Entity\ActionOwners $actionOwners)
    {
        $this->actionOwners[] = $actionOwners;
    
        return $this;
    }

    /**
     * Remove actionOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners
     */
    public function removeActionOwner(\AIE\Bundle\CmsBundle\Entity\ActionOwners $actionOwners)
    {
        $this->actionOwners->removeElement($actionOwners);
    }

    /**
     * Add Basedatabaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $Basedatabaseproperties
     * @return SubProjects
     */
    public function addBasedatabaseproperties(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties)
    {
        $this->databaseproperties[] = $databaseproperties;

        return $this;
    }

    /**
     * Remove Basedatabaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $basedatabaseproperties
     */
    public function removeBasedatabaseproperties(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties)
    {
        $this->databaseproperties->removeElement($databaseproperties);
    }

    /**
     * Add Basedatabaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $basedatabaselinkedproperties
     * @return SubProjects
     */
    public function addBasedatabaselinkedproperties(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties)
    {
        $this->databaselinkedproperties[] = $databaselinkedproperties;

        return $this;
    }

    /**
     * Remove Basedatabaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $Basedatabaseproperties
     */
    public function removeBasedatabaselinkedproperties(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties)
    {
        $this->databaselinkedproperties->removeElement($databaselinkedproperties);
    }

    /**
     * Add systems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems
     * @return SubProjects
     */
    public function addSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems)
    {
        $this->systems[] = $systems;

        return $this;
    }

    /**
     * Remove systems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems
     */
    public function removeSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems)
    {
        $this->systems->removeElement($systems);
    }

    /**
     * Add activity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $activity
     * @return SubProjectActivity
     */
    public function addActivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $activity)
    {
        $this->activity[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $activity
     */
    public function removeActivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $activity)
    {
        $this->activity->removeElement($activity);
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     * @return Files
     */
    public function addFiles(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     */
    public function removeFiles(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get actionOwners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActionOwners()
    {
        return $this->actionOwners;
    }

    /**
     * Get actionOwners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get databaseproperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBasedatabaseproperties()
    {
        return $this->databaseproperties;
    }

    /**
     * Get databaselinkedproperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBasedatabaselinkedproperties()
    {
        return $this->databaselinkedproperties;
    }

    /**
     * Get systems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSystems()
    {
        return $this->systems;
    }

    /**
     * Get activity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return Pipelines
     */
    public function setFolder($folder) {
        $this->folder = $folder;

        return $this;
    }

	public function getUploadDir()
	{
		return $this->getFolder() . '/logos';
	}

    /**
     * Get folder
     *
     * @return string
     */
    public function getFolder() {
        return $this->folder;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Add ug
     *
     * @param \AIE\Bundle\CmsBundle\Entity\UserProjectGroup $ug
     * @return SubProjects
     */
    public function addUg(\AIE\Bundle\CmsBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug[] = $ug;

        return $this;
    }

    /**
     * Remove ug
     *
     * @param \AIE\Bundle\CmsBundle\Entity\UserProjectGroup $ug
     */
    public function removeUg(\AIE\Bundle\CmsBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug->removeElement($ug);
    }

    /**
     * Get ug
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUg()
    {
        return $this->ug;
    }

    /**
     * Add databaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties
     * @return SubProjects
     */
    public function addDatabaseproperty(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties)
    {
        $this->databaseproperties[] = $databaseproperties;

        return $this;
    }

    /**
     * Remove databaseproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties
     */
    public function removeDatabaseproperty(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseProperties $databaseproperties)
    {
        $this->databaseproperties->removeElement($databaseproperties);
    }

    /**
     * Get databaseproperties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatabaseproperties()
    {
        return $this->databaseproperties;
    }

    /**
     * Add databaselinkedproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties
     * @return SubProjects
     */
    public function addDatabaselinkedproperty(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties)
    {
        $this->databaselinkedproperties[] = $databaselinkedproperties;

        return $this;
    }

    /**
     * Remove databaselinkedproperties
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties
     */
    public function removeDatabaselinkedproperty(\AIE\Bundle\CmsBundle\Entity\SubProjectDatabaseLinkedProperties $databaselinkedproperties)
    {
        $this->databaselinkedproperties->removeElement($databaselinkedproperties);
    }

    /**
     * Get databaselinkedproperties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatabaselinkedproperties()
    {
        return $this->databaselinkedproperties;
    }

    /**
     * Add systems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems
     * @return SubProjects
     */
    public function addSystem(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems)
    {
        $this->systems[] = $systems;

        return $this;
    }

    /**
     * Remove systems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems
     */
    public function removeSystem(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $systems)
    {
        $this->systems->removeElement($systems);
    }

    /**
     * Add threats
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectThreats $threats
     * @return SubProjects
     */
    public function addThreat(\AIE\Bundle\CmsBundle\Entity\SubProjectThreats $threats)
    {
        $this->threats[] = $threats;

        return $this;
    }

    /**
     * Remove threats
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectThreats $threats
     */
    public function removeThreat(\AIE\Bundle\CmsBundle\Entity\SubProjectThreats $threats)
    {
        $this->threats->removeElement($threats);
    }

    /**
     * Get threats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getThreats()
    {
        return $this->threats;
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     * @return SubProjects
     */
    public function addFile(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     */
    public function removeFile(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Add filescategories
     *
     * @param \AIE\Bundle\CmsBundle\Entity\FilesCategories $filescategories
     * @return SubProjects
     */
    public function addFilescategory(\AIE\Bundle\CmsBundle\Entity\FilesCategories $filescategories)
    {
        $this->filescategories[] = $filescategories;

        return $this;
    }

    /**
     * Remove filescategories
     *
     * @param \AIE\Bundle\CmsBundle\Entity\FilesCategories $filescategories
     */
    public function removeFilescategory(\AIE\Bundle\CmsBundle\Entity\FilesCategories $filescategories)
    {
        $this->filescategories->removeElement($filescategories);
    }

    /**
     * Get filescategories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilescategories()
    {
        return $this->filescategories;
    }

    /**
     * Add mitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $mitigationtechniques
     * @return SubProjects
     */
    public function addMitigationtechnique(\AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $mitigationtechniques)
    {
        $this->mitigationtechniques[] = $mitigationtechniques;

        return $this;
    }

    /**
     * Remove mitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $mitigationtechniques
     */
    public function removeMitigationtechnique(\AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $mitigationtechniques)
    {
        $this->mitigationtechniques->removeElement($mitigationtechniques);
    }

    /**
     * Get mitigationtechniques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMitigationtechniques()
    {
        return $this->mitigationtechniques;
    }

    /**
     * Add workshop
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops $workshop
     * @return SubProjects
     */
    public function addWorkshop(\AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops $workshop)
    {
        $this->workshop[] = $workshop;

        return $this;
    }

    /**
     * Remove workshop
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops $workshop
     */
    public function removeWorkshop(\AIE\Bundle\CmsBundle\Entity\SubProjectWorkshops $workshop)
    {
        $this->workshop->removeElement($workshop);
    }

    /**
     * Get workshop
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * Add actiontracker
     *
     * @param \AIE\Bundle\CmsBundle\Entity\ActionTracker $actiontracker
     * @return SubProjects
     */
    public function addActiontracker(\AIE\Bundle\CmsBundle\Entity\ActionTracker $actiontracker)
    {
        $this->actiontracker[] = $actiontracker;

        return $this;
    }

    /**
     * Remove actiontracker
     *
     * @param \AIE\Bundle\CmsBundle\Entity\ActionTracker $actiontracker
     */
    public function removeActiontracker(\AIE\Bundle\CmsBundle\Entity\ActionTracker $actiontracker)
    {
        $this->actiontracker->removeElement($actiontracker);
    }

    /**
     * Get actiontracker
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActiontracker()
    {
        return $this->actiontracker;
    }

    /**
     * Add spmr
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports $spmr
     * @return SubProjects
     */
    public function addSpmr(\AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports $spmr)
    {
        $this->spmr[] = $spmr;

        return $this;
    }

    /**
     * Remove spmr
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports $spmr
     */
    public function removeSpmr(\AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports $spmr)
    {
        $this->spmr->removeElement($spmr);
    }

    /**
     * Get spmr
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpmr()
    {
        return $this->spmr;
    }

    /**
     * Add spcpd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase $spcpd
     * @return SubProjects
     */
    public function addSpcpd(\AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase $spcpd)
    {
        $this->spcpd[] = $spcpd;

        return $this;
    }

    /**
     * Remove spcpd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase $spcpd
     */
    public function removeSpcpd(\AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase $spcpd)
    {
        $this->spcpd->removeElement($spcpd);
    }

    /**
     * Get spcpd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpcpd()
    {
        return $this->spcpd;
    }

    /**
     * Add spcmd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase $spcmd
     * @return SubProjects
     */
    public function addSpcmd(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase $spcmd)
    {
        $this->spcmd[] = $spcmd;

        return $this;
    }

    /**
     * Remove spcmd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase $spcmd
     */
    public function removeSpcmd(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase $spcmd)
    {
        $this->spcmd->removeElement($spcmd);
    }

    /**
     * Get spcmd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpcmd()
    {
        return $this->spcmd;
    }

    /**
     * Add spsd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase $spsd
     * @return SubProjects
     */
    public function addSpsd(\AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase $spsd)
    {
        $this->spsd[] = $spsd;

        return $this;
    }

    /**
     * Remove spsd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase $spsd
     */
    public function removeSpsd(\AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase $spsd)
    {
        $this->spsd->removeElement($spsd);
    }

    /**
     * Get spsd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpsd()
    {
        return $this->spsd;
    }

    /**
     * Add spcdd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase $spcdd
     * @return SubProjects
     */
    public function addSpcdd(\AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase $spcdd)
    {
        $this->spcdd[] = $spcdd;

        return $this;
    }

    /**
     * Remove spcdd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase $spcdd
     */
    public function removeSpcdd(\AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase $spcdd)
    {
        $this->spcdd->removeElement($spcdd);
    }

    /**
     * Get spcdd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpcdd()
    {
        return $this->spcdd;
    }

    /**
     * Add spcd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $spcd
     * @return SubProjects
     */
    public function addSpcd(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $spcd)
    {
        $this->spcd[] = $spcd;

        return $this;
    }

    /**
     * Remove spcd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $spcd
     */
    public function removeSpcd(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $spcd)
    {
        $this->spcd->removeElement($spcd);
    }

    /**
     * Get spcd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpcd()
    {
        return $this->spcd;
    }

    /**
     * Add spprd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase $spprd
     * @return SubProjects
     */
    public function addSpprd(\AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase $spprd)
    {
        $this->spprd[] = $spprd;

        return $this;
    }

    /**
     * Remove spprd
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase $spprd
     */
    public function removeSpprd(\AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase $spprd)
    {
        $this->spprd->removeElement($spprd);
    }

    /**
     * Get spprd
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpprd()
    {
        return $this->spprd;
    }
}
