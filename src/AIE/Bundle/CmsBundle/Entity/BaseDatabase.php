<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\BaseDatabaseRepository")
 */
class BaseDatabase {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceid", type="integer", length=11)
     */
    protected $serviceid;

    /**
     * @var integer
     *
     * @ORM\Column(name="samplingorientationid", type="integer", length=11)
     */
    protected $samplingorientationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="activitypersonid", type="integer", length=11)
     */
    protected $activitypersonid;


    /**
     * @var integer
     *
     * @ORM\Column(name="installeddeviceid", type="integer", length=11)
     */
    protected $installeddeviceid;


    /**
     * @var integer
     *
     * @ORM\Column(name="corrosionorientationid", type="integer", length=11)
     */
    protected $corrosionorientationid;


    /**
     * @var integer
     *
     * @ORM\Column(name="accessfittingtypeid", type="integer", length=11)
     */
    protected $accessfittingtypeid;


    /**
     * @var integer
     *
     * @ORM\Column(name="teefittingid", type="integer", length=11)
     */
    protected $teefittingid;

    /**
     * @var integer
     *
     * @ORM\Column(name="holderplugid", type="integer", length=11)
     */
    protected $holderplugid;


    /**
     * @var integer
     *
     * @ORM\Column(name="probetechnologyid", type="integer", length=11)
     */
    protected $probetechnologyid;

    /**
     * @var integer
     *
     * @ORM\Column(name="dataloggerid", type="integer", length=11)
     */
    protected $dataloggerid;

    /**
     * @var integer
     *
     * @ORM\Column(name="couponmaterialid", type="integer", length=11)
     */
    protected $couponmaterialid;


    /**
     * @var integer
     *
     * @ORM\Column(name="densityid", type="integer", length=11)
     */
    protected $densityid;


    /**
     * @var integer
     *
     * @ORM\Column(name="coupontypeid", type="integer", length=11)
     */
    protected $coupontypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="areaid", type="integer", length=11)
     */
    protected $areaid;


     /**
     * @var integer
     *
     * @ORM\Column(name="insulationid", type="integer", length=11)
     */
    protected $insulationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="surfacefinishid", type="integer", length=11)
     */
    protected $surfacefinishid;


    /**
     * @var integer
     *
     * @ORM\Column(name="onlineid", type="integer", length=11)
     */
    protected $onlineid;


    /**
     * @var integer
     *
     * @ORM\Column(name="scaffoldid", type="integer", length=11)
     */
    protected $scaffoldid;


    /**
     * @var integer
     *
     * @ORM\Column(name="accessid", type="integer", length=11)
     */
    protected $accessid;


    /**
     * @var integer
     *
     * @ORM\Column(name="chemicaltypeid", type="integer", length=11)
     */
    protected $chemicaltypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="pumptypeid", type="integer", length=11)
     */
    protected $pumptypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="injectionmethodid", type="integer", length=11)
     */
    protected $injectionmethodid;


    /**
     * @var integer
     *
     * @ORM\Column(name="locationid", type="integer", length=11)
     */
    protected $locationid;


    /**
     * @var integer
     *
     * @ORM\Column(name="cptypeid", type="integer", length=11)
     */
    protected $cptypeid;


    /**
     * @var integer
     *
     * @ORM\Column(name="equipmenttypeid", type="integer", length=11)
     */
    protected $equipmenttypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="anodetypeid", type="integer", length=11)
     */
    protected $anodetypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitoringmethodid", type="integer", length=11)
     */
    protected $monitoringmethodid;

    /**
     * @var integer
     *
     * @ORM\Column(name="referenceelectrodeid", type="integer", length=11)
     */
    protected $referenceelectrodeid;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceid
     *
     * @param integer $serviceid
     * @return BaseDatabase
     */
    public function setServiceid($serviceid)
    {
        $this->serviceid = $serviceid;

        return $this;
    }

    /**
     * Get serviceid
     *
     * @return integer 
     */
    public function getServiceid()
    {
        return $this->serviceid;
    }

    /**
     * Set samplingorientationid
     *
     * @param integer $samplingorientationid
     * @return BaseDatabase
     */
    public function setSamplingorientationid($samplingorientationid)
    {
        $this->samplingorientationid = $samplingorientationid;

        return $this;
    }

    /**
     * Get samplingorientationid
     *
     * @return integer 
     */
    public function getSamplingorientationid()
    {
        return $this->samplingorientationid;
    }

    /**
     * Set activitypersonid
     *
     * @param integer $activitypersonid
     * @return BaseDatabase
     */
    public function setActivitypersonid($activitypersonid)
    {
        $this->activitypersonid = $activitypersonid;

        return $this;
    }

    /**
     * Get activitypersonid
     *
     * @return integer 
     */
    public function getActivitypersonid()
    {
        return $this->activitypersonid;
    }

    /**
     * Set installeddeviceid
     *
     * @param integer $installeddeviceid
     * @return BaseDatabase
     */
    public function setInstalleddeviceid($installeddeviceid)
    {
        $this->installeddeviceid = $installeddeviceid;

        return $this;
    }

    /**
     * Get installeddeviceid
     *
     * @return integer 
     */
    public function getInstalleddeviceid()
    {
        return $this->installeddeviceid;
    }

    /**
     * Set corrosionorientationid
     *
     * @param integer $corrosionorientationid
     * @return BaseDatabase
     */
    public function setCorrosionorientationid($corrosionorientationid)
    {
        $this->corrosionorientationid = $corrosionorientationid;

        return $this;
    }

    /**
     * Get corrosionorientationid
     *
     * @return integer 
     */
    public function getCorrosionorientationid()
    {
        return $this->corrosionorientationid;
    }

    /**
     * Set accessfittingtypeid
     *
     * @param integer $accessfittingtypeid
     * @return BaseDatabase
     */
    public function setAccessfittingtypeid($accessfittingtypeid)
    {
        $this->accessfittingtypeid = $accessfittingtypeid;

        return $this;
    }

    /**
     * Get accessfittingtypeid
     *
     * @return integer 
     */
    public function getAccessfittingtypeid()
    {
        return $this->accessfittingtypeid;
    }

    /**
     * Set teefittingid
     *
     * @param integer $teefittingid
     * @return BaseDatabase
     */
    public function setTeefittingid($teefittingid)
    {
        $this->teefittingid = $teefittingid;

        return $this;
    }

    /**
     * Get teefittingid
     *
     * @return integer 
     */
    public function getTeefittingid()
    {
        return $this->teefittingid;
    }

    /**
     * Set holderplugid
     *
     * @param integer $holderplugid
     * @return BaseDatabase
     */
    public function setHolderplugid($holderplugid)
    {
        $this->holderplugid = $holderplugid;

        return $this;
    }

    /**
     * Get holderplugid
     *
     * @return integer 
     */
    public function getHolderplugid()
    {
        return $this->holderplugid;
    }

    /**
     * Set probetechnologyid
     *
     * @param integer $probetechnologyid
     * @return BaseDatabase
     */
    public function setProbetechnologyid($probetechnologyid)
    {
        $this->probetechnologyid = $probetechnologyid;

        return $this;
    }

    /**
     * Get probetechnologyid
     *
     * @return integer 
     */
    public function getProbetechnologyid()
    {
        return $this->probetechnologyid;
    }

    /**
     * Set dataloggerid
     *
     * @param integer $dataloggerid
     * @return BaseDatabase
     */
    public function setDataloggerid($dataloggerid)
    {
        $this->dataloggerid = $dataloggerid;

        return $this;
    }

    /**
     * Get dataloggerid
     *
     * @return integer 
     */
    public function getDataloggerid()
    {
        return $this->dataloggerid;
    }

    /**
     * Set couponmaterialid
     *
     * @param integer $couponmaterialid
     * @return BaseDatabase
     */
    public function setCouponmaterialid($couponmaterialid)
    {
        $this->couponmaterialid = $couponmaterialid;

        return $this;
    }

    /**
     * Get couponmaterialid
     *
     * @return integer 
     */
    public function getCouponmaterialid()
    {
        return $this->couponmaterialid;
    }

    /**
     * Set densityid
     *
     * @param integer $densityid
     * @return BaseDatabase
     */
    public function setDensityid($densityid)
    {
        $this->densityid = $densityid;

        return $this;
    }

    /**
     * Get densityid
     *
     * @return integer 
     */
    public function getDensityid()
    {
        return $this->densityid;
    }

    /**
     * Set coupontypeid
     *
     * @param integer $coupontypeid
     * @return BaseDatabase
     */
    public function setCoupontypeid($coupontypeid)
    {
        $this->coupontypeid = $coupontypeid;

        return $this;
    }

    /**
     * Get coupontypeid
     *
     * @return integer 
     */
    public function getCoupontypeid()
    {
        return $this->coupontypeid;
    }

    /**
     * Set areaid
     *
     * @param integer $areaid
     * @return BaseDatabase
     */
    public function setAreaid($areaid)
    {
        $this->areaid = $areaid;

        return $this;
    }

    /**
     * Get areaid
     *
     * @return integer 
     */
    public function getAreaid()
    {
        return $this->areaid;
    }

    /**
     * Set insulationid
     *
     * @param integer $insulationid
     * @return BaseDatabase
     */
    public function setInsulationid($insulationid)
    {
        $this->insulationid = $insulationid;

        return $this;
    }

    /**
     * Get insulationid
     *
     * @return integer 
     */
    public function getInsulationid()
    {
        return $this->insulationid;
    }

    /**
     * Set surfacefinishid
     *
     * @param integer $surfacefinishid
     * @return BaseDatabase
     */
    public function setSurfacefinishid($surfacefinishid)
    {
        $this->surfacefinishid = $surfacefinishid;

        return $this;
    }

    /**
     * Get surfacefinishid
     *
     * @return integer 
     */
    public function getSurfacefinishid()
    {
        return $this->surfacefinishid;
    }

    /**
     * Set onlineid
     *
     * @param integer $onlineid
     * @return BaseDatabase
     */
    public function setOnlineid($onlineid)
    {
        $this->onlineid = $onlineid;

        return $this;
    }

    /**
     * Get onlineid
     *
     * @return integer 
     */
    public function getOnlineid()
    {
        return $this->onlineid;
    }

    /**
     * Set scaffoldid
     *
     * @param integer $scaffoldid
     * @return BaseDatabase
     */
    public function setScaffoldid($scaffoldid)
    {
        $this->scaffoldid = $scaffoldid;

        return $this;
    }

    /**
     * Get scaffoldid
     *
     * @return integer 
     */
    public function getScaffoldid()
    {
        return $this->scaffoldid;
    }

    /**
     * Set accessid
     *
     * @param integer $accessid
     * @return BaseDatabase
     */
    public function setAccessid($accessid)
    {
        $this->accessid = $accessid;

        return $this;
    }

    /**
     * Get accessid
     *
     * @return integer 
     */
    public function getAccessid()
    {
        return $this->accessid;
    }

    /**
     * Set chemicaltypeid
     *
     * @param integer $chemicaltypeid
     * @return BaseDatabase
     */
    public function setChemicaltypeid($chemicaltypeid)
    {
        $this->chemicaltypeid = $chemicaltypeid;

        return $this;
    }

    /**
     * Get chemicaltypeid
     *
     * @return integer 
     */
    public function getChemicaltypeid()
    {
        return $this->chemicaltypeid;
    }

    /**
     * Set pumptypeid
     *
     * @param integer $pumptypeid
     * @return BaseDatabase
     */
    public function setPumptypeid($pumptypeid)
    {
        $this->pumptypeid = $pumptypeid;

        return $this;
    }

    /**
     * Get pumptypeid
     *
     * @return integer 
     */
    public function getPumptypeid()
    {
        return $this->pumptypeid;
    }

    /**
     * Set injectionmethodid
     *
     * @param integer $injectionmethodid
     * @return BaseDatabase
     */
    public function setInjectionmethodid($injectionmethodid)
    {
        $this->injectionmethodid = $injectionmethodid;

        return $this;
    }

    /**
     * Get injectionmethodid
     *
     * @return integer 
     */
    public function getInjectionmethodid()
    {
        return $this->injectionmethodid;
    }

    /**
     * Set locationid
     *
     * @param integer $locationid
     * @return BaseDatabase
     */
    public function setLocationid($locationid)
    {
        $this->locationid = $locationid;

        return $this;
    }

    /**
     * Get locationid
     *
     * @return integer 
     */
    public function getLocationid()
    {
        return $this->locationid;
    }

    /**
     * Set cptypeid
     *
     * @param integer $cptypeid
     * @return BaseDatabase
     */
    public function setCptypeid($cptypeid)
    {
        $this->cptypeid = $cptypeid;

        return $this;
    }

    /**
     * Get cptypeid
     *
     * @return integer 
     */
    public function getCptypeid()
    {
        return $this->cptypeid;
    }

    /**
     * Set equipmenttypeid
     *
     * @param integer $equipmenttypeid
     * @return BaseDatabase
     */
    public function setEquipmenttypeid($equipmenttypeid)
    {
        $this->equipmenttypeid = $equipmenttypeid;

        return $this;
    }

    /**
     * Get equipmenttypeid
     *
     * @return integer 
     */
    public function getEquipmenttypeid()
    {
        return $this->equipmenttypeid;
    }

    /**
     * Set anodetypeid
     *
     * @param integer $anodetypeid
     * @return BaseDatabase
     */
    public function setAnodetypeid($anodetypeid)
    {
        $this->anodetypeid = $anodetypeid;

        return $this;
    }

    /**
     * Get anodetypeid
     *
     * @return integer 
     */
    public function getAnodetypeid()
    {
        return $this->anodetypeid;
    }

    /**
     * Set monitoringmethodid
     *
     * @param integer $monitoringmethodid
     * @return BaseDatabase
     */
    public function setMonitoringmethodid($monitoringmethodid)
    {
        $this->monitoringmethodid = $monitoringmethodid;

        return $this;
    }

    /**
     * Get monitoringmethodid
     *
     * @return integer 
     */
    public function getMonitoringmethodid()
    {
        return $this->monitoringmethodid;
    }

    /**
     * Set referenceelectrodeid
     *
     * @param integer $referenceelectrodeid
     * @return BaseDatabase
     */
    public function setReferenceelectrodeid($referenceelectrodeid)
    {
        $this->referenceelectrodeid = $referenceelectrodeid;

        return $this;
    }

    /**
     * Get referenceelectrodeid
     *
     * @return integer 
     */
    public function getReferenceelectrodeid()
    {
        return $this->referenceelectrodeid;
    }
}
