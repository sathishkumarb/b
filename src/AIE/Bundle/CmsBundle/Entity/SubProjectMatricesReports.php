<?php

namespace AIE\Bundle\CmsBundle\Entity;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * SubProjectMatricesReports
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\SubProjectMatricesReportsRepository")
 */
class SubProjectMatricesReports extends AbstractFile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="reporttype", type="integer", length=1)
     */
    protected $reporttype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isApproved", type="boolean")
     */
    private $isApproved;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spmr")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     * */
    private $subproject;

    /**
     * @var json_array
     *
     * @ORM\Column(name="assessment_data", type="json_array")
     */
    private $assessmentData;

    /**
     * @var text
     *
     * @ORM\Column(name="html_report", type="text", nullable=true)
     */
    private $htmlReport;

    /**
     * @var text
     *
     * @ORM\Column(name="complianceData", type="text", nullable=true)
     */
    private $complianceData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reportingperiodfromdate", type="datetime")
     */
    private $reportingperiodfromdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reportingperiodtodate", type="datetime")
     */
    private $reportingperiodtodate;

    protected $allowedMimeTypes = [];

    protected $reportChoices = [
        1 => 'Corrosion Management Report With Exception',
        2 => 'Corrosion Management Supplementary Report',
        3 => 'Corrosion Management Comprehensive Report',
        4 => 'Corrosion Management and Flow Assurance Management Matrices',
        5 => 'Corrosion Management Compliance Report',
        6 => 'Flow Assurance Management Comprehensive Report',
        7 => 'Flow Assurance Management Compliance Report',
    ];

    public function __construct() {
        $this->isApproved = false;
        $this->sections = array();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SubProjectMatricesReports
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SubProjectMatricesReports
     */
    public function setReportingFromDate($reportingperiodfromdate) {
        $this->reportingperiodfromdate = $reportingperiodfromdate;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getReportingFromDate() {
        return $this->reportingperiodfromdate;
    }


    /**
     * Set date
     *
     * @param \DateTime $reportingperiodtodate
     * @return SubProjectMatricesReports
     */
    public function setReportingToDate($reportingperiodtodate) {
        $this->reportingperiodtodate = $reportingperiodtodate;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getReportingToDate() {
        return $this->reportingperiodtodate;
    }

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     * @return SubProjectMatricesReports
     */
    public function setIsApproved($isApproved) {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean 
     */
    public function getIsApproved() {
        return $this->isApproved;
    }

    /**
     * Set assessmentData
     *
     * @param array $assessmentData
     * @return SubProjectMatricesReports
     */
    public function setAssessmentData($assessmentData)
    {
        $this->assessmentData = $assessmentData;
    
        return $this;
    }

    /**
     * Get assessmentData
     *
     * @return array 
     */
    public function getAssessmentData()
    {
        return $this->assessmentData;
    }


    /**
     * Set complianceData
     *
     * @param array $complianceData
     * @return SubProjectMatricesReports
     */
    public function setComplianceData($complianceData)
    {
        $this->complianceData = $complianceData;

        return $this;
    }

    /**
     * Get complianceData
     *
     * @return array
     */
    public function getComplianceData()
    {
        return $this->complianceData;
    }

    public function getUploadDir()
    {
        return 'assessments';
    }


    /**
     * Set htmlReport
     *
     * @param string $htmlReport
     * @return SubProjectMatricesReports
     */
    public function setHtmlReport($htmlReport)
    {
        $this->htmlReport = $htmlReport;
    
        return $this;
    }

    /**
     * Get htmlReport
     *
     * @return string 
     */
    public function getHtmlReport()
    {
        return $this->htmlReport;
    }


    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectMatricesReports
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects 
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set fromdate
     *
     * @param \DateTime $fromdate
     * @return SubProjectMatricesReports
     */
    public function setFromdate($fromdate)
    {
        $this->fromdate = $fromdate;

        return $this;
    }

    /**
     * Get fromdate
     *
     * @return \DateTime 
     */
    public function getFromdate()
    {
        return $this->fromdate;
    }

    /**
     * Set todate
     *
     * @param \DateTime $todate
     * @return SubProjectMatricesReports
     */
    public function setTodate($todate)
    {
        $this->todate = $todate;

        return $this;
    }

    /**
     * Get todate
     *
     * @return \DateTime 
     */
    public function getTodate()
    {
        return $this->todate;
    }

    /**
     * Set reporttype
     *
     * @param integer $reporttype
     * @return SubProjectMatricesReports
     */
    public function setReporttype($reporttype)
    {
        $this->reporttype = $reporttype;

        return $this;
    }

    /**
     * Get reporttype
     *
     * @return integer 
     */
    public function getReporttype()
    {
        return $this->reporttype;
    }
}
