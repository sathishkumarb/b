<?php

namespace AIE\Bundle\CmsBundle\Entity;

use AIE\Bundle\VeracityBundle\Component\Files\FilesCategories as BaseFilesCategories;
use Doctrine\ORM\Mapping as ORM;

/**
 * FilesCategories
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\FilesCategoriesRepository")
 */
class FilesCategories {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="filescategories")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     * */
    private $subproject;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="FilesCategories", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="FilesCategories", mappedBy="parent", fetch="EXTRA_LAZY")
     */
    protected $children;

    /**
     * @ORM\OneToMany(targetEntity="Files" , mappedBy="category" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pfc;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FilesCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pfc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSystems
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set parent
     *
     * @param \AIE\Bundle\CmsBundle\Entity\FilesCategories $parent
     * @return FilesCategories
     */
    public function setParent(\AIE\Bundle\CmsBundle\Entity\FilesCategories $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AIE\Bundle\CmsBundle\Entity\FilesCategories
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \AIE\Bundle\CmsBundle\Entity\FilesCategories $children
     * @return FilesCategories
     */
    public function addChildren(\AIE\Bundle\CmsBundle\Entity\FilesCategories $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AIE\Bundle\CmsBundle\Entity\FilesCategories $children
     */
    public function removeChildren(\AIE\Bundle\CmsBundle\Entity\FilesCategories $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add pfc
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $pfc
     * @return FilesCategories
     */
    public function addPfc(\AIE\Bundle\CmsBundle\Entity\Files $pfc)
    {
        $this->pfc[] = $pfc;

        return $this;
    }

    /**
     * Remove pfc
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $pfc
     */
    public function removePfc(\AIE\Bundle\CmsBundle\Entity\Files $pfc)
    {
        $this->pfc->removeElement($pfc);
    }

    /**
     * Get pfc
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPfc()
    {
        return $this->pfc;
    }


    public function getAsTreeNode(&$files, $fileuploader)
    {

        $node = [
            'id'       => $this->getId(),
            'title'    => $this->getName(),
            'key'      => $this->getId(),
            'folder'   => true,
            'children' => []
        ];

        $children = $this->getChildren();
        $childrenArr = [];

        foreach ($children as $child)
        {
            $childrenArr[] = $child->getAsTreeNode($files, $fileuploader);
        }

        foreach ($files as $key => $file)
        {
            if ($file->getCategory() && $file->getCategory()->getId() == $this->getId())
            {
                $childrenArr[] = $file->getAsTreeNode($fileuploader);
                unset($files[$key]);
            }
        }

        $node['children'] = $childrenArr;

        return $node;
    }
}
