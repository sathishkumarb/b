<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseDatasheetType
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BaseDatasheetType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=32)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="text")
     */
    private $desc;

    /**
     * @ORM\OneToMany(targetEntity="BaseMetricsConfig" , mappedBy="databasesheettype" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $datasheetmetrics;

    /**
     * @ORM\OneToMany(targetEntity="BaseActivity" , mappedBy="basedatasheet" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $baseactivity;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectActivity" , mappedBy="subprojectdatasheet" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectactivity;

    public function __construct()
    {
        $this->datasheetmetrics = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get Id
     *
     * @return BaseDatasheet
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param title
     * @return BaseDatasheet
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseDatasheet
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set desc
     *
     * @param desc
     * @return BaseDatasheet
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return BaseDatasheet
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * Get basemetrics
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig
     */
    public function getDatasheetMetrics()
    {
        return $this->datasheetmetrics;
    }


    function __toString() {
        return $this->getTitle();
    }
}
