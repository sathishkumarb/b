<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseThreats
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseThreats {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="BaseSystems", inversedBy="basethreats")
     * @ORM\JoinColumn(name="basesystemid", referencedColumnName="id")
     */
    private $basesystems;

    /**
     * @ORM\OneToMany(targetEntity="BaseMitigationTechniques" , mappedBy="basethreats" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $basemitigationtechniques;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    protected $threatstitle;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function __construct()
    {
        $this->basemitigationtechniques = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Set basesystems
     *
     * \AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype
     * @return BaseThreats
     */
    public function setBaseSystems(\AIE\Bundle\CmsBundle\Entity\BaseSystems $basesystems = null) {
        $this->basesystems = $basesystems;

        return $this;
    }

    /**
     * get basesystems
     *
     * @param basesystems
     * @return BaseThreats
     */
    public function getBaseSystems()
    {

        return $this->basesystems;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseThreats
     */
    public function setThreatsTitle($threatstitle)
    {
        $this->threatstitle = $threatstitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseThreats
     */
    public function getThreatsTitle()
    {
        return $this->threatstitle;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return BaseThreats
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return BaseThreats
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
