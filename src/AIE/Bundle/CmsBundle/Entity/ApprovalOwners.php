<?php

namespace AIE\Bundle\CmsBundle\Entity;

use AIE\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ApprovalOwners
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ApprovalOwners {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface", cascade={"persist"})
	 * @ORM\JoinColumn(name="action_owner_id", referencedColumnName="id")
	 */
	private $actionOwner;

	/**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="approvalOwners")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	private $project;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="actionOwners")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     *
     */
    protected $subproject;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set actionOwner
	 *
	 * @param integer $actionOwner
	 * @return ApprovalOwners
	 */
	public function setActionOwner($actionOwner) {
		$this->actionOwner = $actionOwner;

		return $this;
	}

	/**
	 * Get actionOwner
	 *
	 * @return integer
	 */
	public function getActionOwner() {
		return $this->actionOwner;
	}


	/**
	 * Set project
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
	 * @return ApprovalOwners
	 */
	public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null) {
		$this->project = $project;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Set anomalyCategory
	 *
	 * @param string $anomalyCategory
	 * @return ApprovalOwners
	 */
	public function setAnomalyCategory($anomalyCategory) {
		$this->anomalyCategory = $anomalyCategory;

		return $this;
	}

	/**
	 * Get anomalyCategory
	 *
	 * @return string
	 */
	public function getAnomalyCategory() {
		return $this->anomalyCategory;
	}

    public function setUser($user) {
		//$this->user = $user->getId();
		$this->user = $user;

		return $this;
	}

	public function getUser() {
		return $this->user;
	}

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return ApprovalOwners
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
