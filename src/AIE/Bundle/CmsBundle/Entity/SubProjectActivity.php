<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;

use AIE\Bundle\UserBundle\Entity\User as User;
//use function GuzzleHttp\default_ca_bundle;
use JMS\Serializer\Annotation as JMS;

/**
 * SubProjectActivity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\SubProjectActivityRepository")
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectActivity
{
    private $_choices = ['R'=>'Red', 'A'=>'Amber', 'G'=>'Green','H'=>'Hold'];
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var double
     *
     * @ORM\Column(name="frequency", type="decimal", scale=2)
     * @JMS\Type("decimal")
     */
    protected $frequency = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="activityperson", type="string", length=120)
     * @JMS\Type("string")
     */
    protected $activityperson;

    /**
     * @var string
     *
     * @ORM\Column(name="weightage", type="integer", length=10)
     * @JMS\Type("string")
     */
    protected $weightage;

    /**
     * @var string
     *
     * @ORM\Column(name="consequences_excursion", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $consequencesexcursion;

    /**
     * @var string
     *
     * @ORM\Column(name="remedialaction", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $remedialaction;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectMitigationTechniques", inversedBy="subprojectactivity")
     * @ORM\JoinColumn(name="subprojectmitigationtechniqueid", referencedColumnName="id")
     */
    private $subprojectmitigationtechniques;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="systems")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="BaseMetricsConfig", inversedBy="subprojectactivity")
     * @ORM\JoinColumn(name="subprojectmetricid", referencedColumnName="id")
     */
    private $subprojectmetrics;

    /**
     * @ORM\ManyToOne(targetEntity="BaseDatasheetType", inversedBy="subprojectactivity")
     * @ORM\JoinColumn(name="subprojectdatasheetid", referencedColumnName="id")
     */
    protected $subprojectdatasheet;

    /**
     * @ORM\ManyToMany(targetEntity="Files")
     * @ORM\JoinTable(name="sub_project_activity_files",
     *      joinColumns={@ORM\JoinColumn(name="spa_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id",onDelete="CASCADE")}
     * )
     */
    protected $files;


    /**
     * @ORM\ManyToOne(targetEntity="ActionOwners", inversedBy="cao")
     * @ORM\JoinColumn(name="action_owner", referencedColumnName="id", nullable=true)
     * */
    protected $action_owner;

    /**
     * @var string
     *
     * @ORM\Column(name="managementtitle", type="string", length=150)
     * @JMS\Type("string")
     */
    protected $managementtitle;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="managementgreen", type="string", length=100)
     * @JMS\Type("string")
     */
    protected $managementgreen;

    /**
     * @var string
     *
     * @ORM\Column(name="managementamber", type="string", length=100, nullable=true)
     * @JMS\Type("string")
     */
    protected $managementamber;

    /**
     * @var string
     *
     * @ORM\Column(name="managementred", type="string", length=100)
     * @JMS\Type("string")
     */
    protected $managementred;

    /**
     * @var string
     *
     * @ORM\Column(name="controltitle", type="string", length=150)
     * @JMS\Type("string")
     */
    protected $controltitle;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreenexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenexpression;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreen", type="string", length=20)
     * @JMS\Type("string")
     */
    protected $controlgreen;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreenexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlgreenrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenrange;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlamberexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberexpression;

    /**
     * @var string
     *
     * @ORM\Column(name="controlamber", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlamberexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlamberrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberrange;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlredexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredexpression;

    /**
     * @var string
     *
     * @ORM\Column(name="controlred", type="string", length=20)
     * @JMS\Type("string")
     */
    protected $controlred;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlredexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlredrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredrange;

    /**
     * @var string
     *
     * @ORM\Column(name="justification_management", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $justification_management;

    /**
     * @var string
     *
     * @ORM\Column(name="justification_control", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $justification_control;

    /**
     * @var string
     *
     * @ORM\Column(name="kpi_control", type="string", length=3, nullable=true)
     * @JMS\Type("string")
     */
    protected $kpi_control;

    /**
     * @var string
     *
     * @ORM\Column(name="kpi_management", type="string", length=3, nullable=true)
     * @JMS\Type("string")
     */
    protected $kpi_management;

    /**
     * @ORM\Column(name="base_mitigationtechnique_id", type="integer", nullable=true)
     */
    private $basemitigationtechniqueid;

    /**
     * @ORM\Column(name="base_activity_id", type="integer", nullable=true)
     */
    private $baseactivityid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $ismanagement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $iscontrol;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isnonmonitoringkpi;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isflowassurancemanagement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isflowassurancecontrol;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $matrixgrouporder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_inspection_date", type="datetime", nullable=true)
     */
    private $last_inspection_date;

    /**
     *
     * @ORM\Column(name="last_modified_frequency", type="integer", nullable=true)
     */
    private $last_modified_frequency; //-1 not defined yet


    private $next_inspection_date = null;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $comments;

    /**
     * @var array
     *
     * @ORM\Column(name="reminded_on", type="json_array", nullable=true)
     */
    private $remindedOn;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    private $systems;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCorrosionDatasheet" , mappedBy="activityid", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $corrosiondatasheet;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $description;

    const KPI_RED_STATUS_VALUE = 0;
    const KPI_AMBER_STATUS_VALUE = 0.5;
    const KPI_GREEN_STATUS_VALUE = 1;

    public function __construct()
    {
        $this->files = [];
        $this->datasheetmetrics = new \Doctrine\Common\Collections\ArrayCollection();
        $this->corrosiondatasheet = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSystems
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }


    public function setSystems($systems)
    {
        $this->systems = $systems;

        return $this;
    }


    public function getSystems()
    {
        return $this->systems;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     * @return SubProjectActivity
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set activityperson
     *
     * @param string $activityperson
     * @return SubProjectActivity
     */
    public function setActivityperson($activityperson)
    {
        $this->activityperson = $activityperson;

        return $this;
    }

    /**
     * Get activityperson
     *
     * @return string
     */
    public function getActivityperson()
    {
        return $this->activityperson;
    }

    /**
     * Set weightage
     *
     * @param string $weightage
     * @return SubProjectActivity
     */
    public function setWeightage($weightage)
    {
        $this->weightage = $weightage;

        return $this;
    }

    /**
     * Get weightage
     *
     * @return string
     */
    public function getWeightage()
    {
        return $this->weightage;
    }

    /**
     * Set consequencesexcursion
     *
     * @param string $consequencesexcursion
     * @return SubProjectActivity
     */
    public function setConsequencesexcursion($consequencesexcursion)
    {
        $this->consequencesexcursion = $consequencesexcursion;

        return $this;
    }

    /**
     * Get consequencesexcursion
     *
     * @return string
     */
    public function getConsequencesexcursion()
    {
        return $this->consequencesexcursion;
    }

    /**
     * Set remedialaction
     *
     * @param string $remedialaction
     * @return SubProjectActivity
     */
    public function setRemedialaction($remedialaction)
    {
        $this->remedialaction = $remedialaction;

        return $this;
    }

    /**
     * Get remedialaction
     *
     * @return string
     */
    public function getRemedialaction()
    {
        return $this->remedialaction;
    }

    /**
     * Set remindedOn
     *
     * @param array $remindedOn
     * @return SubProjectActivity
     */
    public function setRemindedOn($remindedOn)
    {
        $this->remindedOn = $remindedOn;

        return $this;
    }

    /**
     * Get remindedOn
     *
     * @return array
     */
    public function getRemindedOn()
    {
        return $this->remindedOn;
    }


    /**
     * Set subprojectmitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseMitigationTechniques $subprojectmitigationtechniques
     * @return SubProjectActivity
     */
    public function setSubProjectMitigationTechniques(\AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $subprojectmitigationtechniques = null)
    {
        $this->subprojectmitigationtechniques = $subprojectmitigationtechniques;

        return $this;
    }

    /**
     * Get subprojectmitigationtechniques
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques
     */
    public function getSubProjectMitigationTechniques()
    {
        return $this->subprojectmitigationtechniques;
    }

    /**
     * Set managementtitle
     *
     * @param string $managementtitle
     * @return SubProjectActivity
     */
    public function setManagementtitle($managementtitle)
    {
        $this->managementtitle = $managementtitle;

        return $this;
    }

    /**
     * Get managementtitle
     *
     * @return string
     */
    public function getManagementtitle()
    {
        return $this->managementtitle;
    }

    /**
     * Set managementgreen
     *
     * @param string $managementgreen
     * @return SubProjectActivity
     */
    public function setManagementgreen($managementgreen)
    {
        $this->managementgreen = $managementgreen;

        return $this;
    }

    /**
     * Get managementgreen
     *
     * @return string
     */
    public function getManagementgreen()
    {
        return $this->managementgreen;
    }

    /**
     * Set managementamber
     *
     * @param string $managementamber
     * @return SubProjectActivity
     */
    public function setManagementamber($managementamber)
    {
        $this->managementamber = $managementamber;

        return $this;
    }

    /**
     * Get managementamber
     *
     * @return string
     */
    public function getManagementamber()
    {
        return $this->managementamber;
    }

    /**
     * Set managementred
     *
     * @param string $managementred
     * @return SubProjectActivity
     */
    public function setManagementred($managementred)
    {
        $this->managementred = $managementred;

        return $this;
    }

    /**
     * Get managementred
     *
     * @return string
     */
    public function getManagementred()
    {
        return $this->managementred;
    }

    /**
     * Set controltitle
     *
     * @param string $controltitle
     * @return SubProjectActivity
     */
    public function setControltitle($controltitle)
    {
        $this->controltitle = $controltitle;

        return $this;
    }

    /**
     * Get controltitle
     *
     * @return string
     */
    public function getControltitle()
    {
        return $this->controltitle;
    }

    /**
     * Set controlgreenexpression
     *
     * @param string $controlgreenexpression
     * @return SubProjectActivity
     */
    public function setControlgreenexpression($controlgreenexpression)
    {
        $this->controlgreenexpression = $controlgreenexpression;

        return $this;
    }

    /**
     * Get controlgreenexpression
     *
     * @return string
     */
    public function getControlgreenexpression()
    {
        return $this->controlgreenexpression;
    }

    /**
     * Set controlgreen
     *
     * @param string $controlgreen
     * @return SubProjectActivity
     */
    public function setControlgreen($controlgreen)
    {
        $this->controlgreen = $controlgreen;

        return $this;
    }

    /**
     * Get controlgreen
     *
     * @return string
     */
    public function getControlgreen()
    {
        return $this->controlgreen;
    }

    /**
     * Set controlamberexpression
     *
     * @param string $controlamberexpression
     * @return SubProjectActivity
     */
    public function setControlamberexpression($controlamberexpression)
    {
        $this->controlamberexpression = $controlamberexpression;

        return $this;
    }

    /**
     * Get controlamberexpression
     *
     * @return string
     */
    public function getControlamberexpression()
    {
        return $this->controlamberexpression;
    }

    /**
     * Set controlamber
     *
     * @param string $controlamber
     * @return SubProjectActivity
     */
    public function setControlamber($controlamber)
    {
        $this->controlamber = $controlamber;

        return $this;
    }

    /**
     * Get controlamber
     *
     * @return string
     */
    public function getControlamber()
    {
        return $this->controlamber;
    }

    /**
     * Set controlredexpression
     *
     * @param string $controlredexpression
     * @return SubProjectActivity
     */
    public function setControlredexpression($controlredexpression)
    {
        $this->controlredexpression = $controlredexpression;

        return $this;
    }

    /**
     * Get controlredexpression
     *
     * @return string
     */
    public function getControlredexpression()
    {
        return $this->controlredexpression;
    }

    /**
     * Set controlred
     *
     * @param string $controlred
     * @return SubProjectActivity
     */
    public function setControlred($controlred)
    {
        $this->controlred = $controlred;

        return $this;
    }

    /**
     * Get controlred
     *
     * @return string
     */
    public function getControlred()
    {
        return $this->controlred;
    }


    /**
     * Set controlgreenexpressionrange
     *
     * @param string $controlgreenexpressionrange
     * @return SubProjectActivity
     */
    public function setControlgreenexpressionrange($controlgreenexpressionrange)
    {
        $this->controlgreenexpressionrange = $controlgreenexpressionrange;

        return $this;
    }

    /**
     * Get controlgreenexpressionrange
     *
     * @return string
     */
    public function getControlgreenexpressionrange()
    {
        return $this->controlgreenexpressionrange;
    }

    /**
     * Set controlgreenrange
     *
     * @param string $controlgreenrange
     * @return SubProjectActivity
     */
    public function setControlgreenrange($controlgreenrange)
    {
        $this->controlgreenrange = $controlgreenrange;

        return $this;
    }

    /**
     * Get controlgreenrange
     *
     * @return string
     */
    public function getControlgreenrange()
    {
        return $this->controlgreenrange;
    }

    /**
     * Set controlamberexpressionrange
     *
     * @param string $controlamberexpressionrange
     * @return SubProjectActivity
     */
    public function setControlamberexpressionrange($controlamberexpressionrange)
    {
        $this->controlamberexpressionrange = $controlamberexpressionrange;

        return $this;
    }

    /**
     * Get controlamberexpressionrange
     *
     * @return string
     */
    public function getControlamberexpressionrange()
    {
        return $this->controlamberexpressionrange;
    }

    /**
     * Set controlamberrange
     *
     * @param string $controlamberrange
     * @return SubProjectActivity
     */
    public function setControlamberrange($controlamberrange)
    {
        $this->controlamberrange = $controlamberrange;

        return $this;
    }

    /**
     * Get controlamberrange
     *
     * @return string
     */
    public function getControlamberrange()
    {
        return $this->controlamberrange;
    }

    /**
     * Set controlredexpressionrange
     *
     * @param string $controlredexpressionrange
     * @return SubProjectActivity
     */
    public function setControlredexpressionrange($controlredexpressionrange)
    {
        $this->controlredexpressionrange = $controlredexpressionrange;

        return $this;
    }

    /**
     * Get controlredexpressionrange
     *
     * @return string
     */
    public function getControlredexpressionrange()
    {
        return $this->controlredexpressionrange;
    }

    /**
     * Set controlredrange
     *
     * @param string $controlredrange
     * @return SubProjectActivity
     */
    public function setControlredrange($controlredrange)
    {
        $this->controlredrange = $controlredrange;

        return $this;
    }

    /**
     * Get controlredrange
     *
     * @return string
     */
    public function getControlredrange()
    {
        return $this->controlredrange;
    }

    /**
     * Add SubProjectActivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity
     * @return SubProjectActivity
     */
    public function addSubProjectActivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity)
    {
        $this->subprojectactivity[] = $subprojectactivity;

        return $this;
    }

    /**
     * Remove SubProjectActivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity
     */
    public function removeSubProjectActivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity)
    {
        $this->subprojectactivity->removeElement($subprojectactivity);
    }

    /**
     * Get SubProjectActivity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubProjectActivity()
    {
        return $this->subprojectactivity;
    }

    /**
     * Set subprojectmetrics
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig $subprojectmetrics
     * @return SubProjectActivity
     */
    public function setSubprojectmetrics(\AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig $subprojectmetrics = null)
    {
        $this->subprojectmetrics = $subprojectmetrics;

        return $this;
    }

    /**
     * Get subprojectmetrics
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig
     */
    public function getSubprojectmetrics()
    {
        return $this->subprojectmetrics;
    }

    /**
     * Set subprojectdatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $subprojectdatasheet
     * @return SubProjectActivity
     */
    public function setSubprojectdatasheet(\AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $subprojectdatasheet = null)
    {
        $this->subprojectdatasheet = $subprojectdatasheet;

        return $this;
    }

    /**
     * Get subprojectdatasheet
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseDatasheetType
     */
    public function getSubprojectdatasheet()
    {
        return $this->subprojectdatasheet;
    }

    /**
     * Set basemitigationtechniqueid
     *
     * @param integer $basemitigationtechniqueid
     * @return SubProjectMitigationTechniques
     */
    public function setBasemitigationtechniqueid($basemitigationtechniqueid)
    {
        $this->basemitigationtechniqueid = $basemitigationtechniqueid;

        return $this;
    }

    /**
     * Get basemitigationtechniqueid
     *
     * @return integer
     */
    public function getBasemitigationtechniqueid()
    {
        return $this->basemitigationtechniqueid;
    }

    /**
     * Set baseactivityid
     *
     * @param integer $baseactivityid
     * @return SubProjectActivity
     */
    public function setBaseactivityid($baseactivityid)
    {
        $this->baseactivityid = $baseactivityid;

        return $this;
    }

    /**
     * Get baseactivityid
     *
     * @return integer
     */
    public function getBaseactivityid()
    {
        return $this->baseactivityid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Set ismanagement
     *
     * @param boolean $ismanagement
     * @return SubProjectActivity
     */
    public function setIsmanagement($ismanagement)
    {
        $this->ismanagement = $ismanagement;

        return $this;
    }

    /**
     * Get ismanagement
     *
     * @return boolean
     */
    public function getIsmanagement()
    {
        return $this->ismanagement;
    }

    /**
     * Set iscontrol
     *
     * @param boolean $iscontrol
     * @return SubProjectActivity
     */
    public function setIscontrol($iscontrol)
    {
        $this->iscontrol = $iscontrol;

        return $this;
    }

    /**
     * Get iscontrol
     *
     * @return boolean
     */
    public function getIscontrol()
    {
        return $this->iscontrol;
    }

    /**
     * Set isnonmonitoringkpi
     *
     * @param boolean $isnonmonitoringkpi
     * @return SubProjectActivity
     */
    public function setIsnonmonitoringkpi($isnonmonitoringkpi)
    {
        $this->isnonmonitoringkpi = $isnonmonitoringkpi;

        return $this;
    }

    /**
     * Get isnonmonitoringkpi
     *
     * @return boolean
     */
    public function getIsnonmonitoringkpi()
    {
        return $this->isnonmonitoringkpi;
    }

    /**
 * Set isflowassurancemanagement
 *
 * @param boolean $isflowassurancemanagement
 * @return SubProjectActivity
 */
    public function setIsflowassuranceManagement($isflowassurancemanagement)
    {
        $this->isflowassurancemanagement = $isflowassurancemanagement;

        return $this;
    }

    /**
     * Get isflowassurance
     *
     * @return boolean
     */
    public function getIsflowassuranceManagement()
    {
        return $this->isflowassurancemanagement;
    }

    /**
     * Set isflowassurancecontrol
     *
     * @param boolean $isflowassurancecontrol
     * @return SubProjectActivity
     */
    public function setIsflowassuranceControl($isflowassurancecontrol)
    {
        $this->isflowassurancecontrol = $isflowassurancecontrol;

        return $this;
    }

    /**
     * Get isflowassurancecontrol
     *
     * @return boolean
     */
    public function getIsflowassuranceControl()
    {
        return $this->isflowassurancecontrol;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return SubProjectActivity
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get subprojectthreats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectthreats()
    {
        return $this->subprojectthreats;
    }

    /**
     * Set action_owner
     *
     * @param \AIE\Bundle\CmsBundle\Entity\ActionOwners $actionOwner
     * @return SubProjectActivity
     */
    public function setActionOwner(\AIE\Bundle\CmsBundle\Entity\ActionOwners $actionOwner = null)
    {
        $this->action_owner = $actionOwner;

        return $this;
    }

    /**
     * Get action_owner
     *
     * @return \AIE\Bundle\CmsBundle\Entity\ActionOwners
     */
    public function getActionOwner()
    {
        return $this->action_owner;
    }


    /**
     * Set justification_management
     *
     * @param string $justificationManagement
     * @return SubProjectActivity
     */
    public function setJustificationManagement($justificationManagement)
    {
        $this->justification_management = $justificationManagement;

        return $this;
    }

    /**
     * Get justification_management
     *
     * @return string 
     */
    public function getJustificationManagement()
    {
        return $this->justification_management;
    }

    /**
     * Set justification_control
     *
     * @param string $justificationControl
     * @return SubProjectActivity
     */
    public function setJustificationControl($justificationControl)
    {
        $this->justification_control = $justificationControl;

        return $this;
    }

    /**
     * Get justification_control
     *
     * @return string 
     */
    public function getJustificationControl()
    {
        return $this->justification_control;
    }

    /**
     * Set kpi_control
     *
     * @param string $kpiControl
     * @return SubProjectActivity
     */
    public function setKpiControl($kpiControl)
    {
        $this->kpi_control = $kpiControl;

        return $this;
    }

    /**
     * Get kpi_control
     *
     * @return string 
     */
    public function getKpiControl()
    {
        return $this->kpi_control;
    }

    /**
     * Set kpi_management
     *
     * @param string $kpiManagement
     * @return SubProjectActivity
     */
    public function setKpiManagement($kpiManagement)
    {
        $this->kpi_management = $kpiManagement;

        return $this;
    }

    /**
     * Get kpi_management
     *
     * @return string 
     */
    public function getKpiManagement()
    {
        return $this->kpi_management;
    }

    /**
     * Set last_inspection_date
     *
     * @param \DateTime $lastInspectionDate
     * @return SubProjectActivity
     */
    public function setLastInspectionDate($lastInspectionDate)
    {
        $this->last_inspection_date = $lastInspectionDate;

        return $this;
    }

    /**
     * Get last_inspection_date
     *
     * @return \DateTime 
     */
    public function getLastInspectionDate()
    {
        return $this->last_inspection_date;
    }

    /**
     * Set last_modified_frequency
     *
     * @param integer $lastModifiedFrequency
     * @return SubProjectActivity
     */
    public function setLastModifiedFrequency($lastModifiedFrequency)
    {
        $this->last_modified_frequency = $lastModifiedFrequency;

        return $this;
    }

    /**
     * Get last_modified_frequency
     *
     * @return integer 
     */
    public function getLastModifiedFrequency()
    {
        return $this->last_modified_frequency;
    }

    public function getKpiChoices(){
        return $this->_choices;
    }

    public function getNextInspectionDate($force = false)
    {
        if ($force === true || $this->next_inspection_date == null)
        {
            $this->next_inspection_date = InspectionHelper::calculateNextInspectionDate($this->last_inspection_date, $this->frequency);
        }

        return $this->next_inspection_date;
    }

    public static function nextDateSort($a, $b)
    {
        return $a->getNextInspectionDate() > $b->getNextInspectionDate();
    }

    public function getIsInspectionDue()
    {
        //if today is the next inspection or after that
        $now = new \DateTime('now');

        if ($this->getNextInspectionDate())
            return ($this->getNextInspectionDate()->format('U') <= $now->format('U'));
        else
            return null;
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     * @return SubProjectActivity
     */
    public function addFile(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\CmsBundle\Entity\Files $files
     */
    public function removeFile(\AIE\Bundle\CmsBundle\Entity\Files $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }


    public function getCompliance($switchKpi=0)
    {
        if ($switchKpi){

            if ($switchKpi == 1 ) {
                $kpiControl = $this->kpi_control;
            }
            else
                $kpiControl = $this->kpi_management;

            if ( $kpiControl != null && $kpiControl != "H" ) {

                switch ($kpiControl) {
                    case "R":
                        $kpiValue = Self::KPI_RED_STATUS_VALUE;
                        break;

                    case "A":
                        $kpiValue = Self::KPI_AMBER_STATUS_VALUE;
                        break;

                    case "G":
                        $kpiValue = Self::KPI_GREEN_STATUS_VALUE;
                        break;
                }

                $this->weightage = (empty($this->weightage) ? 0 : $this->weightage);
                $weightageValue = ($this->weightage / 100);

                return InspectionHelper::calculateKpiCompliance($kpiValue, $weightageValue);
            }
        }

    }


    /**
     * Set description
     *
     * @param string $description
     * @return SubProjectActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add corrosiondatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $corrosiondatasheet
     * @return SubProjectActivity
     */
    public function addCorrosiondatasheet(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $corrosiondatasheet)
    {
        $this->corrosiondatasheet[] = $corrosiondatasheet;

        return $this;
    }

    /**
     * Remove corrosiondatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $corrosiondatasheet
     */
    public function removeCorrosiondatasheet(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $corrosiondatasheet)
    {
        $this->corrosiondatasheet->removeElement($corrosiondatasheet);
    }

    /**
     * Get corrosiondatasheet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCorrosiondatasheet()
    {
        return $this->corrosiondatasheet;
    }
    /**
     * Set matrixgrouporder
     *
     * @param integer $matrixgrouporder
     * @return SubProjectActivity
     */
    public function setMatrixgrouporder($matrixgrouporder)
    {
        $this->matrixgrouporder = $matrixgrouporder;

        return $this;
    }

    /**
     * Get matrixgrouporder
     *
     * @return integer
     */
    public function getMatrixgrouporder()
    {
        return $this->matrixgrouporder;
    }

}
