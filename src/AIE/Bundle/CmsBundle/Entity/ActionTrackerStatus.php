<?php
namespace AIE\Bundle\CmsBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ActionTrackerStatus
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */

class ActionTrackerStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Name
     *
     * $name
     * @return ActionTrackerStatus
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * get name
     *
     * @return name
     *
     */
    public function getName()
    {

        return $this->name;
    }
}