<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubProjectCorrosionDatasheet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\SubProjectCorrosionDatasheetRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"subprojectcorrosiondatasheet" = "SubProjectCorrosionDatasheet", "generalcorrosion" = "GeneralCorrosion", "pittingcorrosion" = "PittingCorrosion", "controlactivity" = "ControlActivity", "production" = "Production", "corrosioninhibitor" = "CorrosionInhibitor", "corrosioninhibitoravailability" = "CorrosionInhibitorAvailability", "cathodicprotectiondatasheet" = "CathodicProtectionDatasheet"})
 */
class SubProjectCorrosionDatasheet
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spcd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $coupon_serial_no;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $retrieval_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $installed_date;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $average_rate_mmpy;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $average_rate_mpy;

    /**
     * @var integer
     *
     * @ORM\Column(name="dbactivitylocationid", type="integer", length=10, nullable=true)
     */
    protected $dbactivitylocations;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectActivity", inversedBy="corrosiondatasheet")
     * @ORM\JoinColumn(name="activityid", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $activityid;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $activityPerformed;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get coupon_serial_no
     *
     * @return string
     */
    public function getCouponSerialNo()
    {
        return $this->coupon_serial_no;
    }

    /**
     * Set coupon_serial_no
     *
     * @param string $couponSerialNo
     * @return SubProjectCorrosionDatasheet
     */
    public function setCouponSerialNo($couponSerialNo)
    {
        $this->coupon_serial_no = $couponSerialNo;

        return $this;
    }

    /**
     * Get retrieval_date
     *
     * @return \DateTime
     */
    public function getRetrievalDate()
    {
        return $this->retrieval_date;
    }

    /**
     * Set retrieval_date
     *
     * @param \DateTime $retrievalDate
     * @return SubProjectCorrosionDatasheet
     */
    public function setRetrievalDate($retrievalDate)
    {
        $this->retrieval_date = $retrievalDate;

        return $this;
    }

    /**
     * Get installed_date
     *
     * @return \DateTime
     */
    public function getInstalledDate()
    {
        return $this->installed_date;
    }

    /**
     * Set installed_date
     *
     * @param \DateTime $installedDate
     * @return SubProjectCorrosionDatasheet
     */
    public function setInstalledDate($installedDate)
    {
        $this->installed_date = $installedDate;

        return $this;
    }

    /**
     * Get average_rate_mmpy
     *
     * @return string
     */
    public function getAverageRateMmpy()
    {
        return $this->average_rate_mmpy;
    }

    /**
     * Set average_rate_mmpy
     *
     * @param string $averageRateMmpy
     * @return SubProjectCorrosionDatasheet
     */
    public function setAverageRateMmpy($averageRateMmpy)
    {
        $this->average_rate_mmpy = $averageRateMmpy;

        return $this;
    }

    /**
     * Get average_rate_mpy
     *
     * @return string
     */
    public function getAverageRatempy()
    {
        return $this->average_rate_mpy;
    }

    /**
     * Set average_rate_mpy
     *
     * @param string $averageRateMpy
     * @return SubProjectCorrosionDatasheet
     */
    public function setAverageRatempy($averageRateMpy)
    {
        $this->average_rate_mpy = $averageRateMpy;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectCorrosionDatasheet
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Set dbactivitylocations
     *
     * @param string dbactivitylocations
     * @return SubProjectCorrosionDatasheet
     */
    public function setDbactivities($dbactivitylocations)
    {
        $this->dbactivitylocations = $dbactivitylocations;

        return $this;
    }

    /**
     * Get dbactivitylocations
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase
     */
    public function getDbactivities()
    {
        return $this->dbactivitylocations;
    }

    /**
     * Get activityid
     *
     * @return string
     */
    public function getActivityId()
    {
        return $this->activityid;
    }

    /**
     * Set activityid
     *
     * @param string $activityid
     * @return SubProjectCorrosionDatasheet
     */
    public function setActivityId(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $activityid = null)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityPerformed
     *
     * @return string
     */
    public function getActivityPerformed()
    {
        return $this->activityPerformed;
    }

    /**
     * Set activityPerformed
     *
     * @param string $activityPerformed
     * @return SubProjectCorrosionDatasheet
     */
    public function setActivityPerformed($activityPerformed)
    {
        $this->activityPerformed = $activityPerformed;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class GeneralCorrosion extends SubProjectCorrosionDatasheet
{

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $retrieval_weight;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $installation_weight;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $general_rate_mmpy;

    /**
     * Get retrieval_weight
     *
     * @return decimal
     */
    public function getRetrievalWeight()
    {
        return $this->retrieval_weight;
    }

    /**
     * Set retrieval_weight
     *
     * @param string $retrievalWeight
     * @return SubProjectCorrosionDatasheet
     */
    public function setRetrievalWeight($retrievalWeight)
    {
        $this->retrieval_weight = $retrievalWeight;

        return $this;
    }

    /**
     * Get $installationWeight
     *
     * @return decimal
     */
    public function getInstallationWeight()
    {
        return $this->installation_weight;
    }

    /**
     * Set $installationWeight
     *
     * @param string $installationWeight
     * @return SubProjectCorrosionDatasheet
     */
    public function setInstallationWeight($installationWeight)
    {
        $this->installation_weight = $installationWeight;

        return $this;
    }

    /**
     * Get generalRateMmpy
     *
     * @return string
     */
    public function getGeneralRateMmpy()
    {
        return $this->general_rate_mmpy;
    }

    /**
     * Set generalRateMmpy
     *
     * @param string generalRateMmpy
     * @return SubProjectCorrosionDatasheet
     */
    public function setGeneralRateMmpy($generalRateMmpy)
    {
        $this->general_rate_mmpy = $generalRateMmpy;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class PittingCorrosion extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $pitting_rate_mmpy;

    /**
     * @var integer
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $depth_pit;

    /**
     * Get pittingRateMmpy
     *
     * @return string
     */
    public function getPittingRateMmpy()
    {
        return $this->pitting_rate_mmpy;
    }

    /**
     * Set pittingRateMmpy
     *
     * @param string pittingRateMmpy
     * @return SubProjectCorrosionDatasheet
     */
    public function setPittingRateMmpy($pittingRateMmpy)
    {
        $this->pitting_rate_mmpy = $pittingRateMmpy;

        return $this;
    }

    /**
     * Get depthPit
     *
     * @return string
     */
    public function getDepthPit()
    {
        return $this->depth_pit;
    }

    /**
     * Set depthPit
     *
     * @param string depthPit
     * @return SubProjectCorrosionDatasheet
     */
    public function setDepthPit($depthPit)
    {
        $this->depth_pit = $depthPit;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class ControlActivity extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="decimal", precision=15, scale=6)
     */
    protected $control_activity_value;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $control_activity_actual_value;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $control_activity_achievement_value;

    /**
     * Get ControlActivityValue
     *
     * @return string
     */
    public function getControlActivityValue()
    {
        return $this->control_activity_value;
    }

    /**
     * Set activityValue
     *
     * @param string $activityValue
     * @return setControlActivityValue
     */
    public function setControlActivityValue($activityValue)
    {
        $this->control_activity_value = $activityValue;

        return $this;
    }

    /**
     * Get ControlActivityActualValue
     *
     * @return string
     */
    public function getControlActivityActualValue()
    {
        return $this->control_activity_actual_value;
    }

    /**
     * Set activityActualValue
     *
     * @param string $activityActualValue
     * @return setControlActivityValue
     */
    public function setControlActivityActualValue($activityActualValue)
    {
        $this->control_activity_actual_value = $activityActualValue;

        return $this;
    }

    /**
     * Get ControlActivityAchievementValue
     *
     * @return string
     */
    public function getControlActivityAchievementValue()
    {
        return $this->control_activity_achievement_value;
    }

    /**
     * Set activityAchievementValue
     *
     * @param string $activityAchievementValue
     * @return setControlActivityAchievementValue
     */
    public function setControlActivityAchievementValue($activityAchievementValue)
    {
        $this->control_activity_achievement_value = $activityAchievementValue;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class Production extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $gas_production_mmscf_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $oil_production_bbl_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $condensate_production_bbl_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $water_production_bbl_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $total_production_bbl_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $water_cut_percentage;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $gor_scf_stb;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $salinity_ppm;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $h2s_mol_percentage;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $co2_mol_percentage;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $usp_psig;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $dsp_psig;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $ust_degree_celsius;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $esp_ops_frequency_hz;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $esp_intake_pressure_psig;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $esp_discharge_pressure_psig;

    /**
     * Get gas_production_mmscf_per_day
     *
     * @return string
     */
    public function getGasProductionMmscfPerDay()
    {
        return $this->gas_production_mmscf_per_day;
    }

    /**
     * Set gas_production_mmscf_per_day
     *
     * @param string $gasProductionMmscfPerDay
     * @return Production
     */
    public function setGasProductionMmscfPerDay($gasProductionMmscfPerDay)
    {
        $this->gas_production_mmscf_per_day = $gasProductionMmscfPerDay;

        return $this;
    }

    /**
     * Get oil_production_bbl_per_day
     *
     * @return \DateTime
     */
    public function getOilProductionBblPerDay()
    {
        return $this->oil_production_bbl_per_day;
    }

    /**
     * Set oil_production_bbl_per_day
     *
     * @param \DateTime $oilProductionBblPerDay
     * @return Production
     */
    public function setOilProductionBblPerDay($oilProductionBblPerDay)
    {
        $this->oil_production_bbl_per_day = $oilProductionBblPerDay;

        return $this;
    }

    /**
     * Get condensate_production_bbl_per_day
     *
     * @return string
     */
    public function getCondensateProductionBblPerDay()
    {
        return $this->condensate_production_bbl_per_day;
    }

    /**
     * Set condensate_production_bbl_per_day
     *
     * @param string $condensateProductionBblPerDay
     * @return Production
     */
    public function setCondensateProductionBblPerDay($condensateProductionBblPerDay)
    {
        $this->condensate_production_bbl_per_day = $condensateProductionBblPerDay;

        return $this;
    }

    /**
     * Get water_production_bbl_per_day
     *
     * @return string
     */
    public function getWaterProductionBblPerDay()
    {
        return $this->water_production_bbl_per_day;
    }

    /**
     * Set water_production_bbl_per_day
     *
     * @param string $waterProductionBblPerDay
     * @return Production
     */
    public function setWaterProductionBblPerDay($waterProductionBblPerDay)
    {
        $this->water_production_bbl_per_day = $waterProductionBblPerDay;

        return $this;
    }

    /**
     * Get total_production_bbl_per_day
     *
     * @return string
     */
    public function getTotalProductionBblPerDay()
    {
        return $this->total_production_bbl_per_day;
    }

    /**
     * Set total_production_bbl_per_day
     *
     * @param string $totalProductionBblPerDay
     * @return Production
     */
    public function setTotalProductionBblPerDay($totalProductionBblPerDay)
    {
        $this->total_production_bbl_per_day = $totalProductionBblPerDay;

        return $this;
    }

    /**
     * Set water_cut_percentage
     *
     * @param string $waterCutPercentage
     * @return Production
     */
    public function setWaterCutPercentage($waterCutPercentage)
    {
        $this->water_cut_percentage = $waterCutPercentage;

        return $this;
    }

    /**
     * Get water_cut_percentage
     *
     * @return string
     */
    public function getWaterCutPercentage()
    {
        return $this->water_cut_percentage;
    }

    /**
     * Set gor_scf_stb
     *
     * @param string $gorScfStb
     * @return Production
     */
    public function setGorScfStb($gorScfStb)
    {
        $this->gor_scf_stb = $gorScfStb;

        return $this;
    }

    /**
     * Get gor_scf_stb
     *
     * @return string
     */
    public function getGorScfStb()
    {
        return $this->gor_scf_stb;
    }

    /**
     * Set salinity_ppm
     *
     * @param string $salinityPpm
     * @return Production
     */
    public function setSalinityPpm($salinityPpm)
    {
        $this->salinity_ppm = $salinityPpm;

        return $this;
    }

    /**
     * Get salinity_ppm
     *
     * @return string
     */
    public function getSalinityPpm()
    {
        return $this->salinity_ppm;
    }

    /**
     * Set h2s_mol_percentage
     *
     * @param string $h2sMolPercentage
     * @return Production
     */
    public function setH2sMolPercentage($h2sMolPercentage)
    {
        $this->h2s_mol_percentage = $h2sMolPercentage;

        return $this;
    }

    /**
     * Get h2s_mol_percentage
     *
     * @return string
     */
    public function getH2sMolPercentage()
    {
        return $this->h2s_mol_percentage;
    }

    /**
     * Set co2_mol_percentage
     *
     * @param string $co2MolPercentage
     * @return Production
     */
    public function setCo2MolPercentage($co2MolPercentage)
    {
        $this->co2_mol_percentage = $co2MolPercentage;

        return $this;
    }

    /**
     * Get co2_mol_percentage
     *
     * @return string
     */
    public function getCo2MolPercentage()
    {
        return $this->co2_mol_percentage;
    }

    /**
     * Set usp_psig
     *
     * @param string $uspPsig
     * @return Production
     */
    public function setUspPsig($uspPsig)
    {
        $this->usp_psig = $uspPsig;

        return $this;
    }

    /**
     * Get usp_psig
     *
     * @return string
     */
    public function getUspPsig()
    {
        return $this->usp_psig;
    }

    /**
     * Set dsp_psig
     *
     * @param string $dspPsig
     * @return Production
     */
    public function setDspPsig($dspPsig)
    {
        $this->dsp_psig = $dspPsig;

        return $this;
    }

    /**
     * Get dsp_psig
     *
     * @return string
     */
    public function getDspPsig()
    {
        return $this->dsp_psig;
    }

    /**
     * Set ust_degree_celsius
     *
     * @param string $ustDegreeCelsius
     * @return Production
     */
    public function setUstDegreeCelsius($ustDegreeCelsius)
    {
        $this->ust_degree_celsius = $ustDegreeCelsius;

        return $this;
    }

    /**
     * Get ust_degree_celsius
     *
     * @return string
     */
    public function getUstDegreeCelsius()
    {
        return $this->ust_degree_celsius;
    }

    /**
     * Set esp_ops_frequency_hz
     *
     * @param string $espOpsFrequencyHz
     * @return Production
     */
    public function setEspOpsFrequencyHz($espOpsFrequencyHz)
    {
        $this->esp_ops_frequency_hz = $espOpsFrequencyHz;

        return $this;
    }

    /**
     * Get esp_ops_frequency_hz
     *
     * @return string
     */
    public function getEspOpsFrequencyHz()
    {
        return $this->esp_ops_frequency_hz;
    }

    /**
     * Set esp_intake_pressure_psig
     *
     * @param string $espIntakePressurePsig
     * @return Production
     */
    public function setEspIntakePressurePsig($espIntakePressurePsig)
    {
        $this->esp_intake_pressure_psig = $espIntakePressurePsig;

        return $this;
    }

    /**
     * Get esp_intake_pressure_psig
     *
     * @return string
     */
    public function getEspIntakePressurePsig()
    {
        return $this->esp_intake_pressure_psig;
    }

    /**
     * Set esp_discharge_pressure_psig
     *
     * @param string $espDischargePressurePsig
     * @return Production
     */
    public function setEspDischargePressurePsig($espDischargePressurePsig)
    {
        $this->esp_discharge_pressure_psig = $espDischargePressurePsig;

        return $this;
    }

    /**
     * Get esp_discharge_pressure_psig
     *
     * @return string
     */
    public function getEspDischargePressurePsig()
    {
        return $this->esp_discharge_pressure_psig;
    }

}

/**
 * @ORM\Entity()
 */
class CorrosionInhibitor extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $gas_production_mmscf_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $total_production_bbl_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $recommended_gas_dosage_litre_per_mmscf;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $target_ci_dosage_litre_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $target_ci_concentration_ppm;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $actual_ci_dosage_litre_per_day;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $actual_ci_concentration_ppm;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $ci_percentage_availability;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $vol_inject_vs_vol_req_percentage;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $per_time_ci_target_reach_percentage;

    /**
     * Get gas_production_mmscf_per_day
     *
     * @return string
     */
    public function getGasProductionMmscfPerDay()
    {
        return $this->gas_production_mmscf_per_day;
    }

    /**
     * Set gas_production_mmscf_per_day
     *
     * @param string $gasProductionMmscfPerDay
     * @return CorrosionInhibitor
     */
    public function setGasProductionMmscfPerDay($gasProductionMmscfPerDay)
    {
        $this->gas_production_mmscf_per_day = $gasProductionMmscfPerDay;

        return $this;
    }

    /**
     * Get total_production_bbl_per_day
     *
     * @return string
     */
    public function getTotalProductionBblPerDay()
    {
        return $this->total_production_bbl_per_day;
    }

    /**
     * Set total_production_bbl_per_day
     *
     * @param string $totalProductionBblPerDay
     * @return CorrosionInhibitor
     */
    public function setTotalProductionBblPerDay($totalProductionBblPerDay)
    {
        $this->total_production_bbl_per_day = $totalProductionBblPerDay;

        return $this;
    }

    /**
     * Get recommended_gas_dosage_litre_per_mmscf
     *
     * @return string
     */
    public function getRecommendedGasDosageLitrePerMmscf()
    {
        return $this->recommended_gas_dosage_litre_per_mmscf;
    }

    /**
     * Set recommended_gas_dosage_litre_per_mmscf
     *
     * @param string $recommendedGasDosageLitrePerMmscf
     * @return CorrosionInhibitor
     */
    public function setRecommendedGasDosageLitrePerMmscf($recommendedGasDosageLitrePerMmscf)
    {
        $this->recommended_gas_dosage_litre_per_mmscf = $recommendedGasDosageLitrePerMmscf;

        return $this;
    }

    /**
     * Get target_ci_dosage_litre_per_day
     *
     * @return string
     */
    public function getTargetCiDosageLitrePerDay()
    {
        return $this->target_ci_dosage_litre_per_day;
    }

    /**
     * Set target_ci_dosage_litre_per_day
     *
     * @param string $targetCiDosageLitrePerDay
     * @return CorrosionInhibitor
     */
    public function setTargetCiDosageLitrePerDay($targetCiDosageLitrePerDay)
    {
        $this->target_ci_dosage_litre_per_day = $targetCiDosageLitrePerDay;

        return $this;
    }

    /**
     * Get target_ci_concentration_ppm
     *
     * @return string
     */
    public function getTargetCiConcentrationPpm()
    {
        return $this->target_ci_concentration_ppm;
    }

    /**
     * Set target_ci_concentration_ppm
     *
     * @param string $targetCiConcentrationPpm
     * @return CorrosionInhibitor
     */
    public function setTargetCiConcentrationPpm($targetCiConcentrationPpm)
    {
        $this->target_ci_concentration_ppm = $targetCiConcentrationPpm;

        return $this;
    }

    /**
     * Get actual_ci_dosage_litre_per_day
     *
     * @return string
     */
    public function getActualCiDosageLitrePerDay()
    {
        return $this->actual_ci_dosage_litre_per_day;
    }

    /**
     * Set actual_ci_dosage_litre_per_day
     *
     * @param string $actualCiDosageLitrePerDay
     * @return CorrosionInhibitor
     */
    public function setActualCiDosageLitrePerDay($actualCiDosageLitrePerDay)
    {
        $this->actual_ci_dosage_litre_per_day = $actualCiDosageLitrePerDay;

        return $this;
    }

    /**
     * Get actual_ci_concentration_ppm
     *
     * @return string
     */
    public function getActualCiConcentrationPpm()
    {
        return $this->actual_ci_concentration_ppm;
    }

    /**
     * Set actual_ci_concentration_ppm
     *
     * @param string $actualCiConcentrationPpm
     * @return CorrosionInhibitor
     */
    public function setActualCiConcentrationPpm($actualCiConcentrationPpm)
    {
        $this->actual_ci_concentration_ppm = $actualCiConcentrationPpm;

        return $this;
    }

    /**
     * Get ci_percentage_availability
     *
     * @return string
     */
    public function getCiPercentageAvailability()
    {
        return $this->ci_percentage_availability;
    }

    /**
     * Set ci_percentage_availability
     *
     * @param string $ciPercentageAvailability
     * @return CorrosionInhibitor
     */
    public function setCiPercentageAvailability($ciPercentageAvailability)
    {
        $this->ci_percentage_availability = $ciPercentageAvailability;

        return $this;
    }

    /**
     * Get vol_inject_vs_vol_req_percentage
     *
     * @return string
     */
    public function getVolInjectVsVolReqPercentage()
    {
        return $this->vol_inject_vs_vol_req_percentage;
    }

    /**
     * Set vol_inject_vs_vol_req_percentage
     *
     * @param string $volInjectVsVolReqPercentage
     * @return CorrosionInhibitor
     */
    public function setVolInjectVsVolReqPercentage($volInjectVsVolReqPercentage)
    {
        $this->vol_inject_vs_vol_req_percentage = $volInjectVsVolReqPercentage;

        return $this;
    }

    /**
     * Get per_time_ci_target_reach_percentage
     *
     * @return string
     */
    public function getPerTimeCiTargetReachPercentage()
    {
        return $this->per_time_ci_target_reach_percentage;
    }

    /**
     * Set per_time_ci_target_reach_percentage
     *
     * @param string $perTimeCiTargetReachPercentage
     * @return CorrosionInhibitor
     */
    public function setPerTimeCiTargetReachPercentage($perTimeCiTargetReachPercentage)
    {
        $this->per_time_ci_target_reach_percentage = $perTimeCiTargetReachPercentage;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class CorrosionInhibitorAvailability extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $to_date;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $corrosion_inhibitor_percentage_availability;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $vol_inject_vs_vol_req_percentage;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $per_time_ci_target_reach_percentage;

    /**
     * Get ID
     *
     * @return Integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get from_date
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->from_date;
    }

    /**
     * Set $fromDate
     *
     * @param \DateTime $fromDate
     * @return SubProjectCorrosionDatasheet
     */
    public function setFromDate($fromDate)
    {
        $this->from_date = $fromDate;

        return $this;
    }

    /**
     * Get to_date
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->to_date;
    }

    /**
     * Set to_date
     *
     * @param \DateTime $toDate
     * @return SubProjectCorrosionDatasheet
     */
    public function setToDate($toDate)
    {
        $this->to_date = $toDate;

        return $this;
    }

    /**
     * Get corrosion_inhibitor_percentage_availability
     *
     * @return string
     */
    public function getCorrosionInhibitorPercentageAvailability()
    {
        return $this->corrosion_inhibitor_percentage_availability;
    }

    /**
     * Set corrosion_inhibitor_percentage_availability
     *
     * @param string $corrosionInhibitorPercentageAvailability
     * @return CorrosionInhibitorAvailability
     */
    public function setCorrosionInhibitorPercentageAvailability($corrosionInhibitorPercentageAvailability)
    {
        $this->corrosion_inhibitor_percentage_availability = $corrosionInhibitorPercentageAvailability;

        return $this;
    }

    /**
     * Get vol_inject_vs_vol_req_percentage
     *
     * @return string
     */
    public function getVolInjectVsVolReqPercentage()
    {
        return $this->vol_inject_vs_vol_req_percentage;
    }

    /**
     * Set vol_inject_vs_vol_req_percentage
     *
     * @param string $volInjectVsVolReqPercentage
     * @return CorrosionInhibitorAvailability
     */
    public function setVolInjectVsVolReqPercentage($volInjectVsVolReqPercentage)
    {
        $this->vol_inject_vs_vol_req_percentage = $volInjectVsVolReqPercentage;

        return $this;
    }

    /**
     * Get per_time_ci_target_reach_percentage
     *
     * @return string
     */
    public function getPerTimeCiTargetReachPercentage()
    {
        return $this->per_time_ci_target_reach_percentage;
    }

    /**
     * Set per_time_ci_target_reach_percentage
     *
     * @param string $perTimeCiTargetReachPercentage
     * @return CorrosionInhibitorAvailability
     */
    public function setPerTimeCiTargetReachPercentage($perTimeCiTargetReachPercentage)
    {
        $this->per_time_ci_target_reach_percentage = $perTimeCiTargetReachPercentage;

        return $this;
    }

}

/**
 * @ORM\Entity()
 */
class CathodicProtectionDatasheet extends SubProjectCorrosionDatasheet
{
    /**
     * @ORM\Column(type="decimal", precision=15, scale=6)
     */
    protected $on_potential;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=6, nullable=true)
     */
    protected $off_potential;

    /**
     * Get OnPotential
     *
     * @return string
     */
    public function getOnPotential()
    {
        return $this->on_potential;
    }

    /**
     * Set onpotential
     *
     * @param string $onPotential
     * @return setOnPotential
     */
    public function setOnPotential($onPotential)
    {
        $this->on_potential = $onPotential;

        return $this;
    }

    /**
     * Get OffPotential
     *
     * @return string
     */
    public function getOffPotential()
    {
        return $this->off_potential;
    }

    /**
     * Set activityValue
     *
     * @param string $offPotential
     * @return setOffPotential
     */
    public function setOffPotential($offPotential)
    {
        $this->off_potential = $offPotential;

        return $this;
    }



    /**
     * Set dbactivitylocations
     *
     * @param integer $dbactivitylocations
     * @return SubProjectCorrosionDatasheet
     */
    public function setDbactivitylocations($dbactivitylocations)
    {
        $this->dbactivitylocations = $dbactivitylocations;

        return $this;
    }

    /**
     * Get dbactivitylocations
     *
     * @return integer
     */
    public function getDbactivitylocations()
    {
        return $this->dbactivitylocations;
    }
}
