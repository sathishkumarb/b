<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseDatabaseType
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BaseDatabaseType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=32)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="string", length=32)
     */
    private $desc;

    /**
     * @ORM\OneToMany(targetEntity="BaseMetricsConfig" , mappedBy="databasetype" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $databasemetrics;

    /**
     * @ORM\OneToMany(targetEntity="BaseDatasheetConfig" , mappedBy="databasetype" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $datasheetconfig;

    public function __construct()
    {
        $this->databasemetrics = new \Doctrine\Common\Collections\ArrayCollection();
        $this->datasheetconfig = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get Id
     *
     * @return BaseDatabsetype
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseDatabase
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseDatabase
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set desc
     *
     * @param desc
     * @return BaseDatabase
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get desc
     *
     * @return BaseDatabase
     */
    public function getDesc()
    {
        return $this->desc;
    }

}
