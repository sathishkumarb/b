<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;


use AIE\Bundle\VeracityBundle\Component\Core\Tags;
use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseActivity
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseActivity
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var double
     *
     * @ORM\Column(name="frequency", type="decimal", scale=2)
     * @JMS\Type("decimal")
     */
    protected $frequency = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="activityperson", type="string", length=120)
     * @JMS\Type("string")
     */
    protected $activityperson;

    /**
     * @var string
     *
     * @ORM\Column(name="weightage", type="integer", length=10)
     * @JMS\Type("string")
     */
    protected $weightage;

    /**
     * @var string
     *
     * @ORM\Column(name="consequences_excursion", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $consequencesexcursion;

    /**
     * @var string
     *
     * @ORM\Column(name="remedialaction", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $remedialaction;

    /**
     * @ORM\ManyToOne(targetEntity="BaseMitigationTechniques", inversedBy="baseactivity")
     * @ORM\JoinColumn(name="basemitigationtechniqueid", referencedColumnName="id")
     */
    private $basemitigationtechniques;

    /**
     * @ORM\ManyToOne(targetEntity="BaseMetricsConfig", inversedBy="baseactivity")
     * @ORM\JoinColumn(name="basemetricid", referencedColumnName="id")
     */
    private $basemetrics;

    /**
     * @ORM\ManyToOne(targetEntity="BaseDatasheetType", inversedBy="baseactivity")
     * @ORM\JoinColumn(name="basedatasheetid", referencedColumnName="id")
     */
    private $basedatasheet;

    /**
     * @var string
     *
     * @ORM\Column(name="managementtitle", type="string", length=150)
     * @JMS\Type("string")
     */
    protected $managementtitle;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="managementgreen", type="string", length=100)
     * @JMS\Type("string")
     */
    protected $managementgreen;

    /**
     * @var string
     *
     * @ORM\Column(name="managementamber", type="string", length=100, nullable=true)
     * @JMS\Type("string")
     */
    protected $managementamber;

    /**
     * @var string
     *
     * @ORM\Column(name="managementred", type="string", length=100)
     * @JMS\Type("string")
     */
    protected $managementred;

    /**
     * @var string
     *
     * @ORM\Column(name="controltitle", type="string", length=150)
     * @JMS\Type("string")
     */
    protected $controltitle;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreenexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenexpression;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreen", type="string", length=20)
     * @JMS\Type("string")
     */
    protected $controlgreen;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlgreenexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlgreenrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlgreenrange;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlamberexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberexpression;


    /**
     * @var string
     *
     * @ORM\Column(name="controlamber", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamber;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlamberexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlamberrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlamberrange;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlredexpression", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredexpression;

    /**
     * @var string
     *
     * @ORM\Column(name="controlred", type="string", length=20)
     * @JMS\Type("string")
     */
    protected $controlred;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="controlredexpressionrange", type="string", length=10, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredexpressionrange;

    /**
     * @var string
     *
     * @ORM\Column(name="controlredrange", type="string", length=20, nullable=true)
     * @JMS\Type("string")
     */
    protected $controlredrange;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->datasheetmetrics = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     * @return BaseActivity
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set activityperson
     *
     * @param string $activityperson
     * @return BaseActivity
     */
    public function setActivityperson($activityperson)
    {
        $this->activityperson = $activityperson;

        return $this;
    }

    /**
     * Get activityperson
     *
     * @return string
     */
    public function getActivityperson()
    {
        return $this->activityperson;
    }

    /**
     * Set weightage
     *
     * @param string $weightage
     * @return BaseActivity
     */
    public function setWeightage($weightage)
    {
        $this->weightage = $weightage;

        return $this;
    }

    /**
     * Get weightage
     *
     * @return string
     */
    public function getWeightage()
    {
        return $this->weightage;
    }

    /**
     * Set consequencesexcursion
     *
     * @param string $consequencesexcursion
     * @return BaseActivity
     */
    public function setConsequencesexcursion($consequencesexcursion)
    {
        $this->consequencesexcursion = $consequencesexcursion;

        return $this;
    }

    /**
     * Get consequencesexcursion
     *
     * @return string
     */
    public function getConsequencesexcursion()
    {
        return $this->consequencesexcursion;
    }

    /**
     * Set remedialaction
     *
     * @param string $remedialaction
     * @return BaseActivity
     */
    public function setRemedialaction($remedialaction)
    {
        $this->remedialaction = $remedialaction;

        return $this;
    }

    /**
     * Get remedialaction
     *
     * @return string
     */
    public function getRemedialaction()
    {
        return $this->remedialaction;
    }

    /**
     * Set basemetrics
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig $basemetrics
     * @return BaseActivity
     */
    public function setBasemetrics(\AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig $basemetrics = null)
    {
        $this->basemetrics = $basemetrics;

        return $this;
    }

    /**
     * Get basemetrics
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig
     */
    public function getBasemetrics()
    {
        return $this->basemetrics;
    }

    /**
     * Set basedatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $basedatasheet
     * @return BaseActivity
     */
    public function setBasedatasheet(\AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $basedatasheet = null)
    {
        $this->basedatasheet = $basedatasheet;

        return $this;
    }

    /**
     * Get basedatasheet
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseDatasheetType
     */
    public function getBasedatasheet()
    {
        return $this->basedatasheet;
    }

    /**
     * Set basemitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseMitigationTechniques $basemitigationtechniques
     * @return BaseActivity
     */
    public function setBasemitigationtechniques(\AIE\Bundle\CmsBundle\Entity\BaseMitigationTechniques $basemitigationtechniques = null)
    {
        $this->basemitigationtechniques = $basemitigationtechniques;

        return $this;
    }

    /**
     * Get basemitigationtechniques
     *
     * @return \AIE\Bundle\CmsBundle\Entity\BaseMitigationTechniques
     */
    public function getBasemitigationtechniques()
    {
        return $this->basemitigationtechniques;
    }

    /**
     * Set managementtitle
     *
     * @param string $managementtitle
     * @return BaseActivity
     */
    public function setManagementtitle($managementtitle)
    {
        $this->managementtitle = $managementtitle;

        return $this;
    }

    /**
     * Get managementtitle
     *
     * @return string
     */
    public function getManagementtitle()
    {
        return $this->managementtitle;
    }

    /**
     * Set managementgreen
     *
     * @param string $managementgreen
     * @return BaseActivity
     */
    public function setManagementgreen($managementgreen)
    {
        $this->managementgreen = $managementgreen;

        return $this;
    }

    /**
     * Get managementgreen
     *
     * @return string
     */
    public function getManagementgreen()
    {
        return $this->managementgreen;
    }

    /**
     * Set managementamber
     *
     * @param string $managementamber
     * @return BaseActivity
     */
    public function setManagementamber($managementamber)
    {
        $this->managementamber = $managementamber;

        return $this;
    }

    /**
     * Get managementamber
     *
     * @return string
     */
    public function getManagementamber()
    {
        return $this->managementamber;
    }

    /**
     * Set managementred
     *
     * @param string $managementred
     * @return BaseActivity
     */
    public function setManagementred($managementred)
    {
        $this->managementred = $managementred;

        return $this;
    }

    /**
     * Get managementred
     *
     * @return string
     */
    public function getManagementred()
    {
        return $this->managementred;
    }

    /**
     * Set controltitle
     *
     * @param string $controltitle
     * @return BaseActivity
     */
    public function setControltitle($controltitle)
    {
        $this->controltitle = $controltitle;

        return $this;
    }

    /**
     * Get controltitle
     *
     * @return string
     */
    public function getControltitle()
    {
        return $this->controltitle;
    }

    /**
     * Set controlgreenexpression
     *
     * @param string $controlgreenexpression
     * @return BaseActivity
     */
    public function setControlgreenexpression($controlgreenexpression)
    {
        $this->controlgreenexpression = $controlgreenexpression;

        return $this;
    }

    /**
     * Get controlgreenexpression
     *
     * @return string
     */
    public function getControlgreenexpression()
    {
        return $this->controlgreenexpression;
    }

    /**
     * Set controlgreen
     *
     * @param string $controlgreen
     * @return BaseActivity
     */
    public function setControlgreen($controlgreen)
    {
        $this->controlgreen = $controlgreen;

        return $this;
    }

    /**
     * Get controlgreen
     *
     * @return string
     */
    public function getControlgreen()
    {
        return $this->controlgreen;
    }

    /**
     * Set controlamberexpression
     *
     * @param string $controlamberexpression
     * @return BaseActivity
     */
    public function setControlamberexpression($controlamberexpression)
    {
        $this->controlamberexpression = $controlamberexpression;

        return $this;
    }

    /**
     * Get controlamberexpression
     *
     * @return string
     */
    public function getControlamberexpression()
    {
        return $this->controlamberexpression;
    }

    /**
     * Set controlamber
     *
     * @param string $controlamber
     * @return BaseActivity
     */
    public function setControlamber($controlamber)
    {
        $this->controlamber = $controlamber;

        return $this;
    }

    /**
     * Get controlamber
     *
     * @return string
     */
    public function getControlamber()
    {
        return $this->controlamber;
    }

    /**
     * Set controlredexpression
     *
     * @param string $controlredexpression
     * @return BaseActivity
     */
    public function setControlredexpression($controlredexpression)
    {
        $this->controlredexpression = $controlredexpression;

        return $this;
    }

    /**
     * Get controlredexpression
     *
     * @return string
     */
    public function getControlredexpression()
    {
        return $this->controlredexpression;
    }

    /**
     * Set controlred
     *
     * @param string $controlred
     * @return BaseActivity
     */
    public function setControlred($controlred)
    {
        $this->controlred = $controlred;

        return $this;
    }

    /**
     * Get controlred
     *
     * @return string
     */
    public function getControlred()
    {
        return $this->controlred;
    }


    /**
     * Set controlgreenexpressionrange
     *
     * @param string $controlgreenexpressionrange
     * @return BaseActivity
     */
    public function setControlgreenexpressionrange($controlgreenexpressionrange)
    {
        $this->controlgreenexpressionrange = $controlgreenexpressionrange;

        return $this;
    }

    /**
     * Get controlgreenexpressionrange
     *
     * @return string 
     */
    public function getControlgreenexpressionrange()
    {
        return $this->controlgreenexpressionrange;
    }

    /**
     * Set controlgreenrange
     *
     * @param string $controlgreenrange
     * @return BaseActivity
     */
    public function setControlgreenrange($controlgreenrange)
    {
        $this->controlgreenrange = $controlgreenrange;

        return $this;
    }

    /**
     * Get controlgreenrange
     *
     * @return string 
     */
    public function getControlgreenrange()
    {
        return $this->controlgreenrange;
    }

    /**
     * Set controlamberexpressionrange
     *
     * @param string $controlamberexpressionrange
     * @return BaseActivity
     */
    public function setControlamberexpressionrange($controlamberexpressionrange)
    {
        $this->controlamberexpressionrange = $controlamberexpressionrange;

        return $this;
    }

    /**
     * Get controlamberexpressionrange
     *
     * @return string 
     */
    public function getControlamberexpressionrange()
    {
        return $this->controlamberexpressionrange;
    }

    /**
     * Set controlamberrange
     *
     * @param string $controlamberrange
     * @return BaseActivity
     */
    public function setControlamberrange($controlamberrange)
    {
        $this->controlamberrange = $controlamberrange;

        return $this;
    }

    /**
     * Get controlamberrange
     *
     * @return string 
     */
    public function getControlamberrange()
    {
        return $this->controlamberrange;
    }

    /**
     * Set controlredexpressionrange
     *
     * @param string $controlredexpressionrange
     * @return BaseActivity
     */
    public function setControlredexpressionrange($controlredexpressionrange)
    {
        $this->controlredexpressionrange = $controlredexpressionrange;

        return $this;
    }

    /**
     * Get controlredexpressionrange
     *
     * @return string 
     */
    public function getControlredexpressionrange()
    {
        return $this->controlredexpressionrange;
    }

    /**
     * Set controlredrange
     *
     * @param string $controlredrange
     * @return BaseActivity
     */
    public function setControlredrange($controlredrange)
    {
        $this->controlredrange = $controlredrange;

        return $this;
    }

    /**
     * Get controlredrange
     *
     * @return string 
     */
    public function getControlredrange()
    {
        return $this->controlredrange;
    }

    /**
     * Add baseactivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseActivity $baseactivity
     * @return BaseActivity
     */
    public function addBaseactivity(\AIE\Bundle\CmsBundle\Entity\BaseActivity $baseactivity)
    {
        $this->baseactivity[] = $baseactivity;

        return $this;
    }

    /**
     * Remove baseactivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseActivity $baseactivity
     */
    public function removeBaseactivity(\AIE\Bundle\CmsBundle\Entity\BaseActivity $baseactivity)
    {
        $this->baseactivity->removeElement($baseactivity);
    }

    /**
     * Get baseactivity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBaseactivity()
    {
        return $this->baseactivity;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }
}
