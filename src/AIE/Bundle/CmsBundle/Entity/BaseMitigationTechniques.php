<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseMitigationTechniques
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseMitigationTechniques {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="BaseThreats", inversedBy="basemitigationtechniques")
     * @ORM\JoinColumn(name="basethreatid", referencedColumnName="id")
     */
    private $basethreats;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    protected $mitigationtechniquestitle;

    /**
     * @ORM\OneToMany(targetEntity="BaseActivity" , mappedBy="basemitigationtechniques" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $baseactivity;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    public function __construct()
    {
        $this->baseactivity = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set baseMitigationTechnique
     *
     * \AIE\Bundle\CmsBundle\Entity\BaseThreats $basethreats
     * @return BaseThreats
     */
    public function setBaseThreats(\AIE\Bundle\CmsBundle\Entity\BaseThreats $basethreats = null) {
        $this->basethreats = $basethreats;

        return $this;
    }

    /**
     * get baseMitigationTechnique
     *
     * @param baseMitigationTechnique
     * @return baseMitigationTechnique
     */
    public function getBaseThreats()
    {
        return $this->basethreats;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseMitigationTechnique
     */
    public function setMitigationTechniquesTitle($mitigationtechniquestitle)
    {
        $this->mitigationtechniquestitle = $mitigationtechniquestitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseMitigationTechnique
     */
    public function getMitigationTechniquesTitle()
    {
        return $this->mitigationtechniquestitle;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
