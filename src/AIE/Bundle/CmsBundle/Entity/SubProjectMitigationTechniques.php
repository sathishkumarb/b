<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubProjectMitigationTechniques
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectMitigationTechniques {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="SubProjectThreats", inversedBy="subprojectmitigationtechniques")
     * @ORM\JoinColumn(name="subprojectthreatid", referencedColumnName="id")
     */
    private $subprojectthreats;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="systems")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    protected $mitigationtechniquestitle;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectActivity" , mappedBy="subprojectmitigationtechniques" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectactivity;

    /**
     * @ORM\Column(name="base_threat_id", type="integer", nullable=true)
     */
    private $basethreatid;

    /**
     * @ORM\Column(name="base_mitigationtechnique_id", type="integer", nullable=true)
     */
    private $basemitigationtechniqueid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isselected;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subprojectactivity = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSystems
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set SubProjectMitigationTechnique
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats
     * @return SubProjectThreats
     */
    public function setSubProjectThreats(\AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats = null) {
        $this->subprojectthreats = $subprojectthreats;

        return $this;
    }

    /**
     * get SubProjectMitigationTechnique
     *
     * @param SubProjectMitigationTechnique
     * @return SubProjectMitigationTechnique
     */
    public function getSubProjectThreats()
    {
        return $this->subprojectthreats;
    }


    /**
     * Set title
     *
     * @param title
     * @return SubProjectMitigationTechnique
     */
    public function setMitigationTechniquesTitle($mitigationtechniquestitle)
    {
        $this->mitigationtechniquestitle = $mitigationtechniquestitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return SubProjectMitigationTechnique
     */
    public function getMitigationTechniquesTitle()
    {
        return $this->mitigationtechniquestitle;
    }

    /**
     * Add subprojectactivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity
     * @return SubProjectMitigationTechniques
     */
    public function addSubprojectactivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity)
    {
        $this->subprojectactivity[] = $subprojectactivity;

        return $this;
    }

    /**
     * Remove subprojectactivity
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity
     */
    public function removeSubprojectactivity(\AIE\Bundle\CmsBundle\Entity\SubProjectActivity $subprojectactivity)
    {
        $this->subprojectactivity->removeElement($subprojectactivity);
    }

    /**
     * Get subprojectactivity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectactivity()
    {
        return $this->subprojectactivity;
    }

    /**
     * Set basethreatid
     *
     * @param integer $basethreatid
     * @return SubProjectThreats
     */
    public function setBasethreatid($basethreatid)
    {
        $this->basethreatid = $basethreatid;

        return $this;
    }

    /**
     * Get basethreatid
     *
     * @return integer
     */
    public function getBasethreatid()
    {
        return $this->basethreatid;
    }


    /**
     * Set basemitigationtechniqueid
     *
     * @param integer $basemitigationtechniqueid
     * @return SubProjectMitigationTechniques
     */
    public function setBasemitigationtechniqueid($basemitigationtechniqueid)
    {
        $this->basemitigationtechniqueid = $basemitigationtechniqueid;

        return $this;
    }

    /**
     * Get basemitigationtechniqueid
     *
     * @return integer
     */
    public function getBasemitigationtechniqueid()
    {
        return $this->basemitigationtechniqueid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Set isselected
     *
     * @param boolean $isselected
     * @return SubProjectMitigationTechniques
     */
    public function setIsselected($isselected)
    {
        $this->isselected = $isselected;

        return $this;
    }

    /**
     * Get isselected
     *
     * @return boolean
     */
    public function getIsselected()
    {
        return $this->isselected;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
