<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseDatabaseProperties
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseDatabaseProperties {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="colid", type="integer", length=11)
     */
    protected $colid;

    /**
     * @var string
     *
     * @ORM\Column(name="colvalue", type="string", length=255)
     */
    protected $colvalue;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * construct
     */
    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return BaseDatabaseProperties
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colid
     *
     * @param colid
     * @return BaseDatabaseProperties
     */
    public function setColId($colid)
    {
        $this->colid = $colid;

        return $this;
    }

    /**
     * Get Column
     *
     * @return BaseDatabaseProperties
     */
    public function getColId()
    {
        return $this->colid;
    }

    /**
     * Set colvalue
     *
     * @param colvalue
     * @return BaseDatabaseProperties
     */
    public function setColValue($colvalue)
    {
        $this->colvalue = $colvalue;

        return $this;
    }

    /**
     * Get colvalue
     *
     * @return BaseDatabaseProperties
     */
    public function getColValue()
    {
        return $this->colvalue;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
