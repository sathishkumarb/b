<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseSystems
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseSystems {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $systemstitle;


    /**
     * @ORM\OneToMany(targetEntity="BaseThreats" , mappedBy="basesystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $basethreats;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    public function __construct()
    {
        $this->basethreats = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");

    }

    /**
     * Get id
     *
     * @return BaseSystems
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param title
     * @return BaseSystems
     */
    public function setSystemsTitle($systemstitle)
    {
        $this->systemstitle = $systemstitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseSystems
     */
    public function getSystemsTitle()
    {
        return $this->systemstitle;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return BaseThreats
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return BaseThreats
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
