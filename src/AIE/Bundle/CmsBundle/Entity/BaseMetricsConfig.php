<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseMetricsConfig
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\BaseMetricsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class BaseMetricsConfig {

    protected static $CORROSION_INHIBITOR_CHOICES = array(1 => 'Corrosion Inhibitor Availability, %', 2 => 'Volume Injected Against Volume Required, %', 3 => 'Percentage of time CI target is reached, %' );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @ORM\ManyToOne(targetEntity="BaseDatabaseType", inversedBy="databasemetrics")
     * @ORM\JoinColumn(name="databasetypeid", referencedColumnName="id")
     */
    private $databasetype;

    /**
     * @ORM\ManyToOne(targetEntity="BaseDatasheetType", inversedBy="datasheetmetrics")
     * @ORM\JoinColumn(name="datasheettypeid", referencedColumnName="id")
     */
    private $datasheettype;

    /**
     * @ORM\Column(name="corrosioninhibitortype", type="integer", nullable=true)
     */
    protected $corrosioninhibitortype;

    /**
     * @ORM\OneToMany(targetEntity="BaseActivity" , mappedBy="basemetrics" , cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $baseactivity;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectActivity" , mappedBy="subprojectmetrics" , cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $subprojectactivity;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set BaseMetricsConfig
     *
     * \AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype
     * @return BaseMetrics
     */
    public function setDatabasetype(\AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype = null) {
        $this->databasetype = $databasetype;

        return $this;
    }

    /**
     * Set databasetype
     *
     * @param databasetype
     * @return BaseMetrics
     */
    public function getDatabasetype()
    {
        return $this->databasetype;
    }

 /**
     * Set databasetype
     *
      * \AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype
     * @return UserProjectGroup
     */
    public function setDatasheettype(\AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $datasheettype = null) {
        $this->datasheettype = $datasheettype;

        return $this;
    }


    /**
     * Set datasheettype
     *
     * @param datasheettype
     * @return BaseMetrics
     */
    public function getDatasheettype()
    {
        return $this->datasheettype;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseMetrics
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseMetrics
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set corrosioninhibitortype
     *
     * $corrosioninhibitortype
     * @return corrosioninhibitortype
     */
    public function setCorrosioninhibitortype($corrosioninhibitortype) {
        $this->corrosioninhibitortype = $corrosioninhibitortype;

        return $this;
    }

    /**
     * get corrosioninhibitortype
     *
     * @param $corrosioninhibitortype
     * @return corrosioninhibitortype
     */
    public function getCorrosioninhibitortype()
    {

        return $this->corrosioninhibitortype;
    }

    /**
     * get choices
     *
     * @return choices
     */
    public function getChoices()
    {

        return self::$CORROSION_INHIBITOR_CHOICES;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    function __toString() {
        return $this->getTitle();
    }
}
