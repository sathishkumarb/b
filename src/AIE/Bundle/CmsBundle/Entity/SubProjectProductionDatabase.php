<?php
/*
* License: Copyright AIE - 2015
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;


/**
 * SubProjectProductionDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectProductionDatabase extends AbstractFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="welltestdate", type="datetime", nullable=true)
     * @JMS\Type("integer")
     */
    protected $welltestdate;

    /**
     * @var string
     *
     * @ORM\Column(name="wellfunction", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $wellfunction;

    /**
     * @var string
     *
     * @ORM\Column(name="wellstatus", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $wellstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="contractor", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $contractor;

    /**
     * @var integer
     *
     * @ORM\Column(name="welltesttype", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $welltesttype;

    /**
     * @var string
     *
     * @ORM\Column(name="flowpath", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $flowpath;

    /**
     * @var string
     *
     * @ORM\Column(name="chokesize", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $chokesize;


    /**
     * @var string
     *
     * @ORM\Column(name="reservoir", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $reservoir;


    /**
     * @var string
     *
     * @ORM\Column(name="dgs", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $dgs;

    /**
     * @var string
     *
     * @ORM\Column(name="train", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $train;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldmanifold", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $fieldmanifold;

    /**
     * @var string
     *
     * @ORM\Column(name="stationmanifold", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $stationmanifold;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="analysis", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $analysis;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $recommendations;

    /**
     * @var string
     *
     * @ORM\Column(name="columnone", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnone;

    /**
     * @var string
     *
     * @ORM\Column(name="columntwo", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columntwo;

    /**
     * @var string
     *
     * @ORM\Column(name="columnthree", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnthree;

    /**
     * @var string
     *
     * @ORM\Column(name="columnfour", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfour;

    /**
     * @var string
     *
     * @ORM\Column(name="columnfive", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfive;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spcpd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectcathodicprotectiondatabase")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subprojectsystems;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    private $activityid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    public function getUploadDir()
    {
        return 'productiondatabase';
    }

        /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set welltestdate
     *
     * @param \DateTime $welltestdate
     * @return SubProjectProductionDatabase
     */
    public function setWelltestdate($welltestdate)
    {
        $this->welltestdate = $welltestdate;

        return $this;
    }

    /**
     * Get welltestdate
     *
     * @return \DateTime 
     */
    public function getWelltestdate()
    {
        return $this->welltestdate;
    }

    /**
     * Set wellfunction
     *
     * @param string $wellfunction
     * @return SubProjectProductionDatabase
     */
    public function setWellfunction($wellfunction)
    {
        $this->wellfunction = $wellfunction;

        return $this;
    }

    /**
     * Get wellfunction
     *
     * @return string 
     */
    public function getWellfunction()
    {
        return $this->wellfunction;
    }

    /**
     * Set wellstatus
     *
     * @param string $wellstatus
     * @return SubProjectProductionDatabase
     */
    public function setWellstatus($wellstatus)
    {
        $this->wellstatus = $wellstatus;

        return $this;
    }

    /**
     * Get wellstatus
     *
     * @return string 
     */
    public function getWellstatus()
    {
        return $this->wellstatus;
    }

    /**
     * Set contractor
     *
     * @param string $contractor
     * @return SubProjectProductionDatabase
     */
    public function setContractor($contractor)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return string 
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * Set welltesttype
     *
     * @param string $welltesttype
     * @return SubProjectProductionDatabase
     */
    public function setWelltesttype($welltesttype)
    {
        $this->welltesttype = $welltesttype;

        return $this;
    }

    /**
     * Get welltesttype
     *
     * @return string 
     */
    public function getWelltesttype()
    {
        return $this->welltesttype;
    }

    /**
     * Set flowpath
     *
     * @param string $flowpath
     * @return SubProjectProductionDatabase
     */
    public function setFlowpath($flowpath)
    {
        $this->flowpath = $flowpath;

        return $this;
    }

    /**
     * Get flowpath
     *
     * @return string 
     */
    public function getFlowpath()
    {
        return $this->flowpath;
    }

    /**
     * Set chokesize
     *
     * @param string $chokesize
     * @return SubProjectProductionDatabase
     */
    public function setChokesize($chokesize)
    {
        $this->chokesize = $chokesize;

        return $this;
    }

    /**
     * Get chokesize
     *
     * @return string 
     */
    public function getChokesize()
    {
        return $this->chokesize;
    }

    /**
     * Set reservoir
     *
     * @param string $reservoir
     * @return SubProjectProductionDatabase
     */
    public function setReservoir($reservoir)
    {
        $this->reservoir = $reservoir;

        return $this;
    }

    /**
     * Get reservoir
     *
     * @return string 
     */
    public function getReservoir()
    {
        return $this->reservoir;
    }

    /**
     * Set dgs
     *
     * @param string $dgs
     * @return SubProjectProductionDatabase
     */
    public function setDgs($dgs)
    {
        $this->dgs = $dgs;

        return $this;
    }

    /**
     * Get dgs
     *
     * @return string 
     */
    public function getDgs()
    {
        return $this->dgs;
    }

    /**
     * Set train
     *
     * @param string $train
     * @return SubProjectProductionDatabase
     */
    public function setTrain($train)
    {
        $this->train = $train;

        return $this;
    }

    /**
     * Get train
     *
     * @return string 
     */
    public function getTrain()
    {
        return $this->train;
    }

    /**
     * Set fieldmanifold
     *
     * @param string $fieldmanifold
     * @return SubProjectProductionDatabase
     */
    public function setFieldmanifold($fieldmanifold)
    {
        $this->fieldmanifold = $fieldmanifold;

        return $this;
    }

    /**
     * Get fieldmanifold
     *
     * @return string 
     */
    public function getFieldmanifold()
    {
        return $this->fieldmanifold;
    }

    /**
     * Set stationmanifold
     *
     * @param string $stationmanifold
     * @return SubProjectProductionDatabase
     */
    public function setStationmanifold($stationmanifold)
    {
        $this->stationmanifold = $stationmanifold;

        return $this;
    }

    /**
     * Get stationmanifold
     *
     * @return string 
     */
    public function getStationmanifold()
    {
        return $this->stationmanifold;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return SubProjectProductionDatabase
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set columnone
     *
     * @param string $columnone
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnone($columnone)
    {
        $this->columnone = $columnone;

        return $this;
    }

    /**
     * Get columnone
     *
     * @return string
     */
    public function getColumnone()
    {
        return $this->columnone;
    }

    /**
     * Set columntwo
     *
     * @param string $columntwo
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumntwo($columntwo)
    {
        $this->columntwo = $columntwo;

        return $this;
    }

    /**
     * Get columntwo
     *
     * @return string
     */
    public function getColumntwo()
    {
        return $this->columntwo;
    }

    /**
     * Set columnthree
     *
     * @param string $columnthree
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnthree($columnthree)
    {
        $this->columnthree = $columnthree;

        return $this;
    }

    /**
     * Get columnthree
     *
     * @return string
     */
    public function getColumnthree()
    {
        return $this->columnthree;
    }

    /**
     * Set columnfour
     *
     * @param string $columnfour
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnfour($columnfour)
    {
        $this->columnfour = $columnfour;

        return $this;
    }

    /**
     * Get columnfour
     *
     * @return string
     */
    public function getColumnfour()
    {
        return $this->columnfour;
    }

    /**
     * Set columnfive
     *
     * @param string $columnfive
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnfive($columnfive)
    {
        $this->columnfive = $columnfive;

        return $this;
    }

    /**
     * Get columnfive
     *
     * @return string
     */
    public function getColumnfive()
    {
        return $this->columnfive;
    }

    /**
     * Set activityid
     *
     * @param integer $activityid
     * @return SubProjectProductionDatabase
     */
    public function setActivityid($activityid)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityid
     *
     * @return integer 
     */
    public function getActivityid()
    {
        return $this->activityid;
    }

    /**
     * Set from_date
     *
     * @param \DateTime $fromDate
     * @return SubProjectProductionDatabase
     */
    public function setFromDate($fromDate)
    {
        $this->from_date = $fromDate;

        return $this;
    }

    /**
     * Get from_date
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->from_date;
    }

    /**
     * Set to_date
     *
     * @param \DateTime $toDate
     * @return SubProjectProductionDatabase
     */
    public function setToDate($toDate)
    {
        $this->to_date = $toDate;

        return $this;
    }

    /**
     * Get to_date
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->to_date;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectProductionDatabase
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectProductionDatabase
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectProductionDatabase
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects 
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set subprojectsystems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectProductionDatabase
     */
    public function setSubprojectsystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null)
    {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * Get subprojectsystems
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjectSystems 
     */
    public function getSubprojectsystems()
    {
        return $this->subprojectsystems;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjectProductionDatabase
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set analysis
     *
     * @param string $analysis
     * @return SubProjectChemicalDosageDatabase
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;

        return $this;
    }

    /**
     * Get analysis
     *
     * @return string
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SubProjectChemicalDosageDatabase
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

}
