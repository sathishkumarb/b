<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ActionTracker
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */

class ActionTracker{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    protected $date;

    /**
     * @ORM\OneToMany(targetEntity="ActionTrackerItems" , mappedBy="actionTracker", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $actiontrackeritems;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="actiontracker")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     * */
    private $subproject;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actiontrackeritems = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set Title
     *
     * $title
     * @return ActionTracker
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * get title
     *
     * @param title
     * @return ActionTracker
     */
    public function getTitle()
    {

        return $this->title;
    }
    /**
     * Set Date
     *
     * @param \DateTime $date
     * @return ActionTracker
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * get date
     *
     * @param date
     * @return \DateTime
     */
    public function getDate()
    {

        return $this->date;
    }

    /**
     * get date as string
     *
     * @param date
     * @return ActionTracker
     */
    public function getDateSting()
    {
        $result = $this->date->format('d-m-Y');

        return $result;
    }

    /**
     * Set subproject
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return ActionTracker
     */
    public function setSubProject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject) {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subproject
     * @return ActionTracker
     */
    public function getSubProject()
    {

        return $this->subproject;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return ActionTracker
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return ActionTracker
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Add actiontrackeritems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\ActionTrackerItems $actiontrackeritems
     * @return ActionTracker
     */
    public function addActiontrackeritem(\AIE\Bundle\CmsBundle\Entity\ActionTrackerItems $actiontrackeritems)
    {
        $this->actiontrackeritems[] = $actiontrackeritems;

        return $this;
    }

    /**
     * Remove actiontrackeritems
     *
     * @param \AIE\Bundle\CmsBundle\Entity\ActionTrackerItems $actiontrackeritems
     */
    public function removeActiontrackeritem(\AIE\Bundle\CmsBundle\Entity\ActionTrackerItems $actiontrackeritems)
    {
        $this->actiontrackeritems->removeElement($actiontrackeritems);
    }

    /**
     * Get actiontrackeritems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActiontrackeritems()
    {
        return $this->actiontrackeritems;
    }
}
