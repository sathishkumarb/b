<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;


/**
 * SubProjectCorrosionMonitoringDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectCorrosionMonitoringDatabase extends AbstractFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="platformorplant", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $platformorplant;

    /**
     * @var string
     *
     * @ORM\Column(name="pandid", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $pandid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $location;

    /**
     * @var string
     *
     * @ORM\Column(name="linenumber", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $linenumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="linediameterinches", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $linediameterinches;

    /**
     * @var integer
     *
     * @ORM\Column(name="standoffinches", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $standoffinches;

    /**
     * @var string
     *
     * @ORM\Column(name="linematerial", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $linematerial;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $serviceid;


    /**
     * @var integer
     *
     * @ORM\Column(name="operatingpressure", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $operatingpressure;

    /**
     * @var integer
     *
     * @ORM\Column(name="operatingtemperature", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $operatingtemperature;


    /**
     * @var integer
     *
     * @ORM\Column(name="devicetypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $devicetypeid;


    /**
     * @var integer
     *
     * @ORM\Column(name="orientationid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $orientationid;


    /**
     * @var integer
     *
     * @ORM\Column(name="accessfittingtypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $accessfittingtypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="installedcap", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $installedcap;

    /**
     * @var string
     *
     * @ORM\Column(name="installedplug", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $installedplug;


    /**
     * @var integer
     *
     * @ORM\Column(name="holderplugid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $holderplugid;


    /**
     * @var integer
     *
     * @ORM\Column(name="teefittingid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $teefittingid;


    /**
     * @var string
     *
     * @ORM\Column(name="instrumentation", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $instrumentation;


    /**
     * @var string
     *
     * @ORM\Column(name="probeextensionadapter", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $probeextensionadapter;


    /**
     * @var integer
     *
     * @ORM\Column(name="probetechnologyanddataloggerid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $probetechnologyanddataloggerid;


    /**
     * @var string
     *
     * @ORM\Column(name="probetypeandmodel", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $probetypeandmodel;


    /**
     * @var integer
     *
     * @ORM\Column(name="logginginterval", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $logginginterval;

    /**
     * @var integer
     *
     * @ORM\Column(name="probeinstallationdate", type="datetime", nullable=true)
     * @JMS\Type("integer")
     */
    protected $probeinstallationdate;

    /**
     * @var string
     *
     * @ORM\Column(name="couponholdertagno", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $couponholdertagno;

    /**
     * @var integer
     *
     * @ORM\Column(name="couponmaterialanddensityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $couponmaterialanddensityid;


    /**
     * @var integer
     *
     * @ORM\Column(name="coupontypeandareaid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $coupontypeandareaid;


    /**
     * @var integer
     *
     * @ORM\Column(name="insulationid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $insulationid;


    /**
     * @var integer
     *
     * @ORM\Column(name="surfacefinishid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $surfacefinishid;

    /**
     * @var integer
     *
     * @ORM\Column(name="onlineid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $onlineid;

    /**
     * @var string
     *
     * @ORM\Column(name="tool", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $tool;

    /**
     * @var integer
     *
     * @ORM\Column(name="scaffoldid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $scaffoldid;

    /**
     * @var integer
     *
     * @ORM\Column(name="accessid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $accessid;

    /**
     * @var string
     *
     * @ORM\Column(name="analysis", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $analysis;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $recommendations;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spcmd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectcorrosionmonitoringdatabase")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subprojectsystems;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    private $activityid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    public function getUploadDir()
    {
        return 'samplingdatabase';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set platformorplant
     *
     * @param string $platformorplant
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setPlatformorplant($platformorplant)
    {
        $this->platformorplant = $platformorplant;

        return $this;
    }

    /**
     * Get platformorplant
     *
     * @return string 
     */
    public function getPlatformorplant()
    {
        return $this->platformorplant;
    }

    /**
     * Set pandid
     *
     * @param string $pandid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setPandid($pandid)
    {
        $this->pandid = $pandid;

        return $this;
    }

    /**
     * Get pandid
     *
     * @return string 
     */
    public function getPandid()
    {
        return $this->pandid;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set linenumber
     *
     * @param string $linenumber
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setLinenumber($linenumber)
    {
        $this->linenumber = $linenumber;

        return $this;
    }

    /**
     * Get linenumber
     *
     * @return string 
     */
    public function getLinenumber()
    {
        return $this->linenumber;
    }

    /**
     * Set linediameterinches
     *
     * @param integer $linediameterinches
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setLinediameterinches($linediameterinches)
    {
        $this->linediameterinches = $linediameterinches;

        return $this;
    }

    /**
     * Get linediameterinches
     *
     * @return integer 
     */
    public function getLinediameterinches()
    {
        return $this->linediameterinches;
    }

    /**
     * Set standoffinches
     *
     * @param integer $standoffinches
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setStandoffinches($standoffinches)
    {
        $this->standoffinches = $standoffinches;

        return $this;
    }

    /**
     * Get standoffinches
     *
     * @return integer 
     */
    public function getStandoffinches()
    {
        return $this->standoffinches;
    }

    /**
     * Set linematerial
     *
     * @param string $linematerial
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setLinematerial($linematerial)
    {
        $this->linematerial = $linematerial;

        return $this;
    }

    /**
     * Get linematerial
     *
     * @return string 
     */
    public function getLinematerial()
    {
        return $this->linematerial;
    }

    /**
     * Set serviceid
     *
     * @param integer $serviceid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setServiceid($serviceid)
    {
        $this->serviceid = $serviceid;

        return $this;
    }

    /**
     * Get serviceid
     *
     * @return integer 
     */
    public function getServiceid()
    {
        return $this->serviceid;
    }

    /**
     * Set operatingpressure
     *
     * @param integer $operatingpressure
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setOperatingpressure($operatingpressure)
    {
        $this->operatingpressure = $operatingpressure;

        return $this;
    }

    /**
     * Get operatingpressure
     *
     * @return integer 
     */
    public function getOperatingpressure()
    {
        return $this->operatingpressure;
    }

    /**
     * Set operatingtemperature
     *
     * @param integer $operatingtemperature
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setOperatingtemperature($operatingtemperature)
    {
        $this->operatingtemperature = $operatingtemperature;

        return $this;
    }

    /**
     * Get operatingtemperature
     *
     * @return integer
     */
    public function getOperatingtemperature()
    {
        return $this->operatingtemperature;
    }

    /**
     * Set devicetypeid
     *
     * @param integer $devicetypeid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setDevicetypeid($devicetypeid)
    {
        $this->devicetypeid = $devicetypeid;

        return $this;
    }

    /**
     * Get devicetypeid
     *
     * @return integer 
     */
    public function getDevicetypeid()
    {
        return $this->devicetypeid;
    }

    /**
     * Set orientationid
     *
     * @param integer $orientationid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setOrientationid($orientationid)
    {
        $this->orientationid = $orientationid;

        return $this;
    }

    /**
     * Get orientationid
     *
     * @return integer 
     */
    public function getOrientationid()
    {
        return $this->orientationid;
    }

    /**
     * Set accessfittingtypeid
     *
     * @param integer $accessfittingtypeid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setAccessfittingtypeid($accessfittingtypeid)
    {
        $this->accessfittingtypeid = $accessfittingtypeid;

        return $this;
    }

    /**
     * Get accessfittingtypeid
     *
     * @return integer 
     */
    public function getAccessfittingtypeid()
    {
        return $this->accessfittingtypeid;
    }

    /**
     * Set installedcap
     *
     * @param string $installedcap
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setInstalledcap($installedcap)
    {
        $this->installedcap = $installedcap;

        return $this;
    }

    /**
     * Get installedcap
     *
     * @return string 
     */
    public function getInstalledcap()
    {
        return $this->installedcap;
    }

    /**
     * Set installedplug
     *
     * @param string $installedplug
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setInstalledplug($installedplug)
    {
        $this->installedplug = $installedplug;

        return $this;
    }

    /**
     * Get installedplug
     *
     * @return string
     */
    public function getInstalledplug()
    {
        return $this->installedplug;
    }

    /**
     * Set holderplugid
     *
     * @param integer $holderplugid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setHolderplugid($holderplugid)
    {
        $this->holderplugid = $holderplugid;

        return $this;
    }

    /**
     * Get holderplugid
     *
     * @return integer 
     */
    public function getHolderplugid()
    {
        return $this->holderplugid;
    }

    /**
     * Set teefittingid
     *
     * @param integer $teefittingid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setTeefittingid($teefittingid)
    {
        $this->teefittingid = $teefittingid;

        return $this;
    }

    /**
     * Get teefittingid
     *
     * @return integer 
     */
    public function getTeefittingid()
    {
        return $this->teefittingid;
    }

    /**
     * Set instrumentation
     *
     * @param string $instrumentation
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setInstrumentation($instrumentation)
    {
        $this->instrumentation = $instrumentation;

        return $this;
    }

    /**
     * Get instrumentation
     *
     * @return string 
     */
    public function getInstrumentation()
    {
        return $this->instrumentation;
    }

    /**
     * Set probeextensionadapter
     *
     * @param string $probeextensionadapter
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setProbeextensionadapter($probeextensionadapter)
    {
        $this->probeextensionadapter = $probeextensionadapter;

        return $this;
    }

    /**
     * Get probeextensionadapter
     *
     * @return string 
     */
    public function getProbeextensionadapter()
    {
        return $this->probeextensionadapter;
    }

    /**
     * Set probetechnologyanddataloggerid
     *
     * @param integer $probetechnologyanddataloggerid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setProbetechnologyanddataloggerid($probetechnologyanddataloggerid)
    {
        $this->probetechnologyanddataloggerid = $probetechnologyanddataloggerid;

        return $this;
    }

    /**
     * Get probetechnologyanddataloggerid
     *
     * @return integer 
     */
    public function getProbetechnologyanddataloggerid()
    {
        return $this->probetechnologyanddataloggerid;
    }

    /**
     * Set probetypeandmodel
     *
     * @param string $probetypeandmodel
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setProbetypeandmodel($probetypeandmodel)
    {
        $this->probetypeandmodel = $probetypeandmodel;

        return $this;
    }

    /**
     * Get probetypeandmodel
     *
     * @return string 
     */
    public function getProbetypeandmodel()
    {
        return $this->probetypeandmodel;
    }

    /**
     * Set logginginterval
     *
     * @param integer $logginginterval
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setLogginginterval($logginginterval)
    {
        $this->logginginterval = $logginginterval;

        return $this;
    }

    /**
     * Get logginginterval
     *
     * @return integer 
     */
    public function getLogginginterval()
    {
        return $this->logginginterval;
    }

    /**
     * Set probeinstallationdate
     *
     * @param datetime $probeinstallationdate
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setProbeinstallationdate($probeinstallationdate)
    {
        $this->probeinstallationdate = $probeinstallationdate;

        return $this;
    }

    /**
     * Get probeinstallationdate
     *
     * @return integer 
     */
    public function getProbeinstallationdate()
    {
        return $this->probeinstallationdate;
    }

    /**
     * Set couponholdertagno
     *
     * @param string $couponholdertagno
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setCouponholdertagno($couponholdertagno)
    {
        $this->couponholdertagno = $couponholdertagno;

        return $this;
    }

    /**
     * Get couponholdertagno
     *
     * @return string 
     */
    public function getCouponholdertagno()
    {
        return $this->couponholdertagno;
    }

    /**
     * Set couponmaterialanddensityid
     *
     * @param integer $couponmaterialanddensityid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setCouponmaterialanddensityid($couponmaterialanddensityid)
    {
        $this->couponmaterialanddensityid = $couponmaterialanddensityid;

        return $this;
    }

    /**
     * Get couponmaterialanddensityid
     *
     * @return integer 
     */
    public function getCouponmaterialanddensityid()
    {
        return $this->couponmaterialanddensityid;
    }

    /**
     * Set coupontypeandareaid
     *
     * @param integer $coupontypeandareaid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setCoupontypeandareaid($coupontypeandareaid)
    {
        $this->coupontypeandareaid = $coupontypeandareaid;

        return $this;
    }

    /**
     * Get coupontypeandareaid
     *
     * @return integer 
     */
    public function getCoupontypeandareaid()
    {
        return $this->coupontypeandareaid;
    }

    /**
     * Set insulationid
     *
     * @param integer $insulationid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setInsulationid($insulationid)
    {
        $this->insulationid = $insulationid;

        return $this;
    }

    /**
     * Get insulationid
     *
     * @return integer 
     */
    public function getInsulationid()
    {
        return $this->insulationid;
    }

    /**
     * Set surfacefinishid
     *
     * @param integer $surfacefinishid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setSurfacefinishid($surfacefinishid)
    {
        $this->surfacefinishid = $surfacefinishid;

        return $this;
    }

    /**
     * Get surfacefinishid
     *
     * @return integer 
     */
    public function getSurfacefinishid()
    {
        return $this->surfacefinishid;
    }

    /**
     * Set onlineid
     *
     * @param integer $onlineid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setOnlineid($onlineid)
    {
        $this->onlineid = $onlineid;

        return $this;
    }

    /**
     * Get onlineid
     *
     * @return integer 
     */
    public function getOnlineid()
    {
        return $this->onlineid;
    }

    /**
     * Set tool
     *
     * @param string $tool
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setTool($tool)
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * Get tool
     *
     * @return string 
     */
    public function getTool()
    {
        return $this->tool;
    }

    /**
     * Set scaffoldid
     *
     * @param integer $scaffoldid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setScaffoldid($scaffoldid)
    {
        $this->scaffoldid = $scaffoldid;

        return $this;
    }

    /**
     * Get scaffoldid
     *
     * @return integer 
     */
    public function getScaffoldid()
    {
        return $this->scaffoldid;
    }

    /**
     * Set accessid
     *
     * @param integer $accessid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setAccessid($accessid)
    {
        $this->accessid = $accessid;

        return $this;
    }

    /**
     * Get accessid
     *
     * @return integer 
     */
    public function getAccessid()
    {
        return $this->accessid;
    }

    /**
     * Set subprojectsystems
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setSubProjectSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null) {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subprojectsystems
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function getSubProjectSystems()
    {

        return $this->subprojectsystems;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Set activityid
     *
     * @param integer $activityid
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setActivityid($activityid)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityid
     *
     * @return integer 
     */
    public function getActivityid()
    {
        return $this->activityid;
    }

    /**
     * Set analysis
     *
     * @param string $analysis
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;

        return $this;
    }

    /**
     * Get analysis
     *
     * @return string 
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string 
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Add subprojectcorrosiondatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $subprojectcorrosiondatasheet
     * @return SubProjectCorrosionMonitoringDatabase
     */
    public function addSubprojectcorrosiondatasheet(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $subprojectcorrosiondatasheet)
    {
        $this->subprojectcorrosiondatasheet[] = $subprojectcorrosiondatasheet;

        return $this;
    }

    /**
     * Remove subprojectcorrosiondatasheet
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $subprojectcorrosiondatasheet
     */
    public function removeSubprojectcorrosiondatasheet(\AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionDatasheet $subprojectcorrosiondatasheet)
    {
        $this->subprojectcorrosiondatasheet->removeElement($subprojectcorrosiondatasheet);
    }

    /**
     * Get subprojectcorrosiondatasheet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubprojectcorrosiondatasheet()
    {
        return $this->subprojectcorrosiondatasheet;
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return SubProjectSamplingDatabase
     */
    public function setFrom($from_date)
    {
        $this->from_date = $from_date;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from_date;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return SubProjectSamplingDatabase
     */
    public function setTo($to_date)
    {
        $this->to_date = $to_date;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to_date;
    }
}
