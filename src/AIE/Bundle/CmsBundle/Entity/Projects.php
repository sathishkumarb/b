<?php
/**
 * Modified by Sathish Kumar
 * Date: 28/06/2016
 */

namespace AIE\Bundle\CmsBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\VeracityBundle\Component\Core\Projects as ProjectsBase;
use AIE\Bundle\CmsBundle\Security\SecureObjectInterface;
use AIE\Bundle\UserBundle\Entity\User;

/**
 * Projects
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\ProjectsRepository")
 */
class Projects extends ProjectsBase
{

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="folder", type="string", length=32)
     */
    private $folder;


	/**
     * @ORM\OneToMany(targetEntity="UserProjectGroup" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ug;

    /**
     * @ORM\OneToMany(targetEntity="SubProjects" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $subproject;

    /**
     * @ORM\OneToMany(targetEntity="Locations" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $locations;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subproject = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


        /**
     * Set location
     *
     * @param string $location
     * @return Projects
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get subprojectthreats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojects()
    {
        return $this->subproject;
    }


    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Set folder
     *
     * @param string $folder
     * @return Pipelines
     */
    public function setFolder($folder) {
        $this->folder = $folder;

        return $this;
    }

	public function getUploadDir()
	{
		return $this->getFolder() . '/logos';
	}

    /**
     * Get folder
     *
     * @return string
     */
    public function getFolder() {
        return $this->folder;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
