<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;


/**
 * SubProjectSamplingDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectSamplingDatabase extends AbstractFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="platformorplant", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $platformorplant;

    /**
     * @var string
     *
     * @ORM\Column(name="pandid", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $pandid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $location;

    /**
     * @var string
     *
     * @ORM\Column(name="linenumber", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $linenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="waterchemistry", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $waterchemistry;

    /**
     * @var integer
     *
     * @ORM\Column(name="orientationid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $orientationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $serviceid;


    /**
     * @var string
     *
     * @ORM\Column(name="samplingprocedure", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $samplingprocedure;


    /**
     * @var string
     *
     * @ORM\Column(name="analysisprocedure", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $analysisprocedure;


    /**
     * @var string
     *
     * @ORM\Column(name="samplingpointtype", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $samplingpointtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="activitypersonid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $activitypersonid;

    /**
     * @var string
     *
     * @ORM\Column(name="analysis", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $analysis;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $recommendations;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spsd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectsamplingdatabase")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subprojectsystems;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    private $activityid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    public function getUploadDir()
    {
        return 'samplingdatabase';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set platformorplant
     *
     * @param string $platformorplant
     * @return SubProjectSamplingDatabase
     */
    public function setPlatformorplant($platformorplant)
    {
        $this->platformorplant = $platformorplant;

        return $this;
    }

    /**
     * Get platformorplant
     *
     * @return string
     */
    public function getPlatformorplant()
    {
        return $this->platformorplant;
    }

    /**
     * Set pandid
     *
     * @param string $pandid
     * @return SubProjectSamplingDatabase
     */
    public function setPandid($pandid)
    {
        $this->pandid = $pandid;

        return $this;
    }

    /**
     * Get pandid
     *
     * @return string
     */
    public function getPandid()
    {
        return $this->pandid;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjectSamplingDatabase
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set linenumber
     *
     * @param string $linenumber
     * @return SubProjectSamplingDatabase
     */
    public function setLinenumber($linenumber)
    {
        $this->linenumber = $linenumber;

        return $this;
    }

    /**
     * Get linenumber
     *
     * @return string
     */
    public function getLinenumber()
    {
        return $this->linenumber;
    }

    /**
     * Set waterchemistry
     *
     * @param string $waterchemistry
     * @return SubProjectSamplingDatabase
     */
    public function setWaterchemistry($waterchemistry)
    {
        $this->waterchemistry = $waterchemistry;

        return $this;
    }

    /**
     * Get waterchemistry
     *
     * @return string
     */
    public function getWaterchemistry()
    {
        return $this->waterchemistry;
    }

    /**
     * Set orientationid
     *
     * @param integer $orientationid
     * @return SubProjectSamplingDatabase
     */
    public function setOrientationid($orientationid)
    {
        $this->orientationid = $orientationid;

        return $this;
    }

    /**
     * Get orientationid
     *
     * @return integer
     */
    public function getOrientationid()
    {
        return $this->orientationid;
    }

    /**
     * Set serviceid
     *
     * @param integer $serviceid
     * @return SubProjectSamplingDatabase
     */
    public function setServiceid($serviceid)
    {
        $this->serviceid = $serviceid;

        return $this;
    }

    /**
     * Get serviceid
     *
     * @return integer
     */
    public function getServiceid()
    {
        return $this->serviceid;
    }

    /**
     * Set samplingprocedure
     *
     * @param string $samplingprocedure
     * @return SubProjectSamplingDatabase
     */
    public function setSamplingprocedure($samplingprocedure)
    {
        $this->samplingprocedure = $samplingprocedure;

        return $this;
    }

    /**
     * Get samplingprocedure
     *
     * @return string
     */
    public function getSamplingprocedure()
    {
        return $this->samplingprocedure;
    }

    /**
     * Set analysisprocedure
     *
     * @param string $analysisprocedure
     * @return SubProjectSamplingDatabase
     */
    public function setAnalysisprocedure($analysisprocedure)
    {
        $this->analysisprocedure = $analysisprocedure;

        return $this;
    }

    /**
     * Get analysisprocedure
     *
     * @return string
     */
    public function getAnalysisprocedure()
    {
        return $this->analysisprocedure;
    }

    /**
     * Set samplingpointtype
     *
     * @param string $samplingpointtype
     * @return SubProjectSamplingDatabase
     */
    public function setSamplingpointtype($samplingpointtype)
    {
        $this->samplingpointtype = $samplingpointtype;

        return $this;
    }

    /**
     * Get samplingpointtype
     *
     * @return string
     */
    public function getSamplingpointtype()
    {
        return $this->samplingpointtype;
    }

    /**
     * Set activitypersonid
     *
     * @param integer $activitypersonid
     * @return SubProjectSamplingDatabase
     */
    public function setActivitypersonid($activitypersonid)
    {
        $this->activitypersonid = $activitypersonid;

        return $this;
    }

    /**
     * Get activitypersonid
     *
     * @return integer
     */
    public function getActivitypersonid()
    {
        return $this->activitypersonid;
    }

    /**
     * Set subprojectsystems
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectSamplingDatabase
     */
    public function setSubProjectSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null) {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subprojectsystems
     * @return SubProjectSamplingDatabase
     */
    public function getSubProjectSystems()
    {

        return $this->subprojectsystems;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectSamplingDatabase
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectSamplingDatabase
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSamplingDatabase
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Set activityid
     *
     * @param integer $activityid
     * @return SubProjectSamplingDatabase
     */
    public function setActivityid($activityid)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityid
     *
     * @return integer
     */
    public function getActivityid()
    {
        return $this->activityid;
    }

    /**
     * Set analysis
     *
     * @param string $analysis
     * @return SubProjectSamplingDatabase
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;

        return $this;
    }

    /**
     * Get analysis
     *
     * @return string
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SubProjectSamplingDatabase
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return SubProjectSamplingDatabase
     */
    public function setFrom($from_date)
    {
        $this->from_date = $from_date;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime 
     */
    public function getFrom()
    {
        return $this->from_date;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return SubProjectSamplingDatabase
     */
    public function setTo($to_date)
    {
        $this->to_date = $to_date;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime 
     */
    public function getTo()
    {
        return $this->to_date;
    }
}
