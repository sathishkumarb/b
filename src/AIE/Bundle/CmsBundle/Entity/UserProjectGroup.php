<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * UserProjectGroup
 *
 * @ORM\Table(name="UserProjectGroup")
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\UserProjectGroupRepository")
 */
class UserProjectGroup {

	/**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;
    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="ug")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;
        /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="ug")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     * */
    protected $subproject;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get project
     *
     * @return \AIE\Bundle\CmsBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return UserProjectGroup
     */
    public function setProject(\AIE\Bundle\CmsBundle\Entity\Projects $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Set sub-project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SubProjects $subproject
     * @return UserProjectGroup
     */
    public function setSubProject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get sub project
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubProject()
    {
        return $this->subproject;
    }

    /**
     * Get sub-project
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubProjectlists()
    {
        return $this->getProject()->getSubprojects();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function getGroupRoles() {
        return $this->getGroup()->getRoles();
    }

    /**
     * Get group
     *
     * @return \AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\UserBundle\Entity\CcmGroup $group
     * @return UserProjectGroup
     */
    public function setGroup(\AIE\Bundle\UserBundle\Entity\CcmGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    public function getUserRoles(){
        return $this->getUser()->getRoles();
    }

    /**
     * Get user
     *
     * @return \AIE\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \AIE\Bundle\UserBundle\Entity\User $user
     * @return UserProjectGroup
     */
    public function setUser(\AIE\Bundle\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

}