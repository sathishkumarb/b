<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * BaseMetricsConfig
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BaseDatasheetConfig {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

     /**
     * @ORM\ManyToOne(targetEntity="BaseDatabaseType", inversedBy="datasheetconfig")
     * @ORM\JoinColumn(name="databasetypeid", referencedColumnName="id")
     */
    private $databasetype;

    /**
     * @ORM\ManyToOne(targetEntity="BaseDatasheetType", inversedBy="datasheetconfig")
     * @ORM\JoinColumn(name="datasheettypeid", referencedColumnName="id")
     */
    private $datasheettype;


    /**
     * Set group
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype
     * @return datasheettype
     */
    public function setDatabasetype(\AIE\Bundle\CmsBundle\Entity\BaseDatabaseType $databasetype = null) {
        $this->databasetype = $databasetype;

        return $this;
    }

    /**
     * Set title
     *
     * @param title
     * @return BaseDatabasetype
     */
    public function getDatabasetype()
    {

        return $this->databasetype;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $group
     * @return datasheettype
     */
    public function setDatasheettype(\AIE\Bundle\CmsBundle\Entity\BaseDatasheetType $datasheettype = null) {
        $this->datasheettype = $datasheettype;

        return $this;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseDatasheettype
     */
    public function getDatasheettype()
    {
        return $this->datasheettype;
    }


    /**
     * Set title
     *
     * @param title
     * @return BaseMetrics
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return BaseMetrics
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param description
     * @return BaseMetrics
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return BaseMetrics
     */
    public function getDescription()
    {
        return $this->description;
    }
}
