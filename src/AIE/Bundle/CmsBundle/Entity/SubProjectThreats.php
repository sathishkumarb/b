<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * SubProjectThreats
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectThreats {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectthreats")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id")
     */
    private $subprojectsystems;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectMitigationTechniques" , mappedBy="subprojectthreats" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $subprojectmitigationtechniques;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="systems")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    protected $threatstitle;

    /**
     * @ORM\Column(name="base_system_id", type="integer", nullable=true)
     */
    private $basesystemid;

    /**
     * @ORM\Column(name="base_threat_id", type="integer", nullable=true)
     */
    private $basethreatid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isselected;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subprojectmitigationtechniques = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSystems
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }


    /**
     * Set subprojectsystems
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectThreats
     */
    public function setSubProjectSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null) {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subprojectsystems
     * @return SubProjectThreats
     */
    public function getSubProjectSystems()
    {

        return $this->subprojectsystems;
    }


    /**
     * Set title
     *
     * @param title
     * @return SubProjectThreats
     */
    public function setThreatsTitle($threatstitle)
    {
        $this->threatstitle = $threatstitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return SubProjectThreats
     */
    public function getThreatsTitle()
    {
        return $this->threatstitle;
    }

    /**
     * Add subprojectmitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $subprojectmitigationtechniques
     * @return SubProjectThreats
     */
    public function addSubprojectmitigationtechnique(\AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $subprojectmitigationtechniques)
    {
        $this->subprojectmitigationtechniques[] = $subprojectmitigationtechniques;

        return $this;
    }

    /**
     * Remove subprojectmitigationtechniques
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $subprojectmitigationtechniques
     */
    public function removeSubprojectmitigationtechnique(\AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques $subprojectmitigationtechniques)
    {
        $this->subprojectmitigationtechniques->removeElement($subprojectmitigationtechniques);
    }

    /**
     * Get subprojectmitigationtechniques
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubprojectmitigationtechniques()
    {
        return $this->subprojectmitigationtechniques;
    }


    /**
     * Set basethreatid
     *
     * @param integer $basethreatid
     * @return SubProjectThreats
     */
    public function setBasethreatid($basethreatid)
    {
        $this->basethreatid = $basethreatid;

        return $this;
    }

    /**
     * Get basethreatid
     *
     * @return integer 
     */
    public function getBasethreatid()
    {
        return $this->basethreatid;
    }

    /**
     * Set basesystemid
     *
     * @param integer $basesystemid
     * @return SubProjectThreats
     */
    public function setBasesystemid($basesystemid)
    {
        $this->basesystemid = $basesystemid;

        return $this;
    }

    /**
     * Get basesystemid
     *
     * @return integer
     */
    public function getBasesystemid()
    {
        return $this->basesystemid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Set isselected
     *
     * @param boolean $isselected
     * @return SubProjectThreats
     */
    public function setIsselected($isselected)
    {
        $this->isselected = $isselected;

        return $this;
    }

    /**
     * Get isselected
     *
     * @return boolean
     */
    public function getIsselected()
    {
        return $this->isselected;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }
}
