<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseDatabaseLinkedProperties
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class BaseDatabaseLinkedProperties {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="colidone", type="integer", length=11)
     */
    protected $colidone;

    /**
     * @var string
     *
     * @ORM\Column(name="colidonevalue", type="string", length=255)
     */
    protected $colidonevalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="colidtwo", type="integer", length=11)
     */
    protected $colidtwo;

    /**
     * @var string
     *
     * @ORM\Column(name="colidtwovalue", type="string", length=255)
     */
    protected $colidtwovalue;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * construct
     */
    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return BaseDatabaseLinkedProperties
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set colidone
     *
     * @param colidone
     * @return BaseDatabaseLinkedProperties
     */
    public function setColIdOne($colidone)
    {
        $this->colidone = $colidone;

        return $this;
    }

    /**
     * Get Column
     *
     * @return BaseDatabaseLinkedProperties
     */
    public function getColIdOne()
    {
        return $this->colidone;
    }


    /**
     * Get Column
     *
     * @return BaseDatabaseLinkedProperties
     */
    public function getColIdTwo()
    {
        return $this->colidtwo;
    }

    /**
     * Set colidone
     *
     * @param colidone
     * @return BaseDatabaseLinkedProperties
     */
    public function setColIdTwo($colidtwo)
    {
        $this->colidtwo = $colidtwo;

        return $this;
    }

    /**
     * Set colidonevalue
     *
     * @param colidonevalue
     * @return BaseDatabaseLinkedProperties
     */
    public function setColIdOneValue($colidonevalue)
    {
        $this->colidonevalue = $colidonevalue;

        return $this;
    }

    /**
     * Get colidonevalue
     *
     * @return BaseDatabaseLinkedProperties
     */
    public function getColIdOneValue()
    {
        return $this->colidonevalue;
    }

    /**
     * Set colidtwovalue
     *
     * @param colvalue
     * @return BaseDatabaseLinkedProperties
     */
    public function setColIdTwoValue($colidtwovalue)
    {
        $this->colidtwovalue = $colidtwovalue;

        return $this;
    }

    /**
     * Get colidtwovalue
     *
     * @return BaseDatabaseLinkedProperties
     */
    public function getColIdTwoValue()
    {
        return $this->colidtwovalue;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
