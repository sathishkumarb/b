<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * SubProjectSystems
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\SubProjectSystemsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectSystems {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $systemstitle;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectThreats" , mappedBy="subprojectsystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     * 
     */
    protected $subprojectthreats;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectSamplingDatabase" , mappedBy="subprojectsystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectsamplingdatabase;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectChemicalDosageDatabase" , mappedBy="subprojectsystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectchemicaldosagedatabase;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCathodicProtectionDatabase" , mappedBy="subprojectsystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectcathodicprotectiondatabase;

    /**
     * @ORM\OneToMany(targetEntity="SubProjectCorrosionMonitoringDatabase" , mappedBy="subprojectsystems" , cascade={"all"}, fetch="EXTRA_LAZY")
     *
     */
    protected $subprojectcorrosionmonitoringdatabase;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="systems")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\Column(name="base_system_id", type="integer", nullable=true)
     */
    private $basesystemid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isselected;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    public function __construct()
    {
        $this->subprojectthreats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subprojectsamplingdatabase = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subprojectcathodicprotectiondatabase = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subprojectchemicaldosagedatabase = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subprojectcorrosionmonitoringdatabase = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return SubProjectSystems
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param title
     * @return SubProjectSystems
     */
    public function setSystemsTitle($systemstitle)
    {
        $this->systemstitle = $systemstitle;

        return $this;
    }

    /**
     * Get title
     *
     * @return SubProjectSystems
     */
    public function getSystemsTitle()
    {
        return $this->systemstitle;
    }

    /**
     * Add subprojectthreats
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats
     * @return SubProjectSystems
     */
    public function addSubprojectthreat(\AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats)
    {
        $this->subprojectthreats[] = $subprojectthreats;

        return $this;
    }

    /**
     * Remove subprojectthreats
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats
     */
    public function removeSubprojectthreat(\AIE\Bundle\CmsBundle\Entity\SubProjectThreats $subprojectthreats)
    {
        $this->subprojectthreats->removeElement($subprojectthreats);
    }

    /**
     * Get subprojectthreats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectthreats()
    {
        return $this->subprojectthreats;
    }

    /**
     * Get subprojectsamplingdatabase
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectsamplingdatabase()
    {
        return $this->subprojectsamplingdatabase;
    }

    /**
     * Get subprojectsamplingdatabase
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectcathodicprotectiondatabase()
    {
        return $this->subprojectcathodicprotectiondatabase;
    }

    /**
     * Get subprojectsamplingdatabase
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectchemicaldosagedatabase()
    {
        return $this->subprojectchemicaldosagedatabase;
    }

    /**
     * Get subprojectsamplingdatabase
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubprojectcorrosionmonitoringdatabase()
    {
        return $this->subprojectcorrosionmonitoringdatabase;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSystems
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set basesystemid
     *
     * @param integer $basesystemid
     * @return SubProjectSystems
     */
    public function setBasesystemid($basesystemid)
    {
        $this->basesystemid = $basesystemid;

        return $this;
    }

    /**
     * Get basesystemid
     *
     * @return integer
     */
    public function getBasesystemid()
    {
        return $this->basesystemid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return Report
     */
    public function setModifiedAt(\DateTime $modifiedat)
    {
        $this->modifiedat = $modifiedat;
        return $this;
    }
    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Set isselected
     *
     * @param boolean $isselected
     * @return SubProjectSystems
     */
    public function setIsselected($isselected)
    {
        $this->isselected = $isselected;

        return $this;
    }

    /**
     * Get isselected
     *
     * @return boolean
     */
    public function getIsselected()
    {
        return $this->isselected;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

}
