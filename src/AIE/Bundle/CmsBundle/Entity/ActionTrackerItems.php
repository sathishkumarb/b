<?php
/*
* License: Copyright AIE - 2017
*/
namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ActionTrackerItems
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */

class ActionTrackerItems
{

    protected static $STATUS_CHOICES = array('Close' => 'Close', 'Live' => 'Live', 'Hold' => 'Hold', 'Cancelled' => 'Cancelled');
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="item", type="string", length=255)
     */
    protected $item;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="text")
     */
    protected $details;

    /**
     * @var string
     *
     * @ORM\Column(name="responsiblegroup", type="string", length=255)
     */
    protected $responsiblegroup;

    /**
     * @var string
     *
     * @ORM\Column(name="persons", type="string", length=255)
     */
    protected $persons;


    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $duedate;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $firstdefferal;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $seconddefferal;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\ManyToOne(targetEntity="ActionTracker", inversedBy="actiontrackeritems")
     * @ORM\JoinColumn(name="action_tracker_id", referencedColumnName="id")
     */
    protected $actionTracker;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Item
     *
     * $Item
     * @return ActionTrackerItems
     */
    public function setItem($Item) {
        $this->item = $Item;

        return $this;
    }

    /**
     * get persons
     *
     * @return string
     */
    public function getPersons()
    {

        return $this->persons;
    }

    /**
     * Set persons
     *
     * @param $persons
     * @return ActionTrackerItems
     */
    public function setPersons($persons) {
        $this->persons = $persons;

        return $this;
    }

    /**
     * get item
     *
     * @param item
     * @return string
     */
    public function getItem()
    {

        return $this->item;
    }

    /**
     * Set group
     *
     * $group
     * @return ActionTrackerItems
     */
    public function setResponsibleGroup($responsiblegroup) {
        $this->responsiblegroup = $responsiblegroup;

        return $this;
    }

    /**
     * get group
     *
     * @return string
     */
    public function getResponsibleGroup()
    {

        return $this->responsiblegroup;
    }

    /**
     * Set Details
     *
     * $details
     * @return ActionTrackerItems
     */
    public function setDetails($details) {
        $this->details = $details;

        return $this;
    }

    /**
     * Set Comments
     *
     * $comments
     * @return ActionTrackerItems
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * get details
     *
     * @param
     * @return details
     */
    public function getDetails()
    {

        return $this->details;
    }

    /**
     * get comments
     *
     * @param
     * @return comments
     */
    public function getComments()
    {

        return $this->comments;
    }

    /**
     * get choices
     *
     * @return choices
     */
    public function getChoices()
    {

        return self::$STATUS_CHOICES;
    }

    /**
     * get duedate
     *
     * @param duedate
     * @return ActionTrackerItems
     */
    public function getDueDate()
    {

        return $this->duedate;
    }

    /**
     * get date as string
     *
     * @param date
     * @return ActionTracker
     */
    public function getDueDateSting()
    {
        $result = $this->duedate->format('d-m-Y');

        return $result;
    }

    /**
     * get firstdeferral
     *
     * @param firstdeferral
     * @return ActionTrackerItems
     */
    public function getFirstDeferral()
    {

        return $this->firstdefferal;
    }

    /**
     * get first deferral as string
     *
     * @param firstdeferral
     * @return ActionTrackerItems
     */
    public function getFirstDeferralSting()
    {
        $result = $this->firstdefferal->format('d-m-Y');

        return $result;
    }

    /**
     * get seconddeferral
     *
     * @param seconddeferral
     * @return ActionTrackerItems
     */
    public function getSecondDeferral()
    {

        return $this->seconddefferal;
    }

    /**
     * get second deferral as string
     *
     * @param seconddeferral
     * @return ActionTrackerItems
     */
    public function getSecondDeferralSting()
    {
        $result = $this->seconddefferal->format('d-m-Y');

        return $result;
    }

    /**
     * Set duedate
     *
     * $date
     * @return ActionTrackerItems
     */
    public function setDuedate($date) {
        $this->duedate = $date;

        return $this;
    }

    /**
     * Set firstdefferal
     *
     * $date
     * @return ActionTrackerItems
     */
    public function setFirstdeferral($date) {
        $this->firstdefferal = $date;

        return $this;
    }

    /**
     * Set seconddefferal
     *
     * $date
     * @return ActionTrackerItems
     */
    public function setSeconddeferral($date) {
        $this->seconddefferal = $date;

        return $this;
    }

    /**
     * Set actiontracker
     *
     * \AIE\Bundle\CmsBundle\Entity\ActionTracker $actionTracker
     * @return ActionTrackerItems
     */
    public function setActionTracker(\AIE\Bundle\CmsBundle\Entity\ActionTracker $actionTracker) {
        $this->actionTracker = $actionTracker;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     *
     * @return ActionTracker
     */
    public function getActionTracker()
    {

        return $this->actionTracker;
    }

    /**
     * Set status
     *
     * $status
     * @return ActionTrackerItems
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * get status
     *
     * @param status
     * @return ActionTrackerItems
     */
    public function getStatus()
    {

        return $this->status;
    }


    /**
     * Set firstdefferal
     *
     * @param \DateTime $firstdefferal
     * @return ActionTrackerItems
     */
    public function setFirstdefferal($firstdefferal)
    {
        $this->firstdefferal = $firstdefferal;

        return $this;
    }

    /**
     * Get firstdefferal
     *
     * @return \DateTime 
     */
    public function getFirstdefferal()
    {
        return $this->firstdefferal;
    }

    /**
     * Set seconddefferal
     *
     * @param \DateTime $seconddefferal
     * @return ActionTrackerItems
     */
    public function setSeconddefferal($seconddefferal)
    {
        $this->seconddefferal = $seconddefferal;

        return $this;
    }

    /**
     * Get seconddefferal
     *
     * @return \DateTime 
     */
    public function getSeconddefferal()
    {
        return $this->seconddefferal;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return ActionTrackerItems
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return ActionTrackerItems
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }
}
