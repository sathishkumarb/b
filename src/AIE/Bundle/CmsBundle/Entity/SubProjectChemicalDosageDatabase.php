<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;


/**
 * SubProjectChemicalDosageDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectChemicalDosageDatabase extends AbstractFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var double
     *
     * @ORM\Column(name="platformorplant", type="string", length=120)
     * @JMS\Type("string")
     */
    protected $platformorplant;

    /**
     * @var double
     *
     * @ORM\Column(name="pandid", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $pandid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $location;

    /**
     * @var string
     *
     * @ORM\Column(name="stage", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $stage;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="linedescription", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $linedescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $serviceid;


    /**
     * @var string
     *
     * @ORM\Column(name="chemicaltradename", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $chemicaltradename;


    /**
     * @var string
     *
     * @ORM\Column(name="chemicalsupplier", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $chemicalsupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="chemicaltypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $chemicaltypeid;


    /**
     * @var string
     *
     * @ORM\Column(name="targetconcentration", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $targetconcentration;


    /**
     * @var string
     *
     * @ORM\Column(name="targetdosage", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $targetdosage;

    /**
     * @var integer
     *
     * @ORM\Column(name="pumptypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $pumptypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="pumprating", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $pumprating;

    /**
     * @var string
     *
     * @ORM\Column(name="injectionmethodid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $injectionmethodid;

    /**
     * @var string
     *
     * @ORM\Column(name="columnone", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnone;

    /**
     * @var string
     *
     * @ORM\Column(name="columntwo", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columntwo;

    /**
     * @var string
     *
     * @ORM\Column(name="columnthree", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnthree;

    /**
     * @var string
     *
     * @ORM\Column(name="columnfour", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfour;

    /**
     * @var string
     *
     * @ORM\Column(name="columnfive", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfive;

    /**
     * @var string
     *
     * @ORM\Column(name="analysis", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $analysis;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $recommendations;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spcdd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectchemicaldosagedatabase")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subprojectsystems;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    private $activityid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date_normal_injection;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date_normal_injection;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date_normal_concentration;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date_normal_concentration;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date_normal_availability;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date_normal_availability;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date_average;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date_average;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    public function getUploadDir()
    {
        return 'samplingdatabase';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return SubProjectSamplingDatabase
     */
    public function setFrom($from_date)
    {
        $this->from_date = $from_date;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from_date;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return SubProjectSamplingDatabase
     */
    public function setTo($to_date)
    {
        $this->to_date = $to_date;

        return $this;
    }

    /**
     * Set platformorplant
     *
     * @param string $platformorplant
     * @return SubProjectSamplingDatabase
     */
    public function setPlatformorplant($platformorplant)
    {
        $this->platformorplant = $platformorplant;

        return $this;
    }

    /**
     * Get platformorplant
     *
     * @return string 
     */
    public function getPlatformorplant()
    {
        return $this->platformorplant;
    }

    /**
     * Set pandid
     *
     * @param string $pandid
     * @return SubProjectSamplingDatabase
     */
    public function setPandid($pandid)
    {
        $this->pandid = $pandid;

        return $this;
    }

    /**
     * Get pandid
     *
     * @return string 
     */
    public function getPandid()
    {
        return $this->pandid;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjectSamplingDatabase
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }



    /**
     * Set serviceid
     *
     * @param integer $serviceid
     * @return SubProjectSamplingDatabase
     */
    public function setServiceid($serviceid)
    {
        $this->serviceid = $serviceid;

        return $this;
    }

    /**
     * Get serviceid
     *
     * @return integer 
     */
    public function getServiceid()
    {
        return $this->serviceid;
    }


    /**
     * Set stage
     *
     * @param string $stage
     * @return SubProjectChemicalDosageDatabase
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return string 
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SubProjectChemicalDosageDatabase
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set linedescription
     *
     * @param string $linedescription
     * @return SubProjectChemicalDosageDatabase
     */
    public function setLinedescription($linedescription)
    {
        $this->linedescription = $linedescription;

        return $this;
    }

    /**
     * Get linedescription
     *
     * @return string 
     */
    public function getLinedescription()
    {
        return $this->linedescription;
    }

    /**
     * Set chemicaltradename
     *
     * @param string $chemicaltradename
     * @return SubProjectChemicalDosageDatabase
     */
    public function setChemicaltradename($chemicaltradename)
    {
        $this->chemicaltradename = $chemicaltradename;

        return $this;
    }

    /**
     * Get chemicaltradename
     *
     * @return string 
     */
    public function getChemicaltradename()
    {
        return $this->chemicaltradename;
    }

    /**
     * Set chemicalsupplier
     *
     * @param string $chemicalsupplier
     * @return SubProjectChemicalDosageDatabase
     */
    public function setChemicalsupplier($chemicalsupplier)
    {
        $this->chemicalsupplier = $chemicalsupplier;

        return $this;
    }

    /**
     * Get chemicalsupplier
     *
     * @return string 
     */
    public function getChemicalsupplier()
    {
        return $this->chemicalsupplier;
    }

    /**
     * Set chemicaltypeid
     *
     * @param integer $chemicaltypeid
     * @return SubProjectChemicalDosageDatabase
     */
    public function setChemicaltypeid($chemicaltypeid)
    {
        $this->chemicaltypeid = $chemicaltypeid;

        return $this;
    }

    /**
     * Get chemicaltypeid
     *
     * @return integer 
     */
    public function getChemicaltypeid()
    {
        return $this->chemicaltypeid;
    }

    /**
     * Set targetconcentration
     *
     * @param string $targetconcentration
     * @return SubProjectChemicalDosageDatabase
     */
    public function setTargetconcentration($targetconcentration)
    {
        $this->targetconcentration = $targetconcentration;

        return $this;
    }

    /**
     * Get targetconcentration
     *
     * @return string 
     */
    public function getTargetconcentration()
    {
        return $this->targetconcentration;
    }

    /**
     * Set targetdosage
     *
     * @param string $targetdosage
     * @return SubProjectChemicalDosageDatabase
     */
    public function setTargetdosage($targetdosage)
    {
        $this->targetdosage = $targetdosage;

        return $this;
    }

    /**
     * Get targetdosage
     *
     * @return string 
     */
    public function getTargetdosage()
    {
        return $this->targetdosage;
    }

    /**
     * Set pumptypeid
     *
     * @param integer $pumptypeid
     * @return SubProjectChemicalDosageDatabase
     */
    public function setPumptypeid($pumptypeid)
    {
        $this->pumptypeid = $pumptypeid;

        return $this;
    }

    /**
     * Get pumptypeid
     *
     * @return integer 
     */
    public function getPumptypeid()
    {
        return $this->pumptypeid;
    }

    /**
     * Set pumprating
     *
     * @param string $pumprating
     * @return SubProjectChemicalDosageDatabase
     */
    public function setPumprating($pumprating)
    {
        $this->pumprating = $pumprating;

        return $this;
    }

    /**
     * Get pumprating
     *
     * @return string 
     */
    public function getPumprating()
    {
        return $this->pumprating;
    }

    /**
     * Set injectionmethodid
     *
     * @param integer $injectionmethodid
     * @return SubProjectChemicalDosageDatabase
     */
    public function setInjectionmethodid($injectionmethodid)
    {
        $this->injectionmethodid = $injectionmethodid;

        return $this;
    }

    /**
     * Get injectionmethodid
     *
     * @return integer 
     */
    public function getInjectionmethodid()
    {
        return $this->injectionmethodid;
    }

    /**
     * Set columnone
     *
     * @param string $columnone
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnone($columnone)
    {
        $this->columnone = $columnone;

        return $this;
    }

    /**
     * Get columnone
     *
     * @return string 
     */
    public function getColumnone()
    {
        return $this->columnone;
    }

    /**
     * Set columntwo
     *
     * @param string $columntwo
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumntwo($columntwo)
    {
        $this->columntwo = $columntwo;

        return $this;
    }

    /**
     * Get columntwo
     *
     * @return string 
     */
    public function getColumntwo()
    {
        return $this->columntwo;
    }

    /**
     * Set columnthree
     *
     * @param string $columnthree
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnthree($columnthree)
    {
        $this->columnthree = $columnthree;

        return $this;
    }

    /**
     * Get columnthree
     *
     * @return string 
     */
    public function getColumnthree()
    {
        return $this->columnthree;
    }

    /**
     * Set columnfour
     *
     * @param string $columnfour
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnfour($columnfour)
    {
        $this->columnfour = $columnfour;

        return $this;
    }

    /**
     * Get columnfour
     *
     * @return string 
     */
    public function getColumnfour()
    {
        return $this->columnfour;
    }

    /**
     * Set columnfive
     *
     * @param string $columnfive
     * @return SubProjectChemicalDosageDatabase
     */
    public function setColumnfive($columnfive)
    {
        $this->columnfive = $columnfive;

        return $this;
    }

    /**
     * Get columnfive
     *
     * @return string 
     */
    public function getColumnfive()
    {
        return $this->columnfive;
    }




    /**
     * Set subprojectsystems
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectSamplingDatabase
     */
    public function setSubProjectSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null) {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subprojectsystems
     * @return SubProjectSamplingDatabase
     */
    public function getSubProjectSystems()
    {

        return $this->subprojectsystems;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectSamplingDatabase
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectSamplingDatabase
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSamplingDatabase
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Set activityid
     *
     * @param integer $activityid
     * @return SubProjectChemicalDosageDatabase
     */
    public function setActivityid($activityid)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityid
     *
     * @return integer 
     */
    public function getActivityid()
    {
        return $this->activityid;
    }

    /**
     * Set analysis
     *
     * @param string $analysis
     * @return SubProjectChemicalDosageDatabase
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;

        return $this;
    }

    /**
     * Get analysis
     *
     * @return string
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SubProjectChemicalDosageDatabase
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set from_date_normal_injection
     *
     * @param \DateTime $fromDateNormalInjection
     * @return SubProjectChemicalDosageDatabase
     */
    public function setFromDateNormalInjection($fromDateNormalInjection)
    {
        $this->from_date_normal_injection = $fromDateNormalInjection;

        return $this;
    }

    /**
     * Get from_date_normal_injection
     *
     * @return \DateTime 
     */
    public function getFromDateNormalInjection()
    {
        return $this->from_date_normal_injection;
    }

    /**
     * Set to_date_normal_injection
     *
     * @param \DateTime $toDateNormalInjection
     * @return SubProjectChemicalDosageDatabase
     */
    public function setToDateNormalInjection($toDateNormalInjection)
    {
        $this->to_date_normal_injection = $toDateNormalInjection;

        return $this;
    }

    /**
     * Get to_date_normal_injection
     *
     * @return \DateTime 
     */
    public function getToDateNormalInjection()
    {
        return $this->to_date_normal_injection;
    }

    /**
     * Set from_date_normal_concentration
     *
     * @param \DateTime $fromDateNormalConcentration
     * @return SubProjectChemicalDosageDatabase
     */
    public function setFromDateNormalConcentration($fromDateNormalConcentration)
    {
        $this->from_date_normal_concentration = $fromDateNormalConcentration;

        return $this;
    }

    /**
     * Get from_date_normal_concentration
     *
     * @return \DateTime 
     */
    public function getFromDateNormalConcentration()
    {
        return $this->from_date_normal_concentration;
    }

    /**
     * Set to_date_normal_concentration
     *
     * @param \DateTime $toDateNormalConcentration
     * @return SubProjectChemicalDosageDatabase
     */
    public function setToDateNormalConcentration($toDateNormalConcentration)
    {
        $this->to_date_normal_concentration = $toDateNormalConcentration;

        return $this;
    }

    /**
     * Get to_date_normal_concentration
     *
     * @return \DateTime 
     */
    public function getToDateNormalConcentration()
    {
        return $this->to_date_normal_concentration;
    }

    /**
     * Set from_date_normal_availability
     *
     * @param \DateTime $fromDateNormalAvailability
     * @return SubProjectChemicalDosageDatabase
     */
    public function setFromDateNormalAvailability($fromDateNormalAvailability)
    {
        $this->from_date_normal_availability = $fromDateNormalAvailability;

        return $this;
    }

    /**
     * Get from_date_normal_availability
     *
     * @return \DateTime 
     */
    public function getFromDateNormalAvailability()
    {
        return $this->from_date_normal_availability;
    }

    /**
     * Set to_date_normal_availability
     *
     * @param \DateTime $toDateNormalAvailability
     * @return SubProjectChemicalDosageDatabase
     */
    public function setToDateNormalAvailability($toDateNormalAvailability)
    {
        $this->to_date_normal_availability = $toDateNormalAvailability;

        return $this;
    }

    /**
     * Get to_date_normal_availability
     *
     * @return \DateTime 
     */
    public function getToDateNormalAvailability()
    {
        return $this->to_date_normal_availability;
    }

    /**
     * Set from_date_average
     *
     * @param \DateTime $fromDateAverage
     * @return SubProjectChemicalDosageDatabase
     */
    public function setFromDateAverage($fromDateAverage)
    {
        $this->from_date_average = $fromDateAverage;

        return $this;
    }

    /**
     * Get from_date_average
     *
     * @return \DateTime 
     */
    public function getFromDateAverage()
    {
        return $this->from_date_average;
    }

    /**
     * Set to_date_average
     *
     * @param \DateTime $toDateAverage
     * @return SubProjectChemicalDosageDatabase
     */
    public function setToDateAverage($toDateAverage)
    {
        $this->to_date_average = $toDateAverage;

        return $this;
    }

    /**
     * Get to_date_average
     *
     * @return \DateTime 
     */
    public function getToDateAverage()
    {
        return $this->to_date_average;
    }
}
