<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

/**
 * SubProjectChemicalDosageAddColumnsDatabase
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectChemicalDosageAddColumnsDatabase
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="columnone", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnone;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $columnoneselected;


    /**
     * @var string
     *
     * @ORM\Column(name="columntwo", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $columntwo;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $columntwoselected;


    /**
     * @var string
     *
     * @ORM\Column(name="columnthree", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnthree;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $columnthreeselected;

    /**
     * @var string
     *
     * @ORM\Column(name="columnfour", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfour;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $columnfourselected;


    /**
     * @var string
     *
     * @ORM\Column(name="columnfive", type="string", length=120, nullable=true)
     * @JMS\Type("string")
     */
    protected $columnfive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $columnfiveselected;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="systems")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;

    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectSamplingDatabase
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectSamplingDatabase
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectSamplingDatabase
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects 
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set columnone
     *
     * @param string $columnone
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnone($columnone)
    {
        $this->columnone = $columnone;

        return $this;
    }

    /**
     * Get columnone
     *
     * @return string 
     */
    public function getColumnone()
    {
        return $this->columnone;
    }

    /**
     * Set columnoneselected
     *
     * @param boolean $columnoneselected
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnoneselected($columnoneselected)
    {
        $this->columnoneselected = $columnoneselected;

        return $this;
    }

    /**
     * Get columnoneselected
     *
     * @return boolean 
     */
    public function getColumnoneselected()
    {
        return $this->columnoneselected;
    }

    /**
     * Set columntwo
     *
     * @param string $columntwo
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumntwo($columntwo)
    {
        $this->columntwo = $columntwo;

        return $this;
    }

    /**
     * Get columntwo
     *
     * @return string 
     */
    public function getColumntwo()
    {
        return $this->columntwo;
    }

    /**
     * Set columntwoselected
     *
     * @param boolean $columntwoselected
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumntwoselected($columntwoselected)
    {
        $this->columntwoselected = $columntwoselected;

        return $this;
    }

    /**
     * Get columntwoselected
     *
     * @return boolean 
     */
    public function getColumntwoselected()
    {
        return $this->columntwoselected;
    }

    /**
     * Set columnthree
     *
     * @param string $columnthree
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnthree($columnthree)
    {
        $this->columnthree = $columnthree;

        return $this;
    }

    /**
     * Get columnthree
     *
     * @return string 
     */
    public function getColumnthree()
    {
        return $this->columnthree;
    }

    /**
     * Set columnthreeselected
     *
     * @param boolean $columnthreeselected
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnthreeselected($columnthreeselected)
    {
        $this->columnthreeselected = $columnthreeselected;

        return $this;
    }

    /**
     * Get columnthreeselected
     *
     * @return boolean 
     */
    public function getColumnthreeselected()
    {
        return $this->columnthreeselected;
    }

    /**
     * Set columnfour
     *
     * @param string $columnfour
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnfour($columnfour)
    {
        $this->columnfour = $columnfour;

        return $this;
    }

    /**
     * Get columnfour
     *
     * @return string 
     */
    public function getColumnfour()
    {
        return $this->columnfour;
    }

    /**
     * Set columnfourselected
     *
     * @param boolean $columnfourselected
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnfourselected($columnfourselected)
    {
        $this->columnfourselected = $columnfourselected;

        return $this;
    }

    /**
     * Get columnfourselected
     *
     * @return boolean 
     */
    public function getColumnfourselected()
    {
        return $this->columnfourselected;
    }

    /**
     * Set columnfive
     *
     * @param string $columnfive
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnfive($columnfive)
    {
        $this->columnfive = $columnfive;

        return $this;
    }

    /**
     * Get columnfive
     *
     * @return string 
     */
    public function getColumnfive()
    {
        return $this->columnfive;
    }

    /**
     * Set columnfiveselected
     *
     * @param boolean $columnfiveselected
     * @return SubProjectChemicalDosageAddColumnsDatabase
     */
    public function setColumnfiveselected($columnfiveselected)
    {
        $this->columnfiveselected = $columnfiveselected;

        return $this;
    }

    /**
     * Get columnfiveselected
     *
     * @return boolean 
     */
    public function getColumnfiveselected()
    {
        return $this->columnfiveselected;
    }
}
