<?php
/*
* License: Copyright AIE - 2017
*/

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;


/**
 * SubProjectCathodicProtection
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SubProjectCathodicProtectionDatabase extends AbstractFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="platformorplant", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $platformorplant;

    /**
     * @var string
     *
     * @ORM\Column(name="pandid", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $pandid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @JMS\Type("string")
     */
    protected $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="cptypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $cptypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="equipmenttypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $equipmenttypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     * @JMS\Type("string")
     */
    protected $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $serviceid;


    /**
     * @var integer
     *
     * @ORM\Column(name="anodetypeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $anodetypeid;


    /**
     * @var integer
     *
     * @ORM\Column(name="referenceelectrodeid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $referenceelectrodeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="monitoringmethodid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    protected $monitoringmethodid;

    /**
     * @var string
     *
     * @ORM\Column(name="analysis", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $analysis;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $recommendations;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="spcpd")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     */
    protected $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjectSystems", inversedBy="subprojectcathodicprotectiondatabase")
     * @ORM\JoinColumn(name="subprojectsystemid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $subprojectsystems;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityid", type="integer", length=10, nullable=true)
     * @JMS\Type("integer")
     */
    private $activityid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $from_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $to_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifiedat;


    public function __construct()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }

    public function getUploadDir()
    {
        return 'cathodicprotectiondatabase';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subprojectsystems
     *
     * \AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems
     * @return SubProjectCathodicProtection
     */
    public function setSubProjectSystems(\AIE\Bundle\CmsBundle\Entity\SubProjectSystems $subprojectsystems = null) {
        $this->subprojectsystems = $subprojectsystems;

        return $this;
    }

    /**
     * get subprojectsystems
     *
     * @param subprojectsystems
     * @return SubProjectCathodicProtection
     */
    public function getSubProjectSystems()
    {

        return $this->subprojectsystems;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return SubProjectCathodicProtection
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return SubProjectCathodicProtection
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectCathodicProtection
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects 
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Set platformorplant
     *
     * @param string $platformorplant
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setPlatformorplant($platformorplant)
    {
        $this->platformorplant = $platformorplant;

        return $this;
    }

    /**
     * Get platformorplant
     *
     * @return string 
     */
    public function getPlatformorplant()
    {
        return $this->platformorplant;
    }

    /**
     * Set pandid
     *
     * @param string $pandid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setPandid($pandid)
    {
        $this->pandid = $pandid;

        return $this;
    }

    /**
     * Get pandid
     *
     * @return string 
     */
    public function getPandid()
    {
        return $this->pandid;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set cptypeid
     *
     * @param integer $cptypeid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setCptypeid($cptypeid)
    {
        $this->cptypeid = $cptypeid;

        return $this;
    }

    /**
     * Get cptypeid
     *
     * @return integer 
     */
    public function getCptypeid()
    {
        return $this->cptypeid;
    }

    /**
     * Set equipmenttypeid
     *
     * @param integer $equipmenttypeid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setEquipmenttypeid($equipmenttypeid)
    {
        $this->equipmenttypeid = $equipmenttypeid;

        return $this;
    }

    /**
     * Get equipmenttypeid
     *
     * @return integer 
     */
    public function getEquipmenttypeid()
    {
        return $this->equipmenttypeid;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set serviceid
     *
     * @param integer $serviceid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setServiceid($serviceid)
    {
        $this->serviceid = $serviceid;

        return $this;
    }

    /**
     * Get serviceid
     *
     * @return integer 
     */
    public function getServiceid()
    {
        return $this->serviceid;
    }

    /**
     * Set anodetypeid
     *
     * @param integer $anodetypeid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setAnodetypeid($anodetypeid)
    {
        $this->anodetypeid = $anodetypeid;

        return $this;
    }

    /**
     * Get anodetypeid
     *
     * @return integer 
     */
    public function getAnodetypeid()
    {
        return $this->anodetypeid;
    }

    /**
     * Set referenceelectrodeid
     *
     * @param integer $referenceelectrodeid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setReferenceelectrodeid($referenceelectrodeid)
    {
        $this->referenceelectrodeid = $referenceelectrodeid;

        return $this;
    }

    /**
     * Get referenceelectrodeid
     *
     * @return integer 
     */
    public function getReferenceelectrodeid()
    {
        return $this->referenceelectrodeid;
    }

    /**
     * Set monitoringmethodid
     *
     * @param integer $monitoringmethodid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setMonitoringmethodid($monitoringmethodid)
    {
        $this->monitoringmethodid = $monitoringmethodid;

        return $this;
    }

    /**
     * Get monitoringmethodid
     *
     * @return integer 
     */
    public function getMonitoringmethodid()
    {
        return $this->monitoringmethodid;
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modifiedat = new \DateTime("now");
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
        $this->modifiedat = new \DateTime("now");
    }


    /**
     * Set activityid
     *
     * @param integer $activityid
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setActivityid($activityid)
    {
        $this->activityid = $activityid;

        return $this;
    }

    /**
     * Get activityid
     *
     * @return integer 
     */
    public function getActivityid()
    {
        return $this->activityid;
    }

    /**
     * Set analysis
     *
     * @param string $analysis
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setAnalysis($analysis)
    {
        $this->analysis = $analysis;

        return $this;
    }

    /**
     * Get analysis
     *
     * @return string
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SubProjectCathodicProtectionDatabase
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }


    /**
     * Set from
     *
     * @param \DateTime $from
     * @return SubProjectSamplingDatabase
     */
    public function setFrom($from_date)
    {
        $this->from_date = $from_date;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from_date;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return SubProjectSamplingDatabase
     */
    public function setTo($to_date)
    {
        $this->to_date = $to_date;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to_date;
    }
}
