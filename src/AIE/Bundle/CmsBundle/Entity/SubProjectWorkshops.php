<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubProjectWorkshops
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\CmsBundle\Repository\SubProjectWorkshopsRepository")
 */
class SubProjectWorkshops
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="initiallyAssessedBy", type="string", length=255)
     */
    private $initiallyAssessedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="workshop")
     * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
     * */
    private $subproject;

    /**
     * @ORM\OneToMany(targetEntity="WorkshopAttendee" , mappedBy="workshop" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $spwa;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spwa = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initiallyAssessedBy
     *
     * @param string $initiallyAssessedBy
     * @return SubProjectWorkshops
     */
    public function setInitiallyAssessedBy($initiallyAssessedBy)
    {
        $this->initiallyAssessedBy = $initiallyAssessedBy;

        return $this;
    }

    /**
     * Get initiallyAssessedBy
     *
     * @return string 
     */
    public function getInitiallyAssessedBy()
    {
        return $this->initiallyAssessedBy;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SubProjectWorkshops
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set subproject
     *
     * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
     * @return SubProjectWorkshops
     */
    public function setSubproject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null)
    {
        $this->subproject = $subproject;

        return $this;
    }

    /**
     * Get subproject
     *
     * @return \AIE\Bundle\CmsBundle\Entity\SubProjects 
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    /**
     * Add spwa
     *
     * @param \AIE\Bundle\CmsBundle\Entity\WorkshopAttendee $spwa
     * @return SubProjectWorkshops
     */
    public function addSpwa(\AIE\Bundle\CmsBundle\Entity\WorkshopAttendee $spwa)
    {
        $this->spwa[] = $spwa;

        return $this;
    }

    /**
     * Remove spwa
     *
     * @param \AIE\Bundle\CmsBundle\Entity\WorkshopAttendee $spwa
     */
    public function removeSpwa(\AIE\Bundle\CmsBundle\Entity\WorkshopAttendee $spwa)
    {
        $this->spwa->removeElement($spwa);
    }

    /**
     * Get spwa
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpwa()
    {
        return $this->spwa;
    }
}
