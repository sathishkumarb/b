<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface;
//use Doctrine\Common\Collections\ArrayCollection; 


/**
 * UserCcmGroup
 *
 * @ORM\Table(name="user_ccm_groups")
 * @ORM\Entity
 */
class UserCcmGroup {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


	/**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;


    /**
     * Set user
     *
     * @param \AIE\Bundle\UserBundle\Entity\User $user
     * @return UserCcmGroup
     */
    public function setUser(\AIE\Bundle\UserBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AIE\Bundle\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\UserBundle\Entity\CcmGroup $group
     * @return UserCcmGroup
     */
    public function setGroup(\AIE\Bundle\UserBundle\Entity\CcmGroup $group = null) {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface 
     */
    public function getGroup() {
        return $this->group;
    }

       /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

}