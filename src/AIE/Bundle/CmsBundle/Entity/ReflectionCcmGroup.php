<?php

namespace AIE\Bundle\CmsBundle\Entity;


use AIE\Bundle\UserBundle\Model\CcmGroupInterface;
use AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="reflectionccmgroups")
 */
class ReflectionCcmGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="gid", type="integer")
     */
    protected $gid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=256, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="text", length=4294967295, nullable=true)
     */
    protected $roles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getName()
    {
        return $this->name;
    }

        /**
     * Get id
     *
     * @return integer 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function setName($name)
    {
       $this->name = $name;
       return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function setRoles($roles)
    {
       $this->roles = $roles;
       return $this;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function setGid($gid)
    {
       $this->gid = $gid;
       return $this;
    }

        /**
     * Get id
     *
     * @return integer 
     */
    public function getGid()
    {
        return $this->gid;
    }
        
}