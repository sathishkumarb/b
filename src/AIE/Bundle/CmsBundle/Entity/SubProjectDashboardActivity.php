<?php

namespace AIE\Bundle\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SubProjectDashboardActivity {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastdownloadedon;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lastdownloadedon = new \DateTime("now");
    }

	/**
	 * @ORM\ManyToOne(targetEntity="SubProjects", inversedBy="SubProjects")
	 * @ORM\JoinColumn(name="sub_project_id", referencedColumnName="id")
	 * */
	private $subprojectdashboardactivity;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set subproject
	 *
	 * @param \AIE\Bundle\CmsBundle\Entity\SubProjects $subproject
	 * @return SubProjects
	 */
	public function setSubProject(\AIE\Bundle\CmsBundle\Entity\SubProjects $subproject = null) {
		$this->subproject = $subproject;

		return $this;
	}

	/**
	 * Get subproject
	 *
	 * @return \AIE\Bundle\CmsBundle\Entity\SubProjects
	 */
	public function getSubProject() {
		return $this->subproject;
	}


    /**
     * Set lastdownloadedon
     *
     * @param \DateTime $lastdownloadedon
     *
     * @return Report
     */
    public function setLastDownloadedOn(\DateTime $lastdownloadedon)
    {
        $this->lastdownloadedon = $lastdownloadedon;
        return $this;
    }
    /**
     * Get lastdownloadedon
     *
     * @return \DateTime
     */
    public function getLastDownloadedOn()
    {
        return $this->lastdownloadedon;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->lastdownloadedon = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->lastdownloadedon = new \DateTime("now");
    }
}