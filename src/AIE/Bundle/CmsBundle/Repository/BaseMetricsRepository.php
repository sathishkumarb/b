<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class BaseMetricsRepository extends EntityRepository {

    public function findAll()
    {
        return $this->findBy(array('title'), array('title' => 'ASC'));
    }
    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('base_metrics')
            ->orderBy('base_metrics.title', 'ASC');
    }

}
