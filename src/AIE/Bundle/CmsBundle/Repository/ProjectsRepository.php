<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProjectsRepository extends EntityRepository {

    public function findAll()
    {
        return $this->findBy(array(), array('name' => 'ASC'));
    }


}
