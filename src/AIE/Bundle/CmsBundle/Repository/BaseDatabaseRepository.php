<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\DBAL\LockMode;

class BaseDatabaseRepository extends EntityRepository {

    public function find($id,$lockMode = LockMode::NONE, $lockVersion = NULL)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder('c');
        $query = $qb->select('req')
            ->from('AIECmsBundle:BaseDatabase','req');
        $query->setMaxResults(1);
        return $result = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

    }

}
