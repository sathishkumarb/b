<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubProjectMatricesReportsRepository extends EntityRepository
{

    public function updateApproved($subprojectId, $reportingFromDate, $reportingToDate, $period)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->update('AIE\Bundle\CmsBundle\Entity\SubProjectMatricesReports', 'sbmr')
            ->set('sbmr.isApproved', 0)
            ->where('sbmr.subproject = :id')->setParameter('id', $subprojectId)
            ->andWhere('sbmr.reportingperiodfromdate = :start')
            ->andWhere('sbmr.reportingperiodtodate = :end')
            ->setParameter('start', $reportingFromDate->format('Y-m-d'))
            ->setParameter('end', $reportingToDate->format('Y-m-d'))
        ;
        return $qb->getQuery()
            ->execute();
    }

    public function getMaxGroupedReportsSub($subprojectId)
    {
        $subQuery = $this->createQueryBuilder('d');

        $qb = $this->createQueryBuilder('m');


        $subQuery->select('MAX(d.id) as id')
            ->where('d.subproject = :subprojectId')
            ->andWhere('d.reporttype <> 4')
            ->addGroupBy('d.reportingperiodtodate')
            ->addOrderBy('d.id');

        $qb->select('m')
            ->where($qb->expr()->in('m.id', $subQuery->getDQL()))
            ->setParameter('subprojectId', $subprojectId);


        return $qb->getQuery()->getResult();
    }

    public function getMaxGroupedReports($subprojectId, $approved=null)
    {

        $subQuery = $this->createQueryBuilder('d');

        $qb = $this->createQueryBuilder('m');


            $subQuery->select('MAX(d.id) as id')
                ->where('d.subproject = :subprojectId')
                ->andWhere('d.isApproved = 1')
                ->andWhere('d.reporttype <> 4')
                ->addGroupBy('d.reportingperiodtodate')
                ->addOrderBy('d.id');

            $qb->select('m')
                ->where($qb->expr()->in('m.id', $subQuery->getDQL()))
                ->setParameter('subprojectId', $subprojectId);


        return $qb->getQuery()->getResult();

    }

    public static function getQuarterByMonth($monthNumber)
    {
        return floor(($monthNumber - 1) / 3) + 1;
    }

    public static function getHalfByMonth($monthNumber)
    {
        return floor(($monthNumber - 1) / 6) + 1;
    }

    public function getQuarterReports($subprojectId, $fromDate, $toDate)
    {
        $em = $this->getEntityManager();

        $now = new \DateTime("now");
        $currMonth = $now->format('m');
        $currYear = $now->format('Y');

        $quarter = self::getQuarterByMonth($currMonth);

        if(is_object($toDate) && is_object($fromDate))
        {
            $toYear = $toDate->format('Y');
            $toMonth = $toDate->format('m');
            $ignoreFromDate = $fromDate->format('Y-m-d');
            $ignoreToDate = $toDate->format('Y-m-d');
        }
        else
        {
            $explodeVarYear = explode(" ", $toDate);
            if ($explodeVarYear) {
                $toYear = $explodeVarYear[2];
                $tdate = \DateTime::createFromFormat('j-M-Y', str_replace(" ", "-", $toDate));
                $toMonth = $tdate->format('m');
                $ignoreFromDate = \DateTime::createFromFormat('j-M-Y', str_replace(" ", "-", $fromDate));
                $ignoreFromDate = $ignoreFromDate->format('Y-m-d');

                $ignoreToDate = \DateTime::createFromFormat('j-M-Y', str_replace(" ", "-", $toDate));
                $ignoreToDate = $ignoreToDate->format('Y-m-d');
            }
        }

        if ($subprojectId)
        {
            if ($toYear != $currYear)
                $sql = "SELECT id, QUARTER(reportingperiodtodate) as quarter, reportingperiodfromdate, reportingperiodtodate, complianceData FROM SubProjectMatricesReports where isApproved=1 and sub_project_id=".$subprojectId." and YEAR(reportingperiodtodate) <= ".$toYear." and MONTH(reportingperiodtodate) < ".$toMonth." ORDER BY reportingperiodtodate DESC LIMIT 4";
            else
                $sql = "SELECT id, QUARTER(reportingperiodtodate) as quarter, reportingperiodfromdate, reportingperiodtodate, complianceData FROM SubProjectMatricesReports where isApproved=1 and sub_project_id=".$subprojectId." and date(reportingperiodfromdate) <> '".$ignoreFromDate."' and date(reportingperiodtodate) <> '".$ignoreToDate. "' and date(reportingperiodfromdate) < '".$ignoreFromDate."' ORDER BY reportingperiodtodate DESC LIMIT 3";

            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        }

    }

}
