<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FilesCategoriesRepository extends EntityRepository {

    public function deleteCategory($id)
    {
        $em = $this->getEntityManager();
        //set files category to null
        $q = $em->createQuery('UPDATE AIE\Bundle\CmsBundle\Entity\Files f set f.category = NULL WHERE f.category = :id');
        $q->setParameters(['id' => $id ]);
        $q->execute();
        //delete sub categories
        $q = $em->createQuery('DELETE AIE\Bundle\CmsBundle\Entity\FilesCategories fc WHERE fc.parent = :id');
        $q->setParameters(['id' => $id ]);
        $q->execute();

        //delete parent category
        $q = $em->createQuery('DELETE AIE\Bundle\CmsBundle\Entity\FilesCategories fc WHERE fc.id = :id');
        $q->setParameters(['id' => $id ]);
        $nonAffected = $q->execute();

        return $nonAffected > 0;
    }
}
