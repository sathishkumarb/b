<?php

namespace AIE\Bundle\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubProjectWorkshopsRepository extends EntityRepository {

    public function getLatestWorkshop($subprojectId,$limit,$offset)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select( 'e' )
            ->from( 'AIECmsBundle:SubProjectWorkshops',  'e' )
            ->where('e.subproject = :id')->setParameter('id', $subprojectId)
            ->setMaxResults( $limit )
            ->setFirstResult( $offset )
            ->orderBy('e.id', 'ASC');

        $workshops = $qb->getQuery()->getResult();

        return $workshops;
    }


}
