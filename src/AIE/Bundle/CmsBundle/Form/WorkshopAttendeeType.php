<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class WorkshopAttendeeType extends AbstractType
{
    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('position', 'text', $this->options(array('label' => 'Position')))
            ->add('company', 'text', $this->options(array('label' => 'Company')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\WorkshopAttendee'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_workshopattendee';
    }
}
