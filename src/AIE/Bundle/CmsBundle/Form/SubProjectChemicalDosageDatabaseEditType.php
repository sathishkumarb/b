<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase;
use AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;


class SubProjectChemicalDosageDatabaseEditType extends AbstractType
{
    use Helper\FormStyleHelper;

    protected $em;
    protected $projectId;
    protected $subprojectId;
    protected $servicedt;
    protected $orientation;
    protected $activityperson;
    protected $dbcols;
    protected $sId;

    function __construct(SubProjectChemicalDosageDatabase $entity, $em, $projectId, $subprojectId, $sId, $dbcols)
    {
        $this->entity = $entity;
        $this->em = $em;
        $this->projectId = $projectId;
        $this->subprojectId = $subprojectId;
        $this->dbcols = $dbcols;
        $this->sId = $sId;

        $this->servicedt = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,1,$this->em);
        $this->pumptype = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,21,$this->em);
        $this->chemicaltype = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,20,$this->em);
        $this->injectionmethod = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,22,$this->em);
        $this->qb = $this->em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivityForSystem($this->subprojectId, $this->sId, 2);

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'activityid', "choice",
                $this->options(
                    [
                        "label" => "* Activity",
                        'required' => true,
                        "choices" => $this->qb,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'platformorplant',
                'text',
                $this->options(
                    [
                        'label' => '* Platform/Plant',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4', 'class' => 'required', 'style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'pandid',
                'text',
                $this->options(
                    [
                        'label' => 'P&ID',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'location',
                'text',
                $this->options(
                    [
                        'label' => '* Location',
                        'required' => true,
                        'constraints' => [new NotBlank()],
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'stage',
                'text',
                $this->options(
                    [
                        'label' => 'Stage',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'type',
                'text',
                $this->options(
                    [
                        'label' => 'Type',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'linedescription',
                'text',
                $this->options(
                    [
                        'label' => 'Line Description',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'serviceid', "choice",
                $this->options(
                    [
                        "label" => "Service",
                        'required' => false,
                        "choices" => $this->servicedt,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'chemicaltradename',
                'text',
                $this->options(
                    [
                        'label' => 'Chemical Trade Name',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'chemicalsupplier',
                'text',
                $this->options(
                    [
                        'label' => 'Chemical Supplier',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'chemicaltypeid', "choice",
                $this->options(
                    [
                        "label" => "Chemical Type",
                        'required' => false,
                        "choices" => $this->chemicaltype,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'targetconcentration',
                'text',
                $this->options(
                    [
                        'label' => 'Target Concentration',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'targetdosage',
                'text',
                $this->options(
                    [
                        'label' => 'Target Dosage',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'pumptypeid', "choice",
                $this->options(
                    [
                        "label" => "Pump Type",
                        'required' => false,
                        "choices" => $this->pumptype,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'pumprating',
                'text',
                $this->options(
                    [
                        'label' => 'Pump Rating',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'injectionmethodid', "choice",
                $this->options(
                    [
                        "label" => "Injection Method",
                        'required' => false,
                        "choices" => $this->injectionmethod,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            );
            if ($this->dbcols && $this->dbcols->getColumnoneselected() == 1)
            {
                $builder->add(
                    'columnone',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnone(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumntwoselected() == 1) {
                $builder->add(
                    'columntwo',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumntwo(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnthreeselected() == 1) {
                $builder->add(
                    'columnthree',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnthree(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnfourselected() == 1) {
                $builder->add(
                    'columnfour',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnfour(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnfiveselected() == 1) {
                $builder->add(
                    'columnfive',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnfive(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectchemicaldosagedatabase';
    }
}
