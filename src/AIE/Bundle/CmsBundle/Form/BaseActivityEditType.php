<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;

use AIE\Bundle\CmsBundle\Form\BaseControlActivityType;
use AIE\Bundle\CmsBundle\Form\BaseManagementActivityType;

class BaseActivityEditType extends AbstractType
{
    use Helper\FormStyleHelper;

    protected $em;
    protected $editid;

    // These variables are used for storing fields under groups

    function __construct($em,$editid)
    {
        $this->em = $em;
        $this->editid = $editid;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->editid) {

            $qb = $this->em->createQueryBuilder();

            $fields = array('bs.id','bs.title');

            $query = $qb->select('bs')
                ->from('AIECmsBundle:BaseMetricsConfig', 'bm')
                ->innerJoin('AIECmsBundle:BaseDatabaseType', 'bd', 'WITH', 'bd.id = bm.databasetype')
                ->innerJoin('AIECmsBundle:BaseDatasheetType', 'bs', 'WITH', 'bs.id = bm.datasheettype');

            $qb->andWhere('bm.id = :uid')
                ->setParameter('uid', $this->editid)
                ->addOrderBy('bm.title', 'ASC');

            $datasheets = $query->getQuery()->getResult();

        }
        $builder
            ->add('basemetrics', 'entity', $this->options([
                'data' => $this->em->getReference("AIECmsBundle:BaseMetricsConfig", $this->editid),
                'empty_value' => '-- Select a Metric --',
                'class' => 'AIECmsBundle:BaseMetricsConfig',
                'attr' => array(
                    'class' => 'elementMetrics'
                ),
                'label' => false,
                'mapped' => false
            ]))
            ->add('basedatasheet', 'entity', $this->options([
                'class' => 'AIECmsBundle:BaseDatasheetType',
                'choices' => $datasheets,
                'required'    => true,
                'label' => false,
                'attr' => array(
                    'class' => 'elementDatasheet','style' => 'width: 100px',
                ),
            ]));

        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    protected function addElements(FormInterface $form, BaseMetricsConfig $basemetrics = null) {
        // Remove the submit button, we will place this at the end of the form later

        $form->add('basemetrics', 'entity', array(
                'data' => $basemetrics,
                'empty_value' => '-- Select a Metric --',
                'class' => 'AIECmsBundle:BaseMetricsConfig',
                'attr' => array(
                    'class' => 'elementMetrics','style' => 'width: 100px',
                ),
                'label' => false,
                'mapped' => false)
        );
        $datasheets = array();

        if ($basemetrics) {

            $qb = $this->em->createQueryBuilder();

            $fields = array('bs.id','bs.title');

            $query = $qb->select('bs')
                ->from('AIECmsBundle:BaseMetricsConfig', 'bm')
                ->innerJoin('AIECmsBundle:BaseDatabaseType', 'bd', 'WITH', 'bd.id = bm.databasetype')
                ->innerJoin('AIECmsBundle:BaseDatasheetType', 'bs', 'WITH', 'bs.id = bm.datasheettype');

            $qb->andWhere('bm.id = :uid')
                ->setParameter('uid', $basemetrics);

            $datasheets = $query->getQuery()->getResult();

        }

        $form->add('basedatasheet', 'entity', array(
            'empty_value' => '-- Select a DataSheet --',
            'class' => 'AIECmsBundle:BaseDatasheetType',
            'choices' => $datasheets,
            'attr' => array(
                'class' => 'elementDatasheet','style' => 'width: 100px',
            ),
            'label' => false,
        ));


    }

    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Note that the data is not yet hydrated into the entity.
        $metrics = $this->em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($data['basemetrics']);
        $this->addElements($form, $metrics);
    }



    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\BaseActivity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_baseactivtiy_edit';
    }
}
