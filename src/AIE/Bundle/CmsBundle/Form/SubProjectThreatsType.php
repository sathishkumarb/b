<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class SubProjectThreatsType extends AbstractType
{
    use Helper\FormStyleHelper;


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('threatstitle', 'text', $this->options(['label' => false]));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectThreats',
            'attr' => array(
                'class' => 'form-horizontal-from-default'
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectthreats';
    }
}
