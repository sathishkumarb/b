<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;

class SubProjectProductionDatabaseEditType extends AbstractType
{

    use Helper\FormStyleHelper;

    protected $em;
    protected $key;
    protected $subprojectId;
    protected $sId;
    protected $dbcols;
    protected $qb;

    public function __construct(SubProjectProductionDatabase $entity, $em, $subprojectId, $sId, $dbcols) {
        $this->entity = $entity;
        $this->em = $em;
        $this->subprojectId = $subprojectId;
        $this->sId = $sId;
        $this->dbcols = $dbcols;
        $this->qb = $this->em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivityForSystem($this->subprojectId, $this->sId, 6);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'activityid', "choice",
                $this->options(
                    [
                        "label" => "* Activity",
                        'required' => true,
                        "choices" => $this->qb,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'location',
                'text',
                $this->options(
                    [
                        'label' => '* Location',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                $builder->create('welltestdate',
                    new DatePickerType(),
                    $this->options(
                        [
                            'label' => 'Well Test date',
                            'required' => false,
                            'attr' => ['form_class'=>'col-md-12','container_class'=>'col-md-3','style'=>''],
                            'label_attr' => ['class' => 'col-md-3'],
                        ],
                        'date_picker'
                    )
                )->addModelTransformer(new DateTimePickerTransformer())
            )
            ->add(
                'wellfunction',
                'text',
                $this->options(
                    [
                        'label' => 'Well Function',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'wellstatus',
                'text',
                $this->options(
                    [
                        'label' => 'Well Status',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'contractor',
                'text',
                $this->options(
                    [
                        'label' => 'Contractor',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'welltesttype',
                'text',
                $this->options(
                    [
                        'label' => 'Well Test Type',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'flowpath',
                "text",
                $this->options(
                    [
                        "label" => "Flow Path",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'chokesize',
                "text",
                $this->options(
                    [
                        "label" => "Choke Size",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'reservoir',
                "text",
                $this->options(
                    [
                        "label" => "Reservoir",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'dgs',
                "text",
                $this->options(
                    [
                        "label" => "DGS",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'train',
                "text",
                $this->options(
                    [
                        "label" => "Train",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'fieldmanifold',
                "text",
                $this->options(
                    [
                        "label" => "Field Manifold",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'stationmanifold',
                "text",
                $this->options(
                    [
                        "label" => "Station Manifold",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'comments',
                "text",
                $this->options(
                    [
                        "label" => "Comments",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add('file', 'file', $this->options(array('label' => 'Upload','required' => false), 'file'));
            if ($this->dbcols && $this->dbcols->getColumnoneselected() == 1)
            {
                $builder->add(
                    'columnone',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnone(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumntwoselected() == 1) {
                $builder->add(
                    'columntwo',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumntwo(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnthreeselected() == 1) {
                $builder->add(
                    'columnthree',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnthree(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnfourselected() == 1) {
                $builder->add(
                    'columnfour',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnfour(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }
            if ($this->dbcols && $this->dbcols->getColumnfiveselected() == 1) {
                $builder->add(
                    'columnfive',
                    'text',
                    $this->options(
                        [
                            'label' => $this->dbcols->getColumnfive(),
                            'required' => false,
                            'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                            'label_attr' => ['class' => ''],
                        ]
                    )
                );
            }

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectProductionDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectcathodicprotectiondatabase';
    }

}
