<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class SubProjectsType extends AbstractType
{
    use Helper\FormStyleHelper;
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = array('weekly' => 'Weekly', 'monthly' => 'Monthly', 'quarterly' => 'Quarterly', 'tri-annual' => 'Tri-annual', 'bi-annual' => 'Bi-annual', 'two-monthly' => 'Two-monthly');
        
        $builder
            ->add('referenceNo', 'text', $this->options(array('label' => 'Reference Number')))
            ->add('name', 'text', $this->options(array('label' => 'Sub-Project Name')))
            ->add('location', 'text', $this->options(array('label' => 'Location')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description'), 'textarea'))
            ->add('reportingperiod', 'choice', $this->options(array('label' => 'Reporting Period', 'choices' => $choices)))
            ->add('file', 'file', $this->options(array('label' => 'Sub-Project Drawing','required' => false), 'file'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjects',
            'attr' => array(
                'class' => 'form-horizontal-from-default'
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojects';
    }
}
