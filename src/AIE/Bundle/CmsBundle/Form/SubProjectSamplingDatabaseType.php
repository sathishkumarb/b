<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;

class SubProjectSamplingDatabaseType extends AbstractType
{

    use Helper\FormStyleHelper;

    protected $em;
    protected $entity;
    protected $subprojectId;
    protected $sId;
    protected $servicedt;
    protected $orientation;
    protected $activityperson;
    Protected $qb;

    function __construct($entity, $em, $subprojectId, $sId)
    {
        $this->em = $em;
        $this->entity = $entity;
        $this->subprojectId = $subprojectId;
        $this->sId = $sId;
        $this->servicedt = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,1,$this->em);
        $this->orientation = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,3,$this->em);
        $this->activityperson = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,2,$this->em);
        $this->qb = $this->em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivityForSystem($this->subprojectId, $this->sId, 4);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'activityid', "choice",
                $this->options(
                    [
                        "label" => "* Activity",
                        'required' => true,
                        "choices" => $this->qb,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'platformorplant',
                'text',
                $this->options(
                    [
                        'label' => '* Platform/Plant',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'pandid',
                'text',
                $this->options(
                    [
                        'label' => 'P&ID',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'location',
                'text',
                $this->options(
                    [
                        'label' => '* Location',
                        'required' => true,
                        'constraints' => [new NotBlank()],
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'linenumber',
                'text',
                $this->options(
                    [
                        'label' => 'Line Number',
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'required' => false,
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'waterchemistry',
                'text',
                $this->options(
                    [
                        'label' => 'Water Chemistry',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'orientationid', "choice",
                $this->options(
                    [
                        "label" => "Orientation",
                        'required' => false,
                        "choices" => $this->orientation,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'serviceid', "choice",
                $this->options(
                    [
                        "label" => "Service",
                        'required' => false,
                        "choices" => $this->servicedt,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'samplingprocedure',
                'text',
                $this->options(
                    [
                        'label' => 'Sampling Procedure',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'analysisprocedure',
                'text',
                $this->options(
                    [
                        'label' => 'Analysis Procedure',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'samplingpointtype',
                'text',
                $this->options(
                    [
                        'label' => 'Sampling Point Type',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'activitypersonid',
                "choice",
                $this->options(
                    [
                        "label" => "Activity Person",
                        'required' => false,
                        "choices" => $this->activityperson,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add('file', 'file', $this->options(array('label' => 'Upload','required' => false), 'file'));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectsamplingdatabase';
    }

}
