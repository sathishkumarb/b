<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\Request;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RequestType extends AbstractType
{
    use FormStyleHelper;

	private $entity;
	private $files;

	public function __construct(Request $entity, $files) {
		$this->entity = $entity;
		$this->files = $files;
	}


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', 'choice',  $this->options([
                'label'=>'Stage Change',
                'choices' => Request::getStatusChoices()
            ]))
            ->add('justification', 'textarea', $this->options(['label'=>'Justification'],'textarea'))
	        ->add('responseFiles', new SelectableFileType(), $this->options(
		        [
			        'class' => 'AIE\Bundle\AnomalyBundle\Entity\Files',
			        'property' => 'object',
			        'choices' => $this->files,
			        'expanded' => false,
			        'multiple' => true,
                    'label' => 'Response Attachment',
			        'data' => $this->entity->getResponseFiles(),
			        'attr' => ['class' => 'files']
		        ], 'entity'
	        )
	        )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Request'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_request';
    }
}
