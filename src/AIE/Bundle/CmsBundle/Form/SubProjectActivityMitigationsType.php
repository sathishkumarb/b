<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;

class SubProjectActivityMitigationsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;
    private $files;
    private $choices;
    private $permissions;

    public function __construct(SubProjectActivity $entity, $files, $elementPermissions) {
        $this->entity = $entity;
        $this->files = $files;
        $this->choices = $this->entity->getKpiChoices();
        $this->permissions = $elementPermissions;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $date = ($this->entity->getLastInspectionDate()) ? $this->entity->getLastInspectionDate() : new \DateTime();

        if ($this->entity->getIsmanagement() == 1 || $this->entity->getIsflowassuranceManagement() == 1) {
            if ($this->permissions['mitigationStatusManagementKpiGranted']==1) {
                $builder
                    ->add('justification_management', 'textarea', $this->options(array('attr' => array('class' => 'justificationmanagement'), 'required' => false, 'data' => $this->entity->getJustificationManagement()), 'textarea'))
                    ->add('kpi_management', 'choice', $this->options(array('required' => false, 'choices' => $this->choices,
                        'data' => $this->entity->getKpiManagement(),
                        'attr' => array('class' => 'risk-select risk-management'),
                        "empty_value" => '--Select--',
                    )));
            }
            else
            {
                $builder
                    ->add('justification_management', 'textarea', $this->options(array('attr' => array('class' => 'justificationmanagement','disabled' => true), 'required' => false, 'data' => $this->entity->getJustificationManagement()), 'textarea'))
                    ->add('kpi_management', 'choice', $this->options(array('required' => false, 'choices' => $this->choices,
                        'data' => $this->entity->getKpiManagement(),
                        'attr' => array('class' => 'risk-select risk-management','disabled' => true),
                        "empty_value" => '--Select--',
                    )));
            }
        }
        if ($this->entity->getIscontrol() == 1 || $this->entity->getIsflowassuranceControl() == 1){
            if ($this->permissions['mitigationStatusControlKpiGranted']==1) {
                $builder
                    ->add('justification_control', 'textarea', $this->options(array('attr' => array('class' => 'justificationcontrol'), 'required' => false, 'data' => $this->entity->getJustificationControl()), 'textarea'))
                    ->add('kpi_control', 'choice', $this->options(array('required' => false, 'choices' => $this->choices,
                        'data' => $this->entity->getKpiControl(),
                        'attr' => array('class' => 'risk-select risk-control'),
                        "empty_value" => '--Select--',
                    )));
            }
            else{
                $builder
                    ->add('justification_control', 'textarea', $this->options(array('attr' => array('class' => 'justificationcontrol','disabled' => true), 'required' => false, 'data' => $this->entity->getJustificationControl()), 'textarea'))
                    ->add('kpi_control', 'choice', $this->options(array('required' => false, 'choices' => $this->choices,
                        'data' => $this->entity->getKpiControl(),
                        'attr' => array('class' => 'risk-select risk-control','disabled' => true),
                        "empty_value" => '--Select--',
                    )));
            }
        }
        if ($this->permissions['mitigationStatusLastDueDateGranted']==1) {
            $builder
                ->add('last_inspection_date', new DatePickerType(), array('label' => false, 'data' => $date->format('d M Y')));
        } else{
            $builder
                ->add('last_inspection_date', 'text', array('label' => false, 'data' => $date->format('d M Y'), 'attr' => array('disabled' => true)));
        }

        if ($this->permissions['mitigationStatusFilesGranted']==1) {
            $builder
                ->add('files', new SelectableFileType(), $this->options(
                    array(
                        'class' => 'AIE\Bundle\CmsBundle\Entity\Files',
                        'property' => 'object',
                        'choices' => $this->files,
                        'expanded' => false,
                        'multiple' => true,
                        'data' => $this->entity->getFiles(),
                        'attr' => array('class' => 'files')
                    ), 'entity'
                )
                );
        }
        else{
            $builder
                ->add('files', new SelectableFileType(), $this->options(
                    array(
                        'class' => 'AIE\Bundle\CmsBundle\Entity\Files',
                        'attr' => array('disabled' => true)
                    ), 'entity'
                )
                );
        }

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectActivity'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_cmsbundle_subprojectactivitymitigation';
    }

}
