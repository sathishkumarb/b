<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjects;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProjectGroupType extends AbstractType {

    use Helper\FormStyleHelper;

    function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            /*
              ->add('user', 'entity', array(
              'class' => 'UserBundle:User',
              'property' => 'username',
              )) */
            ->add('project', 'entity', $this->options(array(
                'class' => 'AIE\Bundle\CmsBundle\Entity\Projects',
                'property' => 'name',
            )))
            ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))
            ->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'))
            ->add('group', 'entity', $this->options(array(
                'class' => 'UserBundle:CcmGroup',
                'property' => 'name',
                'label' => 'Position'
            )));
        // Add listeners

    }
    protected function addElements(FormInterface $form, SubProjects $subproject = null) {
        // Remove the submit button, we will place this at the end of the form later
//        $submit = $form->get('submit');
//        $form->remove('submit');


        // Add the province element
        $form->add('project', 'entity', $this->options(array(
            'class' => 'AIE\Bundle\CmsBundle\Entity\Projects',
            'property' => 'name',

        )));

        // Cities are empty, unless we actually supplied a province
        $cities = array();
        if ($subproject) {
            // Fetch the cities from specified province
         
            $cities = $this->em->getRepository('AIECmsBundle:SubProjects')
                ->findBy(array('id'=>$subproject));
        }

        // Add the city element
        $form->add('subproject', 'entity', array(
            'empty_value' => '-- Select a sub project--',
            'class' => 'AIECmsBundle:SubProjects',
            'choices' => $cities,
            'attr' => array(
                'class' => 'elementSubProject form-control',
            ),
            'label' => 'Sub Project',
            'label_attr'=> ['class'=> 'control-label col-md-offset-1 col-md-2'],
        ));

        // Add submit button again, this time, it's back at the end of the form
//        $form->add($submit);
    }


    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Note that the data is not yet hydrated into the entity.
        $subproject = $this->em->getRepository('AIECmsBundle:SubProjects')->find($data['subproject']);
        $this->addElements($form, $subproject);
    }


    function onPreSetData(FormEvent $event) {
        $account = $event->getData();
        $form = $event->getForm();

        // We might have an empty account (when we insert a new account, for instance)
        $subproject = $account->getProject() ? $account->getProject()->getSubProjects() : null;
        $this->addElements($form, $subproject);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\UserProjectGroup'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_cmsbundle_userprojectgroup';
    }

}
