<?php
namespace AIE\Bundle\CmsBundle\Form\FormCreators;

use AIE\Bundle\CmsBundle\Form\ArchiveReportType;
use AIE\Bundle\CmsBundle\Form\WorkshopReportType;
use AIE\Bundle\CmsBundle\Helper;

trait ArchiveReportCreator {

    //use Helper\FormStyleHelper;

    private $_blocks = [
        'EXECUTIVE_SUMMARY'            => 'Executive Summary',
        'INTRODUCTION'                 => 'Introduction',
        'ABBREVIATIONS'                => 'Abbreviations',
        'KEY_PERFORMANCE_INDICATORS'   => 'Key Performance Indicators',
        'CORROSION_MANAGEMENT_CONTROL' => 'Corrosion Management and Control',
        'RECOMMENDATIONS'              => 'Recommendations',
        'REFERENCES_LIST'              => 'References',
        'APPENDIX'                     => 'Appendices',
    ];

    /**
     * @param $projectId
     * @param $subprojectId
     * @param $archiveId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createArchiveReportForm($projectId, $subprojectId, $archiveId, $workshops, $reportType)
    {
        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('cms_subproject_archivereports_docx', array('projectId' => $projectId,'subprojectId' => $subprojectId, 'id' => $archiveId)))
            ->setMethod('POST');
            if ($reportType == 1){
                $this->getExceptionBlocks();
            }
            else if ($reportType == 2){
                $this->getSupplementaryBlocks();
            }
            else if ($reportType == 4){
                $this->getMatrixBlocks();
            }
            else if ($reportType == 6){
                $this->getFlowAssuranceBlocks();
            }
            else{
                $this->getBlocks();
            }
            $form->add('archive_report', new ArchiveReportType($this->_blocks), ['label' => ' ']);

            $form->add('workshop_report', new WorkshopReportType($workshops), ['label' => ' ']);

            $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate', 'attr' => ['class' => 'right', 'style' => 'margin-right: 30px;']), 'btn'))
                ->getForm();

            return $form;
    }

    public function getBlocks()
    {
        return  $this->_blocks = [
            'EXECUTIVE_SUMMARY'            => 'Executive Summary',
            'INTRODUCTION'                 => 'Introduction',
            'ABBREVIATIONS'                => 'Abbreviations',
            'KEY_PERFORMANCE_INDICATORS'   => 'Key Performance Indicators',
            'CORROSION_MANAGEMENT_CONTROL' => 'Corrosion Management and Control',
            'RECOMMENDATIONS'              => 'Recommendations',
            'REFERENCES_LIST'              => 'References',
            'APPENDIX'                     => 'Appendices',
        ];
    }

    public function getSupplementaryBlocks()
    {
        return  $this->_blocks = [
            'INTRODUCTION'                 => 'Introduction',
            'LONG_TERM_TRENDING'           => 'Long Term Trending',
            'REFERENCES_LIST'              => 'References',
            'APPENDIX'                     => 'Appendices',
        ];
    }

    public function getExceptionBlocks()
    {
        return  $this->_blocks = [
            'EXECUTIVE_SUMMARY'            => 'Executive Summary',
            'INTRODUCTION'                 => 'Introduction',
            'ABBREVIATIONS'                => 'Abbreviations',
            'KEY_PERFORMANCE_INDICATORS'   => 'Key Performance Indicators',
            'CORROSION_MANAGEMENT_CONTROL' => 'Corrosion Management and Control',
            'RECOMMENDATIONS'              => 'Recommendations',
            'REFERENCES_LIST'              => 'References',
            'ACTION_TRACKER'               => 'Action Tracker',
        ];
    }

    public function getMatrixBlocks()
    {
        return  $this->_blocks = [
            'CORROSION_MATRIX'                 => 'Corrosion Matrix',
            'FLOW_ASSURANCE_MATRIX'           => 'Flow Assurance Matrix',
        ];
    }

    public function getFlowAssuranceBlocks()
    {
        return  $this->_blocks = [
            'EXECUTIVE_SUMMARY'            => 'Executive Summary',
            'INTRODUCTION'                 => 'Introduction',
            'ABBREVIATIONS'                => 'Abbreviations',
            'FLOW_ASSURANCE_KEY_PERFORMANCE_INDICATORS'   => 'Flow Assurance Key Performance Indicators',
            'FLOW_ASSURANCE_MANAGEMENT_CONTROL' => 'Flow Assurance Management and Control',
            'RECOMMENDATIONS'              => 'Recommendations',
            'REFERENCES_LIST'              => 'References',
            'APPENDIX'                     => 'Appendices',
        ];
    }

}