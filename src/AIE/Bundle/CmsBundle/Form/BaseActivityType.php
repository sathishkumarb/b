<?php

namespace AIE\Bundle\CmsBundle\Form;

use Doctrine\ORM\EntityRepository;
use AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;

class BaseActivityType extends AbstractType
{
    use Helper\FormStyleHelper;

    protected $em;

    // These variables are used for storing fields under groups
    protected $managementactivity;
    protected $controlactivity;

    function __construct($em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /* Add additional fields... */
        $builder
            ->add(
                'mitigationtechniquestitle',
                'text',
                $this->options(
                    [
                        'label' => '* Mitigation Techniques',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                        'mapped' =>false,
                    ]
                )
            );
        $builder
            ->add(
                'managementtitle',
                'text',
                $this->options(
                    [
                        'label' => '* Management Activity',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                    ]
                )
            )
            ->add(
                'managementgreen',
                'text',
                $this->options(
                    [
                        'label' => '* Management Activity Green',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                    ]
                )
            )
            ->add(
                'managementamber',
                'text',
                $this->options(
                    [
                        'label' => 'Management Activity Amber',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-3','style' => 'width: 200px'],
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                    ]
                )
            )
            ->add(
                'managementred',
                'text',
                $this->options(
                    [
                        'label' => '* Management Activity Red',
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'required' => true,
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                    ]
                )
            );
        $builder
            ->add(
                'controltitle',
                'text',
                $this->options(
                    [
                        'label' => '* Control Activity',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                        'data' => 'N/A',
                        'label_attr' => ['class' => 'col-md-5 control-label'],
                    ]
                )
            )
            ->add(
                'controlgreenexpression',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width: 100px;','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlgreen',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => true,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'data' => 'N/A',
                        'constraints' => [new NotBlank(),],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'controlgreenexpressionrange',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width: 100px','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlgreenrange',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'controlamberexpression',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width: 100px','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlamber',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'data' => 'N/A',
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'controlamberexpressionrange',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width:100px','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlamberrange',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'label_attr' => [''],
                    ]
                )
            )
            ->add(
                'controlredexpression',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width: 100px','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlred',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'required' => true,
                        'constraints' => [new NotBlank(),],
                        'data' => 'N/A',
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'controlredexpressionrange',
                'choice', array(
                    'label' => false,
                    'empty_value' => 'Symbol',
                    'choices'   => array('<=' => '<=', '>=' => '>=', "=" => "=", "<" => "<", ">" => ">"),
                    'required'  => false,
                    'attr' => ['container_class' => '','style' => 'width: 100px','class'=> 'form-control'],
                    'label_attr'=> ['class'=> ''],
                )
            )
            ->add(
                'controlredrange',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => '','class' => 'required','style' => 'width: 80px'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            );
        $builder
            ->add(
                'frequency',
                'text',
                $this->options(
                    [
                        'label' => '* Frequency, Month',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                    ]
                )
            )
            ->add(
                'activityperson',
                'text',
                $this->options(
                    [
                        'label' => '* Activity Person',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'constraints' => [new NotBlank()],
                    ]
                )
            )
            ->add(
                'weightage',
                'number',
                $this->options(
                    [
                        'label' => '* % Weightage',
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'required' => true,
                        'constraints' => [new NotBlank()],
                    ]
                )
            )
            ->add(
                'consequencesexcursion',
                'text',
                $this->options(
                    [
                        'label' => 'Consequences Excursion',
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'required' => false,
                    ]
                )
            )
            ->add(
                'remedialaction',
                'text',
                $this->options(
                    [
                        'label' => 'Remedial Action',
                        'attr' => ['container_class' => 'col-md-3','class' => 'required','style' => 'width: 200px'],
                        'required' => false,
                    ]
                )
            );

        //$builder->add('Save', 'submit',$this->options(['label' => 'Save', 'attr' => ['class' => 'btn btn-primary btn-sm', 'style' => 'float: left']], 'btn'));

        // Add listeners
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));


    }

    protected function addElements(FormInterface $form, BaseMetricsConfig $basemetrics = null) {
        // Remove the submit button, we will place this at the end of the form later

        $form->add('basemetrics', 'entity', array(
                'data' => $basemetrics,
                'empty_value' => '-- Select a Metric --',
                'class' => 'AIECmsBundle:BaseMetricsConfig',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.title', 'ASC');
                },
                'attr' => array(
                    'class' => 'elementMetrics form-control','style' => 'width: 200px',
                ),
                'label' => '* Metric',
                'label_attr'=> ['class'=> 'control-label col-md-offset-1 col-md-2'],
                'mapped' => false)
        );
        $datasheets = array();

        if ($basemetrics) {

            $repo = $this->em->getRepository('AIECmsBundle:BaseDatasheetType');


            $qb = $this->em->createQueryBuilder();

            $fields = array('bs.id','bs.title');

            $query = $qb->select('bs')
                ->from('AIECmsBundle:BaseMetricsConfig', 'bm')
                ->innerJoin('AIECmsBundle:BaseDatabaseType', 'bd', 'WITH', 'bd.id = bm.databasetype')
                ->innerJoin('AIECmsBundle:BaseDatasheetType', 'bs', 'WITH', 'bs.id = bm.datasheettype');

            $qb->andWhere('bm.id = :uid')
                ->setParameter('uid', $basemetrics)
                ->orderBy('bm.title', 'ASC');

            $datasheets = $query->getQuery()->getResult();

        }

        $form->add('basedatasheet', 'entity', array(
            'empty_value' => '-- Select a DataSheet --',
            'class' => 'AIECmsBundle:BaseDatasheetType',
            'choices' => $datasheets,
            'attr' => array(
                'class' => 'elementDatasheet form-control','style' => 'width: 200px',
            ),
            'label' => '* Datasheet',
            'label_attr'=> ['class'=> 'control-label col-md-offset-1 col-md-2'],
        ));

        // Add submit button again, this time, it's back at the end of the form
        //$form->add($submit);
        //$form->add('submit', 'submit', $this->options(['label' => 'Save', 'attr' => ['class' => 'btn btn-primary btn-sm activitysave',]], 'btn'));
        //$form->add('cancel', 'button', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'btn btn-primary btn-sm subcancelbutton',]], 'btn'));

    }

    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();

        // Note that the data is not yet hydrated into the entity.
        $metrics = $this->em->getRepository('AIECmsBundle:BaseMetricsConfig')->find($data['basemetrics']);
        $this->addElements($form, $metrics);
    }

    function onPreSetData(FormEvent $event) {
        $account = $event->getData();
        $form = $event->getForm();

        $metrics = $account->getBasedatasheet() ? $account->getBasedatasheet()->getBaseMetrics() : null;
        $this->addElements($form, $metrics);
    }



    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\BaseActivity'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_baseactivtiy';
    }
}
