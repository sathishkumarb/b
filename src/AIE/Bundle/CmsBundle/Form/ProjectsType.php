<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ProjectsType extends AbstractType
{
    use Helper\FormStyleHelper;
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referenceNo', 'text', $this->options(array('label' => 'Reference Number')))
            ->add('client', 'text', $this->options(array('label' => 'Company Name')))
            ->add('location', 'text', $this->options(array('label' => 'Location')))
            ->add('name', 'text', $this->options(array('label' => 'Project Name')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description'), 'textarea'))
            ->add('file', 'file', $this->options(array('label' => 'Logo'), 'file'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\Projects',
            'attr' => array(
                'class' => 'form-horizontal-from-default'
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_projects';
    }
}
