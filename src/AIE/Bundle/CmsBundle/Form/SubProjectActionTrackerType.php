<?php

namespace AIE\Bundle\CmsBundle\Form;
use AIE\Bundle\CmsBundle\Entity\ActionTracker;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;

class SubProjectActionTrackerType extends AbstractType
{
    use Helper\FormStyleHelper;
    private $entity;

    function __construct(ActionTracker $entity)
    {
        $this->entity = $entity;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = ($this->entity->getDate()) ? $this->entity->getDate() : new \DateTime();
        if ($date)
        {
            $date = $date->format('d F Y');
        }
        $builder
           -> add(
                'Title',
                'text',
                $this->options(
                    [
                        'label' => '* Title',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 77%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => 'col-md-2 control-label'],
                    ]
                )
            )
         /*   ->add(
                'Date',
                DateType::class,
                $this->options(
                    [
                        'label' => '* Date',
                        'required' => true,
                        'attr' => ['style' => 'height: 50px'],
                        'format' => 'dd  M yyyy',
                    ]
                ));*/


       ->add('Date', new DatePickerType(), $this->options(array('mapped' => true, 'label' => '*Date',  'data' => $date,'attr' => ['class' => 'col-md-9'],'required' => true, 'label_attr' => ['class' => 'col-md-2 control-label']), ''));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\ActionTracker'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectactiontracker';
    }
}
