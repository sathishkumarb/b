<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectSamplingDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;


class SubProjectCathodicProtectionDatabaseType extends AbstractType
{
    use Helper\FormStyleHelper;

    protected $em;
    protected $entity;
    protected $subprojectId;
    protected $servicedt;
    protected $orientation;
    protected $activityperson;
    protected $sId;
    protected $qb;

    function __construct($entity, $em, $subprojectId, $sId)
    {
        $this->em = $em;
        $this->entity = $entity;
        $this->subprojectId = $subprojectId;
        $this->sId = $sId;

        $this->servicedt = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,1,$this->em);
        $this->cptype = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,24,$this->em);
        $this->equipmenttype = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,25,$this->em);
        $this->anodetype = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,26,$this->em);
        $this->referenceelectrode = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,28,$this->em);
        $this->monitoringmethod = CmsDatabasesHelper::getDatabasePropertiesPerType($this->subprojectId,27,$this->em);
        $this->qb = $this->em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivityForSystem($this->subprojectId, $this->sId, 1);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'activityid', "choice",
                $this->options(
                    [
                        "label" => "* Activity",
                        'required' => true,
                        "choices" => $this->qb,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'platformorplant',
                'text',
                $this->options(
                    [
                        'label' => '* Platform/Plant',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'pandid',
                'text',
                $this->options(
                    [
                        'label' => 'P&ID',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'location',
                'text',
                $this->options(
                    [
                        'label' => '* Location',
                        'required' => true,
                        'constraints' => [new NotBlank()],
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'cptypeid', 'choice',
                $this->options(
                    [
                        'label' => 'CP Type',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        "choices" => $this->cptype,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'equipmenttypeid', 'choice',
                $this->options(
                    [
                        'label' => 'Equipment Type',
                        'required' => false,
                        "choices" => $this->equipmenttype,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'tag',
                "text",
                $this->options(
                    [
                        "label" => "Tag",
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4','style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'serviceid', "choice",
                $this->options(
                    [
                        "label" => "Service",
                        'required' => false,
                        "choices" => $this->servicedt,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'anodetypeid', "choice",
                $this->options(
                    [
                        "label" => "Anode Type",
                        'required' => false,
                        "choices" => $this->anodetype,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'referenceelectrodeid', "choice",
                $this->options(
                    [
                        "label" => "Reference Electrode",
                        'required' => false,
                        "choices" => $this->referenceelectrode,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'monitoringmethodid', 'choice',
                $this->options(
                    [
                        'label' => 'Monitoring Method',
                        'required' => false,
                        "choices" => $this->monitoringmethod,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add('file', 'file', $this->options(array('label' => 'Upload','required' => false), 'file'));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectCathodicProtectionDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectcathodicprotectiondatabase';
    }
}
