<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class SubProjectMitigationTechniquesType extends AbstractType
{
    use Helper\FormStyleHelper;


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mitigationtechniquestitle', 'text', $this->options(['label' => '']));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectMitigationTechniques',
            'attr' => array(
                'class' => 'form-horizontal-from-default'
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subprojectmitigationtechniques';
    }
}
