<?php

namespace AIE\Bundle\CmsBundle\Form;
use AIE\Bundle\CmsBundle\Entity\ActionTracker;
use AIE\Bundle\CmsBundle\Entity\ActionTrackerItems;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;

class SubProjectATItemType extends AbstractType
{
    use Helper\FormStyleHelper;
    private $entity;
    function __construct(ActionTrackerItems $entity)
    {
        $this->entity = $entity;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $this->entity->getChoices();
        $date = ($this->entity->getDueDate()) ? $this->entity->getDueDate() : new \DateTime();
        if ($date)
        {
            $date = $date->format('d F Y');
        }
        $builder
           -> add(
                'Item',
                'text',
                $this->options(
                    [
                        'label' => '* Item',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            -> add(
                'Details',
                'text',
                $this->options(
                    [
                        'label' => '* Detail',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width:  71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            -> add(
                'ResponsibleGroup',
                'text',
                $this->options(
                    [
                        'label' => '* Responsible Group',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width:  71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            -> add(
                'Persons',
                'text',
                $this->options(
                    [
                        'label' => '* Responsible Person(s)',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
       ->add('Duedate', new DatePickerType(), $this->options(array('mapped' => true, 'label' => '*Date',  'data' => $date, 'attr' => ['class' => 'col-md-9'], 'required' => true, 'label_attr' => ['class' => 'col-md-3 control-label'],), ''))

            ->add(
                'Status',
                "choice",
                $this->options(
                    [
                        "label" => "* Status",
                        'required' => true,
                        "choices" =>  $choices,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )

       /*     ->add('Status', ChoiceType::class, [
                'choices' =>  $this->status
                ,
                'choice_label' => function($status, $key, $index) {
                    /** @var Category $category */
                    //return $status;
                //}])
            -> add(
        'Comments',
        'text',
        $this->options(
            [
                'label' => 'Comments',
                'required' => false,
                'attr' => ['container_class' => 'col-md-4','class' => 'required','style' => 'width: 71%'],
                'label_attr' => ['class' => ''],
            ]
        ));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\ActionTrackerItems'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subproject_actiontrackeritems';
    }
}
