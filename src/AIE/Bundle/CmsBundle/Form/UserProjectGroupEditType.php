<?php

namespace AIE\Bundle\CmsBundle\Form;


use AIE\Bundle\CmsBundle\Entity\UserProjectGroup;
use AIE\Bundle\CmsBundle\Entity\SubProjects;
use AIE\Bundle\CmsBundle\Repository;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProjectGroupEditType extends AbstractType {

    use Helper\FormStyleHelper;

    function __construct($entity, $em)
    {
        $this->entity = $entity;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            /*
              ->add('user', 'entity', array(
              'class' => 'UserBundle:User',
              'property' => 'username',
              )) */

            ->add('subproject', 'entity', $this->options(array(
                'class' => 'AIE\Bundle\CmsBundle\Entity\SubProjects',
                'data'     => $this->entity->getSubProjectlists(),
                'property' => 'name',
            )))

            ->add('group', 'entity', $this->options(array(
                'data'     => $this->entity->getGroupRoles(),
                'class' => 'UserBundle:CcmGroup',
                'property' => 'name',
                'label' => 'Position'
            )));
        // Add listeners

    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\UserProjectGroup'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_cmsbundle_userprojectgroup_edit';
    }

}
