<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageAddColumnsDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;


class SubProjectChemicalDosageDatabaseEditColumnsType extends AbstractType
{
    use Helper\FormStyleHelper;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'columnone',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 200px', 'placeholder'=> 'Column One'],
                        'label_attr' => ['class' => 'col-md-3 control-label'],
                    ]
                )
            )
            ->add('columnoneselected', CheckboxType::class, array(
                 'label' => false,
                 'attr' => ['container_class' => 'col-md-4',],
                 'required' => false,
                 'label_attr' => ['class' => 'col-md-3 control-label'],
             ))
            ->add(
                'columntwo',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 200px', 'placeholder'=> 'Column Two'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add('columntwoselected', CheckboxType::class, array(
                'label' => false,
                'attr' => ['container_class' => 'col-md-4',],
                'required' => false,
                'label_attr' => ['class' => 'col-md-3 control-label'],
            ))
            ->add(
                'columnthree',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 200px', 'placeholder'=> 'Column Three'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add('columnthreeselected', CheckboxType::class, array(
                'label' => false,
                'attr' => ['container_class' => 'col-md-4',],
                'required' => false,
                'label_attr' => ['class' => 'col-md-3 control-label'],
            ))
            ->add(
                'columnfour',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 200px', 'placeholder'=> 'Column Four'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add('columnfourselected', CheckboxType::class, array(
                'label' => false,
                'attr' => ['container_class' => 'col-md-4',],
                'required' => false,
                'label_attr' => ['class' => 'col-md-3 control-label'],
            ))
            ->add(
                'columnfive',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 200px', 'placeholder'=> 'Column Five'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add('columnfiveselected', CheckboxType::class, array(
                'label' => false,
                'attr' => ['container_class' => 'col-md-4',],
                'required' => false,
                'label_attr' => ['class' => 'col-md-3 control-label'],
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectChemicalDosageAddColumnsDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subproject_chemicaldosage_addcolumns_database';
    }
}
