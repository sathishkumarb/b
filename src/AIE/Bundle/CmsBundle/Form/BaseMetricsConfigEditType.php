<?php

namespace AIE\Bundle\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig;

class BaseMetricsConfigEditType extends AbstractType
{
    use Helper\FormStyleHelper;

    private $entity;
    function __construct(BaseMetricsConfig $entity)
    {
        $this->entity = $entity;

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $this->entity->getChoices();
        $builder
            ->add('Title', 'text', $this->options(array('label' => 'Title','attr' => ['container_class' => 'col-md-1','class' => 'required'],
                'constraints' => [new NotBlank()],
                'label_attr' => ['class' => 'col-md-2 control-label'],)))
            ->add('databasetype', 'entity', array(
                'label'=>'Database Type',
                'class'    => 'AIECmsBundle:BaseDatabaseType',
                'property' => 'title',
            ))
            ->add('datasheettype', 'entity', array(
                'label'=>'Datasheet Type',
                'class'    => 'AIECmsBundle:BaseDatasheetType',
                'property' => 'title',
            ))
            ->add(
                'corrosioninhibitortype',
                "choice",
                $this->options(
                    [
                        "label" => "Corrosion Inhibitor Type",
                        'required' => false,
                        "choices" =>  $choices,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 100%'),
                        "empty_value" => '--Select--',
                        "label_attr" => ['class' => 'col-sm-2 col-md-2'],
                    ]
                )
            );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_basemetrics_edit';
    }
}
