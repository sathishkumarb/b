<?php

namespace AIE\Bundle\CmsBundle\Form;

use AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;

use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;

use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AIE\Bundle\CmsBundle\Helper\CmsDatabasesHelper;


class SubProjectCorrosionMonitoringDatabaseEditType extends AbstractType
{
    use Helper\FormStyleHelper;

    protected $em;
    protected $projectId;
    protected $subprojectId;
    protected $servicedt;
    protected $installeddevice;
    protected $corrosionorientation;
    protected $accessfittingtype;
    protected $holderplug;
    protected $teefitting;
    protected $insulation;
    protected $surfacefinish;
    protected $online;
    protected $scaffold;
    protected $access;
    protected $probetechnologyanddatalogger;
    protected $couponmaterialanddensity;
    protected $coupontypeandarea;
    protected $sId;

    function __construct(SubProjectCorrosionMonitoringDatabase $entity, $em, $projectId, $subprojectId, $sId)
    {
        $this->entity = $entity;
        $this->em = $em;
        $this->projectId = $projectId;
        $this->subprojectId = $subprojectId;
        $this->sId = $sId;

        $this->servicedt = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,1,$this->em);
        $this->installeddevice = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,4,$this->em);
        $this->corrosionorientation = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,5,$this->em);
        $this->accessfittingtype = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,6,$this->em);
        $this->holderplug = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,8,$this->em);
        $this->teefitting = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,7,$this->em);
        $this->insulation = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,15,$this->em);
        $this->surfacefinish = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,16,$this->em);
        $this->online = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,17,$this->em);
        $this->scaffold = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,18,$this->em);
        $this->access = CmsDatabasesHelper::getDatabasePropertiesPerType($subprojectId,19,$this->em);

        $this->probetechnologyanddatalogger = CmsDatabasesHelper::getDatabaseLinkedPropertiesPerType($subprojectId,9,10,$this->em);
        $this->couponmaterialanddensity = CmsDatabasesHelper::getDatabaseLinkedPropertiesPerType($subprojectId,11,12,$this->em);
        $this->coupontypeandarea = CmsDatabasesHelper::getDatabaseLinkedPropertiesPerType($subprojectId,13,14,$this->em);
        $this->qb = $this->em->getRepository('AIECmsBundle:SubProjectActivity')->getSubProjectSelectedActivityForSystem($this->subprojectId, $this->sId, 3);

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add(
                'activityid', "choice",
                $this->options(
                    [
                        "label" => "* Activity",
                        'required' => true,
                        "choices" => $this->qb,
                        "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                        "empty_value" => '--Select--',
                    ]
                )
            )
            ->add(
                'platformorplant',
                'text',
                $this->options(
                    [
                        'label' => '* Platform/Plant',
                        'required' => true,
                        'attr' => ['container_class' => 'col-md-4', 'class' => 'required', 'style' => 'width: 71%'],
                        'constraints' => [new NotBlank()],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'pandid',
                'text',
                $this->options(
                    [
                        'label' => 'P&ID',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'location',
                'text',
                $this->options(
                    [
                        'label' => '* Location',
                        'required' => true,
                        'constraints' => [new NotBlank()],
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'linenumber',
                'text',
                $this->options(
                    [
                        'label' => 'Line Number',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'linediameterinches',
                'text',
                $this->options(
                    [
                        'label' => 'Line Diameter, Inches',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'standoffinches',
                'text',
                $this->options(
                    [
                        'label' => 'Standoff Inches',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'linematerial',
                'text',
                $this->options(
                    [
                        'label' => 'Line Material',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'serviceid', "choice",
                $this->options(
                    [
                        "label" => "Service",
                        'required' => false,
                        "choices" => $this->servicedt,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'operatingpressure',
                'text',
                $this->options(
                    [
                        'label' => 'Operating Pressure, barg',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'operatingtemperature',
                'text',
                $this->options(
                    [
                        'label' => 'Operating Temperature, C',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'devicetypeid',
                "choice",
                $this->options(
                    [
                        "label" => "Device Type",
                        'required' => false,
                        "choices" => $this->installeddevice,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'orientationid',
                "choice",
                $this->options(
                    [
                        "label" => "Orientation",
                        'required' => false,
                        "choices" => $this->corrosionorientation,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'accessfittingtypeid',
                "choice",
                $this->options(
                    [
                        "label" => "Access Fitting Type",
                        'required' => false,
                        "choices" => $this->accessfittingtype,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'installedcap',
                'text',
                $this->options(
                    [
                        'label' => 'Installed Cap',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'installedplug',
                'text',
                $this->options(
                    [
                        'label' => 'Installed Plug',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'holderplugid',
                "choice",
                $this->options(
                    [
                        "label" => "Holder Plug",
                        'required' => false,
                        "choices" => $this->holderplug,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'teefittingid',
                "choice",
                $this->options(
                    [
                        "label" => "Tee Fitting",
                        'required' => false,
                        "choices" => $this->teefitting,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'instrumentation',
                'text',
                $this->options(
                    [
                        'label' => 'Instrumentation',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'probeextensionadapter',
                'text',
                $this->options(
                    [
                        'label' => 'Probe Extension Adapter',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'probetechnologyanddataloggerid',
                "choice",
                $this->options(
                    [
                        "label" => "Probe Technology and Data Logger",
                        'required' => false,
                        "choices" => $this->probetechnologyanddatalogger,
                        "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                        "empty_value" => '--Select--'
                    ]
                )
            )
            ->add(
                'probetypeandmodel',
                'text',
                $this->options(
                    [
                        'label' => 'Probe Type/Model',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'logginginterval',
                'text',
                $this->options(
                    [
                        'label' => 'Logging Interval,s',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                $builder->create('probeinstallationdate',
                    new DatePickerType(),
                    $this->options(
                        [
                            'label' => 'Probe Installation Date',
                            'required' => false,
                            'attr' => ['form_class'=>'col-md-12','container_class'=>'col-md-3','style'=>''],
                            'label_attr' => ['class' => 'col-md-3'],
                        ],
                        'date_picker'
                    )
                )->addModelTransformer(new DateTimePickerTransformer()))
            ->add(
                'couponholdertagno',
                'text',
                $this->options(
                    [
                        'label' => 'Coupon Holder Tag Number',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'couponmaterialanddensityid',"choice",
                [
                    "label" => "Coupon Material and Density, g/cm3",
                    'required' => false,
                    "choices" => $this->couponmaterialanddensity,
                    "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'coupontypeandareaid',"choice",
                [
                    "label" => "Coupon Type and Exposed Area, cm2",
                    'required' => false,
                    "choices" => $this->coupontypeandarea,
                    "attr" => array("class" => "form-control select3", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'insulationid', "choice",
                [
                    "label" => "Insulation",
                    'required' => false,
                    "choices" => $this->insulation,
                    "attr" => array("class" => "form-control select3", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'surfacefinishid',"choice",
                [
                    "label" => "Surface Finish",
                    'required' => false,
                    "choices" => $this->surfacefinish,
                    "attr" => array("class" => "form-control select3", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'onlineid',"choice",
                [
                    "label" => "Online",
                    'required' => false,
                    "choices" => $this->online,
                    "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'tool',
                'text',
                $this->options(
                    [
                        'label' => 'Tool',
                        'required' => false,
                        'attr' => ['container_class' => 'col-md-4', 'style' => 'width: 71%'],
                        'label_attr' => ['class' => ''],
                    ]
                )
            )
            ->add(
                'scaffoldid',"choice",
                [
                    "label" => "Scaffold",
                    'required' => false,
                    "choices" => $this->scaffold,
                    "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add(
                'accessid',"choice",
                [
                    "label" => "Access",
                    'required' => false,
                    "choices" => $this->access,
                    "attr" => array("class" => "form-control select2", 'style' => 'width: 71%'),
                    "empty_value" => '--Select--',
                    'label_attr' => ['class' => 'col-md-3'],
                ]
            )
            ->add('file', 'file', $this->options(array('label' => 'Upload','required' => false), 'file'));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\CmsBundle\Entity\SubProjectCorrosionMonitoringDatabase'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_cmsbundle_subproject_corrosionprobe_database';
    }
}
