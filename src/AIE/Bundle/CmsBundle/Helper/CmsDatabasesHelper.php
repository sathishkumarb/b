<?php
/**
 * User: Asset Fze LLC
 * Date: 05/03/17
 */

namespace AIE\Bundle\CmsBundle\Helper;

use AIE\Bundle\CmsBundle\Entity\BaseMetricsConfig;
use Doctrine\ORM\Query\ResultSetMapping;

class CmsDatabasesHelper
{

    private $doctrine;
    private $em;
    private $results;
    private $kernel;

    public function __construct($doctrine,$kernel)
    {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager('cms');
        $this->kernel = $kernel;
    }

    /*
    * Method Description: to find systems for a sub project if matrix set up configured
    */
    public function getSubProjectSystems($subprojectId,$selected=0)
    {
        // find action owners for that project
        $this->results = $this->em->getRepository('AIECmsBundle:SubProjectSystems')->findBy(array('subproject'=>$subprojectId,'isselected'=>$selected));

        //get User for these actionOwners
        return $this->results;

    }

    /*
    * Method Description: to find all systems,threats.mitigation,activity for a sub project if matrix set up configured and for given database(sampling,cathodic etc)
    */
    public function getSubProjectAllSystemsDatabaseItems($subprojectId, $dbtype)
    {
        $statement = 'SELECT bs
                FROM AIECmsBundle:SubProjectSystems bs 
                join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems 
                join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats 
                join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques 
                join AIECmsBundle:BaseDatasheetType bsa WITH bsa.id = ba.subprojectdatasheet 
                join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.subprojectmetrics';

        $statement.=' AND bs.subproject=:subprojectid 
                AND bt.subproject=:subprojectid 
                AND bmt.subproject=:subprojectid 
                AND ba.subproject=:subprojectid 
                AND bmc.databasetype = :dbtype 
                AND bs.isselected=1 AND bt.isselected=1 AND bmt.isselected=1 AND (ba.ismanagement=1 or ba.iscontrol=1 or ba.isnonmonitoringkpi=1)';

        $statement.= ' GROUP BY bs.id ORDER BY bs.id ASC';

        $query = $this->em->createQuery($statement)
            ->setParameter('subprojectid', $subprojectId)
            ->setParameter('dbtype', $dbtype);

        $this->results = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $this->results;

    }

    /*
    * Method Description: to find all metrics of databases for a sub project if matrix set up configured and for given database(sampling,cathodic etc)
    */
    public function getSubProjectAllSystemsDatabasesMetrics($subprojectId)
    {
        $statement = 'SELECT bs,ba
                FROM AIECmsBundle:SubProjectSystems bs
                join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems
                join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats
                join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques';

        $statement.=' AND bs.subproject=:subprojectid
                AND bt.subproject=:subprojectid
                AND bmt.subproject=:subprojectid
                AND ba.subproject=:subprojectid
                AND bs.isselected=1 AND bt.isselected=1 AND bmt.isselected=1 AND (ba.ismanagement=1 or ba.iscontrol=1 or ba.isnonmonitoringkpi=1)';

        $statement.= ' GROUP BY bs.id ORDER BY bs.id ASC';

        $query = $this->em->createQuery($statement)
            ->setParameter('subprojectid', $subprojectId);

        $this->results = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $this->results;

    }

    /*
    * Method Description: to find all systems,threats.mitigation,activity for a sub project and databse if matrix set up configured and for given database(sampling,cathodic etc)
    */
    public function getSubProjectAllSystemsDatabaseItemsPerDb($subprojectId, $dbtype)
    {
        $statement = 'SELECT bs, bsd
                FROM AIECmsBundle:SubProjectSystems bs 
                join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems 
                join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats 
                join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques 
                join AIECmsBundle:BaseDatasheetType bsa WITH bsa.id = ba.subprojectdatasheet 
                join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.subprojectmetrics';

        switch($dbtype) {
            case 1:
                $statement.= ' join AIECmsBundle:SubProjectCathodicProtectionDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 2:
                $statement.= ' join AIECmsBundle:SubProjectChemicalDosageDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 3:
                $statement.= ' join AIECmsBundle:SubProjectCorrosionMonitoringDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 4:
                $statement.= ' join AIECmsBundle:SubProjectSamplingDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 6:
                $statement.= ' join AIECmsBundle:SubProjectProductionDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            default:
                break;
        }

        $statement.=' AND bs.subproject=:subprojectid 
                AND bt.subproject=:subprojectid 
                AND bmt.subproject=:subprojectid 
                AND ba.subproject=:subprojectid 
                AND bmc.databasetype = :dbtype 
                AND bs.isselected=1 AND bt.isselected=1 AND bmt.isselected=1 AND (ba.ismanagement=1 or ba.iscontrol=1 or ba.isnonmonitoringkpi=1)';

        $statement.= ' ORDER BY bs.systemstitle ASC';

        $query = $this->em->createQuery($statement)
            ->setParameter('subprojectid', $subprojectId)
            ->setParameter('dbtype', $dbtype);

        $this->results = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $this->results = $this->getDropdownActivitiesDatabases($this->results);

        return $this->getDropdownItemsOfDatabase($this->results,$dbtype);

    }

    /*
    * Method Description: to find database items for a sub project of a system if matrix set up configured
    */
    public function getSubProjectSelectedSystemsDatabaseItems($subprojectId, $dbtype, $systemid = null)
    {

        $statement = 'SELECT bs, bsd
                FROM AIECmsBundle:SubProjectSystems bs
                ';

        switch($dbtype){
            case 1:
                $statement.= ' join AIECmsBundle:SubProjectCathodicProtectionDatabase bsd WITH bs.id = bsd.subprojectsystems';

                break;
            case 2:
                $statement.= ' join AIECmsBundle:SubProjectChemicalDosageDatabase bsd WITH bs.id = bsd.subprojectsystems';

                break;
            case 3:
                $statement.= ' join AIECmsBundle:SubProjectCorrosionMonitoringDatabase bsd WITH bs.id = bsd.subprojectsystems';

                break;
            case 4:
                $statement.= ' join AIECmsBundle:SubProjectSamplingDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 6:
                $statement.= ' join AIECmsBundle:SubProjectProductionDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            default:
                break;
        }

        $statement.=' AND bs.subproject=:subprojectid 
                AND bs.isselected=1';

        if ($systemid) {
            $statement.= ' AND bs.id = :systemid';
        }

        $statement.= ' ORDER BY bs.systemstitle, bsd.location ASC';

        $query = $this->em->createQuery($statement)
            ->setParameter('subprojectid', $subprojectId);

        if ($systemid) {
            $query->setParameter('systemid', $systemid);
        }

        $this->results = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $this->results = $this->getDropdownActivitiesDatabases($this->results);

        return $this->getDropdownItemsOfDatabase($this->results,$dbtype);

    }

    /*
    * Method Description: to find database items for a sub project of a system if matrix set up configured
    */
    public function getSubProjectSelectedSystemsDatabaseItemsGroup($subprojectId, $dbtype)
    {

        $statement = 'SELECT bs, bsd, TRIM(bsd.location) as location, count(bs.id) as points from AIECmsBundle:SubProjectSystems bs';

        switch($dbtype){
            case 1:
                $statement.= ' join AIECmsBundle:SubProjectCathodicProtectionDatabase bsd WITH bs.id= bsd.subprojectsystems';
               break;
            case 2:
                $statement.= ' join AIECmsBundle:SubProjectChemicalDosageDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 3:
                $statement.= ' join AIECmsBundle:SubProjectCorrosionMonitoringDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 4:
                $statement.= ' join AIECmsBundle:SubProjectSamplingDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            case 6:
                $statement.= ' join AIECmsBundle:SubProjectProductionDatabase bsd WITH bs.id = bsd.subprojectsystems';
                break;
            default:
                break;
        }

        $statement.=' WHERE bs.subproject=:subprojectid';

        $statement.= ' GROUP BY bs.id, location';

        $query = $this->em->createQuery($statement)
            ->setParameter('subprojectid', $subprojectId);

        $this->results = $query->getScalarResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $this->results;

    }

    /*
    * Method Description: to find sub project database properties for a given database property id
    */
    public static function getDatabasePropertiesPerType($subprojectid,$colid,$em)
    {
        $result = array();

        $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->findBy(array('subproject'=>$subprojectid,'colid'=>$colid));
        foreach ($subprojectsystems as $system) {
            $result[$system->getId()] = $system->getColValue();
        }

        return $result;
    }

    /*
    * Method Description: to find sub project database linked properties for a given database property id's
    */
    public static function getDatabaseLinkedPropertiesPerType($subprojectid,$colidone,$colidtwo,$em)
    {
        $result = array();
        $subprojectsystems = $em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->findBy(array('subproject'=>$subprojectid,'colidone'=>$colidone,'colidtwo'=>$colidtwo));
        foreach ($subprojectsystems as $system) {
            $result[$system->getId()] = $system->getColIdOneValue()." and ".$system->getColIdTwoValue();
        }
        return $result;
    }

    /*
   * Method Description: to find sub project database properties for a given database type
   */
    public function getChemicalDosageColumns($subprojectid)
    {
        $subprojectcmdbcols = $this->em->getRepository('AIECmsBundle:SubProjectChemicalDosageAddColumnsDatabase')->findOneBy(array('subproject'=>$subprojectid));
        return $subprojectcmdbcols;
    }

    /*
   * Method Description: to find sub project database properties for a given database type
   */
    public function getProductionColumns($subprojectid)
    {
        $subprojectcmdbcols = $this->em->getRepository('AIECmsBundle:SubProjectProductionAddColumnsDatabase')->findOneBy(array('subproject'=>$subprojectid));
        return $subprojectcmdbcols;
    }

    /*
   * Method Description: to find drop down list of database items
   */
    public function getDropdownItemsOfDatabase($databases,$type=0)
    {
        switch($type)
        {
            case 1:
                foreach ($databases as $idex => $database) {
                    if (!empty($database['bsd_serviceid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_serviceid']);
                        $databases[$idex]['bss_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_cptypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_cptypeid']);
                        $databases[$idex]['bsc_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_equipmenttypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_equipmenttypeid']);
                        $databases[$idex]['bse_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_anodetypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_anodetypeid']);
                        $databases[$idex]['bsa_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_referenceelectrodeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_referenceelectrodeid']);
                        $databases[$idex]['bsr_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_monitoringmethodid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_monitoringmethodid']);
                        $databases[$idex]['bsm_colvalue'] = $details->getColValue();
                    }

                }
                break;

            case 2:
                foreach ($databases as $idex => $database) {
                    if (!empty($database['bsd_serviceid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_serviceid']);
                        $databases[$idex]['bss_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_pumptypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_pumptypeid']);
                        $databases[$idex]['bsp_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_chemicaltypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_chemicaltypeid']);
                        $databases[$idex]['bsc_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_injectionmethodid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_injectionmethodid']);
                        $databases[$idex]['bsi_colvalue'] = $details->getColValue();
                    }

                }
                break;

            case 3:
                foreach ($databases as $idex => $database) {
                    if (!empty($database['bsd_serviceid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_serviceid']);
                        $databases[$idex]['bss_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_devicetypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_devicetypeid']);
                        $databases[$idex]['bsi_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_orientationid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_orientationid']);
                        $databases[$idex]['bso_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_accessfittingtypeid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_accessfittingtypeid']);
                        $databases[$idex]['bsy_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_holderplugid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_holderplugid']);
                        $databases[$idex]['bsh_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_teefittingid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_teefittingid']);
                        $databases[$idex]['bst_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_insulationid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_insulationid']);
                        $databases[$idex]['bsu_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_surfacefinishid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_surfacefinishid']);
                        $databases[$idex]['bsr_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_onlineid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_onlineid']);
                        $databases[$idex]['bsl_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_scaffoldid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_scaffoldid']);
                        $databases[$idex]['bsf_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_accessid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_accessid']);
                        $databases[$idex]['bsa_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_probetechnologyanddataloggerid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($database['bsd_probetechnologyanddataloggerid']);
                        $databases[$idex]['bspd_colvalue'] = $details->getColIdOneValue()." and ".$details->getColIdTwoValue();
                    }

                    if (!empty($database['bsd_couponmaterialanddensityid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($database['bsd_couponmaterialanddensityid']);
                        $databases[$idex]['bscd_colvalue'] = $details->getColIdOneValue()." and ".$details->getColIdTwoValue();
                    }

                    if (!empty($database['bsd_coupontypeandareaid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseLinkedProperties')->find($database['bsd_coupontypeandareaid']);
                        $databases[$idex]['bsca_colvalue'] = $details->getColIdOneValue()." and ".$details->getColIdTwoValue();
                    }


                }
                break;

            case 4:
                foreach ($databases as $idex => $database) {
                    if (!empty($database['bsd_serviceid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_serviceid']);
                        $databases[$idex]['bss_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_orientationid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_orientationid']);
                        $databases[$idex]['bso_colvalue'] = $details->getColValue();
                    }

                    if (!empty($database['bsd_activitypersonid'])) {
                        $details = $this->em->getRepository('AIECmsBundle:SubProjectDatabaseProperties')->find($database['bsd_activitypersonid']);
                        $databases[$idex]['bsa_colvalue'] = $details->getColValue();
                    }

                }
                break;
        }
        return $databases;
    }

    /*
    * Method Description: to find database items for a sub project of a system if matrix set up configured
    */
    public function getSelectedSystemsExistOnDatabase($systemid = null)
    {

        $connection = $this->em->getConnection();
        $query = $connection->prepare("SELECT * FROM
                    (
                        select 1,id,sub_project_id,subprojectsystemid from SubProjectCathodicProtectionDatabase bscp where bscp.subprojectsystemid = :subprojectsystemid
                        union all 
                        select 2,id,sub_project_id,subprojectsystemid from SubProjectChemicalDosageDatabase bscd where bscd.subprojectsystemid = :subprojectsystemid
                        union all
                        select 3,id,sub_project_id,subprojectsystemid from SubProjectCorrosionMonitoringDatabase bscm where bscm.subprojectsystemid = :subprojectsystemid
                        union all
                        select 4,id,sub_project_id,subprojectsystemid from SubProjectSamplingDatabase bssd where bssd.subprojectsystemid = :subprojectsystemid
                        union all
                        select 6,id,sub_project_id,subprojectsystemid from SubProjectProductionDatabase bspd where bspd.subprojectsystemid = :subprojectsystemid
                    ) bs where bs.subprojectsystemid = :subprojectsystemid;
                 ");
        $query->bindValue('subprojectsystemid', $systemid);
        $query->execute();
        $this->results = $query->fetchAll();

        return $this->results;

    }

    /*
    * Method Description: to find all systems, threats, mitigation, activity for a sub project and database if matrix set up configured and for given database(sampling, cathodic etc)
    */
    public function getDropdownActivitiesDatabases($databases)
    {
        foreach($databases as $idex => $database)
        {
            if ($database['bsd_activityid'])
            {
                $statement = 'SELECT ba, bmt, bmc
                FROM AIECmsBundle:SubProjectSystems bs 
                join AIECmsBundle:SubProjectThreats bt WITH bs.id= bt.subprojectsystems 
                join AIECmsBundle:SubProjectMitigationTechniques bmt WITH bt.id = bmt.subprojectthreats 
                join AIECmsBundle:SubProjectActivity ba WITH bmt.id = ba.subprojectmitigationtechniques
                join AIECmsBundle:BaseMetricsConfig bmc WITH bmc.id = ba.subprojectmetrics';

                $statement .= ' WHERE ba.id=:activityid';

                $query = $this->em->createQuery($statement)
                    ->setParameter('activityid', $database['bsd_activityid']);

                $res = $query->getScalarResult();
                if (!empty($res))
                {
                    if ($res[0]['ba_ismanagement'] == 1 && $res[0]['ba_iscontrol'] == 1)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_controltitle'];
                    elseif ($res[0]['ba_ismanagement'] == 1 && $res[0]['ba_iscontrol'] == NULL)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_managementtitle'];
                    elseif ($res[0]['ba_iscontrol'] == 1 && $res[0]['ba_ismanagement'] == NULL)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_controltitle'];
                    elseif ($res[0]['ba_isflowassurancemanagement'] == 1 && $res[0]['ba_isflowassurancecontrol'] == 1)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_controltitle'];
                    elseif ($res[0]['ba_isflowassurancemanagement'] == 1 && $res[0]['ba_isflowassurancecontrol'] == NULL)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_managementtitle'];
                    elseif ($res[0]['ba_isflowassurancecontrol'] == 1 && $res[0]['ba_isflowassurancemanagement'] == NULL)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_controltitle'];
                    elseif ($res[0]['ba_isnonmonitoringkpi'] == 1)
                        $databases[$idex]['bsd_activitytitle'] = $res[0]['ba_managementtitle'];

                    $databases[$idex]['bsd_controlgreen'] = $res[0]['ba_controlgreen'];
                    $databases[$idex]['bsd_controlgreenexpression'] = $res[0]['ba_controlgreenexpression'];
                    $databases[$idex]['bsd_controlgreenrange'] = $res[0]['ba_controlgreenrange'];
                    $databases[$idex]['bsd_controlgreenexpressionrange'] = $res[0]['ba_controlgreenexpressionrange'];
                    $databases[$idex]['bsd_controlamber'] = $res[0]['ba_controlamber'];
                    $databases[$idex]['bsd_controlamberexpression'] = $res[0]['ba_controlamberexpression'];
                    $databases[$idex]['bsd_controlamberrange'] = $res[0]['ba_controlamberrange'];
                    $databases[$idex]['bsd_controlamberexpressionrange'] = $res[0]['ba_controlamberexpressionrange'];
                    $databases[$idex]['bsd_controlred'] = $res[0]['ba_controlred'];
                    $databases[$idex]['bsd_controlredexpression'] = $res[0]['ba_controlredexpression'];
                    $databases[$idex]['bsd_controlredrange'] = $res[0]['ba_controlredrange'];
                    $databases[$idex]['bsd_controlredexpressionrange'] = $res[0]['ba_controlredexpressionrange'];

                    $databases[$idex]['bsd_justificationmanagement'] = $res[0]['ba_justification_management'];
                    $databases[$idex]['bsd_justificationcontrol'] = $res[0]['ba_justification_control'];

                    $databases[$idex]['bmc_corrosioninhibitortype'] = $res[0]['bmc_corrosioninhibitortype'];

                }

            }

        }

       return $databases;

    }

    /*
    * Method Description: to find all locations from datatbases per activiy
    */
    public function getLocationsFromDatabasesPerActivity($activityid)
    {

        $connection = $this->em->getConnection();
        $query = $connection->prepare("SELECT location FROM
                    (
                        select location from SubProjectCathodicProtectionDatabase bscp where bscp.activityid = :activityid
                        union all 
                        select location from SubProjectChemicalDosageDatabase bscd where bscd.activityid = :activityid
                        union all
                        select location from SubProjectCorrosionMonitoringDatabase bscm where bscm.activityid = :activityid
                        union all
                        select location from SubProjectSamplingDatabase bssd where bssd.activityid = :activityid
                        union all
                        select location from SubProjectProductionDatabase bspd where bspd.activityid = :activityid
                    ) bs 
                 ");
        $query->bindValue('activityid', $activityid);
        $query->execute();
        $this->results = $query->fetchAll();
        $endColumnResult = '';
        if ( count($this->results) < 5 )
        {
            foreach ($this->results as $result)
            {
                if (count($this->results) == 1)
                    $endColumnResult.= $result['location'];
                else
                    $endColumnResult.= $result['location']. ", ";
            }
            $last_character = substr( $endColumnResult, -1 );
            if ($last_character == ", ")
            {
                $endColumnResult = substr(trim($endColumnResult), 0, -1);
            }
        }
        else
        {
            $endColumnResult= "Multiple Locations";
        }
        return $endColumnResult;
    }

     /*
      * Method Description: to find all locations from datatbases per system as mutiple rows
      */
    public function getLocationsFromDatabasesPerActivityAsRows($activityid)
    {

        $connection = $this->em->getConnection();
        $query = $connection->prepare("SELECT id,location FROM
                    (
                        select id,location from SubProjectCathodicProtectionDatabase bscp where bscp.activityid = :activityid
                        union all 
                        select id,location from SubProjectChemicalDosageDatabase bscd where bscd.activityid = :activityid
                        union all
                        select id,location from SubProjectCorrosionMonitoringDatabase bscm where bscm.activityid = :activityid
                        union all
                        select id,location from SubProjectSamplingDatabase bssd where bssd.activityid = :activityid
                        union all
                        select id,location from SubProjectProductionDatabase bspd where bspd.activityid = :activityid
                    ) bs 
                 ");
        $query->bindValue('activityid', $activityid);
        $query->execute();
        $this->results = $query->fetchAll();
        return $this->results;
    }

    /*
    * Method Description: to find system per activty of a database
    */
    public function getSystemsFromDatabasesPerActivity($activityid)
    {

        $connection = $this->em->getConnection();
        $query = $connection->prepare("
                        Select ba.isnonmonitoringkpi, bs.id, bs.isselected as bs_isselected, bt.isselected as bt_isselected, bmt.isselected as bmt_isselected, bs.title, ba.controltitle, ba.managementtitle, ba.kpi_control, ba.kpi_management, ba.justification_management, ba.justification_control, ba.id as ba_actid
                        FROM SubProjectSystems bs 
                        join SubProjectThreats bt ON bs.id= bt.subprojectsystemid
                        join SubProjectMitigationTechniques bmt ON bt.id = bmt.subprojectthreatid
                        join SubProjectActivity ba ON bmt.id = ba.subprojectmitigationtechniqueid
                        where ba.id = :activityid
                 ");
        $query->bindValue('activityid', $activityid);
        $query->execute();
        $this->results = $query->fetchAll();

        if (!empty($this->results[0])) return $this->results[0]; else return false;
    }

    /*
   * Method Description: to find all locations from datatbases per system
   */
    public function getLocationsFromDatabasesPerSystemSubProjectActivity($subprojectsystemid,$subprojectid,$activityid)
    {

        $connection = $this->em->getConnection();
        $query = $connection->prepare("SELECT location,id,type,analysis,recommendations,activityid FROM
                    (
                        select location,id,1 as `type`, analysis, recommendations, activityid from SubProjectCathodicProtectionDatabase bscp where bscp.subprojectsystemid = :subprojectsystemid and bscp.sub_project_id = :subprojectid and bscp.activityid = :activityid
                        union all
                        select location,id,2 as `type`, analysis, recommendations, activityid from SubProjectChemicalDosageDatabase bscd where bscd.subprojectsystemid = :subprojectsystemid and bscd.sub_project_id = :subprojectid and bscd.activityid = :activityid
                        union all
                        select location,id,3 as `type`, analysis, recommendations, activityid from SubProjectCorrosionMonitoringDatabase bscm where bscm.subprojectsystemid = :subprojectsystemid and bscm.sub_project_id = :subprojectid and bscm.activityid = :activityid
                        union all
                        select location,id,4 as `type`, analysis, recommendations, activityid from SubProjectSamplingDatabase bssd where bssd.subprojectsystemid = :subprojectsystemid and bssd.sub_project_id = :subprojectid and bssd.activityid = :activityid
                        union all
                        select location,id,6 as `type`, analysis, recommendations, activityid from SubProjectProductionDatabase bspd where bspd.subprojectsystemid = :subprojectsystemid and bspd.sub_project_id = :subprojectid and bspd.activityid = :activityid
                    ) bs
                 ");
        $query->bindValue('subprojectsystemid', $subprojectsystemid);
        $query->bindValue('subprojectid', $subprojectid);
        $query->bindValue('activityid', $activityid);
        $query->execute();
        $this->results = $query->fetchAll();
        return $this->results;
    }

    public function getAllActivityLocations($activityid, $type, $subprojectId)
    {
        $connection = $this->em->getConnection();

        $statement = "select";

        if ($type <= 4 || $type == 6) {
            $statement .= " c.*";
        }

        switch($type)
        {
            case 1:
                $dbname = "SubProjectCathodicProtectionDatabase";

                break;
            case 2:
                $dbname = "SubProjectChemicalDosageDatabase";

                break;
            case 3:
                $dbname = "SubProjectCorrosionMonitoringDatabase";

                break;
            case 4:
                $dbname = "SubProjectSamplingDatabase";

                break;
            case 6:
                $dbname = "SubProjectProductionDatabase";

                break;
        }


        if ( $type <= 4 || $type == 6) {
            $statement .= " from " . $dbname . " c";
        }

        $statement .= " where c.sub_project_id = :spid";
        $statement .= " and c.activityid = :aid";
        $statement .= " and c.id not in (select dbactivitylocationid from SubProjectCorrosionDatasheet a where a.sub_project_id=:spid and a.activityid=:aid)";

//        echo $statement;
        $query = $connection->prepare($statement);
        $query->bindValue('spid', $subprojectId);
        $query->bindValue('aid', $activityid);

        $query->execute();

        $results = $query->fetchAll();

        return $results;
    }


}
