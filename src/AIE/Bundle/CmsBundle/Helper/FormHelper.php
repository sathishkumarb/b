<?php
/**
 * User: mokha
 * Date: 3/28/16
 * Time: 11:40 AM
 */

namespace AIE\Bundle\CmsBundle\Helper;


use AIE\Bundle\CmsBundle\Entity\CmsRegistrar;

class FormHelper {


    private $em;
    private $kernel;
    private $formFactory;
    public function __construct($em, $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->formFactory = $kernel->getContainer()->get('form.factory');
    }

}