<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 12:17
 */

namespace AIE\Bundle\CmsBundle\Helper;


use Aws\CloudFront\Exception\Exception;

class CmsHelper
{

    private $doctrine;
    private $fileUploader;
    private $em;

    public function __construct($doctrine, $fileUploader)
    {
        $this->doctrine = $doctrine;
        $this->fileUploader = $fileUploader;
        $this->em = $doctrine->getManager('cms');
    }

    /***
     *
     * A function that checks whether a code is used in base mitigation technqiue
     *
     * @param int $threatId
     * @param string $code
     *
     * @return boolean
     */
    public function isBaseMitigationTechniqueUsed($threatId, $code)
    {

        if (
            $this->em->getRepository('AIECmsBundle:BaseMitigationTechniques')->findBy(
                ['basethreats' => $threatId, 'mitigationtechniquestitle' => $code]
            )
            || $this->em->getRepository('AIECmsBundle:BaseMitigationTechniques')->findBy(
                ['basethreats' => $threatId, 'mitigationtechniquestitle' => $code]
            )
        ) {
            return true;
        }

        return false;
    }

     /***
     *
     * A function that checks whether a code is used in threat
     *
     * @param int $systemId
     * @param string $code
     *
     * @return boolean
     */
    public function isBaseThreatUsed($systemId, $code)
    {

        if (
            $this->em->getRepository('AIECmsBundle:BaseThreats')->findBy(
                ['basesystems' => $systemId, 'threatstitle' => $code]
            )
          ) {
            return true;
        }

        return false;
    }


     /***
     *
     * A function that checks whether a system is used
     *
     * @param string $code
     *
     * @return boolean
     */
    public function isBaseSystemUsed($code)
    {

        if (
            $this->em->getRepository('AIECmsBundle:BaseSystems')->findBy(
                ['systemstitle' => $code]
            )
        ) {
            return true;
        }

        return false;
    }

    /***
     *
     * A function that checks whether a system is used
     *
     * @param string $code
     *
     * @return boolean
     */
    public function isMetricsUsed($code,$dbtype)
    {

        if (
        $this->em->getRepository('AIECmsBundle:BaseMetricsConfig')->findBy(
            ['title' => $code,'databasetype' => $dbtype]
        )
        ) {
            return true;
        }

        return false;
    }


    /***
     *
     * A function that checks whether a sub project threat
     *
     * @param int $systemId
     * @param string $code
     *
     * @return boolean
     */
    public function isSubProjectThreatUsed($systemId, $code)
    {

        if (
        $this->em->getRepository('AIECmsBundle:SubProjectThreats')->findBy(
            ['subprojectsystems' => $systemId, 'threatstitle' => $code]
        )
        ) {
            return true;
        }

        return false;
    }


    /***
     *
     * A function that checks whether a code is used in sub project
     *
     * @param string $code
     *
     * @return boolean
     */
    public function isSubProjectSystemUsed($code)
    {

        if (
        $this->em->getRepository('AIECmsBundle:SubProjectSystems')->findBy(
            ['systemstitle' => $code]
        )
        ) {
            return true;
        }

        return false;
    }

    public function compareRedAmberThresholdsYaxis($subProjectActivity, $compareValue)
    {
        $flag = 0;

        if ( $subProjectActivity->getControlred() != "" && $subProjectActivity->getControlamber() != "" )
        {
            if ($subProjectActivity->getControlamberexpression() && $subProjectActivity->getControlamberrange() && $subProjectActivity->getControlamber() != "N/A" && $subProjectActivity->getControlamberexpressionrange())
            {
                if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<="){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = "amber";
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<"){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = "amber";
                    }
                }

                else if ($subProjectActivity->getControlamberexpression() == ">=" && $subProjectActivity->getControlamberexpressionrange() == "<"){

                    if ($compareValue  >= $subProjectActivity->getControlamber() && $compareValue < $subProjectActivity->getControlamberrange()){
                        $flag = "amber";
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">" && $subProjectActivity->getControlamberexpressionrange() == "<="){
                    if ($compareValue  > $subProjectActivity->getControlamber() && $compareValue <= $subProjectActivity->getControlamberrange()){
                        $flag = "amber";
                    }
                }

            }
            else if ($subProjectActivity->getControlamberexpression() && $subProjectActivity->getControlamber() != "N/A" && !$subProjectActivity->getControlamberrange() && !$subProjectActivity->getControlamberexpressionrange())
            {

                if ($subProjectActivity->getControlamberexpression() == ">") {
                    if ($compareValue > $subProjectActivity->getControlamber()) {
                        $flag = "amber";
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == ">=") {
                    if ($compareValue >= $subProjectActivity->getControlamber()) {
                        $flag = "amber";
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<") {
                    if ($compareValue < $subProjectActivity->getControlamber()) {
                        $flag = "amber";
                    }
                }
                else if ($subProjectActivity->getControlamberexpression() == "<=") {
                    if ($compareValue <= $subProjectActivity->getControlamber()) {
                        $flag = "amber";
                    }
                }

            }

            if ($subProjectActivity->getControlredexpression() && $subProjectActivity->getControlredrange() && $subProjectActivity->getControlred() != "N/A" && $subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<="){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = "red";
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<"){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = "red";
                    }
                }

                else if ($subProjectActivity->getControlredexpression() == ">=" && $subProjectActivity->getControlredexpressionrange() == "<"){

                    if ($compareValue  >= $subProjectActivity->getControlred() && $compareValue < $subProjectActivity->getControlredrange()){
                        $flag = "red";
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">" && $subProjectActivity->getControlredexpressionrange() == "<="){
                    if ($compareValue  > $subProjectActivity->getControlred() && $compareValue <= $subProjectActivity->getControlredrange()){
                        $flag = "red";
                    }
                }

            }
            else if ($subProjectActivity->getControlredexpression() && $subProjectActivity->getControlred() != "N/A" && !$subProjectActivity->getControlredrange() && !$subProjectActivity->getControlredexpressionrange())
            {

                if ($subProjectActivity->getControlredexpression() == ">") {

                    if ($compareValue > $subProjectActivity->getControlred()) {
                        $flag = "red";
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == ">=") {
                    if ($compareValue >= $subProjectActivity->getControlred()) {
                        $flag = "red";
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<") {
                    if ($compareValue < $subProjectActivity->getControlred()) {
                        $flag = "red";
                    }
                }
                else if ($subProjectActivity->getControlredexpression() == "<=") {
                    if ($compareValue <= $subProjectActivity->getControlred()) {
                        $flag = "red";
                    }
                }

            }
        }


        return $flag;
    }

    public function compareChemicalRedAmberThresholdsYaxis($act, $compareValue)
    {
        $flag = "";

        if ($act['bsd_controlred'] != "" && $act['bsd_controlamber'] != "" )
        {

            if ($act['bsd_controlamberexpression'] && $act['bsd_controlamber'] != "N/A" && $act['bsd_controlamberexpressionrange'] && $act['bsd_controlamberrange'] != "N/A")
            {
                if ($act['bsd_controlamberexpression'] == ">=" && $act['bsd_controlamberexpressionrange'] == "<="){

                    if ($compareValue  >= $act['bsd_controlamber'] && $compareValue <= $act['bsd_controlamberrange']){
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">" && $act['bsd_controlamberexpressionrange'] == "<"){
                    if ($compareValue  > $act['bsd_controlamber'] && $compareValue < $act['bsd_controlamberrange']){
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">=" && $act['bsd_controlamberexpressionrange'] == "<"){

                    if ($compareValue  >= $act['bsd_controlamber'] && $compareValue < $act['bsd_controlamberrange']){
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">" && $act['bsd_controlamberexpressionrange'] == "<="){
                    if ($compareValue  > $act['bsd_controlamber'] && $compareValue <= $act['bsd_controlamberrange']){
                        $flag = "amber";
                    }
                }

            }
            else if ($act['bsd_controlamberexpression'] && $act['bsd_controlamber'] != "N/A" && !$act['bsd_controlamberrange'] && !$act['bsd_controlamberexpressionrange'])
            {

                if ($act['bsd_controlamberexpression'] == ">") {
                    if ($compareValue > $act['bsd_controlamber']) {
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == ">=") {
                    if ($compareValue >= $act['bsd_controlamber']) {
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == "<") {
                    if ($compareValue < $act['bsd_controlamber']) {
                        $flag = "amber";
                    }
                }
                else if ($act['bsd_controlamberexpression'] == "<=") {
                    if ($compareValue <= $act['bsd_controlamber']) {
                        $flag = "amber";
                    }
                }

            }


            if ($act['bsd_controlredexpression'] && $act['bsd_controlred'] != "N/A" && $act['bsd_controlredexpressionrange'] && $act['bsd_controlredrange'] != "N/A")
            {
                if ($act['bsd_controlredexpression'] == ">=" && $act['bsd_controlredexpressionrange'] == "<="){

                    if ($compareValue  >= $act['bsd_controlred'] && $compareValue <= $act['bsd_controlredrange']){
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">" && $act['bsd_controlredexpressionrange'] == "<"){
                    if ($compareValue  > $act['bsd_controlred'] && $compareValue < $act['bsd_controlredrange']){
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">=" && $act['bsd_controlredexpressionrange'] == "<"){

                    if ($compareValue  >= $act['bsd_controlred'] && $compareValue < $act['bsd_controlredrange']){
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">" && $act['bsd_controlredexpressionrange'] == "<="){
                    if ($compareValue  > $act['bsd_controlred'] && $compareValue <= $act['bsd_controlredrange']){
                        $flag = "red";
                    }
                }

            }
            else if ($act['bsd_controlredexpression'] && $act['bsd_controlred'] != "N/A" && !$act['bsd_controlredrange'] && !$act['bsd_controlredexpressionrange'])
            {

                if ($act['bsd_controlredexpression'] == ">") {
                    if ($compareValue > $act['bsd_controlred']) {
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == ">=") {
                    if ($compareValue >= $act['bsd_controlred']) {
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == "<") {

                    if ($compareValue < $act['bsd_controlred']) {
                        $flag = "red";
                    }
                }
                else if ($act['bsd_controlredexpression'] == "<=") {
                    if ($compareValue <= $act['bsd_controlred']) {
                        $flag = "red";
                    }
                }

            }

        }

        return $flag;
    }


}