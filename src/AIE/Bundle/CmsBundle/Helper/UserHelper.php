<?php
/**
 * Created by PhpStorm.
 * User: mokha
 * Date: 2/12/16
 * Time: 11:30 PM
 */

namespace AIE\Bundle\CmsBundle\Helper;
 use Doctrine\ORM\Query\ResultSetMapping;

class UserHelper
{

    private $doctrine;
    private $aieUsers;
    private $em;

    public function __construct($doctrine, $aieUsers)
    {
        $this->doctrine = $doctrine;
        $this->aieUsers = $aieUsers;
        $this->em = $doctrine->getManager('cms');
    }

    public function getProjectUsers($projectId)
    {
        // find action owners for that project
        $actionOwners = $this->em->getRepository('AIECmsBundle:ActionOwners')->findByProject($projectId);

        //get User for these actionOwners
        return $this->getActionOwnserUsers($actionOwners);

    }

    public function getProjectSpas($projectId,$role)
    {
        $actionGroupUsers = $this->em->getRepository('AIECmsBundle:UserProjectGroup')->findByProject($projectId);

        $actionSpas = [];

        if ($actionGroupUsers){

            foreach ($actionGroupUsers as $actionOwner){
             
                $users = $this->findByRoleSPA($actionOwner->getUser()->getId(),$actionOwner->getGroup()->getId(),$role);

                if ($users){
                    $userinfo = $this->doctrine->getManager()->getRepository('UserBundle:User')->find($actionOwner->getUser()->getId());
                    
                    $actionSpas[$actionOwner->getUser()->getId()] = $userinfo->getUsername();
                }

            }
        }

        return $actionSpas;
    }

    public function getActionOwnserUsers($actionOwners)
    {
        $user_ids = array_map(
            function ($user) {
                return $user->getUserId();
            },
            $actionOwners
        );

        $users = $this->aieUsers->findUsersById($user_ids, true);

        return $users;
    }

    public function setActionOwnerUser(&$actionOwners)
    {
        $users = $this->getActionOwnserUsers($actionOwners);

        array_walk(
            $actionOwners,
            function ($ao) use ($users) {
                $ao->setUser($users[$ao->getUserId()]);
            }
        );

    }

    public function findByRoleSPA($uid,$gid,$role)
    {
        $query = $this->em
            ->createQuery(
                'SELECT g,us FROM AIECmsBundle:ReflectionCcmGroup g join UserBundle:User us WHERE g.gid = :gid and us.id= :usid and g.roles LIKE :role'
            )
            ->setParameter('role', '%'.$role.'%')
            ->setParameter('usid', $uid)
            ->setParameter('gid', $gid);

        $users = $query->getResult();

        if($users)
            return $users;
        else
            return null;
    }

    public function findByRoleAdmin($role)
    {
        $query = $this->doctrine->getEntityManager()
            ->createQuery(
                'SELECT g FROM UserBundle:CcmGroup g  WHERE g.roles LIKE :role'
            )
            ->setParameter('role', '%'.$role.'%')
            ->setMaxResults(1);

        $group = $query->getResult();

        if($group)
            return $group;
        else
            return null;
    }

    public function getUserCcmGroupProjects($uid)
    {

        $qb = $this->doctrine->getEntityManager('cms')->createQueryBuilder('ug','g');
            
        $query = $qb->select('ug,g,p')

                    ->from('AIECmsBundle:ReflectionCcmGroup', 'g')
                    ->innerJoin('AIECmsBundle:UserProjectGroup', 'ug', 'WITH', 'g.gid = ug.group')
                    ->innerJoin('AIECmsBundle:Projects', 'p', 'WITH', 'p.id = ug.project')
                    ->innerJoin('UserBundle:User', 'u', 'WITH', 'u.id = ug.user');

         $qb->andWhere('u.id = :uid')
            ->setParameter('uid', $uid);

        //echo $query->getQuery()->getSql();

        $groups = $query->getQuery()->getScalarResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        if($groups)
            return $groups;
        else
            return null;

    }

    public function getUserCcmGroupSubProjects($uid)
    {

        $qb = $this->doctrine->getEntityManager('cms')->createQueryBuilder('ug','g');

        $query = $qb->select('ug,g,p,sp')

            ->from('AIECmsBundle:ReflectionCcmGroup', 'g')
            ->innerJoin('AIECmsBundle:UserProjectGroup', 'ug', 'WITH', 'g.gid = ug.group')
            ->innerJoin('AIECmsBundle:Projects', 'p', 'WITH', 'p.id = ug.project')
            ->innerJoin('AIECmsBundle:SubProjects', 'sp', 'WITH', 'sp.id = ug.subproject')
            ->innerJoin('UserBundle:User', 'u', 'WITH', 'u.id = ug.user');

        $qb->andWhere('u.id = :uid')
            ->setParameter('uid', $uid);

        //echo $query->getQuery()->getSql();

        $groups = $query->getQuery()->getResult();

        if($groups)
            return $groups;
        else
            return null;

    }

    public function getCcmGroupRoles($userid,$projectId=null)
    {
         
        // if super admin for the group
        $query = $this->em->createQuery(
                'SELECT g FROM AIECmsBundle:ReflectionCcmGroup g  join AIECmsBundle:UserCcmGroup ug WITH g.gid= ug.group WHERE ug.user = :uid'
            )
            ->setParameter('uid', $userid);
          
        $groups = $query->getResult();
   

        if($groups)
        {
            return $groups;
        }
        else
        {
            if ($projectId){
                // if role of the project group
                 $query = $this->em->createQuery(
                        'SELECT g FROM AIECmsBundle:ReflectionCcmGroup g  join AIECmsBundle:UserProjectGroup ug WITH g.gid= ug.group WHERE ug.user = :uid and ug.project= :projectid'
                    )
                    ->setParameter('uid', $userid)
                    ->setParameter('projectid', $projectId);
            } else {
                 // if role of the project group
                 $query = $this->em->createQuery(
                        'SELECT g FROM AIECmsBundle:ReflectionCcmGroup g  join AIECmsBundle:UserProjectGroup ug WITH g.gid= ug.group WHERE ug.user = :uid'
                    )
                    ->setParameter('uid', $userid);
            }
           
            $groups = $query->getResult();

            if($groups)
                return $groups;
            else
                return null;
        }

    }

    public function isRoleGranted($role,$roles)
    {
        if (in_array('ROLE_SUPER_ADMIN', $roles)){
            return true;
        }
        else{
            return in_array($role, $roles);
        }

    }

    public function getSuperAdmin($userid)
    {
        $em =  $this->doctrine->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT roles FROM users WHERE id = :id");
        $statement->bindValue('id', $userid);
        $statement->execute();
        $results = $statement->fetch();
        return $results;
    }

    public function getRequestsByStatusType($status,$type)
    {
         $qb = $this->em->createQueryBuilder();
             $query = $qb->select('req')
                        ->from('AIECmsBundle:Request','req')
                        ->where('req.status= :status');
        
        if ($type == 'A')
        {
            $query = $qb->andWhere('req.requeststatus= :type1 or req.requeststatus= :type2')
                        ->setParameter('status', $status)
                        ->setParameter('type1', 'new')
                        ->setParameter('type2', 'update')
                        ->orderBy("req.date",'DESC');
        }
        else
        {
            $query = $qb->andWhere('req.requeststatus= :type1')
                ->setParameter('status', $status)
                ->setParameter('type1', 'deferral')
                ->orderBy("req.date",'DESC');
        }
        
        //echo $query->getQuery()->getSql()."<br>";
         
        $requests = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
              
        return $requests;
    }

    public function getRequestsByIdUpdatedOrDeferred($id)
    {
        $fields = array('req.id');
        $qb = $this->em->createQueryBuilder();
        $query = $qb->select($fields)
                    ->from('AIECmsBundle:Request','req')
                    //->innerJoin('AIECmsBundle:Registrar', 'reg', 'WITH', 'req.registrar = reg.id')
                    //->where('reg.project= :projectid')
                    ->where('req.requeststatus= :type1')
                    ->orWhere('req.requeststatus= :type2')
                    ->orWhere('req.requeststatus= :type3')
                    ->andWhere('req.registrar=:id')
                    ->andWhere('req.status= :status')
                    ->setParameter('id', $id)
                    ->setParameter('type1', 'update')
                    ->setParameter('type2', 'deferral')
                    ->setParameter('type3', 'new')
                    ->setParameter('status', 1);
        
        //echo $query->getQuery()->getSql()."<br>";
         
        $requests = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
              
        return $requests;
    }
    
}