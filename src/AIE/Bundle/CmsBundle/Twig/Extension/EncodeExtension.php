<?php

namespace AIE\Bundle\CmsBundle\Twig\Extension;

use Exception;

class EncodeExtension extends \Twig_Extension
{
    protected $container;

    public function getName()
    {
        return 'encode';
    }

    public function getFilters() {
        return array(
            'json_decode'   => new \Twig_Filter_Method($this, 'jsonDecode'),
        );
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }
}