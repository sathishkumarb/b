<?php

namespace AIE\Bundle\CmsBundle\Twig\Extension;

use Exception;

class UcWordsExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('ucwords', 'ucwords')
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('ucwords', 'ucwords')
        ];
    }

    public function getName()
    {
        return 'ext.ucwords';
    }
}