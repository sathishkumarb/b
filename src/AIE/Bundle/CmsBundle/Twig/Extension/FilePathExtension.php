<?php
/**
 * User: Mokha
 * Date: 6/25/14
 * Time: 1:07 AM
 */

namespace AIE\Bundle\CmsBundle\Twig\Extension;

use AIE\Bundle\StorageBundle\Upload\FileUploader;
use AIE\Bundle\VeracityBundle\Component\Files\Files;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Gaufrette\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class FilePathExtension extends \Twig_Extension {

    private $filesystem;
    private $container;
    private $fileUploader;

    public function __construct(Filesystem $filesystem, Container $container, $fileUploader)
    {
        $this->filesystem = $filesystem;
        $this->container = $container;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @var \Twig_Environment
     */
    protected $environment;

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFilters()
    {
        return array(
            'get_file_path' => new \Twig_Function_Method($this, 'getFilePath'),
            'absolute_path' => new \Twig_SimpleFunction('absolute_path', [$this, 'absolutePath']),
            'file_url' => new \Twig_SimpleFunction('file_url', [$this, 'fileUrl'], ['pre_escape' => 'html', 'is_safe' => array('html')]),
        );
    }

    public function getFilePath($path = null)
    {
        return FileUploader::getFilePath($this->filesystem, $this->container, $path);
    }

    public function absolutePath(Files $file)
    {
        return $this->fileUploader->getAbsoluteFilePath($file->getFilepath());
    }

    public function fileUrl(Files $file){
        return '<a class="download-attachment" target="_blank" href="' . $this->absolutePath($file) . '"><span class="glyphicon glyphicon-cloud-download"></span></a><span>' . $file->getCaption() . '</span>';
    }

    public function getName()
    {
        return 'storage_file_path_twig_extension';
    }
} 