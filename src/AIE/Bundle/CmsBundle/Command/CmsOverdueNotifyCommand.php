<?php
namespace AIE\Bundle\CmsBundle\Command;


use AIE\Bundle\CmsBundle\Entity\SubProjectActivity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class CmsOverdueNotifyCommand extends ContainerAwareCommand {

    //php app/console aie:cms_notify_overdue
    protected function configure()
    {
        $this
            ->setName('aie:cms_notify_overdue')
            ->setDescription('Greet someone')
            ->addOption(
                'month',
                null,
                InputOption::VALUE_NONE,
                'If set, the action owner will get notified for all the actions coming in the next 30 days'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $container->enterScope('request');
        $container->set('request', new Request(), 'request');

        $mailer = $container->get('mailer');


        $templating = $container->get('templating');
        $em = $container->get('doctrine')->getEntityManager('cms');
        $from_address = [$container->getParameter('mailer_email') => $container->getParameter('mailer_name')];

       //GET ALL OVERDUE ACTIONS
        $overdueActions = null;

        if ($input->getOption('month'))
        {
         $overdueActions = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOverDue(true);

        } else
        {
         $overdueActions = $em->getRepository('AIECmsBundle:SubProjectActivity')->findOverDue();
        }

        $subprojectOwnersActions = [];

        $systemActions = [];


        foreach ($overdueActions as $key => $action)
        {

            $action = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($action['id']);

            $currDate_U = new \DateTime();

            $repeatedAction = clone $action;

            if($repeatedAction->getFrequency() == 0){
                continue;
            }

            $inc=0;

            $nextDate = $repeatedAction->getNextInspectionDate(true);
            $overdueActions[$key]['child'][$inc] = $nextDate->format('d F Y');
            $repeatedAction->setLastInspectionDate($nextDate);
            $nextDate = $repeatedAction->getNextInspectionDate(true);
            //echo $nextDate->format('d/m/y')."<br>";
            $nextDate_U = (int)$nextDate->format('U');

            while ($nextDate_U <= (int) $currDate_U->format('U'))
            {
                $inc++;
                $repeatedActions[] = clone $repeatedAction;

                $overdueActions[$key]['child'][$inc] = $nextDate->format('d F Y');
                //set repeated action last time to next date
                $repeatedAction->setLastInspectionDate($nextDate);
                //set next date from repeated action
                $nextDate = $repeatedAction->getNextInspectionDate(true);

                $nextDate_U = (int)$nextDate->format('U');
            }

        }


        foreach ($overdueActions as $key => $actions)
        {

            $action = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($actions['id']);

            if (! $action)
                continue;

            $mitigation = $action->getControltitle();
            $mitigationTechnique = $action->getSubProjectMitigationTechniques();

            $systemThreat = $mitigationTechnique->getSubProjectThreats();
            $systems = $systemThreat->getSubProjectSystems();

            $subproject = $systems->getSubProject();
            $dueDate = $action->getNextInspectionDate();
            $actionOwner = $action->getActionOwner();

            if (! $actionOwner)
                continue;

            if (! isSet($subprojectOwnersActions[$subproject->getId()]))
            {
                $subprojectOwnersActions[$subproject->getId()] = [
                    'subproject' => $subproject,
                    'owners'   => []
                ];
            }

            if (! isSet($subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]))
            {
                $subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()] = [
                    'owner'   => $actionOwner,
                    'actions' => []
                ];
            }

            $actionName = $mitigation;
            if (isSet($systemActions[$systems->getId()]))
            {
                if (isSet($systemActions[$systems->getId()][$actionName]))
                {
                    $index = $systemActions[$actionName];
                    if ($systemActions[$index]->getFrequency() > $action->getFrequency())
                    {
                        unset($subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]['actions'][$index]);
                    } else
                    {
                        continue;
                    }
                } else
                {
                    $systemActions[$systems->getId()][$actionName] = $key;
                }
            } else
            {
                $systemActions[$systems->getId()] = [];
                $systemActions[$systems->getId()][$actionName] = $key;
            }

            $actions['next_inspection_date']=$action->getNextInspectionDate();
            $actions['is_inspection_due']=$action->getIsInspectionDue();
            $actions['action_owner_name']=$action->getActionOwner()->getName();

            $subprojectOwnersActions[$subproject->getId()]['owners'][$actionOwner->getId()]['actions'][$key] = $actions;
        }


        $today = new \DateTime('now');
        $monthYear = $today->format('F Y');
        foreach ($subprojectOwnersActions as $poa)
        {

            $subproject = $poa['subproject'];

            foreach ($poa['owners'] as $owner)
            {

                $actionOwner = $owner['owner'];
                $actions = $owner['actions'];

                if (empty($actions))
                    continue;

                $message = new \Swift_Message();
                $message->setContentType("text/html");
                $message->setSubject('REMINDER: ' . $subproject->getName() . ' - ' . $monthYear . ' Planned and Overdue Activities')
                    ->setFrom($from_address)
                    ->setTo($actionOwner->getEmail())
                    ->setBody(
                        $templating->render(
                            'AIECmsBundle:Email:overdue.html.twig',
                            [
                                'owner'   => $actionOwner,
                                'actions' => $actions,
                            ]
                        )
                    );

                $mailer->send($message);

                foreach ($actions as $action)
                {

                    $action = $em->getRepository('AIECmsBundle:SubProjectActivity')->find($action['id']);
                    $remindedOn = $action->getRemindedOn();
                    if (! is_array($remindedOn) || empty($remindedOn))
                    {
                        $remindedOn = array();
                    }
                    $remindedOn = $remindedOn + [new \DateTime('now')];
                    $action->setRemindedOn($remindedOn);
                    $em->persist($action);
                }

            }
        }

        // now manually flush the queue
        $spool = $mailer->getTransport()->getSpool();
        $transport = $container->get('swiftmailer.transport.real');

        $spool->flushQueue($transport);
        $em->flush();
        $output->writeln('Sent!');
    }
} 
