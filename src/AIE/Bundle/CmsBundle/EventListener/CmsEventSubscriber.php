<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 15/07/15
 * Time: 04:58
 */

namespace AIE\Bundle\CmsBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CmsEventSubscriber {
    //https://stackoverflow.com/questions/13648974/symfony2-doctrine2-cross-database-join-column-throws-mapping-exception

    private $em;
    private $container;

    public function __construct(EntityManager $em , ContainerInterface $container)
    {
		$this->em = $em;
		$this->container = $container;
    }


    public function prePersist(LifecycleEventArgs $eventArgs)
    {
//        $myBundle2Entity = $eventArgs->getEntity();
//        $defaultEm = $eventArgs->getEntityManager();
//        $myBundle2EntityReflProp = $defaultEm->getClassMetadata('Entity\Request')
//            ->reflClass->getProperty('User');
//        $myBundle2EntityReflProp->setAccessible(true);
//        $myBundle2EntityReflProp->setValue(
//            $requestedByUsers, $this->bundle1Em->getReference('Entity\User', $myBundle2Entity->getMyBundle1Id())
//        );
    }
}