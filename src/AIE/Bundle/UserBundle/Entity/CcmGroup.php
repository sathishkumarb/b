<?php

namespace AIE\Bundle\UserBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;
use AIE\Bundle\UserBundle\Model\CcmGroupInterface;
use AIE\Bundle\UserBundle\Entity\CcmGroupEntityInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="ccmgroups")
 */
class CcmGroup extends BaseGroup implements CcmGroupInterface, CcmGroupEntityInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
        
}