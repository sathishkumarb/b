<?php

namespace AIE\Bundle\UserBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;
use AIE\Bundle\UserBundle\Model\AnomalyGroupInterface;
use AIE\Bundle\UserBundle\Entity\AnomalyGroupEntityInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="anomalygroups")
 */
class AnomalyGroup extends BaseGroup implements AnomalyGroupInterface, AnomalyGroupEntityInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
        
}