<?php

namespace AIE\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
//use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AnomalyUserEditFormType extends AbstractType {

    use Helper\FormStyleHelper;



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('email', 'email', $this->options(array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle')))
            // ->add('username', null, $this->options(array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle')))
            // ->add('company', 'text', $this->options(array()))
            ->add('group', 'entity', $this->options(array(
                'class'    => 'UserBundle:AnomalyGroup',
                'property' => 'name',
                'label'    => 'Positions',
                'mapped'   => true
            ), 'multiple_choice'));

    }


    public function getName()
    {
        return 'aie_user_anomaly_groups';
    }



}