<?php

namespace AIE\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class RegistrationFormType extends BaseType {

    use Helper\FormStyleHelper;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', $this->options(array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle')))
            ->add('username', null, $this->options(array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle')))
            ->add('plainPassword', 'repeated', array(
                'type'            => 'password',
                'options'         => $this->options(array('translation_domain' => 'FOSUserBundle')),
                'first_options'   => array('label' => 'form.password'),
                'second_options'  => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('company', 'text', $this->options(array()))
            ->add('groups', 'entity', $this->options(array(
                'class'    => 'UserBundle:Group',
                'property' => 'name',
                'expanded' => false,
                'multiple' => true,
                'label'    => 'Positions',
                'mapped'   => true
            ), 'multiple_choice'));

    }

    public function getName()
    {
        return 'aie_user_registration';
    }

}