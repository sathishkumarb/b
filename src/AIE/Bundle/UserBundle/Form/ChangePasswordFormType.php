<?php

namespace AIE\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ChangePasswordFormType as BaseType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Security\Core\Validator\Constraint\UserPassword as OldUserPassword;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangePasswordFormType extends BaseType {

    use Helper\FormStyleHelper;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if (class_exists('Symfony\Component\Security\Core\Validator\Constraints\UserPassword'))
        {
            $constraint = new UserPassword();
        } else
        {
            // Symfony 2.1 support with the old constraint class
            $constraint = new OldUserPassword();
        }

        $builder->add('current_password', 'password', $this->options(array(
            'label'              => 'form.current_password',
            'translation_domain' => 'FOSUserBundle',
            'mapped'             => false,
            'constraints'        => $constraint,
        )));

        $builder->add('plainPassword', 'repeated', array(
            'type'            => 'password',
            'options'         => $this->options(array('translation_domain' => 'FOSUserBundle')),
            'first_options'   => array('label' => 'form.new_password'),
            'second_options'  => array('label' => 'form.new_password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
        ));
    }

    public function getName()
    {
        return 'aie_user_change_password';
    }

}