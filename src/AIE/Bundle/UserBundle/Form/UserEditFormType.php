<?php

namespace AIE\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class UserEditFormType extends BaseType {

    use Helper\FormStyleHelper;

    public function __construct($class = 'AIE\Bundle\UserBundle\Entity\User')
    {
        parent::__construct($class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', $this->options(array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle')))
            ->add('username', null, $this->options(array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle')))
            ->add('company', 'text', $this->options(array()))
            ->add('groups', 'entity', $this->options(array(
                'class'    => 'UserBundle:Group',
                'property' => 'name',
                'label'    => 'Positions',
                'expanded' => false,
                'multiple' => true,
                'mapped'   => true
            ), 'multiple_choice'));

    }


    public function getName()
    {
        return 'aie_user_groups';
    }

}