<?php

namespace AIE\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\GroupFormType as BaseType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\UserBundle\Entity\User;
use AIE\Bundle\UserBundle\Entity\Group;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AnomalyGroupFormType extends BaseType {

    use Helper\FormStyleHelper;

    private $roles;

    public function __construct($class, ContainerInterface $container)
    {

        parent::__construct($class);

        $this->roles = $container->getParameter('security.role_hierarchy.roles');
        $this->roles = $this->roles + $container->getParameter('objectAnomalyRoles');
        //$this->roles = $this->flatArray($this->roles);
        $this->roles = $this->refactorRoles($this->roles);

        ksort($this->roles);

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, $this->options(array('label' => 'Position name')))
            ->add('roles', 'choice', $this->options(
                    array(
                        'attr'     => array('style' => 'height:300px'),
                        'choices'  => $this->roles,
                        'label'    => 'Roles',
                        'expanded' => false,
                        'multiple' => true,
                        'mapped'   => true
                    )
                    , 'multiple_choice')
            );
    }

    private function refactorRoles($originRoles)
    {
        $roles = array();
        $rolesAdded = array();

        // Add herited roles
        foreach ($originRoles as $roleParent => $rolesHerit)
        {
            $tmpRoles = array_values($rolesHerit);
            $rolesAdded = array_merge($rolesAdded, $tmpRoles);
            $roles[$roleParent] = array_combine($tmpRoles, $tmpRoles);
        }
        // Add missing superparent roles
        $rolesParent = array_keys($originRoles);
        foreach ($rolesParent as $roleParent)
        {
            if (! in_array($roleParent, $rolesAdded))
            {
                $roles['MAIN'][$roleParent] = $roleParent;
            }
        }

        return $roles;
    }

    public function getName()
    {
        return 'aie_anomaly_group_new';
    }

}