<?php

namespace AIE\Bundle\UserBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\UserBundle\Entity\User;
use AIE\Bundle\UserBundle\Form\AnomalyUserEditFormType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\AnomalyBundle\Entity\UserAnomalyGroup;

/**
 * @Route("anomaly_users")
 */
class AnomalyUserController extends BaseController {

    /**
     * Lists all ConsequencesCategories entities.
     *
     * @Route("/", name="anomaly_users")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $anomalyEm = $this->get('doctrine')->getManager('anomaly');

        $entities = $em->getRepository('UserBundle:User')->findAll();

        //Dump to anomaly connection db users id information to match referential integrity on joins across tables on anomaly module
        if ( $entities ) {
            $connection = $anomalyEm->getConnection();
            foreach( $entities as $index => $entity){
                $anomalyUser = $anomalyEm->getRepository('UserBundle:User')->findOneBy(array('id'=>$entity->getId()));
                $entities[$index]->setCompany('');
                if (!$anomalyUser) {
                    $statement = $connection->prepare("INSERT INTO users SET id = :id");
                    $statement->bindValue('id', $entity->getId());
                    $statement->execute();
                } else {
                    if ($anomalyUser->getCompany() == 'isadmin') $entities[$index]->setCompany('admin');
                }
            }
        }
        

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all ConsequencesCategories entities.
     *
     * @Route("/{id}/edit", name="anomaly_user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager('anomaly');

        //$entity = $em->getRepository('AIEAnomalyBundle:UserAnomalyGroup')->findByUser($id);
        $entity = new UserAnomalyGroup();

        $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->find($id);

        $editForm = $this->createEditForm($entity,$id);
        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'user' => $user
        );
    }

    /**
     * Creates a form to edit a UserProjectGroup entity.
     *
     * @param UserProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(UserAnomalyGroup $entity, $userId) {
        $form = $this->createForm(new AnomalyUserEditFormType(), $entity, array(
            'action' => $this->generateUrl('anomaly_user_update', array('id' => $userId)),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing UserProjectGroup entity.
     *
     * @Route("/{id}/edit", name="anomaly_user_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->get('doctrine')->getManager('anomaly');

        $entity = new UserAnomalyGroup();
        
        $form = $this->createEditForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user = $this->get('doctrine')->getManager()->getRepository('UserBundle:User')->find($id);

            $entity->setUser($user);

            $em->merge($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_user_edit', array('id' => $id)));
    }

    /**
     * Assign admin
     *
     * @Route("/{id}/doadmin", name="anomaly_user_doadmin")
     * @Method("GET")
     * @Template()
     */
    public function doadminAction(Request $request, $id){

        $em = $this->get('doctrine')->getManager('anomaly');

        $entity = new UserAnomalyGroup();

        if ($request) {

            $actionAdminGroup = $this->get('aie_anomaly.user.helper')->findByRoleAdmin('ROLE_SUPER_ADMIN');

            if ($actionAdminGroup) {
                $user = $this->get('doctrine')->getManager()->getRepository('UserBundle:User')->find($id);
                $group = $this->get('doctrine')->getManager()->getRepository('UserBundle:AnomalyGroup')->find($actionAdminGroup[0]->getId());

                $usergroup = $em->getRepository('AIEAnomalyBundle:UserAnomalyGroup')->findBy(array('user'=>$id,'group'=>$actionAdminGroup[0]->getId()));
                if (!$usergroup){
                    $entity->setUser($user);
                    $entity->setGroup($group);
                    $em->merge($entity);
                    $em->flush();

                    $entityuser = $em->getRepository('UserBundle:User')->find($id);
                    if ($entityuser){
                        $entityuser->setCompany('isadmin');
                        $em->persist($entityuser);
                        $em->flush();
                    }

                    return $this->redirect($this->generateUrl('anomaly_users'));
                }
                else {
                    throw new \Exception("Role Administrator is already assigned for the user!");
                    exit;
                }
            } else {
                throw new \Exception("Role Administrator [ROLE_SUPER_ADMIN] is not defined in the position!");
                exit;
            }

        }

    }


     /**
     * UnAssign admin
     *
     * @Route("/{id}/undoadmin", name="anomaly_user_undoadmin")
     * @Method("GET")
     * @Template()
     */
    public function undoadminAction(Request $request, $id){

        $em = $this->get('doctrine')->getManager('anomaly');

        $entity = new UserAnomalyGroup();

        if ($request) {

            $actionAdminGroup = $this->get('aie_anomaly.user.helper')->findByRoleAdmin('ROLE_SUPER_ADMIN');

            if ($actionAdminGroup) {
                $user = $this->get('doctrine')->getManager()->getRepository('UserBundle:User')->find($id);
                $group = $this->get('doctrine')->getManager()->getRepository('UserBundle:AnomalyGroup')->find($actionAdminGroup[0]->getId());

                $usergroup = $em->getRepository('AIEAnomalyBundle:UserAnomalyGroup')->findBy(array('user'=>$id,'group'=>$actionAdminGroup[0]->getId()));
                if ($usergroup){
                    $query = $em->createQuery('DELETE FROM AIEAnomalyBundle:UserAnomalyGroup uag WHERE uag.user = ?1 and uag.group = ?2');
                    $query->setParameter(1, $id);
                    $query->setParameter(2, $actionAdminGroup[0]->getId());
                    $query->execute();
                    $entityuser = $em->getRepository('UserBundle:User')->find($id);
                    if ($entityuser){
                        $entityuser->setCompany('');
                        $em->persist($entityuser);
                        $em->flush();
                    }

                    return $this->redirect($this->generateUrl('anomaly_users'));
                }
                else {
                    throw new \Exception("Role Administrator is already assigned for the user!");
                    exit;
                }
            } else {
                throw new \Exception("Role Administrator [ROLE_SUPER_ADMIN] is not defined in the position!");
                exit;
            }

        }

    }

}