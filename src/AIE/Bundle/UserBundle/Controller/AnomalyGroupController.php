<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AIE\Bundle\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterGroupResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseGroupEvent;
use FOS\UserBundle\Event\GroupEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface as container;
use AIE\Bundle\AnomalyBundle\Entity\ReflectionAnomalyGroup;

/**
 * RESTful controller managing group CRUD
 *
 * @author sathish
 */
class AnomalyGroupController extends Controller
{
    /**
     * Show all groups
     */
    public function listAction()
    {
        $groups = $this->get('aie_anomaly_groups')->findGroups();

        return $this->render('FOSUserBundle:AnomalyGroup:list.html.twig', array(
            'groups' => $groups
        ));
    }

    /**
     * Show one group
     */
    public function showAction($groupName)
    {
        $group = $this->findGroupBy('name', $groupName);

        return $this->render('FOSUserBundle:AnomalyGroup:show.html.twig', array(
            'group' => $group
        ));
    }

    /**
     * Edit one group, show the edit form
     */
    public function editAction(Request $request, $groupName)
    {
        $group = $this->findGroupBy('name', str_replace("%20"," ",$groupName));

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseGroupEvent($group, $request);
        $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('aie_anomaly_group.form.factory');

        $form = $formFactory->createForm();
       
        $form->setData($group);

        $form->handleRequest($request);
      

        if ($form->isValid()) {
            /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
            $groupManager = $this->get('aie_anomaly_groups');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_SUCCESS, $event);

            $groupManager->updateGroup($group);

            $anomalyEm = $this->get('doctrine')->getManager('anomaly');

            // replicating anomaly groups into anomaly db by reflecting into reflection anomalygroups -update
            $reflectionGroup = $anomalyEm->getRepository('AIEAnomalyBundle:ReflectionAnomalyGroup')->findOneBy(array('gid'=>$group->getId()));

            $reflectionGroup->setName($group->getName());
            $reflectionGroup->setRoles(serialize($group->getRoles()));

            $anomalyEm->persist($reflectionGroup);
            $anomalyEm->flush();

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('anomaly_user_group_show', array('groupName' => $group->getName()));
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_EDIT_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:AnomalyGroup:edit.html.twig', array(
            'form'      => $form->createview(),
            'group_name'  => $group->getName(),
        ));
    }

    /**
     * Show the new form
     */
    public function newAction(Request $request)
    {
        /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
        $groupManager = $this->get('aie_anomaly_groups');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('aie_anomaly_group.form.factory');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $group = $groupManager->createGroup('');

        $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_INITIALIZE, new GroupEvent($group, $request));

        $form = $formFactory->createForm();
        $form->setData($group);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_SUCCESS, $event);

            $groupManager->updateGroup($group);

            $anomalyEm = $this->get('doctrine')->getManager('anomaly');

            // replicating anomaly groups into anomaly db by reflecting into reflection anomalygroups - Insert
            $reflectionGroup = new ReflectionAnomalyGroup();
            $reflectionGroup->setGid($group->getId());
            $reflectionGroup->setName($group->getName());
            $reflectionGroup->setRoles(serialize($group->getRoles()));

            $anomalyEm->persist($reflectionGroup);
            $anomalyEm->flush();

            // Insert to maintain orm ref integrity foreign key checks on anomaly groups
            $anomlyGroupCheck = $anomalyEm->getRepository('UserBundle:AnomalyGroup')->findBy(array('id' => $group->getId()));

            if (!$anomlyGroupCheck) {
                $connection = $anomalyEm->getConnection();
                $statement = $connection->prepare("INSERT INTO anomalygroups SET id = :id");
                $statement->bindValue('id', $group->getId());
                $statement->execute();
            }

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('anomaly_user_group_show', array('groupName' => $group->getName()));
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:AnomalyGroup:new.html.twig', array(
            'form' => $form->createview(),
        ));
    }

    /**
     * Delete one group
     */
    public function deleteAction(Request $request, $groupName)
    {
        $group = $this->findGroupBy('name', $groupName);
        $this->get('aie_anomaly_groups')->deleteGroup($group);

        $response = new RedirectResponse($this->generateUrl('anomaly_user_group_list'));

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(FOSUserEvents::GROUP_DELETE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));

        return $response;
    }

    /**
     * Find a group by a specific property
     *
     * @param string $key   property name
     * @param mixed  $value property value
     *
     * @throws NotFoundHttpException                if user does not exist
     * @return \UserBundle\Model\GroupInterface
     */
    protected function findGroupBy($key, $value)
    {
        if (!empty($value)) {
            $group = $this->get('aie_anomaly_groups')->{'findGroupBy'.ucfirst($key)}($value);
        }

        if (empty($group)) {
            throw new NotFoundHttpException(sprintf('The group with "%s" does not exist for value "%s"', $key, $value));
        }

        return $group;
    }
}
