<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AIE\Bundle\UserBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use AIE\Bundle\UserBundle\Model\CcmGroupInterface;
use AIE\Bundle\UserBundle\Model\CcmGroupManager as BaseGroupManager;

class CcmGroupManager extends BaseGroupManager
{
    protected $objectManager;
    protected $class = "AIE\Bundle\UserBundle\Entity\CcmGroup";
    protected $repository;

    public function __construct(ObjectManager $om, $class="AIE\Bundle\UserBundle\Entity\CcmGroup")
    {
        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);

        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteGroup(CcmGroupInterface $group)
    {
        $this->objectManager->remove($group);
        $this->objectManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritDoc}
     */
    public function findGroupBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritDoc}
     */
    public function findGroups()
    {
        return $this->repository->findAll();
    }

    /**
     * Updates a group
     *
     * @param GroupInterface $group
     * @param Boolean        $andFlush Whether to flush the changes (default true)
     */
    public function updateGroup(CcmGroupInterface $group, $andFlush = true)
    {
        $this->objectManager->persist($group);
        if ($andFlush) {
            $this->objectManager->flush();
        }
    }
}
