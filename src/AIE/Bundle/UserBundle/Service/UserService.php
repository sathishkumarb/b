<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 05/01/16
 * Time: 11:40
 */

namespace AIE\Bundle\UserBundle\Service;


class UserService {

	private $defaultEm = null;

	public function __construct($doctrine) {
		$this->defaultEm = $doctrine->getManager('default');
	}

	public function findUsersById($ids = [], $asArray = false) {
		$userObjs = $this->defaultEm->getRepository('UserBundle:User')->findById(array_values($ids));
		if ($asArray) {
			$users = [];
			foreach ($userObjs as $user) {
				$users[ $user->getId() ] = $user;
			}

			return $users;
		}

		return $userObjs;
	}

}