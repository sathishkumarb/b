<?php

namespace AIE\Bundle\StorageBundle\Upload;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Gaufrette\Filesystem;

class FileUploader {

    private $filesystem;
    private $container;

    public function __construct(Filesystem $filesystem, Container $container)
    {
        $this->filesystem = $filesystem;
        $this->container = $container;
    }

    public function upload(File $file, $folder = '', array $allowedMimeTypes = [])
    {
        $isUploadedFile = $file instanceof UploadedFile;
        $mime = ($isUploadedFile) ? $file->getClientMimeType() : $file->getMimeType();
        $extension = ($isUploadedFile) ? $file->getClientOriginalExtension() : $file->getExtension();
        // Check if the file's mime type is in the list of allowed mime types.
        if (! empty($this->_allowedExtentions) && ! in_array($mime, $allowedMimeTypes))
        {
            throw new \InvalidArgumentException(sprintf('Files of type %s are not allowed.', $mime));
        }

        // Generate a unique filename based on the date and add file extension of the uploaded file
        $filename = sprintf('%s.%s', substr(md5(uniqid(rand(), true)), 10), $extension);

        //$folder = $this->sanitizeFilename($folder);

        $fullFilePath = sprintf('%s/%s', $folder, $filename);

        $adapter = $this->filesystem->getAdapter();
        $adapterClass = $this->getAdapterClass($adapter);

        switch ($adapterClass)
        {
            case 'Local': #local storage used
                break;
            case 'AwsS3': #aws s3 storage used
                #set the Mime
                $adapter->setMetadata($fullFilePath, array('contentType' => $file->getMimeType()));
                break;
        }
        $adapter->write($fullFilePath, file_get_contents($file->getPathname()));

        return $fullFilePath;
    }

    public function uploadFromUrl($url)
    {
        // Get file extension
        $extension = pathinfo($url, PATHINFO_EXTENSION);

        // Generate a unique filename based on the date and add file extension of the uploaded file
        $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $extension);

        // Guess mime type
        $mimeType = $this->guessMimeType($extension);

        $adapter = $this->filesystem->getAdapter();
        $adapter->setMetadata($filename, array('contentType' => $mimeType));
        $adapter->write($filename, file_get_contents($url));

        return $filename;
    }

    public function delete($path)
    {
        if ($path === null)
            return false;

        if ($this->filesystem->has($path))
        {
            $adapter = $this->filesystem->getAdapter();

            return $adapter->delete($path);
        }

        return false;
    }

    public function replace($path, File $file, $folder = '', array $allowedMimeTypes = [])
    {
        $this->delete($path);

        return $this->upload($file, $folder, $allowedMimeTypes);
    }

    public function read($path)
    {
        if ($this->filesystem->has($path))
        {
            return $this->filesystem->read($path);
        }

        return null;
    }

    public function getAbsoluteFilePath($path){
        return static::getFilePath($this->filesystem, $this->container, $path);
    }

    public static function getFilePath($filesystem, $container, $path = null)
    {
        if (null === $path)
            return;

        $adapter = $filesystem->getAdapter();
        $adapterClass = explode('\\', get_class($adapter));
        $adapterClass = end($adapterClass);

        $filepath = null;
        switch ($adapterClass)
        {
            case 'Local': #local storage used
                $uploadFolder = $container->getParameter('uploads_folder');
                $filepath = sprintf('%s/%s', $uploadFolder, $path);
                $filepath = $container->get('templating.helper.assets')->getUrl($filepath, null);
                break;
            case 'AwsS3': #aws s3 storage used
                #set the Mime
                $filepath = $container->getParameter('amazon_s3_base_url') . $path;
                break;
            default:
                break;
        }

        $filepath = preg_replace('/(?<!:)\/\//', '/', $filepath);

        return $filepath;
    }

    private function guessMimeType($extension)
    {
        $mimeTypes = array(

            'txt'  => 'text/plain',
            'htm'  => 'text/html',
            'html' => 'text/html',
            'php'  => 'text/html',
            'css'  => 'text/css',
            'js'   => 'application/javascript',
            'json' => 'application/json',
            'xml'  => 'application/xml',
            'swf'  => 'application/x-shockwave-flash',
            'flv'  => 'video/x-flv',

            // images
            'png'  => 'image/png',
            'jpe'  => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg'  => 'image/jpeg',
            'gif'  => 'image/gif',
            'bmp'  => 'image/bmp',
            'ico'  => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif'  => 'image/tiff',
            'svg'  => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip'  => 'application/zip',
            'rar'  => 'application/x-rar-compressed',
            'exe'  => 'application/x-msdownload',
            'msi'  => 'application/x-msdownload',
            'cab'  => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3'  => 'audio/mpeg',
            'qt'   => 'video/quicktime',
            'mov'  => 'video/quicktime',

            // adobe
            'pdf'  => 'application/pdf',
            'psd'  => 'image/vnd.adobe.photoshop',
            'ai'   => 'application/postscript',
            'eps'  => 'application/postscript',
            'ps'   => 'application/postscript',

            // ms office
            'doc'  => 'application/msword',
            'rtf'  => 'application/rtf',
            'xls'  => 'application/vnd.ms-excel',
            'ppt'  => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',

            // open office
            'odt'  => 'application/vnd.oasis.opendocument.text',
            'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (array_key_exists($extension, $mimeTypes))
        {
            return $mimeTypes[$extension];
        } else
        {
            return 'application/octet-stream';
        }

    }

    /**
     * @param $adapter
     * @return array|mixed
     */
    public function getAdapterClass($adapter)
    {
        $adapterClass = explode('\\', get_class($adapter));
        $adapterClass = end($adapterClass);

        return $adapterClass;
    }

    protected function sanitizeFilename($f)
    {
        // a combination of various methods
        // we don't want to convert html entities, or do any url encoding
        // we want to retain the "essence" of the original file name, if possible
        // char replace table found at:
        // http://www.php.net/manual/en/function.strtr.php#98669
        $replace_chars = array(
            'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
            'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
            'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f'
        );
        $f = strtr($f, $replace_chars);
        // convert & to "and", @ to "at", and # to "number"
        $f = preg_replace(array('/[\&]/', '/[\@]/', '/[\#]/'), array('-and-', '-at-', '-number-'), $f);
        $f = preg_replace('/[^(\x20-\x7F)]*/', '', $f); // removes any special chars we missed
        $f = str_replace(' ', '-', $f); // convert space to hyphen
        $f = str_replace('\'', '', $f); // removes apostrophes
        $f = preg_replace('/[^\w\-\.]+/', '', $f); // remove non-word chars (leaving hyphens and periods)
        $f = preg_replace('/[\-]+/', '-', $f); // converts groups of hyphens into one
        return strtolower($f);
    }
}