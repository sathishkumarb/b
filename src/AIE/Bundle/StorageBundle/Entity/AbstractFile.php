<?php
namespace AIE\Bundle\StorageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class AbstractFile {


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $filepath;
    protected $file;
    protected $allowedMimeTypes = [];

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Set path
     *
     * @param string $path
     * @return PipelineFiles
     */
    public function setFilepath($path)
    {
        $this->filepath = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    public function getAllowedMimeTypes()
    {
        return $this->allowedMimeTypes;
    }

    /***
     * A function that returns the folder that the files must be stored in
     *
     * @return mixed
     */
    abstract public function getUploadDir();
}