<?php

namespace AIE\Bundle\StorageBundle\FormHandler;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gaufrette\Filesystem;

class FilesFormHandler {

    protected $form;
    protected $request;
    protected $pageManager;
    protected $filesystem;

    public function __construct(Form $form, Request $request, Filesystem $filesystem, $pageManager)
    {
        $this->form = $form;
        $this->request = $request;
        $this->filesystem = $filesystem;
        $this->pageManager = $pageManager;
    }

    public function process()
    {
        if ($this->request->getMethod() == 'POST')
        {
            $this->form->bind($this->request);

            if ($this->form->isValid())
            {
                $this->onSuccess($this->form->getData());

                return true;
            }
        }

        return false;
    }

    function onSuccess(Page $page)
    {
        //has uploaded file ?
        if ($page->getFile() instanceof UploadedFile)
        {
            //do some logic with gaufrette filesystem
            //if page is already managed in database (update), delete previous file


        }
        //storage
        $this->pageManager->save($page);
    }
}