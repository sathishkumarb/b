<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 11/10/15
 * Time: 03:48
 */

namespace AIE\Bundle\AnomalyBundle\Manager;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\Request;
use AIE\Bundle\VeracityBundle\Component\Service\Manager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use AIE\Bundle\AnomalyBundle\Entity\Deferrals;

class RequestManager extends Manager
{

    public function add(Registrar $entity, $data = null, $type=null)
    {
        if ($data === null) {
            $data = $this->jms->serialize($entity, 'json');
        }

        $request = new Request();
        $request->setType(get_class($entity));
        $request->setData($data);
        $request->setDate(new \DateTime('now'));
        $request->setStatus(-1); //-1 for unkown

        $userId = $this->user->getId();
        $actionOwner = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneByUserId($userId);
        if(!$actionOwner){
            throw new \Exception("You don't have permission to send requests!");
        }

        $request->setRequestedBy($actionOwner);

        $request->setRegistrar($entity);

        if ( $type ){
             $request->setRequestStatus($type);
        }

        return $request;
    }


    public function update(Registrar $entity, $id, $data = null, $type=null, $status =-1, $justification = null)
    {
        if ($data === null) {
            $data = $this->jms->serialize($entity, 'json');
        }
        
        $request = $this->em->getRepository('AIEAnomalyBundle:Request')->find($id);
     
        if (!$request) {
            throw $this->createNotFoundException('Unable to find request entity.');
        }

        $request->setType(get_class($entity));
        $request->setData($data);
        $request->setDate(new \DateTime('now'));
        $request->setStatus($status); //-1 for unknown

        $userId = $this->user->getId();

        if(! empty($justification))
        {
            $request->setJustification($justification);
        } else {
            $request->setJustification($entity->getDeferralRejectJustification());
        }

        if( $type ){
            $request->setRequestStatus($type);
            if ( $type == 'deferral' )
            {
                 $actionOwner = $this->em->getRepository('AIEAnomalyBundle:Deferrals')
                     ->findOneBy( array('actionOwner' => $userId));
            }
            else
            {
                 $actionOwner = $this->em->getRepository('AIEAnomalyBundle:ApprovalOwners')
                     ->findOneBy(array('actionOwner' => $userId));
            }
        }

        if(!$actionOwner){
            throw new \Exception("You don't have permission to approve requests!");
        } else{
            $userinfo = $this->defem->getRepository('UserBundle:User')->find($actionOwner->getActionOwner()->getId());

            if ( $type == 'deferral' )
                $request->setDeferredBy($actionOwner);

            $request->setAnsweredDate(new \DateTime());
            
            $request->setRegistrar($entity);

            $this->em->flush();

            return $request;

        }

        
    }


}