<?php

namespace AIE\Bundle\AnomalyBundle\Controller;


use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\Files;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Form\RegistrarSearchType;
use AIE\Bundle\VeracityBundle\Helper\EntityHelper;
use AIE\Bundle\VeracityBundle\Helper\FormHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Form\AnomalyRegistrarType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use DeepCopy\DeepCopy;
use DeepCopy\Filter\Doctrine\DoctrineProxyFilter;
use DeepCopy\Matcher\Doctrine\DoctrineProxyMatcher;
use DeepCopy\Filter\SetNullFilter;
use DeepCopy\Matcher\PropertyNameMatcher;
use DeepCopy\Filter\Doctrine\DoctrineCollectionFilter;
use DeepCopy\Matcher\PropertyTypeMatcher;
use Doctrine\ORM\Query\ResultSetMapping;
use AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\PipingAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\StaticEquipmentAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\StructureAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\MarineAnomaly;

use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Type;

/**
 * AnomalyRegistrar controller.
 */
class AnomalyRegistrarController extends RegistrarController
{

    protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar';
    protected $registrarClassName = 'AnomalyRegistrar';
    protected $registrarRouteBase = 'anomaly_pl';
    protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\AnomalyRegistrarType';

    Protected $addGranted = 0;
    Protected $editGranted = 0;
    Protected $deleteGranted = 0;
    Protected $showGranted = 0;
    Protected $listGranted = 0;
    Protected $historyGranted = 0;
    Protected $filesGranted = 0;
    Protected $monitorsGranted = 0;
    Protected $items=['PL'=>'PIPELINE','PI'=>'PIPING','ST'=>'STRUCTURE','SE'=>'STATICEQUIPMENT','MR'=>'MARINE'];

    /**
     * Lists anomaly entities by route type.
     *
     * @Template()
     */
    public function deleteIndividualAction(Request $request, $projectId, $id)
    {
        $em =  $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            $this->addFlash('error', 'Anomaly is not exist');
        }else{

            if ($this->securityHelper->isRoleGranted('ROLE_' . $this->items[$entity->getCode()] . '_ANOMALY_DELETE', $this->userRolesMergeToCheck)) {
                $aRegister = $em->getRepository('AIEAnomalyBundle:ActionRegistrar')->findBy(['anomaly'=>$id]);
                $actionRegistersString = "";
                foreach ($aRegister as $item){
                    $actionRegistersString= $actionRegistersString . $item->getId() . ",";
                }
                $actionRegistersString = ($actionRegistersString == "" ? "" : substr($actionRegistersString, 0, -1));
                //echo $actionRegistersString;die();
                $connection = $em->getConnection();
                $statement = $connection->prepare("SET SQL_SAFE_UPDATES = 0;
                    delete FROM anomaly.Monitor where anomaly_id=:id;
                    delete FROM anomaly.RequestFiles where request_id IN (select id from anomaly.Request where registrar_id=:id);
                    delete FROM anomaly.RequestResponseFiles where request_id IN  (select id from anomaly.Request where registrar_id=:id);
                    delete FROM anomaly.Files where anomaly_id=:id;
                    delete from anomaly.TrackingCriteria where registrar_id =:id;
                    delete FROM anomaly.ActionMonitor where action_id IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.AssessmentRegistrar where id  IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.FabricMaintenanceRegistrar where id  IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.TemporaryRepairOrderRegistrar where id IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.RepairOrderRegistrar where id  IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.RequestFiles where request_id IN (select id from anomaly.Request where registrar_id=:id or registrar_id in (select id from anomaly.ActionRegistrar where anomaly_id=:id));
                    delete FROM anomaly.Request where registrar_id IN (select id from anomaly.ActionRegistrar where anomaly_id=:id);
                    delete FROM anomaly.Request where registrar_id = :id;
                    delete FROM anomaly.ActionRegistrar where anomaly_id=:id;
                    delete FROM anomaly.PipingAnomaly where id=:id;
                    delete FROM anomaly.PipelineAnomaly where id=:id;
                    delete FROM anomaly.MarineAnomaly where id=:id;
                    delete FROM anomaly.StaticEquipmentAnomaly where id=:id;
                    delete FROM anomaly.StructureAnomaly where id=:id;
                    delete FROM anomaly.AnomalyRegistrar where id=:id;
                    delete FROM anomaly.Registrar where id  IN (" .  ($actionRegistersString == "" ? "0" : $actionRegistersString) . ");
                    delete FROM anomaly.Registrar where id  = :id;");
                $statement->bindValue('id', $id);
                $statement->execute();
            }else{
                $this->addFlash('error', 'Permission to remove anomaly is denied');
            }

        }
        $referer = $this->getRequest()->headers->get('referer');
        if ($referer == NULL) {
            return $this->redirect($this->generateURL('anomaly_master', array('projectId' => $projectId)));
        } else {
            return $this->redirect($referer);
        }
    }

    /**
     * Lists anomaly entities by route type.
     *
     * @Template()
     */
    public function indexAction(Request $request, $projectId)
    {
        $routeName = $request->get('_route');
        if ($routeName === 'anomaly_master'){
            $this->registrarRouteBase= 'anomaly_master';
        }
        $em = $this->getManager();
        $userHelper = $this->get('aie_anomaly.user.helper');

        /** @var Projects $project */
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $status = [];
        $entityAbbs=[];
        $entity = new $this->registrarClass();
        $entity->setProject($project);
        $searchForm = $this->createSearchForm($projectId, $entity);
        $searchForm->handleRequest($request);
        $nextInspectionDate = null;
        if ($request->isMethod('POST') && $searchForm->isValid()) {

            $data = $searchForm->getData();

            $filter_vars = EntityHelper::getEntityColumnValues($data, $em);
            $risk = $searchForm->get('riskCriticality')->getData();

            $filter_vars = array_filter($filter_vars);
            $entities = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->search($filter_vars);
            if (isset($risk))
            {
                $entities = array_filter(
                    $entities,
                    function ($entity) use ($risk)
                    {
                        if ($entity->getRisk() == $risk)
                        {
                            return true;
                        }
                        return false;
                    }
                );
            }
            if ($searchForm->get('raisedOn')->getData())
            {
                $raisedOnDate = $searchForm->get('raisedOn')->getData();
                $raisedOnDate = new \DateTime($raisedOnDate);
                $raisedOnDate = $raisedOnDate->format('Y-m-d');
                $entities = array_filter(
                    $entities,
                    function ($entity) use ($raisedOnDate)
                    {
                        if ($raisedOnDate && $entity->getRaisedOn()->format('Y-m-d') == $raisedOnDate)
                        {
                            return true;
                        }
                        return false;
                    }
                );

            }
        } else {
            $entities = $project->getRegistrars()->toArray();
        }

        $entityType = $this->registrarClass;
        if ($entityType) {
            $entities = array_filter(
                $entities,
                function ($entity) use ($entityType) {
                    return $entity instanceof $entityType;
                }
            );
        }


        $userIds = [];
        $checkDeferExistsStruct = [];
        $requestUpdatedDeferredStruct = [];

        foreach ($entities as $a) {
            $id = $a->getSpa()->getUserId();
            if ($id && !isset($userIds [$id])) {
                $userIds [$id] = $id;
            }

            $requestUpdatedDeferredStruct = $userHelper->getRequestsByIdUpdatedOrDeferred($a->getId());

            if (count($requestUpdatedDeferredStruct)) {
                $checkDeferExistsStruct[$a->getId()] = 1;
            } else {
                $checkDeferExistsStruct[$a->getId()] = 0;
            }
            $status[$a->getId()]=$a->getStatusTag();
        }

        $users = $this->get('aie_users')->findUsersById(array_values($userIds), true);
        usort($entities, function($a, $b) {
            return $a->getNextInspectionDueDate() < $b->getNextInspectionDueDate();
        });
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

        $grantArray=[
            "PL"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0],
            "PI"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0],
            "ST"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0],
            "SE"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0],
            "MR"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0]
        ];
        $hasPermission=true;
        if ( $routeName == 'anomaly_pl') {
            if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show pipeline anomalies";
            }
        }
        if ( $routeName == 'anomaly_pi') {
            if (!$this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show piping anomalies";
            }
        }
        if ( $routeName == 'anomaly_st' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show structure anomalies";
            }
        }
        if ( $routeName == 'anomaly_se' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show static equipment anomalies";
            }
        }
        if ( $routeName == 'anomaly_mr' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show marine anomalies";
            }
        }
        if ( $routeName === 'anomaly_master' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_ANOMALY_MASTER', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show master anomalies register";
            }
        }
        if(!$hasPermission){
            $this->addFlash('error', $message);
            $referer = $this->getRequest()->headers->get('referer');
            if ($referer == NULL) {
                return $this->redirect($this->generateURL('anomaly_projects', array('id' => $projectId)));
            } else {
                return $this->redirect($referer);
            }
        }


        foreach ($this->items as $key => $value){
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_SHOW', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['show']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_NEW', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['add']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_EDIT', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['edit']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_DELETE', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['delete']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_FILES', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['files']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_MONITOR', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['monitor']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_HISTORY', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['history']=1;
            }
//            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck)) {
//                $grantArray[$key]['duplicate']=1;
//            }
        }


        return [
            'entities' => $entities,
            'projectId' => $projectId,
            'project'=>$project,
            'users' => $users,
            'search_form' => $searchForm->createView(),
            'DeferLinkStruct' => $checkDeferExistsStruct,
            'status'=> $status,
            'granted_list'=>$grantArray,
            'nextInspectionDateInput' => $searchForm->get('nextInspectionDate')->getData()
        ];

    }

    /**
     * Delete confirmation for anomalies
     *
     * @Template()
     */
    public function showdeleteAction(Request $request, $projectId, $id)
    {

        $em = $this->getManager();

        /** @var AnomalyRegistrar $entity */
        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }

        if ($entity instanceof PipelineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_DELETE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to delete pipeline anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to delete pipeline anomalies."));
        }
        if ($entity instanceof PipingAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_DELETE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to delete piping anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to delete piping anomalies."));
        }
        if ($entity instanceof StructureAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_DELETE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to delete structure anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to delete structure anomalies."));
        }
        if ($entity instanceof StaticEquipmentAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_DELETE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to delete static equipment anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to delete static equipment anomalies."));
        }
        if ($entity instanceof MarineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_DELETE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to delete marine anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to delete marine anomalies."));
        }

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'delete_form' => $this->createDeleteForm($projectId, $id)->createView()
        ];

    }

    /**
     * Lists all entities.
     *
     * @Route("master", name="anomaly_search_master")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function masterAction(Request $request, $projectId)
    {
        $this->registrarRouteBase = 'anomaly_master';
        return self::indexAction($request, $projectId);
    }

    /**
     * Creates a form for advanced search for a AnomalyRegistrar entity.
     *
     * @param Registrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createSearchForm($projectId, $entity)
    {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity->setProject($project);

        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);
        if (count($actionOwners) > 0) {
            if (!$entity->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project' => $projectId));
                $userActionId = $userAction->getId();
            } else {
                $userActionId = $entity->getSpaId();
            }
        }else{
            $userActionId=null;
        }

        $form = $this->createForm(
            new RegistrarSearchType($entity, $em, $actionOwners,$userActionId),
            $entity,
            [
                'action' => $this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]),
                'method' => 'POST',
                'data_class' => $this->registrarClass,
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Search', 'attr' => ['class' => 'btn-block']], 'btn'));
        $form->add('Clear', 'button', $this->options(['label' => 'Clear', 'attr' => ['class' => 'btn-block btn-warning']], 'btn'));
        return $form;
    }

    public function is_field_empty($field){
        if ($field=="" || $field==null || empty($field))
            return true;
        return false;
    }

    public function createAction(Request $request, $projectId)
    {
        /** @var AnomalyRegistrar $entity */
        $entity = new $this->registrarClass();

        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $usr = $this->get('security.context')->getToken()->getUser();
        $actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneByUserId($usr->getId());
        if(!$actionOwner){
            $this->addFlash('error', 'You don\'t have permission to send requests!');
            return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
        }

        $approvalOwnerExists = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findBy(array('project'=>$projectId));
        if(!$approvalOwnerExists){
            $this->addFlash('error', 'No approval Owners defined to create the request!');
            return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
        }
        $form = $this->createCreateForm($projectId, $entity);

        $files = $request->files;
        if (isset($files)) {

            $break = false;
            foreach($files as $value) {

                foreach($value['files_group']['files'] as $inneritem) {

                    if (empty($inneritem['files_group']['file'])) {
                        $break = true;
                        break;
                    }

                }

            }

            if ($break==true){
                $error = new FormError("Please attach file[s] if opted to upload documents!");
                $form->addError($error);
                return [
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'projectId' => $projectId,
                    'project' => $project,
                ];

            }
        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $allareValide=true;

            $remedialActions=$request->request->get('aie_bundle_anomalybundle_' . strtolower($this->registrarClassName))['remedial_group'];
            $formItem=$request->request->get('aie_bundle_anomalybundle_' . strtolower($this->registrarClassName));
            if (count($remedialActions)==0){
                $allareValide=false;
            }else{
                foreach ($remedialActions['actionRequired'] as $index) {
                    if ($index==1 && ($this->is_field_empty($formItem['ro_expiryDate']) ||
                            $this->is_field_empty($formItem['ro_description']))){
                        $allareValide=false;
                    }
                    if ($index==2 && ($this->is_field_empty($formItem['tro_expiryDate']) ||
                            $this->is_field_empty($formItem['tro_repairType']) ||
                            $this->is_field_empty($formItem['tro_designLife']))){
                        $allareValide=false;
                    }
                    if ($index==3 && ($this->is_field_empty($formItem['fm_expiryDate']) ||
                            $this->is_field_empty($formItem['fm_description']))){
                        $allareValide=false;
                    }
                    if ($index==4 && ($this->is_field_empty($formItem['as_expiryDate']))){
                        $allareValide=false;
                    }
                }
            }
            if($allareValide){
                $actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($form->getData()->getSpa()->getId());
                $entity->setSpa($actionOwner);

                $usr = $this->get('security.context')->getToken()->getUser();
                $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy(array('actionOwner'=>$usr->getId()));
                $entity->setApprovedspa($approvalOwner);
                $entity->setProject($project);
                $entity->setStatusTag($entity->getTagChoices()['request']['name']);
                $this->setNewEntityFields($entity, $form);

                if ($entity->getTrackingCriteria()) {
                    foreach ($entity->getTrackingCriteria() as $tc) {
                        if (!$tc->getRegistrar()) { // if registrar is not set for tracking criteria
                            $tc->setRegistrar($entity); // set it
                        }
                    }
                }
                $data = $form->getData();
                if ($data->getFiles()) {
                    foreach ($data->getFiles() as $file) {
                        $_file = $file->getFile();
                        if (!$_file) {
                            continue;
                        }
                        $file->setAnomaly($entity);
                        $uploadedPath = $this->uploadFile($_file, $file);
                        $file->setFilepath($uploadedPath);
                        $file->setSize($_file->getSize());
                        $file->setDate(new \DateTime());
                    }
                }

                $em->persist($entity);
                $em->flush();
                $requestsManager = $this->get('anomaly_manager.requests');
                $entityRequest = $requestsManager->add($entity,null,'new');
                $currentDate = new \DateTime();
                $batchId = $currentDate->format('YmdHis') . str_pad(rand(0, pow(10, 4)-1), 4, '0', STR_PAD_LEFT);;
                $entityRequest->setRequestSource("new_anomaly");
                $entityRequest->setRequestBatch($batchId);
                $entityRequest->setApprovalLevel(1);
                $entityRequest->setRequestFiles($data->getFiles());
                $em->persist($entityRequest);
                $em->flush();
                $serializer = $this->get('jms_serializer');
                $criticalityProject=($project instanceof RiskProject ? false : true);
                foreach ($remedialActions['actionRequired'] as $index) {
                    if ($index==1){
                        $register = new RepairOrderRegistrar();
                        $parms=['ro_expiryDate'=>$formItem['ro_expiryDate'],
                            'ro_recommendations'=>($this->is_field_empty($formItem['ro_recommendations'])? null : $formItem['ro_recommendations']),
                            'ro_description'=>$formItem['ro_description'],
                            'ro_locationDetails'=>($this->is_field_empty($formItem['ro_locationDetails'])? null : $formItem['ro_locationDetails']),
                            'ro_repairPriority'=>($this->is_field_empty($formItem['ro_repairPriority'])? null : $formItem['ro_repairPriority']),
                            'ro_closeOutDate'=>($this->is_field_empty($formItem['ro_closeOutDate'])? null : $formItem['ro_closeOutDate']),
                            'ro_closeOutJustification'=>($this->is_field_empty($formItem['ro_closeOutJustification'])? null : $formItem['ro_closeOutJustification']),
                            'ro_status'=>($this->is_field_empty($formItem['ro_status'])? null : $formItem['ro_status']),
                            'ro_spa'=>($this->is_field_empty($formItem['ro_spa'])? null : $formItem['ro_spa'])
                        ];
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        $register->setExpiryDate(\DateTime::createFromFormat('j M Y', $parms['ro_expiryDate']));
                        $register->setRecommendations($parms['ro_recommendations']);
                        $register->setDescription($parms['ro_description']);
                        $register->setLocationDetails($parms['ro_locationDetails']);
                        $register->setRepairPriority($parms['ro_repairPriority']);
                        if ($parms['ro_closeOutDate']!=null)
                            $register->setCloseOutDate(\DateTime::createFromFormat('j M Y', $parms['ro_closeOutDate']));
                        $register->setCloseOutJustification($parms['ro_closeOutJustification']);
                        $roActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($parms['ro_spa']);
                        $register->setSpa($roActionOwner);
                        $register->setStatus($formItem['ro_status']);
                        if (strpos($register->getStatusTag(), 'request') === false) {
                            $register->setStatusTag($register->getTagChoices()['request']['name']);
                        }
                        $em->persist($register);
                        $em->flush();
                        $entityRequest = $requestsManager->add($register, null, 'new');
                        $entityRequest->setRequestSource("new_anomaly");
                        $entityRequest->setRequestBatch($batchId);
                        $entityRequest->setApprovalLevel(1);
                        $em->persist($entityRequest);
                        $em->flush();
                    }
                    if ($index==2){
                        $register = new TemporaryRepairOrderRegistrar();
                        $parms=['tro_expiryDate'=>$formItem['tro_expiryDate'],
                            'tro_recommendations'=>($this->is_field_empty($formItem['tro_recommendations'])? null : $formItem['tro_recommendations']),
                            'tro_repairType'=>$formItem['tro_repairType'],
                            'tro_designLife'=>$formItem['tro_designLife'],
                            'tro_mocNumber'=>($this->is_field_empty($formItem['tro_mocNumber'])? null : $formItem['tro_mocNumber']),
                            'tro_installationDate'=>($this->is_field_empty($formItem['tro_installationDate'])? null : $formItem['tro_installationDate']),
                            'tro_inspectionTechnique'=>($this->is_field_empty($formItem['tro_inspectionTechnique'])? null : $formItem['tro_inspectionTechnique']),
                            'tro_inspectionDate'=>($this->is_field_empty($formItem['tro_inspectionDate'])? null : $formItem['tro_inspectionDate']),
                            'tro_inspectionFrequency'=>($this->is_field_empty($formItem['tro_inspectionFrequency'])? null : $formItem['tro_inspectionFrequency']),
                            'tro_inspectionResults'=>($this->is_field_empty($formItem['tro_inspectionResults'])? null : $formItem['tro_inspectionResults']),
                            'tro_bufferDays'=>($this->is_field_empty($formItem['tro_bufferDays'])? null : $formItem['tro_bufferDays']),
                            'tro_status'=>($this->is_field_empty($formItem['tro_status'])? null : $formItem['tro_status']),
                            'tro_spa'=>($this->is_field_empty($formItem['tro_spa'])? null : $formItem['tro_spa'])
                        ];
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        $register->setExpiryDate(\DateTime::createFromFormat('j M Y', $parms['tro_expiryDate']));
                        $register->setRecommendations($parms['tro_recommendations']);
                        $register->setRepairType($parms['tro_repairType']);
                        $register->setDesignLife($parms['tro_designLife']);
                        $register->setMocNumber($parms['tro_mocNumber']);
                        if ($parms['tro_installationDate']!=null){
                            $register->setInstallationDate(\DateTime::createFromFormat('j M Y', $parms['tro_installationDate']));
                            $register->setInspectionTechnique($parms['tro_inspectionTechnique']);
                            if ($parms['tro_inspectionDate']!=null)
                                $register->setInspectionDate(\DateTime::createFromFormat('j M Y', $parms['tro_inspectionDate']));
                            $register->setInspectionFrequency($parms['tro_inspectionFrequency']);
                            $register->setInspectionResults($parms['tro_inspectionResults']);
                            $register->setBufferDays($parms['tro_bufferDays']);
                        }
                        $roActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($parms['tro_spa']);
                        $register->setSpa($roActionOwner);
                        $register->setStatus($formItem['tro_status']);
                        if (strpos($register->getStatusTag(), 'request') === false) {
                            $register->setStatusTag($register->getTagChoices()['request']['name']);
                        }
                        $em->persist($register);
                        $em->flush();
                        $entityRequest = $requestsManager->add($register, null, 'new');
                        $entityRequest->setRequestSource("new_anomaly");
                        $entityRequest->setRequestBatch($batchId);
                        $entityRequest->setApprovalLevel(1);
                        $em->persist($entityRequest);
                        $em->flush();
                    }
                    if ($index==3){
                        $register = new FabricMaintenanceRegistrar();
                        $parms=['fm_expiryDate'=>$formItem['fm_expiryDate'],
                            'fm_recommendations'=>($this->is_field_empty($formItem['fm_recommendations'])? null : $formItem['fm_recommendations']),
                            'fm_description'=>$formItem['fm_description'],
                            'fm_immediateRepair'=>($this->is_field_empty($formItem['fm_immediateRepair'])? null : $formItem['fm_immediateRepair']),
                            'fm_longIsolation'=>($this->is_field_empty($formItem['fm_longIsolation'])? null : $formItem['fm_longIsolation']),
                            'fm_inService'=>($this->is_field_empty($formItem['fm_inService'])? null : $formItem['fm_inService']),
                            'fm_completionDate'=>($this->is_field_empty($formItem['fm_completionDate'])? null : $formItem['fm_completionDate']),
                            'fm_status'=>($this->is_field_empty($formItem['fm_status'])? null : $formItem['fm_status']),
                            'fm_spa'=>($this->is_field_empty($formItem['fm_spa'])? null : $formItem['fm_spa'])
                        ];
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        $register->setExpiryDate(\DateTime::createFromFormat('j M Y', $parms['fm_expiryDate']));
                        $register->setRecommendations($parms['fm_recommendations']);
                        $register->setDescription($parms['fm_description']);
                        $register->setImmediateRepair($parms['fm_immediateRepair']);
                        $register->setLongIsolation($parms['fm_longIsolation']);
                        $register->setInService($parms['fm_inService']);
                        if ($parms['fm_completionDate']!=null)
                            $register->setCompletionDate(\DateTime::createFromFormat('j M Y', $parms['fm_completionDate']));
                        $roActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($parms['fm_spa']);
                        $register->setSpa($roActionOwner);
                        $register->setStatus($formItem['fm_status']);
                        if (strpos($register->getStatusTag(), 'request') === false) {
                            $register->setStatusTag($register->getTagChoices()['request']['name']);
                        }
                        $em->persist($register);
                        $em->flush();
                        $entityRequest = $requestsManager->add($register, null, 'new');
                        $entityRequest->setRequestSource("new_anomaly");
                        $entityRequest->setRequestBatch($batchId);
                        $entityRequest->setApprovalLevel(1);
                        $em->persist($entityRequest);
                        $em->flush();
                    }
                    if ($index==4){
                        $register = new AssessmentRegistrar();
                        $parms=['as_expiryDate'=>$formItem['as_expiryDate'],
                            'as_recommendations'=>($this->is_field_empty($formItem['as_recommendations'])? null : $formItem['as_recommendations']),
                            'as_assessmentDueDate'=>($this->is_field_empty($formItem['as_assessmentDueDate'])? null : $formItem['as_assessmentDueDate']),
                            'as_assessmentType'=>($this->is_field_empty($formItem['as_assessmentType'])? null : $formItem['as_assessmentType']),
                            'as_assessmentCode'=>($this->is_field_empty($formItem['as_assessmentCode'])? null : $formItem['as_assessmentCode']),
                            'as_assessmentResult'=>($this->is_field_empty($formItem['as_assessmentResult'])? null : $formItem['as_assessmentResult']),
                            'as_assessmentBufferDays'=>($this->is_field_empty($formItem['as_assessmentBufferDays'])? null : $formItem['as_assessmentBufferDays']),
                            'as_status'=>($this->is_field_empty($formItem['as_status'])? null : $formItem['as_status']),
                            'as_spa'=>($this->is_field_empty($formItem['as_spa'])? null : $formItem['as_spa'])
                        ];
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        $register->setExpiryDate(\DateTime::createFromFormat('j M Y', $parms['as_expiryDate']));
                        $register->setRecommendations($parms['as_recommendations']);
                        if ($parms['as_assessmentDueDate']!=null)
                            $register->setAssessmentDueDate(\DateTime::createFromFormat('j M Y', $parms['as_assessmentDueDate']));
                        $register->setAssessmentType($parms['as_assessmentType']);
                        $register->setAssessmentCode($parms['as_assessmentCode']);
                        $register->setAssessmentResult($parms['as_assessmentResult']);
                        $register->setAssessmentBufferDays($parms['as_assessmentBufferDays']);
                        $roActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($parms['as_spa']);
                        $register->setSpa($roActionOwner);
                        $register->setStatus($formItem['as_status']);
                        if (strpos($register->getStatusTag(), 'request') === false) {
                            $register->setStatusTag($register->getTagChoices()['request']['name']);
                        }
                        $em->persist($register);
                        $em->flush();
                        $entityRequest = $requestsManager->add($register, null, 'new');
                        $entityRequest->setRequestSource("new_anomaly");
                        $entityRequest->setRequestBatch($batchId);
                        $entityRequest->setApprovalLevel(1);
                        $em->persist($entityRequest);
                        $em->flush();
                    }
                }


                $this->addFlash('success', 'Anomaly Created and waiting for approval.');

                return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
            }else{
                $this->addFlash('warning', 'Some required fields are empty or invalid.');

                return $this->redirect($this->generateUrl($this->registrarRouteBase . '_new', ['projectId' => $projectId]));
            }


        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
        ];
    }

    protected function setNewEntityFields(&$entity, $form)
    {

        $data = $form->getData();

        //if defectOrientationHours && defectOrientationMinutes are set
        $defectOrientationHours = $form['anomaly_group']['defect_orientation_group']['defectOrientationHours']->getData(
        );
        $defectOrientationMinutes = $form['anomaly_group']['defect_orientation_group']['defectOrientationMinutes']->getData(
        );

        if ($defectOrientationHours !== null && $defectOrientationMinutes !== null) {
            $defectOrientation = new \DateTime();
            $defectOrientation->setTime($defectOrientationHours, $defectOrientationMinutes);
            $entity->setDefectOrientation($defectOrientation);
        }

        if ($entity->getStatus() && $entity->getStatusChoices()[$entity->getStatus()] == 'Live') {
            $entity->setCloseOutDate(null);
            $entity->setCloseOutJustification(null);
        }
    }

    /**
     * Creates a form to create a AnomalyRegistrar entity.
     *
     * @param AnomalyRegistrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($projectId, AnomalyRegistrar $entity)
    {
        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity->setProject($project);
        $actionOwners = $project->getActionOwners()->toArray();

        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);
        if (count($actionOwners) > 0) {
            if (!$entity->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project' => $projectId));
                $userActionId = $userAction->getId();
            } else {
                $userActionId = $entity->getSpaId();
            }
        }else{
            $userActionId=null;
        }
        $form = $this->createForm(
            new $this->registrarType($entity, $em, $actionOwners,$userActionId),
            $entity,
            [
                'action' => $this->generateUrl($this->registrarRouteBase.'_create', ['projectId' => $projectId]),
                'method' => 'POST',
            ]
        );
        $this->createROForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        $this->createTROForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        $this->createFMForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        $this->createASForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        $appLevel = $entity->getProject()->getProjectApprovalLevels();
        $form->add('submit', 'submit', $this->options(['label' => (
        count($appLevel) <= 1 ? 'Submit for Approval' : 'Submit for Review'
        ), 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    private function createCriticalityOrRisk(& $form, $pre, $project, $statusChoices, $actionOwners, $action=null){
        $form
            ->add($pre. '_status', 'choice', $this->options([
                    'label'=>'Status',
                    'data'=>($action==null?null:$action->getStatus()),
                    'required' => true, 'mapped'=>false, 'choices' => $statusChoices
                ]
            ))
            ->add(
                $pre. '_spa',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionOwners',
                        'property' => 'user.username',
                        'required' => true,
                        'data'=>($action==null?null:$action->getSpa()),
                        'label' => 'Action Owner',
                        'mapped' => false,
                        'choices' => $actionOwners
                    ]
                )
            )
        ;
    }

    private function createROForm(& $form, $project, $statusChoices, $actionOwners, $action=null){
        $form->add(
            'ro_expiryDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
            $this->options(['label'       => 'Technical Expiry Date',
                'required'    => true,
                'mapped'=>false,
                'constraints' => [],
                'data'=>($action==null?null:($action->getExpiryDate()==null?null:$action->getExpiryDate()->format('j M Y'))),
                'attr'        => ['class' => 'inspection-date required']], 'date_picker')
        )->add('ro_recommendations', 'textarea',
            $this->options(['label' => 'Recommendations', 'required' => false,
                'data'=>($action==null?null:$action->getRecommendations()),
                'mapped'=>false]));
        $form->add('ro_description', 'text',
            $this->options([
                'label'       => 'Repair Order Description',
                'attr'        => ['class' => 'required'],
                'mapped'=>false,
                'data'=>($action==null?null:$action->getDescription()),
                'required'    => true,
                'constraints' => [],]))
            ->add('ro_locationDetails', 'text',
                $this->options([
                    'label'       => 'Location Details',
                    'attr'        => ['class' => ''],
                    'mapped'=>false,
                    'data'=>($action==null?null:$action->getLocationDetails()),
                    'required'    => false,
                    'constraints' => [],]))
            ->add('ro_repairPriority', 'text',
                $this->options([
                    'label'       => 'Repair Priority',
                    'attr'        => ['class' => ''],
                    'data'=>($action==null?null:$action->getRepairPriority()),
                    'mapped'=>false,
                    'required'    => false,
                    'constraints' => [],]))
            ->add('ro_closeOutDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
                $this->options(['label' => 'Close Out Date',
                    'mapped'=>false,
                    'data'=>($action==null?null:($action->getCloseOutDate()==null?null:$action->getCloseOutDate()->format('j M Y'))),
                ], 'date_picker'))
            ->add('ro_closeOutJustification', 'textarea',
                $this->options(['label'       => 'Close Out Justification',
                    'required'    => false,
                    'mapped'=>false,
                    'data'=>($action==null?null:$action->getCloseOutJustification()),
                    'constraints' => new Callback([
                        'callback' => function ($object, $context) {
                            $form = $context->getRoot();
                            $data = $form->get('ro_closeOutDate');
                            // if $data is null then the field was blank, do nothing more
                            if (is_null($data)) return;

                            $closeOutDate = $data->getData();

                            if ($closeOutDate !== null && $object === null) {
                                $form->addError(new FormError('You should fill close out justification!'));
                            }
                        }
                    ]),
                ]));
        $this->createCriticalityOrRisk($form,"ro",$project, $statusChoices, $actionOwners,$action);

    }

    private function createTROForm(& $form, $project, $statusChoices, $actionOwners,$action=null){
        $form->add(
            'tro_expiryDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
            $this->options(['label'       => 'Technical Expiry Date',
                'required'    => true,
                'data'=>($action==null?null:($action->getExpiryDate()==null?null:$action->getExpiryDate()->format('j M Y'))),
                'mapped'=>false,
                'constraints' => [],
                'attr'        => ['class' => 'inspection-date required']], 'date_picker')
        )->add('tro_recommendations', 'textarea',
            $this->options(['label' => 'Recommendations', 'required' => false,
                'data'=>($action==null?null:($action->getRecommendations())),
                'mapped'=>false]));
        $form->add('tro_repairType', 'text',
            $this->options([
                'label'       => 'Temporary Repair Type',
                'data'=>($action==null?null:($action->getRepairType())),
                'attr'        => ['class' => 'required'],
                'mapped'=>false,
                'required'    => true,
                'constraints' => [],]))
            ->add('tro_designLife', 'number',
                $this->options([
                    'label'       => 'Design Life',
                    'mapped'=>false,
                    'data'=>($action==null?null:($action->getDesignLife())),
                    'attr'        => ['class' => 'required', 'next_group_addon' => 'months'],
                    'required'    => true,
                    'constraints' => [new Type(['type' => 'double'])]]))
            ->add('tro_mocNumber', 'text',
                $this->options([
                    'label'       => 'MOC Number',
                    'data'=>($action==null?null:($action->getMocNumber())),
                    'mapped'=>false,
                    'attr'        => ['class' => ''],
                    'required'    => false]))
            ->add('tro_installationDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
                $this->options(['label' => 'Installation Date',
                    'data'=>($action==null?null:($action->getInstallationDate()==null?null:$action->getInstallationDate()->format('j M Y'))),
                    'mapped'=>false,
                    'attr'        => ['class' => 'installation-date-field'],
                ], 'date_picker'))
            ->add('tro_inspectionTechnique', 'text',
                $this->options([
                    'label'       => 'Latest Inspection Technique',
                    'data'=>($action==null?null:($action->setInspectionTechnique())),
                    'mapped'=>false,
                    'attr'        => ['class' => ''],
                    'required'    => false]))
            ->add('tro_inspectionDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
                $this->options(['label' => 'Latest Inspection Date',
                    'data'=>($action==null?null:($action->getInspectionDate()==null?null:$action->getInspectionDate()->format('j M Y'))),
                    'mapped'=>false,
                    'required'    => false,
                ], 'date_picker'))
            ->add('tro_inspectionFrequency', 'number',
                $this->options([
                    'label'       => 'Inspection Frequency',
                    'data'=>($action==null?null:($action->getInspectionFrequency())),
                    'mapped'=>false,
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'double'])],
                    'attr'        => ['class' => 'number', 'next_group_addon' => 'months']]))
            ->add('tro_inspectionResults', 'textarea',
                $this->options([
                    'label'    => 'Inspection Results',
                    'data'=>($action==null?null:($action->getInspectionResults())),
                    'required' => false,
                    'mapped'=>false,
                    'attr'     => ['class' => '']]))
            ->add('tro_bufferDays', 'integer',
                $this->options([
                    'label'       => 'Buffer Days',
                    'data'=>($action==null?null:($action->getBufferDays())),
                    'required'    => false,
                    'mapped'=>false,
                    'constraints' => [new Type(['type' => 'integer'])],
                    'attr'        => ['class' => 'integer']]))
        ;
        $this->createCriticalityOrRisk($form,"tro",$project, $statusChoices, $actionOwners,$action);

    }

    private function createFMForm(& $form, $project, $statusChoices, $actionOwners,$action=null){
        $form->add(
            'fm_expiryDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
            $this->options(['label'       => 'Technical Expiry Date',
                'required'    => true,
                'data'=>($action==null?null:($action->getExpiryDate()==null?null:$action->getExpiryDate()->format('j M Y'))),
                'mapped'=>false,
                'constraints' => [],
                'attr'        => ['class' => 'inspection-date required']], 'date_picker')
        )->add('fm_recommendations', 'textarea',
            $this->options(['label' => 'Recommendations', 'required' => false,
                'data'=>($action==null?null:($action->getRecommendations())),
                'mapped'=>false]));
        $form->add('fm_description', 'text',
            $this->options([
                'label'       => 'Fabric Maintenance Description',
                'mapped'=>false,
                'data'=>($action==null?null:($action->getDescription())),
                'attr'        => ['class' => 'required'],
                'required'    => true,
                'constraints' => [],]))
            ->add('fm_immediateRepair', 'choice', $this->options([
                'label'    => 'Immediate Repair',
                'data'=>($action==null?null:($action->getImmediateRepair())),
                'mapped'=>false,
                'choices'  => [0 => 'No', 1 => 'Yes'],
                'required' => false,
            ]))
            ->add('fm_longIsolation', 'choice', $this->options([
                'label'    => 'Long Isolation',
                'data'=>($action==null?null:($action->getLongIsolation())),
                'mapped'=>false,
                'choices'  => [0 => 'No', 1 => 'Yes'],
                'required' => false,
            ]))
            ->add('fm_inService', 'number',
                $this->options([
                    'label'       => 'In-Service',
                    'data'=>($action==null?null:($action->getInService())),
                    'mapped'=>false,
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'double'])],
                    'attr'        => ['class' => 'number', 'next_group_addon' => 'months']]))
            ->add('fm_completionDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
                $this->options(['label' => 'Completion Date',
                    'mapped'=>false,
                    'data'=>($action==null?null:($action->getCompletionDate()==null?null:$action->getCompletionDate()->format('j M Y'))),
                ], 'date_picker'));
        $this->createCriticalityOrRisk($form,"fm",$project, $statusChoices, $actionOwners);

    }

    private function createASForm(& $form, $project, $statusChoices, $actionOwners,$action=null){
        $form->add(
            'as_expiryDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
            $this->options(['label'       => 'Technical Expiry Date',
                'required'    => true,
                'mapped'=>false,
                'data'=>($action==null?null:($action->getExpiryDate()==null?null:$action->getExpiryDate()->format('j M Y'))),
                'constraints' => [],
                'attr'        => ['class' => 'inspection-date required']], 'date_picker')
        )->add('as_recommendations', 'textarea',
            $this->options(['label' => 'Recommendations', 'required' => false,
                'data'=>($action==null?null:($action->getRecommendations())),
                'mapped'=>false]));
        $form->add('as_assessmentDueDate', new \AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType(),
            $this->options(['label'       => 'Assessment Completion Date',
                'data'=>($action==null?null:($action->getAssessmentDueDate()==null?null:$action->getAssessmentDueDate()->format('j M Y'))),
                'required'    => false,
                'mapped'=>false,
                'constraints' => [],
                'attr'        => ['class' => 'inspection-date']], 'date_picker')
        )
            ->add('as_assessmentType', 'text',
                $this->options([
                    'label'       => 'Assessment Type',
                    'data'=>($action==null?null:($action->getAssessmentType())),
                    'mapped'=>false,
                    'attr'        => [],
                    'required'    => false,
                    'constraints' => [],]))
            ->add('as_assessmentCode', 'text',
                $this->options([
                    'label'       => 'Assessment Code',
                    'data'=>($action==null?null:($action->getAssessmentCode())),
                    'mapped'=>false,
                    'attr'        => [],
                    'required'    => false,
                    'constraints' => [],]))
            ->add('as_assessmentResult', 'text',
                $this->options([
                    'label'       => 'Assessment Result',
                    'data'=>($action==null?null:($action->getAssessmentResult())),
                    'mapped'=>false,
                    'attr'        => [],
                    'required'    => false,
                    'constraints' => [],]))
            ->add('as_assessmentBufferDays', 'number',
                $this->options([
                    'label'       => 'Buffer Days',
                    'data'=>($action==null?null:($action->getAssessmentBufferDays())),
                    'mapped'=>false,
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'double'])],
                    'attr'        => ['class' => 'number', 'next_group_addon' => 'days']]));
        $this->createCriticalityOrRisk($form,"as",$project, $statusChoices, $actionOwners,$action);

    }


    public function newAction($projectId)
    {
        $entity = new $this->registrarClass();
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        if (!$this->securityHelper->isRoleGranted('ROLE_' . $this->items[$entity->getCode()] . '_ANOMALY_NEW', $this->userRolesMergeToCheck)) {
            $this->addFlash('error', 'Permission to create new ' . $this->items[$entity->getCode()] . ' anomaly is denied');
            $referer = $this->getRequest()->headers->get('referer');
            if ($referer == NULL) {
                return $this->redirect($this->generateURL('anomaly_master', array('projectId' => $projectId)));
            } else {
                return $this->redirect($referer);
            }
        }
        if ($project->getIsSetup()['isSetup']){
            $usr = $this->get('security.context')->getToken()->getUser();
            $actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneByUserId($usr->getId());
            if(!$actionOwner){
                $this->addFlash('error', 'You don\'t have permission to send requests!');
                return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
            }
            $form = $this->createCreateForm($projectId, $entity);
            return [
                'entity' => $entity,
                'form' => $form->createView(),
                'projectId' => $projectId,
                'project' => $project
            ];
        }
        return [
            'project' => $project
        ];
    }

    public function visualSummaryAction($projectId, Request $request)
    {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }


        $data1 = []; // for first plot, points
        $data2 = []; // for first plot, curves
        $data3 = []; // for second plot, points

        //apply stats to anomalies
        $anomalies = array_map(
            function (AnomalyRegistrar $anomaly) use (&$data1, &$data2, &$data3) {
                return $anomaly;
            },
            $entity->getRegistrars()->toArray()
        );


        return [
            'entity' => $entity,
            'anomalies' => $anomalies,
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3,
            'projectId' => $projectId,
        ];
    }

    public function showAction($projectId, $id)
    {
        $em = $this->getManager();

        $editAnomalyVisible = 0;

        $duplicateAnomalyVisible = 0;

        /** @var AnomalyRegistrar $entity */
        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }

        if ($entity instanceof PipelineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_SHOW', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to show pipeline anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show pipeline anomalies."));

        }
        if ($entity instanceof PipingAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_SHOW', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to show piping anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show piping anomalies."));
        }
        if ($entity instanceof StructureAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_SHOW', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to show structure anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show structure anomalies."));
        }
        if ($entity instanceof StaticEquipmentAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_SHOW', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to show static equipment anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show static equipment anomalies."));
        }
        if ($entity instanceof MarineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_SHOW', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to show marine anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show marine anomalies."));
        }


        if ($entity instanceof PipelineAnomaly && ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_EDIT', $this->userRolesMergeToCheck))) {
            $editAnomalyVisible = 1;
        }
        if ($entity instanceof PipingAnomaly && ($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_EDIT', $this->userRolesMergeToCheck))) {
            $editAnomalyVisible = 1;
        }
        if ($entity instanceof StructureAnomaly && ($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_EDIT', $this->userRolesMergeToCheck))) {
            $editAnomalyVisible = 1;
        }
        if ($entity instanceof StaticEquipmentAnomaly && ($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_EDIT', $this->userRolesMergeToCheck))) {
            $editAnomalyVisible = 1;
        }
        if ($entity instanceof MarineAnomaly && ($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_EDIT', $this->userRolesMergeToCheck))) {
            $editAnomalyVisible = 1;
        }

        if ($entity instanceof PipelineAnomaly && ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
            $duplicateAnomalyVisible = 1;
        }
        if ($entity instanceof PipingAnomaly && ($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
            $duplicateAnomalyVisible = 1;
        }
        if ($entity instanceof StructureAnomaly && ($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
            $duplicateAnomalyVisible = 1;
        }
        if ($entity instanceof StaticEquipmentAnomaly && ($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
            $duplicateAnomalyVisible = 1;
        }
        if ($entity instanceof MarineAnomaly && ($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
            $duplicateAnomalyVisible = 1;
        }

        $plot_data1 = []; //ploting data
        $lines = [];

        //conditions for plotting
        //check if internal or external metal loss exists
        $has_ml = ($entity->getClassification()->getCode() == 'EML' ||
            $entity->getClassification()->getCode() == 'IML');


        $nwt = $entity->getNominalWT();
        $conc = 0.85 * $nwt - $entity->getCorrosionAllowance();
        $mawt = $entity->getMAWT();

        $monitors = $entity->getMonitors();
        //make sure at least 2 monitors exist for the anomaly
        $has_plot = $has_ml && count($monitors) >= 2;
        if ($has_plot) {

            for ($i = 0; $i < count($monitors); $i++) { // for individual points
                //x, y
                $plot_data1[] = [$monitors[$i]->getInspectionDate(), $monitors[$i]->getRemainingWallThickness(), 0];
            }

            for ($i = 1; $i < count($monitors); $i++) { // for CR points
                $nm1_date = $monitors[$i - 1]->getInspectionDate();
                $datetime1 = $monitors[$i]->getInspectionDate(); //y2
                $datetime2 = $nm1_date;

                $interval = $datetime1->diff($datetime2); // no absolute , true);
                $date_time_diff = (int)$interval->format('%a'); //the difference in days
                $date_time_diff = (float)($date_time_diff / 365); // convert it to year


                $cr = 0;
                if ($date_time_diff != 0) {
                    $cr = (
                        ($monitors[$i - 1]->getRemainingWallThickness() - $monitors[$i]->getRemainingWallThickness())
                        /
                        ($date_time_diff)
                    );
                }

//              //y = m*x + b
                //find new predicted date where y = 0
                if ($cr > 0) {
                    //add cr to plot data
                    $plot_data1[$i][2] = $cr;

                    $newDate = clone $datetime1;
                    $add_ratio = (float)$monitors[$i]->getRemainingWallThickness() / $cr;
                    $add_ratio *= 365;
                    $add_ratio = (int)$add_ratio;
                    $newDate->modify("+{$add_ratio} day");

                    $prev_i = ($i - 1);
                    $line_id = "M{$prev_i}-M{$i}";
                    $lines[$line_id] = [
                        [$datetime2, $monitors[$i - 1]->getRemainingWallThickness()], //x, y
                        [$datetime1, $monitors[$i]->getRemainingWallThickness()],
                        [$newDate, 0],
                    ];
                }

            }

        }

        $plot = [
            'has_plot' => $has_plot,
            'plot_data' => $plot_data1,
            'lines' => $lines,
            'nwt' => $nwt,
            'conc' => $conc,
            'mawt' => $mawt,
        ];


        return [
            'entity' => $entity,
            'project' => $entity->getProject(),
            'form' => $this->createShowForm($entity)->createView(),
            'projectId' => $projectId,
            'plot' => $plot,
            'editAnomalyGranted' => $editAnomalyVisible,
            'duplicateAnomalyGranted' => $duplicateAnomalyVisible,
        ];


    }

    public function showDeferralAction($projectId, $id)
    {
        $em = $this->getManager();

        /** @var AnomalyRegistrar $entity */
        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }


        $plot_data1 = []; //ploting data
        $lines = [];

        //conditions for plotting
        //check if internal or external metal loss exists
        $has_ml = ($entity->getClassification()->getCode() == 'EML' ||
            $entity->getClassification()->getCode() == 'IML');


        $nwt = $entity->getNominalWT();
        $conc = 0.85 * $nwt - $entity->getCorrosionAllowance();
        $mawt = $entity->getMAWT();

        $monitors = $entity->getMonitors();
        //make sure at least 2 monitors exist for the anomaly
        $has_plot = $has_ml && count($monitors) >= 2;
        if ($has_plot) {

            for ($i = 0; $i < count($monitors); $i++) { // for individual points
                //x, y
                $plot_data1[] = [$monitors[$i]->getInspectionDate(), $monitors[$i]->getRemainingWallThickness(), 0];
            }

            for ($i = 1; $i < count($monitors); $i++) { // for CR points
                $nm1_date = $monitors[$i - 1]->getInspectionDate();
                $datetime1 = $monitors[$i]->getInspectionDate(); //y2
                $datetime2 = $nm1_date;

                $interval = $datetime1->diff($datetime2); // no absolute , true);
                $date_time_diff = (int)$interval->format('%a'); //the difference in days
                $date_time_diff = (float)($date_time_diff / 365); // convert it to year


                $cr = 0;
                if ($date_time_diff != 0) {
                    $cr = (
                        ($monitors[$i - 1]->getRemainingWallThickness() - $monitors[$i]->getRemainingWallThickness())
                        /
                        ($date_time_diff)
                    );
                }

//              //y = m*x + b
                //find new predicted date where y = 0
                if ($cr > 0) {
                    //add cr to plot data
                    $plot_data1[$i][2] = $cr;

                    $newDate = clone $datetime1;
                    $add_ratio = (float)$monitors[$i]->getRemainingWallThickness() / $cr;
                    $add_ratio *= 365;
                    $add_ratio = (int)$add_ratio;
                    $newDate->modify("+{$add_ratio} day");

                    $prev_i = ($i - 1);
                    $line_id = "M{$prev_i}-M{$i}";
                    $lines[$line_id] = [
                        [$datetime2, $monitors[$i - 1]->getRemainingWallThickness()], //x, y
                        [$datetime1, $monitors[$i]->getRemainingWallThickness()],
                        [$newDate, 0],
                    ];
                }

            }

        }

        $plot = [
            'has_plot' => $has_plot,
            'plot_data' => $plot_data1,
            'lines' => $lines,
            'nwt' => $nwt,
            'conc' => $conc,
            'mawt' => $mawt,
        ];

        return $this->render('AIEAnomalyBundle:AnomalyRegistrar:showdeferral.html.twig', array(
            'entity' => $entity,
            'project' => $entity->getProject(),
            'form' => $this->createShowForm($entity)->createView(),
            'projectId' => $projectId,
            'plot' => $plot,
        ));
    }

    public function editAction($projectId, $id)
    {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }
        foreach ($entity->getRequests() as $request){
            if($request->getStatus()== -1){
                $this->addFlash('error', 'You have to approve any modification request related to this anomaly before applying any modification');
                $referer = $this->getRequest()->headers->get('referer');
                if ($referer == NULL) {
                    return $this->redirect($this->generateURL('anomaly_master', array('projectId' => $projectId)));
                } else {
                    return $this->redirect($referer);
                }
            }
        }
        if (!$this->securityHelper->isRoleGranted('ROLE_' . $this->items[$entity->getCode()] . '_ANOMALY_EDIT', $this->userRolesMergeToCheck)) {
            $this->addFlash('error', 'Permission to edit anomaly is denied');
            $referer = $this->getRequest()->headers->get('referer');
            if ($referer == NULL) {
                return $this->redirect($this->generateURL('anomaly_master', array('projectId' => $projectId)));
            } else {
                return $this->redirect($referer);
            }
        }
        $editForm = $this->createEditForm($projectId, $entity);

        $has_live_actions = count(
                $entity->getActions()->filter(
                    function (ActionRegistrar $action) {
                        return 'Live' == $action->getStatusChoices()[$action->getStatus()];
                    }
                )
            ) > 0;

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId,
            'has_live_actions' => $has_live_actions,
            'project' => $project,

        ];
    }

    protected function createEditForm($projectId, AnomalyRegistrar $entity)
    {
        $em = $this->getManager();
        $project = $entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $form = $this->createForm(
            new $this->registrarType($entity, $em, $actionOwners,$entity->getSpaId()),
            $entity,
            [
                'action' => $this->generateUrl(
                    $this->registrarRouteBase.'_update',
                    ['projectId' => $projectId, 'id' => $entity->getId()]
                ),
                'method' => 'PUT',
            ]
        );
        $appLevel = $entity->getProject()->getProjectApprovalLevels();
        //$this->PrepareRegistersFormToShowAndEdit($entity,$form);
        $form->add('submit', 'submit', $this->options(['label' => (
        count($appLevel) <= 1 ? 'Submit Update for Approval' : 'Submit Update for Review'
        ), 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    public function PrepareRegistersFormToShowAndEdit($entity, &$form){
        $project=$entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $registerIsExist=['RO'=>false,'TRO'=>false,'FM'=>false,'AS'=>false];
        foreach ($entity->getActions() as $action){
            if($action instanceof RepairOrderRegistrar){
                $this->createROForm($form, $project,$entity->getStatusChoices(),$actionOwners, $action);
                $registerIsExist['RO']=true;
            }
            if($action instanceof TemporaryRepairOrderRegistrar){
                $this->createTROForm($form, $project,$entity->getStatusChoices(),$actionOwners, $action);
                $registerIsExist['TRO']=true;
            }
            if($action instanceof FabricMaintenanceRegistrar){
                $this->createFMForm($form, $project,$entity->getStatusChoices(),$actionOwners, $action);
                $registerIsExist['FM']=true;
            }
            if($action instanceof AssessmentRegistrar){
                $this->createASForm($form, $project,$entity->getStatusChoices(),$actionOwners, $action);
                $registerIsExist['AS']=true;
            }
        }
        if (!$registerIsExist['RO'])
            $this->createROForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        if (!$registerIsExist['TRO'])
            $this->createTROForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        if (!$registerIsExist['FM'])
            $this->createFMForm($form, $project,$entity->getStatusChoices(),$actionOwners);
        if (!$registerIsExist['AS'])
            $this->createASForm($form, $project,$entity->getStatusChoices(),$actionOwners);
    }


    public function createShowForm($entity){

        $form=$this->get('aie_anomaly.form.helper')->createShowAnomalyForm($entity);
        //$this->PrepareRegistersFormToShowAndEdit($entity,$form);
        return $form;
    }

    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }

        $project = $entity->getProject();
        $editForm = $this->createEditForm($projectId, $entity);

        $has_live_actions = count(
            $entity->getActions()->filter(
                function (ActionRegistrar $action) {
                    return 'Live' == $action->getStatusChoices()[$action->getStatus()];
                }
            )
        ) > 0;

        $files = $request->files;
        if (isset($files)) {

            $break = false;
            foreach($files as $value) {

                foreach($value['files_group']['files'] as $inneritem) {

                    if (empty($inneritem['files_group']['file'])) {
                        $break = true;
                        break;
                    }

                }

            }

            if ($break==true){
                $error = new FormError("Please attach file[s] if opted to upload documents!");
                $editForm->addError($error);
                return [
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'projectId' => $projectId,
                    'has_live_actions' => $has_live_actions,
                    'project' => $project,
                ];

            }
        }

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $data = $editForm->getData();

            // Status closed and got sub registrar in live status don't allow to save data
            if( ! ($data->getStatus() == 1 && $has_live_actions == 1)) {

                /**
                 * Create the request based on the new data
                 */
                $this->setNewEntityFields($entity, $editForm);
                $requestsManager = $this->get('anomaly_manager.requests');
                $serializer = $this->get('jms_serializer');


                if ($data->getTrackingCriteria()) {
                    foreach ($data->getTrackingCriteria() as $tc) {
                        $tc->setRegistrar($data);
                    }
                }

                if ($data->getFiles()) {
                    foreach ($data->getFiles() as $file) {
                        $_file = $file->getFile();
                        if (!$_file) {
                            continue;
                        }
                        $file->setAnomaly($data);
                        $uploadedPath = $this->uploadFile($_file, $file);
                        $file->setFilepath($uploadedPath);
                        $file->setSize($_file->getSize());
                        $file->setDate(new \DateTime());
                        $em->persist($file);
                    }
                }

                $entityRequest = $requestsManager->add($entity, $serializer->serialize($data, 'json'));
                $entityRequest->setRequestFiles($data->getFiles());
                $entityRequest->setAttachedreportanomalys(json_encode($request->request->get('anomaly_file_edit_check')));
                $entityRequest->setRequestStatus('update');

                $em->persist($entityRequest);
                $em->flush($entityRequest);


                $connection = $em->getConnection();
                $statement = $connection->prepare("UPDATE Registrar SET statusTag = :status where id=:id");
                $statement->bindValue('status', $entity->getTagChoices()['request']['name']);
                $statement->bindValue('id', $entity->getId());
                $statement->execute();

                $em->refresh($entity);

                $this->addFlash('success', 'Anomaly ID ' . $entity->getFullId() . ' Updated and waiting for approval.');

                return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
            }
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId,
            'has_live_actions' => $has_live_actions,
            'project' => $project,
        ];
    }

    public function deleteAction(Request $request, $projectId, $id)
    {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Anomaly Registrar entity.');
//                return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Not found','message' => "Unable to find Anomaly Registrar entity."));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl($this->registrarRouteBase,['projectId' => $projectId]));
    }

    protected function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($this->registrarRouteBase.'_delete', ['id' => $id,'projectId' => $projectId]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete','attr' => array('class' => 'btn-danger pull-right')])
            ->getForm();
    }

    public function duplicateAction($projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);
        //echo count($entity->getActions());
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }

        if ($entity instanceof PipelineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to duplicate pipeline anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to duplicate pipeline anomalies."));

        }
        if ($entity instanceof PipingAnomaly && !($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to duplicate piping anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to duplicate piping anomalies."));
        }
        if ($entity instanceof StructureAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to duplicate structure anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to duplicate structure anomalies."));
        }

        if ($entity instanceof StaticEquipmentAnomaly && !($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to duplicate static equipment anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to duplicate static equipment anomalies."));
        }
        if ($entity instanceof MarineAnomaly && !($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_DUPLICATE', $this->userRolesMergeToCheck))) {
//            throw new \Exception("You don't have permission to duplicate marine anomalies!");
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to duplicate marine anomalies."));
        }



        $cloneForm = $this->createCloneForm($projectId, $entity, $id);
        $project = $entity->getProject();

        return [
            'entity' => $entity,
            'edit_form' => $cloneForm->createView(),
            'projectId' => $projectId,
            'project' => $project
        ];
    }

    public function cloneAction($projectId, Request $request, $id)
    {

        $em = $this->getManager();
        $mainEntity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);
        if (!$mainEntity) {
            throw $this->createNotFoundException('Unable to find AnomalyRegistrar entity.');
        }


        $serializer = $this->get('jms_serializer');
        $mainEntityData = $serializer->serialize($mainEntity, 'json');
        $project = $mainEntity->getProject();

        $editForm = $this->createCloneForm($projectId, $mainEntity, $id);
        $editForm->handleRequest($request);

        $usr = $this->get('security.context')->getToken()->getUser();


        $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneByActionOwner($usr->getId());

        if ($editForm->isValid() && $editForm->isSubmitted()) {

            $data = $editForm->getData();
            $currentDate = new \DateTime();
            $batchId = $currentDate->format('YmdHis') . str_pad(rand(0, pow(10, 4)-1), 4, '0', STR_PAD_LEFT);;

            if ($this->registrarClassName == "PipelineAnomaly") {
                $entity = new PipelineAnomaly();
            }
            if ($this->registrarClassName == "PipingAnomaly") {
                $entity = new PipingAnomaly();
            }
            if ($this->registrarClassName == "MarineAnomaly") {
                $entity = new MarineAnomaly();
            }
            if ($this->registrarClassName == "StructureAnomaly") {
                $entity = new StructureAnomaly();
            }
            if ($this->registrarClassName == "StaticEquipmentAnomaly") {
                $entity = new StaticEquipmentAnomaly();
            }
            $oldReflection = new \ReflectionObject($mainEntity);
            $newReflection = new \ReflectionObject($entity);
            foreach ($oldReflection->getProperties() as $property)
            {
                if ($newReflection->hasProperty($property->getName()))
                {
                    $newProperty = $newReflection->getProperty($property->getName());
                    $property->setAccessible(true);
                    $newProperty->setAccessible(true);
                    $newProperty->setValue($entity, $property->getValue($mainEntity));
                }
                $entity->setProject($project);
                $actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($editForm->getData()->getSpa()->getId());
                $entity->setProject($project);
                $entity->setSpa($actionOwner);

                $entity->setApprovedspa($approvalOwner);
                $entity->setStatus(0);
                $entity->setStatusTag($entity->getTagChoices()['request']['name']);
                $this->setNewEntityFields($entity, $editForm);

                $entity->setDeferralJustification($editForm->getData()->getDeferralJustification());
                $entity->setDeferred(0);
                $entity->setProductionCriticality($editForm->getData()->getProductionCriticality());
                $entity->setRemediationRequireShutdown($editForm->getData()->getRemediationRequireShutdown());
                $entity->setProbability($editForm->getData()->getProbability());
                $entity->setCriticality($editForm->getData()->getCriticality());
                $entity->setConsequence($editForm->getData()->getConsequence());
                $entity->setDeferralSafeJustification($editForm->getData()->getDeferralSafeJustification());

                $entity->setThreat($editForm->getData()->getThreat());
                $entity->setClassification($editForm->getData()->getClassification());
                $entity->setDesignCode($editForm->getData()->getDesignCode());
                $entity->setDrawingNumber($editForm->getData()->getDrawingNumber());
                $entity->setDefectDescription($editForm->getData()->getDefectDescription());
                $entity->setAnomalyCustomId($editForm->getData()->getAnomalyCustomId());
                $entity->setRaisedOn($editForm->getData()->getRaisedOn());
                $entity->setActionRequired($editForm->getData()->getActionRequired());
                $entity->setInspectionFrequency($editForm->getData()->getInspectionFrequency());
                $entity->setInspectionTechnique($editForm->getData()->getInspectionTechnique());
                $entity->setInspectionDate($editForm->getData()->getInspectionDate());
                $entity->setInspectionRecommendations($editForm->getData()->getInspectionRecommendations());
                $entity->setAssetTagNumber($editForm->getData()->getAssetTagNumber());
                $entity->setCorrosionAllowance($editForm->getData()->getCorrosionAllowance());
                $entity->setBufferDays($editForm->getData()->getBufferDays());

                $entity->setComments($editForm->getData()->getComments());
                $entity->setLocation($editForm->getData()->getLocation());
                $entity->setRetiralValue($editForm->getData()->getRetiralValue());
                $entity->setSummary($editForm->getData()->getSummary());
                $entity->setFailureSummary($editForm->getData()->getFailureSummary());
                $entity->setRecommendations($editForm->getData()->getRecommendations());
                $entity->setInspectionResults($editForm->getData()->getInspectionResults());
                $entity->setComponent($editForm->getData()->getComponent());
                $entity->setLocationPoint($editForm->getData()->getLocationPoint());
                $entity->setNominalWT($editForm->getData()->getNominalWT());
                $entity->setLength($editForm->getData()->getLength());
                $entity->setRemainingWallThickness($editForm->getData()->getRemainingWallThickness());
                $entity->setMAWT($editForm->getData()->getMAWT());
                $entity->setWidth($editForm->getData()->getWidth());
                $entity->setDefectOrientation($editForm->getData()->getDefectOrientation());
                $entity->setCorrosionRate($editForm->getData()->getCorrosionRate());
                if ($mainEntity->getCode() != "PL") $entity->setSystem($editForm->getData()->getSystem());
                if ($mainEntity->getCode() == "PL" || $mainEntity->getCode() == "PI" || $mainEntity->getCode() == "SE") $entity->setSMTS($editForm->getData()->getSMTS());
                if ($mainEntity->getCode() == "PL" || $mainEntity->getCode() == "PI" || $mainEntity->getCode() == "SE") $entity->setSMYS($editForm->getData()->getSMYS());
                if ($mainEntity->getCode() == "PL") $entity->setGpsE($editForm->getData()->getGpsE());
                if ($mainEntity->getCode() == "PL") $entity->setGpsN($editForm->getData()->getGpsN());
                if ($mainEntity->getCode() == "PL" || $mainEntity->getCode() == "PI") $entity->setOuterDiameter($editForm->getData()->getOuterDiameter());
                if ($mainEntity->getCode() == "PL") $entity->setTolerance($editForm->getData()->getTolerance());


                if ($mainEntity->getTrackingCriteria())
                {
                    foreach ($mainEntity->getTrackingCriteria() as $tc)
                    {
                        $newtc = new TrackingCriteria();
                        $oldReflectionTc = new \ReflectionObject($tc);
                        $newReflectionTc = new \ReflectionObject($newtc);
                        foreach ($oldReflectionTc->getProperties() as $property)
                        {
                            if ($newReflectionTc->hasProperty($property->getName()))
                            {
                                $newProperty = $newReflectionTc->getProperty($property->getName());
                                $property->setAccessible(true);
                                $newProperty->setAccessible(true);
                                $newProperty->setValue($newtc, $property->getValue($tc));
                            }
                            $newtc->setRegistrar($entity);
                            $em->persist($newtc);
                        }
                    }
                }
                if ($mainEntity->getFiles()) {
                    $files = $em->getRepository('AIEAnomalyBundle:Files')->findByAnomaly($mainEntity->getId());
                    foreach ($data->getFiles() as $file) {
                        $newfile = new Files();
                        $oldReflectionFile = new \ReflectionObject($file);
                        $newReflectionFile = new \ReflectionObject($newfile);
                        foreach ($oldReflectionFile->getProperties() as $property) {
                            if ($newReflectionFile->hasProperty($property->getName())) {
                                $newProperty = $newReflectionFile->getProperty($property->getName());
                                $property->setAccessible(true);
                                $newProperty->setAccessible(true);
                                $newProperty->setValue($newfile, $property->getValue($file));
                            }
                        }
                    }
                }


                $em->persist($entity);
                $em->flush();

                $requestsManager = $this->get('anomaly_manager.requests');
                $entityRequest = $requestsManager->add($entity,null,'new');
                $entityRequest->setRequestFiles($data->getFiles());
                $entityRequest->setRequestSource("new_anomaly");
                $entityRequest->setRequestBatch($batchId);
                $entityRequest->setApprovalLevel(1);
                $em->persist($entityRequest);
                $em->flush();

                if ($mainEntity->getActions())
                {
                    foreach ($mainEntity->getActions() as $actionAnomaly)
                    {
                        if ($actionAnomaly instanceof RepairOrderRegistrar){
                            $register = new RepairOrderRegistrar();
                            $register->setProject($project);
                            $register->setAnomaly($entity);
                            $register->setExpiryDate($actionAnomaly->getExpiryDate());
                            $register->setRecommendations($actionAnomaly->getRecommendations());
                            $register->setDescription($actionAnomaly->getDescription());
                            $register->setLocationDetails($actionAnomaly->getLocationDetails());
                            $register->setRepairPriority($actionAnomaly->getRepairPriority());
                            $register->setCloseOutDate($actionAnomaly->getCloseOutDate());
                            $register->setCloseOutJustification($actionAnomaly->getCloseOutJustification());
                            $register->setSpa($actionAnomaly->getSpa());
                            $register->setStatus($actionAnomaly->getStatus());
                            $register->setStatusTag($actionAnomaly->getStatusTag());
                            $em->persist($register);
                            $em->flush();
                            $entityRequest = $requestsManager->add($register, null, 'new');
                            $entityRequest->setRequestSource("new_anomaly");
                            $entityRequest->setRequestBatch($batchId);
                            $entityRequest->setApprovalLevel(1);
                            $em->persist($entityRequest);
                            $em->flush();
                        }
                        if ($actionAnomaly instanceof TemporaryRepairOrderRegistrar){
                            $register = new TemporaryRepairOrderRegistrar();
                            $register->setProject($project);
                            $register->setAnomaly($entity);
                            $register->setExpiryDate($actionAnomaly->getExpiryDate());
                            $register->setRecommendations($actionAnomaly->getRecommendations());
                            $register->setRepairType($actionAnomaly->getRepairType());
                            $register->setDesignLife($actionAnomaly->getDesignLife());
                            $register->setMocNumber($actionAnomaly->getMocNumber());
                            $register->setInstallationDate($actionAnomaly->getInstallationDate());
                            $register->setInspectionTechnique($actionAnomaly->getInspectionTechnique());
                            $register->setInspectionDate($actionAnomaly->getInspectionDate());
                            $register->setInspectionFrequency($actionAnomaly->getInspectionFrequency());
                            $register->setInspectionResults($actionAnomaly->getInspectionResults());
                            $register->setBufferDays($actionAnomaly->getBufferDays());
                            $register->setSpa($actionAnomaly->getSpa());
                            $register->setStatus($actionAnomaly->getStatus());
                            $register->setStatusTag($actionAnomaly->getStatusTag());
                            $em->persist($register);
                            $em->flush();
                            $entityRequest = $requestsManager->add($register, null, 'new');
                            $entityRequest->setRequestSource("new_anomaly");
                            $entityRequest->setRequestBatch($batchId);
                            $entityRequest->setApprovalLevel(1);
                            $em->persist($entityRequest);
                            $em->flush();
                        }
                        if ($actionAnomaly instanceof FabricMaintenanceRegistrar){
                            $register = new FabricMaintenanceRegistrar();
                            $register->setProject($project);
                            $register->setAnomaly($entity);
                            $register->setExpiryDate($actionAnomaly->getExpiryDate());
                            $register->setRecommendations($actionAnomaly->getRecommendations());
                            $register->setDescription($actionAnomaly->getDescription());
                            $register->setImmediateRepair($actionAnomaly->getImmediateRepair());
                            $register->setLongIsolation($actionAnomaly->getLongIsolation());
                            $register->setInService($actionAnomaly->getInService());
                            $register->setCompletionDate($actionAnomaly->getCompletionDate());
                            $register->setSpa($actionAnomaly->getSpa());
                            $register->setStatus($actionAnomaly->getStatus());
                            $register->setStatusTag($actionAnomaly->getStatusTag());
                            $em->persist($register);
                            $em->flush();
                            $entityRequest = $requestsManager->add($register, null, 'new');
                            $entityRequest->setRequestSource("new_anomaly");
                            $entityRequest->setRequestBatch($batchId);
                            $entityRequest->setApprovalLevel(1);
                            $em->persist($entityRequest);
                            $em->flush();
                        }
                        if ($actionAnomaly instanceof AssessmentRegistrar){
                            $register = new AssessmentRegistrar();
                            $register->setProject($project);
                            $register->setAnomaly($entity);
                            $register->setExpiryDate($actionAnomaly->getExpiryDate());
                            $register->setRecommendations($actionAnomaly->getRecommendations());
                            $register->setAssessmentDueDate($actionAnomaly->getAssessmentDueDate());
                            $register->setAssessmentType($actionAnomaly->getAssessmentType());
                            $register->setAssessmentCode($actionAnomaly->getAssessmentCode());
                            $register->setAssessmentResult($actionAnomaly->getAssessmentResult());
                            $register->setAssessmentBufferDays($actionAnomaly->getAssessmentBufferDays());
                            $register->setSpa($actionAnomaly->getSpa());
                            $register->setStatus($actionAnomaly->getStatus());
                            $register->setStatusTag($actionAnomaly->getStatusTag());
                            $em->persist($register);
                            $em->flush();
                            $entityRequest = $requestsManager->add($register, null, 'new');
                            $entityRequest->setRequestSource("new_anomaly");
                            $entityRequest->setRequestBatch($batchId);
                            $entityRequest->setApprovalLevel(1);
                            $em->persist($entityRequest);
                            $em->flush();
                        }
                    }
                }


                $this->addFlash('success', 'Anomaly Id '.$mainEntity->getFullId().' was duplicated and new Anomaly Id :'. $entity->getFullId());
                return $this->redirect($this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId]));
            }
        }

        return [
            'entity' => $mainEntity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId,
            'project' => $project,
        ];
    }


    /**
     * Creates a form to edit a AnomalyRegistrar entity.
     *
     * @param AnomalyRegistrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCloneForm($projectId, AnomalyRegistrar $entity, $id)
    {

        $em = $this->getManager();
        $project = $entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $this->addFlash('info', 'Duplication Mode: '.$entity->getFullId());
        if (count($actionOwners) > 0) {
            if (!$entity->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project' => $projectId));
                $userActionId = $userAction->getId();
            } else {
                $userActionId = $entity->getSpaId();
            }
        }else{
            $userActionId=null;
        }
        $form = $this->createForm(
            new $this->registrarType($entity, $em, $actionOwners, $userActionId),
            $entity,
            [
                'action' => $this->generateUrl(
                    $this->registrarRouteBase.'_clone',
                    ['projectId' => $projectId, 'id' => $entity->getId()]
                ),
                'method' => 'POST',
            ]
        );
        $form->add('cancel', 'submit', $this->options(['label' => 'Cancel', 'attr' => ['class' => 'left']], 'btn'));
        $form->add('submit', 'submit', $this->options(['label' => 'Duplicate', 'attr' => ['class' => 'right']], 'btn'));

        return $form;

    }
}
