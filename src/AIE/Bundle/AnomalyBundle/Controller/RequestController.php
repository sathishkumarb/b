<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionMonitor;
use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Monitor;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Request as AnomalyRequest;
use AIE\Bundle\AnomalyBundle\Form\RequestType;
use AIE\Bundle\AnomalyBundle\Form\RequestBatchType;
use AIE\Bundle\AnomalyBundle\Form\PipelineAnomalyType;
use AIE\Bundle\AnomalyBundle\Form\PipingAnomalyType;
use AIE\Bundle\AnomalyBundle\Form\StructureAnomalyType;
use AIE\Bundle\AnomalyBundle\Form\StaticEquipmentAnomalyType;
use AIE\Bundle\AnomalyBundle\Form\RepairOrderRegistrarType;
use AIE\Bundle\AnomalyBundle\Form\FabricMaintenanceRegistrarType;
use AIE\Bundle\AnomalyBundle\Form\AssessmentRegistrarType;
use AIE\Bundle\AnomalyBundle\Form\TemporaryRepairOrderRegistrarType;
use Doctrine\Common\Collections\ArrayCollection;
use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;
use AIE\Bundle\AnomalyBundle\Helper\WorkpackHelper;
use AIE\Bundle\AnomalyBundle\Helper\WorkpackCustomHelper;
use AIE\Bundle\AnomalyBundle\Helper\PDFReportHelper;
use AIE\Bundle\AnomalyBundle\Helper\PDFSummaryReportHelper;

use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Request controller.
 *
 * @Route("/requests")
 */
class RequestController extends AnomalyBaseController
{

    /**
     * Lists all Request entities.
     *
     * @Route("/", name="anomaly_requests")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $securityContext = $this->get('security.context');
        $token = $securityContext->getToken();
        $user = $token->getUser();
        $userId = $user->getId();

        $reqEntitiesApprovalOwners = $this->mergeUserRequests('A',$userId);
        $reqEntitiesApprovalOwners = $this->array_sort($reqEntitiesApprovalOwners,'id',SORT_DESC);
        $reqEntitiesDeferralOwners = $this->mergeUserRequests('D',$userId);
        $reqEntitiesDeferralOwners = $this->array_sort($reqEntitiesDeferralOwners,'id',SORT_DESC);

        $counts=['ia'=>0,'na'=>0,'da'=>0,'ls'=>0];
        $individualEntities = [];
        $newAnomalyEntities = [];
        $loadsheetEntities = [];
        foreach ($reqEntitiesApprovalOwners as $entity){
            if ($entity['request_source']==null) {
                $requestType = 'Individual Approval';
                $individualEntities[$entity['anomalyid']] = ['assetTagNumber' => $entity['assettagnumber'],
                    'anomlayCustomId' => $entity['anomalycustomid'],
                    'component' => $entity['component'],
                    'projectName' => $entity['registrarprojectname'],
                    'requestType' => $requestType,
                    'data' => []];
            }elseif ($entity['request_source']=="new_anomaly"){
                $requestType = 'New Anomaly Approval';
                $newAnomalyEntities[$entity['request_batch']][$entity['anomalyid']]=
                    ['assetTagNumber'=>$entity['assettagnumber'],
                    'anomlayCustomId'=>$entity['anomalycustomid'],
                    'component'=>$entity['component'],
                    'projectName'=>$entity['registrarprojectname'],
                    'requestType'=>$requestType,
                    'data'=>[]];
            }else{
                $requestType = 'Load-sheet Approval';
                $loadsheetEntities[$entity['request_batch']][$entity['anomalyid']]=
                    ['assetTagNumber'=>$entity['assettagnumber'],
                        'anomlayCustomId'=>$entity['anomalycustomid'],
                        'component'=>$entity['component'],
                        'projectName'=>$entity['registrarprojectname'],
                        'requestType'=>$requestType,
                        'data'=>[]];
            }
        }
        $deferEntities = [];
        foreach ($reqEntitiesDeferralOwners as $entity){
            $requestType = 'Defer Request';
            $deferEntities[$entity['anomalyid']] =
                ['assetTagNumber' => $entity['assettagnumber'],
                'anomlayCustomId' => $entity['anomalycustomid'],
                'component' => $entity['component'],
                'projectName' => $entity['registrarprojectname'],
                'requestType' => $requestType,
                'data' => []];
            }

        foreach ($reqEntitiesApprovalOwners as $entity){
            if ($entity['request_source']==null) {
                array_push($individualEntities[$entity['anomalyid']]['data'], $entity);
                $counts['ia']+=1;
            }elseif ($entity['request_source']=="new_anomaly"){
                array_push($newAnomalyEntities[$entity['request_batch']][$entity['anomalyid']]['data'],$entity);
                $counts['na']+=1;
            }else{
                array_push($loadsheetEntities[$entity['request_batch']][$entity['anomalyid']]['data'],$entity);
                $counts['ls']+=1;
            }
        }
        foreach ($reqEntitiesDeferralOwners as $entity){
            array_push($deferEntities[$entity['anomalyid']]['data'],$entity);
            $counts['da']+=1;
        }
                return [
            'individual_entities' => $individualEntities,
            'new_anomaly_entities' => $newAnomalyEntities,
            'loadsheet_entities' => $loadsheetEntities,
            'defer_entities' => $deferEntities,
            'counts'=>$counts,
            'user' => $user
        ];
    }

    function array_sort($array, $on, $order=SORT_ASC){

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    /**
     * Finds and displays a batch Request entity.
     *
     * @Route("/batch/{id}/{type}", name="anomaly_batch_requests_show")
     * @Method("GET")
     * @Template()
     */
    public function showBatchAction($id, $type)
    {
        $em = $this->getManager();
        $filter="load_sheet";
        if ($type==1){
            $filter="new_anomaly";
        }
        if ($type==2){
            $filter="load_sheet";
        }
        $entities = $em->getRepository('AIEAnomalyBundle:Request')->findBy(
            [
                'requestBatch' => $id,
                'requestSource' => $filter,
                'status' => -1,
            ]
        );

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Request entity.');
        }

        $serializer = $this->get('jms_serializer');

        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
        $counter=0;
        $checkArray=['RO','TRO','AS','FM'];
        $anomalyInfo=[];

        foreach ($entities as $entity){
            $requestEntity = $serializer->deserialize($entity->getData(), $entity->getType(), 'json');
            $entity_obj = $anomalyHelper->formatAnomaly(
                $requestEntity,
                json_decode($entity->getData(), true)
            );

            $registrarEntityCheck = $em->getRepository('AIEAnomalyBundle:Registrar')
                ->findOneBy(array('id'=>$entity_obj['Id']));
            $anomalyId=(in_array($registrarEntityCheck->getCode(),$checkArray) ? $registrarEntityCheck->getAnomaly()->getId() : $registrarEntityCheck->getId());
            if (!in_array($registrarEntityCheck->getCode(),$checkArray)){
                $anomalyInfo[$anomalyId]['assetTagNumber']=$registrarEntityCheck->getAssetTagNumber();
                $anomalyInfo[$anomalyId]['anomalyCustomId']=$registrarEntityCheck->getAnomalyCustomId();
                $anomalyInfo[$anomalyId]['projectName']=$registrarEntityCheck->getProject()->getName();
                if ($type==1){
                    $anomalyInfo[$anomalyId]['requestType']="New Anomaly Request";
                }
                if ($type==2){
                    $anomalyInfo[$anomalyId]['requestType']="Load-sheet Request";
                }
                $anomalyInfo[$anomalyId]['component']=$registrarEntityCheck->getComponent();
            }
            if (!isset($anomalyInfo[$anomalyId]['request'])){
                $anomalyInfo[$anomalyId]['request']=[];
            }
            array_push($anomalyInfo[$anomalyId]['request'], [
                'registerCode'=>$registrarEntityCheck->getCode(),
                'request'=>$entity,
                'entity'=>$registrarEntityCheck
            ]);
        }

        $editForm = $this->createBatchEditForm($id,$type);

        return [
            'entities' => $anomalyInfo,
            'edit_form' => $editForm->createView(),
            'batch_type'=>$type,
            'request_batch'=>$id
        ];
    }

    /**
     * Finds and displays a Request entity.
     *
     * @Route("/{id}", name="anomaly_requests_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Request')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Request entity.');
        }

        $anomalyentity = $em->getRepository('AIEAnomalyBundle:Registrar')->find($entity->getRegistrar()->getId());
        $usrInfoToken = $this->get('security.context')->getToken()->getUser();
        $userDetails = $em->getRepository('UserBundle:User')->find(
            $usrInfoToken->getId()
        );

        if ($anomalyentity->getCode() != 'RO' && $anomalyentity->getCode() != 'TRO' && $anomalyentity->getCode() != 'FM' && $anomalyentity->getCode() != 'AS')
        {
            $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy( array('project'=>$entity->getRegistrar()->getProject()->getId(),'actionOwner'=>$userDetails->getId(), 'anomalyCategory'=>$anomalyentity->getCode()));
        }
        else
        {
            $anomalyfindentity = $em->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($entity->getRegistrar()->getId());
            $anomalyparententity = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($anomalyfindentity->getAnomaly()->getId());
            $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy( array('project'=>$entity->getRegistrar()->getProject()->getId(),'actionOwner'=>$userDetails->getId(), 'anomalyCategory'=>$anomalyparententity->getCode()));
        }

        if ($entity->getApprovalLevel() == 1 && $anomalyentity->getStatusTag() == $anomalyentity->getleveltwoStatusTag()) {
            $this->addFlash('error', 'Requested action performed already1.');
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }
        else if ( $entity->getApprovalLevel() == 2 && $approvalOwner && $entity->getApprovalLevel() != $approvalOwner->getApprovalLevel())
        {
            $this->addFlash('error', 'Requested action performed already2.');
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }
        else if ( $entity->getApprovalLevel() == 2 && $anomalyentity->getleveltwoStatusTag() != 'request' && !empty($anomalyentity->getleveltwoStatusTag()))
        {
            $this->addFlash('error', 'Requested action performed already3.');
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }

        $serializer = $this->get('jms_serializer');

        //get previous requests
        $previous = $em->getRepository('AIEAnomalyBundle:Request')->findByRegistrar($entity->getRegistrar()->getId());

        // get history of same current request type
        $previous = array_filter(
            $previous,
            function ($record) use ($entity) {
                return $record->getType() == $entity->getType();
            }
        );

        array_pop($previous);

        $userIds = [];
        $approvedByUsers= [];

        foreach ($previous as $key => $record) {
            $id = null;

            if($record->getAnsweredBy())
            {
                $approvalOwner =   $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findById($record->getAnsweredBy()->getId());

                if($approvalOwner)
                {
                    $id = $record->getAnsweredBy()->getActionOwner()->getId();
                    $approvedByUsers[$key] = $id;
                }
            }

            if($id)
            {
                if ($id && !isset($userIds [$id])) {
                    $userIds [$id] = $id;
                }
            }

            $id = ($record->getRequestedBy()) ? $record->getRequestedBy()->getUserId() : null;
            if ($id && !isset($userIds [$id])) {
                $userIds [$id] = $id;
            }
        }


        $users = $this->get('aie_users')->findUsersById($userIds, true);

        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
        $requestEntity = $serializer->deserialize($entity->getData(), $entity->getType(), 'json');
        $entity_obj = $anomalyHelper->formatAnomaly(
            $requestEntity,
            json_decode($entity->getData(), true)
        );

        $projectId = $requestEntity->getProject()->getId();
        $entityForm = $this->createRegistrarForm($requestEntity, $projectId, $entity->getId())->createView();

        $mainEntity = $em->getRepository('AIEAnomalyBundle:Registrar')->find($requestEntity->getId());
        $em->refresh($mainEntity);

        $has_live_actions =0;

        if ($mainEntity instanceof AnomalyRegistrar)
        {
            $has_live_actions = count(
                    $mainEntity->getActions()->filter(
                        function (ActionRegistrar $action) {
                            return 'Live' == $action->getStatusChoices()[$action->getStatus()];
                        }
                    )
                ) > 0;
        }

        $getReportattachments = $entity->getAttachedreportanomalys();
        if (!empty($getReportattachments)){
            $strReportAttach = json_decode($getReportattachments);

            $qb = $em->createQueryBuilder();
            if ($mainEntity instanceof AnomalyRegistrar) {
                $passId = $mainEntity->getId();
            }
            else{
                $passId = $mainEntity->getAnomaly()->getId();
            }

            $query = $qb->select('partial r.{id,anomaly,filename,caption,document_number,company,date,document_date,size,filepath}')
                ->from('AIEAnomalyBundle:Files','r')
                ->Where('r.id IN (:ids)')
                ->andWhere('r.anomaly = :id')
                ->setParameter('ids',$strReportAttach)
                ->setParameter('id',$passId);

            $result = $query->getQuery()->getResult();
            $entity->setRequestFiles($result);
        }

        $mainEntity = $anomalyHelper->formatAnomaly($mainEntity);

        $editForm = $this->createEditForm($entity);

        return [
            'entity' => $entity,
            'mainEntity' => $mainEntity,
            'requestEntity' => $requestEntity,
            'entity_obj' => $entity_obj,
            'edit_form' => $editForm->createView(),
            'history' => $previous,
            'users' => $users,
            'is_anomaly' => $requestEntity instanceof AnomalyRegistrar,
            'anomaly_form' => ($requestEntity instanceof ActionRegistrar)? $this->createRegistrarForm($requestEntity->getAnomaly(), $projectId)->createView(): null,
            'entity_form' => $entityForm,
            'has_live_actions' =>$has_live_actions,
        ];
    }

    /**
     * Creates a form to show Registrar
     *
     * @param Registrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createRegistrarForm($entity, $projectId, $requestId=null)
    {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity->setProject($project);

        if ($requestId){
            $requestEntity = $em->getRepository('AIEAnomalyBundle:Request')->find($requestId);

            $serializer = $this->get('jms_serializer');

            $userInfo = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find(json_decode($requestEntity->getData())->spa_id);

            $entity->setSpaUser($em->getRepository('UserBundle:User')->find($userInfo->getUserId()));
        }

        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $_entityFullClass = explode('\\',get_class($entity));
        $entityClass = end($_entityFullClass);
        $typeForm = 'AIE\Bundle\AnomalyBundle\Form\\' . $entityClass . 'Type';

        $entityType = null;
        if($entity instanceof AnomalyRegistrar){
            $entityType = new $typeForm($entity, $em, $actionOwners, $entity->getSpaId());
        }elseif($entity instanceof ActionRegistrar){
            $anomaly = $entity->getAnomaly();
            $entityType = new $typeForm($entity, $anomaly, $actionOwners);
        }

        $form = $this->createForm(
            $entityType,
            $entity,
            [
                'disabled' => true,
                'read_only' => true,
                'action' => '#',
                'method' => 'POST',
            ]
        );

        if($entity instanceof AnomalyRegistrar){
            $form->get('anomaly_group')->remove('add_tracking_criteria');
        }elseif($entity instanceof ActionRegistrar){

        }

        $form->remove('files_group');

        return $form;
    }

    /**
     * Displays a form to edit an existing Request entity.
     *
     * @Route("/{id}/edit", name="anomaly_requests_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Request')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Request entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Request entity.
     *
     * @param Request $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AnomalyRequest $entity)
    {

        $registrar = $entity->getRegistrar();
        $files = ($registrar instanceof AnomalyRegistrar) ? $registrar->getFiles() : $registrar->getAnomaly()->getFiles(
        );

        $form = $this->createForm(
            new RequestType($entity, $files),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_requests_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Send', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Creates a form to edit a Request entity.
     *
     * @param Request $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createBatchEditForm($batchId,$type)
    {

        $form = $this->createForm(
            new RequestBatchType(),
            null,
            [
                'action' => $this->generateUrl('anomaly_batch_requests_update', ['id' => $batchId,'type'=>$type]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Send', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Edits an existing Batch Request entity.
     *
     * @Route("/batch/{id}/{type}", name="anomaly_batch_requests_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Request:showBatch.html.twig")
     */
    public function updateBatchAction(Request $request, $id, $type)
    {
        $em = $this->getManager();
        $filter="load_sheet";
        if ($type==1){
            $filter="new_anomaly";
        }
        $entities = $em->getRepository('AIEAnomalyBundle:Request')->findBy(
            [
                'requestBatch' => $id,
                'requestSource' => $filter,
                'status' => -1,
            ]
        );

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Request entity.');
        }

        $editForm = $this->createBatchEditForm($id,$type);
        $editForm->handleRequest($request);
        $flag=0;
        if ($editForm->isValid()) {
            $isApproved=$request->request->get('aie_bundle_anomalybundle_request')['status'];
            foreach ($entities as $entity){
                $flag=$this->UpdateRequest($em,$entity,$editForm,$isApproved);
            }
            if ($flag){
                $this->addFlash('success', 'Batch Request is Approved.');
            }else{
                $this->addFlash('warning', 'Batch Request is Rejected.');
            }
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }
    }

    /**
     * Edits an existing Request entity.
     *
     * @Route("/{id}", name="anomaly_requests_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Request:show.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Request')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Request entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $val= $this->UpdateRequest($em,$entity,$editForm,$entity->getStatus());
            if ($val){
                $this->addFlash('success', 'Request is Approved.');
            }else{
                $this->addFlash('warning', 'Request is Rejected.');
            }
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        ];
    }

    private function setMonitorAttributes(&$monitor, $requestEntity)
    {
        $monitor->setInspectionFrequency($requestEntity->getInspectionFrequency());
        $monitor->setDefectDescription($requestEntity->getDefectDescription());
        $monitor->setRemainingWallThickness($requestEntity->getRemainingWallThickness());
        $monitor->setLength($requestEntity->getLength());
        $monitor->setCorrosionRate($requestEntity->getCorrosionRate());
        $monitor->setComments($requestEntity->getComments());
        $serializer = $this->get('jms_serializer');

        $trackingCriteria = $serializer->serialize($requestEntity->getTrackingCriteria(), 'json');
        $monitor->setTrackingCriteria($trackingCriteria);
    }

    private function setActionMonitorAttributes(&$monitor, $requestEntity)
    {
        $monitor->setInspectionFrequency($requestEntity->getInspectionFrequency());
        $monitor->setInspectionResults($requestEntity->getInspectionResults());
        $monitor->setRepairType($requestEntity->getRepairType());
    }

    /**
     * Show the request history of an anomaly
     *
     * @Route("/history/{id}", name="anomaly_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Request:history.html.twig")
     */
    public function historyAction($id)
    {
        $em = $this->getManager();

        //get previous requests
        $requests = $em->getRepository('AIEAnomalyBundle:Request')->findByRegistrar($id);

        $userIds = [];
        $users = [];
        $requestData = [];
        $approvedByUsers = [];

        /**
         * Get the users
         */
        foreach ($requests as $requestId => $request) {

            $registrarInfo = $em->getRepository('AIEAnomalyBundle:Registrar')->find($request->getRegistrar());

            if (  $request->getRequestStatus() == 'deferral' ) {
                $approvalOwner = $em->getRepository('AIEAnomalyBundle:Deferrals')->find(
                    $registrarInfo->getSpa()->getId()
                );
            }
            else
            {
                $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy( array('actionOwner'=>$registrarInfo->getApprovedspa()->getId()));
            }
            //if
            if ($approvalOwner)
            {
                $uid = ($approvalOwner->getActionOwner()->getId()) ? $approvalOwner->getActionOwner()->getId() : null;
            }
            else
            {
                $approvalOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find(
                    $registrarInfo->getSpa()->getId()
                );
                $uid = ($approvalOwner->getUserId()) ? $approvalOwner->getUserId() : null;
            }

            $approvedByUsers[] = $uid;

            if ($uid && !isset($userIds [$uid])) {
                $userIds [$uid] = $uid;
            }

            $uid = $request->getRequestedBy()->getUserId();
            if ($uid && !isset($userIds [$uid])) {
                $userIds [$uid] = $uid;
            }

            $serializer = $this->get('jms_serializer');
            $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');

            $requestData[$request->getId()] = $anomalyHelper->formatAnomaly(
                $serializer->deserialize($request->getData(), $request->getType(), 'json'),
                json_decode($request->getData(), true)
            );

        }

        $users = $this->get('aie_users')->findUsersById($userIds, true);

        return [
            'history' => $requests,
            'users' => $users,
            'requestData' => $requestData,
            'approvedByUsers' => $approvedByUsers,
        ];

    }

    public function mergeUserRequests($type, $userId){
        $em = $this->getManager();
        $userHelper = $this->get('aie_anomaly.user.helper');
        $serializer = $this->get('jms_serializer');
        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');

        $reqEntitiesApprovalOwners = [];
        $entitiesPass = [];
        $registrarEntityCheck = null;
        $actionCodeArr = array('TRO','RO','FM','AS');
        $counter=0;
        if ($type == "A")
            $reqEntitiesApprovalOwners = $userHelper->getAllApprovalRequests(-1, $userId);
        else if ($type == "D")
            $reqEntitiesApprovalOwners = $userHelper->getAllDeferralRequests(-1, $userId);

        foreach($reqEntitiesApprovalOwners as $index => $entity)
        {
            $registrarEntityCheck = $em->getRepository('AIEAnomalyBundle:Registrar')->find($reqEntitiesApprovalOwners[$index]['registrar_id']);
            if ($registrarEntityCheck)
            {
                $ind = $counter;
                $entitiesPass[$ind] = $reqEntitiesApprovalOwners[$index];
                $anomalyId=(in_array($registrarEntityCheck->getCode(),$actionCodeArr)?$registrarEntityCheck->getAnomaly()->getId():$registrarEntityCheck->getId());
                $entitiesPass[$ind]['anomalyid']= $anomalyId;

                $assetTagNumber=(in_array($registrarEntityCheck->getCode(),$actionCodeArr)?$registrarEntityCheck->getAnomaly()->getAssetTagNumber():$registrarEntityCheck->getAssetTagNumber());
                $entitiesPass[$ind]['assettagnumber']= $assetTagNumber;
                $anomalyCustomId=(in_array($registrarEntityCheck->getCode(),$actionCodeArr)?$registrarEntityCheck->getAnomaly()->getAnomalyCustomId():$registrarEntityCheck->getAnomalyCustomId());
                $entitiesPass[$ind]['anomalycustomid']= $anomalyCustomId;

                $entitiesPass[$ind]['request_source']= $reqEntitiesApprovalOwners[$index]['request_source'];
                $entitiesPass[$ind]['request_batch']= $reqEntitiesApprovalOwners[$index]['request_batch'];
                $entitiesPass[$ind]['approval_type']= $type;
                $entitiesPass[$ind]['registrarcode'] = ($registrarEntityCheck instanceof AnomalyRegistrar ? $registrarEntityCheck->getCode() : $registrarEntityCheck->getAnomaly()->getCode());
                $entitiesPass[$ind]['registrarprojectid'] = (!empty($registrarEntityCheck->getProject()->getId()) ? $registrarEntityCheck->getProject()->getId() : "");
                $entitiesPass[$ind]['registrarprojectname'] = (!empty($registrarEntityCheck->getProject()->getName()) ? $registrarEntityCheck->getProject()->getName() : "");
                $entitiesPass[$ind]['registrarfullid'] = (!empty($registrarEntityCheck->getFullId()) ? $registrarEntityCheck->getFullId() : "");
                $entitiesPass[$ind]['registrarid'] = (!empty($registrarEntityCheck->getId()) ? $registrarEntityCheck->getId() : "");
                $entitiesPass[$ind]['component'] = (!empty($registrarEntityCheck->getComponent()) ? $registrarEntityCheck->getComponent() : "");
                $counter = $counter + 1;
            }

        }
        return $entitiesPass;
    }

    function check_not_empty($s, $include_whitespace = false)
    {
        if ($include_whitespace) {
            // make it so strings containing white space are treated as empty too
            $s = trim($s);
        }
        return (isset($s) && strlen($s)); // var is set and not an empty string ''
    }

    public function UpdateRequest($em, $entity, $editForm, $isApproved)
    {
        $uploader = $this->get('storage.file_uploader');
        $serializer = $this->get('jms_serializer');

        $nextInspectionFlag =0;

        /*$actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(
            array('userId' => $this->getUser()->getId())
        );*/

        $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy(
            array('actionOwner' => $this->getUser()->getId())
        );

        if (!$approvalOwner) {
            $this->addFlash('error', 'You don\'t have permission to approve requests!');
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }

        $tagChoices = Registrar::getTagChoices();

        $obj = json_decode($entity->getData());
        $requestEntity = $serializer->deserialize($entity->getData(), $entity->getType(), 'json');
        if ($isApproved) { //status approved
            if($entity->getRegistrar()->getProject()->getApprovalLevel() == $entity->getApprovalLevel())
            {
                switch ($entity->getType()) {

                    case 'AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly':
                    case 'AIE\Bundle\AnomalyBundle\Entity\PipingAnomaly':
                    case 'AIE\Bundle\AnomalyBundle\Entity\StaticEquipmentAnomaly':
                    case 'AIE\Bundle\AnomalyBundle\Entity\StructureAnomaly':
                    case 'AIE\Bundle\AnomalyBundle\Entity\MarineAnomaly':
                        $requestEntity->setSpa(
                            $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($obj->spa_id)
                        );
                        $requestEntity->setApprovedspa(
                            $approvalOwner
                        );
                        $requestEntity->setProject(
                            $em->getRepository('AIEAnomalyBundle:Projects')->find($obj->project_id)
                        );
                        $requestEntity->setLocation(
                            $em->getRepository('AIEAnomalyBundle:Locations')->find($obj->location_id)
                        );
                        $requestEntity->setThreat(
                            $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($obj->threat_id)
                        );
                        $requestEntity->setClassification(
                            $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($obj->classification_id)
                        );

                        if ($requestEntity->getStatusChoices()[$requestEntity->getStatus()] == 'Closed') {
                            $requestEntity->setStatusTag($tagChoices['closed']['name']);//set status of entity to final
                            foreach ($requestEntity->getActions() as $action) {
                                $action->setStatusTag($tagChoices['closed']['name']);//set status of entity to cancel
                                $action->setStatus(1);//set status 1
                            }
                        } elseif ($requestEntity->getStatusChoices()[$requestEntity->getStatus()] == 'Cancelled') {
                            $requestEntity->setStatusTag(
                                $tagChoices['cancelled']['name']
                            );//set status of entity to caccelled
                            //cancel all subsections
                            foreach ($requestEntity->getActions() as $action) {
                                $action->setStatusTag($tagChoices['cancelled']['name']);//set status of entity to final
                                $action->setStatus(2);//set status 2
                            }
                        } else {
                            //if new or update (final)

                            $requestEntity->setleveltwoStatusTag($tagChoices['final']['name']);//set status of entity to final
                            $requestEntity->setStatusTag($tagChoices['final']['name']);//set status of entity to final

                            // create the actions requested for the anomaly!
                            //throw new \Exception("WORK IN REQUEST CONTROLLER!");
                            $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
                            $anomalyHelper->createAnomalyActions($em, $requestEntity);
                        }

                        foreach ($requestEntity->getTrackingCriteria() as $tc) {
                            $tc->setRegistrar($requestEntity);
                        }

                        foreach ($requestEntity->getFiles() as $file) {
                            $file->setAnomaly($requestEntity);
                        }

                        //deferred reset zero

                        $fields = array('req.inspectionDate, req.inspectionFrequency');
                        $qb = $em->createQueryBuilder();
                        $query = $qb->select($fields)
                            ->from('AIEAnomalyBundle:AnomalyRegistrar','req')
                            ->where('req.id= :id')
                            ->setParameter('id', $entity->getRegistrar()->getId());

                        $result = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

                        if (isset($result[0]['inspectionDate'])){
                            if ($requestEntity->getInspectionDate()->format('U') != $result[0]['inspectionDate']->format('U')){
                                $requestEntity->setDeferred(0);
                                $nextInspectionFlag = 1;
                            } //if inspection frequency is changed the next inspection date is changed
                            elseif ($requestEntity->getInspectionFrequency() != $result[0]['inspectionFrequency']){
                                $requestEntity->setDeferred(0);
                                $nextInspectionFlag = 1;
                            }
                        } // double check
                        else if(
                                ( empty($result[0]['inspectionDate']) || empty($result[0]['inspectionFrequency']) )
                                    &&
                                ($requestEntity->getInspectionDate() || $requestEntity->getInspectionFrequency() )
                             )
                        {
                            $nextInspectionFlag = 1;
                        }

                        // If AI is unchecked we make the frequency and next inspection date as null
                        if ( ! in_array(0, $requestEntity->getActionRequired())) {
                            $requestEntity->setInspectionFrequency(null);
                            $requestEntity->setNextInspectionDate(null);
                        }

                        /* Adding Monitor per approval only if Inspection is in actions*/
                        if (in_array(0, $requestEntity->getActionRequired())) {

                            $currentMonitors = $requestEntity->getMonitors()->toArray();
                            $date = $requestEntity->getInspectionDate();

                            $monitor = new Monitor();
                            $monitor->setInspectionDate($date);
                            $this->setMonitorAttributes($monitor, $requestEntity);
                            $monitor->setRegistrar($requestEntity);
                            $flagmatch=0;

                            if (count($currentMonitors) > 0) {
//                                $lastMonitor = end($currentMonitors);
                                foreach ($currentMonitors as $currentMonitor)
                                {
                                    if ($currentMonitor->getInspectionDate() == $date)
                                    {
                                        $flagmatch=1;
                                        $lastMonitor = $currentMonitor;
                                    }
                                }
                                if (!$flagmatch) { //if add is different than last date, add new one
                                    $em->persist($monitor);
                                } else {
                                    $this->setMonitorAttributes($lastMonitor, $requestEntity);
                                    $em->persist($lastMonitor);
                                }
                            } else { //if there exist no monitors
                                $em->persist($monitor);
                            }

                        }

                        break;

                    case 'AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar':
                        /* Adding Monitor per approval */
                        $monitor = new ActionMonitor();
                        $currentMonitors = $requestEntity->getMonitors();
                        $currentMonitors = ($currentMonitors) ? $currentMonitors->toArray() : [];

                        $date = (!empty($requestEntity->getInspectionDate()))? $requestEntity->getInspectionDate() : $requestEntity->getExpiryDate();
                        $monitor->setInspectionDate($date);
                        $this->setActionMonitorAttributes($monitor, $requestEntity);
                        $monitor->setAction($requestEntity);

                        // reset deferred to zero if inspection date updated
                        $fields = array('req.inspectionDate, req.inspectionFrequency');
                        $qb = $em->createQueryBuilder();
                        $query = $qb->select($fields)
                            ->from('AIEAnomalyBundle:TemporaryRepairOrderRegistrar','req')
                            ->where('req.id= :id')
                            ->setParameter('id', $entity->getRegistrar()->getId());

                        $result = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

                        if (isset($result[0]['inspectionDate']) || $requestEntity->getInspectionDate() ){

                            if(empty($result[0]['inspectionDate']))
                                $nextInspectionFlag = 2;
                            else if( ! $requestEntity->getInspectionDate())
                                $nextInspectionFlag = 2;
                            else if ($requestEntity->getInspectionDate()->format('U') != $result[0]['inspectionDate']->format('U'))
                            {
                                $requestEntity->setDeferred(0);
                                $nextInspectionFlag = 2;
                            }
                            elseif ($requestEntity->getInspectionFrequency() != $result[0]['inspectionFrequency']){
                                $requestEntity->setDeferred(0);
                                $nextInspectionFlag = 2;
                            }
                        }
                        else
                        {
                            $fields = array('req.expiryDate');
                            $qb = $em->createQueryBuilder();
                            $query = $qb->select($fields)
                                ->from('AIEAnomalyBundle:ActionRegistrar','req')
                                ->where('req.id= :id')
                                ->setParameter('id', $entity->getRegistrar()->getId());

                            $result = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

                            if (isset($result[0]['expiryDate'])){
                                if ($requestEntity->getExpiryDate()->format('U') != $result[0]['expiryDate']->format('U')){
                                    $requestEntity->setDeferred(0);
                                }
                            }
                        }

                        if (count($currentMonitors) > 0) {
                            $lastMonitor = end($currentMonitors);
                            if ($lastMonitor->getInspectionDate() != $date
                            ) { //if add is different than last date, add new one
                                $em->persist($monitor);
                            } else {
                                $this->setActionMonitorAttributes($lastMonitor, $requestEntity);
                                $em->persist($lastMonitor);
                            }
                        } else { //if there exist no monitors
                            $em->persist($monitor);
                        }
                    case 'AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar':
                    case 'AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar':
                    case 'AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar':

                        //Link files
                        $requestEntity->removeAllRegistrarFiles();
                        foreach ($obj->files as $file) {
                            $requestEntity->addFile($em->getRepository('AIEAnomalyBundle:Files')->find($file->id));
                        }
                        $requestEntity->setSpa(
                            $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($obj->spa_id)
                        );

                        if ($requestEntity->getStatusChoices()[$requestEntity->getStatus()] == 'Closed') {
                            $requestEntity->setStatusTag($tagChoices['closed']['name']);//set status of entity to final
                            $requestEntity->setStatus(1);
                        }
                        elseif ($requestEntity->getStatusChoices()[$requestEntity->getStatus()] == 'Cancelled') {
                            $requestEntity->setStatusTag(
                                $tagChoices['cancelled']['name']
                            );//set status of entity to final
                            $requestEntity->setStatus(2);
                        }
                        else {
                            if ($requestEntity->getCode() == "RO"){
                                if (null !== $requestEntity->getCloseOutDate()) {
                                    $requestEntity->setStatusTag(
                                        $tagChoices['closed']['name']
                                    );
                                    $requestEntity->setStatus(1);
                                }
                                else
                                {
                                    $requestEntity->setleveltwoStatusTag($tagChoices['final']['name']);//set status of entity to final
                                    $requestEntity->setStatusTag($tagChoices['final']['name']);//set status of entity to final
                                }


                            }
                            else if ($requestEntity->getCode() == "FM"){
                                if (null !== $requestEntity->getCompletionDate()) {
                                    $requestEntity->setStatusTag(
                                        $tagChoices['closed']['name']
                                    );
                                    $requestEntity->setStatus(1);
                                }
                                else
                                {
                                    $requestEntity->setleveltwoStatusTag($tagChoices['final']['name']);//set status of entity to final
                                    $requestEntity->setStatusTag($tagChoices['final']['name']);//set status of entity to final
                                }
                            }
                            else if ($requestEntity->getCode() == "AS"){
                                if (!null !== $requestEntity->getAssessmentDueDate()) {
                                    if ($requestEntity->getAssessmentResult()== null){
                                        $requestEntity->setStatusTag($tagChoices['final']['name']);
                                    }else{
                                        $requestEntity->setStatusTag(
                                            $tagChoices['closed']['name']
                                        );
                                        $requestEntity->setStatus(1);
                                    }
                                }
                                else
                                {
                                    $requestEntity->setleveltwoStatusTag($tagChoices['final']['name']);//set status of entity to final
                                    $requestEntity->setStatusTag($tagChoices['final']['name']);//set status of entity to final
                                }
                            }
                            else
                            {
                                $requestEntity->setleveltwoStatusTag($tagChoices['final']['name']);//set status of entity to final
                                $requestEntity->setStatusTag($tagChoices['final']['name']);//set status of entity to final
                            }
                        }
                        $fields = array('req.expiryDate');
                        $qb = $em->createQueryBuilder();
                        $query = $qb->select($fields)
                            ->from('AIEAnomalyBundle:ActionRegistrar','req')
                            ->where('req.id= :id')
                            ->setParameter('id', $entity->getRegistrar()->getId());

                        $result = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

                        if (isset($result[0]['expiryDate'])){
                            if ($requestEntity->getExpiryDate()->format('U') != $result[0]['expiryDate']->format('U')){
                                $requestEntity->setDeferred(0);
                            }
                        }
                        break;
                }//end switch case
                $em->persist($requestEntity);
                $em->flush();

                $approvedFlag = 1;

                if ($nextInspectionFlag)
                {
                    $frequency = $requestEntity->getInspectionFrequency();
                    $dateToModify = new \DateTime( $requestEntity->getInspectionDate()->format("Y-m-d"));
                    if ($frequency < 1) {
                        $dateToModify->add(new \DateInterval('P1D'));
                    } else {
                        $months = (int)$frequency;
                        $dateToModify->add(new \DateInterval('P' . $months . 'M'));
                    }

                    $connection = $em->getConnection();
                    if ($nextInspectionFlag == 1){
                        if (null !== $dateToModify) {
                            $statement = $connection->prepare("UPDATE AnomalyRegistrar SET next_inspection_date = :insdate where id=:id");
                            $statement->bindValue('insdate', $dateToModify->format('Y-m-d H:i:s'));
                            $statement->bindValue('id', $requestEntity->getId());
                            $statement->execute();
                        }
                    }
                    else if ($nextInspectionFlag == 2) {

                        if (null !== $dateToModify) {

                            $statement = $connection->prepare("UPDATE TemporaryRepairOrderRegistrar SET next_inspection_date = :insdate where id=:id");
                            $statement->bindValue('insdate', $dateToModify->format('Y-m-d H:i:s'));
                            $statement->bindValue('id', $requestEntity->getId());
                            $statement->execute();
                        }
                    }
                }


                $entity->setAnsweredBy($approvalOwner);
                $entity->setAnsweredDate(new \DateTime());
                $entity->setStatus(1);
                $em->persist($entity);
                $em->flush();


                if($this->container->getParameter('kernel.environment') != 'dev'){
                   // try {
                        /** Set the report */
                        //$reportGenerator = $this->get('aie_anomaly.report.generator');

                        // /** set report name */
                       // $reportName = FileHelper::cleanFileName($requestEntity->getFullId());
                      //  if ($requestEntity instanceof AnomalyRegistrar) {
                      //      $reportName = 'AR-'.$reportName;
                      //  }

                        // /** Generate report */
                    //    $report = $reportGenerator->generateReport($requestEntity, $entity, $reportName);
                      //  $fileName = $reportName.'.'.$report->getExtension();
                      //  $absFilePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.$fileName;

                     //   $file = new File($absFilePath);
                     //   $uploadedPath = $uploader->upload($file, DIRECTORY_SEPARATOR. $entity->getUploadDir(), $entity->getAllowedMimeTypes());

                        //$uploadedPath = $this->uploadFile($file, $entity); // upload it
                     //   @unlink($absFilePath); //remove temp file
                     //   $connection = $em->getConnection();
                     //   $statement = $connection->prepare("UPDATE Request SET filepath = :insdate where id=:id");
                     //   $statement->bindValue('insdate', $uploadedPath);
                     //   $statement->bindValue('id', $entity->getId());
                     //   $statement->execute();
                   // } catch (Exception $e) {

                   // }

                }
                //create pdf report
                $defaultEm = $this->getDoctrine()->getManager();
                $wpHelper = new WorkpackHelper($em,$defaultEm);
                $pdf = $wpHelper->definePDFObject();
                $registrar = $em->getRepository('AIEAnomalyBundle:Registrar')->findOneById($entity->getRegistrar()->getId());
                $em->refresh($registrar);
                $anomaly=($registrar instanceof AnomalyRegistrar ? $registrar : $registrar->getAnomaly());
                $reportName = FileHelper::cleanFileName($registrar->getFullId());
                if($registrar instanceof RepairOrderRegistrar){
                    $title = 'Repair Order Report';
                    $customId=$registrar->getAnomaly()->getAnomalyCustomId();
                    $img=null;
                }elseif($registrar instanceof TemporaryRepairOrderRegistrar){
                    $title = 'Temporary Repair Order Report';
                    $customId=$registrar->getAnomaly()->getAnomalyCustomId();
                    $img=null;
                }elseif($registrar instanceof FabricMaintenanceRegistrar){
                    $title = 'Fabric Maintenance Report';
                    $customId=$registrar->getAnomaly()->getAnomalyCustomId();
                    $img=null;
                }elseif($registrar instanceof AssessmentRegistrar){
                    $title = 'Assessment Report';
                    $customId=$registrar->getAnomaly()->getAnomalyCustomId();
                    $reportName = 'AR-'.$reportName;
                    $img=null;
                }else{
                    $title = 'Anomaly Inspection Report';
                    $img = $this->getMonitorImage($anomaly);
                    $customId=$registrar->getAnomalyCustomId();
                }
                $anomalyReportObj = new PDFReportHelper($em, $defaultEm, ($this->container->getParameter('kernel.environment') == 'dev'?true:false), $this->get('storage.file_uploader'));
                $coverArray = [
                    ['size'=>'L', 'hasImage'=>false,'caption'=>'Title','value'=>$title],
                    ['size'=>'L', 'hasImage'=>true,'caption'=>'Client Name','value'=>$registrar->getProject()->getName()],
                    ['size'=>'L', 'hasImage'=>false,'caption'=>'Location','value'=>$registrar->getLocation()->getName()],
                    ['size'=>'S', 'hasImage'=>false,'caption'=>'Report Ref.','value'=>'REP-'. $registrar->getProject()->getId() . '-' . $registrar->getProject()->getReferenceNo() . '-' . $registrar->getCode() . '-' . date('Y')],
                    ['size'=>'S', 'hasImage'=>false,'caption'=>'Date','value'=>(date('d F Y'))]
                ];
                if ($this->container->getParameter('kernel.environment') == 'dev'){
                    $isEnvDev = true;
                    $projectLogo = $registrar->getProject()->getServerFilePath($isEnvDev);
                }else {
                    $isEnvDev = false;
                    $projectLogo = $uploader->getAbsoluteFilePath($registrar->getProject()->getFilePath());
                }
                $anomalyReportObj->createReportCover($pdf, $title, $customId,$projectLogo,$isEnvDev, $coverArray);
                $pdf->setHeaderInfo($projectLogo,$isEnvDev,$registrar->getProject()->getName() . ' IMS',
                    $title . ' - ' . $customId ,
                    'REP-'. $registrar->getProject()->getId() . '-' . $registrar->getProject()->getReferenceNo() . '-' . $registrar->getCode() . '-' . date('Y'),
                    'P');
                $pdf->AddPage('P', 'A4');
                $anomalyReportObj->createRegisterReport($pdf, $registrar, $img, 1);
                $fileName = $reportName.'.pdf';
                $absFilePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.$fileName;
                $pdf->Output($absFilePath,'F');
                $file = new File($absFilePath);
                $uploadedPath = $uploader->upload($file, DIRECTORY_SEPARATOR. $entity->getUploadDir(), $entity->getAllowedMimeTypes());
                @unlink($absFilePath); //remove temp file
                $pdf = $wpHelper->definePDFObject(0,0,15,10,15,5);
                $summaryRepObj = new PDFSummaryReportHelper($em,$defaultEm,$isEnvDev,$uploader);
                $summaryRepObj->createSummaryReport($pdf,$registrar->getProject(),$projectLogo,$registrar);
                $fileName = $customId .'-summary.pdf';
                $absFilePath = sys_get_temp_dir().DIRECTORY_SEPARATOR.$fileName;
                $pdf->Output($absFilePath,'F');
                $file = new File($absFilePath);
                $uploadedSummaryPath = $uploader->upload($file, DIRECTORY_SEPARATOR. $entity->getUploadDir(), $entity->getAllowedMimeTypes());
                @unlink($absFilePath);
                $connection = $em->getConnection();
                $statement = $connection->prepare("UPDATE Request SET pdffilepath = :insdate, summary_pdffilepath = :summpath where id=:id");
                $statement->bindValue('insdate', $uploadedPath);
                $statement->bindValue('summpath', $uploadedSummaryPath);
                $statement->bindValue('id', $entity->getId());
                $statement->execute();
            }
            else
            {

                $connection = $em->getConnection();
                $statement = $connection->prepare("UPDATE Registrar SET leveltwostatusTag = :leveltwostatusTag, 
                 statusTag=:statusTag
                 where id=:id");
                $statement->bindValue('leveltwostatusTag', $tagChoices['request']['name']);
                $statement->bindValue('statusTag', $tagChoices['request']['name']);
                $statement->bindValue('id', $requestEntity->getId());
                $statement->execute();

                if ($entity->getApprovalLevel() != null)
                    $approvalLevel = $entity->getApprovalLevel() +1;
                else
                    $approvalLevel = 2;

                $today = new \DateTime();
                $statement = $connection->prepare("UPDATE Request SET 
                justification =:justification, status = :status,
                answered_date =:answered_date,
                answered_by =:answered_by,
                approval_level =:approval_level
                where id=:id");

                $statement->bindValue('justification', $editForm->getData()->getJustification());
                $statement->bindValue('status', -1);
                $statement->bindValue('id', $entity->getId());
                $statement->bindValue('answered_date', $today->format('Y-m-d H:i:s'));
                $statement->bindValue('answered_by', $approvalOwner->getId());
                $statement->bindValue('approval_level', $approvalLevel);
                $statement->execute();

                $approvedFlag = 1;
            }
        } //end if approved
        else { //if rejected

            $connection = $em->getConnection();
            $statement = $connection->prepare("UPDATE Registrar SET statusTag = :status, 
                leveltwostatusTag = :status, 
                approved_spa_id=:approved_spa_id 
                where id=:id");
            $statement->bindValue('status', $requestEntity->getTagChoices()['request-rejected']['name']);
            $statement->bindValue('approved_spa_id', $approvalOwner->getId());
            $statement->bindValue('id', $entity->getRegistrar()->getId());
            $statement->execute();



            $today = new \DateTime();
            $statement = $connection->prepare("UPDATE Request SET 
                justification =:justification, status = :status,
                answered_date =:answered_date,
                answered_by =:answered_by
                where id=:id");

            $statement->bindValue('justification', $editForm->getData()->getJustification());
            $statement->bindValue('status', 0);
            $statement->bindValue('id', $entity->getId());
            $statement->bindValue('answered_date',$today->format('Y-m-d H:i:s'));
            $statement->bindValue('answered_by', $approvalOwner->getId());
            $statement->execute();

            $approvedFlag = 0;
        }

        return $approvedFlag;
    }

    public function getMonitorImage($anomaly){
        if($anomaly->getClassification()->getCode() == "EML" || $anomaly->getClassification()->getCode() == "IML"){
            if ($anomaly->getNominalWT() > 0 and count($anomaly->getMonitors())>1){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitor.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $anomaly->getMonitors()
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }else{
            $trackingTypes=[];
            $trackingCount = [];
            $entities=$anomaly->getMonitors();
            foreach ($entities as $entity){
                $trcks = json_decode($entity->getTrackingCriteria());
                foreach ($trcks as $trck){
                    if (!(in_array ($trck->description,$trackingTypes))){
                        array_push($trackingTypes,$trck->description);
                        $trackingCount[$trck->description] = 0;
                    }
                }
            }
            foreach ($trackingTypes as $tc){
                foreach ($entities as $entity){
                    $trcks = json_decode($entity->getTrackingCriteria());
                    foreach ($trcks as $trck){
                        if ($tc == $trck->description && is_numeric($trck->value) && is_numeric($trck->tracking_conc)){
                            $trackingCount[$trck->description] +=1;
                        }
                    }
                }
            }
            $hasimg = false;
            foreach ($trackingCount as $cc){
                if ($cc > 1){
                    $hasimg=true;
                }
            }
            if ($hasimg){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitortc.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $entities,
                    'tracking_types'=> $trackingTypes
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }
        return $img;
    }
}
