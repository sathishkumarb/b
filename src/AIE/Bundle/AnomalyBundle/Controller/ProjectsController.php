<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyClass;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskMatrix;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Form\ProjectsType;
use AIE\Bundle\AnomalyBundle\Form\ProjectsEditType;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Projects controller.
 *
 * @Route("/projects")
 */
class ProjectsController extends AnomalyBaseController
{
    // removed the roles ROLE_project_Add from the config file. Only module level admin (using 'assign as admin' link from admin panel) should be able to do these two tasks as it is not possible to have these two role for project level, but instead only at module level.
    // So no user except module level admin shall be able to see links to add  projects.

    /**
     * Lists all Projects entities.
     *
     * @Route("/", name="anomaly_projects")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $entities = $this->getGrantedProjects();
        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $this->get('request')->query->get('page', 1) /* page number */,
            $pagination_config['limit_per_page'] /* limit per page */
        );
        $showGranted=0;
        $editGranted=0;
        $deleteGranted=0;
        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)){
            $showGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_EDIT',$this->userRolesMergeToCheck)){
            $editGranted = 1;
        }

        if ($this->securityHelper->isRoleGranted('ROLE_PROJECT_DELETE',$this->userRolesMergeToCheck) || $this->assignedAsAdmin){
            $deleteGranted = 1;
        }

        return [
            'entities' => $entities,
            'pagination' => $pagination,
            'showGranted' =>  $showGranted,
            'editGranted' =>  $editGranted,
            'deleteGranted' =>  $deleteGranted,
        ];
    }

    /**
     * Creates a new Projects entity.
     *
     * @Route("/", name="anomaly_projects_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Projects:new.html.twig")
     */
    public function createAction(Request $request)
    {

        $type = $request->request->get('aie_bundle_anomalybundle_projects')['type'];
        if ($type == 1) {
            //Risk Project

            /* Create Risk Matrix */
            $entity = new RiskProject();
            $entity->setRiskMatrix(new RiskMatrix());
            $entity->setEnableFMCriticality(false);
            $entity->setEnableProductionCriticality(false);
        } else {
            $entity = new CriticalityProject();
            $entity->setCriticality(new CriticalityRanks());
        }

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /* Handle Uploaded File */
            $data = $form->getData();
            $file = $data->getFile();
            $uploadedPath = $this->uploadFile($file, $entity);
            $entity->setFilepath($uploadedPath);
            $entity->setFolder(md5(uniqid(rand(), true)));

            /*  */
            $entity->setDateCreate(new \DateTime("now"));

            $securityContext = $this->get('security.context');
            $token = $securityContext->getToken();
            $user = $token->getUser();
            $userId = $user->getId();
            $entity->setCreatedBy($userId);


            /*  */
            $em = $this->getManager();


            /* Add default Class, threats and design codes */
            $class = $em->getRepository('AIEAnomalyBundle:Classifications')->findAll();
            $threats = $em->getRepository('AIEAnomalyBundle:Threat')->findAll();
            $designCodes = $em->getRepository('AIEAnomalyBundle:DesignCode')->findAll();

            foreach ($class as $c) {
                $class_e = new AnomalyClass();
                $class_e->setCode($c->getCode());
                $class_e->setDescription($c->getDescription());
                $class_e->setIsMetalLoss($c->getIsMetalLoss());
                $class_e->setProject($entity);
                $entity->addClass($class_e);
            }

            foreach ($threats as $t) {
                $threat_e = new AnomalyThreat();
                $threat_e->setCode($t->getCode());
                $threat_e->setDescription($t->getDescription());
                $threat_e->setProject($entity);
                $entity->addThreat($threat_e);
            }

            foreach ($designCodes as $dc) {
                $designCode_e = new AnomalyDesignCode();
                $designCode_e->setCode($dc->getCode());
                $designCode_e->setDescription($dc->getDescription());
                $designCode_e->setType($dc->getType());
                $designCode_e->setProject($entity);
                $entity->addCode($designCode_e);
            }

            if ($type == 1) {
                $entity->setEnableFMCriticality(false);
                $entity->setEnableProductionCriticality(false);
            } else {
                $entity->setEnableFMCriticality($request->request->get('aie_bundle_anomalybundle_projects')['enableFMCriticality']);
                $entity->setEnableProductionCriticality($request->request->get('aie_bundle_anomalybundle_projects')['enableProductionCriticality']);
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_projects_show', ['id' => $entity->getId()]));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Projects $entity)
    {
        $form = $this->createForm(
            new ProjectsType(),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_projects_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Projects entity.
     *
     * @Route("/new", name="anomaly_projects_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Projects();
        $form = $this->createCreateForm($entity);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    public function getIsDue($nextinspectionDate)
    {
        //if today is the next inspection or after that
        $now = new \DateTime('now');
        return ($nextinspectionDate !== null && (int)$nextinspectionDate->format('U') <= (int)$now->format('U'));
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}", name="anomaly_projects_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
//        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_SHOW',$this->userRolesMergeToCheck)) {
////           throw $this->createAccessDeniedException('You do not have permission to view the project.');
//        }

        $em = $this->getManager();

        /** @var Projects $entity */
        $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $registrars = $entity->getRegistrars();
        $counter = 1;
        $currentMonth=(int)date('m');
        $currentYear =(int)date('Y');
        $fromDateArray=[];
        $annual_planned = [];
        while ($counter <=12 ) {
            $fromDateArray[$counter] = new \DateTime($currentYear.'-' . $currentMonth . '-01');
            $annual_planned[$counter] = ['yr' => $currentYear.'-' . $currentMonth ,'in' => 0, 'ro' => 0, 'tro' => 0, 'fm' => 0,'as' => 0];
            $counter = $counter + 1;
            $currentMonth = $currentMonth + 1;
            if ($currentMonth > 12) {
                $currentMonth = 1;
                $currentYear = $currentYear + 1;
            }
        }

        $fromDate =  new \DateTime('first day of this month');
        $toDate  = new \DateTime('last day of this month');
        $toDateU = (int)$toDate->format('U');
        $checkDate= new \DateTime('first day of this month');
        $fromDateU = (int)$fromDate->format('U');
        $mitigationRisk=['AN','RO','TRO','FM','AS'];
        $mitigationRisk['AN']=['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];
        $mitigationRisk['RO']=['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];
        $mitigationRisk['TRO']=['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];
        $mitigationRisk['FM']=['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];
        $mitigationRisk['AS']=['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];

        $fmmitigationRisk=['AN','RO','TRO','FM','AS'];
        $fmcArray=[];
        foreach ($entity->getFabricMaintenanceCriticality() as $fmc){
            $fmcArray[$fmc->getId()]=0;
        }
        $fmmitigationRisk['AN']=$fmcArray;
        $fmmitigationRisk['RO']=$fmcArray;
        $fmmitigationRisk['TRO']=$fmcArray;
        $fmmitigationRisk['FM']=$fmcArray;
        $fmmitigationRisk['AS']=$fmcArray;
        $risks = [];
        $FMrisks = [];
        $anomalyInspections = ['open' => 0, 'planned' => 0,'overdue' => 0, 'deferred' => 0];
        $ro = ['open' => 0, 'planned' => 0, 'overdue' => 0, 'deferred' => 0];
        $tro = ['open' => 0, 'planned' => 0, 'overdue' => 0, 'deferred' => 0];
        $fm = ['open' => 0, 'planned' => 0, 'overdue' => 0, 'deferred' => 0];
        $as = ['open' => 0, 'planned' => 0, 'overdue' => 0, 'deferred' => 0];
        $anomaliesCatCount = ['PI' => 0, 'PL' => 0, 'ST' => 0, 'SE' => 0,'MR' => 0];
        $fmArray=[];
        foreach ($entity->getFMCriticalityArray() as $key=>$value)
            $fmArray[$key] = 0;
        $anomaliesFMCatCount = ['PI' => $fmArray,
            'PL' => $fmArray,
            'ST' => $fmArray,
            'SE' => $fmArray,
            'MR' => $fmArray];
        $anomaliesCatRiskCount = [
            'PI' => ['name' =>'Piping Anomalies','0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0],
            'PL' => ['name' =>'Pipeline Anomalies','0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0],
            'ST' => ['name' =>'Structure Anomalies','0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0],
            'SE' => ['name' =>'Static Equipment Anomalies','0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0],
            'MR' => ['name' =>'Marine Anomalies','0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0]];
        $classes = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->findByProject($id);
        $threats = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->findByProject($id);
        $locations = $em->getRepository('AIEAnomalyBundle:Locations')->findByProject($id);
        $locationsCatSet=[];
        $locationsRiskSet=[];
        $locationsFMRiskSet=[];
        foreach ($locations as $location){
            $locationsCatSet[$location->getId()]=['name' => $location->getName(), 'PI' => 0, 'PL' => 0, 'ST' => 0, 'SE' => 0,'MR' => 0];
            $locationsRiskSet[$location->getId()]=['name' => $location->getName(), '0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0];
            foreach ($entity->getFabricMaintenanceCriticality() as $fmc){
                $locationsFMRiskSet[$location->getId()][$fmc->getId()]=0;
            }
        }
        $classesCount = [];
        $threatsCount = [];
        foreach ($classes as $class){
            $classesCount[$class->getDescription()]=0;
        }
        foreach ($threats as $threat){
            $threatsCount[$threat->getDescription()]=0;
        }
        $liveAnomalies = 0;
        $checkArray=['PI',"PL","ST","SE","MR"];

        foreach ($registrars as $registrar) {
            if ($registrar->getStatus() > 0 || ($registrar->getStatusTag() != 'final' )) { // anomaly is closed/canceled
                continue;
            }
            $counter=1;
            $neededIndex =0;
            if($registrar instanceof AnomalyRegistrar && in_array(0,$registrar->getActionRequired()))
            {
                $checkDate = $registrar->getNextInspectionDate();
            }
            elseif ($registrar instanceof TemporaryRepairOrderRegistrar){
                if ($registrar->getNextInspectionDate() != null){
                    $checkDate = $registrar->getNextInspectionDate();
                }else{
                    $checkDate = $registrar->getExpiryDate();
                }
            }elseif (!$registrar instanceof AnomalyRegistrar){
                $checkDate = $registrar->getExpiryDate();
            }
            if ($checkDate != null){
                while ($counter <=12 ) {
                    if((int)$checkDate->format("Y")==(int)$fromDateArray[$counter]->format("Y") && (int)$checkDate->format("m")==(int)$fromDateArray[$counter]->format("m")){
                        $neededIndex=$counter;
                    }
                    $counter=$counter+1;
                }
            }
            $risk = $registrar->getRisk();
            if ($registrar instanceof AnomalyRegistrar) {

                if(in_array(0, $registrar->getActionRequired())){

                    if(!$registrar->getIsFabricMaintenanceCriticality()){
                        $mitigationRisk['AN'][$risk]+=1;
                    }else{
                        $fmmitigationRisk['AN'][$registrar->getFabricMaintenanceCriticality()]+=1;
                    }
                    $anomalyInspections['open'] += 1;
                    if ($registrar->getIsDeferred()) {
                        $anomalyInspections['deferred'] += 1;
                    }else{
                        if ($this->getIsDue($registrar->getNextInspectionDate())){
                            $anomalyInspections['overdue'] += 1;
                        }else{
                            $anomalyInspections['planned'] +=1;
                        }
                    }
                    if($neededIndex > 0){
                        $annual_planned[$neededIndex]['in']+=1;
                    }
                }
                $liveAnomalies += 1;
                if ($entity->hasFMCriticality() && $registrar->getIsFabricMaintenanceCriticality() && $registrar->getFabricMaintenanceCriticality() != null){
                    $anomaliesFMCatCount[$registrar->getCode()][$registrar->getFabricMaintenanceCriticality()]+=1;
                    $locationsFMRiskSet[$registrar->getLocation()->getId()][$registrar->getFabricMaintenanceCriticality()]+=1;
                    $FMrisks[$registrar->getFabricMaintenanceCriticality()] = isSet($FMrisks[$registrar->getFabricMaintenanceCriticality()]) ?
                        $FMrisks[$registrar->getFabricMaintenanceCriticality()] + 1
                        : 1;
                }else{
                    $anomaliesCatCount[$registrar->getCode()]+=1;
                    $locationsCatSet[$registrar->getLocation()->getId()][$registrar->getCode()]+=1;
                    $locationsRiskSet[$registrar->getLocation()->getId()][$risk]+=1;
                    $anomaliesCatRiskCount[$registrar->getCode()][$risk]+=1;
                    $risks[$risk] = isSet($risks[$risk]) ? $risks[$risk] + 1 : 1;
                }
                $classesCount[$registrar->getClassification()->getDescription()]=$classesCount[$registrar->getClassification()->getDescription()] + 1;
                $threatsCount[$registrar->getThreat()->getDescription()]=$threatsCount[$registrar->getThreat()->getDescription()] + 1;

            }elseif ($registrar instanceof RepairOrderRegistrar) {
                $ro['open'] += 1;
                if ($this->getIsDue($registrar->getExpiryDate())){
                    $ro['overdue'] += 1;
                }else{
                    $ro['planned'] +=1;
                }
                if($neededIndex > 0){
                    $annual_planned[$neededIndex]['ro']+=1;
                }
                if (!$registrar->getAnomaly()->getIsFabricMaintenanceCriticality())
                    $mitigationRisk['RO'][$risk]+=1;
                else{
                    $fmmitigationRisk['RO'][$registrar->getAnomaly()->getFabricMaintenanceCriticality()]+=1;
                }

            }elseif ($registrar instanceof TemporaryRepairOrderRegistrar) {
                $tro['open'] += 1;
                if ($registrar->getIsDeferred()) {
                    $tro['deferred'] += 1;
                }else{
                    $inDate = ($registrar->getNextInspectionDate() != null ?$registrar->getNextInspectionDate() : $registrar->getExpiryDate());
                    if ($this->getIsDue($inDate)){
                        $tro['overdue'] += 1;
                    }else{
                        $tro['planned'] +=1;
                    }
                }
                if($neededIndex > 0){
                    $annual_planned[$neededIndex]['tro']+=1;
                }
                if (!$registrar->getAnomaly()->getIsFabricMaintenanceCriticality())
                    $mitigationRisk['TRO'][$risk]+=1;
                else{
                    $fmmitigationRisk['TRO'][$registrar->getAnomaly()->getFabricMaintenanceCriticality()]+=1;
                }
            }elseif ($registrar instanceof FabricMaintenanceRegistrar) {
                $fm['open'] += 1;
                if ($registrar->getIsDeferred()) {
                    $fm['deferred'] += 1;
                }else{
                    if ($this->getIsDue($registrar->getExpiryDate())){
                        $fm['overdue'] += 1;
                    }else{
                        $fm['planned'] +=1;
                    }
                }
                if($neededIndex > 0){
                    $annual_planned[$neededIndex]['fm']+=1;
                }
                if (!$registrar->getAnomaly()->getIsFabricMaintenanceCriticality()){
                    $mitigationRisk['FM'][$risk]+=1;

                }else{
                    $fmmitigationRisk['FM'][$registrar->getAnomaly()->getFabricMaintenanceCriticality()]+=1;
                }

            }elseif ($registrar instanceof AssessmentRegistrar) {
                $as['open'] += 1;
                if ($this->getIsDue($registrar->getExpiryDate())){
                    $as['overdue'] += 1;
                }else{
                    $as['planned'] +=1;
                }
                if($neededIndex > 0){
                    $annual_planned[$neededIndex]['as']+=1;
                }
                if (!$registrar->getAnomaly()->getIsFabricMaintenanceCriticality())
                    $mitigationRisk['AS'][$risk]+=1;
                else{
                    $fmmitigationRisk['AS'][$registrar->getAnomaly()->getFabricMaintenanceCriticality()]+=1;
                }
            }
        }
        $stats = [
            'in' => $anomalyInspections,
            'ro' => $ro,
            'tro' => $tro,
            'fm' => $fm,
            'as' => $as,
            'risks' => $risks,
            'FMrisks' => $FMrisks,
            'live_anomalies' => $liveAnomalies
        ];

        $request = $this->getRequest();
        $delFlag = $request->get('d');
        $token = $this->container->get('security.context')->getToken();


        if(count($locationsCatSet)>10){
            $finalLocs=[];
            $finalLocsRisks=[];
            foreach ($locationsCatSet as $index => $item){
                $finalLocs[$index]=$item['PI'] + $item['PL'] + $item['ST'] + $item['SE'] + $item['MR'];
            }
            arsort($finalLocs);
            $finalLocs2=[];
            $locsCounter=0;
            foreach ($finalLocs as $index => $item){
                if ($locsCounter>15){
                    if(isset($finalLocs2[0])){
                        $finalLocs2[0]['PL']+=$locationsCatSet[$index]['PL'];
                        $finalLocs2[0]['PI']+=$locationsCatSet[$index]['PI'];
                        $finalLocs2[0]['ST']+=$locationsCatSet[$index]['ST'];
                        $finalLocs2[0]['SE']+=$locationsCatSet[$index]['SE'];
                        $finalLocs2[0]['MR']+=$locationsCatSet[$index]['MR'];
                    }else {
                        $finalLocs2[0] = ['name' => 'Others',
                            'PI' => $locationsCatSet[$index]['PI'], 'PL' => $locationsCatSet[$index]['PL'],
                            'ST' => $locationsCatSet[$index]['ST'], 'SE' => $locationsCatSet[$index]['SE'],
                            'MR' => $locationsCatSet[$index]['MR']];
                    }
                }else{
                    $finalLocs2[$index]=['name' => $locationsCatSet[$index]['name'],
                        'PI' => $locationsCatSet[$index]['PI'], 'PL' => $locationsCatSet[$index]['PL'],
                        'ST' => $locationsCatSet[$index]['ST'], 'SE' => $locationsCatSet[$index]['SE'],
                        'MR' => $locationsCatSet[$index]['MR']];
                }
                $locsCounter++;
            }
            $locationsCatSet=$finalLocs2;



            foreach ($locationsRiskSet as $index => $item){
                $finalLocsRisks[$index]=$item[0] + $item[1] + $item[3] + $item[2] + $item[4];
                if($locationsFMRiskSet){
                    foreach ($locationsFMRiskSet[$index] as $cnt)
                    $finalLocsRisks[$index] += $cnt;
                }
            }
            arsort($finalLocsRisks);
            $finalLocsRisks2=[];
            $finalFMLocsRisks2=[];
            $locsCounter=0;
            foreach ($finalLocsRisks as $index => $item){
                if ($locsCounter>15){
                    if(isset($finalLocsRisks2[0])){
                        $finalLocsRisks2[0][0]+=$locationsRiskSet[$index][0];
                        $finalLocsRisks2[0][1]+=$locationsRiskSet[$index][1];
                        $finalLocsRisks2[0][2]+=$locationsRiskSet[$index][2];
                        $finalLocsRisks2[0][3]+=$locationsRiskSet[$index][3];
                        $finalLocsRisks2[0][4]+=$locationsRiskSet[$index][4];
                        foreach ($entity->getFabricMaintenanceCriticality() as $fmc){
                            $finalFMLocsRisks2[0][$fmc->getId()]+=$locationsFMRiskSet[$index][$fmc->getId()];
                        }
                    }else {
                        $finalLocsRisks2[0]['name'] = 'Others';
                        $finalLocsRisks2[0][0]=$locationsRiskSet[$index][0];
                        $finalLocsRisks2[0][1]=$locationsRiskSet[$index][1];
                        $finalLocsRisks2[0][2]=$locationsRiskSet[$index][2];
                        $finalLocsRisks2[0][3]=$locationsRiskSet[$index][3];
                        $finalLocsRisks2[0][4]=$locationsRiskSet[$index][4];
                        foreach ($entity->getFabricMaintenanceCriticality() as $fmc){
                            $finalFMLocsRisks2[0][$fmc->getId()]=$locationsFMRiskSet[$index][$fmc->getId()];
                        }
                    }
                }else{
                    $finalLocsRisks2[$index]['name'] = $locationsRiskSet[$index]['name'];
                    $finalLocsRisks2[$index][0]=$locationsRiskSet[$index][0];
                    $finalLocsRisks2[$index][1]=$locationsRiskSet[$index][1];
                    $finalLocsRisks2[$index][2]=$locationsRiskSet[$index][2];
                    $finalLocsRisks2[$index][3]=$locationsRiskSet[$index][3];
                    $finalLocsRisks2[$index][4]=$locationsRiskSet[$index][4];
                    foreach ($entity->getFabricMaintenanceCriticality() as $fmc){
                        $finalFMLocsRisks2[$index][$fmc->getId()]=$locationsFMRiskSet[$index][$fmc->getId()];
                    }
                }
                $locsCounter++;
            }
            $locationsRiskSet=$finalLocsRisks2;
            $locationsFMRiskSet=$finalFMLocsRisks2;
        }
        $uploader = $this->get('storage.file_uploader');
        if ($this->container->getParameter('kernel.environment') == 'dev'){
            $isDevEnv = true;
            $projectLogo = $entity->getServerFilePath(1);
        }else {
            $isDevEnv = false;
            $projectLogo = $uploader->getAbsoluteFilePath($entity->getFilepath());
        }
        $projectbase64 = '';
        $fileExt = ($isDevEnv ? pathinfo($projectLogo,PATHINFO_EXTENSION) : pathinfo(parse_url($projectLogo, PHP_URL_PATH), PATHINFO_EXTENSION));
        $fileExt = strtolower($fileExt);
        if ($fileExt == 'png' || $fileExt == 'jpg' || $fileExt == 'bmp'|| $fileExt || $fileExt == 'jpeg') {
            if(($isDevEnv && file_exists ($projectLogo)) || (!$isDevEnv && $this->is_url_exist($projectLogo))){
                if ($isDevEnv){
                    $data = file_get_contents($projectLogo);
                    $base64 = 'data:image/' . $fileExt . ';base64,' . base64_encode($data);
                }else{
                    $fileUrl = $this->url($projectLogo);
                    $base64 = 'data:image/' . $fileExt . ';base64,' . base64_encode($fileUrl['page']);
                }
                $projectbase64 = $base64;
            }
        }
        if ( $delFlag == 'Y' )
        {
            $deleteForm = $this->createDeleteForm($id);
            return [
                'entity' => $entity,
                'logo' =>$projectbase64,
                'stats' => $stats,
                'classes'=>$classes,
                'classes_count'=>$classesCount,
                'threats'=>$threats,
                'threats_count'=>$threatsCount,
                'annual_planned'=>$annual_planned,
                'anomalies_cat_count'=>$anomaliesCatCount,
                'anomalies_fm_cat_count'=>$anomaliesFMCatCount,
                'location_cat_anomalies'=>$locationsCatSet,
                'location_risk_anomalies'=>$locationsRiskSet,
                'location_fm_risk_anomalies'=>$locationsFMRiskSet,
                'anomalies_cat_risk'=>$anomaliesCatRiskCount,
                'mitigation_risk'=>$mitigationRisk,
                'fm_mitigation_risk'=>$fmmitigationRisk,
                'delete_form' => $deleteForm->createView()
            ];
        }
        else
        {
             return [
                'entity' => $entity,
                 'classes'=>$classes,
                 'logo' =>$projectbase64,
                 'classes_count'=>$classesCount,
                 'threats'=>$threats,
                 'threats_count'=>$threatsCount,
                 'annual_planned'=>$annual_planned,
                 'anomalies_cat_count'=>$anomaliesCatCount,
                 'anomalies_fm_cat_count'=>$anomaliesFMCatCount,
                 'location_cat_anomalies'=>$locationsCatSet,
                 'location_risk_anomalies'=>$locationsRiskSet,
                 'location_fm_risk_anomalies'=>$locationsFMRiskSet,
                 'anomalies_cat_risk'=>$anomaliesCatRiskCount,
                 'mitigation_risk'=>$mitigationRisk,
                 'fm_mitigation_risk'=>$fmmitigationRisk,
                'stats' => $stats
            ];
        }
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/edit", name="anomaly_projects_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_EDIT',$this->userRolesMergeToCheck) && !$this->assignedAsAdmin) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You do not have permission to edit the project."));

       }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $securityContext = $this->container->get('security.authorization_checker');

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Projects $entity)
    {
        $form = $this->createForm(
            new ProjectsEditType($entity),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_projects_update', ['id' => $entity->getId()]),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="anomaly_projects_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Projects:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $currentApprovalLevel = $entity->getApprovalLevel();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            /* Handle Uploaded File */
            $data = $editForm->getData();
            if (($data->getApprovalLevel() == 1 && $currentApprovalLevel == 2)
                /*|| ($data->getApprovalLevel() == 2 && $currentApprovalLevel == 1)*/){
                $connection = $em->getConnection();
                $statement = $connection->prepare("UPDATE Request SET approval_level = :applevel where registrar_id IN (select id from Registrar where project_id = :projid)");
                $statement->bindValue('applevel', $data->getApprovalLevel());
                $statement->bindValue('projid', $id);
                $statement->execute();
            }
            $file = $data->getFile();
            $uploadedPath = $this->replaceFile($file, $entity);
            $entity->setFilepath($uploadedPath);

            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'The project has been updated');
            return $this->redirect($this->generateUrl('anomaly_projects_show', ['id' => $id]));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a Projects entity.
     *
     * @Route("/{id}", name="anomaly_projects_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_PROJECT_DELETE',$this->userRolesMergeToCheck) && !$this->assignedAsAdmin) {

            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You do not have permission to delete the project."));


        }

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Projects entity.');
            }

            $securityContext = $this->container->get('security.authorization_checker');

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_projects'));
    }

    /**
     * Creates a form to delete a Projects entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_projects_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    function url($url,$option = null) {

        $cURL = curl_init();

        if ($option) {
            curl_setopt($cURL, CURLOPT_URL, $url.$option);
        } else {
            curl_setopt($cURL, CURLOPT_URL, $url);
        }

        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURL, CURLOPT_AUTOREFERER, 1);
        curl_setopt($cURL, CURLOPT_HTTPGET, 1);
        curl_setopt($cURL, CURLOPT_VERBOSE, 0);
        curl_setopt($cURL, CURLOPT_HEADER, 0);
        curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cURL, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($cURL, CURLOPT_DNS_CACHE_TIMEOUT, 2);

        $output['page'] = curl_exec($cURL);
        $output['contentType'] = curl_getinfo($cURL, CURLINFO_CONTENT_TYPE);

        curl_close($cURL);

        return $output;

    }

}
