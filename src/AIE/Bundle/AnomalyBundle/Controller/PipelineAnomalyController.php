<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Monitor;
use AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\Pipelines;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Form\PipelineAnomalyLocationsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Form\PipelineAnomalyType;
use \AIE\Bundle\AnomalyBundle\Entity\Request as AnomalyRequest;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * PipelineAnomaly controller.
 *
 * @Route("projects/{projectId}/pipeline")
 */
class PipelineAnomalyController extends AnomalyRegistrarController
{

    protected $registrarClassName = 'PipelineAnomaly';
    protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly';
    protected $registrarRouteBase = 'anomaly_pl';
    protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\PipelineAnomalyType';

    /**
     * Deletes a PipingAnomaly entity.
     *
     * @Route("/{id}/remove", name="anomaly_pl_remove")
     * @Method("GET")
     */
    public function deleteIndividualAction(Request $request, $projectId, $id) {
        return parent::deleteIndividualAction($request, $projectId, $id);
    }

    /**
     * Show the request history of an anomaly
     *
     * @Route("/{id}/history", name="pl_registrar_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Registrar:history.html.twig")
     */
    public function historyAction($id)
    {
        return parent::historyAction($id);
    }

    /**
     * Lists all PipelineAnomaly entities.
     *
     * @Route("/", name="anomaly_pl")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function indexAction(Request $request, $projectId)
    {
        return parent::indexAction($request, $projectId);
    }

    /**
     * Creates a new PipelineAnomaly entity.
     *
     * @Route("/new", name="anomaly_pl_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:PipelineAnomaly:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        return parent::createAction($request, $projectId);
    }

    protected function setNewEntityFields(&$entity, $data)
    {
        parent::setNewEntityFields($entity, $data);
    }


    /**
     * Displays a form to create a new PipelineAnomaly entity.
     *
     * @Route("/new", name="anomaly_pl_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        return parent::newAction($projectId);
    }

    /**
     * List all PIPELINE locations related to a project
     * @param $projectId
     * @return array
     *
     * @Route("/locations", name="anomaly_pl_locations")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:PipelineAnomaly:locations.html.twig")
     */
    public function selectLocationVisualAction($projectId)
    {
        return [
            'form' => $this->createLocationsForm($projectId)->createView(),
        ];
    }

    /**
     * Creates a form to create a PipelineAnomaly entity.
     *
     * @param PipelineAnomaly $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createLocationsForm($projectId)
    {
        //get list of locations
        $em = $this->getManager();

        /** @var $project Projects */
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project.');
        }

        $locations = array_filter(
            $project->getLocations()->toArray(),
            function ($location) {
                return $location instanceof Pipelines;
            }
        );

        //create the form
        $form = $this->createForm(
            new PipelineAnomalyLocationsType($locations),
            null,
            [
                'action' => $this->generateUrl('anomaly_pl_vs', ['projectId' => $projectId]),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Select', 'attr' => ['class' => 'right']], 'btn'));


        return $form;
    }


    /**
     * Shows the graph summary of the pipeline anomalies for project
     *
     * @Route("/visual", name="anomaly_pl_vs")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:VisualSummary:pipeline.html.twig")
     */
    public function visualSummaryAction($projectId, Request $request)
    {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $form = $this->createLocationsForm($projectId);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->redirect($this->generateUrl('anomaly_pl_locations', ['projectId' => $projectId]));
        }

        $data = $form->getData();
        $location = $data['location'];

        $anomalies = $entity->getRegistrars()->filter(
            function ($anomaly) use ($location) {
                if (!($anomaly instanceof PipelineAnomaly)) {
                    return false;
                }

                $actionsRequired = $anomaly->getActionRequired();
                $actionsChoices = array_flip($anomaly->getActionsChoices());


                $inspectionId = $actionsChoices['Anomaly Inspection'];

                return $anomaly->getStatusTag() != 'new' && $anomaly->getLocation() == $location
                && in_array($inspectionId, $actionsRequired, true); //has action set up
            } // only approved anomalies
        )->toArray();

        $fcg = $this->plotFailureCurveGraph($anomalies, $location);
        $data1 = $fcg['points']; // points
        $data2 = $fcg['curves']; // curves
        $data3 = $this->plotKPCRGraph($anomalies, '2009');

        return [
            'entity' => $entity,
            'anomalies' => $anomalies,
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3,
            'length' => $location->getLength(),
            'projectId' => $projectId,
        ];
    }

    private function plotFailureCurveGraph($anomalies, $location)
    {
        $data1 = []; // for first plot, points
        $data2 = []; // for first plot, curves

        // plot anomalies points
        //apply stats to anomalies
        $anomalies = array_map(
            function (PipelineAnomaly $anomaly) use (&$data1, &$data2, &$data3) {

                $monitors = $anomaly->getMonitors();
                array_map(
                    function ($monitor) use ($anomaly, &$data1) {

                        $point = new \stdClass();

                        // depth = NWT - Retial Thickness
                        $point->depth = $anomaly->getNominalWT() - $monitor->getTrackingValue();
                        $point->year = $monitor->getInspectionDate()->format('Y');
                        $point->length = $monitor->getLength();

                        $data1[$point->year] = (isSet($data1[$point->year])) ? $data1[$point->year] : []; //set default
                        $data1[$point->year][] = $point;
                    },
                    $monitors->toArray()
                );


//			$data2[ $anomaly->getLength() ] = (isSet($data2[ $anomaly->getLength() ])) ? $data2[ $anomaly->getLength() ] : []; //set default
//			$anomaly->maop = $maop;
//			$data2[ $anomaly->getLength() ][ ] = $anomaly;
//

                return $anomaly;
            },
            $anomalies
        );


        // plot curves
        $curves_plotted = false;
        while (!$curves_plotted) {
            break;
            $l = 0; //length (incremental)
            $D = $location->getOuterDiameter();
            $t = $anomaly->getNominalWT();
            $tolerance = ($anomaly->getTolerance() !== null) ?: $t - (0.875 * $t);
            $z = ($l * $l) / ($D * ($t - $tolerance));


            if ($z <= 50) {
                $m = pow(1 + 0.6274 * $z - 0.003375 * $z * $z, 0.5);
            } else { // if z > 50
                $m = 0.032 * $z + 3.3;
            }

            $sflow = 1.1 * $anomaly->getSMYS();

            $maop = $location->getMAOP();
            $sf = (1 / $location->getDesignFactor());


            $anomaly->map = 0.85 * $t; // maximum allowable depth

            $anomaly->Dmax = (
                    ((2 * ($t - $tolerance) * $sflow) - ($sf * $maop * $D))
                    /
                    ((2 * ($t - $tolerance) * $sflow * 0.85) - (($sf * $maop * $D * 0.85) / $m))
                ) * $t; //blue curve

            if ($anomaly->Dmax > $anomaly->map) {
                $anomaly->Dmax = $anomaly->map;
            }


            //for green curve
            //for all failure curves we substitute maop with 1
            $anomaly->fp = (
                    ((2 * ($t - $tolerance) * $sflow) - ($sf * 1 * $D))
                    /
                    ((2 * ($t - $tolerance) * $sflow * 0.85) - (($sf * 1 * $D * 0.85) / $m))
                ) * $t;
        }

        return ['points' => $data1, 'curves' => $data2];
    }

    private function plotKPCRGraph($anomalies, $year = null)
    {

        if ($year == null) {
            $year = new \DateTime();
            $year = $year->format('Y');
        }


        $data = [];

        $anomalies = array_map(
            function (PipelineAnomaly $anomaly) use ($year, &$data) {
                // first graph is done... now moving to second
                $monitors = $anomaly->getMonitors();
                $monitors = $monitors->toArray();

                $N = count($monitors) - 1;
                while ($N > 0 && $monitors[$N]->getInspectionDate()->format('Y') != $year) {
                    unset($monitors[$N]);
                    $N--;
                }

                if (count($monitors) > 1) {//if has more than one monitor [ type should be inspection ]
                    $N = count($monitors) - 1;
                    $length = $anomaly->getLocationPoint();

                    $nm1_date = $monitors[$N - 1]->getInspectionDate();

                    $datetime1 = $monitors[$N]->getInspectionDate();
                    $datetime2 = $nm1_date;
                    $interval = $datetime1->diff($datetime2); //no absolute , true);
                    $date_time_diff = (int)$interval->format('%a'); //the difference in days
                    $date_time_diff = (float)($date_time_diff / 365); // convert it to year


                    $cr = (
                            (float)$monitors[$N - 1]->getTrackingValue() - (float)$monitors[$N]->getTrackingValue())
                        /
                        ($date_time_diff);
                    $data[$length] = (isSet($data[$length])) ? $data[$length] : []; //set default
                    $data[$length][] = $cr;

                }
            },
            $anomalies
        );

        return $data;
    }

    /**
     * Finds and displays a PipelineAnomaly entity.
     *
     * @Route("/{id}", name="anomaly_pl_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {
        return parent::showAction($projectId, $id);
    }

    /**
     * Displays a form to edit an existing PipelineAnomaly entity.
     *
     * @Route("/{id}/edit", name="anomaly_pl_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {
        return parent::editAction($projectId, $id);
    }

    /**
     * Edits an existing PipelineAnomaly entity.
     *
     * @Route("/{id}", name="anomaly_pl_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:PipelineAnomaly:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        return parent::updateAction($request, $projectId, $id);
    }

    /**
     * Deletes a PipelineAnomaly entity.
     *
     * @Route("/{id}", name="anomaly_pl_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {
        return parent::deleteAction($request, $projectId, $id);
    }

    /**
         * Finds and displays a PipelineAnomaly entity.
         *
         * @Route("/{id}/delete", name="anomaly_pl_show_delete")
         * @Method("GET")
         * @Template()
         */
    public function showdeleteAction(Request $request, $projectId, $id)
    {
        return parent::showdeleteAction($request, $projectId, $id);
    }

    /**

    /**
     * @param $projectId
     * @param $id
     *
     * @Route("/{id}/duplicate", name="anomaly_pl_duplicate")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:PipelineAnomaly:edit.html.twig")
     */
    public function duplicateAction($projectId, $id)
    {
        return parent::duplicateAction($projectId, $id);
    }

    /**
     * Creates a new PipelineAnomaly entity.
     *
     * @Route("/{id}/clone", name="anomaly_pl_clone")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:PipelineAnomaly:edit.html.twig")
     */
    public function cloneAction($projectId, Request $request, $id)
    {
        return parent::cloneAction($projectId, $request, $id);
    }
}