<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;



class DefaultController extends AnomalyBaseController
{
    /**
     *
     *
     * @Route("/", name="anomaly_index")
     * @Method("get")
     * @Template("AIEAnomalyBundle:Default:index.html.twig")
     */

    public function indexAction()
    {
        $em = $this->getManager();
        $projects = $this->getGrantedProjects();
        $projectsRisks = [];
        $planned_status=[];
        $fromDate =  new \DateTime('first day of this month');
        $toDate  = new \DateTime('last day of this month');
        $toDateU = (int)$toDate->format('U');
        $fromDateU = (int)$fromDate->format('U');
        $checkArray=['PI',"PL","ST","SE","MR"];
        foreach ($projects as $project) {
            $risks = [];
            $registrars = $project->getRegistrars();
            $in = 0;
            $tro=0;
            $ro=0;
            $fm=0;
            $as=0;
            $liveAnomalies=0;
            foreach ($registrars as $registrar) {
                $requests = $registrar->getRequests()->toArray();
                // analyze only anomalies that have at least 1 approved request
                if (count($requests) == 0 || count(
                        array_filter(
                            $requests,
                            function ($r) {
                                return $r->getStatus() == 1;
                            }
                        )
                    ) == 0
                ) {
                    continue;
                }
                $status = $registrar->getStatus();

               /* $dueDateU = 0;
                if (method_exists($registrar, 'getNextInspectionDate')) {
                    if ($registrar->getNextInspectionDate()) {
                        $dueDateU = (int)$registrar->getNextInspectionDate()->format('U');
                    }
                };*/
                if ($status > 0) { // anomaly is closed/canceled
                    continue;
                }
                if ($registrar instanceof AnomalyRegistrar) {
                    if (in_array($registrar->getCode(),$checkArray)){



                        if($registrar->getStatusTag() == "final"){
                            if(!$registrar->getIsFabricMaintenanceCriticality()){
                                $risk = $registrar->getRisk();
                                $risks[$risk] = isSet($risks[$risk]) ? $risks[$risk] + 1 : 1;
                            }
                            $liveAnomalies += 1;
                        }

                        if (in_array(0, $registrar->getActionRequired()) && $registrar->getStatusTag() == "final") { // check if inspection is set

                            $in = $in +1; //($dueDateU >= $fromDateU /*&& $dueDateU <= $toDateU*/);
                        }
                    }
                } elseif ($registrar instanceof RepairOrderRegistrar) {
                    if ($registrar->getStatusTag() == "final") {
                        $ro = $ro + 1;//($dueDateU >= $fromDateU /*&& $dueDateU <= $toDateU*/);
                    }

                } elseif ($registrar instanceof TemporaryRepairOrderRegistrar) {
                    if ($registrar->getStatusTag() == "final") {
                        $tro = $tro + 1;//($dueDateU >= $fromDateU /*&& $dueDateU <= $toDateU*/);
                    }
                } elseif ($registrar instanceof FabricMaintenanceRegistrar) {
                    if ($registrar->getStatusTag() == "final") {
                        $fm = $fm + 1;//($dueDateU >= $fromDateU /*&& $dueDateU <= $toDateU*/);
                    }
                } elseif ($registrar instanceof AssessmentRegistrar) {
                    if ($registrar->getStatusTag() == "final") {
                        $as = $as + 1;//($dueDateU >= $fromDateU /*&& $dueDateU <= $toDateU*/)
                        /*if ($registrar->getAssessmentDueDate()) {
                            $dueDateU = (int)$registrar->getAssessmentDueDate()->format('U');
                        }*/
                    }
                }
            }

            $projectsRisks[$project->getId()]=[
                'risks' => $risks
            ];
            $planned_status[$project->getId()] = ['in' => $in, 'ro' => $ro,'tro' => $tro, 'fm' => $fm,'as'=>$as, 'live' => $liveAnomalies];
        }
        return ['projects' => $projects, 'project_risks'=>$projectsRisks, 'planned_status'=>$planned_status];
    }
}
