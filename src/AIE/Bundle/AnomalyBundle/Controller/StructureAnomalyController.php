<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\StructureAnomaly;
use AIE\Bundle\AnomalyBundle\Form\StructureAnomalyType;


/**
 * StructureAnomaly controller.
 *
 * @Route("projects/{projectId}/structure")
 */
class StructureAnomalyController extends AnomalyRegistrarController {

	protected $registrarClassName = 'StructureAnomaly';
	protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\StructureAnomaly';
	protected $registrarRouteBase = 'anomaly_st';
	protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\StructureAnomalyType';

    /**
     * Deletes a PipingAnomaly entity.
     *
     * @Route("/{id}/remove", name="anomaly_st_remove")
     * @Method("GET")
     */
    public function deleteIndividualAction(Request $request, $projectId, $id) {
        return parent::deleteIndividualAction($request, $projectId, $id);
    }

    /**
     * Show the request history of an anomaly
     *
     * @Route("/{id}/history", name="st_registrar_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Registrar:history.html.twig")
     */
    public function historyAction($id)
    {
        return parent::historyAction($id);
    }

	/**
	 * Lists all StructureAnomaly entities.
	 *
	 * @Route("/", name="anomaly_st")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function indexAction(Request $request, $projectId)
    {
        return parent::indexAction($request, $projectId);
    }

	/**
	 * Creates a new StructureAnomaly entity.
	 *
	 * @Route("/new", name="anomaly_st_create")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:StructureAnomaly:new.html.twig")
	 */
	public function createAction(Request $request, $projectId) {
		return parent::createAction($request, $projectId);
	}

	protected function setNewEntityFields(&$entity, $data) {
		parent::setNewEntityFields($entity, $data);
	}


	/**
	 * Displays a form to create a new StructureAnomaly entity.
	 *
	 * @Route("/new", name="anomaly_st_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($projectId) {
		return parent::newAction($projectId);
	}


	/**
	 * Shows the graph summary of the structure anomalies for project
	 *
	 * @Route("/visual", name="anomaly_st_vs")
	 * @Method("GET")
	 * @Template("AIEAnomalyBundle:VisualSummary:structure.html.twig")
	 */
	public function visualSummaryAction($projectId, Request $request) {
		return parent::visualSummaryAction($projectId, $request);
	}

	/**
	 * Finds and displays a StructureAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_st_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($projectId, $id) {
		return parent::showAction($projectId, $id);
	}

	/**
	 * Displays a form to edit an existing StructureAnomaly entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_st_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($projectId, $id) {
		return parent::editAction($projectId, $id);
	}

	/**
	 * Edits an existing StructureAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_st_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:StructureAnomaly:edit.html.twig")
	 */
	public function updateAction(Request $request, $projectId, $id) {
		return parent::updateAction($request, $projectId, $id);
	}

	/**
	 * Deletes a StructureAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_st_delete")
	 * @Method("DELETE")
	 */
    public function deleteAction(Request $request, $projectId, $id) {
        return parent::deleteAction($request, $projectId, $id);
	}

    /**
     * Finds and displays a StructureAnomaly entity.
     *
     * @Route("/{id}/delete", name="anomaly_st_show_delete")
     * @Method("GET")
     * @Template()
     */
    public function showdeleteAction(Request $request, $projectId, $id)
    {
        return parent::showdeleteAction($request, $projectId, $id);
    }

	/**
	 * @param $projectId
	 * @param $id
	 *
	 * @Route("/{id}/duplicate", name="anomaly_st_duplicate")
	 * @Method("GET")
	 * @Template("AIEAnomalyBundle:StructureAnomaly:edit.html.twig")
	 */
	public function duplicateAction($projectId, $id) {
		return parent::duplicateAction($projectId, $id);
	}

	/**
	 * Creates a new StructureAnomaly entity.
	 *
	 * @Route("/{id}/clone", name="anomaly_st_clone")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:StructureAnomaly:edit.html.twig")
	 */
	public function cloneAction($projectId, Request $request, $id)
    {
        return parent::cloneAction($projectId, $request, $id);
    }
}