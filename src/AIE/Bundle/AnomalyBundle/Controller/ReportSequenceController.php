<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\VeracityBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\ActionMonitor;
use AIE\Bundle\AnomalyBundle\Form\ActionMonitorType;

use AIE\Bundle\AnomalyBundle\Entity\ReportSequence;

/**
 * ReportSequence
 *
 * controller.
 *
 * @Route("/reportsequence")
 */
class ReportSequenceController extends AnomalyBaseController {

	/**
	 * Lists all ReportSequence entities.
	 *
	 * @Route("/{projectId}/{reportTypeId}", name="anomaly_reportsequence_increase")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction($projectId, $reportTypeId) {
        $em = $this->getManager();
        $resultArray=['message'=>''];
        try {
            $sequence = $em->getRepository('AIEAnomalyBundle:ReportSequence')->findOneBy(array('project'=>$projectId,'reportType'=>$reportTypeId));
            if($sequence){
                $seq=$sequence->getCurrentSequence()+1;
                $resultArray['message']= $this->formatSequence($seq);
                $sequence->setCurrentSequence($seq);

            }else{
                $seq=1;
                $sequence = new ReportSequence();
                $resultArray['message']= $this->formatSequence($seq);
                $sequence->setCurrentSequence($seq);
                $sequence->setProject($em->getRepository('AIEAnomalyBundle:Projects')->find($projectId));
                $sequence->setReportType(1);
                $sequence->setReportName('Dashboard Report');
            }
            $em->persist($sequence);
            $em->flush();
        } catch (Exception $e) {
            $resultArray['message']="error";
        }
        $response = new Response(json_encode($resultArray));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
	}

	public function formatSequence($seq){
	    if(strlen($seq) > 4 || $seq > 9999){
            $seq = 1;
        }
        $sttVal = "";
        for($i = strlen($seq)-1; $i<3;$i++){
            $sttVal = $sttVal . "0";
        }
        $sttVal = $sttVal . $seq;
        return $sttVal;
    }

}
