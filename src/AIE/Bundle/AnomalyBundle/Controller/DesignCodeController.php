<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\DesignCode;
use AIE\Bundle\AnomalyBundle\Form\DesignCodeType;

/**
 * DesignCode controller.
 *
 * @Route("/codes")
 */
class DesignCodeController extends AnomalyBaseController
{

    /**
     * Lists all DesignCode entities.
     *
     * @Route("/", name="anomaly_designcode")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:DesignCode')->findAll();
        $codes = [];
        array_walk(
            $entities,
            function (DesignCode $code) use (&$codes) {
                $codes[$code->getType()][] = $code;
            }
        );

        return array(
            'codes' => $codes,
            'anomalyCategories' => AnomalyRegistrar::getAnomalyCategories(),
            'entities' => $entities,
        );
    }

    /**
     * Creates a new DesignCode entity.
     *
     * @Route("/", name="anomaly_designcode_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:DesignCode:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new DesignCode();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_designcode_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a DesignCode entity.
     *
     * @param DesignCode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(DesignCode $entity)
    {
        $form = $this->createForm(
            new DesignCodeType(),
            $entity,
            array(
                'action' => $this->generateUrl('anomaly_designcode_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new DesignCode entity.
     *
     * @Route("/new", name="anomaly_designcode_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new DesignCode();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a DesignCode entity.
     *
     * @Route("/{id}", name="anomaly_designcode_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:DesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DesignCode entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing DesignCode entity.
     *
     * @Route("/{id}/edit", name="anomaly_designcode_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:DesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DesignCode entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a DesignCode entity.
     *
     * @param DesignCode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(DesignCode $entity)
    {
        $form = $this->createForm(
            new DesignCodeType(),
            $entity,
            array(
                'action' => $this->generateUrl('anomaly_designcode_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));


        return $form;
    }

    /**
     * Edits an existing DesignCode entity.
     *
     * @Route("/{id}", name="anomaly_designcode_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:DesignCode:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:DesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DesignCode entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_designcode_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

        /**
     * Deletes a designcode entity.
     *
     * @Route("/{id}", name="anomaly_designcode_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:DesignCode')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DesignCode entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_designcode'));
    }

    /**
     * Creates a form to delete a designcode entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_designcode_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm()
        ;
    }

}
