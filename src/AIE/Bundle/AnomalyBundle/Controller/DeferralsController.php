<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Deferrals;
use AIE\Bundle\AnomalyBundle\Form\DeferralsType;
use AIE\Bundle\AnomalyBundle\Form\DeferralsEditType;

/**
 * Deferrals controller.
 *
 * @Route("projects/{projectId}/deferrals")
 */
class DeferralsController extends AnomalyBaseController
{

    /**
     * Lists all Deferrals entities.
     *
     * @Route("/", name="anomaly_deferrals")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {

        $editVisible = 0;
        $createVisible = 0;
        $deleteVisible = 0;

        if (!$this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));


        //Checking if user has permission to edit, add, delete
        if ($this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_DELETE', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $deleteVisible = 1;
        }



        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:Deferrals')->findByProject($projectId);
        $users = $this->get('aie_anomaly.user.helper')->getProjectUsers($projectId);

        array_walk(
            $entities,
            function (Deferrals $entity) use ($users) {
                $entity->setAnomalyCategory(AnomalyRegistrar::getAnomalyCategories()[$entity->getAnomalyCategory()]);
                //$entity->setActionCategory(Deferrals::getActionsChoices()[$entity->getActionCategory()]);
                $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));
            }
        );

        return [
            'entities' => $entities,
            'projectId' => $projectId,
            'editGranted' => $editVisible,
            'createGranted' => $createVisible,
            'deleteGranted' => $deleteVisible,
        ];
    }

    /**
     * Creates a new Deferrals entity.
     *
     * @Route("/", name="anomaly_deferrals_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Deferrals:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new Deferrals();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

         $em = $this->getManager();

        if ($form->isValid()) {
            $em = $this->getManager();
            $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

            $deferralsSPAPost = $this->get('request')->request->get('aie_bundle_anomalybundle_deferrals');

            // spa is not loaded on dropdown list
            if(!array_key_exists('actionOwner', $deferralsSPAPost)){
                throw new \Exception("Deferral SPA is missing!");
                exit;
            }

            $userActionSpa = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->find($deferralsSPAPost['actionOwner']);

            //$defUserExist = $em->getRepository('AIEAnomalyBundle:Deferrals')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$deferralsSPAPost['anomalyCategory'],'actionCategory'=>$deferralsSPAPost['actionCategory']));

            $defUserExist = $em->getRepository('AIEAnomalyBundle:Deferrals')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$deferralsSPAPost['anomalyCategory']));
           
            if (!$defUserExist)
            {
                $entity->setActionOwner($userActionSpa);
                $entity->setProject($project);
                $em->merge($entity);
                $em->flush();
            } 
            else
            {
                $this->get('session')->getFlashBag()->add('error', "Oops! This Deferral Owner for the category is already in use.");
            }

            return $this->redirect($this->generateUrl('anomaly_deferrals', ['projectId' => $projectId]));
            
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
        ];
    }


    /**
     * Creates a form to create a Deferrals entity.
     *
     * @param Deferrals $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Deferrals $entity, $projectId)
    {

        $anomalyem = $this->getManager('anomaly');
        $em = $this->getManager();
        $actionSpas = array();
        //$actionOwners = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($projectId);
        //$this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $actionSpas = $this->get('aie_anomaly.user.helper')->getProjectSpas($projectId,'ROLE_DEFERRALOWNER_SPA');
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $form = $this->createForm(
            new DeferralsType($actionSpas, $project->getProjectApprovalLevels()),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_deferrals_create', ['projectId' => $projectId]),
                'method' => 'POST',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Displays a form to create a new Deferrals entity.
     *
     * @Route("/new", name="anomaly_deferrals_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $entity = new Deferrals();
        $form = $this->createCreateForm($entity, $projectId);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
        ];
    }

    /**
     * Finds and displays a Deferrals entity.
     *
     * @Route("/{id}", name="anomaly_deferrals_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Deferrals')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Deferrals entity.');
        }

        $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));

        $deleteForm = $this->createDeleteForm($projectId, $id);

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Deferrals entity.
     *
     * @Route("/{id}/edit", name="anomaly_deferrals_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS_EDIT',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Deferrals')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Deferrals entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $deleteForm = $this->createDeleteForm($projectId, $id);

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a Deferrals entity.
     *
     * @param Deferrals $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Deferrals $entity, $projectId)
    {
        $em = $this->getManager();
        $defaultEm = $this->getDoctrine()->getManager();

        $actionSpas = $this->get('aie_anomaly.user.helper')->getProjectSpas($projectId,'ROLE_DEFERRALOWNER_SPA');
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $actionOwnerInfo = $defaultEm->getRepository('UserBundle:User')->find($entity->getActionOwner()->getId());

        $form = $this->createForm(
            new DeferralsType($actionSpas, $project->getProjectApprovalLevels(),false,$actionOwnerInfo->getUsername()),
            $entity,
            [
                'action' => $this->generateUrl(
                    'anomaly_deferrals_update',
                    ['id' => $entity->getId(), 'projectId' => $projectId]
                ),
                'method' => 'PUT',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Submit', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Edits an existing Deferrals entity.
     *
     * @Route("/{id}", name="anomaly_deferrals_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Deferrals:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Deferrals')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Deferrals entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);
        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_deferrals', ['projectId' => $projectId]));
        }

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a Deferrals entity.
     *
     * @Route("/{id}", name="anomaly_deferrals_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:Deferrals')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Deferrals entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_deferrals', ['projectId' => $projectId]));
    }

    /**
     * Creates a form to delete a Deferrals entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_deferrals_delete', ['id' => $id, 'projectId' => $projectId]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }
}
