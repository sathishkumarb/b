<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 07/01/16
 * Time: 00:32
 */

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\Request;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Request controller.
 *
 * @Route("/reports/{type}", defaults={"type"=null}, requirements={"type"="(docx|pdf|summary-pdf|summary-docx)?"})
 */
class ReportController extends AnomalyBaseController {

	/**
	 * Finds and displays a Request entity.
	 *
	 * @Route("/{id}", name="anomaly_report_download")
	 * @Method("GET")
	 * @Template()
	 */
	public function downloadAction($id, $type) {
		$em = $this->getManager();

		/** @var Registrar $entity */
		$request = $em->getRepository('AIEAnomalyBundle:Request')->find($id);
		$entity = $request->getRegistrar();
		if (! $request) {
			throw $this->createNotFoundException('Unable to find Request entity.');
		}

		/** set report name */
		$reportName = FileHelper::cleanFileName($entity->getFullId());
		if($entity instanceof AnomalyRegistrar) $reportName = 'AR-' . $reportName;
        $uploader = $this->get('storage.file_uploader');
        $ext = 'pdf';
        if ($type=='pdf')
            $content = $uploader->read($request->getPdfFilePath());
        if ($type=='docx'){
            $ext='docx';
            $content = $uploader->read($request->getFilepath());
        }
        if ($type=='summary-pdf')
            $content = $uploader->read($request->getSummaryPdfFilePath());
		$response = new Response();
		$d = $response->headers->makeDisposition(

			ResponseHeaderBag::DISPOSITION_ATTACHMENT, "{$reportName}" . '.' . $ext
		);
		$response->headers->set('Content-Disposition', $d);
		$response->setContent($content);

		return $response;
	}
}