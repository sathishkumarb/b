<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners;
use AIE\Bundle\AnomalyBundle\Form\ApprovalOwnersType;

/**
 * ApprovalOwners controller.
 *
 * @Route("projects/{projectId}/approvalowners")
 */
class ApprovalOwnersController extends AnomalyBaseController
{

    private $layout = 'AIEAnomalyBundle:Default:layout.html.twig';
    private $route_base = 'anomaly_approvalowners';
    /**
     * Lists all ApprovalOwners entities.
     *
     * @Route("/", name="anomaly_approvalowners")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {
        $editVisible = 0;
        $createVisible = 0;
        $deleteVisible = 0;

        if (!$this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));

        //Checking if user has permission to edit, add, delete
        if ($this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_DELETE', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $deleteVisible = 1;
        }


        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findByProject($projectId);
        $users = $this->get('aie_anomaly.user.helper')->getProjectUsers($projectId);
        
        array_walk(
            $entities,
            function (ApprovalOwners $entity) use ($users) {
                $entity->setAnomalyCategory(AnomalyRegistrar::getAnomalyCategories()[$entity->getAnomalyCategory()]);
                //$entity->setActionCategory(ApprovalOwners::getActionsChoices()[$entity->getActionCategory()]);
                $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));
            }
        );


        return [
            'entities' => $entities,
            'projectId' => $projectId,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
            'editGranted' => $editVisible,
            'createGranted' => $createVisible,
            'deleteGranted' => $deleteVisible,
        ];
    }

    /**
     * Creates a new ApprovalOwners entity.
     *
     * @Route("/", name="anomaly_approvalowners_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:ApprovalOwners:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new ApprovalOwners();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

            $approvalsSPAPost = $this->get('request')->request->get('aie_bundle_anomalybundle_approvalowners');

            // spa is not loaded on dropdown list
            if(!array_key_exists('actionOwner', $approvalsSPAPost)){
                throw new \Exception("Approval SPA is missing!");
                exit;
            }

            $userActionSpa = $this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($approvalsSPAPost['actionOwner']);

            //$defUserExist = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$approvalsSPAPost['anomalyCategory'],'actionCategory'=>$approvalsSPAPost['actionCategory']));
            $defUserExist = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findBy(array('actionOwner' =>$userActionSpa->getId(),'project' => $project,'anomalyCategory'=>$approvalsSPAPost['anomalyCategory']));

            if (!$defUserExist) {
                $entity->setActionOwner($userActionSpa);
                $entity->setProject($project);
                $em->merge($entity);
                $em->flush();
                
            } else{
                $this->get('session')->getFlashBag()->add('error', "Oops! This Approval Owner for the category is already in use.");
            }

            return $this->redirect($this->generateUrl('anomaly_approvalowners', ['projectId' => $projectId]));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
        ];
    }

    /**
     * Creates a form to create a ApprovalOwners entity.
     *
     * @param ApprovalOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ApprovalOwners $entity, $projectId)
    {

        $em = $this->getManager();
        $actionSpas = array();
        //$actionOwners = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($projectId);
        //$this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $actionSpas = $this->get('aie_anomaly.user.helper')->getProjectSpas($projectId,'ROLE_APPROVALOWNER_SPA');

        $form = $this->createForm(
            new ApprovalOwnersType($actionSpas, $project->getProjectApprovalLevels()),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_approvalowners_create', ['projectId' => $projectId]),
                'method' => 'POST',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Displays a form to create a new ApprovalOwners entity.
     *
     * @Route("/new", name="anomaly_approvalowners_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        $entity = new ApprovalOwners();
        $form = $this->createCreateForm($entity, $projectId);

        if (!$this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
        ];
    }

    /**
     * Finds and displays a ApprovalOwners entity.
     *
     * @Route("/{id}", name="anomaly_approvalowners_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }


        $deleteForm = $this->createDeleteForm($projectId, $id);

        $entity->setUser($this->getDoctrine()->getManager('default')->getRepository('UserBundle:User')->find($entity->getActionOwner()));

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing ApprovalOwners entity.
     *
     * @Route("/{id}/edit", name="anomaly_approvalowners_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_EDIT',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $deleteForm = $this->createDeleteForm($projectId, $id);

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a ApprovalOwners entity.
     *
     * @param ApprovalOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ApprovalOwners $entity, $projectId)
    {
        $em = $this->getManager();
        $defaultEm = $this->getDoctrine()->getManager();
        $actionOwners = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($projectId);
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $actionOwnerInfo = $defaultEm->getRepository('UserBundle:User')->find($entity->getActionOwner()->getId());

        $form = $this->createForm(
            new ApprovalOwnersType($actionOwners, $project->getProjectApprovalLevels(),false,$actionOwnerInfo->getUsername()),
            $entity,
            [
                'action' => $this->generateUrl(
                    'anomaly_approvalowners_update',
                    ['id' => $entity->getId(), 'projectId' => $projectId]
                ),
                'method' => 'PUT',
            ]
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Edits an existing ApprovalOwners entity.
     *
     * @Route("/{id}", name="anomaly_approvalowners_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:ApprovalOwners:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);
        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_approvalowners', ['projectId' => $projectId]));
        }

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a ApprovalOwners entity.
     *
     * @Route("/{id}", name="anomaly_approvalowners_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS_DELETE',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ApprovalOwners entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_approvalowners', ['projectId' => $projectId]));
    }

    /**
     * Creates a form to delete a ApprovalOwners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_approvalowners_delete', ['id' => $id, 'projectId' => $projectId]))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
    }
}
