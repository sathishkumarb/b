<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Locations;
use AIE\Bundle\AnomalyBundle\Form\LocationsType;
use AIE\Bundle\AnomalyBundle\Entity\Pipelines;
/**
 * Locations controller.
 *
 * @Route("projects/{projectId}/locations")
 */
class LocationsController extends AnomalyBaseController
{

    protected $editVisible = 0;
    protected $createVisible = 0;
    protected $showVisible = 0;
    protected $deleteVisible = 0;

    /**
     * Lists all Locations entities.
     *
     * @Route("/", name="anomaly_locations")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {

        //Checking if user has permission to edit, add, delete, show
        if ($this->securityHelper->isRoleGranted('ROLE_LOCATION_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_LOCATION_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_LOCATION_SHOW', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->showVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_LOCATION_DELETE', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->deleteVisible = 1;
        }

        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:Locations')->findByProject($projectId);

	    #remove pipeline locations
	    $entities = array_filter($entities, function($entity){ return !$entity instanceof Pipelines;});

        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'editGranted' => $this->editVisible,
            'createGranted' => $this->createVisible,
            'showGranted' => $this->showVisible,
            'deleteGranted' => $this->deleteVisible,
        );
    }
    /**
     * Creates a new Locations entity.
     *
     * @Route("/", name="anomaly_locations_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Locations:new.html.twig")
     */
    public function createAction($projectId, Request $request)
    {
        $entity = new Locations();
        $form = $this->createCreateForm($projectId, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity->setProject($em->getRepository('AIEAnomalyBundle:Projects')->find($projectId));
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_locations_show', array('projectId' => $projectId, 'id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Locations entity.
     *
     * @param Locations $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId, Locations $entity)
    {
        $form = $this->createForm(new LocationsType(), $entity, array(
            'action' => $this->generateUrl('anomaly_locations_create', ['projectId'=>$projectId]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Locations entity.
     *
     * @Route("/new", name="anomaly_locations_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_LOCATION_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $entity = new Locations();
        $form   = $this->createCreateForm($projectId, $entity);

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Locations entity.
     *
     * @Route("/{id}", name="anomaly_locations_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_LOCATION_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Locations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Locations entity.');
        }

        $request = $this->getRequest();
        $delFlag = $request->get('d'); 

        if ( $delFlag == 'Y' )
        {
            $deleteForm = $this->createDeleteForm($projectId, $id);
            return array(
                'entity'      => $entity,
                'projectId' => $projectId,
                'delete_form' => $deleteForm->createView(),
            );
        }
        else
        {
             return array(
                'entity'      => $entity,
                'projectId' => $projectId,
            );
        }
    }

    /**
     * Displays a form to edit an existing Locations entity.
     *
     * @Route("/{id}/edit", name="anomaly_locations_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_LOCATION_EDIT',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Locations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Locations entity.');
        }

        $editForm = $this->createEditForm($projectId, $entity);
        $deleteForm = $this->createDeleteForm($projectId, $id);

        return array(
            'entity'      => $entity,
            'projectId' => $projectId,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Locations entity.
    *
    * @param Locations $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm($projectId, Locations $entity)
    {
        $form = $this->createForm(new LocationsType(), $entity, array(
            'action' => $this->generateUrl('anomaly_locations_update', array('projectId' => $projectId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }
    /**
     * Edits an existing Locations entity.
     *
     * @Route("/{id}", name="anomaly_locations_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Locations:edit.html.twig")
     */
    public function updateAction($projectId, Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Locations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Locations entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);
        $editForm = $this->createEditForm($projectId, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_locations_edit', array('projectId' => $projectId, 'id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'projectId' => $projectId,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Locations entity.
     *
     * @Route("/{id}", name="anomaly_locations_delete")
     * @Method("DELETE")
     */
    public function deleteAction($projectId, Request $request, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_LOCATION_DELETE',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:Locations')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Locations entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_locations', ['projectId' => $projectId]));
    }

    /**
     * Creates a form to delete a Locations entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_locations_delete', array('projectId' => $projectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
            ->getForm();
        ;
    }
}
