<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Classifications;
use AIE\Bundle\AnomalyBundle\Form\ClassificationsType;

/**
 * Classifications controller.
 *
 * @Route("/classifications")
 */
class ClassificationsController extends AnomalyBaseController
{

    /**
     * Lists all Classifications entities.
     *
     * @Route("/", name="anomaly_classifications")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:Classifications')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Classifications entity.
     *
     * @Route("/", name="anomaly_classifications_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Classifications:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Classifications();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_classifications_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Classifications entity.
     *
     * @param Classifications $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Classifications $entity)
    {
        $form = $this->createForm(new ClassificationsType(), $entity, array(
            'action' => $this->generateUrl('anomaly_classifications_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Classifications entity.
     *
     * @Route("/new", name="anomaly_classifications_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Classifications();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Classifications entity.
     *
     * @Route("/{id}", name="anomaly_classifications_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Classifications')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Classifications entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Classifications entity.
     *
     * @Route("/{id}/edit", name="anomaly_classifications_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Classifications')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Classifications entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Classifications entity.
    *
    * @param Classifications $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Classifications $entity)
    {
        $form = $this->createForm(new ClassificationsType(), $entity, array(
            'action' => $this->generateUrl('anomaly_classifications_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }
    /**
     * Edits an existing Classifications entity.
     *
     * @Route("/{id}", name="anomaly_classifications_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Classifications:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Classifications')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Classifications entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_classifications_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Classifications entity.
     *
     * @Route("/{id}", name="anomaly_classifications_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:Classifications')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Classifications entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_classifications'));
    }

    /**
     * Creates a form to delete a Classifications entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_classifications_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm()
        ;
    }
}
