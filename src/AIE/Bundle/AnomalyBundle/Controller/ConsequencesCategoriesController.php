<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\ConsequencesCategories;
use AIE\Bundle\AnomalyBundle\Form\ConsequencesCategoriesType;

/**
 * ConsequencesCategories controller.
 *
 * @Route("/consequences")
 */
class ConsequencesCategoriesController extends AnomalyBaseController
{

    /**
     * Lists all ConsequencesCategories entities.
     *
     * @Route("/", name="anomaly_consequences")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new ConsequencesCategories entity.
     *
     * @Route("/", name="anomaly_consequences_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:ConsequencesCategories:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ConsequencesCategories();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity->setDate(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_consequences_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ConsequencesCategories entity.
     *
     * @param ConsequencesCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ConsequencesCategories $entity)
    {
        $form = $this->createForm(
            new ConsequencesCategoriesType(),
            $entity,
            array(
                'action' => $this->generateUrl('anomaly_consequences_create'),
                'method' => 'POST',
            )
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Displays a form to create a new ConsequencesCategories entity.
     *
     * @Route("/new", name="anomaly_consequences_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ConsequencesCategories();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ConsequencesCategories entity.
     *
     * @Route("/{id}", name="anomaly_consequences_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }
        
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ConsequencesCategories entity.
     *
     * @Route("/{id}/edit", name="anomaly_consequences_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ConsequencesCategories entity.
     *
     * @param ConsequencesCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ConsequencesCategories $entity)
    {
        $form = $this->createForm(
            new ConsequencesCategoriesType(),
            $entity,
            array(
                'action' => $this->generateUrl('anomaly_consequences_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Edits an existing ConsequencesCategories entity.
     *
     * @Route("/{id}", name="anomaly_consequences_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:ConsequencesCategories:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_consequences_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    
    /**
     * Deletes a Consequence entity.
     *
     * @Route("/{id}", name="anomaly_consequences_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->find($id);

            if (! $entity) {
                throw $this->createNotFoundException('Unable to find Consequences entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_consequences'));
    }

    /**
     * Creates a form to delete a ConsequencesCategories entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm( $id) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_consequences_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', ['label' => 'Delete'])
            ->getForm();
    }
}
