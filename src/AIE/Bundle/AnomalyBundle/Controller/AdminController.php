<?php
/**
 * User: mokha
 * Date: 3/13/16
 * Time: 12:58 AM
 */

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * consequences controller.
 *
 * @Route("/admin")
 */
class AdminController extends AnomalyBaseController {

    /**
     * Lists all consequences entities.
     *
     * @Route("/", name="anomaly_admin_dashboard")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:Threat')->findAll();

        return array(
            'entities' => $entities,
        );
    }

}