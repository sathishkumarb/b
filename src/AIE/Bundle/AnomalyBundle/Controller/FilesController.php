<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Files;
use AIE\Bundle\AnomalyBundle\Form\FilesType;
use AIE\Bundle\AnomalyBundle\Form\FilesEditType;
use AIE\Bundle\AnomalyBundle\Helper;

/**
 * Files controller.
 *
 * @Route("anomaly/{refId}/files")
 */
class FilesController extends AnomalyBaseController {

	private $layout = 'AIEAnomalyBundle:Default:layout.html.twig';
	private $route_base = 'anomaly_files';

	/**
	 * Lists all Files entities.
	 *
	 * @Route("/", name="anomaly_files")
	 * @Method("GET")
	 * @Template("AIEVeracityBundle:Files:index.html.twig")
	 */
	public function indexAction($refId) {
		$em = $this->getManager();

		$entities = $em->getRepository('AIEAnomalyBundle:Files')->findByAnomaly($refId);

		return [
			'entities'   => $entities,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'refId'      => $refId
		];
	}

	/**
	 * Creates a new Files entity.
	 *
	 * @Route("/", name="anomaly_files_create")
	 * @Method("POST")
	 * @Template("AIEVeracityBundle:Files:new.html.twig")
	 */
	public function createAction(Request $request, $refId) {
		$entity = new Files();
		$form = $this->createCreateForm($entity, $refId);
		$form->handleRequest($request);

		$em = $this->getManager();


		if ($form->isValid()) {
			$anomaly = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($refId);
			$entity->setAnomaly($anomaly);
			$data = $form->getData();
			/* Handle Uploaded File */
			$file = $data->getFile();
			$uploadedPath = $this->uploadFile($file, $entity);
			$entity->setFilepath($uploadedPath);
			$entity->setSize($file->getSize());

			$date = $data->getDocumentDate();
			if ($date) {
				$entity->setDocumentDate(new \DateTime($date));
			}
			$entity->setDate(new \DateTime());

			$entity->setFilereportCategory($data->getFilereportCategory());

			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('anomaly_files', ['refId' => $refId]));
			//return $this->redirect($this->generateUrl('anomaly_files_show', array('id' => $entity->getId(), 'refId' => $refId)));
		}

		return [
			'entity' => $entity,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'form'   => $form->createView(),
		];
	}

	/**
	 * Creates a form to create a Files entity.
	 *
	 * @param Files $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Files $entity, $refId) {
		$form = $this->createForm(new FilesType(), $entity, [
			'action' => $this->generateUrl('anomaly_files_create', ['refId' => $refId]),
			'method' => 'POST',
		]);
		
		$form->add('filereportcategory', 'choice', [
                'required' => true,
				'label' => 'Category',
                'position'    => ['after' => 'caption'],
				'label_attr' => ['class' => 'col-md-3 control-label'],
				'attr' => ['class' => 'form-control'],
                'choices' => ['P&ID' => 'P&ID', 'Isometric' => 'Isometric', 'Picture' => 'Picture','Inspection Report' => 'Inspection', 'Others' => 'Others'],
            ]);

		$form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Displays a form to create a new Files entity.
	 *
	 * @Route("/new", name="anomaly_files_new")
	 * @Method("GET")
	 * @Template("AIEVeracityBundle:Files:new.html.twig")
	 */
	public function newAction($refId) {
		$entity = new Files();
		$form = $this->createCreateForm($entity, $refId);

		return [
			'entity' => $entity,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'form'   => $form->createView(),
			'refId'  => $refId
		];
	}

	/**
	 * Finds and displays a Files entity.
	 *
	 * @Route("/{id}", name="anomaly_files_show")
	 * @Method("GET")
	 * @Template("AIEVeracityBundle:Files:show.html.twig")
	 */
	public function showAction($refId, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Files')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Files entity.');
		}

		// $deleteForm = $this->createDeleteForm($id);

		return [
			'entity' => $entity,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'refId'  => $refId
			//'delete_form' => $deleteForm->createView(),
		];
	}

	/**
	 * Displays a form to edit an existing Files entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_files_edit", options={"expose"=true})
	 * @Method("GET")
	 * @Template("AIEVeracityBundle:Files:edit.html.twig")
	 */
	public function editAction($id, $refId) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Files')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Files entity.');
		}

		$editForm = $this->createEditForm($entity, $refId);
		$deleteForm = $this->createDeleteForm($id, $refId);

		return [
			'entity'      => $entity,
			'refId'       => $refId,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		];
	}

	/**
	 * Creates a form to edit a Files entity.
	 *
	 * @param Files $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Files $entity, $refId) {
		$form = $this->createForm(new FilesEditType($entity), $entity, [
			'action' => $this->generateUrl('anomaly_files_update', ['id' => $entity->getId(), 'refId' => $refId]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Edits an existing Files entity.
	 *
	 * @Route("/{id}", name="anomaly_files_update")
	 * @Method("PUT")
	 * @Template("AIEVeracityBundle:Files:edit.html.twig")
	 */
	public function updateAction(Request $request, $id, $refId) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Files')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Files entity.');
		}

		$deleteForm = $this->createDeleteForm($id, $refId);
		$editForm = $this->createEditForm($entity, $refId);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {

			/* Handle Uploaded File */
			$data = $editForm->getData();
			/*$file = $data->getFile();
			$uploadedPath = $this->replaceFile($file, $entity);
			$entity->setFilepath($uploadedPath);
			$entity->setSize($file->getSize());*/

			$date = $data->getDocumentDate();
			if ($date) {
				$entity->setDocumentDate(new \DateTime($date));
			}

			$em->flush();

			return $this->redirect($this->generateUrl('anomaly_files', ['refId' => $refId]));
			//return $this->redirect($this->generateUrl('anomaly_files_edit', array('id' => $id, 'refId' => $refId)));
		}

		return [
			'entity'      => $entity,
			'route_base' => $this->route_base,
			'layout'     => $this->layout,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		];
	}

	/**
	 * Deletes a Files entity.
	 *
	 * @Route("/{id}/delete", name="anomaly_files_delete", options={"expose"=true})
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, $id, $refId) {

        if ($request->isXmlHttpRequest()) {

            $this->deleteFileData($id);
            return new JsonResponse(['status' => 200, 'success' => true]);
        }
        else
        {
            $form = $this->createDeleteForm($id, $refId);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->deleteFileData($id);
            }

            return $this->redirect($this->generateUrl('anomaly_files', ['refId' => $refId]));

        }
	}

	/**
	 * Creates a form to delete a Files entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id, $refId) {
		return $this->createFormBuilder()
			->setAction($this->generateUrl('anomaly_files_delete', ['id' => $id, 'refId' => $refId]))
			->setMethod('DELETE')
			->add('submit', 'submit', $this->options(['label' => 'Delete', 'attr' => ['class' => 'right btn-danger']], 'btn'))
			->getForm();
	}

    /**
     * @param \AIE\Bundle\StorageBundle\Entity\AbstractFile $id
     * @return bool|void
     */
    private function deleteFileData($id)
    {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:Files')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Files entity.');
        }

        $this->deleteFile($entity);

        $connection = $em->getConnection();
        $statement = $connection->prepare("
                delete FROM anomaly.RequestFiles where file_id = :id");
        $statement->bindValue('id', $id);
        $statement->execute();

        $em->remove($entity);
        $em->flush();
    }

}
