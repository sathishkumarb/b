<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\ActionMonitor;
use AIE\Bundle\AnomalyBundle\Form\ActionMonitorType;

/**
 * ActionMonitor controller.
 *
 * @Route("anomaly/{anomalyId}/action/{actionId}/monitors")
 */
class ActionMonitorController extends AnomalyBaseController {

	/**
	 * Lists all ActionMonitor entities.
	 *
	 * @Route("/", name="action_monitor")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction($anomalyId, $actionId) {
		$em = $this->getManager();

		$entities = $em->getRepository('AIEAnomalyBundle:ActionMonitor')->findBy(['action' => $actionId]);
		$action = $em->getRepository('AIEAnomalyBundle:TemporaryRepairOrderRegistrar')->find($actionId);

		return [
			'action' => $action,
			'entities'  => $entities,
			'anomalyId' => $anomalyId,
			'actionId' => $actionId
		];
	}

	/**
	 * Displays a form to edit an existing ActionMonitor entity.
	 *
	 * @Route("/{id}/edit", name="action_monitor_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($anomalyId, $actionId, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:ActionMonitor')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find ActionMonitor entity.');
		}

		$editForm = $this->createEditForm($entity, $anomalyId, $actionId);

		return [
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
			'anomalyId' => $anomalyId,
			'actionId' => $actionId
		];
	}

	/**
	 * Creates a form to edit a ActionMonitor entity.
	 *
	 * @param ActionMonitor $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(ActionMonitor $entity, $anomalyId, $actionId) {
		$form = $this->createForm(new ActionMonitorType(), $entity, [
			'action' => $this->generateUrl('action_monitor_update', ['anomalyId' => $anomalyId, 'actionId' => $actionId, 'id' => $entity->getId()]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Edits an existing ActionMonitor entity.
	 *
	 * @Route("/{id}", name="action_monitor_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:ActionMonitor:edit.html.twig")
	 */
	public function updateAction(Request $request, $anomalyId, $actionId, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:ActionMonitor')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find ActionMonitor entity.');
		}

		$editForm = $this->createEditForm($entity, $anomalyId, $actionId);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em->flush();

			return $this->redirect($this->generateUrl('action_monitor', ['anomalyId' => $anomalyId, 'actionId' => $actionId]));
		}

		return [
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
			'anomalyId' => $anomalyId,
			'actionId' => $actionId
		];
	}
}
