<?php
/**
 * Created by AIE
 * Date: 29/12/15
 * Time: 14:46
 */

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\ReportSequence;
use AIE\Bundle\AnomalyBundle\Form\ManagementFilterType;
use AIE\Bundle\AnomalyBundle\Helper\PDFSummaryReportHelper;
use AIE\Bundle\AnomalyBundle\Helper\WorkpackHelper;
use AIE\Bundle\AnomalyBundle\Helper\PDFReportHelper;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * PipelineAnomaly controller.
 *
 * @Route("projects/{projectId}/management")
 */
class ManagementController extends AnomalyBaseController {

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    function url($url,$option = null) {

        $cURL = curl_init();

        if ($option) {
            curl_setopt($cURL, CURLOPT_URL, $url.$option);
        } else {
            curl_setopt($cURL, CURLOPT_URL, $url);
        }

        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURL, CURLOPT_AUTOREFERER, 1);
        curl_setopt($cURL, CURLOPT_HTTPGET, 1);
        curl_setopt($cURL, CURLOPT_VERBOSE, 0);
        curl_setopt($cURL, CURLOPT_HEADER, 0);
        curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cURL, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($cURL, CURLOPT_DNS_CACHE_TIMEOUT, 2);

        $output['page'] = curl_exec($cURL);
        $output['contentType'] = curl_getinfo($cURL, CURLINFO_CONTENT_TYPE);

        curl_close($cURL);

        return $output;

    }

    public function mergeFiles(& $pdf ,$files,$reportHelper, $titlePrefix){

        $uploader = $this->get('storage.file_uploader');
        $attachmentNo = 1;
        foreach ($files AS $file) {
            if ($this->container->getParameter('kernel.environment') == 'dev'){
                $isEnvDev = true;
                $fileUrl = $file->getServerFilePath($isEnvDev);
            }else {
                $isEnvDev = false;
                $fileUrl = $uploader->getAbsoluteFilePath($file->getFilepath());
            }
            $fileExt = ($isEnvDev ? pathinfo($fileUrl,PATHINFO_EXTENSION) : pathinfo(parse_url($fileUrl, PHP_URL_PATH), PATHINFO_EXTENSION));
            if (strtolower($fileExt) == 'pdf'){
                if(($isEnvDev && file_exists ($fileUrl)) || (!$isEnvDev && $this->is_url_exist($fileUrl))){
                    if($isEnvDev) {
                        $pageCount = $pdf->setSourceFile($fileUrl);
                    }else {
                        $currentDate = new \DateTime();
                        $tempfilename = $currentDate->format('YmdHis') . str_pad(rand(0, pow(10, 4)-1), 4, '0', STR_PAD_LEFT);;
                        $fileContent = $this->url($fileUrl);
                        $file0 = DIRECTORY_SEPARATOR .
                            trim(sys_get_temp_dir(), DIRECTORY_SEPARATOR) .
                            DIRECTORY_SEPARATOR .
                            ltrim($tempfilename, DIRECTORY_SEPARATOR) . '.pdf';
                        file_put_contents($file0, $fileContent['page']);
                        $pageCount = $pdf->setSourceFile($file0);
                        unlink($file0);
                    }
                    if ($attachmentNo==1)
                        $reportHelper->createAppendixCoverPage($pdf,$titlePrefix . " Attachments", 2 , [0,0,0],true);
                    $reportHelper->createAppendixCoverPage($pdf,'Attachment ' . $attachmentNo . ': ' . $file->getCaption(),3);
                    // iterate through all pages
                    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                        // import a page
                        $templateId = $pdf->importPage($pageNo);
                        // get the size of the imported page
                        $size = $pdf->getTemplateSize($templateId);
                        // create a page (landscape or portrait depending on the imported page size)
                        $pdf->setPrintHeader(false);
                        $pdf->setPrintFooter(false);
                        $width = (isset($size['w']) ? $size['w'] : $size['width']);
                        $height = (isset($size['h']) ? $size['h'] : $size['height']);
                        if ($width > $height) {
                            $pdf->AddPage('L', array($width, $height));
                        } else {
                            $pdf->AddPage('P', array($width, $height));
                        }

                        $pdf->useTemplate($templateId);

                    }
                    $pdf->setPrintHeader(true);
                    $pdf->setPrintFooter(true);
                    $attachmentNo++;
                }
            }
        }

    }

    public function getMonitorImage($anomaly){
        if($anomaly->getClassification()->getIsMetalLoss()){
            if ($anomaly->getNominalWT() > 0 and count($anomaly->getMonitors())>1){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitor.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $anomaly->getMonitors()
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }else{
            $trackingTypes=[];
            $trackingCount = [];
            $entities=$anomaly->getMonitors();
            foreach ($entities as $entity){
                $trcks = json_decode($entity->getTrackingCriteria());
                foreach ($trcks as $trck){
                    if (!(in_array ($trck->description,$trackingTypes))){
                        array_push($trackingTypes,$trck->description);
                        $trackingCount[$trck->description] = 0;
                    }
                }
            }
            foreach ($trackingTypes as $tc){
                foreach ($entities as $entity){
                    $trcks = json_decode($entity->getTrackingCriteria());
                    foreach ($trcks as $trck){
                        if ($tc == $trck->description && is_numeric($trck->value) && is_numeric($trck->tracking_conc)){
                            $trackingCount[$trck->description] +=1;
                        }
                    }
                }
            }
            $hasimg = false;
            foreach ($trackingCount as $cc){
                if ($cc > 1){
                    $hasimg=true;
                }
            }
            if ($hasimg){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitortc.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $entities,
                    'tracking_types'=> $trackingTypes
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }
        return $img;
    }

    public function createWPPDF($project, $location, $regIds, $title, $selected, $dateFrom,$dateTo,$summaryTable,$includeAttachments, $sequenceNo){
        $uploader = $this->get('storage.file_uploader');
        if ($this->container->getParameter('kernel.environment') == 'dev'){
            $isEnvDev = true;
            $projectLogo = $project->getServerFilePath($isEnvDev);
        }else {
            $isEnvDev = false;
            $projectLogo = $uploader->getAbsoluteFilePath($project->getFilepath());
        }

        if(count($location) > 1){
            $locName = "Multiple Locations";
        }else{
            $locName = $location[0]->getName();
        }

        $defaultEm = $this->getDoctrine()->getManager();
        $em = $this->getManager();
        $regsList=[];
        foreach ($regIds as $regId){
            array_push($regsList,$em->getRepository('AIEAnomalyBundle:Registrar')->find($regId));
        }
        $wpHelper = new WorkpackHelper($em,$defaultEm);
        $pdf = $wpHelper->definePDFObject();
        $anomalyReportObj = new PDFReportHelper($em, $defaultEm, ($this->container->getParameter('kernel.environment') == 'dev'?true:false), $this->get('storage.file_uploader'));
        $reportNumber = $project->getReferenceNo() . '-IMS-' . date('y') . '-WP-' . str_replace("IN", "IR", $selected) . '-'.$sequenceNo;
        $coverArray = [
            ['size'=>'L', 'hasImage'=>false,'caption'=>'Title','value'=>$title . ' Workpack ' . '(' . $dateFrom .' to ' . $dateTo . ')' ],
            ['size'=>'L', 'hasImage'=>true,'caption'=>'Client Name','value'=>$project->getName()],
            ['size'=>'L', 'hasImage'=>false,'caption'=>'Location','value'=>$locName],
            ['size'=>'S', 'hasImage'=>false,'caption'=>'Report Ref.','value'=>$reportNumber],
            ['size'=>'S', 'hasImage'=>false,'caption'=>'Date','value'=>(date('d F Y'))]
            ];
        $anomalyReportObj->createReportCover($pdf,"", $title ." Workpack",$projectLogo,$isEnvDev, $coverArray);
        $regsList=[];
        foreach ($regIds as $regId){
            array_push($regsList,$em->getRepository('AIEAnomalyBundle:Registrar')->find($regId));
        }
        $pdf->setHeaderInfo($projectLogo,$isEnvDev,$project->getName() . ' IMS', $title . ' Workpack',$reportNumber);
        $anomalyReportObj->createExecutiveSummary($pdf,$regsList, $title,$dateFrom . ' and ' . $dateTo,$locName,$summaryTable);
        $counter= 0;
        $pdf->setHeaderInfo($projectLogo,$isEnvDev,$project->getName() . ' IMS', $title . ' Workpack',$reportNumber);
        $anomalyReportObj->createReportMainContent($pdf,
            $title . ' Workpack',
            $dateFrom . ' to '. $dateTo,
            count($regIds),
            $summaryTable, $project);

        $pdf->setHeaderInfo($projectLogo,$isEnvDev,$project->getName() . ' IMS', $title . ' Workpack',$reportNumber, 'L');
        $anomalyReportObj->createAnomalyRegister($pdf,$selected,$regsList,$project);

        foreach ($regsList as $register){
            $counter++;
            $anomaly=($register instanceof AnomalyRegistrar ? $register : $register->getAnomaly());
            if($register instanceof AnomalyRegistrar){
                $reportName = 'Anomaly Inspection Report - ' .($anomaly->getAnomalyCustomId());
                $img = $this->getMonitorImage($anomaly);
            }else{
                if($register instanceof RepairOrderRegistrar){
                    $reportName = 'Repair Order Report - ' .($anomaly->getAnomalyCustomId());
                }elseif($register instanceof TemporaryRepairOrderRegistrar){
                    $reportName = 'Temporary Repair Order Report - ' .($anomaly->getAnomalyCustomId());
                }elseif($register instanceof FabricMaintenanceRegistrar){
                    $reportName = 'Fabric Maintenance Report - ' .($anomaly->getAnomalyCustomId());
                }elseif($register instanceof AssessmentRegistrar){
                    $reportName = 'Assessment Report - ' .($anomaly->getAnomalyCustomId());
                }else{
                    $reportName = 'Anomaly Inspection Report - ' .($anomaly->getAnomalyCustomId());
                }

                $img=null;
            }
            $pdf->setHeaderInfo($projectLogo, $isEnvDev,$project->getName() . ' IMS', $reportName,$reportNumber);
            $pdf->AddPage('P', 'A4');
            $anomalyReportObj->createRegisterReport($pdf, $register, $img,$counter);
            if ($includeAttachments == 1){
                $this->mergeFiles($pdf,$anomaly->getFiles(),$anomalyReportObj, '2.' . $counter . '.' . ($anomalyReportObj->getLatestListNumber()+1));
            }
        }
        $pdf->setHeaderInfo($projectLogo,$isEnvDev,$project->getName() . ' IMS', $title . ' Workpack',$reportNumber);
        $anomalyReportObj->createTableOfContent($pdf,2);
        $pdf->Output('workpack_' . $project->getId() . '.pdf', 'D');
        exit;
    }

    /**
     * @param $projectId
     * @param $submitForm
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createDownloadForm($projectId,$selected,$title,$searched,$summaryTable,$regsObject,$datefrom, $dateto, $includeAttachments,$locationId){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_planning_master', array('projectId' => $projectId)))
            ->setMethod('GET')
            ->add('download', 'submit', $this->options(['label' => 'Download Workpack', 'attr' => ['class' => 'btn btn-success btn-block call-loader']], 'btn'))
            ->add('selectedRegsObj', 'hidden', array('data' => json_encode($regsObject)))
            ->add('selectedObj', 'hidden', array('data' => $selected))
            ->add('titleObj', 'hidden', array('data' => $title))
            ->add('searchedObj', 'hidden', array('data' => $searched))
            ->add('includeAttachments', 'hidden', array('data' => $includeAttachments))
            ->add('locationId', 'hidden', array('data' => $locationId))
            ->add('summaryTableObj', 'hidden', array('data' => json_encode($summaryTable)))
            ->add('registersObj', 'hidden', array('data' => json_encode($regsObject)))
            ->add('dateFromObj', 'hidden', array('data' => $datefrom))
            ->add('dateToObj', 'hidden', array('data' => $dateto))
            ->getForm();
        return $downloadform;

    }

	public function planningHandler(Request $request, $projectId, $anomalyType, $submitForm) {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (! $project) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }
		$fromDate = new \DateTime('now');
		$toDate = clone $fromDate;
		$toDate->modify('+1 month');
		$toDate->modify('+1 day');

        $results = [];
        $selected=null;
        $title='';
        $searched=false;
        $regsObject = [];
        $summaryTable=['closed'=>0,'planned'=>0,'overdue'=>0];
        $filter_form = $this->createFilterForm($projectId, $anomalyType, $fromDate, $toDate, $submitForm);
        $filter_form->handleRequest($request);
        $downloadform = $this->createDownloadForm($projectId,null,null,null,null,null,null,null,null,null);
		if ($filter_form->isValid() && $filter_form->isSubmitted()) {
		    $searched=true;
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];

            $fromDate = new \DateTime($filter_data['from_date']);
            $toDate = new \DateTime($filter_data['to_date']);

            /** check dates and repeated events */
            $toDateMax = $toDate->modify('+1 day');
            $toDateMaxU = (int)$toDateMax->format('U');
            $fromDateU = (int)$fromDate->format('U');
            $selected = $request->request->get('form')['filter_form']['type'];
            $locationId = $request->request->get('form')['filter_form']['locationSelected'];
            if ($selected == 'IN'){
                $regname = 'AnomalyRegistrar';
                $title="Anomaly Inspection";
            }
            if ($selected == 'RO'){
                $regname = 'RepairOrderRegistrar';
                $title="Repair Order";
            }
            if ($selected == 'TRO'){
                $regname = 'TemporaryRepairOrderRegistrar';
                $title="Temporary Repair Order";
            }
            if ($selected == 'FM'){
                $regname = 'FabricMaintenanceRegistrar';
                $title="Fabric Maintenance";
            }
            if ($selected == 'AS'){
                $regname = 'AssessmentRegistrar';
                $title="Assessment";
            }

            $setOfLocations = explode(",", $locationId);

            $registers = "";

            if ($selected == 'IN')
            {
                $qb = $em->createQueryBuilder();
                $qb
                    ->select('r')
                    ->from('AIE\Bundle\AnomalyBundle\Entity\\' . $regname, 'r')
                    ->where('r.statusTag IN (:statusTag)')
                    ->andWhere('r.project =:projectid')
                    ->andWhere('r.location IN (:locationid)')
                    ->setParameter('statusTag', ['final','closed','new','request-rejected'])
                    ->setParameter('locationid', $setOfLocations)
                    ->setParameter('projectid', $projectId);

                $registers = $qb->getQuery()->getResult();
            }
            else {

                $actionregisters = array();
                $sql = "SELECT r0_.id FROM 
                            Registrar r0_
                            WHERE r0_.statusTag IN ('final','closed','new','request-rejected') 
                            AND r0_.project_id = $projectId and r0_.registrar_type = '$selected'";

                //echo $sql;

                $stmt = $em->getConnection()->prepare($sql);

                $stmt->execute();
                $resultsPass = $stmt->fetchAll();

                if ($resultsPass) {
                    foreach ($resultsPass as $res) {
                        $idsArray[] = $res['id'];
                    }

                    $sql = "SELECT 
                        repairorder.*,actionregister.*, register.*, anomalyregister.*, actionregister.id as actionId
                        FROM ".$regname." repairorder
                        INNER JOIN ActionRegistrar actionregister ON repairorder.id = actionregister.id
                        INNER JOIN Registrar register ON actionregister.anomaly_id = register.id
                        INNER JOIN AnomalyRegistrar anomalyregister ON anomalyregister.id = register.id
                        AND register.statusTag IN ('final','closed','new','request-rejected')
                        AND register.project_id = $projectId
                        AND anomalyregister.location_id IN ($locationId)";

                    $stmt = $em->getConnection()->prepare($sql);

                    $stmt->execute();
                    $actionregisters = $stmt->fetchAll();

                }
            }

            if ($registers) {
                foreach ($registers as $register) {
                    $anomaly = ($register instanceof AnomalyRegistrar ? $register : $register->getAnomaly());
                    if ($regname == 'AnomalyRegistrar') {
                        if ($anomaly->getInspectionDate() == null || empty($anomaly->getInspectionDate())) {
                            continue;
                        }
                        $anomlayDateU = $anomaly->getNextInspectionDate()->format('U');
                        if ($anomlayDateU < $fromDateU || $anomlayDateU > $toDateMaxU) {
                            continue;
                        }
                    }

                    $item = ['assetTagNumber' => $anomaly->getAssetTagNumber(),
                        'anomalyId' => $anomaly->getAnomalyCustomId(),
                        'component' => $anomaly->getComponent(),
                        'location' => $anomaly->getLocation()->getName(),
                        'latesInspectionDate' => null,
                        'nextInspectionDate' => null,
                        'technicalExpiryDate' => null,
                        'completionDate' => null,
                        'status' => null,
                        'comment' => null
                    ];


                    if ($register instanceof AnomalyRegistrar) {
                        $statusTag = $register->getStatusTag();
                        if ($statusTag == 'closed') {
                            $item['status'] = 'Closed';
                        }
                        $item['latesInspectionDate'] = $anomaly->getInspectionDate()->format('j M Y');
                        $item['nextInspectionDate'] = $anomaly->getNextInspectionDate()->format('j M Y');
                        if ($statusTag != 'closed') {
                            if ($this->getIsDue($anomaly->getNextInspectionDate())) {
                                $item['status'] = 'Overdue';
                            } else {
                                $item['status'] = 'Planned';
                            }
                        }

                        if ($item['status'] == 'Planned')
                            $summaryTable['planned'] += 1;
                        if ($item['status'] == 'Closed')
                            $summaryTable['closed'] += 1;
                        if ($item['status'] == 'Overdue')
                            $summaryTable['overdue'] += 1;

                        array_push($regsObject, $register->getId());
                        array_push($results, $item);
                    }

                }
            }
            else
            {
                $results = array();
                $regTechDate = "";

                if ($actionregisters)
                {
                    foreach ($actionregisters as $register)
                    {
                        if ($regname == 'TemporaryRepairOrderRegistrar')
                        {
                            if (!empty($register['installation_date']) && !empty($register['next_inspection_date']))
                            {

                                $register['next_inspection_date'] = new \DateTime($register['next_inspection_date']);
                                $register['installation_date'] = new \DateTime($register['installation_date']);
                                $RegDateU = $register['next_inspection_date']->format('U');

                                if ($RegDateU < $fromDateU || $RegDateU > $toDateMaxU) {
                                    continue;
                                }
                            } else {
                                $register['expiryDate'] = new \DateTime($register['expiryDate']);
                                if ($register['expiryDate'] == null || empty($register['expiryDate'])) {
                                    continue;
                                }
                                $RegDateU = $register['expiryDate']->format('U');
                                $regTechDate = $register['expiryDate']->format('j M Y');
                                if ($RegDateU < $fromDateU || $RegDateU > $toDateMaxU) {
                                    continue;
                                }
                            }

                        } else {
                            $register['expiryDate'] = new \DateTime($register['expiryDate']);
                            if ($register['expiryDate'] == null || empty($register['expiryDate'])) {
                                continue;
                            }

                            $RegDateU = $register['expiryDate']->format('U');
                            $regTechDate = $register['expiryDate']->format('j M Y');
                            if ($RegDateU < $fromDateU || $RegDateU > $toDateMaxU) {
                                continue;
                            }
                        }

                        $location = $em->getRepository('AIEAnomalyBundle:Locations')->find($register['location_id']);

                        $item = ['assetTagNumber' => $register['asset_tag_number'],
                            'anomalyId' => $register['anomaly_custom_id'],
                            'component' => $register['component'],
                            'location' => $location->getName(),
                            'latesInspectionDate' => null,
                            'nextInspectionDate' => null,
                            'technicalExpiryDate' => null,
                            'completionDate' => null,
                            'status' => null,
                            'comment' => null
                        ];

                        $statusTag = $register['statusTag'];

                        if ($statusTag == 'closed') {
                            $item['status'] = 'Closed';
                        } else {
                            $item['status'] = '';
                        }

                        $item['technicalExpiryDate'] = $regTechDate;
                        if ($regname == 'TemporaryRepairOrderRegistrar' && !empty($register['next_inspection_date'])) {

                            $register['inspection_date'] = new \DateTime($register['inspection_date']);
                            $register['next_inspection_date'] = $register['next_inspection_date'];
                            $register['installation_date'] = $register['installation_date'];

                            $item['latesInspectionDate'] = $register['inspection_date']->format('j M Y');
                            $item['nextInspectionDate'] = $register['next_inspection_date']->format('j M Y');
                            $item['completionDate'] = $register['installation_date']->format('j M Y');
                            if ($statusTag != 'closed') {
                                if ($this->getIsDue($register['next_inspection_date'])) {
                                    $item['status'] = 'Overdue';
                                } else {
                                    $item['status'] = 'Planned';
                                }
                            }
                        } elseif ($regname == 'AssessmentRegistrar' && !empty($register['assessment_due_date'])) {
                            $register['assessment_due_date'] = new \DateTime($register['assessment_due_date']);
                            $item['completionDate'] = $register['assessment_due_date']->format('j M Y');
                        } elseif ($regname == 'RepairOrderRegistrar' && !empty($register['close_out_date'])) {
                            $register['close_out_date'] = new \DateTime($register['close_out_date']);
                            $item['completionDate'] = $register['close_out_date']->format('j M Y');
                        } elseif ($regname == 'FabricMaintenanceRegistrar' && !empty($register['completion_date'])) {
                            $register['completion_date'] = new \DateTime($register['completion_date']);
                            $item['completionDate'] = $register['completion_date']->format('j M Y');
                        } else {
                            if ($statusTag != 'closed') {
                                if ($statusTag != 'closed') {
                                    if ($this->getIsDue($register['expiryDate'])) {
                                        $item['status'] = 'Overdue';
                                    } else {
                                        $item['status'] = 'Planned';
                                    }
                                }
                            }
                            $item['completionDate'] = null;
                        }

                        if ($item['status'] == 'Planned')
                            $summaryTable['planned'] += 1;
                        if ($item['status'] == 'Closed')
                            $summaryTable['closed'] += 1;
                        if ($item['status'] == 'Overdue')
                            $summaryTable['overdue'] += 1;

                        array_push($regsObject, $register['actionId']);
                        array_push($results, $item);
                    }
                }
            }

            $includeAttachments = 1;
            $downloadform = $this->createDownloadForm($projectId,$selected,$title,$searched,$summaryTable,$regsObject,date_format($fromDate,'d F Y'),date_format($toDate,'d F Y'), $includeAttachments,$locationId);
        }

        $downloadform->handleRequest($request);
        if ($downloadform->isValid() && $downloadform->isSubmitted()) {
            $data = $downloadform->getData();
            $summaryTable = json_decode($data['summaryTableObj'],true);
            $includeAttachments = $data['includeAttachments'];
            $regsObject = json_decode($data['registersObj'],true);
            $selected =$data['selectedObj'];
            $title =$data['titleObj'];
            $searched =$data['searchedObj'];
            $locationId =$data['locationId'];
            //echo $locationId;die();
            $dateFrom =$data['dateFromObj'];
            $dateTo =$data['dateToObj'];
            $setOfLocations = explode(",", $locationId);
            $location = $em->getRepository('AIEAnomalyBundle:Locations')->findById($setOfLocations);
            
            $sequence = $em->getRepository('AIEAnomalyBundle:ReportSequence')->findOneBy(array('project'=>$projectId,'reportCategory'=>$selected,'reportType'=>2));
            if($sequence){
                $seq=$sequence->getCurrentSequence()+1;
                $resultArray['message']= $this->formatSequence($seq);
                $sequence->setCurrentSequence($seq);

            }else{
                $seq=1;
                $sequence = new ReportSequence();
                $resultArray['message']= $this->formatSequence($seq);
                $sequence->setCurrentSequence($seq);
                $sequence->setProject($em->getRepository('AIEAnomalyBundle:Projects')->find($projectId));
                $sequence->setReportType(2);
                $sequence->setReportName('Workpack');
                $sequence->setReportCategory($selected);
            }
            $em->persist($sequence);
            $em->flush();

            $this->createWPPDF($project,$location, $regsObject, $title,$selected, $dateFrom,$dateTo, $summaryTable,$includeAttachments, $sequence->getCurrentSequence());
        }
		return [
			'project'          => $project,
            'download_form'=> $downloadform->createView(),
			'filter_form'      => $filter_form->createView(),
            'results'=>$results,
            'registerIds'=>$regsObject,
            'selected'=>$selected,
            'title'=>$title,
            'searched'=>$searched,
            'summaryTable'=>$summaryTable
		];
	}

	private function createFilterForm($projectId, $anomalyType, \DateTime $fromDate, \DateTime $toDate, $submitForm) {
		$requestParameters = ['projectId' => $projectId,];
		if ($anomalyType !== null) {
			$requestParameters[ 'anomalyType' ] = $anomalyType;
		}
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
		$form = $this->createFormBuilder()
			->setAction($this->generateUrl($submitForm, $requestParameters))
			->setMethod('POST');

		$form->add('filter_form', new ManagementFilterType($fromDate, $toDate,$project), ['label' => ' ']);
		$form = $form
			->add('submit', 'submit', $this->options(['label' => 'Generate', 'attr' => ['class' => 'btn btn-primary btn-block']], 'btn'))
			->getForm();

		return $form;
	}

	/**
	 *
	 * @Route("/planning/master", name="anomaly_planning_master")
	 * @Method({"GET", "POST"})
	 * @Template("AIEAnomalyBundle:Management:planning.html.twig")
	 */
	public function masterPlanningAction(Request $request, $projectId, $anomalyType = null) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Planning'];
	}

	/**
	 *
	 * @Route("/planning/{anomalyType}", name="anomaly_planning", requirements={"anomalyType"="(pl|pi|se|st|mr)?"})
	 * @Method({"GET", "POST"})
	 * @Template("AIEAnomalyBundle:Management:planning.html.twig")
	 */
	public function planningAction(Request $request, $projectId, $anomalyType = null) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Planning'];
	}

	/**
	 *
	 * @Route("/reminders/master", name="anomaly_reminders_master")
	 * @Method({"GET", "POST"})
	 * @Template("AIEAnomalyBundle:Management:reminders.html.twig")
	 */
	public function masterRemindersAction(Request $request, $projectId, $anomalyType = null) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Reminders'];
	}

	/**
	 *
	 * @Route("/reminders/{anomalyType}", name="anomaly_reminders", requirements={"anomalyType"="(pl|pi|se|st|mr)?"})
	 * @Method({"GET", "POST"})
	 * @Template("AIEAnomalyBundle:Management:reminders.html.twig")
	 */
	public function remindersAction(Request $request, $projectId, $anomalyType = null) {
		return $this->planningHandler($request, $projectId, $anomalyType, $request->get('_route')) + ['wizard_title' => 'Reminders'];
	}

	public function getIsDue($nextinspectionDate)
    {
        //if today is the next inspection or after that
        $now = new \DateTime('now');
        return ($nextinspectionDate !== null && (int)$nextinspectionDate->format('U') <= (int)$now->format('U'));
    }

    public function formatSequence($seq){
        if(strlen($seq) > 4 || $seq > 9999){
            $seq = 1;
        }
        $sttVal = "";
        for($i = strlen($seq)-1; $i<3;$i++){
            $sttVal = $sttVal . "0";
        }
        $sttVal = $sttVal . $seq;
        return $sttVal;
    }

}