<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Helper\AnomalyHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat;
use AIE\Bundle\AnomalyBundle\Form\AnomalyThreatType;

/**
 * AnomalyThreat controller.
 *
 * @Route("projects/{projectId}/threats")
 */
class AnomalyThreatController extends AnomalyBaseController {

	/**
	 * Lists all AnomalyThreat entities.
	 *
	 * @Route("/", name="anomaly_a_threats")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction($projectId) {

        $editVisible = 0;
        $createVisible = 0;


        if (!$this->securityHelper->isRoleGranted('ROLE_THREATS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));


        $em = $this->getManager();

		$entities = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->findByProject($projectId);




        //Checking if user has permission to edit, add, delete
//        $this->rolesMergeToCheckByProject = $this->getRolesArrayForCurrentProject($projectId);
//        if (!$this->rolesMergeToCheckByProject){
//            return $this->redirectToRoute('anomalyAccessDeniedException');
//        }

        if ($this->securityHelper->isRoleGranted('ROLE_THREATS_EDIT',$this->userRolesMergeToCheck )  || $this->assignedAsAdmin) {
            $editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_THREATS_ADD',  $this->userRolesMergeToCheck)  || $this->assignedAsAdmin) {
            $createVisible = 1;
        }
//        print_r($this->userRolesMergeToCheck);
//        echo("edit visible " . $editVisible . $createVisible);
        // end of check

        return [
			'entities'  => $entities,
			'projectId' => $projectId,
            'editGranted' => $editVisible,
            'createGranted' => $createVisible,

		];
	}

	/**
	 * Creates a new AnomalyThreat entity.
	 *
	 * @Route("/", name="anomaly_a_threats_create")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:AnomalyThreat:new.html.twig")
	 */
	public function createAction($projectId, Request $request) {

		$entity = new AnomalyThreat();
		$form = $this->createCreateForm($projectId, $entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getManager();

			$code = $form->getData()->getCode();
			$anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
			$isUsed = $anomalyHelper->isAnomalyCodeUsed($projectId, $code);
			if ($isUsed) {
				$this->get('session')->getFlashBag()->add('error', "Oops! This Anomaly Code ({$code}) is already in use.");
			} else {
				$project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
				$entity->setProject($project);
				$em->persist($entity);
				$em->flush();
			}

			return $this->redirect($this->generateURL('anomaly_a_threats', ['projectId' => $projectId,]));
		}

		return [
			'entity'    => $entity,
			'projectId' => $projectId,
			'form'      => $form->createView(),
		];
	}

	/**
	 * Creates a form to create a AnomalyThreat entity.
	 *
	 * @param AnomalyThreat $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm($projectId, AnomalyThreat $entity) {
		$form = $this->createForm(new AnomalyThreatType(), $entity, [
			'action' => $this->generateURL('anomaly_a_threats_create', ['projectId' => $projectId,]),
			'method' => 'POST',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Displays a form to create a new AnomalyThreat entity.
	 *
	 * @Route("/new", name="anomaly_a_threats_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($projectId) {
		$entity = new AnomalyThreat();
		$form = $this->createCreateForm($projectId, $entity);

        //Checking if user has permission to create
        if (!$this->securityHelper->isRoleGranted('ROLE_THREATS_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        // end of check

		return [
			'entity'    => $entity,
			'projectId' => $projectId,
			'form'      => $form->createView(),
		];
	}

	/**
	 * Finds and displays a AnomalyThreat entity.
	 *
	 * @Route("/{id}", name="anomaly_a_threats_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($projectId, $id) {


        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($id);

        if (! $entity) {
            throw $this->createNotFoundException('Unable to find AnomalyThreat entity.');
        }

        //      Checking permission to show
        if (!($this->securityHelper->isRoleGranted('ROLE_THREATS_SHOW', $this->userRolesMergeToCheck))  && !$this->assignedAsAdmin) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }



        $deleteForm = $this->createDeleteForm($projectId, $id);

		return [
			'entity'      => $entity,
			'projectId'   => $projectId,
			'delete_form' => $deleteForm->createView(),

		];
	}

	/**
	 * Displays a form to edit an existing AnomalyThreat entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_a_threats_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($projectId, $id) {


		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find AnomalyThreat entity.');
		}

        //Checking if user has permission to edit
     if (!$this->securityHelper->isRoleGranted('ROLE_THREATS_EDIT',$this->userRolesMergeToCheck )   && !$this->assignedAsAdmin ) {
           return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        // end of check


		$editForm = $this->createEditForm($projectId, $entity);
		$deleteForm = $this->createDeleteForm($projectId, $id);

		return [
			'entity'      => $entity,
			'projectId'   => $projectId,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),

		];
	}

	/**
	 * Creates a form to edit a AnomalyThreat entity.
	 *
	 * @param AnomalyThreat $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm($projectId, AnomalyThreat $entity) {
		$form = $this->createForm(new AnomalyThreatType(), $entity, [
			'action' => $this->generateURL('anomaly_a_threats_update', ['projectId' => $projectId, 'id' => $entity->getId()]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Edits an existing AnomalyThreat entity.
	 *
	 * @Route("/{id}", name="anomaly_a_threats_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:AnomalyThreat:edit.html.twig")
	 */
	public function updateAction($projectId, Request $request, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find AnomalyThreat entity.');
		}

		$deleteForm = $this->createDeleteForm($projectId, $id);
		$editForm = $this->createEditForm($projectId, $entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$code = $editForm->getData()->getCode();
			$anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
			$isUsed = $anomalyHelper->isAnomalyCodeUsed($projectId, $code);
			if ($code != $entity->getCode() && $isUsed) {
				$this->get('session')->getFlashBag()->add('error', "Oops! This Anomaly Code ({$code}) is already in use.");
			} else {
				$project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
				$entity->setProject($project);
				$em->flush();
			}

			return $this->redirect($this->generateURL('anomaly_a_threats_edit', ['projectId' => $projectId, 'id' => $id]));
		}

		return [
			'entity'      => $entity,
			'projectId'   => $projectId,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		];
	}

	/**
	 * Deletes a AnomalyThreat entity.
	 *
	 * @Route("/{id}", name="anomaly_a_threats_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction($projectId, Request $request, $id) {
		$form = $this->createDeleteForm($projectId, $id);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getManager();
			$entity = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($id);

			if (! $entity) {
				throw $this->createNotFoundException('Unable to find AnomalyThreat entity.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateURL('anomaly_a_threats', ['projectId' => $projectId,]));
	}

	/**
	 * Creates a form to delete a AnomalyThreat entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($projectId, $id) {
		return $this->createFormBuilder()
			->setAction($this->generateURL('anomaly_a_threats_delete', ['projectId' => $projectId, 'id' => $id]))
			->setMethod('DELETE')
			->add('submit', 'submit', $this->options(['label' => 'Delete', 'attr' => ['class' => 'btn-danger right hide']], 'btn'))
			->getForm();
	}


}
