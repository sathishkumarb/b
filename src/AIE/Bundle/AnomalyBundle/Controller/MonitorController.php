<?php


namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Monitor;
use AIE\Bundle\AnomalyBundle\Form\MonitorType;

/**
 * Monitor controller.
 *
 * @Route("projects/{projectId}/anomaly/{anomalyId}/monitors")
 */
class MonitorController extends AnomalyBaseController {

	/**
	 * Lists all Monitor entities.
	 *
	 * @Route("/", name="anomaly_monitor")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction($projectId, $anomalyId) {
		$em = $this->getManager();

		$entities = $em->getRepository('AIEAnomalyBundle:Monitor')->findBy(['registrar' => $anomalyId],  array('inspectionDate' => 'DESC'));
		$anomaly = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($anomalyId);
        $trackingTypes=[];
		foreach ($entities as $entity){
            $trcks = json_decode($entity->getTrackingCriteria());
            foreach ($trcks as $trck){
                if (!(in_array ($trck->description,$trackingTypes))){
                    array_push($trackingTypes,$trck->description);
                }
            }
        }
        $param = [
            'anomaly' => $anomaly,
            'entities'  => $entities,
            'projectId' => $projectId,
            'tracking_types'=> $trackingTypes,
            'anomalyId' => $anomalyId
        ];


        if($anomaly->getClassification()->getIsMetalLoss()){
            return $this->render('AIEAnomalyBundle:Monitor:index_cr.html.twig', $param);
        }

		return $param;
	}

	/**
	 * Displays a form to edit an existing Monitor entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_monitor_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($projectId, $anomalyId, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Monitor')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Monitor entity.');
		}

		$editForm = $this->createEditForm($entity, $projectId, $anomalyId);

		return [
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
			'projectId' => $projectId,
			'anomalyId' => $anomalyId
		];
	}

	/**
	 * Creates a form to edit a Monitor entity.
	 *
	 * @param Monitor $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Monitor $entity, $projectId, $anomalyId) {
		$form = $this->createForm(new MonitorType(), $entity, [
			'action' => $this->generateUrl('anomaly_monitor_update', ['projectId' => $projectId, 'anomalyId' => $anomalyId, 'id' => $entity->getId()]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Edits an existing Monitor entity.
	 *
	 * @Route("/{id}", name="anomaly_monitor_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:Monitor:edit.html.twig")
	 */
	public function updateAction(Request $request, $projectId, $anomalyId, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Monitor')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Monitor entity.');
		}

		$editForm = $this->createEditForm($entity, $projectId, $anomalyId);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em->flush();

			return $this->redirect($this->generateUrl('anomaly_monitor', ['projectId' => $projectId, 'anomalyId' => $anomalyId]));
		}

		return [
			'entity'    => $entity,
			'edit_form' => $editForm->createView(),
			'projectId' => $projectId,
			'anomalyId' => $anomalyId
		];
	}
}
