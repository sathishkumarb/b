<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * RepairOrderRegistrar controller.
 *
 * @Route("projects/{projectId}/ro/{type}/", defaults={"type"=null}, requirements={"type"="(pl|pi|se|st|mr)?"})
 */
class RepairOrderRegistrarController extends ActionRegistrarController {


	protected $registrarClassName = 'RepairOrderRegistrar';
	protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar';
	protected $registrarRouteBase = 'anomaly_ro';
	protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\RepairOrderRegistrarType';

    /**
     * Show the request history of an anomaly
     *
     * @Route("{id}/history", name="ro_registrar_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Registrar:history.html.twig")
     */
    public function historyAction($id)
    {
        return parent::historyAction($id);
    }

	/**
	 * Lists all entities.
	 *
	 * @Route("", name="anomaly_ro")
	 * @Method({"GET", "POST"})
	 * @Template()
	 */
	public function indexAction(Request $request, $projectId, $type = null) {
		return parent::indexAction($request, $projectId, $type);
	}

	/**
	 * Lists all entities.
	 *
	 * @Route("master", name="anomaly_ro_master")
	 * @Method({"GET", "POST"})
	 * @Template()
	 */
	public function masterAction(Request $request, $projectId, $type = null) {
		$this->registrarRouteBase = 'anomaly_ro_master';
		return parent::masterAction($request, $projectId, $type);
	}

	/**
	 * Finds and displays a entity.
	 *
	 * @Route("show/{id}", name="anomaly_ro_show", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($id, $projectId) {
		return parent::showAction($id, $projectId);
	}

	/**
	 * Finds and displays a entity.
	 *
	 * @Route("showdeferral/{id}", name="anomaly_ro_show_deferral", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function showdeferralAction($id, $projectId) {
		return parent::showDeferralAction($id, $projectId);
	}

	/**
	 * Displays a form to edit an existing entity.
	 *
	 * @Route("edit/{id}", name="anomaly_ro_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id, $projectId, $type=null) {
		return parent::editAction($id, $projectId, $type);
	}


	/**
	 * Edits an existing entity.
	 *
	 * @Route("update/{id}", name="anomaly_ro_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:RepairOrderRegistrar:edit.html.twig")
	 */
	public function updateAction(Request $request, $projectId, $id, $type) {
		return parent::updateAction($request, $projectId, $id, $type);
	}

	protected function setEntityFields($entity, $data) {
		parent::setEntityFields($entity, $data);
	}
}
