<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences;
use AIE\Bundle\AnomalyBundle\Form\ProjectConsequencesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\FormError;

/**
 * ProjectConsequences controller.
 *
 * @Route("/projects/{projectId}/consequences")
 */
class ProjectConsequencesController extends AnomalyBaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all ProjectConsequences entities.
     *
     * @Route("/", name="anomaly_projectsconsequences")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:ProjectConsequences:new.html.twig")
     */
    public function indexAction($projectId) {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findBy(array('project' => $projectId));
        $consequences = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->findAll();


        $form = $this->createCreateForm($projectId, $consequences);
        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'form' => $form->createView(),
            'consequences' => $consequences,
        );
    }

    /**
     * Creates a new ProjectConsequences entity.
     *
     * @Route("/", name="anomaly_projectsconsequences_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:ProjectConsequences:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {
        $em = $this->getManager();
        $entities = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findBy(array('project' => $projectId));
        $consequences = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->findAll();

        $form = $this->createCreateForm($projectId, $consequences);
        $form->handleRequest($request);

        if ($form->isValid()) {


            $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
            $data = $form->getData();


            $count = count(array_filter($data));

            if ($count < 1) {
                $this->get('session')->getFlashBag()->add('error', 'A minimum of 1 consequences should be selected!');
            } else if ($count > 6) {
                $this->get('session')->getFlashBag()->add('error', 'A maximum of 6 consequences can be selected!');
            } else {
                foreach ($data as $k => $d) {

                    $projectConsequence = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findOneBy(array('project' => $projectId, 'consequence' => $k));

                    if (!$projectConsequence && $d === true) {
                        $projectConsequence = new ProjectConsequences();
                        $projectConsequence->setProject($project);
                        $consequence = $em->getRepository('AIEAnomalyBundle:ConsequencesCategories')->find($k);
                        $projectConsequence->setConsequence($consequence);
                        $projectConsequence->setDesc1($consequence->getDesc1());
                        $projectConsequence->setDesc2($consequence->getDesc2());
                        $projectConsequence->setDesc3($consequence->getDesc3());
                        $projectConsequence->setDesc4($consequence->getDesc4());
                        $projectConsequence->setDesc5($consequence->getDesc5());
                        $em->persist($projectConsequence);
                    } else if ($projectConsequence && $d === false) {
                        $em->persist($projectConsequence);
                        $em->remove($projectConsequence);
                    }
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->add('success', 'Consequences successfully updated!');
            }
        }
        return $this->redirect($this->generateURL('anomaly_projectsconsequences', array('projectId' => $projectId)));
    }

    /**
     * Creates a form to create a ProjectConsequences entity.
     *
     * @param ProjectConsequences $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId, $consequences) {

        if ($this->securityHelper->isRoleGranted('ROLE_CONSEQUENCES_EDIT',$this->userRolesMergeToCheck))
            $editable = true;
        else
            $editable = false;



        $form = $this->createFormBuilder()
                ->setAction($this->generateURL('anomaly_projectsconsequences_create', array('projectId' => $projectId)))
                ->setMethod('POST')
                ->setDisabled(!$editable);

        $em = $this->getManager();

        foreach ($consequences as $consequence) {
            $projectConsequence = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findOneBy(array('project' => $projectId, 'consequence' => $consequence->getId()));
            $attr = array();
            if ($projectConsequence) {
                $attr = array('checked' => 'checked');
            }
            $form->add($consequence->getId(), 'checkbox', array('label' => '', 'required' => false, 'attr' => $attr));
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
                ->getForm();


        return $form;
    }

}
