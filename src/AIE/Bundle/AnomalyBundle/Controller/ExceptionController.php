<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 07/10/2018
 * Time: 11:29 AM
 */

namespace AIE\Bundle\AnomalyBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Exception controller.
 *
 * @Route("/anomalyException")
 */
class ExceptionController extends AnomalyBaseController
{
    /**
     * Lists all Request entities.
     *
     * @Route("/", name="anomalyAccessDeniedException")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
//        echo("hola");
        return $this->render(
            'AIEVeracityBundle:Default:accessdenied.html.twig'
        );

    }
}