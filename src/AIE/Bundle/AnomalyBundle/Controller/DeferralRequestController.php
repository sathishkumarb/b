<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Monitor;
use AIE\Bundle\AnomalyBundle\Form\DeferralRequestType;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Deferrals;
use AIE\Bundle\AnomalyBundle\Form\DeferralsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Symfony\Component\HttpFoundation\File\File;
use AIE\Bundle\AnomalyBundle\Helper\WorkpackHelper;
use AIE\Bundle\AnomalyBundle\Helper\PDFReportHelper;
use AIE\Bundle\AnomalyBundle\Helper\PDFSummaryReportHelper;

/**
 * Request controller.
 *
 * @Route("projects/{projectId}/deferralrequest")
 */
class DeferralRequestController extends AnomalyBaseController
{

    private function handleDeferralRequestAction(Request $request, $projectId, $entity) {
        $uploader = $this->get('storage.file_uploader');
        $tempEntity = clone $entity;

        $em = $this->getManager();

        $reqEnt = $em->getRepository('AIEAnomalyBundle:Request')->find($request->get('id'));

        if( $reqEnt->getStatus() != -1 ){
            $this->addFlash('error', 'The item is already either approved or rejected.');
            return $this->redirect($this->generateUrl('anomaly_requests'));
        }

        $serializer = $this->get('jms_serializer');
        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');

        $requestData[$request->get('id')] = $anomalyHelper->formatAnomaly(
            $serializer->deserialize($reqEnt->getData(), $reqEnt->getType(), 'json'),
            json_decode($reqEnt->getData(), true)
        );

        $jsonRequestArray = json_decode($reqEnt->getData(), true);

        if ($requestData[$request->get('id')]['Deferral Safe Justification']){
            $tempEntity->setDeferralSafeJustification($requestData[$request->get('id')]['Deferral Safe Justification']);
        }

        if ($requestData[$request->get('id')]['Deferral Justification']){
            $tempEntity->setDeferralJustification($requestData[$request->get('id')]['Deferral Justification']);
        }

        $nextInspectionDate = null;

        if ( $tempEntity instanceof AnomalyRegistrar)
        {
            $nextInspectionDate = $jsonRequestArray['next_inspection_date'];
        }
        else if ($tempEntity instanceof ActionRegistrar)
        {
            if ( $tempEntity->getCode() == 'TRO' && $tempEntity->getInstallationDate() !== null){

                $nextInspectionDate = $jsonRequestArray['next_inspection_date'];
            }
           else{
                $nextInspectionDate = $jsonRequestArray['expiry_date'];
           }
        }

        $nextInspectionDate = new \DateTime(date('Y-m-d H:i:s',strtotime($nextInspectionDate)));


        $editForm = $this->createDeferralRequestForm($request, $projectId, $tempEntity, $nextInspectionDate);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
           
            $data = $editForm->getData();

            $rejectJustification = new DeferralRequestType($entity,$nextInspectionDate);
            $rejectJustification = $rejectJustification->getName();
            $rejectJustification = $this->get('request')->request->get($rejectJustification);


            $entity->setDeferralJustification(NULL);

            $entity->setDeferralSafeJustification(NULL);
           
            /* New Deferral Logic  ACCEPT */
            if ($rejectJustification['status']) {
                if($reqEnt->getRegistrar()->getProject()->getApprovalLevel() == $reqEnt->getApprovalLevel()){
                    $tempEntity->setDeferred($tempEntity->getDeferred() + 1);


                    //to change the registrar next inspection date
                    if ($tempEntity instanceof AnomalyRegistrar) {
                        $tempEntity->setNextInspectionDate($nextInspectionDate);
                        $entity->setNextInspectionDate($nextInspectionDate);
                    } else if ($tempEntity instanceof ActionRegistrar) {
                        if ($tempEntity->getCode() == 'TRO' && $tempEntity->getInstallationDate() !== null) {
                            $tempEntity->setNextInspectionDate($nextInspectionDate);
                            $entity->setNextInspectionDate($nextInspectionDate);
                        } else {
                            $tempEntity->setExpiryDate($nextInspectionDate);
                            $entity->setExpiryDate($nextInspectionDate);
                        }
                    }
                    $entity->setDeferred($tempEntity->getDeferred());
                    $entity->setDeferralRejectJustification(null);

                    $em->persist($entity);
                    $em->flush();

                    $status = $rejectJustification['status'];

                    if (strpos($entity->getStatusTag(), 'new') === false) {
                        $entity->setStatusTag($entity->getTagChoices()['final']['name']);
                    }
                    $tempEntity->setDeferralRejectJustification(null);
                    $requestsManager = $this->get('anomaly_manager.requests');
                    $serializer = $this->get('jms_serializer');
                    $entityRequest = $requestsManager->update($entity,
                        $request->get('id'),
                        $serializer->serialize($tempEntity, 'json'),
                        'deferral',
                        $status,
                        $rejectJustification['deferralRejectJustification']);

                    $em->persist($entityRequest);
                    $em->detach($tempEntity);
                    $em->flush();

                    if($this->container->getParameter('kernel.environment') != 'dev') {
                        $reportGenerator = $this->get('aie_anomaly.report.generator');

                        $requestEntity = $serializer->deserialize($reqEnt->getData(), $reqEnt->getType(), 'json');

                        /** set report name */
                        $reportName = FileHelper::cleanFileName($tempEntity->getFullId());
                        if ($tempEntity instanceof AnomalyRegistrar) {
                            $reportName = 'AR-' . $reportName;
                        }

                        /** Generate report */
                        $report = $reportGenerator->generateReport($requestEntity, $reqEnt, $reportName);
                        $fileName = $reportName . '.' . $report->getExtension();
                        $absFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
                        $file = new File($absFilePath);
                        //$uploadedPath = $this->uploadFile($file, $reqEnt); // upload it
                        $uploadedPath = $uploader->upload($file, DIRECTORY_SEPARATOR . $reqEnt->getUploadDir(), $reqEnt->getAllowedMimeTypes());
                        @unlink($absFilePath); //remove temp file
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE Request SET filepath = :insdate where id=:id");
                        $statement->bindValue('insdate', $uploadedPath);
                        $statement->bindValue('id', $reqEnt->getId());
                        $statement->execute();
                    }

                    $defaultEm = $this->getDoctrine()->getManager();
                    $wpHelper = new WorkpackHelper($em, $defaultEm);
                    $pdf = $wpHelper->definePDFObject();
                    $registrar = $em->getRepository('AIEAnomalyBundle:Registrar')->findOneById($reqEnt->getRegistrar()->getId());
                    $em->refresh($registrar);
                    $anomaly = ($registrar instanceof AnomalyRegistrar ? $registrar : $registrar->getAnomaly());
                    $reportName = FileHelper::cleanFileName($registrar->getFullId());

                    if ($registrar instanceof RepairOrderRegistrar) {
                        $title = 'Repair Order Report';
                        $customId = $registrar->getAnomaly()->getAnomalyCustomId();
                        $img = null;
                    } elseif ($registrar instanceof TemporaryRepairOrderRegistrar) {
                        $title = 'Temporary Repair Order Report';
                        $customId = $registrar->getAnomaly()->getAnomalyCustomId();
                        $img = null;
                    } elseif ($registrar instanceof FabricMaintenanceRegistrar) {
                        $title = 'Fabric Maintenance Report';
                        $customId = $registrar->getAnomaly()->getAnomalyCustomId();
                        $img = null;
                    } elseif ($registrar instanceof AssessmentRegistrar) {
                        $title = 'Assessment Report';
                        $customId = $registrar->getAnomaly()->getAnomalyCustomId();
                        $reportName = 'AR-' . $reportName;
                        $img = null;
                    } else {
                        $title = 'Anomaly Inspection Report';
                        $img = $this->getMonitorImage($anomaly);
                        $customId = $registrar->getAnomalyCustomId();
                    }
                    $anomalyReportObj = new PDFReportHelper($em, $defaultEm, ($this->container->getParameter('kernel.environment') == 'dev' ? true : false), $this->get('storage.file_uploader'));
                    $coverArray = [
                        ['size' => 'L', 'hasImage' => false, 'caption' => 'Title', 'value' => $title],
                        ['size' => 'L', 'hasImage' => true, 'caption' => 'Client Name', 'value' => $registrar->getProject()->getName()],
                        ['size' => 'L', 'hasImage' => false, 'caption' => 'Location', 'value' => $registrar->getLocation()->getName()],
                        ['size' => 'S', 'hasImage' => false, 'caption' => 'Report Ref.', 'value' => 'REP-' . $registrar->getProject()->getId() . '-' . $registrar->getProject()->getReferenceNo() . '-' . $registrar->getCode() . '-' . date('Y')],
                        ['size' => 'S', 'hasImage' => false, 'caption' => 'Date', 'value' => (date('d F Y'))]
                    ];
                    if ($this->container->getParameter('kernel.environment') == 'dev') {
                        $isEnvDev = true;
                        $projectLogo = $registrar->getProject()->getServerFilePath($isEnvDev);
                    } else {
                        $isEnvDev = false;
                        $projectLogo = $uploader->getAbsoluteFilePath($registrar->getProject()->getFilePath());
                    }
                    $anomalyReportObj->createReportCover($pdf, $title, $customId, $projectLogo, $isEnvDev, $coverArray);
                    $pdf->setHeaderInfo($projectLogo, $isEnvDev, $registrar->getProject()->getName() . ' IMS',
                        $title . ' - ' . $customId,
                        'REP-' . $registrar->getProject()->getId() . '-' . $registrar->getProject()->getReferenceNo() . '-' . $registrar->getCode() . '-' . date('Y'),
                        'P');
                    $pdf->AddPage('P', 'A4');
                    $anomalyReportObj->createRegisterReport($pdf, $registrar, $img, 1);
                    $fileName = $reportName . '.pdf';
                    $absFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
                    $pdf->Output($absFilePath, 'F');
                    $file = new File($absFilePath);
                    //$uploadedPath = $this->uploadFile($file, $reqEnt,true); // upload it
                    $uploadedPath = $uploader->upload($file, DIRECTORY_SEPARATOR . $reqEnt->getUploadDir(), $reqEnt->getAllowedMimeTypes());
                    @unlink($absFilePath); //remove temp file
                    $pdf = $wpHelper->definePDFObject(0, 0, 15, 10, 15, 5);
                    $summaryRepObj = new PDFSummaryReportHelper($em, $defaultEm, $isEnvDev, $uploader);
                    $summaryRepObj->createSummaryReport($pdf, $registrar->getProject(), $projectLogo, $registrar);
                    $fileName = $customId . '-summary.pdf';
                    $absFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
                    $pdf->Output($absFilePath, 'F');
                    $file = new File($absFilePath);
                    $uploadedSummaryPath = $uploader->upload($file, DIRECTORY_SEPARATOR . $reqEnt->getUploadDir(), $reqEnt->getAllowedMimeTypes());
                    @unlink($absFilePath);
                    $connection = $em->getConnection();
                    $statement = $connection->prepare("UPDATE Request SET pdffilepath = :insdate, summary_pdffilepath = :summpath where id=:id");
                    $statement->bindValue('insdate', $uploadedPath);
                    $statement->bindValue('summpath', $uploadedSummaryPath);
                    $statement->bindValue('id', $reqEnt->getId());
                    $statement->execute();

                }else{

                    $deferralOwner = $em->getRepository('AIEAnomalyBundle:Deferrals')
                        ->findOneBy( array('actionOwner' => $this->getUser()->getId()));

                    if ($reqEnt->getApprovalLevel() != null)
                        $approvalLevel = $reqEnt->getApprovalLevel() +1;
                    else
                        $approvalLevel = 2;
                    $reqEnt->setApprovalLevel($approvalLevel);
                    $reqEnt->setDeferredBy($deferralOwner);
                    $reqEnt->setAnsweredDate(new \DateTime());
                    $reqEnt->setJustification($rejectJustification['deferralRejectJustification']);
                    $reqEnt->setStatus(-1);
                    $em->persist($reqEnt);
                    $em->flush();
                }

                $this->addFlash('success', $reqEnt->getId() .' Deferral Request is Approved.');

            } else {
                //REJECT
                //$entity->setNextInspectionDate($oldInspectionDate);

                if ( $tempEntity instanceof AnomalyRegistrar)
                {
                    if($data->getNextInspectionDate())
                    {
                        $oldInspectionDate = new \DateTime($data->getNextInspectionDate()->format('Y-m-d H:i:s'));

                        $entity->setNextInspectionDate($oldInspectionDate);
                    }

                } 
                else if ($tempEntity instanceof ActionRegistrar) 
                {
                    if ( $tempEntity->getCode() == 'TRO' && $tempEntity->getInstallationDate() !== null)
                    {
                        if($data->getNextInspectionDate())
                        {
                            $oldInspectionDate = new \DateTime($data->getNextInspectionDate()->format('Y-m-d H:i:s'));

                            $entity->setNextInspectionDate($oldInspectionDate);
                        }

                    }
                    else
                    {
                        $oldInspectionDate = new \DateTime($data->getExpiryDate()->format('Y-m-d H:i:s'));
                        $entity->setExpiryDate($oldInspectionDate);
                    }
                }

                $entity->setDeferred($tempEntity->getDeferred());

                $entity->setDeferralRejectJustification($rejectJustification['deferralRejectJustification']);
                $status = 0;
                $em->persist($entity);
                $em->flush();
                $requestsManager = $this->get('anomaly_manager.requests');
                $serializer = $this->get('jms_serializer');
                $entityRequest = $requestsManager->update($entity,
                    $request->get('id'),
                    $serializer->serialize($tempEntity, 'json'),
                    'deferral',
                    $status);

                $em->persist($entityRequest);
                $em->detach($tempEntity);
                $em->flush();

                $this->addFlash('success', $reqEnt->getId() .' Deferral Request is Rejected.');
            }

            return $this->redirect($this->generateUrl("anomaly_requests"));
        }


        return [
            'entity' => $entity,
            'request' => $reqEnt,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'id' => $entity->getId()
        ];
    }

    /**
     *
     * @Route("/action/{id}", name="anomaly_deferral_request")
     * @Method({"GET", "POST"})
     * @Template("AIEAnomalyBundle:DeferralRequest:edit.html.twig")
     */
    public function deferralRequestAction(Request $request, $projectId, $id) {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:Request')->find($id);
        $registrar_entity = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($entity->getRegistrar()->getId());

        if (! $registrar_entity) {
            $registrar_entity = $em->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($entity->getRegistrar()->getId());
            if (!$registrar_entity)
                throw $this->createNotFoundException('Unable to find Registrar entity.');
        }

        return $this->handleDeferralRequestAction($request, $projectId, $registrar_entity);
    }
 

    private function createDeferralRequestForm(Request $request, $projectId, $entity, $nextInspectionDate) {
        $form_url = "anomaly_deferral_request";
        if ($entity instanceof ActionRegistrar) {
            $form_url = "anomaly_deferral_request";
        }

        $form = $this->createForm(new DeferralRequestType($entity,$nextInspectionDate), $entity, [
            'action' => $this->generateUrl($form_url, ['projectId' => $projectId, 'id' => $request->get('id')]),
            'method' => 'POST'
        ]);

        $form->add('submit', 'submit', $this->options(['label' => 'Submit', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }
    public function getMonitorImage($anomaly){
        if($anomaly->getClassification()->getCode() == "EML" || $anomaly->getClassification()->getCode() == "IML"){
            if ($anomaly->getNominalWT() > 0 and count($anomaly->getMonitors())>1){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitor.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $anomaly->getMonitors()
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }else{
            $trackingTypes=[];
            $trackingCount = [];
            $entities=$anomaly->getMonitors();
            foreach ($entities as $entity){
                $trcks = json_decode($entity->getTrackingCriteria());
                foreach ($trcks as $trck){
                    if (!(in_array ($trck->description,$trackingTypes))){
                        array_push($trackingTypes,$trck->description);
                        $trackingCount[$trck->description] = 0;
                    }
                }
            }
            foreach ($trackingTypes as $tc){
                foreach ($entities as $entity){
                    $trcks = json_decode($entity->getTrackingCriteria());
                    foreach ($trcks as $trck){
                        if ($tc == $trck->description && is_numeric($trck->value) && is_numeric($trck->tracking_conc)){
                            $trackingCount[$trck->description] +=1;
                        }
                    }
                }
            }
            $hasimg = false;
            foreach ($trackingCount as $cc){
                if ($cc > 1){
                    $hasimg=true;
                }
            }
            if ($hasimg){
                $html = $this->renderView('AIEAnomalyBundle:PDFWorkpack:anomalymonitortc.html.twig', array(
                    'anomaly' => $anomaly,
                    'entities' => $entities,
                    'tracking_types'=> $trackingTypes
                ));
                $img = $this->get('knp_snappy.image')->getOutputFromHtml($html);
            }else{
                $img=null;
            }
        }
        return $img;
    }
}
