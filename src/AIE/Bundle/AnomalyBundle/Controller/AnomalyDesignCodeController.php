<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode;
use AIE\Bundle\AnomalyBundle\Form\AnomalyDesignCodeType;

/**
 * AnomalyDesignCode controller.
 *
 * @Route("projects/{projectId}/codes")
 */
class AnomalyDesignCodeController extends AnomalyBaseController
{
    protected $editVisible = 0;
    protected $createVisible = 0;
    protected $showVisible = 0;
    /**
     * Lists all AnomalyDesignCode entities.
     *
     * @Route("/", name="anomaly_codes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {
        if (!$this->securityHelper->isRoleGranted('ROLE_CODES_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));


        //Checking if user has permission to edit, add, delete
        if ($this->securityHelper->isRoleGranted('ROLE_CODES_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CODES_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CODES_SHOW', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->showVisible = 1;
        }

        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->findByProject($projectId);
        $codes = [];
        array_walk(
            $entities,
            function (AnomalyDesignCode $code) use (&$codes) {
                $codes[$code->getType()][] = $code;
            }
        );

        return array(
            'entities' => $entities,
            'codes' => $codes,
            'anomalyCategories' => AnomalyRegistrar::getAnomalyCategories(),
            'projectId' => $projectId,
            'editGranted' => $this->editVisible,
            'createGranted' => $this->createVisible,
            'showGranted' => $this->showVisible,
        );
    }

    /**
     * Creates a new AnomalyDesignCode entity.
     *
     * @Route("/", name="anomaly_codes_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:AnomalyDesignCode:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new AnomalyDesignCode();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();

            $code = $form->getData()->getCode();

            $type = $form->getData()->getType();

            $isUsed = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->findBy(
                ['project' => $projectId, 'code' => $code, 'type' => $type]
            );

            if ($isUsed) {
                $this->get('session')->getFlashBag()->add(
                    'error',
                    "Oops! This Code ({$code}) is already in use."
                );
            } else {

                $entity->setProject($em->getRepository('AIEAnomalyBundle:Projects')->find($projectId));
                $em->persist($entity);
                $em->flush();
            }


            return $this->redirect($this->generateUrl('anomaly_codes', ['projectId' => $projectId]));
        }

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AnomalyDesignCode entity.
     *
     * @param AnomalyDesignCode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AnomalyDesignCode $entity, $projectId)
    {
        $form = $this->createForm(
            new AnomalyDesignCodeType(),
            $entity,
            array(
                'action' => $this->generateUrl('anomaly_codes_create', ['projectId' => $projectId]),
                'method' => 'POST',
            )
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Displays a form to create a new AnomalyDesignCode entity.
     *
     * @Route("/new", name="anomaly_codes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_CODES_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $entity = new AnomalyDesignCode();
        $form = $this->createCreateForm($entity, $projectId);

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a AnomalyDesignCode entity.
     *
     * @Route("/{id}", name="anomaly_codes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_CODES_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        if ($this->securityHelper->isRoleGranted('ROLE_CODES_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyDesignCode entity.');
        }



        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'editGranted' => $this->editVisible,
        );
    }

    /**
     * Displays a form to edit an existing AnomalyDesignCode entity.
     *
     * @Route("/{id}/edit", name="anomaly_codes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_CODES_EDIT',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyDesignCode entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $deleteForm = $this->createDeleteForm($projectId, $id);
        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Creates a form to edit a AnomalyDesignCode entity.
     *
     * @param AnomalyDesignCode $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AnomalyDesignCode $entity, $projectId)
    {
        $form = $this->createForm(
            new AnomalyDesignCodeType(),
            $entity,
            array(
                'action' => $this->generateUrl(
                    'anomaly_codes_update',
                    array('projectId' => $projectId, 'id' => $entity->getId())
                ),
                'method' => 'PUT',
            )
        );

        $form->add(
            'submit',
            'submit',
            $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn')
        );

        return $form;
    }

    /**
     * Edits an existing AnomalyDesignCode entity.
     *
     * @Route("/{id}", name="anomaly_codes_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:AnomalyDesignCode:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyDesignCode entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $data = $editForm->getData();
            $em->persist($data);
            $em->flush();


            return $this->redirect($this->generateUrl('anomaly_codes', array('projectId' => $projectId)));
        }

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a AnomalyDesignCode entity.
     *
     * @Route("/{id}", name="anomaly_codes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $projectId, $id)
    {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AnomalyDesignCode entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_codes', ['projectId' => $projectId]));
    }

    /**
     * Creates a form to delete a AnomalyDesignCode entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_codes_delete', array('projectId' => $projectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr'=>['class'=>'btn btn-danger right']))
            ->getForm();
    }
}
