<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 01/11/15
 * Time: 01:38
 */

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Form\RegistrarSearchType;
use AIE\Bundle\VeracityBundle\Helper\EntityHelper;
use Symfony\Component\HttpFoundation\Request;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class ActionRegistrarController extends RegistrarController
{

    protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar';
    protected $registrarClassName = 'ActionRegistrar';
    protected $registrarRouteBase = '';
    protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\ActionRegistrarType';
    protected $items=['RO'=>'REPAIR_ORDER','TRO'=>'TEMPORARY_REPAIR_ORDER','FM'=>'FABRIC_MAINTENANCE_ORDER','AS'=>'ASSESSMENT_ORDER'];



    public function indexAction(Request $request, $projectId, $type = null)
    {
        $em = $this->getManager();
        $userHelper = $this->get('aie_anomaly.user.helper');

        $anomalyTypes = $this->container->getParameter('anomaly')['mappings'];

        /** @var Projects $project */
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }

        $entity = new $this->registrarClass();
        $entity->setProject($project);
        $searchForm = $this->createSearchForm($projectId, $entity, $type);
        $searchForm->handleRequest($request);
        $checkDeferExistsStruct = [];
        $status = [];
        $entities = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->findByProject($projectId);

        $typeName=($type==null?'Master':AnomalyRegistrar::getAnomalyCategories()[strtoupper($type)]);

        $data = $nextInspectionDate = $risk = $expiryDate = null;

        $grantArray=[
            "RO"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0,'defer'=>0],
            "TRO"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0,'defer'=>0],
            "FM"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0,'defer'=>0],
            "AS"=>['show'=>0,'add'=>0,'edit'=>0,'delete'=>0,'files'=>0,'monitor'=>0,'history'=>0,'defer'=>0]

        ];

        foreach ($this->items as $key => $value){


            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_SHOW', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['show']=1;
            }

            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_EDIT', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['edit']=1;
            }

            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_FILES', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['files']=1;

            }

            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_HISTORY', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['history']=1;
            }
            if ($this->securityHelper->isRoleGranted('ROLE_' . $value . '_DEFER', $this->userRolesMergeToCheck)) {
                $grantArray[$key]['defer']=1;
            }
        }


        $hasPermission=true;

        if ( $entity->getCode() == 'RO') {
            if (!$this->securityHelper->isRoleGranted('ROLE_REPAIR_ORDER_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show Repair Orders";
            }
        }
        if ( $entity->getCode() == 'TRO') {
            if (!$this->securityHelper->isRoleGranted('ROLE_TEMPORARY_REPAIR_ORDER_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show Temporary Repair Orders";
            }
        }
        if ( $entity->getCode() == 'FM' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_FABRIC_MAINTENANCE_ORDER_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show Fabric Maintenance Orders";
            }
        }
        if ( $entity->getCode() == 'AS' ) {
            if (!$this->securityHelper->isRoleGranted('ROLE_ASSESSMENT_ORDER_LIST', $this->userRolesMergeToCheck)) {
                $hasPermission = false;
                $message = "You don't have permission to show Assessment Orders";
            }
        }


        if(!$hasPermission){
            $this->addFlash('error', $message);
//            $referer = $this->getRequest()->headers->get('referer');
//            if ($referer == NULL) {
//                return $this->redirect($this->generateURL('anomaly_projects', array('id' => $projectId)));
//            } else {
//                return $this->redirect($referer);
//            }
        }


        if ($request->isMethod('POST')) {
            if ($searchForm->isValid()) {
                $data = $searchForm->getData();



                $filter_vars = [
                    'assetTagNumber' => $searchForm->get('assetTagNumber')->getData(),
                    'component' => $searchForm->get('component')->getData(),
                    //'status' => $searchForm->get('status')->getData(),
                    'anomalyCustomId' => $searchForm->get('anomalyCustomId')->getData(),
                    'threat' => $searchForm->get('threat')->getData(),
                    'location' => $searchForm->get('location')->getData(),
                    'project' => $projectId,
                    'classification' => $searchForm->get('classification')->getData(),
                ];

                $entities = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->search($filter_vars);

                $risk = $searchForm->get('riskCriticality')->getData();

                if (isset($risk))
                {
                    $entities = array_filter(
                        $entities,
                        function ($entity) use ($risk)
                        {
                            if ($entity->getRisk() == $risk)
                            {
                                return true;
                            }
                            return false;
                        }
                    );
                }

                if ($searchForm->get('raisedOn')->getData())
                {
                    $raisedOnDate = $searchForm->get('raisedOn')->getData();
                    $raisedOnDate = new \DateTime($raisedOnDate);
                    $raisedOnDate = $raisedOnDate->format('Y-m-d');
                    $entities = array_filter(
                        $entities,
                        function ($entity) use ($raisedOnDate)
                        {
                            if ($raisedOnDate && $entity->getRaisedOn()->format('Y-m-d') == $raisedOnDate)
                            {
                                return true;
                            }
                            return false;
                        }
                    );

                }
            }
        }

        $actions = [];

        if ($type !== null) { // convert it to lower case
            $type = strtoupper($type);
        }

        $type = (array_key_exists($type, $anomalyTypes)) ? $anomalyTypes[$type] : null;
        $registrarClass = $this->registrarClass;

        if ($type) { // Filter based on anomaly type
            $entities = array_filter(
                $entities,
                function ($entity) use ($type) {
                    return $entity instanceof $type;
                }
            );
        }


        if ($data) {
            //$risk = $searchForm->get('riskCriticality')->getData();
            $nextInspectionDate = $searchForm->get('nextInspectionDate')->getData();
            if ($nextInspectionDate) {
                $nextInspectionDate = new \DateTime($nextInspectionDate);
            }

            if ($searchForm->has('expiryDate')) {

                $expiryDate = $searchForm->get('expiryDate')->getData();
                if ($expiryDate) {
                    $expiryDate = new \DateTime($expiryDate);
                }
            }
        }


        /**
         * Approval Stage, Action Owner is linked to the respective registrarClass
         */
        foreach ($entities as $entity) { // Filter based on action type
            $_actions = $entity->getActions()->toArray();

            $_actions = array_filter(
                $_actions,
                function ($action) use ($registrarClass, $data, $risk, $nextInspectionDate, $expiryDate) {
                    if ($data) {
                        if ($nextInspectionDate && $action->getDueDate() != $nextInspectionDate) {
                            return false;
                        }

                        if ($expiryDate && $action->getExpiryDate() != $expiryDate) {
                            return false;
                        }

                        /*if ($risk && $action->getRisk() != $risk) {
                            return false;
                        }*/

                        if ($data->getStatus() && ( $data->getStatus() - 1 ) != $action->getStatus()) {
                            return false;
                        }

                        // Approval Stage
                        if ($data->getStatusTag() && $data->getStatusTag() != $action->getStatusTag()) {
                            return false;
                        }

                        // Action Owner
                        if ($data->getSpa() && $data->getSpa() != $action->getSpa()) {
                            return false;
                        }
                    }

                    return $action instanceof $registrarClass;
                }
            );

            $actions = array_merge($actions, $_actions);
            $requestUpdatedDeferredStruct = $userHelper->getRequestsByIdUpdatedOrDeferred($entity->getId());

            if (count($requestUpdatedDeferredStruct)) {
                $checkDeferExistsStruct[$entity->getId()] = 1;
            } else {
                $checkDeferExistsStruct[$entity->getId()] = 0;
            }
        }

        $userIds = [];
        foreach ($actions as $action) {
            $id = (!empty($action->getSpa()) ? $action->getSpa()->getUserId() : 0);
            //$id = $action->getSpa();
            if ($id && !isset($userIds [$id])) {
                $userIds [$id] = $id;
            }
            $status[$action->getId()]=$action->getStatusTag();
        }

        $userObjs = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->findById(
            array_values($userIds)
        );
        $users = [];
        foreach ($userObjs as $user) {
            $users[$user->getId()] = $user;
        }

        return [
            'entities' => $actions,
            'projectId' => $projectId,
            'project' => $project,
            'users' => $users,
            'typeName'=>$typeName,
            'search_form' => $searchForm->createView(),
            'DeferLinkStruct' => $checkDeferExistsStruct,
            'status'=> $status,
            'granted_list'=>$grantArray,
        ];
    }

    /**
     * Creates a form for advanced search for a AnomalyRegistrar entity.
     *
     * @param Registrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createSearchForm($projectId, $entity, $type)
    {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity->setProject($project);

        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        if (count($actionOwners) > 0){
            if (!$entity->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project'=>$projectId));
                $userActionId = $userAction->getId();
            }
            else{
                $userActionId = $entity->getSpaId();
            }
        }else{
            $userActionId=null;
        }



        $form = $this->createForm(
            new RegistrarSearchType($entity, $em, $actionOwners, $userActionId),
            $entity,
            [
                'action' => $this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId, 'type' => $type]),
                'method' => 'POST',
                'data_class' => $this->registrarClass,
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Search', 'attr' => ['class' => 'btn-block']], 'btn'));
//        $form->add('Reset', ResetType::class, $this->options(['label' => 'Reset', 'attr' => ['class' => 'btn-block btn-warning']], 'btn'));
        $form->add('Clear', 'button', $this->options(['label' => 'Clear', 'attr' => ['class' => 'btn-block btn-warning']], 'btn'));

        return $form;
    }


    public function masterAction(Request $request, $projectId, $type)
    {
        return self::indexAction($request, $projectId, $type);
    }

    public function showAction($id, $projectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->registrarClassName.' entity.');
        }

        if ($entity instanceof RepairOrderRegistrar && !($this->securityHelper->isRoleGranted('ROLE_REPAIR_ORDER_SHOW', $this->userRolesMergeToCheck))) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show Repair Order Register."));
//            throw new \Exception("You don't have permission to show Repair Order Anomaly Register!");
        }
        if ($entity instanceof TemporaryRepairOrderRegistrar && !($this->securityHelper->isRoleGranted('ROLE_TEMPORARY_REPAIR_ORDER_SHOW', $this->userRolesMergeToCheck))) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show show Temporary Repair Order Register."));

//            throw new \Exception("You don't have permission to show Temporary Repair Order Anomaly Register!");
        }
        if ($entity instanceof FabricMaintenanceRegistrar && !($this->securityHelper->isRoleGranted('ROLE_FABRIC_MAINTENANCE_ORDER_SHOW', $this->userRolesMergeToCheck))) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show Fabric Maintenance Order Register."));
//            throw new \Exception("You don't have permission to show Fabric Maintenance Anomaly Register!");
        }

        if ($entity instanceof AssessmentRegistrar && !($this->securityHelper->isRoleGranted('ROLE_ASSESSMENT_ORDER_SHOW', $this->userRolesMergeToCheck))) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show Assessment Order Register."));
//            throw new \Exception("You don't have permission to show Assessment Anomaly Register!");
        }


        return [
            'entity' => $entity,
            'project' => $entity->getProject(),
            'form' => $this->createShowForm($entity)->createView(),
            'anomaly_form' => $this->get('aie_anomaly.form.helper')->createShowAnomalyForm($entity->getAnomaly())->createView(),
            'projectId' => $projectId,
        ];
    }

    public function showDeferralAction($id, $projectId)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->registrarClassName.' entity.');
        }


        return [
            'entity' => $entity,
            'project' => $entity->getProject(),
            'form' => $this->createShowForm($entity)->createView(),
            'anomaly_form' => $this->get('aie_anomaly.form.helper')->createShowAnomalyForm($entity->getAnomaly())->createView(),
            'projectId' => $projectId,
        ];
    }

    public function editAction($id, $projectId, $type)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->registrarClassName.' entity.');
        }

        if (!$this->securityHelper->isRoleGranted('ROLE_' . $this->items[$entity->getCode()] . '_EDIT', $this->userRolesMergeToCheck)) {
            $this->addFlash('error', 'Permission to edit order is denied');
            $referer = $this->getRequest()->headers->get('referer');
            if ($referer == NULL) {
                return $this->redirect($this->generateURL('anomaly_action_ro', array('projectId' => $projectId)));
            } else {
                return $this->redirect($referer);
            }
        }

        $project = $entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $project = $entity->getAnomaly()->getProject();
        $editForm = $this->createEditForm($entity, $projectId, $type, $actionOwners);

        $anomaly = $entity->getAnomaly();
        $anomalyFormType = "AIE\\Bundle\\AnomalyBundle\\Form\\".(new \ReflectionClass($anomaly))->getShortName().'Type';
        if (count($actionOwners) > 0) {
            if (!$anomaly->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project' => $projectId));
                $userActionId = $userAction->getId();
            } else {
                $userActionId = $anomaly->getSpaId();
            }
        }else{
            $userActionId=null;
        }
        $anomalyForm = $this->createForm(
            new $anomalyFormType($anomaly, $em, $actionOwners, $userActionId),
            $anomaly,
            [
                'disabled' => true,
                'inherit_data' => false,
            ]
        );

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'anomaly_form' => $anomalyForm->createView(),
            'project' => $project,
            'projectId' => $projectId,
        ];
    }


    /**
     * Creates a form to edit a RepairOrderRegistrar entity.
     *
     * @param RepairOrderRegistrar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm(ActionRegistrar $entity, $projectId, $type, $actionOwners)
    {
        $anomaly = $entity->getAnomaly();

        $form = $this->createForm(
            new $this->registrarType($entity, $anomaly, $actionOwners),
            $entity,
            [
                'action' => $this->generateUrl(
                    $this->registrarRouteBase.'_update',
                    ['projectId' => $projectId, 'id' => $entity->getId(), 'type' => $type]
                ),
                'method' => 'PUT',
            ]
        );
        $appLevel = $entity->getAnomaly()->getProject()->getProjectApprovalLevels();
        $form->add('submit', 'submit', $this->options(['label' => (
        $appLevel == 1 ? 'Submit Update for Approval' : 'Submit Update for Review'
        ), 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    protected function createShowForm(ActionRegistrar $entity)
    {
        $project = $entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $anomaly = $entity->getAnomaly();

        $form = $this->createForm(
            new $this->registrarType($entity, $anomaly, $actionOwners),
            $entity,
            [
                'disabled' => true,
                'read_only' => true,
                'action' => '#',
                'method' => 'POST',
            ]
        );

        $form->remove('files_group');

        return $form;
    }

    public function updateAction(Request $request, $projectId, $id, $type)
    {

        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:'.$this->registrarClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->registrarClassName.' entity.');
        }


        $anomaly = $entity->getAnomaly();
        $project = $anomaly->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);


        $isRiskProject = $project instanceof RiskProject;
        $tempEntity = clone $entity;

        $editForm = $this->createEditForm($tempEntity, $projectId, $type, $actionOwners);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $data = $editForm->getData();
            /**
             * Create the request based on the new data
             */
            $this->setEntityFields($tempEntity, $data);

            $files = $editForm->get('files_group')->get('files')->getData();
            if ($files) {
                foreach ($files as $file) {
                    $_file = $file->getFile();
                    if (!$_file) {
                        continue;
                    }
                    $file->setAnomaly($entity->getAnomaly());
                    $uploadedPath = $this->uploadFile($_file, $file);
                    $file->setFilepath($uploadedPath);
                    $file->setSize($_file->getSize());
                    $file->setDate(new \DateTime());
                    $em->persist($file);
                }
            }
            if ($entity instanceof AssessmentRegistrar){
                $entity->setAssessmentResult($data->getAssessmentResult());
            }


            $requestsManager = $this->get('anomaly_manager.requests');
            $serializer = $this->get('jms_serializer');
            $entityRequest = $requestsManager->add($entity, $serializer->serialize($tempEntity, 'json'), 'update');
            $entityRequest->setRequestFiles($files);
            $entityRequest->setAttachedreportanomalys(json_encode($request->request->get('anomaly_file_edit_check')));
            $em->persist($entityRequest);
            $em->detach($tempEntity);

            //if (strpos($entity->getStatusTag(), 'request') === false) {
                $entity->setStatusTag($entity->getTagChoices()['request']['name']);
            //}
            $entity->addRequest($entityRequest);

            $em->flush();

            $this->addFlash('success', 'Action Item '.$entity->getFullId().' updated and waiting for approval.');
            return $this->redirect(
                $this->generateUrl($this->registrarRouteBase, ['projectId' => $projectId, 'type' => $type])
            );
        }

        $anomalyFormType = "AIE\\Bundle\\AnomalyBundle\\Form\\".(new \ReflectionClass($anomaly))->getShortName().'Type';
        if (count($actionOwners) > 0) {
            if (!$anomaly->getSpaId()) {
                $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project' => $projectId));
                $userActionId = $userAction->getId();
            } else {
                $userActionId = $anomaly->getSpaId();
            }
        }else{
            $userActionId=null;
        }
        $anomalyForm = $this->createForm(
            new $anomalyFormType($anomaly, $em, $actionOwners, $userActionId),
            $anomaly,
            [
                'disabled' => true,
                'inherit_data' => false,
            ]
        );

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'anomaly_form' => $anomalyForm->createView(),
            'project' => $project,
            'projectId' => $projectId,
        ];
    }

    protected function setEntityFields($entity, $data)
    {
        $entity->setSpa($data->getSpa());
		$entity->setApprovedspa($data->getApprovedspa());
    }
}