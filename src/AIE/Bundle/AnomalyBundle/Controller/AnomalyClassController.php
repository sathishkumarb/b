<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Helper\AnomalyHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyClass;
use AIE\Bundle\AnomalyBundle\Form\AnomalyClassType;

/**
 * AnomalyClass controller.
 *
 * @Route("projects/{projectId}/classes")
 */
class AnomalyClassController extends AnomalyBaseController
{

    /**
     * Lists all AnomalyClass entities.
     *
     * @Route("/", name="anomaly_classes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId)
    {
        $editVisible = 0;
        $createVisible = 0;


        if (!$this->securityHelper->isRoleGranted('ROLE_CLASS_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));



        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->findByProject($projectId);

        //      Checking permission to show

        //Checking if user has permission to edit, add, delete
        if ($this->securityHelper->isRoleGranted('ROLE_CLASS_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
                $editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_CLASS_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $createVisible = 1;
        }


        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'editGranted' => $editVisible,
            'createGranted' => $createVisible,
        );
    }
    /**
     * Creates a new AnomalyClass entity.
     *
     * @Route("/", name="anomaly_classes_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:AnomalyClass:new.html.twig")
     */
    public function createAction($projectId, Request $request)
    {
        $entity = new AnomalyClass();
        $form = $this->createCreateForm($projectId, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();

            $code = $form->getData()->getCode();
	        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
            $isUsed = $anomalyHelper->isAnomalyCodeUsed($projectId, $code);
            if($isUsed){
                $this->get('session')->getFlashBag()->add('error', "Oops! This Anomaly Code ({$code}) is already in use.");
            }else{
                $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $entity->setProject($project);
                $em->persist($entity);
                $em->flush();
            }

            return $this->redirect($this->generateURL('anomaly_classes', array('projectId' => $projectId)));
        }

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AnomalyClass entity.
     *
     * @param AnomalyClass $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId, AnomalyClass $entity)
    {
        $form = $this->createForm(new AnomalyClassType(), $entity, array(
            'action' => $this->generateURL('anomaly_classes_create', ['projectId' => $projectId,]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new AnomalyClass entity.
     *
     * @Route("/new", name="anomaly_classes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId)
    {
        $entity = new AnomalyClass();
        $form   = $this->createCreateForm($projectId, $entity);

        if (!$this->securityHelper->isRoleGranted('ROLE_CLASS_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        return array(
            'entity' => $entity,
            'projectId' => $projectId,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a AnomalyClass entity.
     *
     * @Route("/{id}", name="anomaly_classes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyClass entity.');
        }

        if (!($this->securityHelper->isRoleGranted('ROLE_CLASS_SHOW', $this->userRolesMergeToCheck))  && !$this->assignedAsAdmin) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);

        return array(
            'entity'      => $entity,
            'projectId' => $projectId,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing AnomalyClass entity.
     *
     * @Route("/{id}/edit", name="anomaly_classes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyClass entity.');
        }

        if (!$this->securityHelper->isRoleGranted('ROLE_CLASS_EDIT',$this->userRolesMergeToCheck )   && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $editForm = $this->createEditForm($projectId, $entity);
        $deleteForm = $this->createDeleteForm($projectId, $id);

        return array(
            'entity'      => $entity,
            'projectId' => $projectId,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a AnomalyClass entity.
    *
    * @param AnomalyClass $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm($projectId, AnomalyClass $entity)
    {
        $form = $this->createForm(new AnomalyClassType(), $entity, array(
            'action' => $this->generateURL('anomaly_classes_update', array('projectId' => $projectId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }
    /**
     * Edits an existing AnomalyClass entity.
     *
     * @Route("/{id}", name="anomaly_classes_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:AnomalyClass:edit.html.twig")
     */
    public function updateAction($projectId, Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AnomalyClass entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);
        $editForm = $this->createEditForm($projectId, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $code = $editForm->getData()->getCode();
	        $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');
            $isUsed = $anomalyHelper->isAnomalyCodeUsed($projectId, $code);
            if($code != $entity->getCode() && $isUsed){
                $this->get('session')->getFlashBag()->add('error', "Oops! This Anomaly Code ({$code}) is already in use.");
            }else {
                $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $entity->setProject($project);
                $em->flush();
            }

            return $this->redirect($this->generateURL('anomaly_classes_edit', array('projectId'=>$projectId, 'id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'projectId' => $projectId,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a AnomalyClass entity.
     *
     * @Route("/{id}", name="anomaly_classes_delete")
     * @Method("DELETE")
     */
    public function deleteAction($projectId, Request $request, $id)
    {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AnomalyClass entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateURL('anomaly_classes', ['projectId' => $projectId,]));
    }

    /**
     * Creates a form to delete a AnomalyClass entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateURL('anomaly_classes_delete', array('projectId' => $projectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger right hide')), 'btn'))
            ->getForm()
        ;
    }
}
