<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\PipingAnomaly;
use AIE\Bundle\AnomalyBundle\Form\PipingAnomalyType;

/**
 * PipingAnomaly controller.
 *
 * @Route("projects/{projectId}/piping")
 */
class PipingAnomalyController extends AnomalyRegistrarController {

	protected $registrarClassName = 'PipingAnomaly';
	protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\PipingAnomaly';
	protected $registrarRouteBase = 'anomaly_pi';
	protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\PipingAnomalyType';

    /**
     * Deletes a PipingAnomaly entity.
     *
     * @Route("/{id}/remove", name="anomaly_pi_remove")
     * @Method("GET")
     */
    public function deleteIndividualAction(Request $request, $projectId, $id) {
        return parent::deleteIndividualAction($request, $projectId, $id);
    }

    /**
     * Show the request history of an anomaly
     *
     * @Route("/{id}/history", name="pi_registrar_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Registrar:history.html.twig")
     */
    public function historyAction($id)
    {
        return parent::historyAction($id);
    }

	/**
	 * Lists all PipingAnomaly entities.
	 *
	 * @Route("/", name="anomaly_pi")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function indexAction(Request $request, $projectId)
    {
        return parent::indexAction($request, $projectId);
    }

	/**
	 * Creates a new PipingAnomaly entity.
	 *
	 * @Route("/new", name="anomaly_pi_create")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:PipingAnomaly:new.html.twig")
	 */
	public function createAction(Request $request, $projectId) {
		return parent::createAction($request, $projectId);
	}

	protected function setNewEntityFields(&$entity, $data) {
		parent::setNewEntityFields($entity, $data);
	}


	/**
	 * Displays a form to create a new PipingAnomaly entity.
	 *
	 * @Route("/new", name="anomaly_pi_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($projectId) {
		return parent::newAction($projectId);
	}


	/**
	 * Shows the graph summary of the piping anomalies for project
	 *
	 * @Route("/visual", name="anomaly_pi_vs")
	 * @Method("GET")
	 * @Template("AIEAnomalyBundle:VisualSummary:piping.html.twig")
	 */
	public function visualSummaryAction($projectId, Request $request) {
		return parent::visualSummaryAction($projectId, $request);
	}

	/**
	 * Finds and displays a PipingAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_pi_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($projectId, $id) {
		return parent::showAction($projectId, $id);
	}

	/**
	 * Displays a form to edit an existing PipingAnomaly entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_pi_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($projectId, $id) {
		return parent::editAction($projectId, $id);
	}

	/**
	 * Edits an existing PipingAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_pi_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:PipingAnomaly:edit.html.twig")
	 */
	public function updateAction(Request $request, $projectId, $id) {
		return parent::updateAction($request, $projectId, $id);
	}

	/**
	 * Deletes a PipingAnomaly entity.
	 *
	 * @Route("/{id}", name="anomaly_pi_delete")
	 * @Method("DELETE")
	 */
    public function deleteAction(Request $request, $projectId, $id) {
        return parent::deleteAction($request, $projectId, $id);
	}

    /**
     * Finds and displays a PipingAnomaly entity.
     *
     * @Route("/{id}/delete", name="anomaly_pi_show_delete")
     * @Method("GET")
     * @Template()
     */
    public function showdeleteAction(Request $request, $projectId, $id)
    {
        return parent::showdeleteAction($request, $projectId, $id);
    }

	
	/**
	 * @param $projectId
	 * @param $id
	 *
	 * @Route("/{id}/duplicate", name="anomaly_pi_duplicate")
	 * @Method("GET")
	 * @Template("AIEAnomalyBundle:PipingAnomaly:edit.html.twig")
	 */
	public function duplicateAction($projectId, $id) {
		return parent::duplicateAction($projectId, $id);
	}

	/**
	 * Creates a new PipingAnomaly entity.
	 *
	 * @Route("/{id}//clone", name="anomaly_pi_clone")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:PipingAnomaly:edit.html.twig")
	 */
    public function cloneAction($projectId, Request $request, $id)
    {
        return parent::cloneAction($projectId, $request, $id);
    }
}