<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\StorageBundle\Upload\FileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\FilesCategories;

/**
 * FilesCategories controller.
 *
 * @Route("anomaly/{refId}/files/categories")
 */
class FilesCategoriesController extends AnomalyBaseController {

    private $fileuploader;

    private function generateTree($categories, $files)
    {

        $tree = [];
        $_catFiles = $files;

        foreach ($_catFiles as $key => $file)
        {
            if ($file->getCategory() === null)
            {
                $tree[] = $file->getAsTreeNode($this->fileuploader);
                unset($_catFiles[$key]);
            }
        }

        foreach ($categories as $category)
        {
            $categoryNode = $category->getAsTreeNode($_catFiles, $this->fileuploader);
            $tree[] = $categoryNode;
        }

        return $tree;
    }

    /**
     * Lists all FilesCategories entities.
     *
     * @Route("/", name="anomaly_files_categories", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function getFilesTreeAction($refId)
    {
        $em = $this->getManager();
        $folders = $em->getRepository('AIEAnomalyBundle:FilesCategories')->findBy(['anomaly' => $refId, 'parent' => null]);
        $files = $em->getRepository('AIEAnomalyBundle:Files')->findBy(['anomaly' => $refId]);

        $this->fileuploader = $this->get('storage.file_uploader');

        $tree = $this->generateTree($folders, $files);

        return new JsonResponse($tree);
    }

    /**
     * Creates a new FilesCategories entity.
     *
     * @Route("/", name="anomaly_files_categories_create", options={"expose"=true})
     * @Method("POST")
     */
    public function createAction(Request $request, $refId)
    {
        $entity = new FilesCategories();
        $name = $request->get('name');
        $parentId = $request->get('parent');
        $response = false;

        if ($name)
        {
            $em = $this->getManager();
            $anomaly = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($refId);

            if ($anomaly)
            {
                $entity->setName($name);
                $entity->setAnomaly($anomaly);
                if ($parentId)
                {
                    $parent = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($parentId);
                    $entity->setParent($parent);
                }

                $em->persist($entity);
                $em->flush();

                $response = true;
            }
        }

        return new JsonResponse(['success' => $response, 'id' => $entity->getId()]);
    }

    /**
     * Edits an existing FilesCategories entity.
     *
     * @Route("/{id}", name="anomaly_files_categories_update", options={"expose"=true})
     * @Method("PUT")
     */
    public function updateAction(Request $request, $refId, $id)
    {
        $em = $this->getManager();

        $category = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($id);

        if (! $category)
        {
            throw $this->createNotFoundException('Unable to find FilesCategories entity.');
        }

        $name = $request->get('name');
        $response = false;

        if ($name)
        {
            $category->setName($name);
            $em->flush();
            $response = true;
        }

        return new JsonResponse(['success' => $response]);
    }

    /**
     * Deletes a FilesCategories entity.
     *
     * @Route("/{id}", name="anomaly_files_categories_delete", options={"expose"=true})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $refId, $id)
    {
        $em = $this->getManager();
        $response = false;
        $entity = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find FilesCategories entity.');
        }

        if ($em->getRepository('AIEAnomalyBundle:FilesCategories')->deleteCategory($id))
        {
            $response = true;
        }

        return new JsonResponse(['success' => $response]);
    }


    /**
     * Creates a new FilesCategories entity.
     *
     * @Route("/move/{id}", name="anomaly_files_categories_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveAction(Request $request, $refId, $id)
    {
        $em = $this->getManager();
        $response = false;
        $category = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($id);

        if ($category)
        {
            $parentId = $request->get('parent');
            if ($parentId)
            {
                $parent = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($parentId);
                if ($parent)
                {
                    $category->setParent($parent);
                    $response = true;
                }
            } else
            {
                $category->setParent(NULL);
                $response = true;
            }

            $em->flush();
        }

        return new JsonResponse(['success' => $response]);
    }


	/**
     * Creates a new FilesCategories entity.
     *
     * @Route("/move_file/{id}", name="anomaly_files_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveFileAction(Request $request, $refId, $id)
    {
        $em = $this->getManager();
        $response = false;
        $file = $em->getRepository('AIEAnomalyBundle:Files')->find($id);

        if ($file)
        {
            $parentId = $request->get('parent');
            if ($parentId)
            {
                $parent = $em->getRepository('AIEAnomalyBundle:FilesCategories')->find($parentId);
                if ($parent)
                {
                    $file->setCategory($parent);
                    $response = true;
                }
            } else
            {
                $file->setCategory(NULL);
                $response = true;
            }

            $em->flush();
        }

        return new JsonResponse(['success' => $response]);
    }
}
