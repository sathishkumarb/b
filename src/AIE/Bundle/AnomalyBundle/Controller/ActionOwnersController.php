<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Controller\AnomalyBaseController;
use AIE\Bundle\AnomalyBundle\Entity\ActionOwners;
use AIE\Bundle\AnomalyBundle\Form\ActionOwnersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * ActionOwners controller.
 *
 * @Route("/project/{projectId}/ao")
 */
class ActionOwnersController extends AnomalyBaseController {

    private $layout = 'AIEAnomalyBundle:Default:layout.html.twig';
    private $route_base = 'anomaly_ao';


    /**
     * Lists all Action Owners
     *
     * @Route("/", name="anomaly_ao")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:ActionOwners:index.html.twig")
     */
    public function indexAction($projectId)
    {
        $createVisible = 0;
        $deleteVisible = 0;




//        echo("isAdmin" . !$this->assignedAsAdmin);
//        print_r( $this->userRolesMergeToCheck);
        //Checking if user has permission to add, delete

        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_DELETE', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $deleteVisible = 1;
        }


        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($projectId);


        foreach ($entities as $entity)
        {
	        $entity->setUser($this->getDoctrine()->getManager()->getRepository('UserBundle:User')->find($entity->getUserId()));
            $deleteForm = $this->createDeleteForm($entity->getId(), $projectId);
            $entity->delete_form = $deleteForm->createView();
        }
        
        return array(
            'entities'   => $entities,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
            'projectId' => $projectId,
            'createGranted' => $createVisible,
            'deleteGranted' => $deleteVisible,
        );
    }

    /**
     * Creates a new Action Owner.
     *
     * @Route("/", name="anomaly_ao_create")
     * @Method("POST")
     * @Template("AIEVeracityBundle:ActionOwners:new.html.twig")
     */
    public function createAction(Request $request, $projectId)
    {
        $entity = new ActionOwners();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);


	    $em = $this->getManager();
	    if (is_numeric($form->get('user')->getData())){
            $userId = $form->get('user')->getData();
        }else{
            $userId = $form->get('user')->getData()->getId();
        }

        if ($form->isValid())
        {
	        if(!$em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('userId'=>$userId,'project'=>$projectId))){
		        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
		        $entity->setUserId($userId);
		        $entity->setProject($project);
		        $em->persist($entity);
		        $em->flush();
	        } 
            else
            {
                $this->get('session')->getFlashBag()->add('error', "Oops! This Action Owner is already in use.");
            }

            return $this->redirect($this->generateUrl('anomaly_ao', array('projectId' => $projectId)));
        }

        return $this->redirect($this->generateUrl('anomaly_ao_new', array('projectId' => $projectId)));
    }

    /**
     * Creates a form to create a ActionOwners entity.
     *
     * @param ActionOwners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ActionOwners $entity, $projectId)
    {
        $actionSpas = $this->get('aie_anomaly.user.helper')->getProjectSpas($projectId);
        $form = $this->createForm(new ActionOwnersType($actionSpas), $entity, array(
            'action' => $this->generateUrl('anomaly_ao_create', array('projectId' => $projectId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new ActionOwners entity.
     *
     * @Route("/new", name="anomaly_ao_new")
     * @Method("GET")
     * @Template("AIEVeracityBundle:ActionOwners:new.html.twig")
     */
    public function newAction($projectId)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

        $entity = new ActionOwners();
        $form = $this->createCreateForm($entity, $projectId);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'projectId' => $projectId,
            'route_base' => $this->route_base,
            'layout' => $this->layout,
        );
    }

    /**
     * Deletes a ActionOwners entity.
     *
     * @Route("/{id}", name="anomaly_ao_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $projectId)
    {

        if (!$this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS_DELETE',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


        $form = $this->createDeleteForm($id, $projectId);
        $form->handleRequest($request);

        $em = $this->getManager();
        if ($form->isValid()) {
            $entity = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ActionOwners entity.');
            }
            try {
                $em->remove($entity);
                $em->flush();
            } catch (\Exception $e) {
                $this->addFlash('error', 'Cannot delete this action owner as there are LIVE anomalies assigned to this user.');
            }

        }
        return $this->redirect($this->generateUrl('anomaly_ao', array('projectId' => $projectId)));
    }


    /**
     * Creates a form to delete a ActionOwners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $projectId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_ao_delete', array('projectId' => $projectId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => [ 'onClick' => 'return confirm(\'Are you sure you want to delete this item?\');', 'class'=>'btn btn-default']))
            ->getForm();
    }

}
