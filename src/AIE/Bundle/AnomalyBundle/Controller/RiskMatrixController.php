<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 16:15
 */
namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\RiskMatrix;
use AIE\Bundle\VeracityBundle\Form\RiskMatrixType;
use AIE\Bundle\AnomalyBundle\Form\ProjectConsequencesType;
use AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * RiskMatrix controller.
 *
 * @Route("projects/{projectId}/riskmatrix")
 */
class RiskMatrixController extends AnomalyBaseController {

    use Helper\FormStyleHelper;


    /**
     * Creates a form to create a RiskMatrix entity.
     *
     * @param RiskMatrix $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId, $consequences) {
        if ($this->securityHelper->isRoleGranted('ROLE_RISKMATRIX_EDIT',$this->userRolesMergeToCheck))
            $editable = true;
        else
            $editable = false;

        $form = $this->createFormBuilder()
            ->setAction($this->generateURL('anomaly_riskmatrix_create', array('projectId' => $projectId)))
            ->setMethod('POST')
            ->setDisabled(!$editable);

        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $riskmatrix = $project->getRiskMatrix();

        $form->add('riskmatrix', new RiskMatrixType($riskmatrix));

        foreach ($consequences as $consequence) {
            $form->add('consequence_' . $consequence->getId(), new ProjectConsequencesType($consequence));
        }

        $choices = $riskmatrix->getChoices();

        $matrix = json_decode($riskmatrix->getMatrix(), true);

        //$i for consequences
        //$j for liklyhood
        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 5; $j++) {
                $form = $form->add('matrix_' . $i . '_' . $j, 'choice', $this->options(array('choices' => $choices,
                    'data' => array_search($matrix[$i][$j], $choices),
                    'attr' => array('class'=>'risk-select')
                )));
            }
        }

//        $securityContext = $this->container->get('security.authorization_checker');
//        if ($this->securityHelper->isRoleGranted('ROLE_RISKMATRIX_EDIT',$this->userRolesMergeToCheck)){

            $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'));


        return $form->getForm();
    }

    /**
     * Creates a new RiskMatrix entity.
     *
     * @Route("/", name="anomaly_riskmatrix_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:RiskMatrix:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {

        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $securityContext = $this->container->get('security.context');
        //if (!$securityContext->isGranted('ROLE_PROJECT_RISKMATRIX_EDIT', $project))
        //    $this->redirect($this->generateURL('anomaly_riskmatrix_new', array('projectId' => $projectId)));

        $consequences = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findBy(array('project' => $projectId));


        $form = $this->createCreateForm($projectId, $consequences);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();


            /* Risk Matrix */
            $riskmatrix = $project->getRiskMatrix();

            $choices = $riskmatrix->getChoices();

            if ($data['riskmatrix']){
            $riskmatrix->setDesc1($data['riskmatrix']->getDesc1());
            $riskmatrix->setDesc2($data['riskmatrix']->getDesc2());
            $riskmatrix->setDesc3($data['riskmatrix']->getDesc3());
            $riskmatrix->setDesc4($data['riskmatrix']->getDesc4());
            $riskmatrix->setDesc5($data['riskmatrix']->getDesc5());


            $matrix = array();
            for ($i = 0; $i < 5; $i++) {
                $matrix[$i] = array();
                for ($j = 0; $j < 5; $j++) {
                    $matrix[$i][$j] = $choices[$data["matrix_{$i}_{$j}"]];
                }
            }

            $riskmatrix->setMatrix(json_encode($matrix));
            //Set Risk Array
            $em->persist($riskmatrix);
            /* End of Risk Matrix */

            /* Update Consequences */
            $consequences = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findBy(array('project' => $projectId));

            foreach ($consequences as $consequence) {
                $id = $consequence->getId();

                $consequence->setDesc1($data["consequence_{$id}"]->getDesc1());
                $consequence->setDesc2($data["consequence_{$id}"]->getDesc2());
                $consequence->setDesc3($data["consequence_{$id}"]->getDesc3());
                $consequence->setDesc4($data["consequence_{$id}"]->getDesc4());
                $consequence->setDesc5($data["consequence_{$id}"]->getDesc5());

                $em->persist($consequence);
            }
            /* End of Update Consequences */
            }

            $em->flush();
        }

        return $this->redirect($this->generateURL('anomaly_riskmatrix_new', array('projectId' => $projectId)));
    }

    /**
     * Displays a form to create a new RiskMatrix entity.
     *
     * @Route("/new", name="anomaly_riskmatrix_new")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:RiskMatrix:new.html.twig")
     */
    public function newAction($projectId) {

        if (!$this->securityHelper->isRoleGranted('ROLE_RISKMATRIX_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));


        $em = $this->getManager();

        $consequences = $em->getRepository('AIEAnomalyBundle:ProjectConsequences')->findBy(array('project' => $projectId));

        $entity = new RiskMatrix();
        $form = $this->createCreateForm($projectId, $consequences);
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
            'consequences' => $consequences,
            'wizard_title' => 'Risk Matrix'
        );
    }
}
