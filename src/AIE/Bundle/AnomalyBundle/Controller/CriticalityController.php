<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 23:00
 */

namespace AIE\Bundle\AnomalyBundle\Controller;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;

use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality;
use AIE\Bundle\AnomalyBundle\Entity\ProductionCriticality;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Form\CriticalityPreselectType;
use AIE\Bundle\AnomalyBundle\Form\CriticalityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * Criticality controller.
 *
 * @Route("projects/{projectId}/criticality")
 */
class CriticalityController extends AnomalyBaseController {

    use Helper\FormStyleHelper;
    protected $editVisible = 0;

    /**
     * Creates a form to create a RiskMatrix entity.
     *
     * @param CriticalityProject $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId) {
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

        $form = $this->createForm(new CriticalityType($project), $project, array(
            'action' => $this->generateUrl('anomaly_criticality_create', ['projectId' => $projectId]),
            'method' => 'POST',
        ));

        if ($this->securityHelper->isRoleGranted('ROLE_CRITICALITY_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }


        if ($this->editVisible == 1)
        $form->add('submit', 'submit', $this->options(array('label' => 'Save', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Creates a form to select pre select criticality definition.
     *
     * @param CriticalityProject $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPreselectCreateForm($projectId,$editable) {

        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);

        $form = $this->createForm(new CriticalityPreselectType($project), $project, array(
            'action' => $this->generateUrl('anomaly_criticality_preselect_create', ['projectId' => $projectId]),
            'method' => 'POST',
            'disabled' => !$editable ,
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Next', 'attr' => array('class' => 'right')), 'btn'));

        return  $form;
    }

    /**
     * Creates a new CriticalityProject entity.
     *
     * @Route("/", name="anomaly_criticality_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Criticality:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {

        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:CriticalityProject')->find($projectId);
        $securityContext = $this->container->get('security.context');

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateURL('anomaly_criticality_new', array('projectId' => $projectId)));
    }

    /**
     * Creates a Pre Select form for the criticality definition.
     *
     * @Route("/preselect/create", name="anomaly_criticality_preselect_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Criticality:preselect.html.twig")
     */
    public function preselectcreateAction(Request $request, $projectId) {

        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:CriticalityProject')->find($projectId);

        $securityContext = $this->container->get('security.context');
        $data = $request->request->get('form');
        $flag =0;
        $dataArray=['VL','L','M','H','VH'];
        $dataArray['VL']=['selected'=>0,'name'=>null,'color'=>null];
        $dataArray['L']=['selected'=>0,'name'=>null,'color'=>null];
        $dataArray['M']=['selected'=>0,'name'=>null,'color'=>null];
        $dataArray['H']=['selected'=>0,'name'=>null,'color'=>null];
        $dataArray['VH']=['selected'=>0,'name'=>null,'color'=>null];
        $NamesObject=['VH'=>'Very High','H'=>'High','M'=>'Medium','L'=>'Low','VL'=>'Very Low'];
        $ColorObject=['VH'=>'AB0000','H'=>'F72200','M'=>'EDB518','L'=>'68E34B','VL'=>'56AB54'];
        if (isset($request->request->get('aie_bundle_anomalybundle_criticalityproject')['_choices']) && count($request->request->get('aie_bundle_anomalybundle_criticalityproject')['_choices'])) {
            foreach ($request->request->get('aie_bundle_anomalybundle_criticalityproject')['_choices'] as $index => $choice) {
                $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')[strtolower($choice).'Name'];
                $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')[strtolower($choice).'Color'];
                $dataArray[$choice]=['selected'=>1,
                    'name'=>empty($name)?$NamesObject[$choice]:$name,
                    'color'=>empty($color)?$ColorObject[$choice]:$color];
                if (strtolower($choice) === 'vl'){
                    $entity->setVlEnabled($dataArray['VL']['selected']);
                    $entity->setVlName($dataArray['VL']['name']);
                    $entity->setVlColor($dataArray['VL']['color']);
                }
                if (strtolower($choice) === 'l'){
                    $entity->setLEnabled($dataArray['L']['selected']);
                    $entity->setLName($dataArray['L']['name']);
                    $entity->setLColor($dataArray['L']['color']);
                }
                if (strtolower($choice) === 'm'){
                    $entity->setMEnabled($dataArray['M']['selected']);
                    $entity->setMName($dataArray['M']['name']);
                    $entity->setMColor($dataArray['M']['color']);
                }
                if (strtolower($choice) === 'h'){
                    $entity->setHEnabled($dataArray['H']['selected']);
                    $entity->setHName($dataArray['H']['name']);
                    $entity->setHColor($dataArray['H']['color']);
                }
                if (strtolower($choice) === 'vh'){
                    $entity->setVhEnabled($dataArray['VH']['selected']);
                    $entity->setVhName($dataArray['VH']['name']);
                    $entity->setVhColor($dataArray['VH']['color']);
                }
            }
            $entity->setCriticalitySelected(1);

            $em->persist($entity);
            $em->flush();
        }else{
            if ($entity->getCriticalitySelected() == 1){
                if ($entity->getVhEnabled() == 1){
                    $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['vhName'];
                    $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['vhColor'];
                    $dataArray['VH']=['selected'=>1,
                        'name'=>empty($name)?$entity->getVhName():$name,
                        'color'=>empty($color)?$entity->getVhColor():$color];
                    $entity->setVhName($dataArray['VH']['name']);
                    $entity->setVhColor($dataArray['VH']['color']);
                }
                if ($entity->getHEnabled() == 1){
                    $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['hName'];
                    $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['hColor'];
                    $dataArray['H']=['selected'=>1,
                        'name'=>empty($name)?$entity->getHName():$name,
                        'color'=>empty($color)?$entity->getHColor():$color];
                    $entity->setHName($dataArray['H']['name']);
                    $entity->setHColor($dataArray['H']['color']);
                }
                if ($entity->getMEnabled() == 1){
                    $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['mName'];
                    $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['mColor'];
                    $dataArray['M']=['selected'=>1,
                        'name'=>empty($name)?$entity->getMName():$name,
                        'color'=>empty($color)?$entity->getMColor():$color];
                    $entity->setMName($dataArray['M']['name']);
                    $entity->setMColor($dataArray['M']['color']);
                }
                if ($entity->getLEnabled() == 1){
                    $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['lName'];
                    $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['lColor'];
                    $dataArray['L']=['selected'=>1,
                        'name'=>empty($name)?$entity->getLName():$name,
                        'color'=>empty($color)?$entity->getLColor():$color];
                    $entity->setLName($dataArray['L']['name']);
                    $entity->setLColor($dataArray['L']['color']);
                }
                if ($entity->getVlEnabled() == 1){
                    $name=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['vlName'];
                    $color=$request->request->get('aie_bundle_anomalybundle_criticalityproject')['vlColor'];
                    $dataArray['VL']=['selected'=>1,
                        'name'=>empty($name)?$entity->getVlName():$name,
                        'color'=>empty($color)?$entity->getVlColor():$color];
                    $entity->setVlName($dataArray['VL']['name']);
                    $entity->setVlColor($dataArray['VL']['color']);
                }
                $em->persist($entity);
                $em->flush();
            }else{
                $this->addFlash('error', 'Please choose ranks!');
                return $this->redirect($this->generateURL('anomaly_criticality_preselect', array('projectId' => $projectId)));
            }

        }
        return $this->redirect($this->generateURL('anomaly_criticality_new', array('projectId' => $projectId)));
    }

    /**
     * Displays a form to create a new CriticalityProject entity.
     *
     * @Route("/new", name="anomaly_criticality_new")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Criticality:new.html.twig")
     */
    public function newAction($projectId) {
        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity = $project;
        $form = $this->createCreateForm($projectId);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
            'wizard_title' => 'Criticality Definition'
        );
    }

    /**
     * Displays a form to create a new Criticality Preselect entity.
     *
     * @Route("/preselect", name="anomaly_criticality_preselect")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Criticality:preselect.html.twig")
     */
    public function preselectAction($projectId) {

        if (!$this->securityHelper->isRoleGranted('ROLE_CRITICALITY_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));



        //Checking if user has permission to edit
        if ($this->securityHelper->isRoleGranted('ROLE_CRITICALITY_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }


        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entity = $project;
//        if ($entity->getCriticalitySelected() == 1)
//        {
//            return $this->redirect($this->generateURL('anomaly_criticality_new', array('projectId' => $projectId)));
//        }

        $form = $this->createPreselectCreateForm($projectId,$this->editVisible);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
            'wizard_title' => 'Criticality Definition',
            'editGranted' => $this->editVisible,
        );
    }

    /**
     * Displays a form to create a new Criticality Preselect entity.
     *
     * @Route("/fmcriticality", name="anomaly_fmcriticality")
     * @Method({"GET","POST"})
     * @Template("AIEAnomalyBundle:Criticality:fmcriticality.html.twig")
     */
    public function fmcriticalityAction($projectId) {
        if ($this->securityHelper->isRoleGranted('ROLE_FM_CRITICALITY_ADD',$this->userRolesMergeToCheck))
            $addToMe = true;
        else
            $addToMe = false;

        if ($this->securityHelper->isRoleGranted('ROLE_FM_CRITICALITY_EDIT',$this->userRolesMergeToCheck))
            $editable = true;
        else
            $editable = false;


        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if($project instanceof RiskProject || (!$project->hasFMCriticality()))
            return $this->redirect($this->generateURL('anomaly_projects'));
        $entities = $project->getFabricMaintenanceCriticality();

        $form = $this->createFmCriticalityForm($projectId,$editable);

        return array(
            'entities' => $entities,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
            'createGranted'  => $addToMe,
            'editGranted'  => $editable,
        );
    }


    /**
     * Creates a Pre Select form for the criticality definition.
     *
     * @Route("/fmcriticality/edit", name="anomaly_fmcriticality_edit")
     * @Method({"GET","POST"})
     * @Template("AIEAnomalyBundle:Criticality:fmcriticality.html.twig")
     */
    public function fmcriticalityEditAction(Request $request, $projectId) {
        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entities = $project->getFabricMaintenanceCriticality();

        $data = $request->request->get('form');
        $fmData = $data['fmData'];
        $modifications = json_decode($fmData, true);
        if(is_array($modifications) || is_object($modifications)){
            foreach ($modifications as $modification){
                if($modification["isExist"] == 1){
                    $fmItem = $em->getRepository('AIEAnomalyBundle:FabricMaintenanceCriticality')->find($modification["id"]);
                }else{
                    $fmItem = new FabricMaintenanceCriticality();
                    $fmItem->setProject($project);
                }
                $fmItem->setFMCriticalityCode($modification["code"]);
                $fmItem->setFMCriticalityDescription($modification["description"]);
                $fmItem->setFMCriticalityColor($modification["color"]);
                $em->persist($fmItem);
            }
        }
        $em->flush();
        $this->addFlash('success', 'Fabric Maintenance Criticality list has been updated');
        return $this->redirect($this->generateURL('anomaly_fmcriticality', array('projectId' => $projectId)));
    }

    /**
     * Creates a form to select pre select criticality definition.
     *
     * @param CriticalityProject $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFmCriticalityForm($projectId,$editable) {



        $em = $this->getManager();
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_fmcriticality_edit', array('projectId' => $projectId)))
            ->setMethod('POST')
            ->setDisabled(!$editable)
            ->add('submit', 'submit', $this->options(['label' => 'Save Changes', 'attr' => ['class' => 'btn btn-primary btn-block call-loader btnSave']], 'btn'))
            ->add('fmData', 'hidden', array('data' => null, 'attr'=>['class'=>'hdnFMData']))
            ->getForm();
        return $downloadform;
    }

    /**
     * Displays a form to create a new Criticality Preselect entity.
     *
     * @Route("/productioncriticality", name="anomaly_productioncriticality")
     * @Method({"GET","POST"})
     * @Template("AIEAnomalyBundle:Criticality:productioncriticality.html.twig")
     */
    public function productioncriticalityAction($projectId) {
        if ($this->securityHelper->isRoleGranted('ROLE_PRODUCTION_CRITICALITY_ADD',$this->userRolesMergeToCheck))
            $addToMe = true;
        else
            $addToMe = false;

        if ($this->securityHelper->isRoleGranted('ROLE_PRODUCTION_CRITICALITY_EDIT',$this->userRolesMergeToCheck))
            $editable = true;
        else
            $editable = false;


        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if($project instanceof RiskProject || !$project->hasProductionCriticality())
            return $this->redirect($this->generateURL('anomaly_projects'));
        $entities = $project->getProductionCriticality();

        $form = $this->createProductionCriticalityForm($projectId,$editable);

        return array(
            'entities' => $entities,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'project' => $project,
            'createGranted'  => $addToMe,
            'editGranted'  => $editable,
        );
    }

    /**
     * Creates a form to select pre select criticality definition.
     *
     * @param CriticalityProject $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createProductionCriticalityForm($projectId,$editable) {

        $em = $this->getManager();
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_productioncriticality_edit', array('projectId' => $projectId)))
            ->setMethod('POST')
            ->setDisabled(!$editable)
            ->add('submit', 'submit', $this->options(['label' => 'Save Changes', 'attr' => ['class' => 'btn btn-primary btn-block call-loader btnSave']], 'btn'))
            ->add('fmData', 'hidden', array('data' => null, 'attr'=>['class'=>'hdnFMData']))
            ->getForm();
        return $downloadform;
    }

    /**
     * Creates a Pre Select form for the criticality definition.
     *
     * @Route("/productioncriticality/edit", name="anomaly_productioncriticality_edit")
     * @Method({"GET","POST"})
     * @Template("AIEAnomalyBundle:Criticality:productioncriticality.html.twig")
     */
    public function productioncriticalityEditAction(Request $request, $projectId) {
        $em = $this->getManager();

        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $entities = $project->getProductionCriticality();

        $data = $request->request->get('form');
        $fmData = $data['fmData'];
        $modifications = json_decode($fmData, true);
        if(is_array($modifications) || is_object($modifications)){
            foreach ($modifications as $modification){
                if($modification["isExist"] == 1){
                    $fmItem = $em->getRepository('AIEAnomalyBundle:ProductionCriticality')->find($modification["id"]);
                }else{
                    $fmItem = new ProductionCriticality();
                    $fmItem->setProject($project);
                }
                $fmItem->setProductionCriticalityCode($modification["code"]);
                $fmItem->setProductionCriticalityDescription($modification["description"]);
                $fmItem->setProductionCriticalityColor($modification["color"]);
                $em->persist($fmItem);
            }
        }
        $em->flush();
        $this->addFlash('success', 'Production Criticality list has been updated');
        return $this->redirect($this->generateURL('anomaly_productioncriticality', array('projectId' => $projectId)));
    }
}