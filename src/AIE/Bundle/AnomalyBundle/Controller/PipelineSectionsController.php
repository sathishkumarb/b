<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\PipelineSections;
use AIE\Bundle\AnomalyBundle\Form\PipelineSectionsType;

/**
 * PipelineSections controller.
 *
 * @Route("projects/{projectId}/pipelines/{pipelineId}/sections")
 */
class PipelineSectionsController extends AnomalyBaseController
{

    /**
     * Lists all PipelineSections entities.
     *
     * @Route("/", name="anomaly_pipeline_sections")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId,$projectId)
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:PipelineSections')->findByPipeline($pipelineId);
        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return [
            'entities' => $entities,
            'projectId' => $projectId,
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline
        ];
    }

    /**
     * Creates a new PipelineSections entity.
     *
     * @Route("/", name="anomaly_pipeline_sections_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:PipelineSections:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId, $projectId)
    {


        $entity = new PipelineSections();
        $form = $this->createCreateForm($entity, $projectId, $pipelineId, 0);
        $form->handleRequest($request);

        $em = $this->getManager();
        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);

        if ($form->isValid()) {
            $entity->setPipeline($pipeline);
            foreach ($entity->getSegments() as $segment) {
                $segment->setSection($entity);
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_pipeline_sections_new', ['pipelineId' => $pipelineId, 'projectId' => $projectId]));
        }


        $sections = $em->getRepository('AIEAnomalyBundle:PipelineSections')->findByPipeline($pipelineId);

        $entity = new PipelineSections();
        $this->setLeftTitleFromPipeline($pipeline);

        $length = $pipeline->getLength();

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'sections' => $sections,
            'projectId' => $projectId,
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline,
            'length' => $length
        ];
    }

    /**
     * Creates a form to create a PipelineSections entity.
     *
     * @param PipelineSections $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PipelineSections $entity, $projectId, $pipelineId, $length)
    {
        $form = $this->createForm(
            new PipelineSectionsType($entity, $length),
            $entity,
            [
                'action' => $this->generateUrl('anomaly_pipeline_sections_create', ['pipelineId' => $pipelineId, 'projectId' => $projectId]),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new PipelineSections entity.
     *
     * @Route("/new", name="anomaly_pipeline_sections_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId,$pipelineId)
    {
        $em = $this->getManager();

        $sections = $em->getRepository('AIEAnomalyBundle:PipelineSections')->findByPipeline($pipelineId);


        $entity = new PipelineSections();
        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $length = $pipeline->getLength();

        $form = $this->createCreateForm($entity, $projectId, $pipelineId, $length);


        return [
            'sections' => $sections,
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId,
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline,
            'length' => $length
        ];
    }

    /**
     * Finds and displays a PipelineSections entity.
     *
     * @Route("/{id}", name="anomaly_pipeline_sections_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $pipelineId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:PipelineSections')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        //$deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'projectId' => $projectId,
            //  'delete_form' => $deleteForm->createView(),
            'pipelineId' => $pipelineId
        ];
    }

    /**
     * Displays a form to edit an existing PipelineSections entity.
     *
     * @Route("/{id}/edit", name="anomaly_pipeline_sections_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $pipelineId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:PipelineSections')->find($id);
        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $length = $pipeline->getLength();

        $editForm = $this->createEditForm($entity, $projectId, $pipelineId, $length);

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'projectId' => $projectId,
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline,
            'length' => $length
        ];
    }

    /**
     * Creates a form to edit a PipelineSections entity.
     *
     * @param PipelineSections $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PipelineSections $entity, $projectId, $pipelineId, $length)
    {
        $form = $this->createForm(
            new PipelineSectionsType($entity, $length),
            $entity,
            [
                'action' => $this->generateUrl(
                    'anomaly_pipeline_sections_update',
                    ['id' => $entity->getId(), 'pipelineId' => $pipelineId, 'projectId' => $projectId]
                ),
                'method' => 'PUT',
            ]
        );

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Edits an existing PipelineSections entity.
     *
     * @Route("/{id}", name="anomaly_pipeline_sections_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:PipelineSections:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $pipelineId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:PipelineSections')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId, $pipelineId, 0);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($entity->getSegments() as $segment) {
                $segment->setSection($entity);
            }
            $em->flush();
        }

        return $this->redirect(
            $this->generateUrl('anomaly_pipeline_sections', ['id' => $id, 'pipelineId' => $pipelineId, 'projectId' => $projectId])
        );
    }

    /**
     * Displays a form to edit an existing PipelineSections entity.
     *
     * @Route("/{id}/delete", name="anomaly_pipeline_sections_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction($projectId, $pipelineId, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:PipelineSections')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $deleteForm = $this->createDeleteForm($entity->getId(), $pipelineId, $projectId);

        return [
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'projectId' => $projectId,
            'pipelineId' => $pipelineId
        ];
    }

    /**
     * Deletes a PipelineSections entity.
     *
     * @Route("/{id}", name="anomaly_pipeline_sections_post_delete")
     * @Method("DELETE")
     */
    public function postDeleteAction(Request $request, $id, $pipelineId, $projectId)
    {
        $form = $this->createDeleteForm($id, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:PipelineSections')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PipelineSections entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect(
            $this->generateUrl('anomaly_pipeline_sections', ['id' => $id, 'pipelineId' => $pipelineId, 'projectId' => $projectId])
        );
    }

    /**
     * Creates a form to delete a PipelineSections entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId, $projectId)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl('anomaly_pipeline_sections_post_delete', ['id' => $id, 'pipelineId' => $pipelineId, 'projectId' => $projectId])
            )
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                $this->options(['label' => 'Delete', 'attr' => ['class' => 'btn-danger right hide']], 'btn')
            )
            ->getForm();
    }

}
