<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Threat;
use AIE\Bundle\AnomalyBundle\Form\ThreatType;

/**
 * Threat controller.
 *
 * @Route("/threats")
 */
class ThreatController extends AnomalyBaseController
{

    /**
     * Lists all Threat entities.
     *
     * @Route("/", name="anomaly_threats")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $entities = $em->getRepository('AIEAnomalyBundle:Threat')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Threat entity.
     *
     * @Route("/", name="anomaly_threats_create")
     * @Method("POST")
     * @Template("AIEAnomalyBundle:Threat:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Threat();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_threats_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Threat entity.
     *
     * @param Threat $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Threat $entity)
    {
        $form = $this->createForm(new ThreatType(), $entity, array(
            'action' => $this->generateUrl('anomaly_threats_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Threat entity.
     *
     * @Route("/new", name="anomaly_threats_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Threat();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Threat entity.
     *
     * @Route("/{id}", name="anomaly_threats_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Threat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Threat entity.
     *
     * @Route("/{id}/edit", name="anomaly_threats_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Threat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threat entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Threat entity.
    *
    * @param Threat $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Threat $entity)
    {
        $form = $this->createForm(new ThreatType(), $entity, array(
            'action' => $this->generateUrl('anomaly_threats_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(['label' => 'Update', 'attr' => ['class' => 'right']], 'btn'));

        return $form;
    }
    /**
     * Edits an existing Threat entity.
     *
     * @Route("/{id}", name="anomaly_threats_update")
     * @Method("PUT")
     * @Template("AIEAnomalyBundle:Threat:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getManager();

        $entity = $em->getRepository('AIEAnomalyBundle:Threat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('anomaly_threats_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Threat entity.
     *
     * @Route("/{id}", name="anomaly_threats_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getManager();
            $entity = $em->getRepository('AIEAnomalyBundle:Threat')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Threat entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('anomaly_threats'));
    }

    /**
     * Creates a form to delete a Threat entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anomaly_threats_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm()
        ;
    }
}
