<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Pipelines;
use AIE\Bundle\AnomalyBundle\Form\PipelinesType;

/**
 * Pipelines controller.
 *
 * @Route("projects/{projectId}/pipelines")
 */
class PipelinesController extends AnomalyBaseController {

    protected $editVisible = 0;
    protected $createVisible = 0;
    protected $showVisible = 0;
    protected $deleteVisible = 0;
    protected $sectionVisible = 0;

    /**
	 * Lists all Pipelines entities.
	 *
	 * @Route("/", name="anomaly_pipelines")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction($projectId) {

        //Checking if user has permission to edit, add, delete, show, section
        if ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_EDIT', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->editVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ADD', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->createVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_SHOW', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->showVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_DELETE', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->deleteVisible = 1;
        }
        if ($this->securityHelper->isRoleGranted('ROLE_PIPELINE_SECTION', $this->userRolesMergeToCheck) || $this->assignedAsAdmin) {
            $this->sectionVisible = 1;
        }


		$em = $this->getManager();

		$entities = $em->getRepository('AIEAnomalyBundle:Pipelines')->findByProject($projectId);

		return [
			'entities'  => $entities,
			'projectId' => $projectId,
            'editGranted' => $this->editVisible,
            'createGranted' => $this->createVisible,
            'showGranted' => $this->showVisible,
            'deleteGranted' => $this->deleteVisible,
            'sectionGranted' => $this->sectionVisible,
		];
	}

	/**
	 * Creates a new Pipelines entity.
	 *
	 * @Route("/", name="anomaly_pipelines_create")
	 * @Method("POST")
	 * @Template("AIEAnomalyBundle:Pipelines:new.html.twig")
	 */
	public function createAction($projectId, Request $request) {

        if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_ADD',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

		$entity = new Pipelines();
		$form = $this->createCreateForm($projectId, $entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getManager();
			$entity->setProject($em->getRepository('AIEAnomalyBundle:Projects')->find($projectId));
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('anomaly_pipelines', ['projectId' => $projectId]));
		}

		return [
			'entity'    => $entity,
			'projectId' => $projectId,
			'form'      => $form->createView(),
		];
	}

	/**
	 * Creates a form to create a Pipelines entity.
	 *
	 * @param Pipelines $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm($projectId, Pipelines $entity) {
		$form = $this->createForm(new PipelinesType(), $entity, [
			'action' => $this->generateUrl('anomaly_pipelines_create', ['projectId' => $projectId]),
			'method' => 'POST',
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Create', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}

	/**
	 * Displays a form to create a new Pipelines entity.
	 *
	 * @Route("/new", name="anomaly_pipelines_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction($projectId) {
		$entity = new Pipelines();
		$form = $this->createCreateForm($projectId, $entity);

		return [
			'entity'    => $entity,
			'form'      => $form->createView(),
			'projectId' => $projectId,
		];
	}

	/**
	 * Finds and displays a Pipelines entity.
	 *
	 * @Route("/{id}", name="anomaly_pipelines_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($projectId, $id) {

        if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_SHOW',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }


		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Pipelines entity.');
		}
		$request = $this->getRequest();
        $delFlag = $request->get('d'); 

        if ( $delFlag == 'Y' )
        {
            if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_DELETE',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
                return $this->redirectToRoute('anomalyAccessDeniedException');
        }

            $deleteForm = $this->createDeleteForm($projectId, $id);
			return [
				'entity'      => $entity,
				'delete_form' => $deleteForm->createView(),
				'projectId' => $projectId,
                'deleteGranted' => $deleteVisible,
			];
		}
		else
		{
			return [
				'entity'      => $entity,
				'projectId' => $projectId,
			];
		}
	}

	/**
	 * Displays a form to edit an existing Pipelines entity.
	 *
	 * @Route("/{id}/edit", name="anomaly_pipelines_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($projectId, $id) {
        if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_EDIT',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Pipelines entity.');
		}

		$editForm = $this->createEditForm($projectId, $entity);
		$deleteForm = $this->createDeleteForm($projectId, $id);

		return [
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
			'projectId' => $projectId,
		];
	}

	/**
	 * Creates a form to edit a Pipelines entity.
	 *
	 * @param Pipelines $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm($projectId, Pipelines $entity) {
		$form = $this->createForm(new PipelinesType(), $entity, [
			'action' => $this->generateUrl('anomaly_pipelines_update', ['projectId' => $projectId, 'id' => $entity->getId()]),
			'method' => 'PUT',
		]);

		$form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

		return $form;
	}

	/**
	 * Edits an existing Pipelines entity.
	 *
	 * @Route("/{id}", name="anomaly_pipelines_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:Pipelines:edit.html.twig")
	 */
	public function updateAction($projectId, Request $request, $id) {
		$em = $this->getManager();

		$entity = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Pipelines entity.');
		}

		$deleteForm = $this->createDeleteForm($projectId, $id);
		$editForm = $this->createEditForm($projectId, $entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em->flush();

			return $this->redirect($this->generateUrl('anomaly_pipelines', ['projectId' => $projectId]));
		}

		return [
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
			'projectId' => $projectId,
		];
	}

	/**
	 * Deletes a Pipelines entity.
	 *
	 * @Route("/{id}", name="anomaly_pipelines_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction($projectId, Request $request, $id) {

        if (!$this->securityHelper->isRoleGranted('ROLE_PIPELINE_DELETE',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin ) {
            return $this->redirectToRoute('anomalyAccessDeniedException');
        }

		$form = $this->createDeleteForm($projectId, $id);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getManager();
			$entity = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($id);

			if (! $entity) {
				throw $this->createNotFoundException('Unable to find Pipelines entity.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('anomaly_pipelines', ['projectId' => $projectId]));
	}

	/**
	 * Creates a form to delete a Pipelines entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($projectId, $id) {
		return $this->createFormBuilder()
			->setAction($this->generateUrl('anomaly_pipelines_delete', ['projectId' => $projectId, 'id' => $id]))
			->setMethod('DELETE')
			->add(
                'submit',
                'submit',
                $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn')
            )
			->getForm();
	}
}
