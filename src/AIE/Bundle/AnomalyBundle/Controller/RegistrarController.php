<?php
/**
 * User: mokha
 * Date: 2/29/16
 * Time: 11:04 AM
 */

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Form\RegistrarSearchType;

class RegistrarController extends AnomalyBaseController {

    protected $registrarClass;
    protected $registrarClassName;
    protected $registrarRouteBase;
    protected $registrarType;

    /**
     * Lists anomaly requests history by route type.
     *
     * @Template()
     */
    public function historyAction($id)
    {
        $em = $this->getManager();

        //get previous requests
        $requests = $em->getRepository('AIEAnomalyBundle:Request')->findByRegistrar($id);
        $userIds = [];
        $users = [];
        $requestData = [];
        $approvedByUsers = [];
        $deferredByUsers = [];

        /**
         * Get the users
         */
        foreach ($requests as $requestId => $request) {

            $registrarInfo = $em->getRepository('AIEAnomalyBundle:Registrar')->find($request->getRegistrar());

            $uid = null;
            if (  $request->getRequestStatus() == 'deferral' && $request->getDeferredBy())
            {
                $deferralOwner =   $em->getRepository('AIEAnomalyBundle:Deferrals')->findById($request->getDeferredBy()->getId());

                if($deferralOwner)
                {
                    $uid = $request->getDeferredBy()->getActionOwner()->getId();
                    $deferredByUsers[$request->getId()] = $uid;
                }
            }
            elseif($request->getAnsweredBy())
            {
                $approvalOwner =   $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findById($request->getAnsweredBy()->getId());

                if($approvalOwner)
                {
                    $uid = $request->getAnsweredBy()->getActionOwner()->getId();
                    $approvedByUsers[$request->getId()] = $uid;
                }


                //$approvedByUsers[] = $uid;
                /*if ( $registrarInfo->getApprovedspa() && !empty($registrarInfo->getApprovedspa()) )
                {
                    $approvalOwner = $em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($registrarInfo->getApprovedspa()->getId());
                }
                else
                {
                    $approvalOwner = null;
                }*/
            }
            //if
            /*if ($approvalOwner)
            {
                $uid = ($approvalOwner->getActionOwner()->getId()) ? $approvalOwner->getActionOwner()->getId() : null;
            }
            else
            {
                $approvalOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->find(
                    $registrarInfo->getSpa()->getId()
                );
                $uid = ($approvalOwner->getUserId()) ? $approvalOwner->getUserId() : null;
            }*/



            if ($uid && !isset($userIds [$uid])) {
                $userIds [$uid] = $uid;
            }

            $uid = $request->getRequestedBy()->getUserId();
            if ($uid && !isset($userIds [$uid])) {
                $userIds [$uid] = $uid;
            }


            $serializer = $this->get('jms_serializer');
            $anomalyHelper = $this->get('aie_anomaly.anomaly.helper');

            $requestData[$request->getId()] = $anomalyHelper->formatAnomaly(
                $serializer->deserialize($request->getData(), $request->getType(), 'json'),
                json_decode($request->getData(), true)
            );

        }

        $users = $this->get('aie_users')->findUsersById($userIds, true);

        return [
            'history' => $requests,
            'users' => $users,
            'requestData' => $requestData,
            'approvedByUsers' => $approvedByUsers,
            'deferredByUsers' => $deferredByUsers,
        ];

    }
}