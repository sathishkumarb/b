<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Form\FabricMaintenanceRegistrarType;

/**
 * FabricMaintenanceRegistrar controller.
 *
 * @Route("projects/{projectId}/fm/{type}/", defaults={"type"=null}, requirements={"type"="(pl|pi|se|st|mr)?"})
 */
class FabricMaintenanceRegistrarController extends ActionRegistrarController {

	protected $registrarClassName = 'FabricMaintenanceRegistrar';
	protected $registrarClass = 'AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar';
	protected $registrarRouteBase = 'anomaly_fm';
	protected $registrarType = 'AIE\Bundle\AnomalyBundle\Form\FabricMaintenanceRegistrarType';

    /**
     * Show the request history of an anomaly
     *
     * @Route("{id}/history", name="fm_registrar_request_history")
     * @Method("GET")
     * @Template("AIEAnomalyBundle:Registrar:history.html.twig")
     */
    public function historyAction($id)
    {
        return parent::historyAction($id);
    }

	/**
	 * Lists all entities.
	 *
	 * @Route("", name="anomaly_fm")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function indexAction(Request $request, $projectId, $type = null) {
        return parent::indexAction($request, $projectId, $type);
    }

	/**
	 * Lists all entities.
	 *
	 * @Route("master", name="anomaly_fm_master")
	 * @Method({"GET", "POST"})
	 * @Template()
	 */
	public function masterAction(Request $request, $projectId, $type = null) {
		$this->registrarRouteBase = 'anomaly_fm_master';
		return parent::masterAction($request, $projectId, $type);
	}

	/**
	 * Finds and displays a entity.
	 *
	 * @Route("show/{id}", name="anomaly_fm_show", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($id, $projectId) {
		return parent::showAction($id, $projectId);
	}


	/**
	 * Finds and displays a entity.
	 *
	 * @Route("showdeferral/{id}", name="anomaly_fm_show_deferral", requirements={"id": "\d+"})
	 * @Method("GET")
	 * @Template()
	 */
	public function showDeferralAction($id, $projectId) {
		return parent::showDeferralAction($id, $projectId);
	}

	/**
	 * Displays a form to edit an existing entity.
	 *
	 * @Route("edit/{id}", name="anomaly_fm_edit")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction($id, $projectId, $type=null) {
		return parent::editAction($id, $projectId, $type);
	}


	/**
	 * Edits an existing entity.
	 *
	 * @Route("update/{id}", name="anomaly_fm_update")
	 * @Method("PUT")
	 * @Template("AIEAnomalyBundle:FabricMaintenanceRegistrar:edit.html.twig")
	 */
	public function updateAction(Request $request, $projectId, $id, $type=null) {
		return parent::updateAction($request, $projectId, $id, $type);
	}

	protected function setEntityFields($entity, $data) {
		parent::setEntityFields($entity, $data);
	}
}
