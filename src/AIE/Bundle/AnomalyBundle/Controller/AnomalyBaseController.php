<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Security\AuthenticatedController;
use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AIE\Bundle\AnomalyBundle\Security;
use Symfony\Component\HttpFoundation\File\File;
use AIE\Bundle\VeracityBundle\Controller\BaseController as VeracityBaseController;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class AnomalyBaseController extends VeracityBaseController
{

    protected $connection = 'anomaly';
    protected $folderDirBase = 'anomalies';
    protected $registrarClassName;

    protected $anomalyUserGroupRoles;
    protected $userRolesMergeToCheck;
    protected $assignedAsAdmin; //module level admin
    protected $securityHelper;

    protected $userRoles;
    protected $currentProject;

    public function __construct() {

        global $kernel;
        $this->currentProject = 0;
        $securityContext = $kernel->getContainer()->get('security.authorization_checker');

        /* Set Roles based on anomaly project groups user associated */
        $token = $kernel->getContainer()->get('security.context')->getToken();

        $loggedInUser = $token->getUser();


        //logic to extract the currently accessed URL using the $_SERVER global variable
        $currentUrl =$_SERVER['REQUEST_URI'];
        if (strpos($currentUrl,'/projects/') !==false ) {
            $currentUrl = str_replace("/a/projects/","",$currentUrl);
            $this->currentProject = substr($currentUrl, 0,strpos($currentUrl, '/')?strpos($currentUrl, '/'):4);

        }
        elseif (strpos($currentUrl,'/project/') !==false ) { // This case is for Action Owners as the route has "project"
            $currentUrl = str_replace("/a/project/","",$currentUrl);
            $this->currentProject = substr($currentUrl, 0,strpos($currentUrl, '/')?strpos($currentUrl, '/'):4);

        }


        /* Set Roles based on anomaly project groups user associated  */
        $this->anomalyUserGroupRoles = [];

        //logic to determine if user is module level admin with "Assign as Admin" link
        $this->assignedAsAdmin = $kernel->getContainer()->get('aie_anomaly.user.helper')->getIsAdmin($loggedInUser->getId());


        if ($this->currentProject == 0  || !is_numeric($this->currentProject) ) { // for screens where the URL does not contain 'projects', bring the array based on the roles assigned to the user id
            $this->anomalyUserGroupRoles = $kernel->getContainer()->get('aie_anomaly.user.helper')->getAnomalyGroupRoles($loggedInUser->getId());

        }
        else {
            // for screens where the URL contains 'projects', bring the array based on the roles assigned to the user id for the current project
            $this->anomalyUserGroupRoles = $kernel->getContainer()->get('aie_anomaly.user.helper')->getAnomalyGroupRoles($loggedInUser->getId(),$this->currentProject);
        }

        $this->userRolesMergeToCheck = [];

        if ($this->anomalyUserGroupRoles && count($this->anomalyUserGroupRoles) > 1){
            foreach($this->anomalyUserGroupRoles as $roles){
                $this->userRolesMergeToCheck = array_merge( $this->userRolesMergeToCheck, unserialize($roles->getRoles()) );
            }
        }
        else{
            if($this->anomalyUserGroupRoles)
                $this->userRolesMergeToCheck = (count($this->anomalyUserGroupRoles) ? unserialize($this->anomalyUserGroupRoles[0]->getRoles()) : []);
        }

        $this->securityHelper = $kernel->getContainer()->get('aie_anomaly.user.helper');

        /* End Roles set for anomaly groups*/
        if (!$this->userRolesMergeToCheck && !$this->assignedAsAdmin) {
//            $this->getADException();
            throw $this->createAccessDeniedException('You do not have permission to view this resource.');
        }

    }

    /**
     * @param $em
     * @return array
     */
    public function getGrantedProjects() {
        $em = $this->getManager();
        $projects = [];
        if ($this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN',$this->userRolesMergeToCheck)){
            $projects = $em->getRepository('AIEAnomalyBundle:Projects')->findBy(array(), array('name' => 'ASC'));
        }else{
            $usr = $this->get('security.context')->getToken()->getUser();
            $projectIds=[];
            $userGroups = $em->getRepository('AIEAnomalyBundle:UserProjectGroup')->findBy(['user'=>$usr->getId()]);
            foreach ($userGroups as $ug){
                if (!in_array($ug->getProject()->getId(),$projectIds)){
                    array_push($projectIds,$ug->getProject()->getId());
                }
            }
            $projects = $em->getRepository('AIEAnomalyBundle:Projects')->findById($projectIds);
        }

        return $projects;
    }


    public function getADException(){
//        return $this->redirect('anomalyAccessDeniedException');
        return $this->redirectToRoute('anomalyAccessDeniedException');
    }


}