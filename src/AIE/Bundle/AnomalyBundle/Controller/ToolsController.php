<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\DesignCode;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Form\CompareTestPointType;
use AIE\Bundle\AnomalyBundle\Form\MasterLoadSheetType;
use AIE\Bundle\AnomalyBundle\Helper\LoadsheetHelper;
use AIE\Bundle\AnomalyBundle\Entity\PipingAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\StructureAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\StaticEquipmentAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\MarineAnomaly;

/**
 * Tools controller.
 *
 * @Route("projects/{projectId}/tools")
 */
class ToolsController extends AnomalyBaseController {


    /**
     * Loadsheet upload/download.
     *
     * @Route("/loadsheet", name="anomaly_loadsheet")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function loadsheetAction(Request $request, $projectId){
        if (!$this->securityHelper->isRoleGranted('ROLE_TOOLS_LOADSHEET',$this->userRolesMergeToCheck )  && !$this->assignedAsAdmin)
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to show this resource."));

        return $this->uploadLoadSheetHandler($request, $projectId, $request->get('_route'));
    }

    /**
     * compare testPoints.
     *
     * @Route("/comparetp", name="anomaly_compare_tp")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function comparetpAction(Request $request, $projectId) {
        return $this->comparetpHandler($request, $projectId, $request->get('_route'));
    }

    /**
     * download loasheet.
     *
     * @Route("/downloadloadsheet", name="download_loadsheet")
     * @Method("GET")
     * @Template()
     */
    public function downloadloadsheetAction($projectId) {
        $excelObj = $this->get('phpexcel')->createPHPExcelObject();
        $lsHelper = new LoadsheetHelper($excelObj, $this->getManager(),$this->getDoctrine()->getManager() , $projectId);
        $excelObj = $lsHelper->downloadExcelLoadsheet();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="loadsheet_' . $projectId .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = $this->get('phpexcel')->createWriter($excelObj, 'Excel2007');
        $writer->save('php://output');
        exit;
    }

    public function uploadLoadSheetHandler(Request $request, $projectId, $submitForm) {
        $isReadFile =0;
        $dataList=[];
        $excelHeader=null;
        $isValidLoadSheetFormat=true;
        $fileInfo=['name'=>'','ext'=>'' ,'size'=>'','rowsCount'=>0,'status'=>0];
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (! $project) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }
        $form = $this->createMasterLoadSheetForm($projectId, $project, $submitForm);
        $form->handleRequest($request);
        $downloadform = $this->createDownloadForm($projectId);
        $insertform = $this->createInsertLoadSheetForm($projectId, $submitForm, null, null,null, null);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $file = $data['loadsheetfile'];
                if ($file->getMimeType() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    $this->addFlash('warning', 'File format must be xlsx (Excel 2007 or later)');
                    return $this->redirect(
                        $this->generateUrl('anomaly_loadsheet', ['projectId' => $projectId])
                    );
                }else{
                    $fileInfo['name']=$file->getClientOriginalName();
                    $fileInfo['ext']=$file->guessExtension();
                    $fileInfo['size']=$this->formatSizeUnits($file->getClientSize());
                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file);
                    $lsHelper = new LoadsheetHelper($phpExcelObject, $em,$this->getDoctrine()->getManager(), $projectId);
                    if ($lsHelper->isValidLoadSheet()){
                        $excelHeader = $lsHelper->getHeader();
                        $dataList= $lsHelper->readLoadSheetDataRows($lsHelper->getDataSheetHeaderRowsCount() + 1, $lsHelper->getDataSheetHeaderColumnsCount());
                        $fileInfo['rowsCount']=count($dataList['data']);
                        $fileInfo['status']=$dataList['dataStatus'];
                        $validationArray=$lsHelper->getValidationArray();
                        $insertform = $this->createInsertLoadSheetForm($projectId, $submitForm, $dataList, $validationArray,  $excelHeader, $fileInfo);
                    }else{
                        $isValidLoadSheetFormat=false;
                    }
                    $isReadFile=1;
                }
            } catch (\Exception $e) {
                $this->addFlash('error', $e);
            }
        }
        $isSetup = $project->getIsSetup();
        $insertform->handleRequest($request);
        if ($insertform->isSubmitted() && $insertform->isValid()) {
            $data = $insertform->getData();
            $fileInfo = json_decode($data['fileInfo'],true);
            $excelHeader= json_decode($data['headersObject'],true);
            $dataList= json_decode($data['dataObject'],true);
            $validationData= json_decode($data['validationObject'],true);
            $isValidLoadSheetFormat=true;
            $isReadFile=1;
            $dataList = $this->InsertAnomaly($dataList,$validationData,$project,$em,$this->getDoctrine()->getManager());
            $fileInfo['status'] = $dataList['dataStatus'];
//            $isSetup = true;
            if($dataList['dataStatus'] == 3)
                $this->addFlash('error', 'The loadsheet has not been submitted');
            else
                $this->addFlash('success', 'The loadsheet has been submitted and requires approval');
        }


        return [
            'fileInfo'=>$fileInfo,
            'excelHeaderData'=>$excelHeader,
            'excelData'=>$dataList,
            'hasFile'=>$isReadFile,
            'validExcelFormat'=>$isValidLoadSheetFormat,
            'project' =>$project,
            'form'=>$form->createView(),
            'download_form'=>$downloadform->createView(),
            'insert_form'=>$insertform->createView(),
            'isSetup'=>$isSetup,
        ];
    }

    public function comparetpHandler(Request $request, $projectId, $submitForm) {
        $entities=null;
        $finalEntities=null;
        $isSearch = 0;
        $location='';
        $em = $this->getManager();
        $project = $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        if (! $project) {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }
        $form = $this->createTPSearchForm($projectId, $project, $submitForm);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $filter_data = $data;

            $TP = ($filter_data['locationPoint']);
            $loc = ($filter_data['location']);
            $entities = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->findBy(
                ['locationPoint' => $TP,'project'=>$project,'location'=>$loc,'status'=>0]
            );
            $finalEntities=[];
            foreach ($entities as $entity){
                if ($entity->getClassification()->getCode()=='EML' || $entity->getClassification()->getCode()=='IML'){
                    array_push($finalEntities,$entity);
                }
            }
            $isSearch = 1;
            $location=$loc->getName();
        }
        return [
            'project' =>$project,
            'entities'=>$finalEntities,
            'isSearch'=>$isSearch,
            'locationName'=>$location,
            'form'=>$form->createView(),

        ];
    }

    private function createMasterLoadSheetForm($projectId, $project, $submitForm) {
        $requestParameters = ['projectId' => $projectId,];

        $form = $this->createForm(new MasterLoadSheetType($project),null, [
            'action' => $this->generateUrl($submitForm,
                $requestParameters),
            'method' => 'POST',
        ]);
        $form = $form
            ->add('submit', 'submit', $this->options(['label' => 'Upload', 'attr' => ['class' => 'btn btn-primary btn-block call-loader']], 'btn'));
        return $form;
    }

    private function createTPSearchForm($projectId, $project, $submitForm) {
        $requestParameters = ['projectId' => $projectId,];

        $form = $this->createForm(new CompareTestPointType($project),null, [
            'action' => $this->generateUrl($submitForm,
                $requestParameters),
            'method' => 'POST',
        ]);
        $form = $form
            ->add('submit', 'submit', $this->options(['label' => 'Search', 'attr' => ['class' => 'btn btn-primary btn-block']], 'btn'));
        return $form;
    }

    /**
     * @param $projectId
     * @param $submitForm
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInsertLoadSheetForm($projectId, $submitForm, $dataObject, $validationObject, $headersObject, $fileInfo){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl($submitForm, array('projectId' => $projectId)))
            ->setMethod('POST')
            ->add('insert', 'submit', $this->options(['label' => 'Submit Data Master Sheet', 'attr' => ['class' => 'btn btn-success btn-block call-loader']], 'btn'))
            ->add('dataObject', 'hidden', array('data' => json_encode($dataObject)))
            ->add('validationObject', 'hidden', array('data' => json_encode($validationObject)))
            ->add('headersObject', 'hidden', array('data' => json_encode($headersObject)))
            ->add('fileInfo', 'hidden', array('data' => json_encode($fileInfo)))
            ->getForm();
        return $downloadform;

    }

    /**
     * @param $projectId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createDownloadForm($projectId){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('download_loadsheet', array('projectId' => $projectId)))
            ->setMethod('GET')
            ->add('download', 'submit', $this->options(['label' => 'Download Master Sheet', 'attr' => ['class' => 'btn btn-success btn-block']], 'btn'))
            ->getForm();
        return $downloadform;

    }

    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
    public function InsertAnomaly($items, $validationObj, $project, $em, $defaultEm){
        $hasFailure=false;
        $requestsManager = $this->get('anomaly_manager.requests');
        $currentDate = new \DateTime();
        $batchId = $currentDate->format('YmdHis') . str_pad(rand(0, pow(10, 4)-1), 4, '0', STR_PAD_LEFT);;
        $designCodes = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->findBy(['project'=>$project->getId()]);
        $finalCodes=['Pipeline'=>[],'Piping'=>[],'Static Equipment'=>[],'Structure'=>[],'Marine'=>[]];
        foreach ($designCodes as $code){
            if($code->getType()=="PL")
                array_push($finalCodes['Pipeline'],['id'=>$code->getId(),'name'=>$code->getCode()]);
            if($code->getType()=="PI")
                array_push($finalCodes['Piping'],['id'=>$code->getId(),'name'=>$code->getCode()]);
            if($code->getType()=="SE")
                array_push($finalCodes['Static Equipment'],['id'=>$code->getId(),'name'=>$code->getCode()]);
            if($code->getType()=="ST")
                array_push($finalCodes['Structure'],['id'=>$code->getId(),'name'=>$code->getCode()]);
            if($code->getType()=="MR")
                array_push($finalCodes['Marine'],['id'=>$code->getId(),'name'=>$code->getCode()]);
        }
        $locationsObj = $em->getRepository('AIEAnomalyBundle:Locations')->findByProject($project->getId());
        $classificationsObj = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->findByProject($project->getId());
        $threatsObj = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->findByProject($project->getId());
        $threats=[];
        foreach ($threatsObj as $obj){
            array_push($threats,['id'=>$obj->getId(), 'name'=>$obj->getDescription()]);
        }
        $classifications=[];
        foreach ($classificationsObj as $obj){
            array_push($classifications,['id'=>$obj->getId(), 'name'=>$obj->getCode()]);
        }
        $locations=[];
        foreach ($locationsObj as $obj){
            array_push($locations,['id'=>$obj->getId(), 'name'=>$obj->getName()]);
        }
        if ($project instanceof RiskProject){
            $risks = AnomalyRegistrar::getRiskChoices();
        }else{
            $risks = $project->getEnabledCriticalityChoices();
        }
        $fmRisks = $project->getFMCriticalityArray();
        $prodCriticality = $project->getProductionCriticalityArray();
        $users=[];
        $actionOwners = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($project->getId());
        foreach ($actionOwners as $ao){
            $actionOwnerInfo = $defaultEm->getRepository('UserBundle:User')->find($ao->getUserId());
            array_push($users, ['id'=>$actionOwnerInfo->getId(),'username'=>$actionOwnerInfo->getUsername()]);
        }
        $dataIndex=0;
        if (count($risks)==0 || count($locations)==0 || count($classifications)==0 ||
            count($threats)==0 || count($users)==0){
            $hasFailure=true;
            $dataIndex=0;
            foreach($items['data'] as $data){
                $items['data'][$dataIndex]['itemStatus']=3;
                $dataIndex++;
            }
        }else{
            foreach($items['data'] as $data){
                $values=[];
                $index=0;
                foreach ($data['item'] as $item){
                    $anomaly = new AnomalyRegistrar();
                    $anomaly->setProject($project);
                    if($validationObj[$index]['type']=='date' && !empty($item['value'])) {
                        $val=\DateTime::createFromFormat("Y-m-d",$item['value']);
                        if ($val instanceof \DateTime) {
                            $values[$validationObj[$index]['name']]=$val;
                        }else{
                            $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);
                        }
                    }else{
                        $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);
                    }
                    $index++;
                }
                //try {
                $statusChoices = [0=>'Live', 1=>'Closed', 2=>'Cancelled'];
                $actionOwnerId=$this->getArrayItemByKey($values['actionOwner'],$users,'username','id');
                $threatId=$this->getArrayItemByKey($values['anomalyThreat'],$threats,'name','id');
                $classificationId=$this->getArrayItemByKey($values['anomalyClass'],$classifications,'name','id');
                $locationId=$this->getArrayItemByKey($values['location'],$locations,'name','id');
                $designCodeId=$this->getArrayItemByKey($values['designCode'],$finalCodes,'name','id',$values['assetCategory']);
                $statusId=$this->getArrayItemByKey($values['status'],$statusChoices,null,null,null,true);
                if(!is_null($actionOwnerId) && !is_null($threatId) && !is_null($classificationId) && !is_null($locationId) && !is_null($designCodeId) && !is_null($statusId)){
                    $categoryCode=null;
                    if ($values['assetCategory']=="Piping"){
                        $entity=new PipingAnomaly();
                        $categoryCode="PI";
                    }elseif ($values['assetCategory']=="Pipeline"){
                        $entity=new PipelineAnomaly();
                        $categoryCode="PL";
                    }elseif ($values['assetCategory']=="Static Equipment"){
                        $entity=new StaticEquipmentAnomaly();
                        $categoryCode="SE";
                    }elseif ($values['assetCategory']=="Structure"){
                        $entity=new StructureAnomaly();
                        $categoryCode="ST";
                    }elseif ($values['assetCategory']=="Marine"){
                        $entity=new MarineAnomaly();
                        $categoryCode="MR";
                    }else{
                        $entity=null;
                        $categoryCode=null;
                    }
                    $entity->setProject($project);
                    $entity->setAssetTagNumber($values['assetTagNumber']);
                    $entity->setComponent($values['component']);
                    $entity->setComponentDescription($values['componentDescription']);
                    $designCode = $em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->find($designCodeId);
                    $entity->setDesignCode($designCode);
                    $actionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findBy(['userId'=>$actionOwnerId, 'project'=>$project->getId()]);
                    if ($categoryCode == "PL" || $categoryCode == "PI") $entity->setOuterDiameter($values['outerDiameter']);
                    $entity->setDrawingNumber($values['drawingNumber']);
                    $entity->setNominalWT($values['nominalWT']);
                    $entity->setCorrosionAllowance($values['corrosionAllowance']);
                    $entity->setMAWT($values['MAWT']);
                    if ($categoryCode == "PL" || $categoryCode == "PI" || $categoryCode == "SE") $entity->setSMTS($values['SMTS']);
                    if ($categoryCode == "PL" || $categoryCode == "PI" || $categoryCode == "SE") $entity->setSMYS($values['SMYS']);
                    if ($categoryCode != "PL") $entity->setSystem($values['system']);
                    $entity->setAnomalyCustomId($values['anomalyId']);
                    if ($values['raisedOn']!=null && $values['raisedOn'] instanceof \DateTime)
                        $entity->setRaisedOn($values['raisedOn']);
                    if ($values['inspectionDate']!=null && $values['inspectionDate'] instanceof \DateTime)
                        $entity->setInspDate($values['inspectionDate']);

                    $entity->setDefectDescription($values['defectDescription']);
                    $location = $em->getRepository('AIEAnomalyBundle:Locations')->find($locationId);
                    $entity->setLocation($location);
                    $entity->setLocationPoint($values['testPoint']);
                    if ($categoryCode == "PL"){
                        $gps=explode(",",$values['GPSCoordinates']);
                        if(is_array($gps) && count($gps)==2){
                            $entity->setGpsE($gps[0]);
                            $entity->setGpsN($gps[1]);
                        }
                    }
                    $anomalyThreat = $em->getRepository('AIEAnomalyBundle:AnomalyThreat')->find($threatId);
                    $entity->setThreat($anomalyThreat);
                    $anomalyClass = $em->getRepository('AIEAnomalyBundle:AnomalyClass')->find($classificationId);
                    $entity->setClassification($anomalyClass);
                    $orientation=explode(":",$values['defectOrientation']);
                    if ($values['defectOrientation']!=null & count($orientation)==2){
                        if ($orientation[0]>12 || $orientation[0]<=0)
                            $orientation[0]=12;
                        if ($orientation[1]>59 || $orientation[1]<0)
                            $orientation[1]=0;
                        $orientationTime=\DateTime::createFromFormat("h:i",$orientation[0] . ":" . $orientation[1]);
                        $entity->setDefectOrientation($orientationTime);
                    }
                    $entity->setCorrosionRate($values['corrosionRate']);
                    $entity->setRemainingWallThickness($values['remainingWT']);
                    $entity->setRetiralValue($values['retiralValue']);
                    $entity->setLength($values['length']);
                    $entity->setWidth($values['width']);
                    if ($categoryCode == "PL") $entity->setTolerance($values['inspectionToolTolerance']);
                    $entity->setComments($values['comments']);
                    $entity->setSummary($values['anomalySummary']);
                    $entity->setFailureSummary($values['failureSummary']);
                    $entity->setRecommendations($values['recommendations']);
                    $entity->setRemediationRequireShutdown(($values['shutdownRequired'] == 'Yes' ? 1 : 0));                       $entity->setRemediationRequireShutdown(($values['shutdownRequired'] == 'Yes' ? 1 : 0));

                    if ($values['ro_enabled']=="No" && $values['ai_enabled']=="No" && $values['tro_enabled']=="No" && $values['fm_enabled']=="No" && $values['as_enabled']=="No" )
                    {
                        $this->addFlash('error', 'Please enable atleast one action from Anomlay Inpections/RO/TRO/FM/Assessment in excel sheet!');
                        $items['dataStatus']=3;
                        return $items;
                    }

                    if ($project instanceof RiskProject){
                        if ($values['risk']!=null){
                            $risk=explode("x",$values['risk']);
                            if(is_array($risk) && count($risk)==2){
                                $prob = $this->getArrayItemByKey($risk[0],$risks,null,null,null,true);
                                $cons = $this->getArrayItemByKey($risk[1],$risks,null,null,null,true);
                                $entity->setProbability($prob);
                                $entity->setConsequence($cons);
                                $entity->setIsFabricMaintenanceCriticality(null);
                                $entity->setFabricMaintenanceCriticality(null);
                                $entity->setProductionCriticality(null);
                                $entity->setCriticality(null);
                            }
                        }else{
                            $entity->setProbability(0);
                            $entity->setConsequence(0);
                            $entity->setCriticality(null);
                        }
                    }else{
                        if(!$project->hasFMCriticality() || $values['risk'] != ""){
                            $criticalityId=$this->getArrayItemByKey($values['risk'],$risks,null,null,null,true);
                            $entity->setCriticality($criticalityId);
                            $entity->setProbability(null);
                            $entity->setConsequence(null);
                            $entity->setIsFabricMaintenanceCriticality(null);
                            $entity->setFabricMaintenanceCriticality(null);
                        }else{
                            $fmCrId=$this->getArrayItemByKey($values['fmRisk'],$fmRisks,null,null,null,true);
                            $entity->setCriticality(null);
                            $entity->setProbability(null);
                            $entity->setConsequence(null);
                            $entity->setIsFabricMaintenanceCriticality(true);
                            $entity->setFabricMaintenanceCriticality($fmCrId);
                        }
                    }
                    if ($project instanceof CriticalityProject && $project->hasProductionCriticality()){
                        $prCrId=$this->getArrayItemByKey($values['productionCriticality'],$prodCriticality,null,null,null,true);
                        $entity->setProductionCriticality($prCrId);
                    }else{
                        $entity->setProductionCriticality(null);
                    }
                    $entity->setStatus($statusId);
                    $entity->setStatusTag($entity->getTagChoices()['request']['name']);
                    $entity->setSpa($actionOwner[0]);
                    $entity->setDeferred(0);
                    $actions=[];
                    if ($values['ai_enabled']=="Yes") array_push($actions,0);
                    if ($values['ro_enabled']=="Yes") array_push($actions,1);
                    if ($values['tro_enabled']=="Yes") array_push($actions,2);
                    if ($values['fm_enabled']=="Yes") array_push($actions,3);
                    if ($values['as_enabled']=="Yes") array_push($actions,4);
                    $entity->setActionRequired($actions);
                    if ($values['ai_enabled']=="Yes"){
                        $entity->setInspectionTechnique($values['ai_latestInspectionTechnique']);
                        if ($values['ai_latestInspectionDate']!=null && $values['ai_latestInspectionDate'] instanceof \DateTime)
                            $entity->setInspectionDate($values['ai_latestInspectionDate']);
                        $entity->setInspectionFrequency($values['ai_inspectionFrequency']);
                        $entity->setInspectionResults($values['ai_inspectionResult']);
                        $entity->setInspectionRecommendations($values['ai_inspectionRecommendations']);
                        $entity->setBufferDays($values['ai_bufferDays']);
                    }
                    $em->persist($entity);
                    $em->flush();
                    $this->createBatchRequest($em,$requestsManager,$entity,$batchId);
                    if ($values['ro_enabled']=="Yes"){
                        $regActionOwnerId=$this->getArrayItemByKey($values['ro_actionOwner'],$users,'username','id');
                        $regStatusId=$this->getArrayItemByKey($values['ro_status'],$statusChoices,null,null,null,true);
                        $register = new RepairOrderRegistrar();
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        if ($values['ro_technicalExpiryDate']!=null && $values['ro_technicalExpiryDate'] instanceof \DateTime)
                            $register->setExpiryDate($values['ro_technicalExpiryDate']);
                        $register->setRecommendations($values['ro_recommendations']);
                        $register->setDescription($values['ro_description']);
                        $register->setLocationDetails($values['ro_locationDetails']);
                        $register->setRepairPriority($values['ro_repairPriority']);
                        if ($values['ro_closeOutDate']!=null && $values['ro_closeOutDate'] instanceof \DateTime)
                            $register->setCloseOutDate($values['ro_closeOutDate']);
                        $register->setCloseOutJustification($values['ro_closeOutJustification']);
                        $regActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findBy(['userId'=>$regActionOwnerId, 'project'=>$project->getId()]);
                        $register->setSpa($regActionOwner[0]);
                        $register->setStatus($regStatusId);
                        $register->setStatusTag($register->getTagChoices()['request']['name']);
                        $em->persist($register);
                        $em->flush();
                        $this->createBatchRequest($em,$requestsManager,$register,$batchId);
                    }
                    if ($values['tro_enabled']=="Yes"){
                        $regActionOwnerId=$this->getArrayItemByKey($values['tro_actionOwner'],$users,'username','id');
                        $regStatusId=$this->getArrayItemByKey($values['tro_status'],$statusChoices,null,null,null,true);
                        $register = new TemporaryRepairOrderRegistrar();
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        if ($values['tro_technicalExpiryDate']!=null && $values['tro_technicalExpiryDate'] instanceof \DateTime)
                            $register->setExpiryDate($values['tro_technicalExpiryDate']);
                        $register->setRecommendations($values['tro_recommendations']);
                        $register->setRepairType($values['tro_Type']);
                        $register->setDesignLife($values['tro_designLife']);
                        $register->setMocNumber($values['tro_MOCNumber']);
                        if ($values['tro_installDate']!=null && $values['tro_installDate'] instanceof \DateTime){
                            $register->setInstallationDate($values['tro_installDate']);
                            $register->setInspectionTechnique($values['tro_inspectionTechnique']);
                            if ($values['tro_inspectionDate']!=null && $values['tro_inspectionDate'] instanceof \DateTime)
                                $register->setInspectionDate($values['tro_inspectionDate']);
                            $register->setInspectionFrequency($values['tro_inspectionFrequency']);
                            $register->setInspectionResults($values['tro_inspectionResult']);
                            $register->setBufferDays($values['tro_bufferDays']);
                        }
                        $regActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findBy(['userId'=>$regActionOwnerId, 'project'=>$project->getId()]);
                        $register->setSpa($regActionOwner[0]);
                        $register->setStatus($regStatusId);
                        $register->setStatusTag($register->getTagChoices()['request']['name']);
                        $em->persist($register);
                        $em->flush();
                        $this->createBatchRequest($em,$requestsManager,$register,$batchId);
                    }
                    if ($values['fm_enabled']=="Yes"){
                        $regActionOwnerId=$this->getArrayItemByKey($values['fm_actionOwner'],$users,'username','id');
                        $regStatusId=$this->getArrayItemByKey($values['fm_status'],$statusChoices,null,null,null,true);
                        $register = new FabricMaintenanceRegistrar();
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        if ($values['fm_technicalExpiryDate']!=null && $values['fm_technicalExpiryDate'] instanceof \DateTime)
                            $register->setExpiryDate($values['fm_technicalExpiryDate']);
                        $register->setRecommendations($values['fm_recommendations']);
                        $register->setDescription($values['fm_description']);
                        $register->setImmediateRepair(($values['fm_immediateRepair']!=null?(($values['fm_immediateRepair']=="Yes")?true:false):null));
                        $register->setLongIsolation(($values['fm_longIsolation']!=null?(($values['fm_longIsolation']=="Yes")?true:false):null));
                        $register->setInService($values['fm_inService']);
                        if ($values['fm_completionDate']!=null && $values['fm_completionDate'] instanceof \DateTime)
                            $register->setCompletionDate($values['fm_completionDate']);
                        $regActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findBy(['userId'=>$regActionOwnerId, 'project'=>$project->getId()]);
                        $register->setSpa($regActionOwner[0]);
                        $register->setStatus($regStatusId);
                        $register->setStatusTag($register->getTagChoices()['request']['name']);
                        $em->persist($register);
                        $em->flush();
                        $this->createBatchRequest($em,$requestsManager,$register,$batchId);
                    }
                    if ($values['as_enabled']=="Yes"){
                        $regActionOwnerId=$this->getArrayItemByKey($values['as_actionOwner'],$users,'username','id');
                        $regStatusId=$this->getArrayItemByKey($values['as_status'],$statusChoices,null,null,null,true);
                        $register = new AssessmentRegistrar();
                        $register->setProject($project);
                        $register->setAnomaly($entity);
                        if ($values['as_technicalExpiryDate']!=null && $values['as_technicalExpiryDate'] instanceof \DateTime)
                            $register->setExpiryDate($values['as_technicalExpiryDate']);
                        $register->setRecommendations($values['as_recommendations']);
                        if ($values['as_dueDate']!=null && $values['as_dueDate'] instanceof \DateTime)
                            $register->setAssessmentDueDate($values['as_dueDate']);
                        $register->setAssessmentType($values['as_type']);
                        $register->setAssessmentCode($values['as_code']);
                        $register->setAssessmentResult($values['as_results']);
                        $register->setAssessmentBufferDays($values['as_bufferDays']);
                        $regActionOwner = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findBy(['userId'=>$regActionOwnerId, 'project'=>$project->getId()]);
                        $register->setSpa($regActionOwner[0]);
                        $register->setStatus($regStatusId);
                        $register->setStatusTag($register->getTagChoices()['request']['name']);
                        $em->persist($register);
                        $em->flush();
                        $this->createBatchRequest($em,$requestsManager,$register,$batchId);
                    }
                    $items['data'][$dataIndex]['itemStatus']=2;
                }else{
                    $hasFailure=true;
                    $items['data'][$dataIndex]['itemStatus']=3;
                }
                /*} catch (\Exception $e) {
                    $hasFailure=true;
                    $items['data'][$dataIndex]['itemStatus']=3;
                }*/
                $dataIndex++;
            }
        }
        if ($hasFailure){
            $items['dataStatus']=3;
        }else{
            $items['dataStatus']=2;
        }
        $em->clear();
        return $items;
    }

    private function createBatchRequest($em,$requestsManager,$entity,$batchId){
        $entityRequest = $requestsManager->add($entity,null,'new');
        $entityRequest->setRequestSource("load_sheet");
        $entityRequest->setRequestBatch($batchId);
        $entityRequest->setApprovalLevel(1);
        $em->persist($entityRequest);
        $em->flush();
    }

    public function getArrayItemByKey($value, $array,$key, $returnKey,$parentKey=null,$iskeyValue=false){
        if (!$iskeyValue){
            if ($parentKey==null){
                foreach ($array as $obj){
                    if (strtolower($obj[$key])==strtolower($value)){
                        return $obj[$returnKey];
                    }
                }
            }else{
                foreach ($array[$parentKey] as $obj){
                    if (strtolower($obj[$key])==strtolower($value)){
                        return $obj[$returnKey];
                    }
                }
            }
        }else{
            if ($parentKey==null){
                foreach ($array as $k => $obj){
                    if (strtolower($obj)==strtolower($value)){
                        return $k;
                    }
                }
            }else{
                foreach ($array[$parentKey] as $k => $obj){
                    if (strtolower($obj)==strtolower($value)){
                        return $k;
                    }
                }
            }
        }


        return null;
    }
    public function returnValue($field){
        if ($field==null || $field=="" || empty($field)){
            return null;
        }
        return $field;
    }
    private function getIndexByValidationName($name,$validationObj){
        $index =0;
        foreach ($validationObj as $val){
            if (strtolower($val['name'])==strtolower($name)){
                return $index;
            }
            $index++;
        }
        return null;
    }
}
