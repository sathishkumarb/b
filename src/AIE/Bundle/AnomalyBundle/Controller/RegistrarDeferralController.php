<?php

namespace AIE\Bundle\AnomalyBundle\Controller;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Form\RegistrarDeferralType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\AnomalyBundle\Entity\Deferrals;
use AIE\Bundle\AnomalyBundle\Form\DeferralsType;

/**
 * Deferrals controller.
 *
 * @Route("projects/{projectId}/deferrals")
 */
class RegistrarDeferralController extends AnomalyBaseController {
//this function is not used anymore
	private function handleDeferralAction(Request $request, $projectId, $entity) {
		$tempEntity = clone $entity;

		$editForm = $this->createDeferralForm($projectId, $tempEntity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
			$em = $this->getManager();
			$data = $editForm->getData();
			$flag = 0;
			if($tempEntity instanceof ActionRegistrar) {

				if ($tempEntity->getCode() == 'TRO')
				{
					if ( $tempEntity->getInstallationDate() !== null ) {
						$newInspectionDate = new \DateTime($data->getNextInspectionDate());
		                if($newInspectionDate <= $entity->getNextInspectionDate()) {
		                    $this->addFlash('error', 'Cannot defer activity to an expired date.');
		                    return $this->redirectToRoute('anomaly_deferral_action', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
		                } elseif($newInspectionDate > $entity->getNextInspectionDate()) {
		                    $tempEntity->setNextInspectionDate($newInspectionDate);
		                } else {
		                    $tempEntity->setNextInspectionDate(null);
		                }
		            }
		            else{
		            	$flag = 1;
		            }
	            }
	            else{
	            	$flag = 1;
	            }

				if($tempEntity->getExpiryDate() !== null  && $flag == 1) {
					$newExpiryDate = new \DateTime($data->getExpiryDate());
					if($newExpiryDate <= $entity->getExpiryDate()) {
	                    $this->addFlash('error', 'Cannot defer activity to an expired date.');
	                    return $this->redirectToRoute('anomaly_deferral_action', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
	                } else {
						$tempEntity->setExpiryDate($newExpiryDate);
					}
				}
				
			}
			else
			{
                $newInspectionDate = new \DateTime($data->getNextInspectionDate());
                if($newInspectionDate <= $entity->getNextInspectionDate()){
                    $this->addFlash('error', 'Cannot defer activity to an expired date.');
                    return $this->redirectToRoute('anomaly_deferral_anomaly', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
                }elseif($newInspectionDate > $entity->getNextInspectionDate()){
                    $tempEntity->setNextInspectionDate($newInspectionDate);
                }else{
                    $tempEntity->setNextInspectionDate(null);
                }
            }

			$tempEntity->setDeferred($tempEntity->getDeferred());

			$requestsManager = $this->get('anomaly_manager.requests');
			$serializer = $this->get('jms_serializer');
			$entityRequest = $requestsManager->add($entity, $serializer->serialize($tempEntity, 'json'), 'deferral');

			$em->persist($entityRequest);
			$em->detach($tempEntity);

			if (strpos($entity->getStatusTag(), 'new') === false) {
				$entity->setStatusTag($entity->getTagChoices()[ 'request' ][ 'name' ]);
			}

			$em->flush();

			// $type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode()) . 'r';

			$type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode());

			$this->addFlash('success', $entity->getId(). ' Anomaly / Action Item sent for deferral approval.');
			return $this->redirect($this->generateUrl("anomaly_{$type}_show", ['projectId' => $projectId, 'id' => $entity->getId()]));
		}

		return [
			'entity'    => $entity,
			'projectId' => $projectId,
			'edit_form' => $editForm->createView(),
		];
	}

    private function checkRoles($id,$register){
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:'.$register)->find($id);

        if (!$entity) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array('type' => 'Error' ,'message'=>'Unable to find entity.'));
        }

        if ($register == "AnomalyRegistrar"){
            if ($entity->getCode() == "PL" && !($this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY_DEFER', $this->userRolesMergeToCheck))) {
                return false;
            }
            if ($entity->getCode() == "PI" && !($this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            if ($entity->getCode() == "ST" && !($this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            if ($entity->getCode() == "SE" && !($this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            if ($entity->getCode() == "MR" && !($this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            else
                return true;
        }

        elseif ($register == "ActionRegistrar"){

            if ($entity->getCode() == "RO" && !($this->securityHelper->isRoleGranted('ROLE_REPAIR_ORDER_DEFER', $this->userRolesMergeToCheck))) {
                return false;
            }
            if ($entity->getCode() == "TRO" && !($this->securityHelper->isRoleGranted('ROLE_TEMPORARY_REPAIR_ORDER_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            if ($entity ->getCode() == "FM" && !($this->securityHelper->isRoleGranted('ROLE_FABRIC_MAINTENANCE_ORDER_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }
            if ($entity->getCode() == "AS" && !($this->securityHelper->isRoleGranted('ROLE_ASSESSMENT_ORDER_DEFER', $this->userRolesMergeToCheck))) {
                return false;

            }

            else
                return true;
        }
    }

	/**
	 *
	 * @Route("/anomaly/{id}", name="anomaly_deferral_anomaly")
	 * @Method({"GET"})
	 * @Template("AIEAnomalyBundle:RegistrarDeferral:edit.html.twig")
	 */
	public function anomalyDeferralAction(Request $request, $projectId, $id) {
        if (!$this->checkRoles($id,'AnomalyRegistrar')){

            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to defer this anomaly"));

        }
	    $em = $this->getManager();
		$entity = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Anomaly Registrar entity.');
		}

        $editForm = $this->createDeferralForm($projectId, $entity);
//		return $this->handleDeferralAction($request, $projectId, $entity);

        return [
            'entity'    => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
        ];
    }

	/**
	 *
	 * @Route("/action/{id}", name="anomaly_deferral_action")
	 * @Method({"GET"})
	 * @Template("AIEAnomalyBundle:RegistrarDeferral:edit.html.twig")
	 */
	public function actionDeferralAction(Request $request, $projectId, $id) {
        if (!$this->checkRoles($id,'ActionRegistrar')){
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Permission Denied','message' => "You don't have permission to defer this order."));

        }
	    $em = $this->getManager();
		$entity = $em->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($id);

		if (! $entity) {
			throw $this->createNotFoundException('Unable to find Action Registrar entity.');
		}

        $editForm = $this->createDeferralForm($projectId, $entity);
        return [
            'entity'    => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
        ];
	}

    /**
     *
     * @Route("/action/{id}/update", name="anomaly_deferral_action_update")
     * @Method({"POST"})
     * @Template("AIEAnomalyBundle:RegistrarDeferral:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id) {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($id);
        $tempEntity = clone $entity;

        $editForm = $this->createDeferralForm($projectId, $tempEntity);
        $editForm->handleRequest($request);



        if ( $editForm->isSubmitted() and $editForm->isValid()) {

            $em = $this->getManager();
            $data = $editForm->getData();
            $flag = 0;



            if($tempEntity instanceof ActionRegistrar) {

                if ($tempEntity->getCode() == 'TRO')
                {

                    if ( $tempEntity->getInstallationDate() !== null )
                    {

                        $newInspectionDate = new \DateTime($data->getNextInspectionDate());
                        $newInspectionDate_U = (int)$newInspectionDate->format('U');

                        $oldInspectionDate = new \DateTime($entity->getNextInspectionDate()->format('Y-m-d'));
                        $oldInspectionDate_U = (int)$oldInspectionDate->format('U');

                        if($newInspectionDate_U <= $oldInspectionDate_U) {
                            $this->addFlash('error', 'Cannot defer activity to an expired date.');
                            return $this->redirectToRoute('anomaly_deferral_action', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
                        } elseif($newInspectionDate_U > $oldInspectionDate_U) {
                            $tempEntity->setNextInspectionDate($newInspectionDate);
                        } else {
                            $tempEntity->setNextInspectionDate(null);
                        }

                    }

                    else{
                        $flag = 1;
                    }

                }
                else{  // for other action registers
                    $flag = 1;
                }

                if($entity->getExpiryDate() !== null  && $flag == 1)
                {
                    $newExpiryDate = new \DateTime($data->getExpiryDate());
                    $newExpiryDate_U = (int)$newExpiryDate->format('U');

                    $oldExpiryDate = new \DateTime($entity->getExpiryDate()->format('Y-m-d'));
                    $oldExpiryDate_U = (int)$oldExpiryDate->format('U');
                    if($newExpiryDate_U <= $oldExpiryDate_U) {
                        $this->addFlash('error', 'Cannot defer activity to an expired date.');
                        return $this->redirectToRoute('anomaly_deferral_action', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
                    } elseif($newExpiryDate_U > $oldExpiryDate_U) {

                        $tempEntity->setExpiryDate($newExpiryDate);
                    } else {

                        $tempEntity->setExpiryDate(null);
                    }
                }

            }



            $entity->setDeferred($tempEntity->getDeferred());

            $requestsManager = $this->get('anomaly_manager.requests');
            $serializer = $this->get('jms_serializer');
            $entityRequest = $requestsManager->add($entity, $serializer->serialize($tempEntity, 'json'), 'deferral');

            $em->persist($entityRequest);
            $em->detach($tempEntity);

            if (strpos($entity->getStatusTag(), 'new') === false) {
                $entity->setStatusTag($entity->getTagChoices()[ 'request' ][ 'name' ]);
            }

            $em->flush();
//
//                 $type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode()) . 'r';

            $type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode());

            $this->addFlash('success', $entity->getId(). ' Action Item sent for deferral approval.');
            return $this->redirect($this->generateUrl("anomaly_{$type}_show", ['projectId' => $projectId, 'id' => $entity->getId()]));
        }
        return [
            'entity'    => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
        ];

    }


    /**
     *
     * @Route("/anomaly/{id}/update", name="anomaly_deferral_anomaly_edit")
     * @Method({"POST"})
     * @Template("AIEAnomalyBundle:RegistrarDeferral:edit.html.twig")
     */
    public function updateAnomalyAction(Request $request, $projectId, $id)
    {
        $em = $this->getManager();
        $entity = $em->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($id);
        $tempEntity = clone $entity;

        $editForm = $this->createDeferralForm($projectId, $tempEntity);
        $editForm->handleRequest($request);



        if ($editForm->isSubmitted() and $editForm->isValid()) {

            $em = $this->getManager();
            $data = $editForm->getData();


            if ($entity->getNextInspectionDate() != false) // next inspection date is not null
            {

                $oldInspectionDate = new \DateTime($entity->getNextInspectionDate()->format('Y-m-d'));
                $oldInspectionDate_U = (int)$oldInspectionDate->format('U');




//            if ($data->getDate() != false)
//            {
                $newInspectionDate = new \DateTime($data->getDate());
                $newInspectionDate_U = (int)$newInspectionDate->format('U');
//            }


                if($newInspectionDate_U <= $oldInspectionDate_U) {

                    $this->addFlash('error', 'Cannot defer activity to an expired date.');
                    return $this->redirectToRoute('anomaly_deferral_anomaly', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
                } elseif($newInspectionDate_U > $oldInspectionDate_U) {

                    $tempEntity->setNextInspectionDate($newInspectionDate);

                }


                $entity->setDeferred($tempEntity->getDeferred());

                $requestsManager = $this->get('anomaly_manager.requests');
                $serializer = $this->get('jms_serializer');
                $entityRequest = $requestsManager->add($entity, $serializer->serialize($tempEntity, 'json'), 'deferral');

                $em->persist($entityRequest);
                $em->detach($tempEntity);

                if (strpos($entity->getStatusTag(), 'new') === false) {
                    $entity->setStatusTag($entity->getTagChoices()[ 'request' ][ 'name' ]);
                }

                $em->flush();
//
//                 $type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode()) . 'r';

                $type = ($entity instanceof AnomalyRegistrar) ? strtolower($entity->getCode()) : strtolower($entity->getCode());

                $this->addFlash('success', $entity->getId(). ' Anomaly item sent for deferral approval.');
                return $this->redirect($this->generateUrl("anomaly_{$type}_show", ['projectId' => $projectId, 'id' => $entity->getId()]));

            }

            else
            {
                $this->addFlash('error', 'Cannot defer an activity with no "Current Next Inspection Activity". Click on the "Back" arrow to go back.');
                return $this->redirectToRoute('anomaly_deferral_anomaly', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
            }
        }
        else
        {
            $this->addFlash('error', 'This form is not valid as some data is missing. Enter the missing data or click on the "Back" arrow to go back.');
            return $this->redirectToRoute('anomaly_deferral_anomaly', ['projectId' => $projectId, 'id' => $entity->getId()], 301);
        }


        return [
            'entity'    => $entity,
            'projectId' => $projectId,
            'edit_form' => $editForm->createView(),
        ];
    }

	private function createDeferralForm($projectId, $entity) {
		$form_url = "anomaly_deferral_anomaly_edit";
		if ($entity instanceof ActionRegistrar) {
			$form_url = "anomaly_deferral_action_update";
		}

		$form = $this->createForm(new RegistrarDeferralType($entity), $entity, [
			'action' => $this->generateUrl($form_url, ['projectId' => $projectId, 'id' => $entity->getId()]),
			'method' => 'POST'
		]);

		$form->add('submit', 'submit', $this->options(['label' => 'Defer', 'attr' => ['class' => 'right']], 'btn'));

		return $form;
	}
}
