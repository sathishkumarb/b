<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;


/**
 * PipingAnomaly
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ExclusionPolicy("None")
 */
class PipingAnomaly extends AnomalyRegistrar {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="system", type="text")
	 */
	private $system;

    /**
     * @var double
     *
     * @ORM\Column(name="outer_diameter", type="decimal", scale=2)
     */
    private $outerDiameter;
    
	/**
	 * @var double
	 *
	 * @ORM\Column(name="smys", type="decimal", scale=2, nullable=true)
	 */
	private $sMYS;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="smts", type="decimal", scale=2, nullable=true)
	 */
	private $sMTS;


	public function getCode() {
		return 'PI';
	}

    /**
     * Set system
     *
     * @param string $system
     * @return PipingAnomaly
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set sMYS
     *
     * @param string $sMYS
     * @return PipingAnomaly
     */
    public function setSMYS($sMYS)
    {
        $this->sMYS = $sMYS;

        return $this;
    }

    /**
     * Get sMYS
     *
     * @return string 
     */
    public function getSMYS()
    {
        return $this->sMYS;
    }

    /**
     * Set sMTS
     *
     * @param string $sMTS
     * @return PipingAnomaly
     */
    public function setSMTS($sMTS)
    {
        $this->sMTS = $sMTS;

        return $this;
    }

    /**
     * Get sMTS
     *
     * @return string 
     */
    public function getSMTS()
    {
        return $this->sMTS;
    }

    /**
     * Set outerDiameter
     *
     * @param string $outerDiameter
     * @return PipingAnomaly
     */
    public function setOuterDiameter($outerDiameter)
    {
        $this->outerDiameter = $outerDiameter;

        return $this;
    }

    /**
     * Get outerDiameter
     *
     * @return string 
     */
    public function getOuterDiameter()
    {
        return $this->outerDiameter;
    }
}
