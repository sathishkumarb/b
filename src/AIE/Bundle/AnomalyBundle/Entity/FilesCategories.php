<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 20/09/15
 * Time: 05:01
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\VeracityBundle\Component\Files\FilesCategories as BaseFilesCategories;
use Doctrine\ORM\Mapping as ORM;

/**
 * FilesCategories
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Repository\FilesCategoriesRepository")
 */
class FilesCategories extends BaseFilesCategories {

	/**
	 * @ORM\ManyToOne(targetEntity="AnomalyRegistrar", inversedBy="filesCategories")
	 * @ORM\JoinColumn(name="anomaly_id", referencedColumnName="id")
	 **/
	protected $anomaly;


    /**
     * Set anomaly
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly
     * @return FilesCategories
     */
    public function setAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly = null)
    {
        $this->anomaly = $anomaly;

        return $this;
    }

    /**
     * Get anomaly
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar 
     */
    public function getAnomaly()
    {
        return $this->anomaly;
    }

    /**
     * Add children
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FilesCategories $children
     * @return FilesCategories
     */
    public function addChild(\AIE\Bundle\AnomalyBundle\Entity\FilesCategories $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FilesCategories $children
     */
    public function removeChild(\AIE\Bundle\AnomalyBundle\Entity\FilesCategories $children)
    {
        $this->children->removeElement($children);
    }
}
