<?php
/**
 * User: mokha
 * Date: 3/13/16
 * Time: 1:23 AM
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesignCode
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DesignCode extends  DesignCodeAbstract {}