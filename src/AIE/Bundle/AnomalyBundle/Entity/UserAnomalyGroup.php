<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
//use Doctrine\Common\Collections\ArrayCollection; 


/**
 * UserAnomalyGroup
 *
 * @ORM\Table(name="user_anomaly_groups")
 * @ORM\Entity
 */
class UserAnomalyGroup {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


	/**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\AnomalyGroupEntityInterface")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;


    /**
     * Set user
     *
     * @param \AIE\Bundle\UserBundle\Entity\User $user
     * @return UserAnomalyGroup
     */
    public function setUser(\AIE\Bundle\UserBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AIE\Bundle\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\UserBundle\Entity\AnomalyGroup $group
     * @return UserAnomalyGroup
     */
    public function setGroup(\AIE\Bundle\UserBundle\Entity\AnomalyGroup $group = null) {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AIE\Bundle\UserBundle\Entity\AnomalyGroupEntityInterface 
     */
    public function getGroup() {
        return $this->group;
    }

       /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

}