<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 16:15
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * CriticalityProject
 *
 * @ORM\Table()
 * @ORM\Entity
 **/
class CriticalityProject extends Projects
{
    private $_choices = [ 'VH' => 'Very High', 'H' => 'High', 'M' => 'Medium', 'L' => 'Low', 'VL' => 'Very Low' ];

    /**
     * @ORM\OneToOne(targetEntity="CriticalityRanks", cascade={"all"})
     * @ORM\JoinColumn(name="criticality_id", referencedColumnName="id")
     */
    protected $criticality;

    /**
     * @ORM\OneToOne(targetEntity="CriticalityRanks" , mappedBy="project" , cascade={"all"})
     */
    protected $ranks;

    /**
     * @var integer
     *
     * @ORM\Column(name="criticality_selected", type="integer", nullable=true)
     */
    private $criticalitySelected;

    /**
     * @var integer
     *
     * @ORM\Column(name="h_enabled", type="integer", nullable=true)
     */
    private $hEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="h_name", type="text", nullable=true)
     */
    private $hName;

    /**
     * @var string
     *
     * @ORM\Column(name="h_color", type="text", nullable=true)
     */
    private $hColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="m_enabled", type="integer", nullable=true)
     */
    private $mEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="m_name", type="text", nullable=true)
     */
    private $mName;

    /**
     * @var string
     *
     * @ORM\Column(name="m_color", type="text", nullable=true)
     */
    private $mColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="l_enabled", type="integer", nullable=true)
     */
    private $lEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="l_name", type="text", nullable=true)
     */
    private $lName;

    /**
     * @var string
     *
     * @ORM\Column(name="l_color", type="text", nullable=true)
     */
    private $lColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="vh_enabled", type="integer", nullable=true)
     */
    private $vhEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="vh_name", type="text", nullable=true)
     */
    private $vhName;

    /**
     * @var string
     *
     * @ORM\Column(name="vh_color", type="text", nullable=true)
     */
    private $vhColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="vl_enabled", type="integer", nullable=true)
     */
    private $vlEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="vl_name", type="text", nullable=true)
     */
    private $vlName;

    /**
     * @var string
     *
     * @ORM\Column(name="vl_color", type="text", nullable=true)
     */
    private $vlColor;

    /**
     * Set criticalitySelected
     *
     * @param integer $criticalitySelected
     * @return CriticalityRanks
     */
    public function setCriticalitySelected($criticalitySelected)
    {
        $this->criticalitySelected = $criticalitySelected;

        return $this;
    }

    /**
     * Get criticalitySelected
     *
     * @return integer
     */
    public function getCriticalitySelected()
    {
        return $this->criticalitySelected;
    }

    /**
     * Set hEnabled
     *
     * @param integer $hEnabled
     * @return CriticalityRanks
     */
    public function setHEnabled($hEnabled)
    {
        $this->hEnabled = $hEnabled;

        return $this;
    }

    /**
     * Get hEnabled
     *
     * @return integer
     */
    public function getHEnabled()
    {
        return $this->hEnabled;
    }

    /**
     * Set hName
     *
     * @param string $hName
     * @return CriticalityRanks
     */
    public function setHName($hName)
    {
        $this->hName = $hName;

        return $this;
    }

    /**
     * Get hName
     *
     * @return string
     */
    public function getHName()
    {
        return $this->hName;
    }

    /**
     * Set hColor
     *
     * @param string $hColor
     * @return CriticalityRanks
     */
    public function setHColor($hColor)
    {
        $this->hColor = $hColor;

        return $this;
    }

    /**
     * Get hColor
     *
     * @return string
     */
    public function getHColor()
    {
        return $this->hColor;
    }

    /**
     * Set mEnabled
     *
     * @param integer $mEnabled
     * @return CriticalityRanks
     */
    public function setMEnabled($mEnabled)
    {
        $this->mEnabled = $mEnabled;

        return $this;
    }

    /**
     * Get mEnabled
     *
     * @return integer
     */
    public function getMEnabled()
    {
        return $this->mEnabled;
    }

    /**
     * Set mName
     *
     * @param string $mName
     * @return CriticalityRanks
     */
    public function setMName($mName)
    {
        $this->mName = $mName;

        return $this;
    }

    /**
     * Get mName
     *
     * @return string
     */
    public function getMName()
    {
        return $this->mName;
    }

    /**
     * Set mColor
     *
     * @param string $mColor
     * @return CriticalityRanks
     */
    public function setMColor($mColor)
    {
        $this->mColor = $mColor;

        return $this;
    }

    /**
     * Get mColor
     *
     * @return string
     */
    public function getMColor()
    {
        return $this->mColor;
    }

    /**
     * Set lEnabled
     *
     * @param integer $lEnabled
     * @return CriticalityRanks
     */
    public function setLEnabled($lEnabled)
    {
        $this->lEnabled = $lEnabled;

        return $this;
    }

    /**
     * Get lEnabled
     *
     * @return integer
     */
    public function getLEnabled()
    {
        return $this->lEnabled;
    }

    /**
     * Set lName
     *
     * @param string $lName
     * @return CriticalityRanks
     */
    public function setLName($lName)
    {
        $this->lName = $lName;

        return $this;
    }

    /**
     * Get lName
     *
     * @return string
     */
    public function getLName()
    {
        return $this->lName;
    }

    /**
     * Set lColor
     *
     * @param string $lColor
     * @return CriticalityRanks
     */
    public function setLColor($lColor)
    {
        $this->lColor = $lColor;

        return $this;
    }

    /**
     * Get lColor
     *
     * @return string
     */
    public function getLColor()
    {
        return $this->lColor;
    }

    /**
     * Set vhEnabled
     *
     * @param integer $vhEnabled
     * @return CriticalityRanks
     */
    public function setVhEnabled($vhEnabled)
    {
        $this->vhEnabled = $vhEnabled;

        return $this;
    }

    /**
     * Get vhEnabled
     *
     * @return integer
     */
    public function getVhEnabled()
    {
        return $this->vhEnabled;
    }

    /**
     * Set vhName
     *
     * @param string $vhName
     * @return CriticalityRanks
     */
    public function setVhName($vhName)
    {
        $this->vhName = $vhName;

        return $this;
    }

    /**
     * Get vhName
     *
     * @return string
     */
    public function getVhName()
    {
        return $this->vhName;
    }

    /**
     * Set vhColor
     *
     * @param string $vhColor
     * @return CriticalityRanks
     */
    public function setVhColor($vhColor)
    {
        $this->vhColor = $vhColor;

        return $this;
    }

    /**
     * Get vhColor
     *
     * @return string
     */
    public function getVhColor()
    {
        return $this->vhColor;
    }

    /**
     * Set vlEnabled
     *
     * @param integer $vlEnabled
     * @return CriticalityRanks
     */
    public function setVlEnabled($vlEnabled)
    {
        $this->vlEnabled = $vlEnabled;

        return $this;
    }

    /**
     * Get vlEnabled
     *
     * @return integer
     */
    public function getVlEnabled()
    {
        return $this->vlEnabled;
    }

    /**
     * Set vlName
     *
     * @param string $vlName
     * @return CriticalityRanks
     */
    public function setVlName($vlName)
    {
        $this->vlName = $vlName;

        return $this;
    }

    /**
     * Get vlName
     *
     * @return string
     */
    public function getVlName()
    {
        return $this->vlName;
    }

    /**
     * Set vlColor
     *
     * @param string $vlColor
     * @return CriticalityRanks
     */
    public function setVlColor($vlColor)
    {
        $this->vlColor = $vlColor;

        return $this;
    }

    /**
     * Get vlColor
     *
     * @return string
     */
    public function getVlColor()
    {
        return $this->vlColor;
    }

    /**
     * Set criticality
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $criticality
     * @return CriticalityProject
     */
    public function setCriticality(\AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $criticality = null)
    {
        $this->criticality = $criticality;

        return $this;
    }

    /**
     * Get criticality
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks
     */
    public function getCriticality()
    {
        return $this->criticality;
    }

    public function getChoices() {
        return $this->_choices;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ranks = new CriticalityRanks;
        $this->enabledCriticalityColors=new \Doctrine\Common\Collections\ArrayCollection();
        $this->enabledCriticalityChoices=new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ranks
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $ranks
     * @return CriticalityProject
     */
    public function addRank(\AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $ranks)
    {
        $this->ranks[] = $ranks;

        return $this;
    }

    /**
     * Remove ranks
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $ranks
     */
    public function removeRank(\AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks $ranks)
    {
        $this->ranks->removeElement($ranks);
    }

    /**
     * Get ranks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRanks()
    {
        return $this->ranks;
    }

    ///**
    // * @var array
    // *
    // * @JMS\Type("array")
    // * @JMS\Exclude
    // * @JMS\ReadOnly
    // */
    private $enabledCriticalityChoices = [
        0 => 'Ver Low',
        1 => 'Low',
        2 => 'Medium',
        3 => 'High',
        4 => 'Very High'
    ];

    public function getEnabledCriticalityChoices()
    {
        if ($this instanceof CriticalityProject and $this->getId()){
            if ($this->getVlEnabled() != 1) {
                unset($this->enabledCriticalityChoices[0]);
            }else{
                $this->enabledCriticalityChoices[0]=$this->getVlName();
            }
            if ($this->getLEnabled() != 1) {
                unset($this->enabledCriticalityChoices[1]);
            }else{
                $this->enabledCriticalityChoices[1]=$this->getLName();
            }
            if ($this->getMEnabled() != 1) {
                unset($this->enabledCriticalityChoices[2]);
            }else{
                $this->enabledCriticalityChoices[2]=$this->getMName();
            }
            if ($this->getHEnabled() != 1) {
                unset($this->enabledCriticalityChoices[3]);
            }else{
                $this->enabledCriticalityChoices[3]=$this->getHName();
            }
            if ($this->getVHEnabled() != 1) {
                unset($this->enabledCriticalityChoices[4]);
            }else{
                $this->enabledCriticalityChoices[4]=$this->getVhName();
            }
            return $this->enabledCriticalityChoices;
        }

    }

    ///**
    // * @var array
    // *
    // * @JMS\Type("array")
    // * @JMS\Exclude
    // * @JMS\ReadOnly
    // */
    private $enabledCriticalityColors = [
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => ''
    ];

    public function getEnabledCriticalityColors()
    {

        if ($this->getVlEnabled() != 1) {
            unset($this->enabledCriticalityColors[0]);
        }else{
            $this->enabledCriticalityColors[0]=$this->getVlColor();
        }
        if ($this->getLEnabled() != 1) {
            unset($this->enabledCriticalityColors[1]);
        }
        else{
            $this->enabledCriticalityColors[1]=$this->getLColor();
        }
        if ($this->getMEnabled() != 1) {
            unset($this->enabledCriticalityColors[2]);
        }else{
            $this->enabledCriticalityColors[2]=$this->getMColor();
        }
        if ($this->getHEnabled() != 1) {
            unset($this->enabledCriticalityColors[3]);
        }else{
            $this->enabledCriticalityColors[3]=$this->getHColor();
        }
        if ($this->getVHEnabled() != 1) {
            unset($this->enabledCriticalityColors[4]);
        }else{
            $this->enabledCriticalityColors[4]=$this->getVhColor();
        }
        return $this->enabledCriticalityColors;
    }
}