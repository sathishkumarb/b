<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * UserProjectGroup
 *
 * @ORM\Table(name="UserProjectGroup")
 * @ORM\Entity
 */
class UserProjectGroup {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\AnomalyGroupEntityInterface")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="ug")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * Set user
     *
     * @param \AIE\Bundle\UserBundle\Entity\User $user
     * @return UserProjectGroup
     */
    public function setUser(\AIE\Bundle\UserBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AIE\Bundle\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\UserBundle\Entity\AnomalyGroup $group
     * @return UserProjectGroup
     */
    public function setGroup(\AIE\Bundle\UserBundle\Entity\AnomalyGroup $group = null) {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AIE\Bundle\UserBundle\Entity\AnomalyGroupEntityInterface 
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return UserProjectGroup
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects 
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function getGroupRoles() {
        return $this->getGroup()->getRoles();
    }
    
    public function getUserRoles(){
        return $this->getUser()->getRoles();
    }
}