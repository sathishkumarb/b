<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Monitor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Monitor extends BaseMonitor {
	/**
	 * @ORM\ManyToOne(targetEntity="AnomalyRegistrar", inversedBy="monitors")
	 * @ORM\JoinColumn(name="anomaly_id", referencedColumnName="id")
	 * */
	protected $registrar;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="defect_description", type="text", nullable=true)
	 */
	protected $defectDescription;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="remaining_thickness", type="decimal", scale=2, nullable=true)
	 */
	protected $remainingWallThickness;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="length", type="decimal", scale=2, nullable=true)
	 */
	protected $length;

    /**
     * @var double
     *
     * @ORM\Column(name="corrosion_rate", type="decimal", scale=2, nullable=true)
     */
    protected $corrosionRate;


	/**
	 * @var array
	 *
	 * @ORM\Column(name="tracking_criteria", type="json_array", nullable=true)
	 */
	protected $trackingCriteria;

    /**
     * @var array
     *
     * @ORM\Column(name="tracking_value", type="json_array", nullable=true)
     */
    protected $trackingValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="comments", type="text", nullable=true)
	 */
	protected $comments;

	/**
	 * Set registrar
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $registrar
	 * @return Monitor
	 */
	public function setRegistrar(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $registrar = null) {
		$this->registrar = $registrar;

		return $this;
	}

	/**
	 * Get registrar
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar
	 */
	public function getRegistrar() {
		return $this->registrar;
	}

    /**
     * Set defectDescription
     *
     * @param string $defectDescription
     * @return Monitor
     */
    public function setDefectDescription($defectDescription)
    {
        $this->defectDescription = $defectDescription;

        return $this;
    }

    /**
     * Get defectDescription
     *
     * @return string 
     */
    public function getDefectDescription()
    {
        return $this->defectDescription;
    }

    /**
     * Set remainingWallThickness
     *
     * @param string $remainingWallThickness
     * @return Monitor
     */
    public function setRemainingWallThickness($remainingWallThickness)
    {
        $this->remainingWallThickness = $remainingWallThickness;

        return $this;
    }

    /**
     * Get remainingWallThickness
     *
     * @return string 
     */
    public function getRemainingWallThickness()
    {
        return $this->remainingWallThickness;
    }

    /**
     * Set length
     *
     * @param string $length
     * @return Monitor
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Monitor
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set trackingCriteria
     *
     * @param array $trackingCriteria
     * @return Monitor
     */
    public function setTrackingCriteria($trackingCriteria)
    {
        $this->trackingCriteria = $trackingCriteria;

        return $this;
    }

    /**
     * Get trackingCriteria
     *
     * @return array 
     */
    public function getTrackingCriteria()
    {
        return $this->trackingCriteria;
    }

    /**
     * Set corrosionRate
     *
     * @param string $corrosionRate
     * @return Monitor
     */
    public function setCorrosionRate($corrosionRate)
    {
        $this->corrosionRate = $corrosionRate;

        return $this;
    }

    /**
     * Get corrosionRate
     *
     * @return string 
     */
    public function getCorrosionRate()
    {
        return $this->corrosionRate;
    }

    /**
     * Get trackingValue
     *
     * @return string
     */
    public function getTrackingValue() {
        return $this->trackingValue;
    }
}
