<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActionOwners
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\ActionOwnersRepository")
 */
class ActionOwners {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="user_id", type="integer")
	 */
	protected $userId;

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="position", type="string", length=255)
	 */
	protected $position;

	/**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="actionOwners")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	protected $project;


    /**
     * @ORM\OneToMany(targetEntity="Registrar", mappedBy="spa", cascade={"persist", "merge", "detach", "refresh"}, fetch="EXTRA_LAZY")
     */
    protected $registrars;

    /**
     * @ORM\OneToMany(targetEntity="Request", mappedBy="requestedBy" , cascade={"persist", "merge", "detach", "refresh"}, fetch="EXTRA_LAZY")
     */
    protected $requests;


	/**
	 * Set project
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
	 * @return ActionOwners
	 */
	public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null) {
		$this->project = $project;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set position
	 *
	 * @param string $position
	 * @return ActionOwners
	 */
	public function setPosition($position) {
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return string
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * Set userId
	 *
	 * @param integer $userId
	 * @return ActionOwners
	 */
	public function setUserId($userId) {
		$this->userId = $userId;

		return $this;
	}

	/**
	 * Get userId
	 *
	 * @return integer
	 */
	public function getUserId() {
		return $this->userId;
	}


	public function setUser($user) {
	    if(is_numeric($user)){
            $this->user = $user;

            return $this;
        }
		$this->user = $user->getId();
		$this->user = $user;

		return $this;
	}

	public function getUser() {
		return $this->user;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deferrals = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add registrars
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars
     * @return ActionOwners
     */
    public function addRegistrar(\AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars)
    {
        $this->registrars[] = $registrars;

        return $this;
    }

    /**
     * Remove registrars
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars
     */
    public function removeRegistrar(\AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars)
    {
        $this->registrars->removeElement($registrars);
    }

    /**
     * Get registrars
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegistrars()
    {
        return $this->registrars;
    }

    /**
     * Add requests
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $requests
     * @return ActionOwners
     */
    public function addRequest(\AIE\Bundle\AnomalyBundle\Entity\Request $requests)
    {
        $this->requests[] = $requests;

        return $this;
    }

    /**
     * Remove requests
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $requests
     */
    public function removeRequest(\AIE\Bundle\AnomalyBundle\Entity\Request $requests)
    {
        $this->requests->removeElement($requests);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * Add answers
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $approvals
     * @return ActionOwners
     */
    public function addApproval(\AIE\Bundle\AnomalyBundle\Entity\Request $approvals)
    {
        $this->answers[] = $approvals;

        return $this;
    }

    /**
     * Remove answers
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $approvals
     */
    public function removeApproval(\AIE\Bundle\AnomalyBundle\Entity\Request $approvals)
    {
        $this->answers->removeElement($approvals);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
