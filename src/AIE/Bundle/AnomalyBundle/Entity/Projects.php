<?php
/**
 * Modified by Sathish Kumar
 * Date: 28/06/2016
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\VeracityBundle\Component\Core\Projects as ProjectsBase;
use AIE\Bundle\AnomalyBundle\Entity\RiskMatrix;
use AIE\Bundle\AnomalyBundle\Security\SecureObjectInterface;
use AIE\Bundle\UserBundle\Entity\User;
use JMS\Serializer\Annotation as JMS;

/**
 * Projects
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="project_type", type="integer")
 * @ORM\DiscriminatorMap({0 = "Projects", 1 = "RiskProject", 2 = "CriticalityProject"})
 */
class Projects extends ProjectsBase
{

    /**
     * @ORM\OneToMany(targetEntity="Locations" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $locations;

    /**
     * @ORM\OneToMany(targetEntity="AnomalyClass" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $classes;

    /**
     * @ORM\OneToMany(targetEntity="AnomalyThreat" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $threats;

    /**
     * @ORM\OneToMany(targetEntity="ActionOwners" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $actionOwners;

    /**
     * @ORM\OneToMany(targetEntity="FabricMaintenanceCriticality" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $fabricMaintenanceCriticality;

    /**
     * @ORM\OneToMany(targetEntity="ProductionCriticality" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $productionCriticality;

    /**
     * @ORM\OneToMany(targetEntity="ReportSequence" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $reportSequence;

    /**
     * @var string
     *
     * @ORM\Column(name="folder", type="string", length=32)
     */
    private $folder;

    /**
     * @var integer
     * @ORM\Column(name="approval_level", type="integer")
     */
    private $approvalLevel;

    /**
     * @var boolean
     * @ORM\Column(name="has_production_criticality", type="boolean")
     */
    private $enableProductionCriticality;

    /**
     * @var boolean
     * @ORM\Column(name="has_fm_criticality", type="boolean")
     */
    private $enableFMCriticality;

    /**
     * @ORM\OneToMany(targetEntity="Registrar" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $registrars;

	/**
	 * @ORM\OneToMany(targetEntity="Deferrals" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
	 */
	private $deferrals;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApprovalOwners" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $approvalOwners;

    /**
     * @ORM\OneToMany(targetEntity="AnomalyDesignCode" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $codes;

	/**
	 * @ORM\OneToMany(targetEntity="ProjectConsequences" , mappedBy="project" , cascade={"persist", "merge", "detach"}, fetch="EXTRA_LAZY")
	 */
	protected $consequences;
	
	/**
     * @ORM\OneToMany(targetEntity="UserProjectGroup" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ug;

    protected $isSetup;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->locations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->classes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actionOwners = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deferrals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->codes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->approvalOwners = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fabricMaintenanceCriticality = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productionCriticality = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reportDownload = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->criticalityProject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getProjectApprovalLevels(){
        if ($this->getApprovalLevel()==1){
            return [1=>"Approval Request"];
        }
        return [1 => "Review Request", 2=>"Approval Request"];
    }

    public function getIsSetup(){
        $this->isSetup=['isSetup'=>true, 'missing'=>[
            'locations'=>false,
            'classifications'=>false,
            'threats'=>false,
            'actionOwners'=>true,
            'codes'=>false,
            'risk'=>false,
        ]];
        if (count($this->locations)==0){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['locations']=true;
        }
        if (count($this->classes)==0){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['classifications']=true;
        }
        if (count($this->threats)==0){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['threats']=true;
        }
        if (count($this->actionOwners)==0){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['actionOwners']=true;
        }
        if (count($this->codes)==0){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['codes']=true;
        }
        if($this instanceof RiskProject && (count($this->getConsequences())==0)){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['risk']=true;
        }
        if($this instanceof CriticalityProject && !$this->getCriticalitySelected()){
            $this->isSetup['isSetup']=false;
            $this->isSetup['missing']['risk']=true;
        }
        return $this->isSetup;
    }

    /**
     * Add locations
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Locations $locations
     * @return Projects
     */
    public function addLocation(\AIE\Bundle\AnomalyBundle\Entity\Locations $locations)
    {
        $this->locations[] = $locations;
    
        return $this;
    }

    /**
     * Remove locations
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Locations $locations
     */
    public function removeLocation(\AIE\Bundle\AnomalyBundle\Entity\Locations $locations)
    {
        $this->locations->removeElement($locations);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Get classes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Add threats
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threats
     * @return Projects
     */
    public function addThreat(\AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threats)
    {
        $this->threats[] = $threats;

        return $this;
    }

    /**
     * Remove threats
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threats
     */
    public function removeThreat(\AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threats)
    {
        $this->threats->removeElement($threats);
    }

    /**
     * Get threats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreats()
    {
        return $this->threats;
    }

    /**
     * Add actionOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners
     * @return Projects
     */
    public function addActionOwner(\AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners)
    {
        $this->actionOwners[] = $actionOwners;
    
        return $this;
    }

    /**
     * Remove actionOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners
     */
    public function removeActionOwner(\AIE\Bundle\AnomalyBundle\Entity\ActionOwners $actionOwners)
    {
        $this->actionOwners->removeElement($actionOwners);
    }

    /**
     * Get actionOwners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActionOwners()
    {
        return $this->actionOwners;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return Pipelines
     */
    public function setFolder($folder) {
        $this->folder = $folder;

        return $this;
    }

	public function getUploadDir()
	{
		return $this->getFolder() . 'logos';
	}

    /**
     * Get folder
     *
     * @return string
     */
    public function getFolder() {
        return $this->folder;
    }

    /**
     * Set approvalLevel
     *
     * @param integer $approvalLevel
     * @return ApprovalOwners
     */
    public function setApprovalLevel($approvalLevel) {
        $this->approvalLevel = $approvalLevel;

        return $this;
    }

    /**
     * Get approvalLevel
     *
     * @return integer
     */
    public function getApprovalLevel() {
        return $this->approvalLevel;
    }


    public function getType(){
        return ($this instanceof RiskProject)? 1: 2;
    }

    /**
     * Add registrars
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars
     * @return Projects
     */
    public function addRegistrar(\AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars)
    {
        $this->registrars[] = $registrars;
    
        return $this;
    }

    /**
     * Remove registrars
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars
     */
    public function removeRegistrar(\AIE\Bundle\AnomalyBundle\Entity\Registrar $registrars)
    {
        $this->registrars->removeElement($registrars);
    }

    /**
     * Get registrars
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegistrars()
    {
        return $this->registrars;
    }

    /**
     * Add classes
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classes
     * @return Projects
     */
    public function addClass(\AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classes)
    {
        $this->classes[] = $classes;

        return $this;
    }

    /**
     * Remove classes
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classes
     */
    public function removeClass(\AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classes)
    {
        $this->classes->removeElement($classes);
    }

    /**
     * Add consequences
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences
     * @return Projects
     */
    public function addConsequence(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences)
    {
        $this->consequences[] = $consequences;

        return $this;
    }

    /**
     * Remove consequences
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences
     */
    public function removeConsequence(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences)
    {
        $this->consequences->removeElement($consequences);
    }

    /**
     * Get consequences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsequences()
    {
        return $this->consequences;
    }

    /**
     * Add deferrals
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Deferrals $deferrals
     * @return Projects
     */
    public function addDeferral(\AIE\Bundle\AnomalyBundle\Entity\Deferrals $deferrals)
    {
        $this->deferrals[] = $deferrals;

        return $this;
    }

    /**
     * Remove deferrals
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Deferrals $deferrals
     */
    public function removeDeferral(\AIE\Bundle\AnomalyBundle\Entity\Deferrals $deferrals)
    {
        $this->deferrals->removeElement($deferrals);
    }

    /**
     * Get deferrals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeferrals()
    {
        return $this->deferrals;
    }

    /**
     * Add codes
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $codes
     * @return Projects
     */
    public function addCode(\AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $codes)
    {
        $this->codes[] = $codes;

        return $this;
    }

    /**
     * Remove codes
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $codes
     */
    public function removeCode(\AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $codes)
    {
        $this->codes->removeElement($codes);
    }

    /**
     * Get codes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * Add approvalOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalOwners
     * @return Projects
     */
    public function addApprovalOwner(\AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalOwners)
    {
        $this->approvalOwners[] = $approvalOwners;

        return $this;
    }

    /**
     * Remove approvalOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalowners
     */
    public function removeApprovalOwners(\AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalOwners)
    {
        $this->approvalOwners->removeElement($approvalOwners);
    }

    /**
     * Get deferrals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApprovalOwners()
    {
        return $this->approvalOwners;
    }

    /**
     * Add ug
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\UserProjectGroup $ug
     * @return Projects
     */
    public function addUg(\AIE\Bundle\AnomalyBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug[] = $ug;
    
        return $this;
    }

    /**
     * Remove ug
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup $ug
     */
    public function removeUg(\AIE\Bundle\AnomalyBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug->removeElement($ug);
    }

    /**
     * Get ug
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUg()
    {
        return $this->ug;
    }

    public function getServerFilePath($isEnv){
        if($isEnv){
            return '../web/uploads/' . $this->getFilePath();
        }
        return $this->getFilepath();
    }

    /**
     * Get fabricMaintenanceCriticality
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFabricMaintenanceCriticality()
    {
        return $this->fabricMaintenanceCriticality;
    }

    /**
     * Get fabricMaintenanceCriticality
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductionCriticality()
    {
        return $this->productionCriticality;
    }

    public function getFMCriticalityArray(){
        $arr = [];
        if (is_array($this->fabricMaintenanceCriticality) || is_object($this->fabricMaintenanceCriticality))
        {
            foreach ($this->fabricMaintenanceCriticality as $fm){
                $arr[$fm->getId()] = $fm->getFMCriticalityCode();
            }
        }
        return $arr;
    }

    public function getFMCriticalityColorsArray(){
        $arr = [];
        if (is_array($this->fabricMaintenanceCriticality) || is_object($this->fabricMaintenanceCriticality))
        {
            foreach ($this->fabricMaintenanceCriticality as $fm){
                $arr[$fm->getId()] = $fm->getFMCriticalityColor();
            }
        }
        return $arr;
    }

    public function getProductionCriticalityArray(){
        $arr = [];
        foreach ($this->productionCriticality as $fm){
            $arr[$fm->getId()] = $fm->getProductionCriticalityCode();
        }
        return $arr;
    }

    /**
     * Set enableProductionCriticality
     *
     * @param boolean $enableProductionCriticality
     * @return Projects
     */
    public function setEnableProductionCriticality($enableProductionCriticality) {
        $this->enableProductionCriticality = $enableProductionCriticality;

        return $this;
    }

    /**
     * Get enableProductionCriticality
     *
     * @return boolean
     */
    public function getEnableProductionCriticality() {
        return $this->enableProductionCriticality;
    }

    /**
     * Has enableProductionCriticality
     *
     * @return boolean
     */
    public function hasProductionCriticality() {
        if ($this->enableProductionCriticality == null)
            return false;
        return $this->enableProductionCriticality;
    }


    /**
     * Set enableFMCriticality
     *
     * @param boolean $enableFMCriticality
     * @return Projects
     */
    public function setEnableFMCriticality($enableFMCriticality) {
        $this->enableFMCriticality = $enableFMCriticality;
        return $this;
    }

    /**
     * Get enableFMCriticality
     *
     * @return boolean
     */
    public function getEnableFMCriticality() {
        return $this->enableFMCriticality;
    }

    /**
     * Has enableFMCriticality
     *
     * @return boolean
     */
    public function hasFMCriticality() {
        if ($this->enableFMCriticality == null)
            return false;
        return $this->enableFMCriticality;
    }


    /**
     * Add pipelines
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Locations $pipelines
     * @return Projects
     */
    public function addPipeline(\AIE\Bundle\AnomalyBundle\Entity\Locations $pipelines)
    {
        $this->pipelines[] = $pipelines;

        return $this;
    }

    /**
     * Remove pipelines
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Locations $pipelines
     */
    public function removePipeline(\AIE\Bundle\AnomalyBundle\Entity\Locations $pipelines)
    {
        $this->pipelines->removeElement($pipelines);
    }

    /**
     * Get pipelines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPipelines()
    {
        return $this->pipelines;
    }

    /**
     * Add fabricMaintenanceCriticality
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality $fabricMaintenanceCriticality
     * @return Projects
     */
    public function addFabricMaintenanceCriticality(\AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality $fabricMaintenanceCriticality)
    {
        $this->fabricMaintenanceCriticality[] = $fabricMaintenanceCriticality;

        return $this;
    }

    /**
     * Remove fabricMaintenanceCriticality
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality $fabricMaintenanceCriticality
     */
    public function removeFabricMaintenanceCriticality(\AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceCriticality $fabricMaintenanceCriticality)
    {
        $this->fabricMaintenanceCriticality->removeElement($fabricMaintenanceCriticality);
    }

    /**
     * Add productionCriticality
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProductionCriticality $productionCriticality
     * @return Projects
     */
    public function addProductionCriticality(\AIE\Bundle\AnomalyBundle\Entity\ProductionCriticality $productionCriticality)
    {
        $this->productionCriticality[] = $productionCriticality;

        return $this;
    }

    /**
     * Remove productionCriticality
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProductionCriticality $productionCriticality
     */
    public function removeProductionCriticality(\AIE\Bundle\AnomalyBundle\Entity\ProductionCriticality $productionCriticality)
    {
        $this->productionCriticality->removeElement($productionCriticality);
    }

    /**
     * Remove approvalOwners
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalOwners
     */
    public function removeApprovalOwner(\AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvalOwners)
    {
        $this->approvalOwners->removeElement($approvalOwners);
    }

    /**
     * Add reportSequence
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ReportSequence $reportSequence
     * @return Projects
     */
    public function addReportSequence(\AIE\Bundle\AnomalyBundle\Entity\ReportSequence $reportSequence)
    {
        $this->reportSequence[] = $reportSequence;

        return $this;
    }

    /**
     * Remove reportSequence
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ReportSequence $reportSequence
     */
    public function removeReportSequence(\AIE\Bundle\AnomalyBundle\Entity\ReportSequence $reportSequence)
    {
        $this->reportSequence->removeElement($reportSequence);
    }

    /**
     * Get reportSequence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportSequence()
    {
        return $this->reportSequence;
    }
}
