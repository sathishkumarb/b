<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;


/**
 * StaticEquipmentAnomaly
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ExclusionPolicy("None")
 */
class StaticEquipmentAnomaly extends AnomalyRegistrar {


	/**
	 * @var string
	 *
	 * @ORM\Column(name="system", type="text")
	 */
	private $system;


	/**
	 * @var double
	 *
	 * @ORM\Column(name="smys", type="decimal", scale=2, nullable=true)
	 */
	private $sMYS;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="smts", type="decimal", scale=2, nullable=true)
	 */
	private $sMTS;

	public function getCode(){
		return 'SE';
	}

    /**
     * Set system
     *
     * @param string $system
     * @return StaticEquipmentAnomaly
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set sMYS
     *
     * @param string $sMYS
     * @return StaticEquipmentAnomaly
     */
    public function setSMYS($sMYS)
    {
        $this->sMYS = $sMYS;

        return $this;
    }

    /**
     * Get sMYS
     *
     * @return string 
     */
    public function getSMYS()
    {
        return $this->sMYS;
    }

    /**
     * Set sMTS
     *
     * @param string $sMTS
     * @return StaticEquipmentAnomaly
     */
    public function setSMTS($sMTS)
    {
        $this->sMTS = $sMTS;

        return $this;
    }

    /**
     * Get sMTS
     *
     * @return string 
     */
    public function getSMTS()
    {
        return $this->sMTS;
    }
}
