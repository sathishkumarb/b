<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 04/01/16
 * Time: 20:23
 */

namespace AIE\Bundle\AnomalyBundle\Entity;


use AIE\Bundle\VeracityBundle\Component\Core\Tags;
use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;

/**
 * Registrar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="registrar_type", type="string")
 * @ORM\DiscriminatorMap({
 * "AN" = "AnomalyRegistrar", "MR"="MarineAnomaly",
 * "PL" = "PipelineAnomaly", "PI" = "PipingAnomaly", "ST"="StructureAnomaly", "SE"="StaticEquipmentAnomaly",
 * "RO" = "RepairOrderRegistrar", "TRO" = "TemporaryRepairOrderRegistrar", "FM" = "FabricMaintenanceRegistrar",
 * "AS" = "AssessmentRegistrar", "AC" = "ActionRegistrar",
 * })
 * @JMS\Discriminator(disabled=true)
 * @JMS\ExclusionPolicy("None")
 */
class Registrar
{
    use Tags;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="registrars")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $project;

    /**
     * The index of the status from statusChoices
     *
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     * @JMS\Type("integer")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="ActionOwners", inversedBy="registrars")
     * @ORM\JoinColumn(name="spa_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $spa;

    /**
     * @ORM\ManyToOne(targetEntity="ApprovalOwners", inversedBy="registrars")
     * @ORM\JoinColumn(name="approved_spa_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $approvedspa;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    protected $spaUser;

    /**
     * A number that Starts from 0 and increments by 1 every time the Action is deferred
     *
     * @var double
     *
     * @ORM\Column(name="deferred", type="integer")
     * @JMS\Type("integer")
     */
    protected $deferred = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="deferral_justification", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $deferralJustification;

    /**
     * @var string
     *
     * @ORM\Column(name="deferral_safe_justification", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $deferralSafeJustification;


    /**
     * @ORM\ManyToMany(targetEntity="Files")
     * @ORM\JoinTable(name="RegistrarFiles",
     *      joinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     * @JMS\Exclude
     */
    protected $registrarFiles;

    /**
     * @ORM\OneToMany(targetEntity="Request" , mappedBy="registrar", cascade={"all"}, fetch="EXTRA_LAZY")
     * @JMS\Exclude
     */
    protected $requests;


    /**
     * @var string
     *
     * @ORM\Column(name="deferral_reject_justification", type="text", nullable=true)
     * @JMS\Type("string")
     */
    protected $deferralRejectJustification;

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    static protected $criticalityChoices = [
        0 => 'Low',
        1 => 'Medium',
        2 => 'High'
    ];

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    protected $statusChoices = ['Live', 'Closed', 'Cancelled'];

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    protected $stageChoices = ['new'=>'New', 'request'=>'Pending', 'request-rejected'=>'Rejected'];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("project_id")
     */
    public function getProjectId()
    {
        return ($this->project) ? $this->project->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("spa_id")
     */
    public function getSpaId()
    {
        return ($this->spa) ? $this->spa->getId() : null;
    }

    /**
     * Get deferred
     *
     * @return integer
     */
    public function getDeferred()
    {
        return $this->deferred;
    }

    /**
     * Set deferralJustification
     *
     * @param string $deferralJustification
     * @return Registrar
     */
    public function setDeferralJustification($deferralJustification)
    {
        $this->deferralJustification = $deferralJustification;

        return $this;
    }

    /**
     * Get deferralJustification
     *
     * @return string
     */
    public function getDeferralJustification()
    {
        return $this->deferralJustification;
    }

    /**
     * Set deferred
     *
     * @param integer $deferred
     * @return Registrar
     */
    public function setDeferred($deferred)
    {
        $this->deferred = $deferred;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Registrar
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return User
     */
    public function getSpaUser()
    {
        return $this->spaUser;
    }

    public function setSpaUser(User $spaUser)
    {
        $this->spaUser = $spaUser;
    }

    public static function getCriticalityChoices()
    {
        return self::$criticalityChoices;
    }

    public static function getRiskChoices($numerical=true) {
        return RiskMatrix::getRiskChoices($numerical);
    }

    public function getStatusChoices()
    {
        return $this->statusChoices;
    }

    public function getStageChoices()
    {
        return $this->stageChoices;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return Registrar
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("files")
     */
    public function getVirtualFiles()
    {
        $files = [];
        foreach ($this->getRegistrarFiles() as $file) {
            $files[] = $file;
        }

        return $files;
    }

    public function getIsDue()
    {
        //if today is the next inspection or after that
        $now = new \DateTime('now');
        return ($this->getDueDate() !== null && (int)$this->getDueDate()->format('U') <= (int)$now->format('U'));
    }


    // public function getAnomalyIsDue()
    // {
    //     //if today is the next inspection or after that
    //     $now = new \DateTime('now');
    //     return ($this->getNextInspectionDate() !== null && (int)$this->getNextInspectionDate()->format('U') <= (int)$now->format('U'));
    // }


    public function getCode()
    {
        throw new \Exception("Define Anomaly Code");
    }

    public static function getTagChoices()
    {
        return [
            'new' => [
                'name' => 'new',
                'label' => 'new'
            ], //when posted fresh, still admin didn't approve
            'cancelled' => [
                'name' => 'cancelled',
                'label' => 'cancelled'
            ],
            'closed' => [
                'name' => 'closed',
                'label' => 'closed'
            ],
            'final' => [
                'name' => 'final',
                'label' => 'approved',
            ], //admin approved (whether it is create or update)
            'request' => [
                'name' => 'request',
                'label' => 'pending'
            ], //a request was sent
            'request-rejected' => [
                'name' => 'request-rejected',
                'label' => 'rejected'
            ], //request was rejected
        ]; //a mapping between tags and colors
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->registrarFiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add registrarFiles
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $registrarFiles
     * @return Registrar
     */
    public function addRegistrarFile(\AIE\Bundle\AnomalyBundle\Entity\Files $registrarFiles)
    {
        $this->registrarFiles[] = $registrarFiles;

        return $this;
    }

    /**
     * Remove registrarFiles
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $registrarFiles
     */
    public function removeRegistrarFile(\AIE\Bundle\AnomalyBundle\Entity\Files $registrarFiles)
    {
        $this->registrarFiles->removeElement($registrarFiles);
    }

    /**
     * Get registrarFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegistrarFiles()
    {
        return $this->registrarFiles;
    }

    /**
     * Remove all registrarFiles
     */
    public function removeAllRegistrarFiles()
    {
        foreach ($this->registrarFiles as $file) {
            $this->removeRegistrarFile($file);
        }
    }

    /**
     * Add requests
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $requests
     * @return AnomalyRegistrar
     */
    public function addRequest(\AIE\Bundle\AnomalyBundle\Entity\Request $requests)
    {
        $this->requests[] = $requests;

        return $this;
    }

    /**
     * Remove requests
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $requests
     */
    public function removeRequest(\AIE\Bundle\AnomalyBundle\Entity\Request $requests)
    {
        $this->requests->removeElement($requests);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * Get requests that are approved
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getApprovedRequests(){
        $approvedRequests = new \Doctrine\Common\Collections\ArrayCollection();
        foreach($this->requests as $request){
            if($request->getStatus() == 1)//approved
            {
                $approvedRequests->add($request);
            }
        }
        return $approvedRequests;
    }

    /**
     * Set spa
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $spa
     * @return Registrar
     */
    public function setSpa(\AIE\Bundle\AnomalyBundle\Entity\ActionOwners $spa = null)
    {
        $this->spa = $spa;

        return $this;
    }

    /**
     * Get spa
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\ActionOwners
     */
    public function getSpa()
    {
        return $this->spa;
    }



    /**
     * Set deferralSafeJustification
     *
     * @param string $deferralSafeJustification
     * @return Registrar
     */
    public function setDeferralSafeJustification($deferralSafeJustification)
    {
        $this->deferralSafeJustification = $deferralSafeJustification;

        return $this;
    }

    /**
     * Get deferralSafeJustification
     *
     * @return string
     */
    public function getDeferralSafeJustification()
    {
        return $this->deferralSafeJustification;
    }

    /**
     * Set deferralRejectJustification
     *
     * @param string $deferralSafeJustification
     * @return Registrar
     */
    public function setDeferralRejectJustification($deferralRejectJustification)
    {
        $this->deferralRejectJustification = $deferralRejectJustification;

        return $this;
    }

    /**
     * Get deferralRejectJustification
     *
     * @return string
     */
    public function getDeferralRejectJustification()
    {
        return $this->deferralRejectJustification;
    }

    public function getCriticality(){
        if ($this instanceof AnomalyRegistrar)
            return $this->getCritcality();
        else
            return $this->getAnomaly()->getCriticality();
    }

    /**
     * A function that returns the risk/criticality based on project type
     * @return integer
     */
    public function getRisk()
    {
        $project = $this->getProject();
        if ($project instanceof RiskProject) {
            return $project->getRiskMatrix()->getRisk($this->getConsequence(), $this->getProbability(), true);
        } else {
            return $this->getCriticality();
        }
    }

    public function getConsequence()
    {
        if ($this instanceof AnomalyRegistrar)
            return $this->getConsequence();
        else
            return $this->getAnomaly()->getConsequence();
    }

    public function getProbability()
    {
        if ($this instanceof AnomalyRegistrar)
            return $this->getProbability();
        else
            return $this->getAnomaly()->getProbability();
    }

    /**
     * Set approvedspa
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvedspa
     * @return Registrar
     */
    public function setApprovedspa(\AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $approvedspa = null)
    {
        $this->approvedspa = $approvedspa;

        return $this;
    }

    /**
     * Get approvedspa
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners
     */
    public function getApprovedspa()
    {
        return $this->approvedspa;
    }
}
