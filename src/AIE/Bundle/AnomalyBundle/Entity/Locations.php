<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap( {0 = "Locations", 1 = "Pipelines"} )
 */
class Locations {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="geometry", type="text", nullable=true)
	 */
	private $geometry;

	/**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="locations")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	private $project;

	/**
	 * @ORM\OneToMany(targetEntity="AnomalyRegistrar" , mappedBy="location", cascade={"all"}, fetch="EXTRA_LAZY")
	 */
	private $anomalies;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Locations
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Locations
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set geometry
	 *
	 * @param string $geometry
	 * @return Locations
	 */
	public function setGeometry($geometry) {
		$this->geometry = $geometry;

		return $this;
	}

	/**
	 * Get geometry
	 *
	 * @return string
	 */
	public function getGeometry() {
		return $this->geometry;
	}

	/**
	 * Set project
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
	 * @return Locations
	 */
	public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null) {
		$this->project = $project;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->anomalies = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add anomalies
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
	 * @return Locations
	 */
	public function addAnomalie(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies) {
		$this->anomalies[ ] = $anomalies;

		return $this;
	}

	/**
	 * Remove anomalies
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
	 */
	public function removeAnomalie(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies) {
		$this->anomalies->removeElement($anomalies);
	}

	/**
	 * Get anomalies
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAnomalies() {
		return $this->anomalies;
	}
}