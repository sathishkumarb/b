<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;


/**
 * StructureAnomaly
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ExclusionPolicy("None")
 */
class StructureAnomaly extends AnomalyRegistrar {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="system", type="text")
	 */
	private $system;

	public function getCode(){
		return 'ST';
	}

    /**
     * Set system
     *
     * @param string $system
     * @return StructureAnomaly
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }
}
