<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 25/10/15
 * Time: 04:40
 */

namespace AIE\Bundle\AnomalyBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * ActionMonitor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FabricMaintenanceCriticality {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="fabricMaintenanceCriticality")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	protected $project;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="fm_criticality_code", type="string", length=20)
	 */
	protected $fmCriticalityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="fm_criticality_description", type="string",nullable=true)
     */
    protected $fmCriticalityDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="fm_criticality_color", type="string", length=7)
     */
    protected $fmCriticalityColor;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set action
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return FabricMaintenanceCriticality
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set fmCriticalityCode
     *
     * @param string $fmCriticalityCode
     * @return FabricMaintenanceCriticality
     */
    public function setFMCriticalityCode($fmCriticalityCode)
    {
        $this->fmCriticalityCode = $fmCriticalityCode;

        return $this;
    }

    /**
     * Get fmCriticalityCode
     *
     * @return string
     */
    public function getFMCriticalityCode()
    {
        return $this->fmCriticalityCode;
    }

    /**
     * Set fmCriticalityDescription
     *
     * @param string $fmCriticalityDescription
     * @return FabricMaintenanceCriticality
     */
    public function setFMCriticalityDescription($fmCriticalityDescription)
    {
        $this->fmCriticalityDescription = $fmCriticalityDescription;

        return $this;
    }

    /**
     * Get fmCriticalityDescription
     *
     * @return string
     */
    public function getFMCriticalityDescription()
    {
        return $this->fmCriticalityDescription;
    }

    /**
     * Set fmCriticalityColor
     *
     * @param string $fmCriticalityColor
     * @return FabricMaintenanceCriticality
     */
    public function setFMCriticalityColor($fmCriticalityColor)
    {
        $this->fmCriticalityColor = $fmCriticalityColor;

        return $this;
    }

    /**
     * Get fmCriticalityColor
     *
     * @return string
     */
    public function getFMCriticalityColor()
    {
        return $this->fmCriticalityColor;
    }

}
