<?php
/**
 * User: mokha
 * Date: 3/13/16
 * Time: 1:20 AM
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classifications
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Classifications extends ClassificationsAbstract {}