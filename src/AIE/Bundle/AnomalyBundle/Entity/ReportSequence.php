<?php
/**
 * Created by AIE
 * Date: 25/10/28
 */

namespace AIE\Bundle\AnomalyBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * ReportSequence
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ReportSequence {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="report_name", type="string", length=100, nullable=true)
	 */
	protected $reportName;

    /**
     * @var integer
     *
     * @ORM\Column(name="report_type", type="integer", nullable=true)
     */
    protected $reportType;
    // 1 dashboard
    // 2 planning

    /**
     * @var string
     *
     * @ORM\Column(name="report_category", type="string", length=5, nullable=true)
     */
    protected $reportCategory;
    //master register types (AN,RO,TRO,FM,AS)

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="current_sequence", type="integer")
	 */
	protected $currentSequence;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdat;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="reportSequence")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    public function __construct()
    {
        $this->createdat = new \DateTime("now");
    }

    /**
     * Set reportName
     *
     * @param string $reportName
     * @return ReportSequence
     */
    public function setReportName($reportName)
    {
        $this->reportName = $reportName;

        return $this;
    }

    /**
     * Get reportName
     *
     * @return string 
     */
    public function getReportName()
    {
        return $this->reportName;
    }

    /**
     * Set reportType
     *
     * @param string $reportType
     * @return ReportSequence
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;

        return $this;
    }

    /**
     * Get reportType
     *
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * Set reportCategory
     *
     * @param string $reportCategory
     * @return ReportSequence
     */
    public function setReportCategory($reportCategory)
    {
        $this->reportCategory = $reportCategory;

        return $this;
    }

    /**
     * Get reportCategory
     *
     * @return string
     */
    public function getReportCategory()
    {
        return $this->reportCategory;
    }

    /**
     * Set currentSequence
     *
     * @param integer $currentSequence
     * @return ReportSequence
     */
    public function setCurrentSequence($currentSequence)
    {
        $this->currentSequence = $currentSequence;

        return $this;
    }

    /**
     * Get currentSequence
     *
     * @return integer
     */
    public function getCurrentSequence()
    {
        return $this->currentSequence;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return Report
     */
    public function setCreatedAt(\DateTime $createdat)
    {
        $this->createdat = $createdat;
        return $this;
    }
    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdat;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdat = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return ReportSequence
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects 
     */
    public function getProject()
    {
        return $this->project;
    }
}
