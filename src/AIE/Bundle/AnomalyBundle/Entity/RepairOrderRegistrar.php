<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * RepairOrderRegistrar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Repository\RepairOrderRegistrarRepository")
 * @JMS\ExclusionPolicy("None")
 */
class RepairOrderRegistrar extends ActionRegistrar
{

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="location_details", type="text", nullable=true)
	 */
	protected $locationDetails;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="repair_priority", type="text", nullable=true)
	 */
	protected $repairPriority;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="close_out_date", type="datetime", nullable=true)
	 * @JMS\Type("DateTime")
	 */
	protected $closeOutDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="closeOutJustification", type="text", nullable=true)
	 */
	protected $closeOutJustification;


	public function getCode(){
		return 'RO';
	}

    /**
     * Set description
     *
     * @param string $description
     * @return RepairOrderRegistrar
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set locationDetails
     *
     * @param string $locationDetails
     * @return RepairOrderRegistrar
     */
    public function setLocationDetails($locationDetails)
    {
        $this->locationDetails = $locationDetails;

        return $this;
    }

    /**
     * Get locationDetails
     *
     * @return string 
     */
    public function getLocationDetails()
    {
        return $this->locationDetails;
    }

    /**
     * Set repairPriority
     *
     * @param string $repairPriority
     * @return RepairOrderRegistrar
     */
    public function setRepairPriority($repairPriority)
    {
        $this->repairPriority = $repairPriority;

        return $this;
    }

    /**
     * Get repairPriority
     *
     * @return string 
     */
    public function getRepairPriority()
    {
        return $this->repairPriority;
    }

    /**
     * Set closeOutDate
     *
     * @param \DateTime $closeOutDate
     * @return RepairOrderRegistrar
     */
    public function setCloseOutDate($closeOutDate)
    {
        $this->closeOutDate = $closeOutDate;

        return $this;
    }

    /**
     * Get closeOutDate
     *
     * @return \DateTime 
     */
    public function getCloseOutDate()
    {
        return $this->closeOutDate;
    }

    /**
     * Set closeOutJustification
     *
     * @param string $closeOutJustification
     * @return RepairOrderRegistrar
     */
    public function setCloseOutJustification($closeOutJustification)
    {
        $this->closeOutJustification = $closeOutJustification;

        return $this;
    }

    /**
     * Get closeOutJustification
     *
     * @return string 
     */
    public function getCloseOutJustification()
    {
        return $this->closeOutJustification;
    }
}
