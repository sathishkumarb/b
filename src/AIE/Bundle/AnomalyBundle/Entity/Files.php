<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 20/09/15
 * Time: 04:52
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\VeracityBundle\Component\Files\Files as BaseFiles;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Files
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Files extends BaseFiles {

	/**
	 * @ORM\ManyToOne(targetEntity="AnomalyRegistrar", inversedBy="files")
	 * @ORM\JoinColumn(name="anomaly_id", referencedColumnName="id")
	 * @JMS\Exclude
	 **/
	protected $anomaly;

	public function getUploadDir()
	{
		// get rid of the __DIR__ so it doesn't screw up
		// when displaying uploaded doc/image in the view.
		return $this->anomaly->getProject()->getFolder() . '/files';
	}



    /**
     * Set anomaly
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly
     * @return Files
     */
    public function setAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly = null)
    {
        $this->anomaly = $anomaly;

        return $this;
    }

    /**
     * Get anomaly
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar 
     */
    public function getAnomaly()
    {
        return $this->anomaly;
    }

    public function getServerFilePath($isEnv){
        if($isEnv){
            return '../web/uploads/' . $this->getFilePath();
        }
        return $this->getFilepath();
    }
}
