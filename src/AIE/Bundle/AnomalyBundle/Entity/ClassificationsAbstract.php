<?php
/**
 * User: mokha
 * Date: 3/13/16
 * Time: 1:20 AM
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class ClassificationsAbstract
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_metal_loss", type="boolean")
     */
    protected $isMetalLoss;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return AnomalyClass
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AnomalyClass
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isMetalLoss
     *
     * @param boolean $isMetalLoss
     * @return AnomalyClass
     */
    public function setIsMetalLoss($isMetalLoss)
    {
        $this->isMetalLoss = $isMetalLoss;

        return $this;
    }

    /**
     * Get isMetalLoss
     *
     * @return boolean
     */
    public function getIsMetalLoss()
    {
        return $this->isMetalLoss;
    }
}