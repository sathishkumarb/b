<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 25/10/15
 * Time: 04:40
 */

namespace AIE\Bundle\AnomalyBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * ActionMonitor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ProductionCriticality {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="productionCriticality")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	protected $project;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="production_criticality_code", type="string", length=20)
	 */
	protected $productionCriticalityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="production_criticality_description", type="string",nullable=true)
     */
    protected $productionCriticalityDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="production_criticality_color", type="string", length=7)
     */
    protected $productionCriticalityColor;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return ProductionCriticality
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set productionCriticalityCode
     *
     * @param string $productionCriticalityCode
     * @return ProductionCriticality
     */
    public function setProductionCriticalityCode($productionCriticalityCode)
    {
        $this->productionCriticalityCode = $productionCriticalityCode;

        return $this;
    }

    /**
     * Get productionCriticalityCode
     *
     * @return string
     */
    public function getProductionCriticalityCode()
    {
        return $this->productionCriticalityCode;
    }

    /**
     * Set productionCriticalityDescription
     *
     * @param string $productionCriticalityDescription
     * @return ProductionCriticality
     */
    public function setProductionCriticalityDescription($productionCriticalityDescription)
    {
        $this->productionCriticalityDescription = $productionCriticalityDescription;

        return $this;
    }

    /**
     * Get productionCriticalityDescription
     *
     * @return string
     */
    public function getProductionCriticalityDescription()
    {
        return $this->productionCriticalityDescription;
    }

    /**
     * Set productionCriticalityColor
     *
     * @param string $productionCriticalityColor
     * @return ProductionCriticality
     */
    public function setProductionCriticalityColor($productionCriticalityColor)
    {
        $this->productionCriticalityColor = $productionCriticalityColor;

        return $this;
    }

    /**
     * Get productionCriticalityColor
     *
     * @return string
     */
    public function getProductionCriticalityColor()
    {
        return $this->productionCriticalityColor;
    }

}
