<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;

/**
 * PipelineAnomaly
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ExclusionPolicy("None")
 */
class PipelineAnomaly extends AnomalyRegistrar
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="tolerance", type="decimal", scale=2, nullable=true)
	 */
	private $tolerance;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="smys", type="decimal", scale=2)
	 */
	private $sMYS;

    /**
     * @var double
     *
     * @ORM\Column(name="outer_diameter", type="decimal", scale=2)
     */
    private $outerDiameter;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="smts", type="decimal", scale=2)
	 */
	private $sMTS;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gps_east", type="string", length=255, nullable=true)
	 */
	private $gpsE;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gps_north", type="string", length=255, nullable=true)
	 */
	private $gpsN;


	public function getCode(){
		return 'PL';
	}

    /**
     * Set tolerance
     *
     * @param string $tolerance
     * @return PipelineAnomaly
     */
    public function setTolerance($tolerance)
    {
        $this->tolerance = $tolerance;

        return $this;
    }

    /**
     * Get tolerance
     *
     * @return string 
     */
    public function getTolerance()
    {
        return $this->tolerance;
    }

    /**
     * Set sMYS
     *
     * @param string $sMYS
     * @return PipelineAnomaly
     */
    public function setSMYS($sMYS)
    {
        $this->sMYS = $sMYS;

        return $this;
    }

    /**
     * Get sMYS
     *
     * @return string 
     */
    public function getSMYS()
    {
        return $this->sMYS;
    }

    /**
     * Set sMTS
     *
     * @param string $sMTS
     * @return PipelineAnomaly
     */
    public function setSMTS($sMTS)
    {
        $this->sMTS = $sMTS;

        return $this;
    }

    /**
     * Get sMTS
     *
     * @return string 
     */
    public function getSMTS()
    {
        return $this->sMTS;
    }

    /**
     * Set gpsE
     *
     * @param string $gpsE
     * @return PipelineAnomaly
     */
    public function setGpsE($gpsE)
    {
        $this->gpsE = $gpsE;

        return $this;
    }

    /**
     * Get gpsE
     *
     * @return string 
     */
    public function getGpsE()
    {
        return $this->gpsE;
    }

    /**
     * Set gpsN
     *
     * @param string $gpsN
     * @return PipelineAnomaly
     */
    public function setGpsN($gpsN)
    {
        $this->gpsN = $gpsN;

        return $this;
    }

    /**
     * Get gpsN
     *
     * @return string 
     */
    public function getGpsN()
    {
        return $this->gpsN;
    }

    /**
     * Set outerDiameter
     *
     * @param string $outerDiameter
     * @return PipelineAnomaly
     */
    public function setOuterDiameter($outerDiameter)
    {
        $this->outerDiameter = $outerDiameter;

        return $this;
    }

    /**
     * Get outerDiameter
     *
     * @return string 
     */
    public function getOuterDiameter()
    {
        return $this->outerDiameter;
    }
}
