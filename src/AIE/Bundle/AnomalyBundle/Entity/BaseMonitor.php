<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 25/10/15
 * Time: 04:38
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class BaseMonitor {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="inspection_date", type="datetime")
	 */
	protected $inspectionDate;

	/**
	 *
	 * @var double
	 *
	 * @ORM\Column(name="inspection_frequency", type="decimal", nullable=true, scale=2)
	 */
	protected $inspectionFrequency;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set inspectionDate
	 *
	 * @param \DateTime $inspectionDate
	 * @return Monitor
	 */
	public function setInspectionDate($inspectionDate) {
		$this->inspectionDate = $inspectionDate;

		return $this;
	}

	/**
	 * Get inspectionDate
	 *
	 * @return \DateTime
	 */
	public function getInspectionDate() {
		return $this->inspectionDate;
	}

	/**
	 * Set inspectionFrequency
	 *
	 * @param string $inspectionFrequency
	 * @return AnomalyRegistrar
	 */
	public function setInspectionFrequency($inspectionFrequency) {
		$this->inspectionFrequency = $inspectionFrequency;

		return $this;
	}

	/**
	 * Get inspectionFrequency
	 *
	 * @return string
	 */
	public function getInspectionFrequency() {
		return $this->inspectionFrequency;
	}

}