<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * RepairOrderRegistrar
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Repository\AssessmentRegistrarRepository")
 * @JMS\ExclusionPolicy("None")
 */
class AssessmentRegistrar extends ActionRegistrar
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assessment_due_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $assessmentDueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="assessment_type", type="text", nullable=true)
     */
    protected $assessmentType;

    /**
     * @var string
     *
     * @ORM\Column(name="assessment_code", type="text", nullable=true)
     */
    protected $assessmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="assessment_results", type="text", nullable=true)
     */
    protected $assessmentResult;

    /**
     * @var integer
     *
     * @ORM\Column(name="assessment_buffer_days", type="integer", nullable=true)
     */
    protected $assessmentBufferDays;


    public function getCode()
    {
        return 'AS';
    }

    /**
     * Set assessmentDueDate
     *
     * @param \DateTime $assessmnetDueDate
     * @return AssessmentRegistrar
     */
    public function setAssessmentDueDate($assessmentDueDate)
    {
        $this->assessmentDueDate = $assessmentDueDate;

        return $this;
    }

    /**
     * Get assessmentDueDate
     *
     * @return \DateTime
     */
    public function getAssessmentDueDate()
    {
        return $this->assessmentDueDate;
    }

    /**
     * Set assessmentType
     *
     * @param string $assessmentType
     * @return AssessmentRegistrar
     */
    public function setAssessmentType($assessmentType)
    {
        $this->assessmentType = $assessmentType;

        return $this;
    }

    /**
     * Get assessmentType
     *
     * @return string
     */
    public function getAssessmentType()
    {
        return $this->assessmentType;
    }

    /**
     * Set assessmentCode
     *
     * @param string $assessmentCode
     * @return AssessmentRegistrar
     */
    public function setAssessmentCode($assessmentCode)
    {
        $this->assessmentCode = $assessmentCode;

        return $this;
    }

    /**
     * Get assessmentCode
     *
     * @return string
     */
    public function getAssessmentCode()
    {
        return $this->assessmentCode;
    }

    /**
     * Set assessmentResult
     *
     * @param string $assessmentResult
     * @return AssessmentRegistrar
     */
    public function setAssessmentResult($assessmentResult)
    {
        $this->assessmentResult = $assessmentResult;

        return $this;
    }

    /**
     * Get assessmentResult
     *
     * @return string
     */
    public function getAssessmentResult()
    {
        return $this->assessmentResult;
    }

    /**
     * Set assessmentBufferDays
     *
     * @param integer assessmentBufferDays
     * @return AssessmentRegistrar
     */
    public function setAssessmentBufferDays($assessmentBufferDays)
    {
        $this->assessmentBufferDays = $assessmentBufferDays;

        return $this;
    }

    /**
     * Get assessmentBufferDays
     *
     * @return integer
     */
    public function getAssessmentBufferDays()
    {
        return $this->assessmentBufferDays;
    }
}