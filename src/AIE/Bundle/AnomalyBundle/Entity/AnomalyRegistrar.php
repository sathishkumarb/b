<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\UserBundle\Entity\User;
use AIE\Bundle\VeracityBundle\Component\Core\Tags;
use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AnomalyRegistrar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @JMS\Discriminator(disabled=true)
 * @ORM\HasLifecycleCallbacks()
 * @JMS\ExclusionPolicy("None")
 */
class AnomalyRegistrar extends Registrar
{

    /**
     * @ORM\ManyToOne(targetEntity="Locations", inversedBy="anomalies")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $location;

    /**
     * @var string
     *
     * @ORM\Column(name="defect_description", type="text")
     */
    protected $defectDescription;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="raised_on", type="datetime")
     * @JMS\Type("DateTime")
     */
    protected $raisedOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Insp_Date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $InspDate;

    /**
     * @ORM\ManyToOne(targetEntity="AnomalyThreat", inversedBy="anomalies")
     * @ORM\JoinColumn(name="threat_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $threat;

    /**
     * @ORM\ManyToOne(targetEntity="AnomalyClass", inversedBy="anomalies")
     * @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $classification;

    /**
     * @var integer
     *
     * @ORM\Column(name="production_crititcality_id", type="integer", nullable=true)
     */
    protected $productionCriticality;

    /**
     * @var array
     *
     * @ORM\Column(name="action_required", type="json_array")
     */
    protected $actionRequired;


    /**
     * @var string
     *
     * @ORM\Column(name="inspection_technique", type="text", nullable=true)
     */
    protected $inspectionTechnique; // inspection technique

    /**
     *
     * @var double
     *
     * @ORM\Column(name="inspection_frequency", type="decimal", scale=2, nullable=true)
     */
    protected $inspectionFrequency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inspection_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $inspectionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="inspection_recommendations", type="text", nullable=true)
     */
    protected $inspectionRecommendations;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_inspection_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $nextInspectionDate;

    /**
     * @var double
     *
     * @ORM\Column(name="corrosion_allowance", type="decimal", scale=2, nullable=true)
     */
    protected $corrosionAllowance;

    /**
     * @var string
     *
     * @ORM\Column(name="component", type="text")
     */
    protected $component;

    /**
     * @var string
     *
     * @ORM\Column(name="component_description", type="text", nullable=true)
     */
    protected $componentDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="asset_tag_number", type="text")
     */
    protected $assetTagNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="anomaly_custom_id", type="text")
     */
    protected $anomalyCustomId;

    /**
     * @var double
     *
     * @ORM\Column(name="location_point", type="string", length=20, nullable=true)
     */
    protected $locationPoint;


    /**
     * @var double
     *
     * @ORM\Column(name="nominal_wall_thickness", type="decimal", scale=2, nullable=true)
     */
    protected $nominalWT;


    /**
     * @var double
     *
     * @ORM\Column(name="length", type="decimal", scale=2, nullable=true)
     */
    protected $length;


    /**
     * @var double
     *
     * @ORM\Column(name="remaining_thickness", type="decimal", scale=2, nullable=true)
     */
    protected $remainingWallThickness;

    /**
     * @var double
     *
     * @ORM\Column(name="mawt", type="decimal", scale=2, nullable=true)
     */
    protected $mAWT;

    /**
     * @var string
     *
     * @ORM\Column(name="criticality", type="integer", nullable=true)
     */
    protected $criticality;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_fm_criticality", type="boolean", nullable=true)
     */
    protected $isFabricMaintenanceCriticality = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="fm_crititcality_id", type="integer", nullable=true)
     */
    protected $fabricMaintenanceCriticality;

    /**
     * @var integer
     *
     * @ORM\Column(name="probability", type="integer", nullable=true)
     */
    protected $probability;

    /**
     * @var integer
     *
     * @ORM\Column(name="consequence", type="integer", nullable=true)
     */
    protected $consequence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="close_out_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $closeOutDate;

    /**
     * @var string
     *
     * @ORM\Column(name="close_out_justification", type="text", nullable=true)
     */
    protected $closeOutJustification;


    /**
     * @var integer
     *
     * @ORM\Column(name="buffer_days", type="integer", nullable=true)
     */
    protected $bufferDays;

    /**
     * @ORM\OneToMany(targetEntity="Monitor" , mappedBy="registrar", cascade={"all"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"inspectionDate" = "ASC"})
     * @JMS\Exclude
     */
    protected $monitors;


    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="Files" , mappedBy="anomaly" , cascade={"all"}, fetch="EXTRA_LAZY")
     * @JMS\Exclude
     */
    protected $files;

    /**
     * @ORM\OneToMany(targetEntity="FilesCategories" , mappedBy="anomaly" , cascade={"all"}, fetch="EXTRA_LAZY")
     * @JMS\Exclude
     */
    protected $filesCategories;

    /**
     * @ORM\OneToMany(targetEntity="ActionRegistrar" , mappedBy="anomaly", cascade={"all"}, fetch="EXTRA_LAZY")
     * @JMS\Exclude
     */
    protected $actions;

    /**
     * @ORM\ManyToOne(targetEntity="AnomalyDesignCode", inversedBy="anomalies")
     * @ORM\JoinColumn(name="design_code", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $designCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="defect_orientation", type="time", nullable=true)
     */
    protected $defectOrientation;

    /**
     * @var double
     *
     * @ORM\Column(name="corrosion_rate", type="decimal", scale=2, nullable=true)
     */
    protected $corrosionRate;

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    protected $actionsChoices = [
        0 => 'Anomaly Inspection',
        1 => 'Repair Order',
        2 => 'Temporary Repair Order',
        3 => 'Fabric Maintenance',
        4 => 'Assessment'
    ];

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    protected $actionsAbbreviation = [
        0 => 'AI',
        1 => 'RO',
        2 => 'TRO',
        3 => 'FM',
        4 => 'AS'
    ];

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    static protected $anomalyCategories = [
        'PL' => 'Pipelines',
        'PI' => 'Piping',
        'SE' => 'Static Equipment',
        'ST' => 'Structure',
        'MR' => 'Marine',
    ];

    /**
     *
     * @var double
     *
     * @ORM\Column(name="retiral_value", type="decimal", scale=2, nullable=true)
     */
    protected $retiralValue;

    /**
     *
     * @var double
     *
     * @ORM\Column(name="width", type="decimal", scale=2, nullable=true)
     */
    protected $width;

    /**
     * @ORM\OneToMany(targetEntity="TrackingCriteria" , mappedBy="registrar", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $trackingCriteria;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="text", nullable=true)
     */
    protected $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    protected $recommendations;

    /**
     * @var string
     *
     * @ORM\Column(name="inspection_results", type="text", nullable=true)
     */
    protected $inspectionResults;

    /**
     * @var string
     *
     * @ORM\Column(name="drawing_number", type="text", nullable=true)
     */
    protected $drawingNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="failure_summary", type="text", nullable=true)
     */
    protected $failureSummary;

    /**
     * @var boolean
     *
     * @ORM\Column(name="remediation_require_shutdown", type="boolean", nullable=true)
     */
    protected $remediationRequireShutdown;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->monitors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tempdate = null;
    }

    /**
     * Clone
     */
    public function __clone()
    {
        if ($this->id) {
            //$this->id = null;
        }
    }

    /**
     * Set defectDescription
     *
     * @param string $defectDescription
     * @return AnomalyRegistrar
     */
    public function setDefectDescription($defectDescription)
    {
        $this->defectDescription = $defectDescription;

        return $this;
    }

    /**
     * Get defectDescription
     *
     * @return string
     */
    public function getDefectDescription()
    {
        return $this->defectDescription;
    }

    /**
     * Set raisedOn
     *
     * @param \DateTime $raisedOn
     * @return AnomalyRegistrar
     */
    public function setRaisedOn($raisedOn)
    {
        $this->raisedOn = $raisedOn;

        return $this;
    }

    /**
     * Get raisedOn
     *
     * @return \DateTime
     */
    public function getRaisedOn()
    {
        return $this->raisedOn;
    }

    /**
     * Set actionRequired
     *
     * @param array $actionRequired
     * @return AnomalyRegistrar
     */
    public function setActionRequired($actionRequired)
    {
        $this->actionRequired = $actionRequired;

        return $this;
    }

    /**
     * Get actionRequired
     *
     * @return array
     */
    public function getActionRequired()
    {
        return $this->actionRequired;
    }

    /**
     * Set inspectionFrequency
     *
     * @param string $inspectionFrequency
     * @return AnomalyRegistrar
     */
    public function setInspectionFrequency($inspectionFrequency)
    {
        $this->inspectionFrequency = $inspectionFrequency;

        return $this;
    }

    /**
     * Get inspectionFrequency
     *
     * @return string
     */
    public function getInspectionFrequency()
    {
        return $this->inspectionFrequency;
    }

    /**
     * Set corrosionAllowance
     *
     * @param string $corrosionAllowance
     * @return AnomalyRegistrar
     */
    public function setCorrosionAllowance($corrosionAllowance)
    {
        $this->corrosionAllowance = $corrosionAllowance;

        return $this;
    }

    /**
     * Get corrosionAllowance
     *
     * @return string
     */
    public function getCorrosionAllowance()
    {
        return $this->corrosionAllowance;
    }

    /**
     * Set closeOutDate
     *
     * @param \DateTime $closeOutDate
     * @return AnomalyRegistrar
     */
    public function setCloseOutDate($closeOutDate)
    {
        $this->closeOutDate = $closeOutDate;

        return $this;
    }

    /**
     * Get closeOutDate
     *
     * @return \DateTime
     */
    public function getCloseOutDate()
    {
        return $this->closeOutDate;
    }

    /**
     * Set bufferDays
     *
     * @param integer $bufferDays
     * @return AnomalyRegistrar
     */
    public function setBufferDays($bufferDays)
    {
        $this->bufferDays = $bufferDays;

        return $this;
    }

    /**
     * Get bufferDays
     *
     * @return integer
     */
    public function getBufferDays()
    {
        return $this->bufferDays;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return AnomalyRegistrar
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set threat
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threat
     * @return AnomalyRegistrar
     */
    public function setThreat(\AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat $threat = null)
    {
        $this->threat = $threat;

        return $this;
    }

    /**
     * Get threat
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat
     */
    public function getThreat()
    {
        return $this->threat;
    }

    /**
     * Set classification
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classification
     * @return AnomalyRegistrar
     */
    public function setClassification(\AIE\Bundle\AnomalyBundle\Entity\AnomalyClass $classification = null)
    {
        $this->classification = $classification;

        return $this;
    }

    /**
     * Get classification
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyClass
     */
    public function getClassification()
    {
        return $this->classification;
    }

    /**
     * Add monitors
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Monitor $monitors
     * @return AnomalyRegistrar
     */
    public function addMonitor(\AIE\Bundle\AnomalyBundle\Entity\Monitor $monitors)
    {
        $this->monitors[] = $monitors;

        return $this;
    }

    /**
     * Remove monitors
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Monitor $monitors
     */
    public function removeMonitor(\AIE\Bundle\AnomalyBundle\Entity\Monitor $monitors)
    {
        $this->monitors->removeElement($monitors);
    }

    /**
     * Get monitors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitors()
    {
        return $this->monitors;
    }

    /**
     * Get actions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActions()
    {
        return $this->actions;
    }

    public function getActionsChoices()
    {
        return $this->actionsChoices;
    }

    public function getActionsAbbreviation()
    {
        return $this->actionsAbbreviation;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("location_id")
     */
    public function getLocationId()
    {
        return ($this->location) ? $this->location->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("threat_id")
     */
    public function getThreatId()
    {
        return ($this->threat) ? $this->threat->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("classification_id")
     */
    public function getClassificationId()
    {
        return ($this->classification) ? $this->classification->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("design_code_id")
     */
    public function getDesignCodeId()
    {
        return ($this->designCode) ? $this->designCode->getId() : null;
    }

    /**
     * Set location
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Locations $location
     * @return AnomalyRegistrar
     */
    public function setLocation(\AIE\Bundle\AnomalyBundle\Entity\Locations $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Locations
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $files
     * @return AnomalyRegistrar
     */
    public function addFile(\AIE\Bundle\AnomalyBundle\Entity\Files $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $files
     */
    public function removeFile(\AIE\Bundle\AnomalyBundle\Entity\Files $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setFiles(\AIE\Bundle\AnomalyBundle\Entity\Files $files = null)
    {
        $this->files[] = $files;
        return $this->files;
    }

    /**
     * Add filesCategories
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FilesCategories $filesCategories
     * @return AnomalyRegistrar
     */
    public function addFilesCategory(\AIE\Bundle\AnomalyBundle\Entity\FilesCategories $filesCategories)
    {
        $this->filesCategories[] = $filesCategories;

        return $this;
    }

    /**
     * Remove filesCategories
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\FilesCategories $filesCategories
     */
    public function removeFilesCategory(\AIE\Bundle\AnomalyBundle\Entity\FilesCategories $filesCategories)
    {
        $this->filesCategories->removeElement($filesCategories);
    }

    /**
     * Get filesCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilesCategories()
    {
        return $this->filesCategories;
    }

    /**
     * Add actions
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar $actions
     * @return AnomalyRegistrar
     */
    public function addAction(\AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar $actions)
    {
        $this->actions[] = $actions;

        return $this;
    }

    /**
     * Remove actions
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar $actions
     */
    public function removeAction(\AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar $actions)
    {
        $this->actions->removeElement($actions);
    }

    /**
     * @return string
     */
    public function getFullId()
    {
        return sprintf('%s-%d-%03d', $this->getCode(), $this->getRaisedOn()->format('y'), $this->getId());
    }

    /**
     * Set designCode
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $designCode
     * @return AnomalyRegistrar
     */
    public function setDesignCode(\AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode $designCode = null)
    {
        $this->designCode = $designCode;

        return $this;
    }

    /**
     * Get designCode
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode
     */
    public function getDesignCode()
    {
        return $this->designCode;
    }

    /**
     * Set productionCriticality
     *
     * @param integer $productionCriticality
     * @return AnomalyRegistrar
     */
    public function setProductionCriticality($productionCriticality = null)
    {
        $this->productionCriticality = $productionCriticality;

        return $this;
    }

    /**
     * Get productionCriticality
     *
     * @return integer
     */
    public function getProductionCriticality()
    {
        return $this->productionCriticality;
    }

    /**
     * Set inspectionTechnique
     *
     * @param string $inspectionTechnique
     * @return AnomalyRegistrar
     */
    public function setInspectionTechnique($inspectionTechnique)
    {
        $this->inspectionTechnique = $inspectionTechnique;

        return $this;
    }

    /**
     * Get inspectionTechnique
     *
     * @return string
     */
    public function getInspectionTechnique()
    {
        return $this->inspectionTechnique;
    }

    /**
     * Set inspectionDate
     *
     * @param \DateTime $inspectionDate
     * @return AnomalyRegistrar
     */
    public function setInspectionDate($inspectionDate)
    {
        $this->inspectionDate = $inspectionDate;

        return $this;
    }

    /**
     * Get inspectionDate
     *
     * @return \DateTime
     */
    public function getInspectionDate()
    {
        return $this->inspectionDate;
    }

    /**
     * Set closeOutJustification
     *
     * @param string $closeOutJustification
     * @return AnomalyRegistrar
     */
    public function setCloseOutJustification($closeOutJustification)
    {
        $this->closeOutJustification = $closeOutJustification;

        return $this;
    }

    /**
     * Get closeOutJustification
     *
     * @return string
     */
    public function getCloseOutJustification()
    {
        return $this->closeOutJustification;
    }

    /**
     * Set retiralValue
     *
     * @param string $retiralValue
     * @return AnomalyRegistrar
     */
    public function setRetiralValue($retiralValue)
    {
        $this->retiralValue = $retiralValue;

        return $this;
    }

    /**
     * Get retiralValue
     *
     * @return string
     */
    public function getRetiralValue()
    {
        return $this->retiralValue;
    }


    /**
     * Set summary
     *
     * @param string $summary
     * @return AnomalyRegistrar
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return AnomalyRegistrar
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set inspectionResults
     *
     * @param string $inspectionResults
     * @return AnomalyRegistrar
     */
    public function setInspectionResults($inspectionResults)
    {
        $this->inspectionResults = $inspectionResults;

        return $this;
    }

    /**
     * Get inspectionResults
     *
     * @return string
     */
    public function getInspectionResults()
    {
        return $this->inspectionResults;
    }

    /**
     * Set component
     *
     * @param string $component
     * @return AnomalyRegistrar
     */
    public function setComponent($component)
    {
        $this->component = $component;

        return $this;
    }

    /**
     * Get component
     *
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * Set componentDescription
     *
     * @param string $componentDescription
     * @return AnomalyRegistrar
     */
    public function setComponentDescription($componentDescription)
    {
        $this->componentDescription = $componentDescription;

        return $this;
    }

    /**
     * Get componentDescription
     *
     * @return string
     */
    public function getComponentDescription()
    {
        return $this->componentDescription;
    }

    /**
     * Set assetTagNumber
     *
     * @param string $assetTagNumber
     * @return AnomalyRegistrar
     */
    public function setAssetTagNumber($assetTagNumber)
    {
        $this->assetTagNumber = $assetTagNumber;

        return $this;
    }

    /**
     * Get assetTagNumber
     *
     * @return string
     */
    public function getAssetTagNumber()
    {
        return $this->assetTagNumber;
    }

    /**
     * Set anomalyId
     *
     * @param string $anomalyCustomId
     * @return AnomalyRegistrar
     */
    public function setAnomalyCustomId($anomalyCustomId)
    {
        $this->anomalyCustomId = $anomalyCustomId;

        return $this;
    }

    /**
     * Get anomalyId
     *
     * @return string
     */
    public function getAnomalyCustomId()
    {
        return $this->anomalyCustomId;
    }

    /**
     * Set locationPoint
     *
     * @param string $locationPoint
     * @return AnomalyRegistrar
     */
    public function setLocationPoint($locationPoint)
    {
        $this->locationPoint = $locationPoint;

        return $this;
    }

    /**
     * Get locationPoint
     *
     * @return string
     */
    public function getLocationPoint()
    {
        return $this->locationPoint;
    }

    /**
     * Set nominalWT
     *
     * @param string $nominalWT
     * @return AnomalyRegistrar
     */
    public function setNominalWT($nominalWT)
    {
        $this->nominalWT = $nominalWT;

        return $this;
    }

    /**
     * Get nominalWT
     *
     * @return string
     */
    public function getNominalWT()
    {
        return $this->nominalWT;
    }

    /**
     * Set length
     *
     * @param string $length
     * @return AnomalyRegistrar
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set remainingWallThickness
     *
     * @param string $remainingWallThickness
     * @return AnomalyRegistrar
     */
    public function setRemainingWallThickness($remainingWallThickness)
    {
        $this->remainingWallThickness = $remainingWallThickness;

        return $this;
    }

    /**
     * Get remainingWallThickness
     *
     * @return string
     */
    public function getRemainingWallThickness()
    {
        return $this->remainingWallThickness;
    }

    /**
     * Set mAWT
     *
     * @param string $mAWT
     * @return AnomalyRegistrar
     */
    public function setMAWT($mAWT)
    {
        $this->mAWT = $mAWT;

        return $this;
    }

    /**
     * Get mAWT
     *
     * @return string
     */
    public function getMAWT()
    {
        return $this->mAWT;
    }

    /**
     * Set width
     *
     * @param string $width
     * @return AnomalyRegistrar
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }


    public static function getAnomalyCategories()
    {
        return self::$anomalyCategories;
    }

    /**
     * Set nextInspectionDate
     *
     * @param \DateTime $date
     * @return AnomalyRegistrar
     */
    public function setNextInspectionDate($date)
    {
        $this->nextInspectionDate = $date;

        return $this;
    }

    /**
     * @return bool if the anomaly is deferred and the next inspection date is a deferred date
     */
    public function getIsDeferred()
    {
        return ($this->deferred > 0 && $this->nextInspectionDate !== null);
    }

    public function getNextInspectionDate($force = false)
    {
        if ($this->inspectionDate == null || $this->inspectionFrequency == null)
            return false;

        /*$force = true;
        if ($this->inspectionDate == null || $this->inspectionFrequency == null)
            return false;
        else
            return InspectionHelper::calculateNextInspectionDate(
                $this->inspectionDate,
                $this->inspectionFrequency
            );*/

        // This is mostly not called
        if ($force === true || $this->nextInspectionDate == null) {
            $this->nextInspectionDate = InspectionHelper::calculateNextInspectionDate(
                $this->inspectionDate,
                $this->inspectionFrequency
            );
        }

        return ($this->nextInspectionDate) ? $this->nextInspectionDate : null;

    }

    public function getDueDate()
    {
        if ($this->inspectionDate) {
            return $this->getNextInspectionDate(true);
        } else {
            return null;
        }

        throw new \Exception("Anomaly Register doesn't have inspection date!");
    }

    public function getDate()
    {
        return ($this->nextInspectionDate) ? $this->nextInspectionDate : false;
    }

    public function getNextInspectionDueDate()
    {
        if ($this->inspectionDate == null || $this->inspectionFrequency == null)
            return false;
        else
            return InspectionHelper::calculateNextInspectionDate(
                $this->inspectionDate,
                $this->inspectionFrequency
            );
      
       if ($this->nextInspectionDate == null) {
        
            $this->nextInspectionDate = InspectionHelper::calculateNextInspectionDate(
                $this->inspectionDate,
                $this->inspectionFrequency
            );
        }

        return ($this->nextInspectionDate) ? $this->nextInspectionDate : null;
    }


    /**
     * Set defectOrientation
     *
     * @param \DateTime $defectOrientation
     * @return AnomalyRegistrar
     */
    public function setDefectOrientation($defectOrientation)
    {
        $this->defectOrientation = $defectOrientation;

        return $this;
    }

    /**
     * Get defectOrientation
     *
     * @return \DateTime
     */
    public function getDefectOrientation()
    {
        return $this->defectOrientation;
    }

    /**
     * Set corrosionRate
     *
     * @param string $corrosionRate
     * @return AnomalyRegistrar
     */
    public function setCorrosionRate($corrosionRate)
    {
        $this->corrosionRate = $corrosionRate;

        return $this;
    }

    /**
     * Get corrosionRate
     *
     * @return string
     */
    public function getCorrosionRate()
    {
        return $this->corrosionRate;
    }

    /**
     * Add trackingCriteria
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria $trackingCriteria
     * @return AnomalyRegistrar
     */
    public function addTrackingCriterium(\AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria $trackingCriteria)
    {
        $this->trackingCriteria[] = $trackingCriteria;

        return $this;
    }

    /**
     * Remove trackingCriteria
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria $trackingCriteria
     */
    public function removeTrackingCriterium(\AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria $trackingCriteria)
    {
        $this->trackingCriteria->removeElement($trackingCriteria);
    }

    /**
     * Get trackingCriteria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrackingCriteria()
    {
        return $this->trackingCriteria;
    }

    public function clearTrackingCriterium()
    {
        $this->trackingCriteria = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set drawingNumber
     *
     * @param string $drawingNumber
     * @return AnomalyRegistrar
     */
    public function setDrawingNumber($drawingNumber)
    {
        $this->drawingNumber = $drawingNumber;

        return $this;
    }

    /**
     * Get drawingNumber
     *
     * @return string
     */
    public function getDrawingNumber()
    {
        return $this->drawingNumber;
    }


    /**
     * Set failure_summary
     *
     * @param string $failureSummary
     * @return AnomalyRegistrar
     */
    public function setFailureSummary($failureSummary)
    {
        $this->failureSummary = $failureSummary;

        return $this;
    }

    /**
     * Get failure_summary
     *
     * @return string
     */
    public function getFailureSummary()
    {
        return $this->failureSummary;
    }

    /**
     * Set inspection_recommendations
     *
     * @param string $inspectionRecommendations
     * @return AnomalyRegistrar
     */
    public function setInspectionRecommendations($inspectionRecommendations)
    {
        $this->inspectionRecommendations = $inspectionRecommendations;

        return $this;
    }

    /**
     * Get inspection_recommendations
     *
     * @return string
     */
    public function getInspectionRecommendations()
    {
        return $this->inspectionRecommendations;
    }

    /**
     * Set remediationRequireShutdown
     *
     * @param boolean $remediationRequireShutdown
     * @return AnomalyRegistrar
     */
    public function setRemediationRequireShutdown($remediationRequireShutdown)
    {
        $this->remediationRequireShutdown = $remediationRequireShutdown;

        return $this;
    }

    /**
     * Get remediationRequireShutdown
     *
     * @return boolean
     */
    public function getRemediationRequireShutdown()
    {
        return $this->remediationRequireShutdown;
    }

    /**
     * Set probability
     *
     * @param integer $probability
     * @return AnomalyRegistrar
     */
    public function setProbability($probability)
    {
        $this->probability = $probability;

        return $this;
    }

    /**
     * Get probability
     *
     * @return integer
     */
    public function getProbability()
    {
        return $this->probability;
    }

    /**
     * Set criticality
     *
     * @param string $criticality
     * @return AnomalyRegistrar
     */
    public function setCriticality($criticality)
    {
        $this->criticality = $criticality;

        return $this;
    }

    /**
     * Get criticality
     *
     * @return string
     */
    public function getCriticality()
    {
        return $this->criticality;
    }

    /**
     * Set consequence
     *
     * @param integer $consequence
     * @return AnomalyRegistrar
     */
    public function setConsequence($consequence)
    {
        $this->consequence = $consequence;

        return $this;
    }

    /**
     * Get consequence
     *
     * @return integer
     */
    public function getConsequence()
    {
        return $this->consequence;
    }


    /**
     * A function that returns the risk/criticality based on project type
     * @return integer
     */
    public function getRisk()
    {
        $project = $this->getProject();
        if ($project instanceof RiskProject) {
            return $project->getRiskMatrix()->getRisk($this->getConsequence(), $this->getProbability(), true);
        } else {
            return $this->getCriticality();
        }
    }

    /**
     * Set fabricMaintenanceCriticality
     *
     * @param integer $fabricMaintenanceCriticality
     * @return AnomalyRegistrar
     */
    public function setFabricMaintenanceCriticality($fabricMaintenanceCriticality = null)
    {
        $this->fabricMaintenanceCriticality = $fabricMaintenanceCriticality;

        return $this;
    }

    /**
     * Get fabricMaintenanceCriticality
     *
     * @return integer
     */
    public function getFabricMaintenanceCriticality()
    {
        return $this->fabricMaintenanceCriticality;
    }

    /**
     * Set isFabricMaintenanceCriticality
     *
     * @param boolean $isFabricMaintenanceCriticality
     * @return AnomalyRegistrar
     */
    public function setIsFabricMaintenanceCriticality($isFabricMaintenanceCriticality = null)
    {
        $this->isFabricMaintenanceCriticality = $isFabricMaintenanceCriticality;

        return $this;
    }

    /**
     * Get isFabricMaintenanceCriticality
     *
     * @return boolean
     */
    public function getIsFabricMaintenanceCriticality()
    {
        if ($this->isFabricMaintenanceCriticality == null)
            return false;
        return $this->isFabricMaintenanceCriticality;
    }

    /**
     * Set InspDate
     *
     * @param \DateTime $InspDate
     * @return AnomalyRegistrar
     */
    public function setInspDate($InspDate)
    {
        $this->InspDate = $InspDate;

        return $this;
    }

    /**
     * Get InspDate
     *
     * @return \DateTime
     */
    public function getInspDate()
    {
        return $this->InspDate;
    }
}
