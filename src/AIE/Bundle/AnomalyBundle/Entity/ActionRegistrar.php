<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 10/10/15
 * Time: 20:36
 */

namespace AIE\Bundle\AnomalyBundle\Entity;


use AIE\Bundle\VeracityBundle\Component\Core\Tags;
use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\UserBundle\Entity\User as User;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;


/**
 * ActionRegistrar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @JMS\Discriminator(disabled=true)
 * @JMS\ExclusionPolicy("None")
 */
class ActionRegistrar extends Registrar
{

    /**
     * @ORM\ManyToOne(targetEntity="AnomalyRegistrar", inversedBy="actions")
     * @ORM\JoinColumn(name="anomaly_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    private $anomaly;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    protected $recommendations;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiryDate", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
    protected $expiryDate;

    /**
     * @var array
     *
     * @JMS\Type("array")
     * @JMS\Exclude
     * @JMS\ReadOnly
     */
    static protected $actionCategories = [
        'RO' => 'Repair Order',
        'TRO' => 'Temporary Repair Order',
        'FM' => 'Fabric Maintenance',
        'AS' => 'Assessment'
    ];


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getLocation(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getLocation();
        }
        return null;
    }

    public function setLocation($location){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setLocation($location);
        }
        return $this;
    }

    public function getAssetTagNumber(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getAssetTagNumber();
        }
        return null;

    }

    public function setAssetTagNumber($assetTagNumber){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setAssetTagNumber($assetTagNumber);
        }
        return $this;
    }

    public function getAnomalyCustomId(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getAnomalyCustomId();
        }
        return null;
    }

    public function setAnomalyCustomId($anomalyCustomId){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setAnomalyCustomId($anomalyCustomId);
        }
        return $this;
    }

    public function getThreat(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getThreat();
        }
        return null;
    }

    public function setThreat($threat){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setThreat($threat);
        }
        return $this;
    }

    public function getClassification(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getClassification();
        }
        return null;
    }

    public function setClassification($classification){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setClassification($classification);
        }
        return $this;
    }

    public function getComponent(){
        if($this->getAnomaly()){
            return $this->getAnomaly()->getComponent();
        }
        return null;
    }

    public function setComponent($component){
        if($this->getAnomaly()){
            return $this->getAnomaly()->setComments($component);
        }
        return $this;
    }

    public static function getActionCategories()
    {
        return self::$actionCategories;
    }

    /**
     * Set anomaly
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly
     * @return ActionRegistrar
     */
    public function setAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomaly = null)
    {
        $this->anomaly = $anomaly;

        return $this;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return ActionRegistrar
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }


    /**
     * Get anomaly
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar
     */
    public function getAnomaly()
    {
        return $this->anomaly;
    }


    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("anomaly_id")
     */
    public function getAnomalyId()
    {
        return ($this->anomaly) ? $this->anomaly->getId() : null;
    }

    /**
     * @return string
     */
    public function getFullId()
    {
        $anomaly = $this->getAnomaly();

        return sprintf(
            '%s-%s-%d-%03d',
            $this->getCode(),
            $anomaly->getCode(),
            $anomaly->getRaisedOn()->format('y'),
            $this->getId()
        );
    }

    public function getDueDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     * @return ActionRegistrar
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }
}
