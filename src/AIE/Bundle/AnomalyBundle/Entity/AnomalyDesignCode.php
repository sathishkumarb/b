<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnomalyDesignCode
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AnomalyDesignCode extends  DesignCodeAbstract
{

    /**
     * @ORM\OneToMany(targetEntity="AnomalyRegistrar" , mappedBy="designCode", cascade={"persist", "merge", "detach"}, fetch="EXTRA_LAZY")
     */
    protected $anomalies;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="codes")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * */
    protected $project;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anomalies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     * @return AnomalyDesignCode
     */
    public function addAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies[] = $anomalies;

        return $this;
    }

    /**
     * Remove anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     */
    public function removeAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies->removeElement($anomalies);
    }

    /**
     * Get anomalies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnomalies()
    {
        return $this->anomalies;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return AnomalyDesignCode
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }
}
