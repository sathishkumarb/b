<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * TemporaryRepairOrderRegister
 *
 * @ORM\Table()
 * @ORM\Entity
 */

class TemporaryRepairOrderRegistrar extends ActionRegistrar {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="repair_type", type="text", nullable=true)
	 */
	protected $repairType;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="design_life", type="decimal", scale=2, nullable=true)
	 */
	protected $designLife;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="moc_number", type="text", nullable=true)
	 */
	protected $mocNumber;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="installation_date", type="datetime", nullable=true)
	 * @JMS\Type("DateTime")
	 */
	protected $installationDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inspection_technique", type="text", nullable=true)
	 */
	protected $inspectionTechnique; // inspection technique

	/**
	 *
	 * @var double
	 *
	 * @ORM\Column(name="inspection_frequency", type="decimal", scale=2, nullable=true)
	 */
	protected $inspectionFrequency;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="inspection_date", type="datetime", nullable=true)
	 * @JMS\Type("DateTime")
	 */
	protected $inspectionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_inspection_date", type="datetime", nullable=true)
     * @JMS\Type("DateTime")
     */
	protected $nextInspectionDate;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="inspection_results", type="text", nullable=true)
	 */
	protected $inspectionResults;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="buffer_days", type="integer", nullable=true)
	 */
	protected $bufferDays;

	/**
	 * @ORM\OneToMany(targetEntity="ActionMonitor", mappedBy="action", cascade={"all"}, fetch="EXTRA_LAZY")
	 * @JMS\Exclude
	 */
	private $monitors;

	public function getCode() {
		return 'TRO';
	}

	public function getDueDate() {
		if ($this->installationDate != null) {
			return $this->getNextInspectionDate(true);
		}

		return $this->expiryDate;

	}

	/**
	 * Set repairType
	 *
	 * @param string $repairType
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setRepairType($repairType) {
		$this->repairType = $repairType;

		return $this;
	}

	/**
	 * Get repairType
	 *
	 * @return string
	 */
	public function getRepairType() {
		return $this->repairType;
	}

	/**
	 * Set designLife
	 *
	 * @param string $designLife
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setDesignLife($designLife) {
		$this->designLife = $designLife;

		return $this;
	}

	/**
	 * Get designLife
	 *
	 * @return string
	 */
	public function getDesignLife() {
		return $this->designLife;
	}

	/**
	 * Set mocNumber
	 *
	 * @param string $mocNumber
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setMocNumber($mocNumber) {
		$this->mocNumber = $mocNumber;

		return $this;
	}

	/**
	 * Get mocNumber
	 *
	 * @return string
	 */
	public function getMocNumber() {
		return $this->mocNumber;
	}

	/**
	 * Set installationDate
	 *
	 * @param \DateTime $installationDate
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setInstallationDate($installationDate) {
		$this->installationDate = $installationDate;

		return $this;
	}

	/**
	 * Get installationDate
	 *
	 * @return \DateTime
	 */
	public function getInstallationDate() {
		return $this->installationDate;
	}

	/**
	 * Set inspectionTechnique
	 *
	 * @param string $inspectionTechnique
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setInspectionTechnique($inspectionTechnique) {
		$this->inspectionTechnique = $inspectionTechnique;

		return $this;
	}

	/**
	 * Get inspectionTechnique
	 *
	 * @return string
	 */
	public function getInspectionTechnique() {
		return $this->inspectionTechnique;
	}

	/**
	 * Set inspectionFrequency
	 *
	 * @param string $inspectionFrequency
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setInspectionFrequency($inspectionFrequency) {
		$this->inspectionFrequency = $inspectionFrequency;

		return $this;
	}

	/**
	 * Get inspectionFrequency
	 *
	 * @return string
	 */
	public function getInspectionFrequency() {
		return $this->inspectionFrequency;
	}

	/**
	 * Set inspectionDate
	 *
	 * @param \DateTime $inspectionDate
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setInspectionDate($inspectionDate) {
		$this->inspectionDate = $inspectionDate;

		return $this;
	}

	/**
	 * Get inspectionDate
	 *
	 * @return \DateTime
	 */
	public function getInspectionDate() {
		return $this->inspectionDate;
	}

	/**
	 * Set inspectionResults
	 *
	 * @param string $inspectionResults
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setInspectionResults($inspectionResults) {
		$this->inspectionResults = $inspectionResults;

		return $this;
	}

	/**
	 * Get inspectionResults
	 *
	 * @return string
	 */
	public function getInspectionResults() {
		return $this->inspectionResults;
	}

	/**
	 * Set bufferDays
	 *
	 * @param integer $bufferDays
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function setBufferDays($bufferDays) {
		$this->bufferDays = $bufferDays;

		return $this;
	}

	/**
	 * Get bufferDays
	 *
	 * @return integer
	 */
	public function getBufferDays() {
		return $this->bufferDays;
	}

	/**
	 * Add monitors
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\ActionMonitor $monitors
	 * @return TemporaryRepairOrderRegistrar
	 */
	public function addMonitor(\AIE\Bundle\AnomalyBundle\Entity\ActionMonitor $monitors) {
		$this->monitors[ ] = $monitors;

		return $this;
	}

	/**
	 * Remove monitors
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\ActionMonitor $monitors
	 */
	public function removeMonitor(\AIE\Bundle\AnomalyBundle\Entity\ActionMonitor $monitors) {
		$this->monitors->removeElement($monitors);
	}

	/**
	 * Get monitors
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getMonitors() {
		return $this->monitors;
	}

    /**
     * @return bool if the anomaly is deferred and the next inspection date is a deferred date
     */
    public function getIsDeferred()
    {
        return ($this->deferred > 0 && $this->nextInspectionDate !== null);
    }

    /**
     * Set nextInspectionDate
     *
     * @param \DateTime $date
     * @return TemporaryRepairOrderRegistrar
     */
    public function setNextInspectionDate($date) {
        $this->nextInspectionDate = $date;

        return $this;
    }

	public function getNextInspectionDate($force = false) {
		if ($force === true || $this->nextInspectionDate == null) {
			$this->nextInspectionDate = InspectionHelper::calculateNextInspectionDate($this->inspectionDate, $this->inspectionFrequency);
		}

		return $this->nextInspectionDate;
	}


	public function getNextInspectionDueDate()
    {
       if ($this->nextInspectionDate != null) {
        
            $this->nextInspectionDate = InspectionHelper::calculateNextInspectionDate(
                $this->nextInspectionDate,
                $this->inspectionFrequency
            );
        }

        return ($this->nextInspectionDate) ? $this->nextInspectionDate : null;
    }

}
