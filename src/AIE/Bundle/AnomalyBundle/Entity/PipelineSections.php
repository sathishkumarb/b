<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 06/09/15
 * Time: 01:05
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PipelineSections
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PipelineSections
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pse")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     */
    private $pipeline;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="PipelineSectionSegment", mappedBy="section" , cascade={"all"}, fetch="EXTRA_LAZY")
     */
    private $segments;


    /**
     * @var double
     *
     * @ORM\Column(name="outer_diameter", type="decimal", scale=2, nullable=true)
     */
    private $outerDiameter;

    /**
     * @var double
     *
     * @ORM\Column(name="nominal_wall_thickness", type="decimal", scale=2, nullable=true)
     */
    private $nominalWT;

    /**
     * @var double
     *
     * @ORM\Column(name="material", type="text", nullable=true)
     */
    private $material;

    /**
     * @var double
     *
     * @ORM\Column(name="min_strength", type="decimal", scale=2, nullable=true)
     */
    private $minStrength;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PipelineSections
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Pipelines $pipeline
     * @return PipelineSections
     */
    public function setPipeline(\AIE\Bundle\AnomalyBundle\Entity\Pipelines $pipeline = null)
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Pipelines
     */
    public function getPipeline()
    {
        return $this->pipeline;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->segments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add segments
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\PipelineSectionSegment $segments
     * @return PipelineSections
     */
    public function addSegment(\AIE\Bundle\AnomalyBundle\Entity\PipelineSectionSegment $segments)
    {
        $this->segments[] = $segments;

        return $this;
    }

    /**
     * Remove segments
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\PipelineSectionSegment $segments
     */
    public function removeSegment(\AIE\Bundle\AnomalyBundle\Entity\PipelineSectionSegment $segments)
    {
        $this->segments->removeElement($segments);
    }

    /**
     * Get segments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegments()
    {
        return $this->segments;
    }


    /**
     * Set material
     *
     * @param string $material
     * @return PipelineSections
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string 
     */
    public function getMaterial()
    {
        return $this->material;
    }



    /**
     * Set outerDiameter
     *
     * @param string $outerDiameter
     * @return PipelineSections
     */
    public function setOuterDiameter($outerDiameter)
    {
        $this->outerDiameter = $outerDiameter;

        return $this;
    }

    /**
     * Get outerDiameter
     *
     * @return string 
     */
    public function getOuterDiameter()
    {
        return $this->outerDiameter;
    }

    /**
     * Set nominalWT
     *
     * @param string $nominalWT
     * @return PipelineSections
     */
    public function setNominalWT($nominalWT)
    {
        $this->nominalWT = $nominalWT;

        return $this;
    }

    /**
     * Get nominalWT
     *
     * @return string 
     */
    public function getNominalWT()
    {
        return $this->nominalWT;
    }

    /**
     * Set minStrength
     *
     * @param string $minStrength
     * @return PipelineSections
     */
    public function setMinStrength($minStrength)
    {
        $this->minStrength = $minStrength;

        return $this;
    }

    /**
     * Get minStrength
     *
     * @return string 
     */
    public function getMinStrength()
    {
        return $this->minStrength;
    }
}
