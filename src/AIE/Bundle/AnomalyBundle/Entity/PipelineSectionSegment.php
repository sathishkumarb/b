<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PipelineSectionSegment
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PipelineSectionSegment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineSections", inversedBy="segments")
     * @ORM\JoinColumn(name="pipeline_section_id", referencedColumnName="id")
     */
    private $section;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", scale=2)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", scale=2)
     */
    private $y;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set x
     *
     * @param float $x
     * @return PipelineSectionSegment
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param float $y
     * @return PipelineSectionSegment
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set section
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\PipelineSections $section
     * @return PipelineSectionSegment
     */
    public function setSection(\AIE\Bundle\AnomalyBundle\Entity\PipelineSections $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\PipelineSections 
     */
    public function getSection()
    {
        return $this->section;
    }
}
