<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 16:15
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsequencesCategories
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ConsequencesCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @ORM\OneToMany(targetEntity="ProjectConsequences" , mappedBy="consequence" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pc;


    /**
     * @var string
     *
     * @ORM\Column(name="desc_1", type="text", nullable=true)
     */
    private $desc_1;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_2", type="text", nullable=true)
     */
    private $desc_2;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_3", type="text", nullable=true)
     */
    private $desc_3;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_4", type="text", nullable=true)
     */
    private $desc_4;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_5", type="text", nullable=true)
     */
    private $desc_5;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ConsequencesCategories
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ConsequencesCategories
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pc
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $pc
     * @return ConsequencesCategories
     */
    public function addPc(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $pc)
    {
        $this->pc[] = $pc;
    
        return $this;
    }

    /**
     * Remove pc
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $pc
     */
    public function removePc(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $pc)
    {
        $this->pc->removeElement($pc);
    }

    /**
     * Get pc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPc()
    {
        return $this->pc;
    }

    /**
     * Set desc_1
     *
     * @param string $desc1
     * @return ConsequencesCategories
     */
    public function setDesc1($desc1)
    {
        $this->desc_1 = $desc1;
    
        return $this;
    }

    /**
     * Get desc_1
     *
     * @return string 
     */
    public function getDesc1()
    {
        return $this->desc_1;
    }

    /**
     * Set desc_2
     *
     * @param string $desc2
     * @return ConsequencesCategories
     */
    public function setDesc2($desc2)
    {
        $this->desc_2 = $desc2;
    
        return $this;
    }

    /**
     * Get desc_2
     *
     * @return string 
     */
    public function getDesc2()
    {
        return $this->desc_2;
    }

    /**
     * Set desc_3
     *
     * @param string $desc3
     * @return ConsequencesCategories
     */
    public function setDesc3($desc3)
    {
        $this->desc_3 = $desc3;
    
        return $this;
    }

    /**
     * Get desc_3
     *
     * @return string 
     */
    public function getDesc3()
    {
        return $this->desc_3;
    }

    /**
     * Set desc_4
     *
     * @param string $desc4
     * @return ConsequencesCategories
     */
    public function setDesc4($desc4)
    {
        $this->desc_4 = $desc4;
    
        return $this;
    }

    /**
     * Get desc_4
     *
     * @return string 
     */
    public function getDesc4()
    {
        return $this->desc_4;
    }

    /**
     * Set desc_5
     *
     * @param string $desc5
     * @return ConsequencesCategories
     */
    public function setDesc5($desc5)
    {
        $this->desc_5 = $desc5;
    
        return $this;
    }

    /**
     * Get desc_5
     *
     * @return string 
     */
    public function getDesc5()
    {
        return $this->desc_5;
    }
}