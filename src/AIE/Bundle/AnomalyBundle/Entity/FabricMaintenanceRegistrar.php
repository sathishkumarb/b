<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * FabricMaintenanceRegistrar
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Repository\FabricMaintenanceRegistrarRepository")
 */
class FabricMaintenanceRegistrar extends ActionRegistrar {


    /**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="immediate_repair", type="boolean", nullable=true)
	 */
	protected $immediateRepair;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="long_isolation", type="boolean", nullable=true)
	 */
	protected $longIsolation;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="in_service", type="decimal", scale=2, nullable=true)
	 */
	protected $inService;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="completion_date", type="datetime", nullable=true)
	 * @JMS\Type("DateTime")
	 */
	protected $completionDate;

	public function getCode() {
		return 'FM';
	}

    /**
     * Set description
     *
     * @param string $description
     * @return FabricMaintenanceRegistrar
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set immediateRepair
     *
     * @param boolean $immediateRepair
     * @return FabricMaintenanceRegistrar
     */
    public function setImmediateRepair($immediateRepair)
    {
        $this->immediateRepair = $immediateRepair;

        return $this;
    }

    /**
     * Get immediateRepair
     *
     * @return boolean 
     */
    public function getImmediateRepair()
    {
        return $this->immediateRepair;
    }

    /**
     * Set longIsolation
     *
     * @param boolean $longIsolation
     * @return FabricMaintenanceRegistrar
     */
    public function setLongIsolation($longIsolation)
    {
        $this->longIsolation = $longIsolation;

        return $this;
    }

    /**
     * Get longIsolation
     *
     * @return boolean 
     */
    public function getLongIsolation()
    {
        return $this->longIsolation;
    }

    /**
     * Set inService
     *
     * @param string $inService
     * @return FabricMaintenanceRegistrar
     */
    public function setInService($inService)
    {
        $this->inService = $inService;

        return $this;
    }

    /**
     * Get inService
     *
     * @return string 
     */
    public function getInService()
    {
        return $this->inService;
    }

    /**
     * Set completionDate
     *
     * @param \DateTime $completionDate
     * @return FabricMaintenanceRegistrar
     */
    public function setCompletionDate($completionDate)
    {
        $this->completionDate = $completionDate;

        return $this;
    }

    /**
     * Get completionDate
     *
     * @return \DateTime 
     */
    public function getCompletionDate()
    {
        return $this->completionDate;
    }

    /**
     * @return bool if the anomaly is deferred and the completion date is a deferred date
     */
    public function getIsDeferred()
    {
        return ($this->deferred > 0 && $this->completionDate !== null);
    }
}
