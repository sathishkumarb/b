<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Deferrals
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Deferrals {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface", cascade={"persist"})
	 * @ORM\JoinColumn(name="action_owner_id", referencedColumnName="id")
	 * */
	private $actionOwner;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="notify_on", type="integer")
	 */
	private $notifyOn;


	/**
	 * @var string
	 * @ORM\Column(name="anomaly_category", type="string", length=2)
	 */
	private $anomalyCategory;

    ///**
    // * @var string
    // * @ORM\Column(name="action_category", type="string", length=3)
    // */
    //private $actionCategory;

    /**
     * @var integer
     * @ORM\Column(name="approval_level", type="integer")
     */
    private $approvalLevel;

	/**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="deferrals")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	private $project;

    /**
     * @ORM\OneToMany(targetEntity="Request", mappedBy="deferredBy" , cascade={"persist", "merge", "detach", "refresh"})
     */
    protected $answers;

    ///**
    // * @var array
    // *
    // * @JMS\Type("array")
    // * @JMS\Exclude
    // * @JMS\ReadOnly
     //*/
    //static protected $actionsChoices = ['AN' => 'Anomaly', 'AC' => 'All Actions'];

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set actionOwner
	 *
	 * @param integer $actionOwner
	 * @return Deferrals
	 */
	public function setActionOwner($actionOwner) {
		$this->actionOwner = $actionOwner;

		return $this;
	}

	/**
	 * Get actionOwner
	 *
	 * @return integer
	 */
	public function getActionOwner() {
		return $this->actionOwner;
	}

	/**
	 * Set notifyOn
	 *
	 * @param integer $notifyOn
	 * @return Deferrals
	 */
	public function setNotifyOn($notifyOn) {
		$this->notifyOn = $notifyOn;

		return $this;
	}

	/**
	 * Get notifyOn
	 *
	 * @return integer
	 */
	public function getNotifyOn() {
		return $this->notifyOn;
	}

	/**
	 * Set project
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
	 * @return Deferrals
	 */
	public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null) {
		$this->project = $project;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Set anomalyCategory
	 *
	 * @param string $anomalyCategory
	 * @return Deferrals
	 */
	public function setAnomalyCategory($anomalyCategory) {
		$this->anomalyCategory = $anomalyCategory;

		return $this;
	}

	/**
	 * Get anomalyCategory
	 *
	 * @return string
	 */
	public function getAnomalyCategory() {
		return $this->anomalyCategory;
	}

    // /**
    //  * Set actionCategory
    //  *
    //  * @param string $actionCategory
    //  * @return Deferrals
    //  */
    // public function setActionCategory($actionCategory)
    // {
    //     $this->actionCategory = $actionCategory;

    //     return $this;
    // }

    // /**
    //  * Get actionCategory
    //  *
    //  * @return string 
    //  */
    // public function getActionCategory()
    // {
    //     return $this->actionCategory;
    // }


    // public static function getActionsChoices(){
    //     return ActionRegistrar::getActionCategories() + self::$actionsChoices;
    // }

    public function setUser($user) {
		$this->user = $user->getId();
		$this->user = $user;

		return $this;
	}

	public function getUser() {
		return $this->user;
	}

    /**
     * Set approvalLevel
     *
     * @param integer $approvalLevel
     * @return Deferrals
     */
    public function setApprovalLevel($approvalLevel) {
        $this->approvalLevel = $approvalLevel;

        return $this;
    }

    /**
     * Get approvalLevel
     *
     * @return integer
     */
    public function getApprovalLevel() {
        return $this->approvalLevel;
    }

}
