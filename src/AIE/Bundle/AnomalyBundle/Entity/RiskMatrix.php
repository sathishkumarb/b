<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\VeracityBundle\Component\Risk\RiskMatrix as RMBase;

/**
 * RiskMatrix
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RiskMatrix extends RMBase {}