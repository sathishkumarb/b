<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;


/**
 * MarineAnomaly
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AIE\Bundle\AnomalyBundle\Entity\RegistrarRepository")
 * @ExclusionPolicy("None")
 */
class MarineAnomaly extends AnomalyRegistrar {

	/**
	 * @var string
	 *
	 * @ORM\Column(name="system", type="text")
	 */
	private $system;

	public function getCode(){
		return 'MR';
	}

    /**
     * Set system
     *
     * @param string $system
     * @return MarineAnomaly
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return string 
     */
    public function getSystem()
    {
        return $this->system;
    }
}
