<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 25/10/15
 * Time: 04:40
 */

namespace AIE\Bundle\AnomalyBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * ActionMonitor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ActionMonitor extends BaseMonitor {
	/**
	 * @ORM\ManyToOne(targetEntity="TemporaryRepairOrderRegistrar", inversedBy="monitors")
	 * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
	 * */
	protected $action;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="inspection_results", type="text", nullable=true)
	 */
	protected $inspectionResults;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="repair_type", type="string", length=255)
	 */
	protected $repairType;

    /**
     * Set action
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar $action
     * @return ActionMonitor
     */
    public function setAction(\AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set inspectionResults
     *
     * @param string $inspectionResults
     * @return ActionMonitor
     */
    public function setInspectionResults($inspectionResults)
    {
        $this->inspectionResults = $inspectionResults;

        return $this;
    }

    /**
     * Get inspectionResults
     *
     * @return string 
     */
    public function getInspectionResults()
    {
        return $this->inspectionResults;
    }

    /**
     * Set repairType
     *
     * @param string $repairType
     * @return ActionMonitor
     */
    public function setRepairType($repairType)
    {
        $this->repairType = $repairType;

        return $this;
    }

    /**
     * Get repairType
     *
     * @return string 
     */
    public function getRepairType()
    {
        return $this->repairType;
    }
}
