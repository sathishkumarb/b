<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ApprovalOwners
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ApprovalOwners {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\UserEntityInterface", cascade={"persist"})
	 * @ORM\JoinColumn(name="action_owner_id", referencedColumnName="id")
	 */
	private $actionOwner;

	/**
	 * @var string
	 * @ORM\Column(name="anomaly_category", type="string", length=2)
	 */
	private $anomalyCategory;

    /**
     * @var integer
     * @ORM\Column(name="approval_level", type="integer")
     */
    private $approvalLevel;

    ///**
    // * @var string
    // * @ORM\Column(name="action_category", type="string", length=3)
    // */
    //private $actionCategory;

	/**
	 * @ORM\ManyToOne(targetEntity="Projects", inversedBy="approvalOwners")
	 * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
	 * */
	private $project;

    /**
     * @ORM\OneToMany(targetEntity="Request", mappedBy="answeredBy" , cascade={"persist", "merge", "detach", "refresh"})
     */
    protected $answers;

    ///**
    // * @var array
    // *
    // * @JMS\Type("array")
    // * @JMS\Exclude
    // * @JMS\ReadOnly
    // */
    //static protected $actionsChoices = ['AN' => 'Anomaly', 'AC' => 'All Actions'];

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set actionOwner
	 *
	 * @param integer $actionOwner
	 * @return ApprovalOwners
	 */
	public function setActionOwner($actionOwner) {
		$this->actionOwner = $actionOwner;

		return $this;
	}

	/**
	 * Get actionOwner
	 *
	 * @return integer
	 */
	public function getActionOwner() {
		return $this->actionOwner;
	}


	/**
	 * Set project
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
	 * @return ApprovalOwners
	 */
	public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null) {
		$this->project = $project;

		return $this;
	}

	/**
	 * Get project
	 *
	 * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
	 */
	public function getProject() {
		return $this->project;
	}

	/**
	 * Set anomalyCategory
	 *
	 * @param string $anomalyCategory
	 * @return ApprovalOwners
	 */
	public function setAnomalyCategory($anomalyCategory) {
		$this->anomalyCategory = $anomalyCategory;

		return $this;
	}

	/**
	 * Get anomalyCategory
	 *
	 * @return string
	 */
	public function getAnomalyCategory() {
		return $this->anomalyCategory;
	}


    /**
     * Set approvalLevel
     *
     * @param integer $approvalLevel
     * @return ApprovalOwners
     */
    public function setApprovalLevel($approvalLevel) {
        $this->approvalLevel = $approvalLevel;

        return $this;
    }

    /**
     * Get approvalLevel
     *
     * @return integer
     */
    public function getApprovalLevel() {
        return $this->approvalLevel;
    }

    // /**
    //  * Set actionCategory
    //  *
    //  * @param string $actionCategory
    //  * @return ApprovalOwners
    //  */
    // public function setActionCategory($actionCategory)
    // {
    //     $this->actionCategory = $actionCategory;

    //     return $this;
    // }

    // /**
    //  * Get actionCategory
    //  *
    //  * @return string 
    //  */
    // public function getActionCategory()
    // {
    //     return $this->actionCategory;
    // }


    // public static function getActionsChoices(){
    //     return ActionRegistrar::getActionCategories() + self::$actionsChoices;
    // }


    public function setUser($user) {
		//$this->user = $user->getId();
		$this->user = $user;

		return $this;
	}

	public function getUser() {
		return $this->user;
	}

    /**
     * Add answers
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $approvals
     * @return ActionOwners
     */
    public function addApproval(\AIE\Bundle\AnomalyBundle\Entity\Request $approvals)
    {
        $this->answers[] = $approvals;

        return $this;
    }

    /**
     * Remove answers
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Request $approvals
     */
    public function removeApproval(\AIE\Bundle\AnomalyBundle\Entity\Request $approvals)
    {
        $this->answers->removeElement($approvals);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

}
