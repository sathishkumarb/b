<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 16:15
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RiskProject
 *
 * @ORM\Table()
 * @ORM\Entity
 **/
class RiskProject extends Projects {
    /**
     * @ORM\OneToOne(targetEntity="RiskMatrix", cascade={"all"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="risk_matrix_id", referencedColumnName="id")
     */
    protected $riskMatrix;

    /**
     * @ORM\OneToMany(targetEntity="ProjectConsequences" , mappedBy="project" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $consequences;

    /**
     * Set riskMatrix
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\RiskMatrix $riskMatrix
     * @return Projects
     */
    public function setRiskMatrix(\AIE\Bundle\AnomalyBundle\Entity\RiskMatrix $riskMatrix = null)
    {
        $this->riskMatrix = $riskMatrix;

        return $this;
    }

    /**
     * Get riskMatrix
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\RiskMatrix
     */
    public function getRiskMatrix()
    {
        return $this->riskMatrix;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->consequences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add consequences
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences
     * @return RiskProject
     */
    public function addConsequence(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences)
    {
        $this->consequences[] = $consequences;
    
        return $this;
    }

    /**
     * Remove consequences
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences
     */
    public function removeConsequence(\AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences $consequences)
    {
        $this->consequences->removeElement($consequences);
    }

    /**
     * Get consequences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsequences()
    {
        return $this->consequences;
    }
}