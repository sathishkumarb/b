<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CriticalityRanks
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CriticalityRanks
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CriticalityProject", inversedBy="ranks")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * */
    protected $project;

    /**
     * @var string
     *
     * @ORM\Column(name="vh_description", type="text", nullable=true)
     */
    private $vhDescription = "Immediate on-site action is required to eliminate the anomaly.";

    /**
     * @var string
     *
     * @ORM\Column(name="vh_action", type="text", nullable=true)
     */
    private $vhAction = "An anomaly has surpassed pre-determined acceptance criteria (such as leak, wall thickness below cut off thickness, cracks etc.) usually associated with design code limits and its continued operation poses and integrity risk. Immediate action is required to mitigate its condition.";

    /**
     * @var string
     *
     * @ORM\Column(name="h_description", type="text", nullable=true)
     */
    private $hDescription = "The anomaly has surpassed pre-determined acceptance criteria usually associated with design code limits and its continued operation poses and integrity risk. Immediate action is required to mitigate its condition.";

    /**
     * @var string
     *
     * @ORM\Column(name="h_action", type="text", nullable=true)
     */
    private $hAction = "Action must be taken immediately and / or within maximum three months (based on the anomaly assessment completed by the Executive Dept. This action must eliminate/minimize the risk to personnel, equipment or the environment. Such anomalies must be brought to the immediate attention of the senior management.";

    /**
     * @var string
     *
     * @ORM\Column(name="m_description", type="text", nullable=true)
     */
    private $mDescription = "The anomaly currently complies with its pre-defined acceptance criteria but its deterioration is such that a Repair order must be raised for remedial action prior to the next planned inspection and its deterioration needs to be closely monitored.";

    /**
     * @var string
     *
     * @ORM\Column(name="m_action", type="text", nullable=true)
     */
    private $mAction = "The anomaly must be rectified prior to the next planned inspection & within maximum one year (actual timing is subject to assessment report for each individual case) by the Executive Dept.";

    /**
     * @var string
     *
     * @ORM\Column(name="l_description", type="text", nullable=true)
     */
    private $lDescription = "The component is deteriorating at a manageable rate and is found within its acceptance criteria. No remedial action is required and future monitoring is the only current form of mitigation Ex. Fabric maintenance anomalies The anomaly is classified as not posing an integrity risk before the next planned inspection monitoring.";

    /**
     * @var string
     *
     * @ORM\Column(name="l_action", type="text", nullable=true)
     */
    private $lAction = "The anomaly must be monitored or rectified within maximum two years (actual timing is subject to assessment report for each individual case) by the Executive Dept.";

    /**
     * @var string
     *
     * @ORM\Column(name="vl_description", type="text", nullable=true)
     */
    private $vlDescription = "Monitoring  only.";

    /**
     * @var string
     *
     * @ORM\Column(name="vl_action", type="text", nullable=true)
     */
    private $vlAction = "An anomaly that might not realistically lead to an LOPC and doesn’t present safety risk, hence No remedial action is required.";


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hDescription
     *
     * @param string $hDescription
     * @return CriticalityRanks
     */
    public function setHDescription($hDescription)
    {
        $this->hDescription = $hDescription;

        return $this;
    }

    /**
     * Get hDescription
     *
     * @return string
     */
    public function getHDescription()
    {
        return $this->hDescription;
    }

    /**
     * Set hAction
     *
     * @param string $hAction
     * @return CriticalityRanks
     */
    public function setHAction($hAction)
    {
        $this->hAction = $hAction;

        return $this;
    }

    /**
     * Get hAction
     *
     * @return string
     */
    public function getHAction()
    {
        return $this->hAction;
    }

    /**
     * Set mDescription
     *
     * @param string $mDescription
     * @return CriticalityRanks
     */
    public function setMDescription($mDescription)
    {
        $this->mDescription = $mDescription;

        return $this;
    }

    /**
     * Get mDescription
     *
     * @return string
     */
    public function getMDescription()
    {
        return $this->mDescription;
    }

    /**
     * Set mAction
     *
     * @param string $mAction
     * @return CriticalityRanks
     */
    public function setMAction($mAction)
    {
        $this->mAction = $mAction;

        return $this;
    }

    /**
     * Get mAction
     *
     * @return string
     */
    public function getMAction()
    {
        return $this->mAction;
    }

    /**
     * Set lDescription
     *
     * @param string $lDescription
     * @return CriticalityRanks
     */
    public function setLDescription($lDescription)
    {
        $this->lDescription = $lDescription;

        return $this;
    }

    /**
     * Get lDescription
     *
     * @return string
     */
    public function getLDescription()
    {
        return $this->lDescription;
    }

    /**
     * Set lAction
     *
     * @param string $lAction
     * @return CriticalityRanks
     */
    public function setLAction($lAction)
    {
        $this->lAction = $lAction;

        return $this;
    }

    /**
     * Get lAction
     *
     * @return string
     */
    public function getLAction()
    {
        return $this->lAction;
    }

    /**
     * Set vhDescription
     *
     * @param string $vhDescription
     * @return CriticalityRanks
     */
    public function setVhDescription($vhDescription)
    {
        $this->vhDescription = $vhDescription;

        return $this;
    }

    /**
     * Get vhDescription
     *
     * @return string
     */
    public function getVhDescription()
    {
        return $this->vhDescription;
    }

    /**
     * Set vhAction
     *
     * @param string $vhAction
     * @return CriticalityRanks
     */
    public function setVhAction($vhAction)
    {
        $this->vhAction = $vhAction;

        return $this;
    }

    /**
     * Get vhAction
     *
     * @return string
     */
    public function getVhAction()
    {
        return $this->vhAction;
    }

    /**
     * Set vlDescription
     *
     * @param string $vlDescription
     * @return CriticalityRanks
     */
    public function setVlDescription($vlDescription)
    {
        $this->vlDescription = $vlDescription;

        return $this;
    }

    /**
     * Get vlDescription
     *
     * @return string
     */
    public function getVlDescription()
    {
        return $this->vlDescription;
    }

    /**
     * Set vlAction
     *
     * @param string $vlAction
     * @return CriticalityRanks
     */
    public function setVlAction($vlAction)
    {
        $this->vlAction = $vlAction;

        return $this;
    }

    /**
     * Get vlAction
     *
     * @return string
     */
    public function getVlAction()
    {
        return $this->vlAction;
    }


    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\CriticalityProject $project
     * @return CriticalityRanks
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\CriticalityProject $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\CriticalityProject
     */
    public function getProject()
    {
        return $this->project;
    }
}