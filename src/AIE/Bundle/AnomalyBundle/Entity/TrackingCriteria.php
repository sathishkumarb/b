<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * TrackingCriteria
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TrackingCriteria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AnomalyRegistrar", inversedBy="trackingCriteria")
     * @ORM\JoinColumn(name="registrar_id", referencedColumnName="id")
     * @JMS\Exclude
     */
    protected $registrar;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=100, nullable=true)
     */
    private $unit;


    /**
     *
     * @var double
     *
     * @ORM\Column(name="tracking_conc", type="string", length=100)
     */
    protected $trackingConc;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_conc_unit", type="string", length=100, nullable=true)
     */
    protected $trackingConcUnit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TrackingCriteria
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return TrackingCriteria
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set unit
     *
     * @param string $unit
     * @return TrackingCriteria
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set registrar
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $registrar
     * @return TrackingCriteria
     */
    public function setRegistrar(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $registrar = null)
    {
        $this->registrar = $registrar;

        return $this;
    }

    /**
     * Get registrar
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar 
     */
    public function getRegistrar()
    {
        return $this->registrar;
    }

    /**
     * Set trackingConc
     *
     * @param string $trackingConc
     * @return TrackingCriteria
     */
    public function setTrackingConc($trackingConc)
    {
        $this->trackingConc = $trackingConc;

        return $this;
    }

    /**
     * Get trackingConc
     *
     * @return string 
     */
    public function getTrackingConc()
    {
        return $this->trackingConc;
    }

    /**
     * Set trackingConcUnit
     *
     * @param string $trackingConcUnit
     * @return TrackingCriteria
     */
    public function setTrackingConcUnit($trackingConcUnit)
    {
        $this->trackingConcUnit = $trackingConcUnit;

        return $this;
    }

    /**
     * Get trackingConcUnit
     *
     * @return string 
     */
    public function getTrackingConcUnit()
    {
        return $this->trackingConcUnit;
    }

    public function getTrackingCriteriaString(){
        return $this->getDescription() . ' = ' . $this->getValue() . ' ' . $this->getUnit();
    }

    public function getTrackingConcString(){
        return $this->getTrackingConc() . ' ' . $this->getTrackingConcUnit();
    }
}
