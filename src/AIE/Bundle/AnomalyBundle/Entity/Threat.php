<?php
/**
 * User: mokha
 * Date: 3/13/16
 * Time: 1:18 AM
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Threat
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Threat extends ThreatAbstract {}