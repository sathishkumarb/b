<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\UserBundle\Entity as User;

/**
 * Request
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Request extends AbstractFile {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="text")
	 */
	private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="request_source", type="text", nullable=true)
     */
    private $requestSource;

    /**
     * @var string
     *
     * @ORM\Column(name="request_batch", type="text", nullable=true)
     */
    private $requestBatch;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="data", type="json_array")
	 */
	private $data;


    /**
     * @ORM\ManyToOne(targetEntity="ActionOwners", inversedBy="requests")
     * @ORM\JoinColumn(name="requested_by", referencedColumnName="id")
     */
	private $requestedBy;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @ORM\ManyToOne(targetEntity="Registrar", inversedBy="requests")
	 * @ORM\JoinColumn(name="registrar_id", referencedColumnName="id", nullable=true)
	 */
	private $registrar;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="justification", type="text", nullable=true)
	 */
	private $justification;


    /**
     * @var string
     *
     * @ORM\Column(name="pdffilepath", type="string", nullable=true)
     */
    private $pdfFilePath;

    /**
     * @var string
     *
     * @ORM\Column(name="summary_pdffilepath", type="string", nullable=true)
     */
    private $summaryPdfFilePath;


	/**
	 * Indicates whether the request is submitted or not
	 * @var boolean
	 */
	protected $isSubmitted;

    /**
     * @var integer
     * @ORM\Column(name="approval_level", type="integer")
     */
    private $approvalLevel = 1;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="answered_date", type="datetime", nullable=true)
	 */
	private $answeredDate;

    /**
     * @ORM\ManyToOne(targetEntity="ApprovalOwners", inversedBy="answers")
     * @ORM\JoinColumn(name="answered_by", referencedColumnName="id")
     */
	private $answeredBy;

    /**
     * @ORM\ManyToOne(targetEntity="Deferrals",  inversedBy="answers")
     * @ORM\JoinColumn(name="deferred_by", referencedColumnName="id")
     */
    private $deferredBy;

	//private $files;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="status", type="integer") //-1 unknown, 0 rejected, 1 approved
	 */
	private $status = -1; //unknown, rejected, approved

    /**
     * @ORM\ManyToMany(targetEntity="Files")
     * @ORM\JoinTable(name="RequestFiles",
     *      joinColumns={@ORM\JoinColumn(name="request_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    protected $requestFiles;

	/**
	 * @ORM\ManyToMany(targetEntity="Files")
	 * @ORM\JoinTable(name="RequestResponseFiles",
	 *      joinColumns={@ORM\JoinColumn(name="request_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
	 * )
	 */
	protected $responseFiles;

	private static $statusChoices = [
		//-1 => 'pending approval',
		0 => 'rejected',
		1 => 'approved'
	];

	/**
	 * @var string
	 *
	 * @ORM\Column(name="requeststatus", type="string")
	 */
	private $requeststatus;

	/**
	 * @var array
	 *
	 * @ORM\Column(name="attachedreportanomalys", type="json_array", nullable=true)
	 */
	private $attachedreportanomalys;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	public function getUploadDir() {
		return $this->registrar->getProject()->getFolder() . '/reports';
	}

	/**
	 * Set data
	 *
	 * @param array $data
	 * @return Request
	 */
	public function setData($data) {
		$this->data = $data;

		return $this;
	}

	/**
	 * Get data
	 *
	 * @return array
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 * @return Request
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Request
	 */
	public function setType($type) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

    /**
     * Set pdfFilePath
     *
     * @param string $pdffilepath
     * @return Request
     */
    public function setPdfFilePath($pdffilepath) {
        $this->pdfFilePath = $pdffilepath;

        return $this;
    }

    /**
     * Get pdfFilePath
     *
     * @return string
     */
    public function getPdfFilePath() {
        return $this->pdfFilePath;
    }


    /**
     * Set summaryPdfFilePath
     *
     * @param string $summaryPdfFilePath
     * @return Request
     */
    public function setSummaryPdfFilePath($summaryPdfFilePath) {
        $this->summaryPdfFilePath = $summaryPdfFilePath;

        return $this;
    }

    /**
     * Get summaryPdfFilePath
     *
     * @return string
     */
    public function getSummaryPdfFilePath() {
        return $this->summaryPdfFilePath;
    }

	/**
	 * Set justification
	 *
	 * @param string $justification
	 * @return Request
	 */
	public function setJustification($justification) {
		$this->justification = $justification;

		return $this;
	}

	/**
	 * Get justification
	 *
	 * @return string
	 */
	public function getJustification() {
		return $this->justification;
	}

    /**
     * Set approvalLevel
     *
     * @param integer $approvalLevel
     * @return ApprovalOwners
     */
    public function setApprovalLevel($approvalLevel) {
        $this->approvalLevel = $approvalLevel;

        return $this;
    }

    /**
     * Get approvalLevel
     *
     * @return integer
     */
    public function getApprovalLevel() {
        return $this->approvalLevel;
    }

	/**
	 * Set status
	 *
	 * @param boolean $status
	 * @return Request
	 */
	public function setStatus($status) {
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status
	 *
	 * @return boolean
	 */
	public function getStatus() {
		return $this->status;
	}


	public static function getStatusChoices() {
		return self::$statusChoices;
	}

	/**
	 * Set answeredDate
	 *
	 * @param \DateTime $answeredDate
	 * @return Request
	 */
	public function setAnsweredDate($answeredDate) {
		$this->answeredDate = $answeredDate;

		return $this;
	}

	/**
	 * Get answeredDate
	 *
	 * @return \DateTime
	 */
	public function getAnsweredDate() {
		return $this->answeredDate;
	}


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->responseFiles = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add responseFiles
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Files $responseFiles
	 * @return Request
	 */
	public function addResponseFile(\AIE\Bundle\AnomalyBundle\Entity\Files $responseFiles) {
		$this->responseFiles[ ] = $responseFiles;

		return $this;
	}

	/**
	 * Remove responseFiles
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\Files $responseFiles
	 */
	public function removeResponseFile(\AIE\Bundle\AnomalyBundle\Entity\Files $responseFiles) {
		$this->responseFiles->removeElement($responseFiles);
	}

	/**
	 * Get responseFiles
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getResponseFiles() {
		return $this->responseFiles;
	}

    /**
     * Set registrar
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Registrar $registrar
     * @return Request
     */
    public function setRegistrar(\AIE\Bundle\AnomalyBundle\Entity\Registrar $registrar = null)
    {
        $this->registrar = $registrar;

        return $this;
    }

    /**
     * Get registrar
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Registrar 
     */
    public function getRegistrar()
    {
        return $this->registrar;
    }

    /**
     * Set requestedBy
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ActionOwners $requestedBy
     * @return Request
     */
    public function setRequestedBy(\AIE\Bundle\AnomalyBundle\Entity\ActionOwners $requestedBy = null)
    {
        $this->requestedBy = $requestedBy;

        return $this;
    }

    /**
     * Get requestedBy
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\ActionOwners 
     */
    public function getRequestedBy()
    {
        return $this->requestedBy;
    }

    /**
     * Set answeredBy
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $answeredBy
     * @return Request
     */
    public function setAnsweredBy(\AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners $answeredBy = null)
    {
        $this->answeredBy = $answeredBy;

        return $this;
    }

    /**
     * Get answeredBy
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners
     */
    public function getAnsweredBy()
    {
        return $this->answeredBy;
    }

    /**
     * @return mixed
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Deferrals
     */
    public function getDeferredBy()
    {
        return $this->deferredBy;
    }

    /**
     * @return Request
     * @param \AIE\Bundle\AnomalyBundle\Entity\Deferrals $deferredBy
     */
    public function setDeferredBy($deferredBy)
    {
        $this->deferredBy = $deferredBy;
    }


    /**
     * Add requestFiles
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $requestFiles
     * @return Request
     */
    public function addRequestFile(\AIE\Bundle\AnomalyBundle\Entity\Files $requestFiles)
    {
        $this->requestFiles[] = $requestFiles;

        return $this;
    }

    /**
     * Remove requestFiles
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Files $requestFiles
     */
    public function removeRequestFile(\AIE\Bundle\AnomalyBundle\Entity\Files $requestFiles)
    {
        $this->requestFiles->removeElement($requestFiles);
    }

    /**
     * Get requestFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequestFiles()
    {
        return $this->requestFiles;
    }


    public function setRequestFiles($files){
        if($files !== null) {
            foreach ($files as $files) {
                $this->addRequestFile($files);
            }
        }

        return $this;
    }

    /**
     * Set requeststatus
     *
     * @param $requeststatus
     * @return Request
     */
    public function setRequestStatus($requeststatus = null)
    {
        $this->requeststatus = $requeststatus;

        return $this;
    }

    /**
     * Get requeststatus
     *
     * @return string
     */
    public function getRequestStatus()
    {
        return $this->requeststatus;
    }

   	/**
	 * Set attachedreportanomalys
	 *
	 * @param array $attachedreportanomalys
	 * @return Request
	 */
	public function setAttachedreportanomalys($attachedreportanomalys) {
		$this->attachedreportanomalys = $attachedreportanomalys;
		return $this;
	}

	/**
	 * Get attachedreportanomalys
	 *
	 * @return array
	 */
	public function getAttachedreportanomalys() {
		return $this->attachedreportanomalys;
	}

    /**
     * Set request_source
     *
     * @param string $requestSource
     * @return Request
     */
    public function setRequestSource($requestSource) {
        $this->requestSource = $requestSource;
        return $this;
    }

    /**
     * Get request_source
     *
     * @return string
     */
    public function getRequestSource() {
        return $this->requestSource;
    }

    /**
     * Set request_batch
     *
     * @param string $requestBatch
     * @return Request
     */
    public function setRequestBatch($requestBatch) {
        $this->requestBatch = $requestBatch;
        return $this;
    }

    /**
     * Get request_batch
     *
     * @return string
     */
    public function getRequestBatch() {
        return $this->requestBatch;
    }
}
