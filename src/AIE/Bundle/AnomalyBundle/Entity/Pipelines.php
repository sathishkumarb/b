<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 06/09/15
 * Time: 01:05
 */

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pipelines
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Pipelines extends Locations {


	/**
	 * @ORM\OneToMany(targetEntity="PipelineSections", mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
	 */
	protected $pse;

	/**
	 * the length of the pipeline in KM
	 *
	 * @var double
	 *
	 * @ORM\Column(name="length", type="decimal", scale=2)
	 */
	protected $length;

    /**
     * @var double
     *
     * @ORM\Column(name="outer_diameter", type="decimal", scale=2)
     */
	private $outerDiameter;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="maop", type="integer")
	 */
	private $mAOP;

	/**
	 * @var double
	 *
	 * @ORM\Column(name="design_factor", type="decimal", scale=2)
	 */
	private $designFactor;


	/**
	 * Add pse
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\PipelineSections $pse
	 * @return Pipelines
	 */
	public function addPse(\AIE\Bundle\AnomalyBundle\Entity\PipelineSections $pse) {
		$this->pse[ ] = $pse;

		return $this;
	}

	/**
	 * Remove pse
	 *
	 * @param \AIE\Bundle\AnomalyBundle\Entity\PipelineSections $pse
	 */
	public function removePse(\AIE\Bundle\AnomalyBundle\Entity\PipelineSections $pse) {
		$this->pse->removeElement($pse);
	}

	/**
	 * Get pse
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPse() {
		return $this->pse;
	}

	/**
	 * Set length
	 *
	 * @param double $length
	 * @return Pipelines
	 */
	public function setLength($length) {
		$this->length = $length;

		return $this;
	}

	/**
	 * Get length
	 *
	 * @return double
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * Set outerDiameter
	 *
	 * @param integer $outerDiameter
	 * @return Pipelines
	 */
	public function setOuterDiameter($outerDiameter) {
		$this->outerDiameter = $outerDiameter;

		return $this;
	}

	/**
	 * Get outerDiameter
	 *
	 * @return integer
	 */
	public function getOuterDiameter() {
		return $this->outerDiameter;
	}

	/**
	 * Set mAOP
	 *
	 * @param integer $mAOP
	 * @return Pipelines
	 */
	public function setMAOP($mAOP) {
		$this->mAOP = $mAOP;

		return $this;
	}

	/**
	 * Get mAOP
	 *
	 * @return integer
	 */
	public function getMAOP() {
		return $this->mAOP;
	}

	/**
	 * Set designFactor
	 *
	 * @param double $designFactor
	 * @return Pipelines
	 */
	public function setDesignFactor($designFactor) {
		$this->designFactor = $designFactor;

		return $this;
	}

	/**
	 * Get designFactor
	 *
	 * @return double
	 */
	public function getDesignFactor() {
		return $this->designFactor;
	}
}
