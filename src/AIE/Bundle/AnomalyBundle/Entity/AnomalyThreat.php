<?php

namespace AIE\Bundle\AnomalyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AnomalyClass
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AnomalyThreat extends ThreatAbstract
{


    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="threats")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * @JMS\Type("AIE\Bundle\AnomalyBundle\Entity\Projects")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="AnomalyRegistrar" , mappedBy="threat", cascade={"persist", "merge", "detach"}, fetch="EXTRA_LAZY")
     */
    private $anomalies;


    /**
     * Set project
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\Projects $project
     * @return AnomalyThreat
     */
    public function setProject(\AIE\Bundle\AnomalyBundle\Entity\Projects $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\AnomalyBundle\Entity\Projects
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anomalies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     * @return AnomalyThreat
     */
    public function addAnomalie(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies[] = $anomalies;

        return $this;
    }

    /**
     * Remove anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     */
    public function removeAnomalie(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies->removeElement($anomalies);
    }

    /**
     * Get anomalies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnomalies()
    {
        return $this->anomalies;
    }

    /**
     * Add anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     * @return AnomalyThreat
     */
    public function addAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies[] = $anomalies;

        return $this;
    }

    /**
     * Remove anomalies
     *
     * @param \AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies
     */
    public function removeAnomaly(\AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar $anomalies)
    {
        $this->anomalies->removeElement($anomalies);
    }
}
