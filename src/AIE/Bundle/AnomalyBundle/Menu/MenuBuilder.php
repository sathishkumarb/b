<?php

namespace AIE\Bundle\AnomalyBundle\Menu;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\Pipelines;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilder
{

    private $factory;
    private $securityContext;
    private $securityAuthorizationChecker;
    private $entityManager;

    protected $anomalyUserGroupRoles;
    protected $userGroupRoles;
    protected $securityHelper;
    protected $loggedInUser;
    protected $assignedAsAdmin;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, $em, $securityTokenStorage, $securityAuthorizationChecker)
    {
        $this->factory = $factory;
        $this->securityContext = $securityTokenStorage;
        $this->securityAuthorizationChecker = $securityAuthorizationChecker;
        $this->entityManager = $em;

        global $kernel;

        /* Set Roles based on anomaly project groups user associated */
        $token = $kernel->getContainer()->get('security.context')->getToken();

        $this->loggedInUser = $token->getUser();

        //check if user is module level admin
        $this->assignedAsAdmin = $kernel->getContainer()->get('aie_anomaly.user.helper')->getIsAdmin($this->loggedInUser->getId());


        if ($kernel->getContainer()->has('request')) {
            $request = $kernel->getContainer()->get('request');
        } else {
            $request = Request::createFromGlobals();
        }
        $pID = null;
   
        if ($request->get('projectId') && $request->get('id')){
            $pID = $request->get('projectId');
        }  else if ($request->get('projectId') && !$request->get('id')){
            $pID = $request->get('projectId');
        } else {
            $pID = $request->get('id');
        }


        $this->anomalyUserGroupRoles = $kernel->getContainer()->get('aie_anomaly.user.helper')->getAnomalyGroupRoles($this->loggedInUser->getId(), $pID);
        
        $this->userGroupRoles = [];

   
        //if no user role selected user role from users
        if (!$this->anomalyUserGroupRoles){
//           $this->anomalyUserGroupRoles = $kernel->getContainer()->get('aie_anomaly.user.helper')->getSuperAdmin($this->loggedInUser->getId());
//            $this->userGroupRoles = unserialize($this->anomalyUserGroupRoles['roles']);
        } else {

            if (count($this->anomalyUserGroupRoles) > 1){
                foreach($this->anomalyUserGroupRoles as $roles){
                    $this->userGroupRoles = array_merge( $this->userGroupRoles, unserialize($roles->getRoles()) );
                }
            }
            else{
                $this->userGroupRoles = (count($this->anomalyUserGroupRoles) ? unserialize($this->anomalyUserGroupRoles[0]->getRoles()) : []);
            }
        }
 
        $this->securityHelper = $kernel->getContainer()->get('aie_anomaly.user.helper');
    }


    public function createMainLeftMenu(Request $request)
    {

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu->setChildrenAttribute('id', 'top-nav');

        $menu->addChild('Home', ['route' => "index"]);
        $menu->addChild('Dashboard', ['route' => "anomaly_index"]);
        $menu->addChild('Projects', ['route' => 'anomaly_projects']);

        $projectId = $request->get('projectId', $request->get('id'));

        switch ($request->get('_route')) {

            case 'anomaly_projects_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_projects_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_riskmatrix_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_locations':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_locations_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_locations_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_locations_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipelines':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipelines_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipelines_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipelines_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_projectsconsequences':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_criticality_preselect':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_criticality_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_a_threats':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_a_threats_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_a_threats_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_classes':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_classes_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_classes_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ao':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ao_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ao_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ao_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferrals':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferrals_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferrals_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferrals_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
             case 'anomaly_approvalowners':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_approvalowners_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_approvalowners_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_approvalowners_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipeline_sections':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipeline_sections_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipeline_sections_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipeline_sections_edit':
                $pipelineId = $request->get('pipelineId', $request->get('id'));
                /** @var Pipelines $pipeline */
                $pipeline = $this->entityManager->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
                $this->createProjectTopMenu($menu, $pipeline->getProject()->getId());
                break;
            case 'anomaly_pl':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_locations':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_vs':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pl_create':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi_vs':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_pi_create':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se_vs':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_se_create':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_st':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_st_vs':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_st_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_st_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_st_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr_vs':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr_new':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_mr_create':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ro_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ro':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ro_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_ro_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_fm_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_fm':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_fm_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_fm_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_tro_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_tro':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_tro_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_tro_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_as_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_as':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_as_show':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_as_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_monitor':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_monitor_edit':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferral_anomaly':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferral_action':
                $projectId = $request->get('projectId', $request->get('id'));
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_deferral_request':
                $projectId = $request->get('projectId', $request->get('id'));
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'action_monitor':
                break;
            case 'action_monitor_edit':
                $anomalyId = $request->get('anomalyId', $request->get('id'));
                $this->createActionTopMenu($menu, $anomalyId);
                break;
            case 'anomaly_planning':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_reminders':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_planning_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_reminders_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_search_master':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_compare_tp':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            case 'anomaly_loadsheet':
                $this->createProjectTopMenu($menu, $projectId, true);
                break;
            
        }

        return $menu;
    }



    public function createMainRightMenu(Request $request)
    {

        $user = $this->securityContext->getToken()->getUser();

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');


        //$requests = $this->entityManager->getRepository('AIEAnomalyBundle:Request')->findByStatus(-1);

        $reqEntitiesApprovalOwners = [];
        $reqEntitiesDeferralOwners = [];

        $entities =  [];


        $userId= $this->loggedInUser->getId();

        global $kernel;
        $userHelper = $kernel->getContainer()->get('aie_anomaly.user.helper');
        $badgeCount = 0;

        $badgeCount += count($userHelper->getAllDeferralRequests(-1, $userId));
        $badgeCount += count($userHelper->getAllApprovalRequests(-1, $userId));

        $notificationExtras = [];
        if ($badgeCount > 0) {
            $notificationExtras += ['badge' => $badgeCount];
        }

        $menu->addChild(
            'Notifications',
            ['label' => 'Pending Requests', 'route' => 'anomaly_requests', 'extras' => $notificationExtras]
        );

        /* User Security */
        if ($this->securityAuthorizationChecker->isGranted('ROLE_SUPER_ADMIN') || $this->securityHelper->isRoleGranted('ROLE_SUPER_ADMIN',$this->userGroupRoles)) {
            $menu->addChild('Admin', ['label' => 'Admin Panel', 'route' => 'anomaly_admin_dashboard']);
        }

        $menu->addChild('User', ['label' => $user->getUsername()])
            ->setAttribute('dropdown', true)
            ->setAttribute('class', 'dropdown');

        $menu['User']->addChild('Profile', ['route' => 'fos_user_profile_show']);
        $menu['User']->addChild('Change Password', ['route' => 'fos_user_change_password'])->setAttribute(
            'divider_append',
            true
        );

        $menu['User']->addChild('Logout', ['route' => 'fos_user_security_logout']);

        return $menu;
    }
    public function createThirdLeveltMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        switch ($request->get('_route')) {

            case 'anomaly_pl':
            case 'anomaly_pl_locations':
            case 'anomaly_pl_vs':
            case 'anomaly_pl_new':
            case 'anomaly_pl_duplicate':
            case 'anomaly_pl_create':
                $projectId = $request->get('projectId', $request->get('id'));
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, 'pl');
                break;
            case 'anomaly_pi':
            case 'anomaly_pi_vs':
            case 'anomaly_pi_new':
            case 'anomaly_pi_duplicate':
            case 'anomaly_pi_create':
                $projectId = $request->get('projectId', $request->get('id'));
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, 'pi');
                break;
            case 'anomaly_se':
            case 'anomaly_se_vs':
            case 'anomaly_se_new':
            case 'anomaly_se_duplicate':
            case 'anomaly_se_create':
                $projectId = $request->get('projectId', $request->get('id'));
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, 'se');
                break;
            case 'anomaly_st':
            case 'anomaly_st_vs':
            case 'anomaly_st_new':
            case 'anomaly_st_duplicate':
            case 'anomaly_st_create':
                $projectId = $request->get('projectId', $request->get('id'));
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, 'st');
                break;
            case 'anomaly_mr':
            case 'anomaly_mr_vs':
            case 'anomaly_mr_new':
            case 'anomaly_mr_duplicate':
            case 'anomaly_mr_create':
                $projectId = $request->get('projectId', $request->get('id'));
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, 'mr');
                break;
            case 'anomaly_planning':
            case 'anomaly_reminders':
                $projectId = $request->get('projectId', $request->get('id'));
                $anomalyType = $request->get('anomalyType');
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, $anomalyType);
                break;
            case 'anomaly_pl_show':
            case 'anomaly_pl_edit':
            case 'anomaly_pi_show':
            case 'anomaly_pi_edit':
            case 'anomaly_se_show':
            case 'anomaly_se_edit':
            case 'anomaly_st_show':
            case 'anomaly_st_edit':
            case 'anomaly_mr_show':
            case 'anomaly_mr_edit':
            case 'anomaly_monitor':
            case 'anomaly_monitor_edit':
            case 'anomaly_deferral_anomaly':
                // When inside an anomaly registrar
                $anomalyId = $request->get('anomalyId', $request->get('id'));
                /** @var AnomalyRegistrar $anomaly */
                $anomaly = $this->entityManager->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($anomalyId);
                $this->createAnomalyLeftMenu($menu, $anomaly);
                break;
            case 'anomaly_deferral_request':
                // When inside an deferral request of a action or registrar
                $requestentity = $this->entityManager->getRepository('AIEAnomalyBundle:Request')->find($request->get('id'));
                /** @var AnomalyRegistrar $anomaly */
                $anomaly = $this->entityManager->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($requestentity->getRegistrar()->getId());
                if (!$anomaly) $anomaly = $this->entityManager->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($requestentity->getRegistrar()->getId());

                $this->createAnomalyLeftMenu($menu, $anomaly);
                break;
            case 'anomaly_files':
            case 'anomaly_files_new':
            case 'anomaly_files_edit':
                // When inside an anomaly registrar
                $anomalyId = $request->get('anomalyId', $request->get('refId'));
                /** @var AnomalyRegistrar $anomaly */
                $anomaly = $this->entityManager->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($anomalyId);
                $this->createAnomalyLeftMenu($menu, $anomaly);
                break;
            case 'anomaly_ro':
            case 'anomaly_fm':
            case 'anomaly_tro':
                $projectId = $request->get('projectId', $request->get('id'));
                $type = $request->get('type');
                $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
                $this->createAnomalyCategoryLeftMenu($menu, $project, $projectId, $type);
                break;
            case 'anomaly_ro_show':
            case 'anomaly_ro_edit':
            case 'anomaly_fm_show':
            case 'anomaly_fm_edit':
            case 'anomaly_tro_show':
            case 'anomaly_tro_edit':
            case 'anomaly_as_show':
            case 'anomaly_as_edit':
            case 'anomaly_deferral_action':
            case 'action_monitor':
                // when inside an action registrar
                $actionId = $request->get('actionId', $request->get('id'));
                $action = $this->entityManager->getRepository('AIEAnomalyBundle:ActionRegistrar')->find($actionId);
                $this->createActionLeftMenu($menu, $action);
                break;

            case 'anomaly_request_history':
                // When inside an anomaly registrar
                $id = $request->get('id');
                /** @var AnomalyRegistrar $anomaly */
                $registrar = $this->entityManager->getRepository('AIEAnomalyBundle:Registrar')->find($id);
                if ($registrar instanceof AnomalyRegistrar) {
                    $this->createAnomalyLeftMenu($menu, $registrar);
                } else {
                    $this->createActionLeftMenu($menu, $registrar);
                }
                break;
            case 'anomaly_admin_dashboard':
            case 'anomaly_consequences':
            case 'anomaly_consequences_show':
            case 'anomaly_consequences_edit':
            case 'anomaly_consequences_new':
            case 'anomaly_threats':
            case 'anomaly_threats_new':
            case 'anomaly_threats_edit':
            case 'anomaly_threats_show':
            case 'anomaly_classifications':
            case 'anomaly_classifications_new':
            case 'anomaly_classifications_edit':
            case 'anomaly_classifications_show':
            case 'anomaly_designcode':
            case 'anomaly_designcode_new':
            case 'anomaly_designcode_edit':
            case 'anomaly_designcode_show':
                $this->createAdminMenu($menu);
                break;
            case 'anomaly_users':
                $this->createAdminMenu($menu);

                break;
            case 'anomaly_user_edit':
                $this->createAdminMenu($menu);

                break;
        }
        return $menu;
    }
    public function createLeftNav(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        switch ($request->get('_route')) {
            case 'anomaly_index':
            case 'anomaly_user_group_list':
            case 'anomaly_user_group_show':
            case 'anomaly_user_group_new':
            case 'anomaly_user_group_edit':
            case 'anomaly_user_group_delete':
            case 'anomaly_user_group':
			case 'anomaly_userprojectgroup_show':
            case 'anomaly_userprojectgroup_edit':
            case 'anomaly_userprojectgroup_new':
            case 'anomaly_userprojectgroup_delete':
            case 'anomaly_userprojectgroup':
                $this->createAdminMenu($menu);
                break;
            case 'anomaly_projects':
            case 'anomaly_projects_new':
            case 'anomaly_projects_delete':
                $menu->addChild(
                    'Projects',
                    ['route' => 'anomaly_projects']
                );
                if( $this->assignedAsAdmin){
                    $menu->addChild(
                        'Add New',
                        ['route' => 'anomaly_projects_new']
                    );
                }
                break;
            case 'anomaly_projects_show':
            case 'anomaly_projects_edit':
            case 'anomaly_riskmatrix_new':
            case 'anomaly_locations':
            case 'anomaly_locations_new':
            case 'anomaly_locations_edit':
            case 'anomaly_locations_show':
            case 'anomaly_pipelines':
            case 'anomaly_pipelines_new':
            case 'anomaly_pipelines_edit':
            case 'anomaly_pipelines_show':
            case 'anomaly_projectsconsequences':
            case 'anomaly_criticality_new':
            case 'anomaly_criticality_preselect':
            case 'anomaly_riskmatrix_create':
            case 'anomaly_a_threats':
            case 'anomaly_a_threats_new':
            case 'anomaly_a_threats_edit':
            case 'anomaly_classes':
            case 'anomaly_classes_new':
            case 'anomaly_classes_edit':
            case 'anomaly_ao':
            case 'anomaly_ao_new':
            case 'anomaly_ao_edit':
            case 'anomaly_ao_show':
            case 'anomaly_deferrals':
            case 'anomaly_deferrals_new':
            case 'anomaly_deferrals_edit':
            case 'anomaly_deferrals_show':
            case 'anomaly_approvalowners':
            case 'anomaly_approvalowners_new':
            case 'anomaly_approvalowners_edit':
            case 'anomaly_approvalowners_show':
            $projectId = $request->get('projectId', $request->get('id'));
            $this->createProjectLeftMenu($menu, $projectId, true);
                break;
            case 'anomaly_codes':
            case 'anomaly_codes_new':
            case 'anomaly_codes_edit':
            case 'anomaly_codes_show':
            case 'anomaly_master':
            case 'anomaly_search_master':
            case 'anomaly_ro_master':
            case 'anomaly_tro_master':
            case 'anomaly_fm_master':
            case 'anomaly_as_master':
            case 'anomaly_planning_master':
            case 'anomaly_reminders_master':
            case 'anomaly_compare_tp':
            case 'anomaly_loadsheet':
                $projectId = $request->get('projectId', $request->get('id'));
                $this->createProjectLeftMenu($menu, $projectId, true);
                break;
            case 'anomaly_pipeline_sections':
            case 'anomaly_pipeline_sections_new':
            case 'anomaly_pipeline_sections_show':
            case 'anomaly_pipeline_sections_delete':
            case 'anomaly_pipeline_sections_edit':
                $pipelineId = $request->get('pipelineId', $request->get('id'));
                /** @var Pipelines $pipeline */
                $pipeline = $this->entityManager->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
                $this->createProjectLeftMenu($menu, $pipeline->getProject()->getId());
                break;
            default:
                if (!empty($request->get('projectId'))){
                    $projectId = $request->get('projectId', $request->get('id'));
                    $this->createProjectLeftMenu($menu, $projectId, true);
                }
                break;
        }

        return $menu;
    }

    private function createAnomalyCategoryLeftMenu(&$menu, $project, $projectId, $type = 'pl')
    {

//		$anomalies = $menu->addChild('Anomalies', ['uri' => '#', 'extras' => ['icon' => 'cog']])->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
//		$anomalies->setChildrenAttribute('class', 'subnav')->setAttribute('dropdown', true);
//		$anomalies->addChild('Register', ['uri' => '#', 'routeParameters' => ['projectId' => $projectId], 'extras' => ['icon' => 'flag']])->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
        if ($project->getIsSetup()['isSetup']){
            $menu = $this->factory->createItem('root');
            $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

            if ($type === null) {
                return;
            }

            switch  ($type) {
                case 'pl';
                    $masRegisterTypeLabel = 'Pipeline';
                    $masRegisterRole      = 'ROLE_PIPELINE_ANOMALY';
                    break;

                case 'pi';
                    $masRegisterTypeLabel = 'Piping';
                    $masRegisterRole      = 'ROLE_PIPING_ANOMALY';
                    break;

                case 'se';
                    $masRegisterTypeLabel = 'Static Equipment';
                    $masRegisterRole      = 'ROLE_STATICEQUIPMENT_ANOMALY';
                    break;

                case 'st';
                    $masRegisterTypeLabel = 'Structure';
                    $masRegisterRole      = 'ROLE_STRUCTURE_ANOMALY';
                    break;

                case 'mr';
                    $masRegisterTypeLabel = 'Marine';
                    $masRegisterRole      = 'ROLE_MARINE_ANOMALY';
                    break;

                default:
                    $masRegisterTypeLabel = $type;
                    break;
            }

            if( $this->securityHelper->isRoleGranted($masRegisterRole,$this->userGroupRoles) ){
                $actions = $menu->addChild(
                    $masRegisterTypeLabel.' Register',
                    [
                        'route' => 'anomaly_'.$type,
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                $actions->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            }

            $actionsList = $menu->addChild(
                'Action',
                ['uri' => '#']
            )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');

            $actionsList->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            $ActionGranted=false;
            if( $this->securityHelper->isRoleGranted($masRegisterRole."_NEW",$this->userGroupRoles) ){
                $actionsList->addChild(
                    'New Anomaly',
                    [
                        'route' => 'anomaly_'.$type.'_new',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
                $ActionGranted=true;
            }
            if (!$ActionGranted){
                $menu->removeChild($actionsList);
            }

            if( $this->securityHelper->isRoleGranted($masRegisterRole,$this->userGroupRoles) ){
                $actions->addChild(
                    'Anomaly Master',
                    [
                        'route' => 'anomaly_'.$type,
                        'routeParameters' => ['projectId' => $projectId, 'type' => $type],
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_REPAIR_ORDER',$this->userGroupRoles) ){
                $actions->addChild(
                    'Repair Order',
                    [
                        'route' => 'anomaly_ro',
                        'routeParameters' => ['projectId' => $projectId, 'type' => $type]
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_FABRIC_MAINTENANCE_ORDER',$this->userGroupRoles) ){
                $actions->addChild(
                    'Fabric Maintenance',
                    [
                        'route' => 'anomaly_fm',
                        'routeParameters' => ['projectId' => $projectId, 'type' => $type]
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_TEMPORARY_REPAIR_ORDER',$this->userGroupRoles) ){
                $actions->addChild(
                    'Temporary Repair',
                    [
                        'route' => 'anomaly_tro',
                        'routeParameters' => ['projectId' => $projectId, 'type' => $type]
                    ]
                );
                $enableActionMenu = true;
            }
            //ROLE for Assessment
            if( $this->securityHelper->isRoleGranted('ROLE_ASSESSMENT_ORDER',$this->userGroupRoles) ) {
                $actions->addChild(
                    'Assessment',
                    [
                        'route' => 'anomaly_as',
                        'routeParameters' => ['projectId' => $projectId, 'type' => $type]
                    ]
                );
            }
        }

    }


    private function createProjectLeftMenu(&$menu, $projectId)
    {

        $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $menu->setAttribute('class','nav navbar-nav navbar-right');

        $projectSetup = $menu->addChild(
            'Project Setup',
            ['uri' => '#']
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $projectSetup->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if( $this->securityHelper->isRoleGranted('ROLE_THREATS',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Anomaly Threats',
                [
                    'route' => 'anomaly_a_threats',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }
        if( $this->securityHelper->isRoleGranted('ROLE_CLASS',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Anomaly Class',
                [
                    'route' => 'anomaly_classes',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }
        if( $this->securityHelper->isRoleGranted('ROLE_ACTION_OWNERS',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Action Owners',
                ['route' => 'anomaly_ao', 'routeParameters' => ['projectId' => $projectId]]
            );
        }
        if( $this->securityHelper->isRoleGranted('ROLE_DEFERRAL_OWNERS',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Deferral Owners',
                [
                    'route' => 'anomaly_deferrals',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }
        if( $this->securityHelper->isRoleGranted('ROLE_APPROVAL_OWNERS',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Approval Owners',
                [
                    'route' => 'anomaly_approvalowners',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }
        if( $this->securityHelper->isRoleGranted('ROLE_CODES',$this->userGroupRoles) ){
            $projectSetup->addChild(
                'Codes & Standards',
                [
                    'route' => 'anomaly_codes',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }

        if ($project instanceof RiskProject) {
            if( $this->securityHelper->isRoleGranted('ROLE_RISK_MATRIX',$this->userGroupRoles) ){
                $projectSetup->addChild(
                    'Consequences',
                    [
                        'route' => 'anomaly_projectsconsequences',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
                $projectSetup->addChild(
                    'Risk Matrix Definition',
                    [
                        'route' => 'anomaly_riskmatrix_new',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
        }else{
            if( $this->securityHelper->isRoleGranted('ROLE_CRITICALITY',$this->userGroupRoles) ){
                $projectSetup->addChild(
                    'Criticality Definition',
                    [
                        'route' => 'anomaly_criticality_preselect',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
                if($project->hasFMCriticality()){
                    $projectSetup->addChild(
                        'FM Criticality Definition',
                        [
                            'route' => 'anomaly_fmcriticality',
                            'routeParameters' => ['projectId' => $projectId]
                        ]
                    );
                }
                if($project->hasProductionCriticality()){
                    $projectSetup->addChild(
                        'Production Criticality Definition',
                        [
                            'route' => 'anomaly_productioncriticality',
                            'routeParameters' => ['projectId' => $projectId]
                        ]
                    );
                }

            }
        }

        $locationSetup = $menu->addChild(
            'Location Setup',
            ['uri' => '#']
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $locationSetup->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);

        if( $this->securityHelper->isRoleGranted('ROLE_LOCATION',$this->userGroupRoles) ){

            $locationSetup->addChild(
                'Location',
                [
                    'route' => 'anomaly_locations',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
        }

        if( $this->securityHelper->isRoleGranted('ROLE_PIPELINE',$this->userGroupRoles) ){

            $locationSetup->addChild(
                'Pipeline',
                [
                    'route' => 'anomaly_pipelines',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );

        }
        if($project->getIsSetup()['isSetup']){
            $anomalies = $menu->addChild(
                'Asset Categories',
                ['uri' => '#']
            )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');
            $anomalies->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            if( $this->securityHelper->isRoleGranted('ROLE_PIPELINE_ANOMALY',$this->userGroupRoles) ){
                $anomalies->addChild(
                    'Pipelines',
                    ['route' => 'anomaly_pl', 'routeParameters' => ['projectId' => $projectId]]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_PIPING_ANOMALY',$this->userGroupRoles) ){
                $anomalies->addChild(
                    'Piping',
                    ['route' => 'anomaly_pi', 'routeParameters' => ['projectId' => $projectId]]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_STRUCTURE_ANOMALY',$this->userGroupRoles) ){
                $anomalies->addChild(
                    'Structures',
                    ['route' => 'anomaly_st', 'routeParameters' => ['projectId' => $projectId]]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_STATICEQUIPMENT_ANOMALY',$this->userGroupRoles) ){
                $anomalies->addChild(
                    'Static Equipment',
                    ['route' => 'anomaly_se', 'routeParameters' => ['projectId' => $projectId]]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_MARINE_ANOMALY',$this->userGroupRoles) ){
                $anomalies->addChild('Marine',
                    ['route' => 'anomaly_mr', 'routeParameters' => ['projectId' => $projectId]]
                );
            }

            $master = $menu->addChild('Master Registers', ['uri' => '#'])
                ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');
            $master->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            if( $this->securityHelper->isRoleGranted('ROLE_ANOMALY',$this->userGroupRoles) && $this->securityHelper->isRoleGranted('ROLE_ANOMALY_MASTER',$this->userGroupRoles) ){
                $master->addChild(
                    'Anomaly Master',
                    [
                        'route' => 'anomaly_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_REPAIR_ORDER',$this->userGroupRoles) ){
                $master->addChild(
                    'Repair Order',
                    [
                        'route' => 'anomaly_ro_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_TEMPORARY_REPAIR_ORDER',$this->userGroupRoles) ){
                $master->addChild(
                    'Fabric Maintenance',
                    [
                        'route' => 'anomaly_fm_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
            if( $this->securityHelper->isRoleGranted('ROLE_FABRIC_MAINTENANCE_ORDER',$this->userGroupRoles) ){
                $master->addChild(
                    'Temporary Repair',
                    [
                        'route' => 'anomaly_tro_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
            //ROLE for Assessment
            if( $this->securityHelper->isRoleGranted('ROLE_ASSESSMENT_ORDER',$this->userGroupRoles) ){
            $master->addChild(
                'Assessment',
                [
                    'route' => 'anomaly_as_master',
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );
            }

            $management = $menu->addChild(
                'Activities Management',
                ['uri' => '#']
            )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');
            $management->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            if( $this->securityHelper->isRoleGranted('ROLE_PLANNING',$this->userGroupRoles) ){
                $management->addChild(
                    'Planning',
                    [
                        'route' => 'anomaly_planning_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }
            /*if( $this->securityHelper->isRoleGranted('ROLE_REMINDERS',$this->userGroupRoles) ){
                $management->addChild(
                    'Reminders',
                    [
                        'route' => 'anomaly_reminders_master',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
            }*/
            if( $this->securityHelper->isRoleGranted('ROLE_TOOLS_LOADSHEET',$this->userGroupRoles) || $this->securityHelper->isRoleGranted('ROLE_TOOLS_LOADSHEET',$this->userGroupRoles)){
                $toolsMenu = $menu->addChild('Tools', ['uri' => '#'])
                    ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                $toolsMenu->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
//                if( $this->securityHelper->isRoleGranted('ROLE_TOOLS_LOADSHEET',$this->userGroupRoles) ){
//                    $toolsMenu->addChild(
//                        'Test Point Analysis',
//                        [
//                            'route' => 'anomaly_compare_tp',
//                            'routeParameters' => ['projectId' => $projectId]
//                        ]
//                    );
//                }
                if( $this->securityHelper->isRoleGranted('ROLE_TOOLS_LOADSHEET',$this->userGroupRoles) ){
                    $toolsMenu->addChild(
                        'Master Load sheet',
                        [
                            'route' => 'anomaly_loadsheet',
                            'routeParameters' => ['projectId' => $projectId]
                        ]
                    );
                }
            }


        }

    }

    private function createProjectTopMenu(&$menu, $projectId, $dropdown = false)
    {
        $project = $this->entityManager->getRepository('AIEAnomalyBundle:Projects')->find($projectId);
        $projectMenu = $menu->addChild(
            'Project',
            [
                'label' => $project->getName(),
                'route' => 'anomaly_projects_show',
                'routeParameters' => ['id' => $projectId]
            ]
        );

        if ($dropdown) {
            //$projectMenu->setAttribute('dropdown', true)->setAttribute('class', 'dropdown');
            //$this->createProjectLeftMenu($projectMenu, $projectId);
        }
    }

    private function createActionTopMenu(&$menu, $anomalyId)
    {
        /** @var AnomalyRegistrar $anomaly */
        $anomaly = $this->entityManager->getRepository('AIEAnomalyBundle:AnomalyRegistrar')->find($anomalyId);
        $projectId = $anomaly->getProject()->getId();
        $type = strtolower($anomaly->getCode());

        $this->createProjectTopMenu($menu, $projectId, true);

        $menu->addChild(
            $anomaly->getFullId(),
            [
                'route' => 'anomaly_'.$type.'_show',
                'routeParameters' => ['projectId' => $projectId, 'id' => $anomaly->getId()]
            ]
        );


    }

    public function createBreadcrumbMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'breadcrumb');

        // this item will always be displayed

        return $menu;
    }

    public function createAnomalyLeftMenu(&$menu, $anomaly)
    {
        $project=$anomaly->getProject();
        if ($project->getIsSetup()['isSetup']){
            $projectId = $anomaly->getProject()->getId();
            $anomalyId = $anomaly->getId();
            $type = strtolower($anomaly->getCode());
            $flagIcon = '';

            switch  ($type) {
                case 'pl';
                    $masRegisterTypeLabel = 'Pipeline';
                    $flagIcon = 'road';
                    break;

                case 'pi';
                    $masRegisterTypeLabel = 'Piping';
                    $flagIcon = 'random';
                    break;

                case 'se';
                    $masRegisterTypeLabel = 'StaticEquipment';
                    $flagIcon = 'tower';
                    break;

                case 'st';
                    $masRegisterTypeLabel = 'Structure';
                    $flagIcon = 'home';
                    break;

                case 'mr';
                    $masRegisterTypeLabel = 'Marine';
                    $flagIcon = 'leaf';
                    break;

                case 'tro';
                    $masRegisterTypeLabel = 'Temporary Repair Order';
                    $flagIcon = 'time';
                    break;

                case 'ro';
                    $masRegisterTypeLabel = 'Repair Order';
                    $flagIcon = 'wrench';
                    break;

                case 'fm';
                    $masRegisterTypeLabel = 'Fabric Maintenance';
                    $flagIcon = 'filter';
                    break;

                case 'as';
                    $masRegisterTypeLabel = 'Assessment';
                    $flagIcon = 'signal';
                    break;

                default:
                    $masRegisterTypeLabel = $type;
                    break;
            }

            $menu->addChild(
                $masRegisterTypeLabel.' Register',
                [
                    'route' => 'anomaly_'.$type,
                    'routeParameters' => ['projectId' => $projectId]
                ]
            );

            $actionsList = $menu->addChild(
                'Action',
                ['uri' => '#']
            )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');

            $actionsList->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
            $ActionGranted=false;
            if( $this->securityHelper->isRoleGranted('ROLE_'.strtoupper($masRegisterTypeLabel).'_ANOMALY_NEW',$this->userGroupRoles) ){

                $actionsList->addChild(
                    'New Anomaly',
                    [
                        'route' => 'anomaly_'.$type.'_new',
                        'routeParameters' => ['projectId' => $projectId]
                    ]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_MONITOR',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Monitors',
                    [
                        'route' => 'anomaly_monitor',
                        'routeParameters' => ['projectId' => $projectId, 'anomalyId' => $anomalyId]
                    ]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_DOCUMENT',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Documents',
                    ['route' => 'anomaly_files', 'routeParameters' => ['refId' => $anomalyId]]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_REQUEST',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Request History',
                    [
                        'route' => 'anomaly_request_history',
                        'routeParameters' => ['id' => $anomaly->getId()]
                    ]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_'.strtoupper($masRegisterTypeLabel).'_ANOMALY_EDIT',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Edit',
                    [
                        'route' => 'anomaly_'.$type.'_edit',
                        'routeParameters' => ['projectId' => $projectId, 'id' => $anomalyId]
                    ]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_'.strtoupper($masRegisterTypeLabel).'_ANOMALY_DUPLICATE',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Duplicate Anomaly',
                    [
                        'route' => 'anomaly_'.$type.'_duplicate',
                        'routeParameters' => ['projectId' => $projectId, 'id' => $anomalyId]
                    ]
                );
                $ActionGranted=true;
            }
            if (!$ActionGranted){
                $menu->removeChild($actionsList);
            }
        }
    }

    public function createActionLeftMenu(&$menu, $action)
    {
        $project=$action->getAnomaly()->getProject();
        if ($project->getIsSetup()['isSetup']){
            $anomaly = $action->getAnomaly();
            $projectId = $anomaly->getProject()->getId();
            $anomalyId = $anomaly->getId();
            $actionId = $action->getId();

            $type = strtolower($action->getCode());
            $anomalyType = strtolower($anomaly->getCode());

            switch  ($action->getCode()) {
                case 'AN';
                    $masRegisterTypeLabel = 'Anomaly';
                    $masRegisterRole      = 'ANOMALY';
                    break;

                case 'RO';
                    $masRegisterTypeLabel = 'Repair Order';
                    $masRegisterRole      = 'REPAIR_ORDER';
                    break;

                case 'TRO';
                    $masRegisterTypeLabel = 'Temporary Repair Order';
                    $masRegisterRole      = 'TEMPORARY_REPAIR_ORDER';
                    break;

                case 'FM';
                    $masRegisterTypeLabel = 'Fabric Maintenance Order';
                    $masRegisterRole      = 'FABRIC_MAINTENANCE_ORDER';
                    break;

//            case 'AS';
//                $masRegisterTypeLabel = 'Fabric Maintenance Order';
//                $masRegisterRole      = 'FABRIC_MAINTENANCE_ORDER';
//                break;

                default:
                    $masRegisterTypeLabel = $action->getCode();
                    $masRegisterRole      = '';
                    break;
            }

            switch  ($anomaly->getCode()) {
                case 'pl';
                    $masRegister = 'PIPELINE_ANOMALY';
                    break;

                case 'pi';
                    $masRegister = 'PIPING_ANOMALY';
                    break;

                case 'se';
                    $masRegister = 'STATIC_EQUIPMENT_ANOMALY';
                    break;

                case 'st';
                    $masRegister = 'STRUCTURE_ANOMALY';
                    break;

                case 'mr';
                    $masRegister = 'MARINE_ANOMALY';
                    break;

                default:
                    $masRegister = $type;
                    break;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_'.$masRegisterRole,$this->userGroupRoles) ){
                $menu->addChild(
                     AnomalyRegistrar::getAnomalyCategories()[strtoupper($anomalyType)] . ' ' .$masRegisterTypeLabel.' Register',
                    [
                        'route' => 'anomaly_'.$type,
                        'routeParameters' => ['projectId' => $projectId, 'type' => $anomalyType]
                    ]
                );
            }
            $actionsList = $menu->addChild(
                'Action',
                ['uri' => '#']
            )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                ->setLinkAttribute('aria-expanded','false')
                ->setLabelAttribute('class', 'caret');

            $actionsList->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);


            if ($type == 'tror') {
                if( $this->securityHelper->isRoleGranted('ROLE_MONITOR',$this->userGroupRoles) ){
                    $actionsList->addChild(
                        'Monitors',
                        [
                            'route' => 'action_monitor',
                            'routeParameters' => ['anomalyId' => $anomalyId, 'actionId' => $actionId]
                        ]
                    );
                }
            }
            $ActionGranted=false;
            if($this->securityHelper->isRoleGranted('ROLE_'.$masRegister,$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Anomaly '.$anomaly->getFullId(),
                    [
                        'route' => 'anomaly_'.$anomalyType.'_show',
                        'routeParameters' => ['projectId' => $projectId, 'id' => $anomalyId]
                    ]
                );
                $ActionGranted=true;
            }

            if($this->securityHelper->isRoleGranted('ROLE_DOCUMENT',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Documents',
                    ['route' => 'anomaly_files', 'routeParameters' => ['refId' => $anomalyId]]
                );
                $ActionGranted=true;
            }

            if( $this->securityHelper->isRoleGranted('ROLE_REQUEST',$this->userGroupRoles) ){
                $actionsList->addChild(
                    'Request History',
                    [
                        'route' => 'anomaly_request_history',
                        'routeParameters' => ['id' => $action->getId()]
                    ]
                );
                $ActionGranted=true;
            }


            if( $this->securityHelper->isRoleGranted('ROLE_'.$masRegisterRole.'_edit',$this->userGroupRoles) ){

                $actionsList->addChild(
                    'Edit',
                    [
                        'route' => 'anomaly_'.$type.'_edit',
                        'routeParameters' => ['projectId' => $projectId, 'id' => $actionId]
                    ]
                );
                $ActionGranted=true;
            }
            if (!$ActionGranted){
                $menu->removeChild($actionsList);
            }
        }

    }

    public function createAdminMenu(&$menu)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $basedata = $menu->addChild(
            'Base Data',
            array('uri' => '#')
        )->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
            ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
            ->setLinkAttribute('aria-expanded','false')
            ->setLabelAttribute('class', 'caret');
        $basedata->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
        if ($this->securityAuthorizationChecker->isGranted('ROLE_CONSEQUENCES_LIST') || $this->securityHelper->isRoleGranted('ROLE_CONSEQUENCES_LIST',$this->userGroupRoles)) {
            $basedata->addChild('Consequences', array('route' => "anomaly_consequences"));
        }

        $basedata->addChild('Threats', array('route' => "anomaly_threats"));

        $basedata->addChild('Class', array('route' => "anomaly_classifications"));

        $basedata->addChild('Design Codes', array('route' => "anomaly_designcode"));


        if ($this->securityAuthorizationChecker->isGranted('ROLE_POSITIONS') || $this->securityHelper->isRoleGranted('ROLE_POSITIONS',$this->userGroupRoles)) {
            $menu->addChild(
                'Positions',
                array('route' => 'anomaly_user_group_list'));
        }
        if ($this->securityAuthorizationChecker->isGranted('ROLE_USERS') || $this->securityHelper->isRoleGranted('ROLE_USERS',$this->userGroupRoles)) {
            $menu->addChild('Users', array('route' => 'anomaly_users'));
        }
    }
}
