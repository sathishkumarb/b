<?php
/**
 * Created by AIE
 * Date: 29/06/18
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyDesignCode;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class LoadsheetSourcesHelper
{
    private $em;
    private $projectId;
    private $defaultem;
    private $locationsArray;
    private $designCodeArray;
    private $designCodeKeyArray;
    private $actionOwnersArray;
    private $anomalyThreatsArray;
    private $anomalyClassesArray;
    private $risksArray;
    private $fmRisksArray;
    private $productionCriticalityArray;

    public function __construct($em, $defaultEm, $projectId)
    {
        $this->em = $em;
        $this->defaultem = $defaultEm;
        $this->projectId = $projectId;
        $this->locationsArray = $this->getLocationsArray();
        $this->designCodeArray = $this->getDesignCodesArray();
        $this->designCodeKeyArray = $this->getDesignCodesKeyArray();
        $this->actionOwnersArray = $this->getActionOwners();
        $this->anomalyThreatsArray = $this->getAnomalyThreatsArray();
        $this->anomalyClassesArray = $this->getAnomalyClassArray();
        $this->risksArray = $this->getRisksCriticalities();
        $this->fmRisksArray = $this->getFMRiskArray();
        $this->productionCriticalityArray = $this->getProductionCriticalityArray();
    }

    public function getCheckSourceArray($arg){

        if (is_array ($arg)){
            return $arg;
        }else{
            if ($arg=='locationsArray'){
                return $this->locationsArray;
            }elseif ($arg=='anomalyThreatsArray'){
                return $this->anomalyThreatsArray;
            }elseif ($arg=='anomalyClassesArray'){
                return $this->anomalyClassesArray;
            }elseif ($arg=='risksArray'){
                return $this->risksArray;
            }elseif ($arg=='fmRisksArray'){
                return $this->fmRisksArray;
            }elseif ($arg=='productionCriticalityArray'){
                return $this->productionCriticalityArray;
            }elseif ($arg=='actionOwnersArray'){
                return $this->actionOwnersArray;
            }elseif ($arg=='designCodeArray'){
                return $this->designCodeKeyArray;
            }else{
                return null;
            }
        }
    }

    public function defineSourcesSheetArray($refSheetName, $validationArray){
        $arr=[];
        $letter = "A";
        foreach ($validationArray as $item){
            if ($item['type']=="list" && !is_array($item['source']) && $item['source'] != null){
                $arr[$item['source']]=["cell"=>$letter . "1",
                    "range"=>$refSheetName."!" . $letter . "1:" . $letter . (is_array($this->{$item['source']}) && count($this->{$item['source']}) > 0 ? count($this->{$item['source']}) : "1"),
                    'data'=>$this->{$item['source']}];
                ++$letter;

            }
        }
        return $arr;
    }

    private function getDesignCodesKeyArray(){
        $entities = $this->em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->findByProject($this->projectId);
        $codes = [];
        array_walk(
            $entities,
            function (AnomalyDesignCode $code) use (&$codes) {
                $codes[$code->getType()][] = $code;
            }
        );
        $finalCodes=['Pipeline'=>[],'Piping'=>[],'Static Equipment'=>[],'Structure'=>[],'Marine'=>[]];
        foreach ($codes['PL'] as $item){
            array_push($finalCodes['Pipeline'],$item->getCode());
        }
        foreach ($codes['PI'] as $item){
            array_push($finalCodes['Piping'],$item->getCode());
        }
        foreach ($codes['SE'] as $item){
            array_push($finalCodes['Static Equipment'],$item->getCode());
        }
        foreach ($codes['ST'] as $item){
            array_push($finalCodes['Structure'],$item->getCode());
        }
        foreach ($codes['MR'] as $item){
            array_push($finalCodes['Marine'],$item->getCode());
        }
        return $finalCodes;

    }

    private function getDesignCodesArray(){
        $entities = $this->em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')->findByProject($this->projectId);
        $codes = [];
        array_walk(
            $entities,
            function (AnomalyDesignCode $code) use (&$codes) {
                $codes[$code->getType()][] = $code;
            }
        );
        $finalCodes2=[];
        foreach ($codes['PL'] as $item){
            array_push($finalCodes2,$item->getCode());
        }
        foreach ($codes['PI'] as $item){
            array_push($finalCodes2,$item->getCode());
        }
        foreach ($codes['SE'] as $item){
            array_push($finalCodes2,$item->getCode());
        }
        foreach ($codes['ST'] as $item){
            array_push($finalCodes2,$item->getCode());
        }
        foreach ($codes['MR'] as $item){
            array_push($finalCodes2,$item->getCode());
        }
        return $finalCodes2;

    }

    private function getLocationsArray(){
        $entities = $this->em->getRepository('AIEAnomalyBundle:Locations')->findByProject($this->projectId);
        $locations=[];
        foreach ($entities as $entity){
            array_push($locations,$entity->getName());
        }
        return $locations;
    }

    private function getAnomalyClassArray(){

        $entities = $this->em->getRepository('AIEAnomalyBundle:AnomalyClass')->findByProject($this->projectId);
        $classes=[];
        foreach ($entities as $entity){
            array_push($classes,$entity->getCode());
        }
        return $classes;
    }

    private function getAnomalyThreatsArray(){
        $entities = $this->em->getRepository('AIEAnomalyBundle:AnomalyThreat')->findByProject($this->projectId);
        $threats=[];
        foreach ($entities as $entity){
            array_push($threats,$entity->getDescription());
        }
        return $threats;
    }

    private function getRisksCriticalities(){
        $project = $this->em->getRepository('AIEAnomalyBundle:Projects')->find($this->projectId);
        if ($project instanceof RiskProject){
            $data=[];
            $riskChoices=AnomalyRegistrar::getRiskChoices();
            foreach ($riskChoices as $prob){
                foreach ($riskChoices as $con){
                    array_push($data,$prob.'x'.$con);
                }
            }
        }else{
            $data = $project->getEnabledCriticalityChoices();
        }
        return $data;
    }

    private function getFMRiskArray(){
        $project = $this->em->getRepository('AIEAnomalyBundle:Projects')->find($this->projectId);
        $data=[];
        if ($project instanceof CriticalityProject && $project->hasFMCriticality()){
            foreach ($project->getFMCriticalityArray() as $key=>$value){
                array_push($data,$value);
            }
        }
        return $data;
    }

    private function getProductionCriticalityArray(){
        $project = $this->em->getRepository('AIEAnomalyBundle:Projects')->find($this->projectId);
        $data=[];
        if ($project instanceof CriticalityProject && $project->hasProductionCriticality()){
            foreach ($project->getProductionCriticalityArray() as $key=>$value){
                array_push($data,$value);
            }
        }
        return $data;
    }

    private function getActionOwners(){

        $users = [];
        $actionOwners = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($this->projectId);
        foreach ($actionOwners as $ao){
            $actionOwnerInfo = $this->defaultem->getRepository('UserBundle:User')->find($ao->getUserId());
            array_push($users, $actionOwnerInfo->getUsername());
        }
        return $users;
    }

}