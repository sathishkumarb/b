<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 12:17
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

require_once('../vendor/tecnickcom/tcpdf/tcpdi.php');

class WorkpackCustomHelper extends \TCPDI{//\FPDI {

    private $topTitle;
    private $bottomTitle;
    private $reportNo;
    private $pageType;
    private $projectImage;
    private $isDevEnv;

    public function setHeaderInfo($projectImage,$isDevEnv,$topTitle,$bottomTitle,$reportNo,$pageType='P'){
        $this->topTitle=$topTitle;
        $this->bottomTitle=$bottomTitle;
        $this->reportNo=$reportNo;
        $this->pageType=$pageType;
        $this->projectImage=$projectImage;
        $this->isDevEnv=$isDevEnv;
    }

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    function url($url,$option = null) {

        $cURL = curl_init();

        if ($option) {
            curl_setopt($cURL, CURLOPT_URL, $url.$option);
        } else {
            curl_setopt($cURL, CURLOPT_URL, $url);
        }

        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURL, CURLOPT_AUTOREFERER, 1);
        curl_setopt($cURL, CURLOPT_HTTPGET, 1);
        curl_setopt($cURL, CURLOPT_VERBOSE, 0);
        curl_setopt($cURL, CURLOPT_HEADER, 0);
        curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cURL, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($cURL, CURLOPT_DNS_CACHE_TIMEOUT, 2);

        $output['page'] = curl_exec($cURL);
        $output['contentType'] = curl_getinfo($cURL, CURLINFO_CONTENT_TYPE);


        curl_close($cURL);

        return $output;

    }

    //Page header
    /**
     *
     */
    public function Header() {
        $image_file ='../web/assets/img/VAnomaly.png';
        $this->Image($image_file, 18, 25, 39, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetLineStyle( array( 'width' => 0.3, 'color' => array(0,0,0)));
        $this->Line(15,15,$this->getPageWidth()-15,15);
        $this->Line(15,40,$this->getPageWidth()-15,40);
        $this->Line($this->getPageWidth()-15,15,$this->getPageWidth()-15,$this->getPageHeight()-15);
        $this->Line(15,$this->getPageHeight()-15,$this->getPageWidth()-15,$this->getPageHeight()-15);
        $this->Line(15,15,15,$this->getPageHeight()-15);
        //logo separator
        $this->Line(60,15,60,40);

        $fileExt = ($this->isDevEnv ? pathinfo($this->projectImage,PATHINFO_EXTENSION) : pathinfo(parse_url($this->projectImage, PHP_URL_PATH), PATHINFO_EXTENSION));

        if ($fileExt == 'png' || $fileExt == 'jpg' || $fileExt == 'bmp'|| $fileExt || $fileExt == 'jpeg') {
            if(($this->isDevEnv && file_exists ($this->projectImage)) || (!$this->isDevEnv && $this->is_url_exist($this->projectImage))){
                if ($this->isDevEnv){
                    $imgInfo = getimagesize($this->projectImage);
                }else{
                    $fileUrl = $this->url($this->projectImage);
                    $imgInfo = getimagesizefromstring($fileUrl['page']);
                }
                $ratio = $imgInfo[1] / $imgInfo[0];
                if ($ratio > 1){
                    $h = 22;
                    $w = 23 / $ratio;
                }else{
                    $w = 23;
                    $h = 22 * $ratio;
                }
                $imgType = strtoupper($fileExt);
                if ($imgType == 'JPEG')
                    $imgType='JPG';
                if ($this->tocpage || $this->pageType=='P'){
                    $this->Image(($this->isDevEnv ? $this->projectImage : '@' . $fileUrl['page']), 123 + ((27-$w)/2), 15 + ((25-$h)/2), $w, $h, $imgType, '', 'T', false, 300, '', false, false, 0, false, false, false);
                }else{
                    $this->Image(($this->isDevEnv ? $this->projectImage : '@' . $fileUrl['page']), 210 + ((27-$w)/2), 15 + ((25-$h)/2), $w, $h, $imgType, '', 'T', false, 300, '', false, false, 0, false, false, false);
                }

            }
        }
        if ($this->tocpage || $this->pageType=='P'){
            $this->Line(60,27.5,123,27.5);
            $this->Line(123,15,123,40);
            $this->Line(150,15,150,40);
            $this->SetXY(60, 15);
            $this->SetFont('', 'B', 10);
            $this->MultiCell(123-60, 12.5, $this->topTitle , 0, 'C', 0, 0, '', '', true, 0, false, true, 12.5, 'M');
            $this->SetXY(61, (27.5));
            $this->MultiCell(123-62, 12.5, $this->bottomTitle , 0, 'C', 0, 0, '', '', true, 0, false, true, 12.5, 'M');
            $this->SetXY(161, 25);
            $this->SetFont('', '', 10);
            $this->Cell(36, 12.5, 'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
            $this->SetXY(150, 30);
            $this->Cell(45, 12.5, 'Report No.', 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
            $this->SetXY(150, 35);
            $this->Cell(45, 12.5, $this->reportNo, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
            //$this->SetXY(150, 37);
           // $this->Cell(45, 12.5, 'Rev. 0', 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
        }else{
            $this->Line(60,27.5,210,27.5);
            $this->Line(210,15,210,40);
            $this->Line(237,15,237,40);
            $this->SetXY(60, 15);
            $this->SetFont('', 'B', 10);
            $this->MultiCell(210-60, 12.5, $this->topTitle , 0, 'C', 0, 0, '', '', true, 0, false, true, 12.5, 'M');
            $this->SetXY(60, (27.5));
            $this->MultiCell(210-62, 12.5, $this->bottomTitle , 0, 'C', 0, 0, '', '', true, 0, false, true, 12.5, 'M');
            $this->SetXY(237+11, 25);
            $this->SetFont('', '', 10);
            $this->Cell(45-11, 12.5, 'Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages(), 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
            $this->SetXY(237, 30);
            $this->Cell(45, 12.5, 'Report No.', 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
            $this->SetXY(237, 35);
            $this->Cell(45, 12.5, $this->reportNo, 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
           // $this->SetXY(237, 37);
            //$this->Cell(45, 12.5, 'Rev. 0', 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
        }

        // Set font
    }

    // Page footer
    public function Footer() {
    }
}