<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 12:17
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

use AIE\Bundle\AnomalyBundle\Entity\ActionOwners;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Request;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class PDFReportHelper
{
    private $em;
    private $defaultEm;
    private $isDev;
    private $uploader;
    private $latestListNumber;
    public function __construct($em,$defaultEm,$env,$uploader)
    {
        $this->em=$em;
        $this->defaultEm=$defaultEm;
        $this->isDev=$env;
        $this->uploader=$uploader;
    }

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    public function getValue($val){
        if ($val == null || empty($val) || $val==""){
            return "-";
        }
        return $val;
    }

    public function createExecutiveSummary(& $pdf, $registers , $title, $dateFrame, $locationName, $summaryTable){
        $pdf->SetFont('', '', 12);
        $innerHtml='';
        foreach ($registers as $register){
            $anomaly = ($register instanceof AnomalyRegistrar ? $register : $register->getAnomaly());
            $innerHtml=$innerHtml . '<tr>
                <td style="border: 1px solid black;width: 13%;">' . $this->getValue($anomaly->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 13%;">' . $this->getValue($anomaly->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 18%;">' .  $this->getValue($anomaly->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' .
                ($register->getProject() instanceof RiskProject ? $this->getValue($anomaly->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($register->getCriticality() != null ? $this->getValue($register->getProject()->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-'))
                . '</td>
                <td style="border: 1px solid black;width: 13%;" cellpadding="1">' . $this->getValue($anomaly->getInspectionTechnique()) . '</td>    
                <td style="border: 1px solid black;width: 18%;" cellpadding="1">' . $this->getValue($anomaly->getInspectionRecommendations()) . '</td>          
                <td style="border: 1px solid black;width: 13%;" cellpadding="1">' .
                ($anomaly->getNextInspectionDate() != null ? $this->getValue(date_format($anomaly->getNextInspectionDate(),'d-M-y')) : '-')
                . '</td>
                
                </tr>';
        }

        $html = '<h1>Executive Summary</h1><div style="text-align:justify;">This workpack includes the ' . strtolower($title) . ' to be conducted in the period of '. $dateFrame . ' at ' .$locationName .'.<br/><br/>
            The table below represents the following information:<ul>
            <li>Locations where the ' . strtolower($title) . ' are due along with a brief description of the anomaly.</li>
            <li>Type of ' . strtolower($title) . ' to be performed for the respective anomaly (e.g. UT, PRT).</li>
            <li>Pre-arrangements required to perform the ' . strtolower($title) . ' (e.g. scaffolding).</li>
            <li>Date by which the ' . strtolower($title) . ' needs to be performed.</li>
            </ul><div style="font-size: 12px;"><table cellpadding="2">
            <thead>
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 13%;background-color:#dddddd;line-height: 250%;"><b>Anomaly ID</b></td>
            <td style="border: 1px solid black;width: 13%;background-color:#dddddd;line-height: 250%;"><b>Location</b></td>
            <td style="border: 1px solid black;width: 18%;background-color:#dddddd;line-height: 250%;"><b>Description</b></td>
            <td style="border: 1px solid black;width: 12%;background-color:#dddddd;"><b>Risk / Criticality</b></td>
            <td style="border: 1px solid black;width: 13%;background-color:#dddddd;"><b>Inspection Technique</b></td>
            <td style="border: 1px solid black;width: 18%;background-color:#dddddd;"><b>Inspection Requirements</b></td>
            <td style="border: 1px solid black;width: 13%;background-color:#dddddd;"><b>Inspection Date</b></td>
            </tr>
            <tbody>' . $innerHtml .
            '</tbody>
            </thead>
            </table><br/><div style="font-size: 14px;">Refer to Section 2 for more details of the ' . strtolower($title) . 's.</div></div>';
        $pdf->addPage('A4','P');
        $pdf->writeHTML($html, true, false, true, false, '');

        $html= '<div>The summary of the attached inspections/mitigation are as follows:</div>';
        $html = $html . '<div><table border="2"><tbody>
            <tr style="text-align: center;">
            <td style="border: 2px solid black;width: 25%;background-color:#dddddd;">Action</td>
            <td style="border: 2px solid black;width: 25%;background-color:#61a0ff;">Closed</td>
            <td style="border: 2px solid black;width: 25%;background-color:#00b050;">Planned</td>
            <td style="border: 2px solid black;width: 25%;background-color:#ff6161;">Overdue</td>
            </tr>
            <tr style="text-align: center;">
            <td style="border: 2px solid black;width: 25%;">' . $title . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['closed'] . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['planned'] . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['overdue'] . '</td>
            </tr>
            </tbody></table></div>';
        $pdf->writeHTML($html, true, false, true, false, '');
    }

    public function createTableOfContent(& $pdf, $pageNumber){

        $pdf->addTOCPage('A4','P');
        $pdf->writeHTML('<table border="0" cellpadding="0" cellspacing="0"><tr><td><h1 height="20">List of Contents</h1></td></tr></table>', true, false, true, false, '');
        $bookmark_templates = array();
        $bookmark_templates[0] = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="80%"><div style="font-weight:bold;font-size:11pt;color:black;">#TOC_DESCRIPTION#</div></td><td width="20%"><div style="font-size:11pt;color:black;" align="right">#TOC_PAGE_NUMBER#</div></td></tr></table>';
        $bookmark_templates[1] = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="3%">&nbsp;</td><td width="77%"><div style="font-size:11pt;color:black;">#TOC_DESCRIPTION#</div></td><td width="20%"><div style="font-size:11pt;color:black;" align="right">#TOC_PAGE_NUMBER#</div></td></tr></table>';
        $bookmark_templates[2] = '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="6%">&nbsp;</td><td width="74%"><div style="font-size:10pt;font-weight: normal;font-style: normal;"><i>#TOC_DESCRIPTION#</i></div></td><td width="20%"><div style="font-size:10pt;" align="right">#TOC_PAGE_NUMBER#</div></td></tr></table>';
        //$pdf->addHTMLTOC($pageNumber, '', $bookmark_templates, true, '', array(0,0,0));

        $pdf->SetFont('', '', 11);
        $pdf->addTOC($pageNumber, 'helvetica', '.', '', 'B', array(0,0,0));
        $pdf->endTOCPage();
    }

    public function createAppendixCoverPage(& $pdf, $title, $bookmarkLevel, $textColor = [122,122,122],$setBookmark = false){
        $pdf->addPage('A4','P');
        $pdf->SetXY(40,50);
        $pdf->SetFont('', '', 20);
        if ($setBookmark)
            $pdf->Bookmark($title, $bookmarkLevel, 0, '', '', array(0,0,0));
        $pdf->SetTextColor($textColor[0],$textColor[1],$textColor[2]);
        $pdf->MultiCell($pdf->getPageWidth()-80, $pdf->getPageHeight()-100, $title, 0, 'C', 0, 0, '', '', true, 0, false, true, $pdf->getPageHeight()-100, 'M');
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('', '', 12);
    }

    public function createReportCover (& $pdf, $firstTop, $secondTop, $projectLogo,$isDevEnv, $coverArray){
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->addPage();
        $pdf->SetLineStyle( array( 'width' => 0.3, 'color' => array(0,0,0)));
        $pdf->Line(15,15,$pdf->getPageWidth()-15,15);
        $pdf->Line($pdf->getPageWidth()-15,15,$pdf->getPageWidth()-15,$pdf->getPageHeight()-15);
        $pdf->Line(15,$pdf->getPageHeight()-15,$pdf->getPageWidth()-15,$pdf->getPageHeight()-15);
        $pdf->Line(15,15,15,$pdf->getPageHeight()-15);

        $shiftDown = 5;

        $pdf->SetLineStyle( array( 'width' => 1, 'color' => array(23,54,93)));
        $pdf->Line($pdf->getPageWidth()/2 + 5,35,$pdf->getPageWidth()-17,35);

        $image_file ='../web/assets/img/logo.bk.png';
        $pdf->Image($image_file, 17, 22, 30, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $image_file ='../web/assets/img/VAnomaly.png';
        $pdf->Image($image_file, 17, 32, $pdf->getPageWidth()/2 - 40, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $image_file ='../web/assets/img/VAReportCoverText.jpg';
        $pdf->Image($image_file, 15.2, 45+$shiftDown, $pdf->getPageWidth()-30.4, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        if ($shiftDown > 0)
            $pdf->Line(15.4,44.5+$shiftDown,$pdf->getPageWidth()-15.4,44.5+$shiftDown);

        $rectangleStyle = array('width' => 0.4, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
        $pdf->SetAlpha(0.5);
        //$pdf->Rect(30, 55+$shiftDown, 60, 10, 'DF', array('all' => $rectangleStyle),array(127,127,127));
        //$pdf->Rect(30, 66+$shiftDown, 70, 10, 'DF');
        $pdf->SetAlpha(1);

        $fileExt = ($isDevEnv ? pathinfo($projectLogo,PATHINFO_EXTENSION) : pathinfo(parse_url($projectLogo, PHP_URL_PATH), PATHINFO_EXTENSION));
        $fileExt = strtolower($fileExt);
        if ($fileExt == 'png' || $fileExt == 'jpg' || $fileExt == 'bmp'|| $fileExt || $fileExt == 'jpeg') {
            if(($isDevEnv && file_exists ($projectLogo)) || (!$isDevEnv && $this->is_url_exist($projectLogo))){
                if ($isDevEnv){
                    $imgInfo = getimagesize($projectLogo);
                }else{
                    $fileUrl = $this->url($projectLogo);
                    $imgInfo = getimagesizefromstring($fileUrl['page']);
                }
                $ratio = $imgInfo[1] / $imgInfo[0];
                if ($ratio > 1){
                    $h = 20;
                    $w = 20 / $ratio;
                }else{
                    $w = 20;
                    $h = 20 * $ratio;
                }
                $xPos = $pdf->getPageWidth()-30-$w;
                $yPos = (100+ $shiftDown + 20) + (20/2 - $h/2);
                $rectangleStyle2 = array('width' => 0.8, 'cap' => 'square', 'join' => 'miter', 'dash' => 0, 'color' => array(127,127,127));
                $pdf->Rect($xPos, $yPos , $w, $h, 'DF', array('all' => $rectangleStyle2),array(0,0,0));
                $imgType = strtoupper($fileExt);
                if ($imgType == 'JPEG')
                    $imgType='JPG';
                $pdf->Image(($isDevEnv ? $projectLogo : '@' . $fileUrl['page']), $xPos, $yPos, $w, $h, $imgType, '', 'T', false, 300, '', false, false, 0, false, false, false);

            }
        }
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('', '', 20);
        $pdf->SetXY(30,64+$shiftDown);
        //$pdf->Cell(60, 10,'A complete defect' , 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');
        $pdf->SetXY(30,75+$shiftDown);
        //$pdf->Cell(70, 10,'management solution' , 0, $ln=0, 'C', 0, '', 0, false, 'B', 'B');

        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('', '', 10);
        $pdf->SetXY($pdf->getPageWidth(), 28);
        $pdf->Cell('', '',$firstTop , 0, $ln=0, 'R', 0, '', 0, false, 'B', 'B');
        $pdf->SetXY($pdf->getPageWidth(), 33);
        $pdf->Cell('', '',$secondTop , 0, $ln=0, 'R', 0, '', 0, false, 'B', 'B');

        $currentY = 100+$shiftDown;
        foreach ($coverArray as $ca){
            $yVal = ($ca['size'] == 'L' ? 20 : 20);
            $pdf->SetFont('', 'B', 14);
            $pdf->SetXY(30, $currentY);
            $pdf->MultiCell($pdf->getPageWidth()/2 -30, $yVal, $ca['caption'] , 0, 'L', 0, 0, '', '', true, 0, false, true, $yVal, 'M');
            $pdf->SetFont('', '', 14);
            $pdf->SetXY($pdf->getPageWidth()/2-30, $currentY);
            if ($ca['hasImage']){
                $pdf->MultiCell($pdf->getPageWidth()/2-30, $yVal, $ca['value'] , 0, 'L', 0, 1, '', '', true, 0, false, true, $yVal, 'M');
            }else{
                $pdf->MultiCell($pdf->getPageWidth()/2, $yVal, $ca['value'] , 0, 'L', 0, 1, '', '', true, 0, false, true, $yVal, 'M');
            }
            $currentY = $currentY + $yVal;

        }

        $pdf->SetXY(20, 220);
        $pdf->SetFont('', '', 12);
        $revTblHtml = '<table><tbody>
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 16%;height:40px;background-color:#dddddd;line-height: 250%;"><b>Rev</b></td>
            <td style="border: 1px solid black;width: 17%;height:40px;background-color:#dddddd;line-height: 250%;"><b>Date</b></td>
            <td style="border: 1px solid black;width: 16%;height:40px;background-color:#dddddd;"><b>Reason for Issue</b></td>
            <td style="border: 1px solid black;width: 17%;height:40px;background-color:#dddddd;line-height: 250%;"><b>Prepared</b></td>
            <td style="border: 1px solid black;width: 16%;height:40px;background-color:#dddddd;line-height: 250%;"><b>Checked</b></td>
            <td style="border: 1px solid black;width: 18%;height:40px;background-color:#dddddd;line-height: 250%;"><b>Approved</b></td>
            </tr>
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 18%;height:40px;"></td>
            </tr>
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 18%;height:40px;"></td>
            </tr>
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 17%;height:40px;"></td>
            <td style="border: 1px solid black;width: 16%;height:40px;"></td>
            <td style="border: 1px solid black;width: 18%;height:40px;"></td>
            </tr>
            </tbody></table>';
        $pdf->writeHTML($revTblHtml, true, false, true, false, '');

        $pdf->SetPrintHeader(true);
        $pdf->SetPrintFooter(true);
    }

    public function createReportMainContent(& $pdf, $docType , $docPeriod, $reportsCount, $summaryTable, $project){
        $clientName = $project->getClient();
        $docType = str_replace(' Workpack', '',$docType);
        $docType = str_replace(' Inspection', ' Monitoring Inspection',$docType);
        $tagvs = array('li' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
        $pdf->setHtmlVSpace($tagvs);
        $tagvs = array('ul' => array(0 => array('h' => 0.5, 'n' => 1), 1 => array('h' => 0, 'n' => 0)));
        $pdf->setHtmlVSpace($tagvs);
        $tagvs = array('div' => array(0 => array('h' => 1.3, 'n' => 1), 1 => array('h' => 1.30, 'n' => 1)));
        $pdf->setHtmlVSpace($tagvs);

        $pdf->addPage();
        $pdf->Bookmark('1 Introduction', 0, 0, '', '', array(0,0,0));
        $html='<table><tr><td width="8.5%"><h1>1</h1></td><td><h1>Introduction</h1></td></tr></table>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Bookmark('1.1 Scope of Work', 1, 0, '', '', array(0,0,0));
        $html= '<table><tr><td width="8.5%"><h3>1.1</h3></td><td><h3>Scope of Work</h3></td></tr></table>';
        $html = $html . '<div style="text-align:justify;">This work pack covers the ' . $docType . 's which are due for the period of ' . $docPeriod
            . '. The ' . $docType . ' register is provided in Section 2.'  . '</div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Bookmark('1.2 Work Pack Content', 1, 0, '', '', array(0,0,0));
        $html = '<div></div><table><tr><td width="8.5%"><h3>1.2</h3></td><td><h3>Work Pack Content</h3></td></tr></table>';
        $html = $html . '<div style="text-align:justify;">The content of this Work Pack includes:</div>
            <ul style="text-align:justify;">';
        if($docType == 'Anomaly Monitoring Inspection'){
            $html = $html . '<li>List of Anomalies that are due from ' . $docPeriod . '.</li>
            <li style="text-align:justify;">Isometric, GA Drawing or defect schematic drawing of the anomalies as required to identify the
             exact location of the defect on the component relative to major structural features. 
            These are included along with the respective Anomaly Reports.</li>
            </ul><div style="text-align:justify;">The details in these documents shall give clear instructions regarding the 
            location and physical measurements of the anomaly that must be monitored.
            </div>';
        }else{
            $html = $html . '<li style="text-align:justify;">List of ' . $docType . 's due in the time-frame ' . $docPeriod . '.</li>
            <li style="text-align:justify;">' . $docType . ' reports with the limits and details of the anomaly.</li>
            <li style="text-align:justify;">Recommendations to carry out the ' . $docType . '.</li>
            <li style="text-align:justify;">Isometric, GA Drawing or defect schematic drawing of the anomaly as 
            required to identify the exact location of the ' . $docType . '.</li>
            </ul><div style="text-align:justify;">The details in these documents shall give clear instructions regarding the 
            location and physical measurements of the anomaly that must be mitigated.
            </div>';
        }
        $pdf->writeHTML($html, true, false, true, false, '');

        //$pdf->Bookmark('1.3 Anomaly Management Process', 1, 0, '', '', array(0,0,0));
        $html = '<div></div><table><tr><td width="8.5%"><h3>1.3</h3></td><td><h3>Anomaly Management Process</h3></td></tr></table>';
        $html = $html . '<div style="text-align:justify;">Anomalies are identified and registered when inspection results reveal conditions of non-conformance during any inspection and testing activity. Note: Critical findings during inspections must be reported within 24 hours of its discovery to the Integrity Technical Authority.
        <br />After identification, verification, and assessment, the anomaly is registered in Veracity Anomaly where the corrective actions are approved. This may include Anomaly Monitoring Inspection, Assessment, Repair Order, Temporary Repair Orders and/or Fabric Maintenance.
        <br />Technical due date is assigned and approved for each corrective action (Assessment, RO, TRO & FM) by the assigned member of the project in Veracity Anomaly. The deferral process shall be followed for cases where the due date cannot be met.</div>';
        //$pdf->writeHTML($html, true, false, true, false, '');

        if($docType == 'Anomaly Monitoring Inspection'){
            $pdf->Bookmark('1.3 Anomaly Monitoring Process', 1, 0, '', '', array(0,0,0));
            $html = '<div></div><table><tr><td width="8.5%"><h3>1.3</h3></td><td><h3>Anomaly Monitoring Process</h3></td></tr></table>';
            $html = $html . '<div style="text-align:justify;">The Anomaly Monitoring Inspection Process is managed through Veracity Anomaly as follows:</div>
            <div style="text-align:justify;"><ul>
            <li style="text-align:justify;">Alert received via Veracity Anomaly as the due date for an Anomaly Inspection approaches.</li>
            <li style="text-align:justify;">A work pack (this document) with instruction and details (such as drawings) for the inspection of each anomaly is generated by Veracity Anomaly.</li>
            <li style="text-align:justify;">This work pack is then issued to the inspection engineer for impelmentation.</li>
            <li style="text-align:justify;">After the issuance of the Work Pack, the procedure is transferred to the team responsible for on-site inspection and data collection.</li>
            <li style="text-align:justify;">Following data collection, reporting shall be completed, reviewed and approved as per ' .  $clientName . '’s requirements.</li>
            <li style="text-align:justify;">The reviewed and approved data will then be entered back into Veracity Anomaly; the status of the anomaly (closed, cancelled) and/or the frequency of the anomaly inspections can be altered at this stage.</li>
            <li style="text-align:justify;">Veracity Anomaly will then send a request for approval to the assigned team member(s).</li>
            <li style="text-align:justify;">Following the approval phase, the status of the anomaly is updated.</li>
            </ul></div><div></div>';
        }else{
            $pdf->Bookmark('1.3 ' . $docType . ' Execution Process', 1, 0, '', '', array(0,0,0));
            $html = '<div></div><table><tr><td width="8.5%"><h3>1.3</h3></td><td><h3>' . $docType . ' Execution Process</h3></td></tr></table>';
            $html = $html . '<div style="text-align:justify;">The ' . $docType . ' is monitored through Veracity Anomaly as follows:</div>
            <div><ul style="text-align:justify;">
            <li style="text-align:justify;">After approval of the ' . $docType . ' on Veracity Anomaly, the work pack is generated at the end of each week.</li>
            <li style="text-align:justify;">Work Pack is generated (this document) with instruction and details (including the drawings) for the respective  ' . $docType . ' .</li>
            <li style="text-align:justify;">After the issuance of the Work Pack, the process is transferred to the team responsible for the on-site repair.</li>
            <li style="text-align:justify;">Alert via Veracity Anomaly as the due date for the ' . $docType . ' approaches.</li>
            <li style="text-align:justify;">Following completion of the mitigation, reporting shall be completed, reviewed and approved as per ' .  $clientName . '’s requirements.</li>
            <li style="text-align:justify;">' . $docType . ' close-out/completion details will then be entered in Veracity Anomaly along with the attachments of any related report/document.</li>
            <li style="text-align:justify;">' . $docType . ' close-out shall be sent for approval to the assigned member of the project along with a notification via Veracity Anomaly.</li>
            </ul></div>';
        }
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->addPage();
        $pdf->Bookmark('1.4 Safety Statement', 1, 0, '', '', array(0,0,0));
        $html = '<table><tr><td width="8.5%"><h3>1.4</h3></td><td><h3>Safety Statement</h3></td></tr></table>';
        $html = $html . '<div style="text-align:justify;">Safety is of primary importance when carrying out any inspection or repair activities. All work should be carried out in a safe manner and in accordance with the risk assessment results and permit requirements at the client\'s facilities.</div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        //$pdf->Bookmark('1.6 Summary of Activities', 1, 0, '', '', array(0,0,0));
        $html= '<div></div><table><tr><td width="8.5%"><h3>1.6</h3></td><td><h3>Summary of Activities</h3></td></tr></table>
<div>Summary of activities over selected date range:<br/>No. of ' . $reportsCount . ' Reports attached:</div>';
        $html = $html . '<br/><table border="2"><tbody>
            <tr style="text-align: center;">
            <td style="border: 2px solid black;width: 25%;background-color:#dddddd;">Action</td>
            <td style="border: 2px solid black;width: 25%;background-color:#61a0ff;">Closed</td>
            <td style="border: 2px solid black;width: 25%;background-color:#00b050;">Planned</td>
            <td style="border: 2px solid black;width: 25%;background-color:#ff6161;">Overdue</td>
            </tr>
            <tr style="text-align: center;">
            <td style="border: 2px solid black;width: 25%;">' . $docType . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['closed'] . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['planned'] . '</td>
            <td style="border: 2px solid black;width: 25%;">' . $summaryTable['overdue'] . '</td>
            </tr>
            </tbody></table>';
        //$pdf->writeHTML($html, true, false, true, false, '');
    }

    public function createAnomalyRegister(& $pdf, $type, $registers, $project){
        $pdf->AddPage('L', 'A4');

        if ($type == 'IN'){
            $pdf->Bookmark('2 Anomaly Inspection Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Anomaly Inspection Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require inspection:</div><br/>', true, false, true, false, '');
            $html = $this->createAnomalyInspectionRegistersTable($registers,$project);
        }elseif ($type == 'RO'){
            $pdf->Bookmark('2 Repair Order Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Repair Order Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require repair:</div><br/>', true, false, true, false, '');
            $html = $this->createRORegistersTable($registers,$project);
        }elseif ($type == 'TRO'){
            $pdf->Bookmark('2 Temporary Repair Order Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Temporary Repair Order Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require temporary repair:</div><br/>', true, false, true, false, '');
            $html = $this->createTRORegistersTable($registers,$project);
        }elseif ($type == 'FM') {
            $pdf->Bookmark('2 Fabric Maintenance Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Fabric Maintenance Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require fabric maintenance:</div><br/>', true, false, true, false, '');
            $html = $this->createFMRegistersTable($registers,$project);
        }elseif ($type == 'AS') {
            $pdf->Bookmark('2 Assessment Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Assessment Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require assessment:</div><br/>', true, false, true, false, '');
            $html = $this->createASRegistersTable($registers,$project);
        }else{
            $pdf->Bookmark('2 Anomaly Inspection Register', 0, 0, '', '', array(0,0,0));
            $pdf->writeHTML('<table><tr><td width="6%"><h2>2</h2></td><td><h2>Anomaly Inspection Register</h2></td></tr></table>', true, false, true, false, '');
            $pdf->writeHTML('<div>The following table shows the summary of the anomalies which require inspection:</div><br/>', true, false, true, false, '');
            $html = $this->createAnomalyInspectionRegistersTable($registers,$project);
        }

        $pdf->writeHTML($html, true, false, true, false, '');
    }

    public function createAnomalyInspectionRegistersTable($registers, $project){
        $riskHtml = '';
        if ($project instanceof CriticalityProject){
            $checkVal = false;
            foreach ($project->getEnabledCriticalityChoices() as $index=>$value){
                if($index==0){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[0] != 'Very Low'){
                        $checkVal=true;
                        $riskHtml=$riskHtml. $project->getEnabledCriticalityChoices()[0].  ': Very Low | ';
                    }
                }
                if($index==1){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[1] != 'Low') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[1] . ': Low | ';
                    }
                }
                if($index==2){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[2] != 'Medium') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[2] . ': Medium | ';
                    }
                }
                if($index==3){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[3] != 'High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[3] . ': High | ';
                    }
                }
                if($index==4){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[4] != 'Very High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[4] . ': Very High | ';
                    }
                }
            }
            if($checkVal)
                $riskHtml= '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Criticality Definition:</b> ' . substr($riskHtml, 0, -3) . '</td></tr>';
        }else{
            $riskHtml = '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Risk Definition:</b> VL: Very Low | L: Low | M: Medium | H: High | VH: Very High</td></tr>';
        }
        $html = '<table style="font-size: 8px;"><thead> '. $riskHtml . '
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Anomaly ID</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;">Location / Pipeline</td>
            <td style="border: 1px solid black;width: 12%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Description</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;">Nominal WT (mm)</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;">Minimum Allowable WT (mm)</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;">Anomaly Class</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Test Point</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Risk / Criticality</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;">Latest Inspection Technique</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Inspection Date</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;">Latest Inspection Results</td>
            </tr></thead><tbody>';
        foreach ($registers as $register){
            $html=$html . '<tr>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' .  $this->getValue($register->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getNominalWT()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getMAWT()) . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getClassification()->getCode()) . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getLocationPoint()) . '</td>
                <td style="border: 1px solid black;width: 8%;">' .
                ($project instanceof RiskProject ? $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($project->hasFMCriticality() && $register->getIsFabricMaintenanceCriticality()
                        ? $project->getFMCriticalityArray()[$register->getFabricMaintenanceCriticality()]
                        : ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-')
                    )
                )
                . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getInspectionTechnique()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' .
                ($register->getNextInspectionDate() != null ? $this->getValue(date_format($register->getNextInspectionDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getInspectionResults()) . '</td>
                </tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;
    }

    public function createRORegistersTable($registers, $project){
        $riskHtml = '';
        if ($project instanceof CriticalityProject){
            $checkVal = false;
            foreach ($project->getEnabledCriticalityChoices() as $index=>$value){
                if($index==0){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[0] != 'Very Low'){
                        $checkVal=true;
                        $riskHtml=$riskHtml. $project->getEnabledCriticalityChoices()[0].  ': Very Low | ';
                    }
                }
                if($index==1){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[1] != 'Low') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[1] . ': Low | ';
                    }
                }
                if($index==2){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[2] != 'Medium') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[2] . ': Medium | ';
                    }
                }
                if($index==3){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[3] != 'High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[3] . ': High | ';
                    }
                }
                if($index==4){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[4] != 'Very High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[4] . ': Very High | ';
                    }
                }
            }
            if($checkVal)
                $riskHtml= '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Criticality Definition:</b> ' . substr($riskHtml, 0, -3) . '</td></tr>';
        }else{
            $riskHtml = '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Risk Definition:</b> VL: Very Low | L: Low | M: Medium | H: High | VH: Very High</td></tr>';
        }
        $html = '<table style="font-size: 8px;"><thead> '. $riskHtml . '
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Anomaly ID</td>
            <td style="border: 1px solid black;width: 7%;font-weight:bold;background-color:#dddddd;">Location / Pipeline</td>
            <td style="border: 1px solid black;width: 12%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Description</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Nominal WT (mm)</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Minimum Allowable WT (mm)</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Test Point</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Risk / Criticality</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Technical Expiry Date</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Location Details</td>
            <td style="border: 1px solid black;width: 11%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Repair Priority</td>
            <td style=": 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Close-out Date</td>
            <td style="borderborder: 1px solid black;width: 11%;background-color:#dddddd;line-height: 250%;">Close-out Justification</td>
            </tr></thead><tbody>';
        foreach ($registers as $register){
            $html=$html . '<tr>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 7%;">' . $this->getValue($register->getAnomaly()->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' . $this->getValue($register->getAnomaly()->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' . $this->getValue($register->getAnomaly()->getNominalWT()) . '</td>
                <td style="border: 1px solid black;width: 9%;">' . $this->getValue($register->getAnomaly()->getMAWT()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getLocationPoint()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($project instanceof RiskProject ? $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($project->hasFMCriticality() && $register->getAnomaly()->getIsFabricMaintenanceCriticality()
                        ? $project->getFMCriticalityArray()[$register->getAnomaly()->getFabricMaintenanceCriticality()]
                        : ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-')
                    )
                )
                . '</td>
                <td style="border: 1px solid black;width: 10%;">' .
                ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getLocationDetails()) . '</td>
                <td style="border: 1px solid black;width: 11%;">' . $this->getValue($register->getRepairPriority()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' .
                ($register->getCloseOutDate() != null ? $this->getValue(date_format($register->getCloseOutDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 11%;">' . $this->getValue($register->getCloseOutJustification()) . '</td>
                </tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;
    }

    public function createTRORegistersTable($registers, $project){
        $riskHtml = '';
        if ($project instanceof CriticalityProject){
            $checkVal = false;
            foreach ($project->getEnabledCriticalityChoices() as $index=>$value){
                if($index==0){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[0] != 'Very Low'){
                        $checkVal=true;
                        $riskHtml=$riskHtml. $project->getEnabledCriticalityChoices()[0].  ': Very Low | ';
                    }
                }
                if($index==1){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[1] != 'Low') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[1] . ': Low | ';
                    }
                }
                if($index==2){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[2] != 'Medium') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[2] . ': Medium | ';
                    }
                }
                if($index==3){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[3] != 'High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[3] . ': High | ';
                    }
                }
                if($index==4){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[4] != 'Very High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[4] . ': Very High | ';
                    }
                }
            }
            if($checkVal)
                $riskHtml= '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Criticality Definition:</b> ' . substr($riskHtml, 0, -3) . '</td></tr>';
        }else{
            $riskHtml = '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Risk Definition:</b> VL: Very Low | L: Low | M: Medium | H: High | VH: Very High</td></tr>';
        }
        $html = '<table style="font-size: 8px;"><thead> '. $riskHtml . '
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Anomaly ID</td>
            <td style="border: 1px solid black;width: 7%;font-weight:bold;background-color:#dddddd;">Location / Pipeline</td>
            <td style="border: 1px solid black;width: 12%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Description</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Nominal WT (mm)</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Minimum Allowable WT (mm)</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Test Point</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Risk / Criticality</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Technical Expiry Date</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Repair Type</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Design Life</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">MOC Number</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Installation Date</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;">Latest Inspection Technique</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Inspection Date</td>
            <td style="border: 1px solid black;width: 8%;font-weight:bold;background-color:#dddddd;">Latest Inspection Results</td>
            </tr></thead><tbody>';
        foreach ($registers as $register){
            $html=$html . '<tr>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 7%;">' . $this->getValue($register->getAnomaly()->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' . $this->getValue($register->getAnomaly()->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' . $this->getValue($register->getAnomaly()->getNominalWT()) . '</td>
                <td style="border: 1px solid black;width: 9%;">' . $this->getValue($register->getAnomaly()->getMAWT()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getLocationPoint()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' .
                ($project instanceof RiskProject ? $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($project->hasFMCriticality() && $register->getAnomaly()->getIsFabricMaintenanceCriticality()
                        ? $project->getFMCriticalityArray()[$register->getAnomaly()->getFabricMaintenanceCriticality()]
                        : ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-')
                    )
                )
                . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getRepairType()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getDesignLife()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' . $this->getValue($register->getMocNumber()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($register->getInstallationDate() != null ? $this->getValue(date_format($register->getInstallationDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getInspectionTechnique()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($register->getNextInspectionDate() != null ? $this->getValue(date_format($register->getNextInspectionDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 8%;">' . $this->getValue($register->getInspectionResults()) . '</td>
                </tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;
        $html = $html . '</thead></table>';
    }

    public function createFMRegistersTable($registers, $project){
        $riskHtml = '';
        if ($project instanceof CriticalityProject){
            $checkVal = false;
            foreach ($project->getEnabledCriticalityChoices() as $index=>$value){
                if($index==0){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[0] != 'Very Low'){
                        $checkVal=true;
                        $riskHtml=$riskHtml. $project->getEnabledCriticalityChoices()[0].  ': Very Low | ';
                    }
                }
                if($index==1){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[1] != 'Low') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[1] . ': Low | ';
                    }
                }
                if($index==2){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[2] != 'Medium') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[2] . ': Medium | ';
                    }
                }
                if($index==3){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[3] != 'High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[3] . ': High | ';
                    }
                }
                if($index==4){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[4] != 'Very High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[4] . ': Very High | ';
                    }
                }
            }
            if($checkVal)
                $riskHtml= '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Criticality Definition:</b> ' . substr($riskHtml, 0, -3) . '</td></tr>';
        }else{
            $riskHtml = '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Risk Definition:</b> VL: Very Low | L: Low | M: Medium | H: High | VH: Very High</td></tr>';
        }
        $html = '<table style="font-size: 8px;"><thead> '. $riskHtml . '
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Anomaly ID</td>
            <td style="border: 1px solid black;width: 7%;font-weight:bold;background-color:#dddddd;">Location / Pipeline</td>
            <td style="border: 1px solid black;width: 12%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Description</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Nominal WT (mm)</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Minimum Allowable WT (mm)</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Test Point</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Risk / Criticality</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Technical Expiry Date</td>
            <td style="border: 1px solid black;width: 11%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Immediate Repair</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Long Isolation</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">In-Service</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Completion Date</td>
            </tr></thead><tbody>';
        foreach ($registers as $register){
            $html=$html . '<tr>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 7%;">' . $this->getValue($register->getAnomaly()->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' . $this->getValue($register->getAnomaly()->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' . $this->getValue($register->getAnomaly()->getNominalWT()) . '</td>
                <td style="border: 1px solid black;width: 9%;">' . $this->getValue($register->getAnomaly()->getMAWT()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getLocationPoint()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($project instanceof RiskProject ? $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($project->hasFMCriticality() && $register->getAnomaly()->getIsFabricMaintenanceCriticality()
                        ? $project->getFMCriticalityArray()[$register->getAnomaly()->getFabricMaintenanceCriticality()]
                        : ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-')
                    )
                )
                . '</td>
                <td style="border: 1px solid black;width: 9%;">' .
                ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 11%;">' . $this->getValue($register->getImmediateRepair()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getLongIsolation()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getInService()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' .
                ($register->getCompletionDate() != null ? $this->getValue(date_format($register->getCompletionDate(),'d-M-Y')) : '-')
                . '</td>
                </tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;
    }

    public function createASRegistersTable($registers, $project){
        $riskHtml = '';
        if ($project instanceof CriticalityProject){
            $checkVal = false;
            foreach ($project->getEnabledCriticalityChoices() as $index=>$value){
                if($index==0){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[0] != 'Very Low'){
                        $checkVal=true;
                        $riskHtml=$riskHtml. $project->getEnabledCriticalityChoices()[0].  ': Very Low | ';
                    }
                }
                if($index==1){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[1] != 'Low') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[1] . ': Low | ';
                    }
                }
                if($index==2){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[2] != 'Medium') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[2] . ': Medium | ';
                    }
                }
                if($index==3){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[3] != 'High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[3] . ': High | ';
                    }
                }
                if($index==4){
                    if ($riskHtml. $project->getEnabledCriticalityChoices()[4] != 'Very High') {
                        $checkVal = true;
                        $riskHtml = $riskHtml . $project->getEnabledCriticalityChoices()[4] . ': Very High | ';
                    }
                }
            }
            if($checkVal)
                $riskHtml= '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Criticality Definition:</b> ' . substr($riskHtml, 0, -3) . '</td></tr>';
        }else{
            $riskHtml = '<tr style="text-align: center;"><td style="border: 1px solid black;width: 100%;text-align: center;"><b>Risk Definition:</b> VL: Very Low | L: Low | M: Medium | H: High | VH: Very High</td></tr>';
        }
        $html = '<table style="font-size: 8px;"><thead> '. $riskHtml . '
            <tr style="text-align: center;">
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;">Anomaly ID</td>
            <td style="border: 1px solid black;width: 7%;font-weight:bold;background-color:#dddddd;">Location / Pipeline</td>
            <td style="border: 1px solid black;width: 12%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Description</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Nominal WT (mm)</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Minimum Allowable WT (mm)</td>
            <td style="border: 1px solid black;width: 5%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Test Point</td>
            <td style="border: 1px solid black;width: 6%;font-weight:bold;background-color:#dddddd;">Risk / Criticality</td>
            <td style="border: 1px solid black;width: 9%;font-weight:bold;background-color:#dddddd;">Technical Expiry Date</td>
            <td style="border: 1px solid black;width: 11%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Assessment Type</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Assessment Code</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;">Assessment Completion Date</td>
            <td style="border: 1px solid black;width: 10%;font-weight:bold;background-color:#dddddd;line-height: 250%;">Assessment Results</td>
            </tr></thead><tbody>';

        foreach ($registers as $register){
            $html=$html . '<tr>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getAnomalyCustomId()) . '</td>
                <td style="border: 1px solid black;width: 7%;">' . $this->getValue($register->getAnomaly()->getLocation()->getName()) . '</td>
                <td style="border: 1px solid black;width: 12%;">' . $this->getValue($register->getAnomaly()->getComponentDescription()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' . $this->getValue($register->getAnomaly()->getNominalWT()) . '</td>
                <td style="border: 1px solid black;width: 9%;">' . $this->getValue($register->getAnomaly()->getMAWT()) . '</td>
                <td style="border: 1px solid black;width: 5%;">' . $this->getValue($register->getAnomaly()->getLocationPoint()) . '</td>
                <td style="border: 1px solid black;width: 6%;">' .
                ($project instanceof RiskProject ? $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability())) :
                    ($project->hasFMCriticality() && $register->getAnomaly()->getIsFabricMaintenanceCriticality()
                        ? $project->getFMCriticalityArray()[$register->getAnomaly()->getFabricMaintenanceCriticality()]
                        : ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$register->getCriticality()]) : '-')
                    )
                )
                . '</td>
                <td style="border: 1px solid black;width: 9%;">' .
                ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 11%;">' . $this->getValue($register->getAssessmentType()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getAssessmentCode()) . '</td>
                <td style="border: 1px solid black;width: 10%;">' .
                ($register->getAssessmentDueDate() != null ? $this->getValue(date_format($register->getAssessmentDueDate(),'d-M-Y')) : '-')
                . '</td>
                <td style="border: 1px solid black;width: 10%;">' . $this->getValue($register->getAssessmentResult()) . '</td>
                </tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;

    }

    public function getLatestListNumber(){
        return $this->latestListNumber;
    }

    public function createRegisterReport(& $pdf,$register, $monitorImg,$itemNumber){
        $anomaly = ($register instanceof AnomalyRegistrar ? $register : $register->getAnomaly());
        $pdf->Bookmark('2.' . ($itemNumber). ' ' . $anomaly->getAnomalyCustomId(), 1, 0, '', '', array(0,0,0));
        $pdf->writeHTML('<table><tr><td width="8.5%"><h3>2.' . ($itemNumber) . '</h3></td><td><h3>' . $anomaly->getAnomalyCustomId() . '</h3></td></tr></table>', true, false, true, false, '');
        $pdf->writeHTML($this->setAnomlayInfo($anomaly), true, false, true, false, '');
        $pdf->writeHTML($this->setEquipmentInfo($anomaly), true, false, true, false, '');
        //$pdf->writeHTML($this->setWTInfo($anomaly), true, false, true, false, '');
        $pdf->writeHTML($this->setAnomalyLocationInfo($anomaly), true, false, true, false, '');
        $pdf->writeHTML($this->setAnomalyDescriptionInfo($anomaly), true, false, true, false, '');
        if($anomaly->getClassification()->getCode() == "EML" || $anomaly->getClassification()->getCode() == "IML"){
            $pdf->writeHTML($this->setMLInfo($anomaly), true, false, true, false, '');
        }else{
            if($anomaly->getTrackingCriteria() && count($anomaly->getTrackingCriteria()) > 0){
                $pdf->writeHTML($this->setTrackingCriteriaInfo($anomaly), true, false, true, false, '');
            }
        }
        $pdf->writeHTML($this->setAnomalyFailureSummary($anomaly), true, false, true, false, '');
        $pdf->writeHTML($this->setAttachedPictures($anomaly), true, false, true, false, '');
        $pdf->writeHTML($this->setRecommendations($anomaly), true, false, true, false, '');
        if ($register instanceof AnomalyRegistrar){
            $pdf->writeHTML($this->setRemdialActions($anomaly), true, false, true, false, '');
            $pdf->writeHTML($this->setInspectionInfo($anomaly), true, false, true, false, '');
            $pdf->writeHTML($this->setCloseOutInfo($anomaly), true, false, true, false, '');
        }elseif($register instanceof RepairOrderRegistrar){
            $pdf->writeHTML($this->setROInfo($register), true, false, true, false, '');
            $pdf->writeHTML($this->setROCloseOutInfo($register), true, false, true, false, '');
        }
        elseif($register instanceof TemporaryRepairOrderRegistrar){
            $pdf->writeHTML($this->setTROInfo($register), true, false, true, false, '');
        }elseif($register instanceof FabricMaintenanceRegistrar){
            $pdf->writeHTML($this->setFMInfo($register), true, false, true, false, '');
        }elseif($register instanceof AssessmentRegistrar){
            $pdf->writeHTML($this->setASInfo($register), true, false, true, false, '');
        }
        $pdf->writeHTML($this->setSignatureInfo($anomaly, $register), true, false, true, false, '');
        if ($register instanceof AnomalyRegistrar){
            if (count($anomaly->getMonitors()) > 0){
                if($anomaly->getClassification()->getCode() == "EML" || $anomaly->getClassification()->getCode() == "IML"){
                    $pdf->addPage();
                    $pdf->Bookmark('2.' .  $itemNumber . '.1 Anomaly Monitors', 2, 0, '', '', array(0,0,0));
                    $pdf->writeHTML('<table><tr><td width="8.5%"><h4>2.' .  $itemNumber . '.1</h4></td><td><h4>Anomaly Monitors</h4></td></tr></table>', true, false, true, false, '');
                    $pdf->writeHTML($this->setMonitorsCR($anomaly), true, false, true, false, '');
                    $this->latestListNumber=1;
                    if ($monitorImg != null){
                        $imginfo =getimagesizefromstring  ($monitorImg);
                        $pdfWidth = 170;
                        $ratio = $imginfo[1]/$imginfo[0];
                        $pdfHeight = $pdfWidth * $ratio;
                        $pdf->Bookmark('2.' .  $itemNumber . '.2 Monitors Trending Graph', 2, 0, '', '', array(0,0,0));
                        $pdf->writeHTML('<table><tr><td width="8.5%"><h4>2.' .  $itemNumber . '.2</h4></td><td><h4>Monitors Trending Graph</h4></td></tr></table>', true, false, true, false, '');
                        $pdf->Image('@'.$monitorImg,'','',$pdfWidth,$pdfHeight);
                        $this->latestListNumber=2;
                    }
                }else{
                    $pdf->addPage();
                    $pdf->Bookmark('2.' .  $itemNumber . '.1 Anomaly Monitors', 2, 0, '', '', array(0,0,0));
                    $pdf->writeHTML('<table><tr><td width="8.5%"><h4>2.' .  $itemNumber . '.1</h4></td><td><h4>Anomaly Monitors</h4></td></tr></table>', true, false, true, false, '');
                    $pdf->writeHTML($this->setMonitorsTrackingCriteria($anomaly), true, false, true, false, '');
                    $this->latestListNumber=1;
                    if ($monitorImg != null){
                        $imginfo =getimagesizefromstring  ($monitorImg);
                        $pdfWidth = 170;
                        $ratio = $imginfo[1]/$imginfo[0];

                        $pdfHeight = $pdfWidth * $ratio;
                        if ($pdfHeight>170)
                            $pdf->addPage();
                        $pdf->Bookmark('2.' .  $itemNumber . '.2 Monitors Trending Graph', 2, 0, '', '', array(0,0,0));
                        $pdf->writeHTML('<table><tr><td width="8.5%"><h4>2.' .  $itemNumber . '.2</h4></td><td><h4>Monitors Trending Graph</h3></td></tr></table>', true, false, true, false, '');
                        $pdf->Image('@'.$monitorImg,'','',$pdfWidth,$pdfHeight);
                        $this->latestListNumber=2;
                    }
                }
            }
        }
        return $pdf;
    }

    public function setAnomlayInfo($anomaly){
        $user=$this->defaultEm->getRepository('UserBundle:User')->find($anomaly->getSpa()->getUserId());
        $username= $user->getUsername();
        $html='<table cellpadding="1" style="font-size: 13px;">
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">Anomaly ID</td>
<td style="border: 1px solid black;width: 25%">' . $this->getValue($anomaly->getAnomalyCustomId()) . '</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">Risk / Criticality</td>
<td style="border: 1px solid black;width: 25%">' .
            ($anomaly->getProject() instanceof RiskProject ? $this->getValue($anomaly->getProject()->getRiskMatrix()->getRisk($anomaly->getConsequence(), $anomaly->getProbability())) :
                ($anomaly->getProject()->hasFMCriticality() && $anomaly->getIsFabricMaintenanceCriticality()
                    ? $anomaly->getProject()->getFMCriticalityArray()[$anomaly->getFabricMaintenanceCriticality()]
                    : ($anomaly->getCriticality() != null ? $this->getValue($anomaly->getProject()->getEnabledCriticalityChoices()[$anomaly->getCriticality()]) : '-')
                )
            )
            . '</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">Location / Pipeline</td>
<td style="border: 1px solid black;width: 25%">' .  $this->getValue($anomaly->getLocation()->getName()) . '</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">SPA</td>
<td style="border: 1px solid black;width: 25%">' .$username. '</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">System</td>
<td style="border: 1px solid black;width: 25%">' .  ($anomaly->getCode()!='PL'?$this->getValue($anomaly->getSystem()):'-') . '</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd">Outer Diameter (mm)</td>
<td style="border: 1px solid black;width: 25%">' .
            ($anomaly->getCode() == "PL" || $anomaly->getCode() == "PI"?
                $this->getValue($anomaly->getOuterDiameter()):'-')
            . '</td>
</tr>
</table>';

        return $html;
    }

    public function setEquipmentInfo($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="4" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Equipment Description</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Asset Tag Number</td>
<td style="border: 1px solid black;width: 25%;">' . $this->getValue($anomaly->getAssetTagNumber()) . '</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Component</td>
<td style="border: 1px solid black;width: 25%;">' . $this->getValue($anomaly->getComponent()) . '</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Description</td>
<td style="border: 1px solid black;width: 25%;">' . $this->getValue($anomaly->getComponentDescription()) . '</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Design Code</td>
<td style="border: 1px solid black;width: 25%;">' . $this->getValue($anomaly->getDesignCode()->getCode()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">SMYS (ksi)</td>
<td style="border: 1px solid black;width: 25%;">' .
            ($anomaly->getCode() == "PL" || $anomaly->getCode() == "PI" || $anomaly->getCode() == "SE"?
                $this->getValue( $anomaly->getSMYS()) : '-')
            .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">SMTS (ksi)</td>
<td style="border: 1px solid black;width: 25%;">' .
            ($anomaly->getCode() == "PL" || $anomaly->getCode() == "PI" || $anomaly->getCode() == "SE"?
                $this->getValue( $anomaly->getSMTS()) : '-')
            .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 50%;background-color:#dddddd;">Nominal Wall Thickness (NWT) (mm)</td>
<td colspan="2" style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getNominalWT()) .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 50%;background-color:#dddddd;">Minimum Allowable Wall Thickness (MAWT) (mm)</td>
<td colspan="2" style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getMAWT()) .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 50%;background-color:#dddddd;">Corrosion Allowance (CA) (mm)</td>
<td colspan="2" style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getCorrosionAllowance()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setWTInfo($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 17%;background-color:#dddddd;">Nominal Wall Thickness (NWT) (mm)</td>
<td style="border: 1px solid black;width: 16%;">' .$this->getValue( $anomaly->getNominalWT()) .'</td>
<td style="border: 1px solid black;width: 17%;background-color:#dddddd;">Minimum Allowable Wall Thickness (MAWT) (mm)</td>
<td style="border: 1px solid black;width: 16%;">' .$this->getValue( $anomaly->getMAWT()) .'</td>
<td style="border: 1px solid black;width: 18%;background-color:#dddddd;">Corrosion Allowance (CA) (mm)</td>
<td style="border: 1px solid black;width: 16%;">' .$this->getValue( $anomaly->getCorrosionAllowance()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setAnomalyLocationInfo($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="5" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Anomaly Location</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Test Point</td>
<td style="border: 1px solid black;width: 20%;">' .$this->getValue( $anomaly->getLocationPoint()) .'</td>
<td colspan="2" style="border: 1px solid black;width: 40%;background-color:#dddddd;">Orientation</td>
<td style="border: 1px solid black;width: 20%;">' .
            ($anomaly->getDefectOrientation() != null ? $this->getValue( date_format($anomaly->getDefectOrientation(), 'h:i')) : '-')
            .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">GPS Coordinates</td>
<td style="border: 1px solid black;width: 20%;">' .($anomaly->getCode() == "PL"? $this->getValue( $anomaly->getGpsE()): '-') .'</td>
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Easting</td>
<td style="border: 1px solid black;width: 20%;">' .($anomaly->getCode() == "PL"? $this->getValue( $anomaly->getGpsN()): '-') .'</td>
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Northing</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setAnomalyDescriptionInfo($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="4" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Anomaly Description and Dimensions</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Defect Description</td>
<td colspan="3" style="border: 1px solid black;width: 70%;">' .$this->getValue( $anomaly->getDefectDescription()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Anomaly Threat</td>
<td style="border: 1px solid black;width: 20%;">' .$this->getValue( $anomaly->getThreat()->getDescription()) .'</td>
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Anomaly Class</td>
<td style="border: 1px solid black;width: 20%;">' .$this->getValue( $anomaly->getClassification()->getDescription()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Inspection Tool Tolerence</td>
<td colspan="3" style="border: 1px solid black;width: 70%;">' .($anomaly->getCode() == "PL"? $this->getValue( $anomaly->getTolerance()): '-') .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Comments</td>
<td colspan="3" style="border: 1px solid black;width: 70%;">' .$this->getValue( $anomaly->getComments()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setMLInfo($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Remaining Wall Thickness (mm)</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue( $anomaly->getRemainingWallThickness()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Length (mm)</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue( $anomaly->getLength()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Retiral Value</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue( $anomaly->getRetiralValue()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Width (mm)</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue( $anomaly->getWidth()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Corrosion Rate (mm/yr)</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue( $anomaly->getCorrosionRate()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setTrackingCriteriaInfo($anomaly){
        $html = '';
        if($anomaly->getTrackingCriteria() && count($anomaly->getTrackingCriteria()) > 0){
            $html= $html . '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td rowspan="' . (count($anomaly->getTrackingCriteria()) + 1) . '" style="border: 1px solid black;width: 25%;background-color:#dddddd;">Tracking Criteria</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;text-align: center;">#</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;text-align: center;">Tracking Criteria</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;text-align: center;">Tracking CONC</td>
</tr>';
            $counter = 1;
            foreach ($anomaly->getTrackingCriteria() as $tc){
                $html = $html . '<tr style="text-align: left;">
                    <td style="border: 1px solid black;width: 25%;">' . $counter .'</td>
                    <td style="border: 1px solid black;width: 25%;">' .$this->getValue( $tc->getDescription()) .'</td>
                    <td style="border: 1px solid black;width: 25%;">' .$this->getValue($tc->getValue()) .'</td>
                    </tr>';
                $counter++;
            }

            $html = $html . '</tbody></table>';
        }

        return $html;
    }

    public function setAnomalyFailureSummary($anomaly){
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Anomaly Summary</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;height: 78px;">' .$this->getValue( $anomaly->getSummary()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Failure Summary</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;height: 78px;">' .$this->getValue( $anomaly->getFailureSummary()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setAttachedPictures($anomaly){
        ini_set('memory_limit', '-1');
        $imgHtml = '';
        $tdHeight = 10 ;
        $loopVal = 0;
        $fileUrl = '';
        $fileProductionUrl = '';
        foreach ($anomaly->getFiles() as $file) {

            if ($file->getFilereportCategory() != null && $file->getFilereportCategory() == 'Picture') {
                if ($this->isDev) {
                    $fileUrl = $file->getServerFilePath($this->isDev);
                } else {

                    $fileUrl = $this->uploader->getAbsoluteFilePath($file->getFilepath());
                    $fileProductionUrl = $this->uploader->getAbsoluteFilePath($file->getFilepath());
                }
            }

            if ($fileUrl && !is_array($fileUrl))
            {
                $fileExt = ($this->isDev ? pathinfo($fileUrl, PATHINFO_EXTENSION) : pathinfo(parse_url($fileUrl, PHP_URL_PATH), PATHINFO_EXTENSION));
                if (strtolower($fileExt) == 'png' || strtolower($fileExt) == 'jpg' || strtolower($fileExt) == 'bmp' || strtolower($fileExt) == 'gif' || strtolower($fileExt) == 'jpeg')
                {

                    if (($this->isDev && file_exists($fileUrl)) || (!$this->isDev && $this->is_url_exist($fileUrl)))
                    {
                        $maxWidthpx = 3.7795275591 * 150;
                        if ($this->isDev) {
                            $imgInfo = getimagesize($fileUrl);
                        } else {
                            $fileUrl = $this->url($fileUrl);
                            $imgInfo = getimagesizefromstring($fileUrl['page']);
                        }
                        if ($imgInfo[0] > $maxWidthpx) {
                            $width = $maxWidthpx . 'px';
                            $height = $maxWidthpx / $imgInfo[0] * $imgInfo[1];
                            $tdHeight += $maxWidthpx / $imgInfo[0] * $imgInfo[1];
                        } else {
                            $width = $imgInfo[0] . 'px';
                            $height = $imgInfo[1] . 'px';
                            $tdHeight += $imgInfo[1];
                        }
                        $loopVal = $loopVal + 1;

                        if ($loopVal == 1 )
                        {
                            $instr = '<div></div>';
                        }
                        else{
                            $instr = '';
                        }


                        $imgHtml = $imgHtml . $instr. '<div style="text-align: center;"><img style="width: ' . $width . ';height:' . $height . ';" src="' .
                            ($this->isDev ? $fileUrl : $fileProductionUrl)
                            . '" />
                        </div><div><span style="text-align: center;">' . $file->getCaption() . ' (Document No.: ' . $file->getDocumentNumber() . ')</span></div><div></div>';
                    }

                }
            }
        }

        if($loopVal==0){
            $imgHtml = '<div style="text-align: center;">No pictures attached</div>';
        }

        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Picture(s)</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;">' . $imgHtml . '</td>
</tr>
</tbody></table>';
        return $html;
    }

    function url($url,$option = null) {

        $cURL = curl_init();

        if ($option) {
            curl_setopt($cURL, CURLOPT_URL, $url.$option);
        } else {
            curl_setopt($cURL, CURLOPT_URL, $url);
        }

        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURL, CURLOPT_AUTOREFERER, 1);
        curl_setopt($cURL, CURLOPT_HTTPGET, 1);
        curl_setopt($cURL, CURLOPT_VERBOSE, 0);
        curl_setopt($cURL, CURLOPT_HEADER, 0);
        curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cURL, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($cURL, CURLOPT_DNS_CACHE_TIMEOUT, 2);

        $output['page'] = curl_exec($cURL);
        $output['base64'] = chunk_split(base64_encode(curl_exec($cURL)));
        $output['contentType'] = curl_getinfo($cURL, CURLINFO_CONTENT_TYPE);

        curl_close($cURL);

        return $output;

    }

    public function setRecommendations($anomaly) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Recommendation(s)</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;height: 30px;">' .$this->getValue( $anomaly->getRecommendations()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setRemdialActions($anomaly) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Remedial Action(s)</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;"><ul>';
        foreach ($anomaly->getActionRequired() as $action){
            $html=$html . '<li>' . $anomaly->getActionsChoices()[$action] . '</li>';
        }

        $html=$html . '</ul></td></tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">
Shutdown Required?
</td>
<td style="border: 1px solid black;width: 75%;">
' . ($anomaly->getRemediationRequireShutdown() != null && $anomaly->getRemediationRequireShutdown() ? 'Yes' : 'No') . '
</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setInspectionInfo($anomaly) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Inspection Management Strategy</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Latest Inspection Technique</td>
<td style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getInspectionTechnique()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Latest Inspection Date</td>
<td style="border: 1px solid black;width: 50%;">' .
            ($anomaly->getInspectionDate() != null ? $this->getValue(date_format ($anomaly->getInspectionDate(), 'd F Y')) : '-')
            .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Inspection Frequency (Months)</td>
<td style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getInspectionFrequency()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Next Inspection Requirements</td>
<td style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getInspectionRecommendations()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Next Inspection Date</td>
<td style="border: 1px solid black;width: 50%;">' .
            ($anomaly->getNextInspectionDate() != null ? $this->getValue(date_format ($anomaly->getNextInspectionDate(), 'd F Y')) : '-')
            .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 50%;background-color:#dddddd;">Inspection Results</td>
<td style="border: 1px solid black;width: 50%;">' .$this->getValue( $anomaly->getInspectionResults()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setCloseOutInfo($anomaly) {
        $html= '<hr/><br/><table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;"><b>Close-out Date</b></td>
<td style="border: 1px solid black;width: 70%;">' .
            ($anomaly->getCloseOutDate() == null? '-' : $this->getValue( date_format($anomaly->getCloseOutDate(), 'd F Y')))
            .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Close-out Justification</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;height: 50px;">' .$this->getValue( $anomaly->getCloseOutJustification()) .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;">
Note: This inspection report shall not be reproduced without the approval of inspection party
</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setSignatureInfo($anomaly, $registrar) {
        $register = $registrar;
        $requests = $this->em->getRepository('AIEAnomalyBundle:Request')->findBy(
            array('registrar'=>$register->getId(), 'status'=>1)
        );
        $spaId = null;
        $spaApproveId = null;
        $dataTest = date_create();
        date_date_set($dataTest,1970,1,1);
        foreach ($requests as $request){
            if($request->getAnsweredDate() > $dataTest){
                $dataTest = $request->getAnsweredDate();
                $spaApproveId= $request->getAnsweredBy();
                $spaId = $request->getRequestedBy();
            }
        }
        if ($spaId!=null and $spaApproveId!=null){
            $regCode = ($register instanceof AnomalyRegistrar ? $register->getCode() : $register->getAnomaly()->getCode());
            $actionOwner = $register->getSpa();
            $approvalOwner = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->find($spaApproveId);
        }else{
            $actionOwner =null;
            $approvalOwner = null;
        }
        if($actionOwner){
            $AOTitle = $actionOwner->getPosition();
            $actionOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($actionOwner->getUserId());
            $actionName = $actionOwnerInfo->getUsername();
        }else{
            $AOTitle ="Action Owner";
            $actionName="";
        }
        if($approvalOwner){
            $AppTitle = $approvalOwner->getPosition();
            $approvalOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($approvalOwner->getUserId());
            $approvalName = $approvalOwnerInfo->getUsername();
        }else{
            $AppTitle ="Approved By";
            $approvalName = "";
        }

        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 60%;background-color:#dddddd;"><b>Owners</b></td>
<td style="border: 1px solid black;width: 40%;background-color:#dddddd;"><b>Signature</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">' . $AOTitle . '</td>
<td style="border: 1px solid black;width: 35%;">' .$actionName.'</td>
<td style="border: 1px solid black;width: 40%;height: 40px;"></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">'.  $AppTitle .'</td>
<td style="border: 1px solid black;width: 35%;">' .$approvalName .'</td>
<td style="border: 1px solid black;width: 40%;height: 40px;"></td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setMonitorsCR($anomaly) {
        $html= '<table cellpadding="2" style="font-size: 10px;"><tbody>
<tr style="text-align: center;">
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">ID</td>
<td style="border: 1px solid black;width: 10%;background-color:#dddddd;">Monitor ID</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Inspection Date</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Frequency (months)</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">RWT (mm)</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Length (mm)</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Corrosion Rate (mm/yr)</td>
</tr>';
        $counter=0;
        foreach ($anomaly->getMonitors() as $entity) {
            $html= $html. '<tr style="text-align: center;">';
            if ($counter==0){
                $html=$html.'
<td rowspan="' . count($anomaly->getMonitors()) .'" style="border: 1px solid black;width: 15%;">' .$this->getValue( $anomaly->getAnomalyCustomId()) .'</td>';
            }
            $html= $html. '
<td style="border: 1px solid black;width: 10%;">M-' . $counter . '</td>
<td style="border: 1px solid black;width: 15%;">' .
                ($entity->getInspectionDate() != null ? $this->getValue( date_format ($entity->getInspectionDate(), 'd F Y')) : '-')
                .'</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue( $entity->getInspectionFrequency()) .'</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue( $entity->getRemainingWallThickness()) .'</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue( $entity->getLength()) .'</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue( $entity->getCorrosionRate()) .'</td>
</tr>';
            $counter++;
        }

        $html= $html. '</tbody></table>';
        return $html;
    }

    public function setMonitorsTrackingCriteria($anomaly) {
        $html= '<table cellpadding="2" style="font-size: 10px;"><tbody>
<tr style="text-align: center;">
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">ID</td>
<td style="border: 1px solid black;width: 10%;background-color:#dddddd;">Monitor ID</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Inspection Date</td>
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Frequency (months)</td>
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Tracking Criteria</td>
<td style="border: 1px solid black;width: 20%;background-color:#dddddd;">Tracking CONC</td>
</tr>';
        $counter=0;
        foreach ($anomaly->getMonitors() as $entity) {
            $html= $html. '<tr style="text-align: center;">';
            if ($counter==0){
                $html=$html.'
<td rowspan="' . count($anomaly->getMonitors()) .'" style="border: 1px solid black;width: 15%;">' .$this->getValue( $anomaly->getAnomalyCustomId()) .'</td>';
            }
            $html= $html. '
<td style="border: 1px solid black;width: 10%;">M-' . $counter . '</td>
<td style="border: 1px solid black;width: 15%;">' .
                ($entity->getInspectionDate() != null ? $this->getValue( date_format ($entity->getInspectionDate(), 'd F Y')) : '-')
                .'</td>
<td style="border: 1px solid black;width: 20%;">' .$this->getValue( $entity->getInspectionFrequency()) .'</td>
<td style="border: 1px solid black;width: 20%;">';
            foreach (json_decode($entity->getTrackingCriteria(),true) as $tc){
                $html = $html.'Description: ' . $this->getValue($tc['description']) . '<br/>';
                $html = $html.'Value: ' . $this->getValue($tc['value']) . '<br/>';
                $html = $html.'<br/>';
            }
            $html=$html . '</td>';
            $html=$html.'<td style="border: 1px solid black;width: 20%;">';
            foreach (json_decode($entity->getTrackingCriteria(),true) as $tc){
                $html = $html.'CONC: ' . $this->getValue($tc['tracking_conc']) . '<br/>';
                $html = $html.'Unit: ' . $this->getValue($tc['tracking_conc_unit']) . '<br/>';
                $html = $html.'<br/>';
            }
            $html=$html . '</td>';
            $html=$html . '</tr>';
            $counter++;
        }

        $html= $html. '</tbody></table>';
        return $html;
    }

    public function setROInfo($register) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Repair Order Management Strategy</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Repair Order Description</td>
<td style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getDescription()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Location Details</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getLocationDetails()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Repair Priority</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getRepairPriority()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Drawing Number</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getAnomaly()->getDrawingNumber()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Technical Expiry Date</td>
<td style="border: 1px solid black;width: 25%;">' .
            ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d F Y')) : '-')
            .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Recommendations</td>
<td style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getRecommendations()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setROCloseOutInfo($register) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 30%;background-color:#dddddd;">Close out Date</td>
<td style="border: 1px solid black;width: 70%;">' .
            ($register->getCloseOutDate() == null? '-' : $this->getValue( date_format($register->getCloseOutDate(), 'd F Y')))
            .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;">Close out Justification</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 100%;height: 50px;">' .$this->getValue( $register->getCloseOutJustification()) .'</td>
</tr>
<tr style="text-align: left;">
<td colspan="2" style="border: 1px solid black;width: 100%;background-color:#dddddd;">
Note: This inspection report shall not be reproduced without the approval of inspection party
</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setTROInfo($register) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="4" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Temporary Repair Order Management Strategy</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Temporary Repair Type</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getRepairType()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Design Life</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getDesignLife()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">MOC Number</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getMocNumber()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Drawing Number</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getAnomaly()->getDrawingNumber()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Technical Expiry Date</td>
<td style="border: 1px solid black;width: 25%;">' .
            ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d F Y')) : '-')
            .'</td>
</tr>';
        if($register->getInstallationDate() != null){
            $html = $html . '<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Installation Date</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue(date_format($register->getInstallationDate(), 'd F Y')) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Latest Inspection Technique</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getInspectionTechnique()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Latest Inspection Date</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .
                ($register->getInspectionDate() != null ? $this->getValue(date_format($register->getInspectionDate(), 'd F Y')) : '-')
                .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Inspection Frequency</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getInspectionFrequency()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Next Inspection Date</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .
                ($register->getNextInspectionDate() != null ? $this->getValue(date_format($register->getNextInspectionDate(), 'd F Y')) : '-')
                .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Inspection Results</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getInspectionResults()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Recommendations</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getRecommendations()) .'</td>
</tr>';
        }
        $html = $html . '</tbody></table>';
        return $html;
    }

    public function setFMInfo($register) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="6" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Fabric Maintenance Management Strategy</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Fabric Maintenance Description</td>
<td colspan="5" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getDescription()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Immediate Repair</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue($register->getImmediateRepair()) .'</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">In-Service</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue($register->getInService()) .'</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Long Isolation</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue($register->getLongIsolation()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Drawing Number</td>
<td style="border: 1px solid black;width: 15%;">' .$this->getValue($register->getAnomaly()->getDrawingNumber()) .'</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Technical Expiry Date</td>
<td style="border: 1px solid black;width: 15%;">' .
            ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d F Y')) : '-')
            .'</td>
<td style="border: 1px solid black;width: 15%;background-color:#dddddd;">Completion Date</td>
<td style="border: 1px solid black;width: 15%;">' .
            ($register->getCompletionDate() != null ? $this->getValue(date_format($register->getCompletionDate(),'d F Y')) : '-') .
            '</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Recommendations</td>
<td colspan="5" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getRecommendations()) .'</td>
</tr>
</tbody></table>';
        return $html;
    }

    public function setASInfo($register) {
        $html= '<table cellpadding="2" style="font-size: 13px;"><tbody>
<tr style="text-align: left;">
<td colspan="4" style="border: 1px solid black;width: 100%;background-color:#dddddd;"><b>Assessment Management Strategy</b></td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Assessment Type</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getAssessmentType()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Assessment Code</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getAssessmentCode()) .'</td>
</tr>
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Drawing Number</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getAnomaly()->getDrawingNumber()) .'</td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Technical Expiry Date</td>
<td style="border: 1px solid black;width: 25%;">' .
            ($register->getExpiryDate() != null ? $this->getValue(date_format($register->getExpiryDate(),'d F Y')) : '-')
            .'
</td></tr>';
        if($register->getAssessmentDueDate() != null){
            $html= $html . '
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Assessment Completion Date</td>
<td style="border: 1px solid black;width: 25%;">' .
                ($register->getAssessmentDueDate() != null ? $this->getValue(date_format($register->getAssessmentDueDate(),'d F Y')): '-')
                .'
 </td>
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Assessment Result</td>
<td style="border: 1px solid black;width: 25%;">' .$this->getValue($register->getAssessmentResult()) .'</td>
</tr>';
        }

            $html= $html . '  
<tr style="text-align: left;">
<td style="border: 1px solid black;width: 25%;background-color:#dddddd;">Recommendations</td>
<td colspan="3" style="border: 1px solid black;width: 75%;">' .$this->getValue($register->getRecommendations()) .'</td>
</tr>';
        $html= $html . '</tbody></table>';
        return $html;
    }

}
