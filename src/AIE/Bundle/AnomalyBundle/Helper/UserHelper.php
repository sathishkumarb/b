<?php


namespace AIE\Bundle\AnomalyBundle\Helper;
 use Doctrine\ORM\Query\ResultSetMapping;

class UserHelper
{


    private $doctrine;
    private $aieUsers;
    private $em;

    public function __construct($doctrine, $aieUsers)
    {
        $this->doctrine = $doctrine;
        $this->aieUsers = $aieUsers;
        $this->em = $doctrine->getManager('anomaly');
    }

    public function getProjectUsers($projectId)
    {
        // find action owners for that project
        $actionOwners = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->findByProject($projectId);

        //get User for these actionOwners
        return $this->getActionOwnserUsers($actionOwners);

    }

    public function getProjectSpas($projectId,$role =null)
    {
        $actionGroupUsers = $this->em->getRepository('AIEAnomalyBundle:UserProjectGroup')->findByProject($projectId);

        $actionSpas = [];
        if($role != null){
            if ($actionGroupUsers){

                foreach ($actionGroupUsers as $actionOwner){

                    $users = $this->findByRoleSPA($actionOwner->getUser()->getId(),$actionOwner->getGroup()->getId(),$role);

                    if ($users){
                        $userinfo = $this->doctrine->getManager()->getRepository('UserBundle:User')->find($actionOwner->getUser()->getId());

                        $actionSpas[$actionOwner->getUser()->getId()] = $userinfo->getUsername();
                    }

                }
            }
        }else{
            if ($actionGroupUsers){

                foreach ($actionGroupUsers as $actionOwner){

                    $userinfo = $this->doctrine->getManager()->getRepository('UserBundle:User')->find($actionOwner->getUser()->getId());

                    $actionSpas[$actionOwner->getUser()->getId()] = $userinfo->getUsername();

                }
            }
        }


        return $actionSpas;
    }

    public function getActionOwnserUsers($actionOwners)
    {
        $user_ids = array_map(
            function ($user) {
                return $user->getUserId();
            },
            $actionOwners
        );

        $users = $this->aieUsers->findUsersById($user_ids, true);

        return $users;
    }

    public function setActionOwnerUser(&$actionOwners)
    {
        $users = $this->getActionOwnserUsers($actionOwners);

        array_walk(
            $actionOwners,
            function ($ao) use ($users) {
                $ao->setUser($users[$ao->getUserId()]);
            }
        );

    }


    public function findByRoleSPA($uid,$gid,$role)
    {
        $query = $this->em
            ->createQuery(
                'SELECT g,us FROM AIEAnomalyBundle:ReflectionAnomalyGroup g join UserBundle:User us WHERE g.gid = :gid and us.id= :usid and g.roles LIKE :role'
            )
            ->setParameter('role', '%'.$role.'%')
            ->setParameter('usid', $uid)
            ->setParameter('gid', $gid);

        $users = $query->getResult();

        if($users)
            return $users;
        else
            return null;
    }

    public function findByRoleAdmin($role)
    {
        $query = $this->doctrine->getEntityManager()
            ->createQuery(
                'SELECT g FROM UserBundle:AnomalyGroup g  WHERE g.roles LIKE :role'
            )
            ->setParameter('role', '%'.$role.'%')
            ->setMaxResults(1);

        $group = $query->getResult();

        if($group)
            return $group;
        else
            return null;
    }

    public function getUserAnomalyGroupProjects($uid)
    {

        $qb = $this->doctrine->getEntityManager('anomaly')->createQueryBuilder('ug','g');
            

        $sql = "SELECT g.*, ug.id as ug_id, p.name as p_name
                FROM reflectionanomalygroups g 
                INNER JOIN UserProjectGroup ug ON g.gid = ug.group_id               
                INNER JOIN users u ON u.id = ug.user_id
                INNER JOIN Projects p ON p.id = ug.project_id
                WHERE u.id =:uid";

        $params['uid'] = $uid;
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute($params);
        $groups = $stmt->fetchAll();

        if($groups)
            return $groups;
        else
            return null;

    }

    public function getIsAdmin($userid){
//        $anomalyEm = $this->get('doctrine')->getManager('anomaly');
//        $connection = $anomalyEm->getConnection();
//        $anomalyUser = $anomalyEm->getRepository('UserBundle:User')->findOneBy(array('id'=>$userid));
//        if ($anomalyUser) {
//            if ($anomalyUser->getCompany() == 'isadmin')
//                return True;
//            else
//                return False;
//        }
//        else
//            return False;

        $em =  $this->doctrine->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT company FROM anomaly.users WHERE id = :id");
        $statement->bindValue('id', $userid);
        $statement->execute();
        $results = $statement->fetch();
        if ($results['company'] == "isadmin")
        return True;
        else
        return False;
        return $results;
    }

    public function getAnomalyGroupRoles($userid,$projectId=null)
    {
         
        // if super admin for the group


            if ($projectId){
                // if role of the project group
                 $query = $this->em->createQuery(
                        'SELECT g FROM AIEAnomalyBundle:ReflectionAnomalyGroup g  join AIEAnomalyBundle:UserProjectGroup ug WITH g.gid= ug.group WHERE ug.user = :uid and ug.project= :projectid'
                    )
                    ->setParameter('uid', $userid)
                    ->setParameter('projectid', $projectId);

            } else {
                 // if role of the project group
                 $query = $this->em->createQuery(
                        'SELECT g FROM AIEAnomalyBundle:ReflectionAnomalyGroup g  join AIEAnomalyBundle:UserProjectGroup ug WITH g.gid= ug.group WHERE ug.user = :uid'
                    )
                    ->setParameter('uid', $userid);

            }
           
            $groups = $query->getResult();

            if($groups)
                return $groups;
            else
                return null;
        }



    public function isRoleGranted($role,$roles)
    {
        return in_array($role, $roles);
    }

    public function getSuperAdmin($userid)
    {
        $em =  $this->doctrine->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT roles FROM users WHERE id = :id");
        $statement->bindValue('id', $userid);
        $statement->execute();
        $results = $statement->fetch();
        return $results;
    }


    public function getAllDeferralRequests($status, $userId)
    {
        $connection = $this->em->getConnection();

        $sql = "SELECT Request.id, Request.registrar_id, Request.request_source, Request.request_batch,Request.status,Request.approval_level,Request.requeststatus,
                        Request.type, Request.date
                FROM Deferrals
                inner join Request on Request.approval_level = Deferrals.approval_level
                inner join Registrar on Request.registrar_id=Registrar.id
                left join ActionRegistrar on Registrar.id=ActionRegistrar.id
                where Request.status=? And Request.requeststatus IN (?) And
                    (select registrar_type from Registrar as Reg1 where 
                        (ActionRegistrar.anomaly_id is not null and Reg1.id = ActionRegistrar.anomaly_id) Or
                        (ActionRegistrar.anomaly_id is null and Reg1.id = Registrar.id)) = Deferrals.anomaly_category
                        and Deferrals.action_owner_id=?  and Registrar.project_id = Deferrals.project_id";

        $typeStage = ['deferral'];
        $values = [
            $status,
            $typeStage,
            $userId
        ];
        $types = [
            \PDO::PARAM_INT,
            \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
            \PDO::PARAM_INT,
        ];
        $statement = $connection->executeQuery($sql, $values, $types);
        $result = $statement->fetchAll();

        //print_r($result);
        //die();
        return $result;
    }

    public function getAllApprovalRequests($status, $userId)
    {
        $connection = $this->em->getConnection();

        $sql = "SELECT Request.id, Request.registrar_id, Request.request_source, Request.request_batch,Request.status,Request.approval_level,Request.requeststatus,
                        Request.type, Request.date
                FROM ApprovalOwners
                inner join Request on Request.approval_level = ApprovalOwners.approval_level
                inner join Registrar on Request.registrar_id=Registrar.id
                left join ActionRegistrar on Registrar.id=ActionRegistrar.id
                where Request.status=? And Request.requeststatus IN (?) And
                    (select registrar_type from Registrar as Reg1 where 
                        (ActionRegistrar.anomaly_id is not null and Reg1.id = ActionRegistrar.anomaly_id) Or
                        (ActionRegistrar.anomaly_id is null and Reg1.id = Registrar.id)) = ApprovalOwners.anomaly_category
                        and ApprovalOwners.action_owner_id=?  and Registrar.project_id = ApprovalOwners.project_id";

        $typeStage = ['update', 'new'];
        $values = [
            $status,
            $typeStage,
            $userId
        ];
        $types = [
            \PDO::PARAM_INT,
            \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
            \PDO::PARAM_INT,
        ];
        $statement = $connection->executeQuery($sql, $values, $types);
        $result = $statement->fetchAll();

        //print_r($result);
        //die();
        return $result;
    }

    public function getRequestsByIdUpdatedOrDeferred($id)
    {
        $fields = array('req.id');
        $qb = $this->em->createQueryBuilder();
        $query = $qb->select($fields)
                    ->from('AIEAnomalyBundle:Request','req')
                    //->innerJoin('AIEAnomalyBundle:Registrar', 'reg', 'WITH', 'req.registrar = reg.id')
                    //->where('reg.project= :projectid')
                    ->where('req.requeststatus= :type1')
                    ->orWhere('req.requeststatus= :type2')
                    ->orWhere('req.requeststatus= :type3')
                    ->andWhere('req.registrar=:id')
                    ->andWhere('req.status= :status')
                    ->setParameter('id', $id)
                    ->setParameter('type1', 'update')
                    ->setParameter('type2', 'deferral')
                    ->setParameter('type3', 'new')
                    ->setParameter('status', 1);
        
        //echo $query->getQuery()->getSql()."<br>";
         
        $requests = $query->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
              
        return $requests;
    }
    
}