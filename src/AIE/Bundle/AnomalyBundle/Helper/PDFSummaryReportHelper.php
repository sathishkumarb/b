<?php
/**
 * Created by AIE
 * Date: 29/08/18
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Request;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class PDFSummaryReportHelper
{
    private $em;
    private $defaultEm;
    private $isDev;
    private $uploader;
    public function __construct($em,$defaultEm,$env,$uploader)
    {
        $this->em=$em;
        $this->defaultEm=$defaultEm;
        $this->isDev=$env;
        $this->uploader=$uploader;
    }

    function url($url,$option = null) {

        $cURL = curl_init();

        if ($option) {
            curl_setopt($cURL, CURLOPT_URL, $url.$option);
        } else {
            curl_setopt($cURL, CURLOPT_URL, $url);
        }

        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cURL, CURLOPT_AUTOREFERER, 1);
        curl_setopt($cURL, CURLOPT_HTTPGET, 1);
        curl_setopt($cURL, CURLOPT_VERBOSE, 0);
        curl_setopt($cURL, CURLOPT_HEADER, 0);
        curl_setopt($cURL, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cURL, CURLOPT_DNS_USE_GLOBAL_CACHE, 0);
        curl_setopt($cURL, CURLOPT_DNS_CACHE_TIMEOUT, 2);

        $output['page'] = curl_exec($cURL);
        $output['base64'] = chunk_split(base64_encode(curl_exec($cURL)));
        $output['contentType'] = curl_getinfo($cURL, CURLINFO_CONTENT_TYPE);

        curl_close($cURL);

        return $output;

    }

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    public function getValue($val){
        if ($val instanceof \DateTime){
            return date_format($val,'d-M-Y');
        }
        if ($val == null || empty($val) || $val==""){
            return "-";
        }

        return htmlentities($val);
    }

    private function createTopBar(&$pdf,$projectLogo,$registrer){

        $anomaly = ($registrer instanceof AnomalyRegistrar ? $registrer : $registrer->getAnomaly());
        $fileExt = ($this->isDev ? pathinfo($projectLogo,PATHINFO_EXTENSION) : pathinfo(parse_url($projectLogo, PHP_URL_PATH), PATHINFO_EXTENSION));
        $imgHtml = '';
        if (strtolower($fileExt) == 'png' || strtolower($fileExt) == 'jpg' || strtolower($fileExt) == 'bmp'|| strtolower($fileExt) == 'gif' || strtolower($fileExt) == 'jpeg') {
            if (($this->isDev && file_exists ($projectLogo)) || (!$this->isDev && $this->is_url_exist($projectLogo))) {
                $maxWidthpx = 3.7795275591 * 50;
                $maxHeigh = 46;
                if ($this->isDev){
                    $fileUrl=$projectLogo;
                    $imgInfo = getimagesize($projectLogo);
                }else{
                    $fileUrl = $this->url($projectLogo);
                    $imgInfo = getimagesizefromstring($fileUrl['page']);
                }
                if($imgInfo[1] > $maxHeigh){
                    $height = $maxHeigh . 'px';
                    $width = $maxHeigh/$imgInfo[1] * $imgInfo[0];
                }else{
                    $width = $imgInfo[0] . 'px';
                    $height = $imgInfo[1] . 'px';
                }
                $imgHtml = '<div style="text-align: center;"><img style="width: ' . $width . ';height:' . $height . ';" src="' .
                    ($this->isDev ? $fileUrl : 'data:image/' . $fileExt . ';base64,' . $fileUrl['base64'])
                    . '" />
                        </div>';
            }
        }

        /**
         * Revision Count
         * Right now all request are taken irrespective of approved or rejected
         */

        $revision_qb = $this->em->createQueryBuilder();
        $revision_qb->select('count(request.id)');
        $revision_qb->from('AIEAnomalyBundle:Request', 'request');

        $revision_qb->andWhere('request.registrar = :registrar_id')
            ->setParameter('registrar_id', $registrer->getId());

        $revision_count = $revision_qb->getQuery()->getSingleScalarResult();


        $html = '<table cellpadding="5" style="font-size:10px">
            <tr  style="height: 50px;">
            <td colspan="5" style="width:85%;border: 1px solid black; font-size: 20px;font-weight: bold;text-align: center;line-height: 250%">
            ' . $registrer->getProject()->getName() . ' Anomaly Report
            </td>
            <td style="width:15%;border: 1px solid black;">' . $imgHtml .
            '</td>
            </tr>
            <tr>
            <td style="width: 20%;border: 1px solid black;background-color:#eeeeee;">Report No / Anomaly ID</td>  
            <td style="width: 20%;border: 1px solid black;">' . $this->getValue($anomaly->getAnomalyCustomId()) . '</td>  
            <td style="width: 15%;border: 1px solid black;background-color:#eeeeee;">Rev.</td>  
            <td style="width: 15%;border: 1px solid black;">'.$revision_count.'</td>  
            <td style="width: 15%;border: 1px solid black;background-color:#eeeeee;">Date</td>  
            <td style="width: 15%;border: 1px solid black;">' . $this->getValue($anomaly->getRaisedOn()) . '</td>  
            </tr>
            </table><br/><br/>';

        return $html;
    }

    private function createSummaryData($register){
        $anomaly = ($register instanceof AnomalyRegistrar ? $register : $register->getAnomaly());
        $PIDs = [];
        $PIDStr = null;
        $InspectionStr = null;
        $drawingStr = null;
        $Reports = [];
        $Drawings = [];

        $PIDNames = [];
        $PIDNameStr = null;

        $DrawingNames = [];
        $drawingNameStr = null;

        $ReportNames = [];
        $reportNameStr = null;

        $fileUrl = '';
        foreach ($anomaly->getFiles() as $file){
            if($file->getFilereportCategory() != null && $file->getFilereportCategory()=='P&ID'){
               $fileUrl = $this->uploader->getAbsoluteFilePath($file->getFilepath());
               array_push($PIDs, $fileUrl);
               array_push($PIDNames, $file->getFilename());
            }
            if($file->getFilereportCategory() != null && $file->getFilereportCategory()=='Inspection Report'){
                $fileUrl = $this->uploader->getAbsoluteFilePath($file->getFilepath());
                array_push($Reports,  $fileUrl);
                array_push($ReportNames, $file->getFilename());
            }
            if($file->getFilereportCategory() != null && ($file->getFilereportCategory()=='Isometric' || $file->getFilereportCategory()=='Drawing')){
                $fileUrl = $this->uploader->getAbsoluteFilePath($file->getFilepath());
                array_push($Drawings, $fileUrl);
                array_push($DrawingNames, $file->getFilename());
            }
        }
        $References = '';
        foreach ($PIDs as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $PIDStr = $item;
        }
        foreach ($Reports as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $InspectionStr =  $item;
        }
        foreach ($Drawings as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $drawingStr = $item;
        }
        foreach ($PIDNames as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $PIDNameStr = $item;
        }
        foreach ($ReportNames as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $reportNameStr =  $item;
        }
        foreach ($DrawingNames as $index => $item){
//            $References = $References . '- ' . $item . ' ';
            $drawingNameStr = $item;
        }
        $project = $anomaly->getProject();
        $inspectionDate = null;
        $loopindex=0;
        $closeOutDate = null;
        $closeOutJustification = null;
        foreach ($register->getRequests() as $request) {
            $requestEntity = json_decode($request->getData(), true);
            if ($loopindex != 0) {
                if ((isset($requestEntity['inspection_date']) && $inspectionDate == null && $requestEntity['inspection_date'] != null) ||
                    ($inspectionDate != null && $requestEntity['inspection_date'] != null && $inspectionDate > \DateTime($requestEntity['inspection_date']))) {
                    $inspectionDate = new \DateTime($requestEntity['inspection_date']);
                }
            } else {
                if (isset($requestEntity['inspection_date']) && $requestEntity['inspection_date'] != null) {
                    $inspectionDate = new \DateTime($requestEntity['inspection_date']);
                } else {
                    $inspectionDate = null;
                }
            }
            if(isset($requestEntity['close_out_date'])){
                $closeOutDate = new \DateTime($requestEntity['close_out_date']);
                $closeOutJustification = $requestEntity['close_out_justification'];
                $closeOutReqId = $request->getId();
            }
        }
        $actions = $anomaly->getActionRequired();
        $actionsStr = [0=>'No',1=>'No',2=>'No'];
        if ($actions != null && count($actions)>0){
            if(in_array(0,$actions)){
                $actionsStr[1]='Yes';
            }
            if(in_array(1,$actions) || in_array(2,$actions) || in_array(3,$actions)){
                $actionsStr[0]='Yes';
            }
            if(in_array(4,$actions)){
                $actionsStr[2]='Yes';
            }
        }
        $newRequest = $this->em->getRepository('AIEAnomalyBundle:Request')->findOneBy(
            [
                'registrar' => $anomaly->getId(),
                'requeststatus' => 'new'
            ]
        );
        $spaId = null;
        $actionOwnerInfo = "";
        $approvalOwnerInfo = "";
        $spaApproveId = null;
        $dataTest = date_create();
        date_date_set($dataTest,1970,1,1);
        $dataTest = $request->getAnsweredDate();
        $spaApproveId= $request->getAnsweredBy();
        $spaId = $request->getRequestedBy();
        if ($spaId!=null and $spaApproveId!=null){
            $regCode = ($register instanceof AnomalyRegistrar ? $register->getCode() : $register->getAnomaly()->getCode());
            $actionOwner = $register->getSpa()->getId();
            if ($register->getApprovedspa())
				$approvalOwner = $register->getApprovedspa()->getId();
			else
				$approvalOwner = null;
			
        }else{
            $actionOwner =null;
            $approvalOwner = null;
        }
        if($actionOwner){
            $ac= $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->find($actionOwner);
            $AOTitle = $ac->getPosition();
            $actionOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($ac->getUserId());
            $actionName = $actionOwnerInfo->getUsername();
        }else{
            $AOTitle ="Action Owner";
            $actionName="";
        }
        if($approvalOwner){
            $ac= $this->em->getRepository('AIEAnomalyBundle:ApprovalOwners')->find($approvalOwner);
            $appOwner = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('userId'=>$ac->getActionOwner()->getId()));

            $AppTitle = $appOwner->getPosition();
            $approvalOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($ac->getActionOwner()->getId());
            $approvalName = $approvalOwnerInfo->getUsername();
        }else{
            $AppTitle ="Approved By";
            $approvalName = "";
        }
        $requireShutdown = ($anomaly->getRemediationRequireShutdown() == null ? false : $anomaly->getRemediationRequireShutdown());
        $color = 'FFFFFF';
        $criticalityVal = null;
        if($project instanceof RiskProject){
            $this->getValue($register->getProject()->getRiskMatrix()->getRisk($register->getConsequence(), $register->getProbability()));
        }else{
            if($register->getProject()->hasFMCriticality() && $anomaly->getIsFabricMaintenanceCriticality() && $anomaly->getFabricMaintenanceCriticality() != null){
                $criticalityVal= $this->getValue($project->getFMCriticalityArray()[$anomaly->getFabricMaintenanceCriticality()]);
                $color = $project->getFMCriticalityColorsArray()[$anomaly->getFabricMaintenanceCriticality()];
            }else{
                $criticalityVal = ($register->getCriticality() != null ? $this->getValue($project->getEnabledCriticalityChoices()[$anomaly->getCriticality()]) : '-');
                $color=$project->getEnabledCriticalityColors()[$anomaly->getCriticality()];
            }
        }

        /*
         * Raised By User Name
         *  First Request for the register hold the one who found the anomaly and hence the below steps
         */

        $raisedByName = $raisedByCompany = null;
        $qb = $this->em->createQueryBuilder();
        $qb->select('request')
            ->from('AIEAnomalyBundle:Request', 'request')
            ->andWhere('request.registrar = :registrar_id')
            ->setParameter('registrar_id', $register->getId())
            ->orderBy('request.id', 'ASC')
            ->setMaxResults(1);

        $first_request = $qb->getQuery()->getResult();

        if( ! empty($first_request))
        {
            $raisedBy = $first_request[0]->getRequestedBy()->getUserId();
            $raisedByInfo = $this->defaultEm->getRepository('UserBundle:User')->find($raisedBy);
            $raisedByName = $raisedByInfo->getUsername();
            $raisedByCompany = $raisedByInfo->getCompany();
        }




        $html = '<table cellpadding="5" style="font-size:9px">
            <tr>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Location</td>
            <td style="width: 28%;border: 1px solid black;">' . $this->getValue($anomaly->getLocation()->getName()) . '</td>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Inspection Date</td>
            <td style="width: 28%;border: 1px solid black;">' . $this->getValue($anomaly->getInspDate()) . '</td>
            </tr>
            <tr>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Tag No.</td>
            <td style="width: 28%;border: 1px solid black;">'. $this->getValue($anomaly->getAssetTagNumber()) . '</td>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Anomaly Code</td>
            <td style="width: 28%;border: 1px solid black;">'. $this->getValue($anomaly->getClassification()->getCode()) . '</td>
            </tr>
            <tr>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Description</td>
            <td style="width: 28%;border: 1px solid black;">'. $this->getValue($anomaly->getComponentDescription()) . '</td>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">P&ID</td>
            <td style="width: 28%;border: 1px solid black;"><a href="'. (!empty($PIDStr) ? $PIDStr : "#") . '" target="_blank">'.(!empty($PIDNameStr) ? $PIDNameStr : 'N/A').'</a></td>
            </tr>
            <tr>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Component</td>
            <td style="width: 28%;border: 1px solid black;">'. $this->getValue($anomaly->getComponent()) . '</td>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Drawing No.</td>
            <td style="width: 28%;border: 1px solid black;"><a href="'. (!empty($drawingStr) ? $drawingStr : "#") . '" target="_blank">'.(!empty($drawingNameStr) ? $drawingNameStr : 'N/A').'</a></td>
            </tr>
            <tr>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Anomaly ' . ($project instanceof RiskProject ? 'Risk' : 'Criticality') .'</td>
            <td style="width: 20%;border: 1px solid black;">' .
            $criticalityVal
            . '</td>
             <td style="width: 8%;border: 1px solid black;background-color:#' . $color . '"></td>
            <td style="width: 22%;border: 1px solid black;background-color:#eeeeee;">Production Criticality</td>
            <td style="width: 28%;border: 1px solid black;">' .
            ($project instanceof CriticalityProject && $project->hasProductionCriticality() && $anomaly->getProductionCriticality() != null?
                $project->getProductionCriticalityArray()[$anomaly->getProductionCriticality()]
                : '-')
            . '</td>
            </tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>References (Inspection Report)</b></td></tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;height: 40px;"><a href="'. (!empty($InspectionStr) ? $InspectionStr : "#") . '" target="_blank">'.(!empty($reportNameStr) ? $reportNameStr : 'N/A').'</a></td></tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Anomaly Description and Dimensions</b></td></tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;height: 60px;">' .
            $this->getValue($anomaly->getDefectDescription()) .
            ($this->getValue($anomaly->getWidth()) != '-' ? '<br/>Remaining WT:' . $this->getValue($anomaly->getRemainingWallThickness()) : '') .
            ($this->getValue($anomaly->getLength()) != '-' ? '<br/>Length: ' . $this->getValue($anomaly->getLength()) : '') .
            ($this->getValue($anomaly->getWidth()) != '-' ? '<br/>Width: ' . $this->getValue($anomaly->getLength()) : '')
            . '</td></tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Actions/Recommendations</b></td></tr>
            <tr>
            <td style="width: 25%;border: 1px solid black;text-align: right;">Repair/Remediation</td>
            <td style="width: 8%;border: 1px solid black;">' . $this->getValue($actionsStr[0]) . '</td>
            <td style="width: 25%;border: 1px solid black;text-align: right;">Monitor</td>
            <td style="width: 8%;border: 1px solid black;">' . $this->getValue($actionsStr[1]) . '</td>
            <td style="width: 26%;border: 1px solid black;text-align: right;">Further Assessment/Inspection</td>
            <td style="width: 8%;border: 1px solid black;">' . $this->getValue($actionsStr[2]) . '</td>
            </tr>
            <tr>
            <td style="width: 42%;border: 1px solid black;text-align: right;">The remediation can be done online</td>
            <td style="width: 8%;border: 1px solid black;">' . ($requireShutdown ? 'No' : 'Yes') . '</td>
            <td style="width: 42%;border: 1px solid black;text-align: right;">Shutdown required to perform remediation</td>
            <td style="width: 8%;border: 1px solid black;">' . ($requireShutdown ? 'Yes' : 'No') . '</td>
            </tr>
            <tr><td colspan="4" style="width: 100%;border: 1px solid black;height: 70px;">' . $this->getValue($anomaly->getRecommendations()) . '</td></tr>
            <tr>
            <td rowspan="3" style="width: 15%;border: 1px solid black;line-height: 550%;background-color:#eeeeee;">Raised By</td>
            <td rowspan="3" style="width: 25%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Name</td>
            <td style="width: 50%;border: 1px solid black;">' . $this->getValue(null)/*$this->getValue($actionName)*/ . '</td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Position</td>
            <td style="width: 50%;border: 1px solid black;">' .  $this->getValue(null)/*$this->getValue($AOTitle)*/ . '</td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Company</td>
            <td style="width: 20%;border: 1px solid black;">' . $this->getValue(null) . '</td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Date</td>
            <td style="width: 20%;border: 1px solid black;">' . $this->getValue(null) . '</td>
            </tr>
            <tr>
            <td rowspan="3" style="width: 15%;border: 1px solid black;line-height: 550%;background-color:#eeeeee;">Approved By</td>
            <td rowspan="3" style="width: 25%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Name</td>
            <td style="width: 50%;border: 1px solid black;">' . $this->getValue(null) . '</td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Position</td>
            <td style="width: 50%;border: 1px solid black;">' .  $this->getValue(null)/*$this->getValue($AppTitle) */. '</td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Company</td>
            <td style="width: 20%;border: 1px solid black;">' . $this->getValue(null) . '</td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Date</td>
            <td style="width: 20%;border: 1px solid black;">' . $this->getValue(null) . '</td>
            </tr>';
            if($closeOutDate!=null){
                $requests = $this->em->getRepository('AIEAnomalyBundle:Request')->findBy(
                    array('registrar'=>$register->getId(), 'status'=>1)
                );
                $spaId = null;
                $spaApproveId = null;
                $dataTest = date_create();
                date_date_set($dataTest,1970,1,1);
                foreach ($requests as $request){
                    if($request->getAnsweredDate() > $dataTest){
                        $dataTest = $request->getAnsweredDate();
                        $spaApproveId= $request->getAnsweredBy();
                        $spaId = $request->getRequestedBy();
                    }
                }
                if ($spaId!=null and $spaApproveId!=null){
                    $regCode = ($register instanceof AnomalyRegistrar ? $register->getCode() : $register->getAnomaly()->getCode());
                    $actionOwner = $register->getSpa();;
                    $approvalOwner = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->find($spaApproveId);
                }else{
                    $actionOwner =null;
                    $approvalOwner = null;
                }
                if($actionOwner){
                    $AOTitle = $actionOwner->getPosition();
                    $actionOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($actionOwner->getUserId());
                    $actionName = $actionOwnerInfo->getUsername();
                }else{
                    $AOTitle ="Action Owner";
                    $actionName="";
                }
                if($approvalOwner){
                    $AppTitle = $approvalOwner->getPosition();
                    $approvalOwnerInfo = $this->defaultEm->getRepository('UserBundle:User')->find($approvalOwner->getUserId());
                    $approvalName = $approvalOwnerInfo->getUsername();
                }else{
                    $AppTitle ="Approved By";
                    $approvalName = "";
                }

            }
            $html =$html . '<tr><td style="width: 100%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Close-out Actions</b></td></tr>
            <tr><td style="width: 100%;border: 1px solid black;height: 50px;">' . ($closeOutDate!=null ? $this->getValue($closeOutJustification) : '') . '</td></tr>
            <tr>
            <td rowspan="3" style="width: 15%;border: 1px solid black;line-height: 550%;background-color:#eeeeee;">Raised By</td>
            <td rowspan="3" style="width: 25%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Name</td>
            <td style="width: 50%;border: 1px solid black;"></td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Position</td>
            <td style="width: 50%;border: 1px solid black;"></td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Company</td>
            <td style="width: 20%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Date</td>
            <td style="width: 20%;border: 1px solid black;"></td>
            </tr><tr>
            <td rowspan="3" style="width: 15%;border: 1px solid black;line-height: 550%;background-color:#eeeeee;">Approved By</td>
            <td rowspan="3" style="width: 25%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Name</td>
            <td style="width: 50%;border: 1px solid black;"></td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Position</td>
            <td style="width: 50%;border: 1px solid black;"></td>
            </tr>
            <tr>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Company</td>
            <td style="width: 20%;border: 1px solid black;"></td>
            <td style="width: 10%;border: 1px solid black;background-color:#eeeeee;">Date</td>
            <td style="width: 20%;border: 1px solid black;"></td>
            </tr>';
             $html =$html . '</table><br/><br/>';

        return $html;
    }

    public function createLegend($project){
        $classes=$project->getClasses();
        $currentCount = 0;
        $innerHtml = '';
        while ($currentCount<=count($classes)-1){
            $innerHtml = $innerHtml . '<tr>';
            $incline = 0;
            for($i=$currentCount;$i<=($currentCount+4);$i++){
                if($i<=count($classes)-1){
                    $incline++;
                    $innerHtml = $innerHtml . '<td style="width: 7%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>'  .  $classes[$i]->getCode() . '</b></td>';
                    $innerHtml = $innerHtml . '<td style="width: 13%;border: 1px solid black;">' .  $classes[$i]->getDescription() . '</td>';
                }
            }
            $currentCount = $i;
            if($i>count($classes)-1){
                $percent = (5-$incline)*20;
                $innerHtml = $innerHtml . '<td style="width: ' . $percent . '%;border: 1px solid black;"></td>';
            }

            $innerHtml = $innerHtml . '</tr>';
        }
        $html = '<table cellpadding="3" style="font-size:6px;"><tr><td style="width: 100%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Anomaly Codes</b></td></tr>' . $innerHtml . '</table>';

        return $html;
    }

    public function createLegendTwo($project){
        if($project instanceof RiskProject){
            $trs = 3;
        }else{
            $trs = floor(count($project->getEnabledCriticalityChoices()) / 2 + 1);
        }

        if ($project->hasFMCriticality()){
            if ($trs<count($project->getFMCriticalityArray())){
                $trs = count($project->getFMCriticalityArray());
            }
        }
        if($project->hasProductionCriticality()){
            if ($trs<count($project->getProductionCriticalityArray())-1){
                $trs = count($project->getProductionCriticalityArray())-1;
            }
        }
        $secondHtml='';
        $secondHtml =$secondHtml . '<table cellpadding="3" style="font-size:6px;">
                    <tr>
                    <td style="width: 35%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Anomaly Criticality</b></td>
                    <td style="width: 65%;border: 1px solid black;background-color:#eeeeee;text-align: center;"><b>Anomaly Production Criticality</b></td>
                    </tr>
                    <tr>
                    <td style="width: 20%;border: 1px solid black;background-color:#eeeeee;text-align: center;">Loss of Primary Containment (LOPC)</td>
                    <td style="width: 15%;border: 1px solid black;background-color:#eeeeee;text-align: center;">Fabric Maintenance (FM)</td>';
        if($project->hasProductionCriticality()){
            $pctext = '';
            $pcdesc='';
            foreach ($project->getProductionCriticality() as $pc){
                $pctext = $pc->getProductionCriticalityCode();
                $pcdesc = $pc->getProductionCriticalityDescription();
                break;
            }
            $secondHtml =$secondHtml . '<td style="width: 10%;border: 1px solid black;text-align: center;">' . $pctext . '</td>';
            $secondHtml =$secondHtml . '<td style="width: 55%;border: 1px solid black;">' . $pcdesc . '</td>';
        }else{
            $secondHtml =$secondHtml . '<td style="width: 10%;border: 1px solid black;"></td>';
            $secondHtml =$secondHtml . '<td style="width: 55%;border: 1px solid black;"></td>';
        }
        $secondHtml = $secondHtml . '</tr>';
        $criticalitiesCodesC1 = [];
        $criticalitiesColorsC1 = [];
        $criticalitiesCodesC2 = [];
        $criticalitiesColorsC2 = [];
        $counter = 0;
        $fmCriticalitiesCodes = [];
        $fmCriticalitiesColors = [];
        $pCriticalitiesCodes = [];
        $pCriticalitiesDescriptions = [];
        if($project instanceof RiskProject){
            $criticalitiesCodesC1 = ["Very High","High","Medium"];
            $criticalitiesColorsC1 = ['bf1e2e','ef4035','f1592a'];
            $criticalitiesCodesC2 = ["Low","Ver Low"];
            $criticalitiesColorsC2 = ['0b9444','38b549'];
        }else{
            foreach ($project->getEnabledCriticalityChoices() as $key => $choice){
                if ($counter % 2 == 0) {
                    array_push($criticalitiesCodesC1, $choice);
                    array_push($criticalitiesColorsC1, $project->getEnabledCriticalityColors()[$key]);
                }else{
                    array_push($criticalitiesCodesC2, $choice);
                    array_push($criticalitiesColorsC2, $project->getEnabledCriticalityColors()[$key]);
                }
                $counter++;
            }
            foreach ($project->getFabricMaintenanceCriticality() as $fmc){
                array_push($fmCriticalitiesCodes,$fmc->getFMCriticalityCode());
                array_push($fmCriticalitiesColors,$fmc->getFMCriticalityColor());
            }

            foreach ($project->getProductionCriticality() as $fmc){
                array_push($pCriticalitiesCodes,$fmc->getProductionCriticalityCode());
                array_push($pCriticalitiesDescriptions,$fmc->getProductionCriticalityDescription());
            }
        }


        for($i=0;$i<$trs;$i++){
            $secondHtml = $secondHtml . '<tr>';
            if(isset($criticalitiesCodesC1[$i])){
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;background-color:#' . $criticalitiesColorsC1[$i] . ';text-align: center;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 7%;border: 1px solid black;">' . htmlentities ($criticalitiesCodesC1[$i]) . '</td>';
            }else{
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 7%;border: 1px solid black;"></td>';
            }
            if(isset($criticalitiesCodesC2[$i])){
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;background-color:#' .  $criticalitiesColorsC2[$i] . ';text-align: center;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 7%;border: 1px solid black;">' . htmlentities($criticalitiesCodesC2[$i]) . '</td>';
            }else{
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 7%;border: 1px solid black;"></td>';
            }
            if(isset($fmCriticalitiesCodes[$i])){
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;background-color:#' . $fmCriticalitiesColors[$i] . ';text-align: center;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 12%;border: 1px solid black;">' . htmlentities($fmCriticalitiesCodes[$i]) . '</td>';
            }else{
                $secondHtml =$secondHtml . '<td style="width: 3%;border: 1px solid black;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 12%;border: 1px solid black;"></td>';
            }
            if(isset($pCriticalitiesCodes[$i+1])){
                $secondHtml =$secondHtml . '<td style="width: 10%;border: 1px solid black;text-align: center;">' . $pCriticalitiesCodes[$i+1] . '</td>';
                $secondHtml =$secondHtml . '<td style="width: 55%;border: 1px solid black;">' . htmlentities($pCriticalitiesDescriptions[$i+1]) . '</td>';
            }else{
                $secondHtml =$secondHtml . '<td style="width: 10%;border: 1px solid black;"></td>';
                $secondHtml =$secondHtml . '<td style="width: 55%;border: 1px solid black;"></td>';
            }
            $secondHtml =$secondHtml . '</tr>';
        }

        $secondHtml =$secondHtml . '</table><br/><div style="text-align: center;font-size: 6px;">This document is the property of ' . $project->getName() . '. It must not be stored, reproduced or disclosed to others without written authorisation from the Company</div>';
        return $secondHtml;
    }

    public function createSummaryReport(& $pdf,$project,$projectLogo, $register){
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $no_margins = array(
            0 => array('h' => 2, 'n' => 1),
            1 => array('h' => 2, 'n' => 1)
        );
        $pdf->setHtmlVSpace(array(
            'br' => $no_margins
        ));
        $pdf->addPage('A4','P');
        $pdf->writeHTML($this->createTopBar($pdf,$projectLogo,$register), false, false, true, false, '');
        $pdf->writeHTML($this->createSummaryData($register), false, false, true, false, '');
        $pdf->writeHTML($this->createLegend($project), false, false, true, false, '');
        $pdf->writeHTML($this->createLegendTwo($project), false, false, true, false, '');
        return $pdf;
    }

}
