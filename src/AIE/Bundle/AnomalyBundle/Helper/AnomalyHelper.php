<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 12:17
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyThreat;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use Aws\CloudFront\Exception\Exception;

class AnomalyHelper
{

    private $doctrine;
    private $fileUploader;
    private $em;

    public function __construct($doctrine, $fileUploader)
    {
        $this->doctrine = $doctrine;
        $this->fileUploader = $fileUploader;
        $this->em = $doctrine->getManager('anomaly');
    }

    /***
     *
     * A function that checks whether a code is used in class & threat anomalies or not
     *
     * @param int $projectId
     * @param string $code
     *
     * @return boolean
     */
    public function isAnomalyCodeUsed($projectId, $code)
    {

        if (
            $this->em->getRepository('AIEAnomalyBundle:AnomalyClass')->findBy(
                ['project' => $projectId, 'code' => $code]
            )
            || $this->em->getRepository('AIEAnomalyBundle:AnomalyThreat')->findBy(
                ['project' => $projectId, 'code' => $code]
            )
        ) {
            return true;
        }

        return false;
    }


    /**
     * Convert an anomaly to human readable array
     * @param $anomaly
     * @return array
     */
    public function formatAnomaly($anomaly, $data=null)
    {
        $result = [];

        $get_functions = array_filter(
            get_class_methods($anomaly),
            function ($f) {
                return strrpos($f, 'get', -strlen($f)) !== false;
            }
        );

        $fileUploader = $this->fileUploader;
        $doctrine = $this->doctrine;

        array_walk(
            $get_functions,
            function (&$f, $k) use (&$result, $anomaly, $doctrine, $fileUploader, $data) {
                $keyCamel = substr($f, 3);
                $key = trim(
                    ucwords(preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $keyCamel))
                ); //convert camel case to human readable
                $value = call_user_func([$anomaly, $f]);//call the function of get
                $keyCamel = $key; //lcfirst($keyCamel);
                switch ($key) {
                    case 'Project':
                        $result[$keyCamel] = $value->getName();
                        break;
                    case 'Threat':
                        if ($value instanceof AnomalyThreat) {
                            $result[$keyCamel] = $value->getCode();
                        } else {
                            $result[$keyCamel] = $value;
                        }
                        break;
                    case 'Location':
                        $result[$keyCamel] = $value->getName();
                        break;
                    case 'Classification':
                        $result[$keyCamel] = $value->getCode();
                        break;
                    case 'Action Required':
                        $result[$keyCamel] = array_map(
                            function ($action) use ($anomaly) {
                                return $anomaly->getActionsChoices()[$action];
                            },
                            $value
                        );
                        break;
                    case 'Virtual Files':
                        $result[$keyCamel] = array_map(
                            function ($file) use ($fileUploader) {
                                //list of files
                                $node = $file->getAsTreeNode($fileUploader);
                                $a = '<a target="_blank" href="'.$node['absolutepath'].'">'.$node['caption'].'</a>';

                                return $a;
                            },
                            $value
                        );
                        break;
                    case 'Status':
                        $result[$keyCamel] = $anomaly->getStatusChoices()[$value];
                        break;
                    case 'Criticality':
                        if ($anomaly instanceof AnomalyRegistrar) {
                            if ($anomaly->getProject() instanceof CriticalityProject) {
                                $result[$keyCamel] = $value;
                            }
                        }
                        //if critical project, show
                        break;
                    case 'Probability':
                    case 'Consequence':
                        //if risk project, show
                        if ($anomaly instanceof AnomalyRegistrar) {
                            if ($anomaly->getProject() instanceof RiskProject) {
                                $result[$keyCamel] = $value;
                            }
                        }
                        break;
                    case 'Design Code Id':
                    case 'Design Code':
                        $designCode = $anomaly->getDesignCode();
                        $result[$keyCamel] = $designCode->getCode().' - '.$designCode->getDescription();
                        break;
                    case 'Spa Id':
                    case 'Spa':
                        if(array_key_exists('spa', $result) && $result['spa'] !== null ){
                            break;
                        }
                        $actionOwner = $anomaly->getSpa();
                        if($data !== null && array_key_exists('spa_id', $data)){
                            $actionOwner = $doctrine->getManager('anomaly')->getRepository(
                                'AIEAnomalyBundle:ActionOwners'
                            )->find($data['spa_id']);
                        }
                        $user = $doctrine->getManager('default')->getRepository('UserBundle:User')->find(
                            $actionOwner->getUserId()
                        );
                        $result['spa'] = $user->getUsername();
                        break;
                    case 'Is Due':
                        $result[$keyCamel] = ($value)? 'Yes' : 'No';
                        break;
                    case 'Defect Orientation':
                        if ($value instanceof \DateTime) {
                                $result[$keyCamel] = $value->format('h:m');
                        }
                        break;
                    case 'Risk':
                        if ($anomaly->getProject() instanceof RiskProject) {
                            $result[$keyCamel] = $anomaly->getProject()->getRiskMatrix()->getRisk($anomaly->getConsequence(), $anomaly->getProbability());
                        }else{
                            $result[$keyCamel] = $value;
                        }
                        break;
                    case 'Threat Id':
                    case 'Location Id':
                    case 'Classification Id':
//                    case 'Anomaly ID':
//                    case 'Id':
                    case 'Project Id':
                    case 'Actions Choices':
                    case 'Tag Choices':
                    case 'Status Tag':
                    case 'Status Choices':
                    case 'Risk Choices':
                    case 'Criticality Choices':
                    case 'Spa User':
                        //ignore them
                        break;
                    default:
                        if ($value instanceof \DateTime) {
                            $result[$keyCamel] = $value->format('d F Y');
                        } elseif (is_object($value)) {
                            //ignore other objects
                            break;
                        }elseif(is_bool($value)){
                            $result[$keyCamel] = ($value)?'True':'False';//set it as result
                        } else {
                            $result[$keyCamel] = $value;//set it as result
                        }
                }
            }
        );

        return $result;
    }


    public function createAnomalyActions($entityManager, AnomalyRegistrar &$anomaly)
    {


        //first lets get which actions the anomaly have
        $requestedActions = $anomaly->getActionRequired();
        //get current actions
        $currentActions = array_map(
            function ($action) {
                return get_class($action);
            },
            $anomaly->getActions()->toArray()
        );


        foreach ($requestedActions as $key => $actionReq) {
            $actionType = $anomaly->getActionsChoices()[$actionReq];
            switch ($actionReq) {
                case 0:
                    //no action
                    break;
                case 1:
                    $action = new RepairOrderRegistrar();
                    $found = false;
                    foreach ($currentActions as $ca) {
                        if ($action instanceof $ca) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found) {
                        break;
                    }
                    $action->setAnomaly($anomaly);
                    $action->setSpa($anomaly->getSpa());
                    $action->setProject($anomaly->getProject());
                    $action->setStatusTag($action->getTagChoices()['new']['name']);
                    $action->setStatus($anomaly->getStatus());
                    $anomaly->addAction($action); //add it to anomaly
                    break;
                case 4:
                    $action = new AssessmentRegistrar();
                    $found = false;
                    foreach ($currentActions as $ca) {
                        if ($action instanceof $ca) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found) {
                        break;
                    }
                    $action->setAnomaly($anomaly);
                    $action->setSpa($anomaly->getSpa());
                    $action->setProject($anomaly->getProject());
                    $action->setStatusTag($action->getTagChoices()['new']['name']);
                    $action->setStatus($anomaly->getStatus());
                    $anomaly->addAction($action); //add it to anomaly
                    break;
                case 3:
                    $action = new FabricMaintenanceRegistrar();
                    $found = false;
                    foreach ($currentActions as $ca) {
                        if ($action instanceof $ca) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found) {
                        break;
                    }
                    $action->setAnomaly($anomaly);
                    $action->setSpa($anomaly->getSpa());
                    $action->setProject($anomaly->getProject());
                    $action->setStatusTag($action->getTagChoices()['new']['name']);
                    $action->setStatus($anomaly->getStatus());
                    $anomaly->addAction($action); //add it to anomaly
                    break;
                case 2:
                    $action = new TemporaryRepairOrderRegistrar();
                    $found = false;
                    foreach ($currentActions as $ca) {
                        if ($action instanceof $ca) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found) {
                        break;
                    }
                    $action->setAnomaly($anomaly);
                    $action->setSpa($anomaly->getSpa());
                    $action->setProject($anomaly->getProject());
                    $action->setStatusTag($action->getTagChoices()['new']['name']);
                    $action->setStatus($anomaly->getStatus());
                    $anomaly->addAction($action); //add it to anomaly
                    break;
                default:
                    break;
            }
        }
    }
}