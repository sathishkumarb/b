<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 12:17
 */

namespace AIE\Bundle\AnomalyBundle\Helper;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class WorkpackHelper
{

    private $em;
    private $defaultEm;
    public function __construct($em,$defaultEm)
    {
        $this->em=$em;
        $this->defaultEm=$defaultEm;
    }
    public function definePDFObject($headerMarging=15, $footerMargin=15, $leftMargin=20,$topMargin=45,$rightMargin=20,$bottomMargin=20){
        $pdf = new WorkpackCustomHelper(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator('AIE');
        $pdf->SetAuthor('AIE');
        $pdf->SetTitle('AIE Workpack');
        $pdf->SetSubject('AIE Workpack');
        $pdf->SetKeywords('AIE');
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 060', PDF_HEADER_STRING);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetHeaderMargin($headerMarging);
        if($footerMargin === null)
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        else
            $pdf->SetFooterMargin($footerMargin);
        $pdf->SetAutoPageBreak(TRUE, $bottomMargin);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetMargins($leftMargin, $topMargin, $rightMargin,$bottomMargin);
        return $pdf;
    }


}