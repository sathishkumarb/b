<?php
/**
 * User: mokha
 * Date: 3/28/16
 * Time: 11:40 AM
 */

namespace AIE\Bundle\AnomalyBundle\Helper;


use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;

class FormHelper {


    private $em;
    private $kernel;
    private $formFactory;
    public function __construct($em, $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->formFactory = $kernel->getContainer()->get('form.factory');
    }

    public function createShowAnomalyForm(AnomalyRegistrar $entity){
        $em = $this->em;
        $project = $entity->getProject();
        $actionOwners = $project->getActionOwners()->toArray();
        $this->kernel->getContainer()->get('aie_anomaly.user.helper')->setActionOwnerUser($actionOwners);

        $_entityFullClass = explode('\\',get_class($entity));
        $entityClass = end($_entityFullClass);
        $typeForm = 'AIE\Bundle\AnomalyBundle\Form\\' . $entityClass . 'Type';

        if (!$entity->getSpaId()) {
            $userAction = $em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(array('project'=>$project->getId()));
            $userActionId = $userAction->getId();
        }
        else{
            $userActionId = $entity->getSpaId();
        }


        $form = $this->formFactory->create(
            new $typeForm($entity, $em, $actionOwners, $userActionId),
            $entity,
            [
                'disabled' => true,
                'read_only' => true,
                'action' => '#',
                'method' => 'POST',
            ]
        );
        $form->get('anomaly_group')->remove('add_tracking_criteria');
        $form->remove('files_group');

        return $form;
    }

}