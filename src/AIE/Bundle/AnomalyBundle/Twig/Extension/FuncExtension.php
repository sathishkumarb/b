<?php

namespace AIE\Bundle\AnomalyBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\Request;

class FuncExtension  extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('frequency', array($this, 'frequencyFilter')),
        );
    }

    /**
    * Get current controller name
    */
    public function frequencyFilter($decimalInput)
    {
        return round($decimalInput);
    }

    public function getName()
    {
        return 'aie_func_helper_twig_extension';
    }
}