<?php

namespace AIE\Bundle\AnomalyBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class FabricMaintenanceRegistrarRepository extends EntityRepository {

    public function findByCompletionDate($closedate) {
    	//echo $closedate;
        $em = $this->getEntityManager();

        $rsm = new ResultSetMapping($em);
        $rsm->addEntityResult('AIEAnomalyBundle:FabricMaintenanceRegistrar', 'u');
		$rsm->addFieldResult('u', 'id', 'id');
		$rsm->addFieldResult('u', 'status', 'status');
        //$rsm->addRootEntityFromClassMetadata('Model_Record_Delivery', 'u');
        $query = $em->createNativeQuery("SELECT * FROM FabricMaintenanceRegistrar WHERE date(completion_date) <= ? FOR UPDATE", $rsm);
        $query->setParameter("1", $closedate);
        $result = $query->getResult();
        
		foreach($result as $res) {
			//echo "id".$res->getId()."\n";
			if ($res->getId()){
				$qb = $em->createQueryBuilder();
				$q = $qb->update('AIEAnomalyBundle:Registrar', 'r')
				->set('r.status', 2)
				->where('r.id = ?1')
				->setParameter(1, $res->getId())
				->getQuery();
				$p = $q->execute();
			}

		}

    }

}
