<?php

namespace AIE\Bundle\AnomalyBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FilesCategoriesRepository extends EntityRepository {

    public function deleteCategory($id)
    {
        $em = $this->getEntityManager();
        //set files category to null
        $q = $em->createQuery('UPDATE AIE\Bundle\AnomalyBundle\Entity\Files f set f.category = NULL WHERE f.category = :id');
        $q->setParameters(['id' => $id ]);
        $q->execute();
        //delete sub categories
        $q = $em->createQuery('DELETE AIE\Bundle\AnomalyBundle\Entity\FilesCategories fc WHERE fc.parent = :id');
        $q->setParameters(['id' => $id ]);
        $q->execute();

        //delete parent category
        $q = $em->createQuery('DELETE AIE\Bundle\AnomalyBundle\Entity\FilesCategories fc WHERE fc.id = :id');
        $q->setParameters(['id' => $id ]);
        $nonAffected = $q->execute();

        return $nonAffected > 0;
    }
}
