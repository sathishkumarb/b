<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MonitorType extends AbstractType
{
	use FormStyleHelper;
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $date = new \DateTime();
        $builder
	        ->add('comments', 'textarea',
		        $this->options(['label' => 'Comments', 'required' => false]))
	        //->add('nextInspectionDate', new DatePickerType(), $this->options(['label' => '', 'data' => $date->format('d F Y'), 'attr' => ['class' => 'inspection']], 'date_picker'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Monitor'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_monitor';
    }
}
