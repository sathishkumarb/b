<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;

class RepairOrderRegistrarType extends ActionRegistrarType {


	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
	}

	protected function buildAction(FormBuilderInterface &$builder) {
		parent::buildAction($builder);
		$builder->add('description', 'text',
			$this->options([
				'label'       => 'Repair Order Description',
				'attr'        => ['class' => 'required'],
				'required'    => true,
				'constraints' => [new NotBlank()],]))
			->add('locationDetails', 'text',
				$this->options([
					'label'       => 'Location Details',
					'attr'        => ['class' => ''],
					'required'    => false,
					'constraints' => [],]))
			->add('repairPriority', 'text',
				$this->options([
					'label'       => 'Repair Priority',
					'attr'        => ['class' => ''],
					'required'    => false,
					'constraints' => [],]))
			->add($builder->create('closeOutDate', new DatePickerType(),
				$this->options([
				    'label' => 'Close Out Date',
                    'constraints' => new Callback(                            [
                            'callback' => function ($value, $context) {
                                $form = $context->getRoot();
                                $data = $form->get('status');
                                // If $data is null then the field was blank, do nothing more
                                if (is_null($data)) {
                                    return;
                                }

                                $status = $data->getData();
                                if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                    $form->addError(new FormError('You should fill close out date!'));
                                }
                            }
                        ]
                    ),
				], 'date_picker'))->addModelTransformer(new DateTimePickerTransformer()))
			->add('closeOutJustification', 'textarea',
				$this->options([
				    'label' => 'Close Out Justification',
                    'error_bubbling' => true,
                    'constraints' => new Callback(                            [
                            'callback' => function ($value, $context) {
                                $form = $context->getRoot();
                                $data = $form->get('status');

                                // If $data is null then the field was blank, do nothing more
                                if (is_null($data)) {
                                    return;
                                }

                                $status = $data->getData();

                                if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                    $form->addError(new FormError('You should fill close out justification!'));
                                }

                            }
                        ]
                    ),

				]));

	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_repairorderregistrar';
	}
}
