<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class PipelineSectionsType extends AbstractType
{
    use FormStyleHelper;

    private $length;
    private $entity;

    public function __construct($entity, $length)
    {
        $this->length = $length;
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                $this->options(
                    [
                        'label' => 'Section',
                        'attr' => [
                            'class' => 'section required',
                            'autocomplete' => 'off',
                            'data-provide' => 'typeahead',
                        ],
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'outerDiameter',
                'number',
                $this->options(
                    [
                        'label' => 'Outer Diameter',
                        'required' => false,
                        'attr' => ['class' => 'number', 'next_group_addon' => 'inch'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'nominalWT',
                'number',
                $this->options(
                    [
                        'label' => 'Nominal Wall Thickness',
                        'required' => false,
                        'attr' => ['class' => 'number', 'next_group_addon' => 'ksi'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'material',
                'text',
                $this->options(
                    [
                        'label' => 'Material',
                    ]
                )
            )
            ->add(
                'minStrength',
                'number',
                $this->options(
                    [
                        'label' => 'Specified Minimum Yield Strength',
                        'required' => false,
                        'attr' => ['class' => 'number', 'next_group_addon' => 'ksi'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'segments',
                CollectionType::class,
                array(
                    'entry_type' => PipelineSectionSegmentType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'label' => false
                )
            )
            ->add(
                'add_segment',
                'button',
                $this->options(
                    [
                        'label' => '+ Add Segment',
                        'attr' => ['class' => 'btn btn-link', 'container_class' => 'button-container col-md-offset-3'],

                    ]
                    ,
                    'button'
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\PipelineSections'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_pipelinesections';
    }
}
