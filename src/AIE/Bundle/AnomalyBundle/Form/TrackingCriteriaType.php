<?php
/**
 * Created by PhpStorm.
 * User: mokha
 * Date: 2/21/16
 * Time: 3:48 AM
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;


class TrackingCriteriaType extends AbstractType
{
    use FormStyleHelper;


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tracking = $builder->create(
            'tracking_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Tracking Criteria',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9 not-cr-ml']
            ]
        );

        $tracking
            ->add(
                'description',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => true,
                        'attr' => ['container_class' => '', 'form_class' => 'col-md-5', 'class' => 'required'],
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'value',
                'text',
                $this->options(
                    [
                        'label' => 'Value',
                        'required' => true,
                        'attr' => ['form_class' => 'col-md-4', 'class' => 'required'],
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'unit',
                'text',
                $this->options(
                    [
                        'label' => 'Unit',
                        'required' => false,
                        'attr' => ['form_class' => 'col-md-3']
                    ]
                )
            );

        $trackingConc = $builder->create(
            'tracking_conc_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Tracking CONC',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9 not-cr-ml']
            ]
        );


        $trackingConc
            ->add(
                'trackingConc',
                'text',
                $this->options(
                    [
                        'label' => false,
                        'required' => true,
                        'constraints' => [new NotNull()],
                        'attr' => ['container_class' => '', 'form_class' => 'col-md-5', 'class' => 'required']
                    ]
                )
            )
            ->add(
                'trackingConcUnit',
                'text',
                $this->options(
                    [
                        'label' => 'Unit',
                        'required' => false,
                        'attr' => ['form_class' => 'col-md-4']
                    ]
                )
            );

        $builder->add($tracking)->add($trackingConc);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\TrackingCriteria'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_trackingcriteria';
    }
}