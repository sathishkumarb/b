<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 30/12/15
 * Time: 12:05
 */

namespace AIE\Bundle\AnomalyBundle\Form;


use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Validator\Constraints\NotNull;

class ManagementFilterType extends AbstractType {

	use FormStyleHelper;

	private $from_date;
	private $to_date;
    private $project;
	protected $typeChoices = [
		'IN'  => 'Anomaly Inspections',
		'TRO' => 'Temporary Repair Orders',
		'RO'  => 'Repair Order',
		'FM'  => 'Fabric Maintenance',
        'AS' => 'Assessment'
	];

	public function __construct(\DateTime $from_date, \DateTime $to_date, $project) {
		$this->from_date = $from_date;
		$this->to_date = $to_date;
		$this->project=$project;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
	    $locStr = '';
	    foreach ($this->project->getLocations() as $loc){
            $locStr .= '<option locid="' . $loc->getId() . '" value="' . $loc->getName() . '">' . $loc->getName() . '</option>';
        }
		$builder
			->add('type', 'choice', $this->options([
				'label'       => false,
				'multiple'    => false,
				'expanded'    => false,
				'choices'     => $this->typeChoices,
				'required'    => true,
				'constraints' => [new NotNull(),],
				'attr'        => ['class' => 'required form-control']
			]));
        $builder->add(
            'locationStr',
            'hidden',
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => $locStr,
                    'attr'        => ['class' => 'location-string']
                ]
            )
        );
        $builder->add(
            'locationSelected',
            'hidden',
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => '',
                    'attr'        => ['class' => 'location-selected']
                ]
            )
        );
		$builder->add(
            'from_date',
            new DatePickerType(),
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => $this->from_date->format('d F Y'),
                ],
                'date_picker'
            )
        );
        $builder->add(
            'to_date',
            new DatePickerType(),
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => $this->to_date->format('d F Y'),
                ],
                'date_picker'
            )
        );
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'empty_data' => function (FormInterface $form) {
				return [
					'from_date' => $form->get('from_date')->getData(),
					'to_date'   => $form->get('to_date')->getData()
				];
			},
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomaly_management_filter';
	}


}