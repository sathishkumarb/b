<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Form\FormError;

class TemporaryRepairOrderRegistrarType extends ActionRegistrarType {


    protected $management;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);


    }

    protected function buildAction(FormBuilderInterface &$builder) {
        parent::buildAction($builder);
        $builder
            ->add('repairType', 'text',
                $this->options([
                    'label'       => 'Temporary Repair Type',
                    'attr'        => ['class' => 'required'],
                    'required'    => true,
                    'constraints' => [new NotBlank()],]))
            ->add('designLife', 'number',
                $this->options([
                    'label'       => 'Design Life',
                    'attr'        => ['class' => 'required', 'next_group_addon' => 'months'],
                    'required'    => true,
                    'constraints' => [
                        new NotBlank(), new Type(['type' => 'double'])]]))
            ->add('mocNumber', 'text',
                $this->options([
                    'label'       => 'MOC Number',
                    'attr'        => ['class' => ''],
                    'required'    => false]))
            ->add($builder->create('installationDate', new DatePickerType(),
                $this->options(['label'       => 'Installation Date',
                    'required'    => false,
                    'constraints' => new Callback(                            [
                            'callback' => function ($value, $context) {
                                $form = $context->getRoot();
                                $data = $form->get('status');
                                // If $data is null then the field was blank, do nothing more
                                if (is_null($data)) {
                                    return;
                                }

                                $status = $data->getData();
                                if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                    $form->addError(new FormError('Installation Date is mandatory'));
                                }
                            }
                        ]
                    ),
                    'attr'        => ['class' => 'inspection-date installation-date-field']], 'date_picker'))->addModelTransformer(new DateTimePickerTransformer()));
        ;
        $this->management = $builder->create('management_group', null,
            ['compound' => true, 'mapped' => false, 'label' => false, 'inherit_data' => true, 'attr' => ['class' => 'col-lg-12']]);

        $this->buildManagement($this->management);
        $builder->add($this->management);
    }

    protected function buildManagement(FormBuilderInterface &$builder) {
        $builder
            ->add('inspectionTechnique', 'text',
                $this->options([
                    'label'       => 'Latest Inspection Technique',
                    'required'    => false,
                    'attr'        => ['class' => 'management_group']]))
            ->add($builder->create('inspectionDate', new DatePickerType(),
                $this->options(['label'       => 'Latest Inspection Date',
                    'required'    => false,
                    'constraints' => [],
                    'attr'        => ['class' => 'inspection-date management_group']], 'date_picker'))->addModelTransformer(new DateTimePickerTransformer()))
            ->add('inspectionFrequency', 'number',
                $this->options([
                    'label'       => 'Inspection Frequency',
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'double'])],
                    'attr'        => ['class' => 'number management_group', 'next_group_addon' => 'months']]))
            ->add('inspectionResults', 'textarea',
                $this->options([
                    'label'    => 'Inspection Results',
                    'required' => false,
                    'attr'     => ['class' => 'management_group']]))
            ->add('bufferDays', 'integer',
                $this->options([
                    'label'       => 'Buffer Days',
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'integer'])],
                    'attr'        => ['class' => 'integer management_group']]));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar'
        ]);
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_anomalybundle_temporaryrepairorderregistrar';
    }
}
