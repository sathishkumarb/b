<?php
/**
 * Author: AIE
 * Date: 2/28/16
 * Time: 1:31 AM
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class RegistrarSearchType extends AnomalyRegistrarType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildRest($builder);
    }

    protected function buildEquipment(FormBuilderInterface &$builder)
    {
        $design_codes = $this->em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')
            ->findBy(
                [
                    'project' => $this->entity->getProject(),
                    'type' => $this->entity->getCode()
                ]
            );

        $builder
            ->add(
                'component',
                'text',
                $this->options(
                    [
                        'label' => 'Component',
                        'position' => 'first',
                        'required' => false,
                    ]
                )
            )
            ->add(
                'designCode',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:AnomalyDesignCode',
                        'property' => 'code',
                        'label' => 'Design Code',
                        'required' => false,
                        'choices' => $design_codes,
                    ]
                )
            )
            ->add(
                'nominalWT',
                'number',
                $this->options(
                    [
                        'label' => 'Nominal Wall Thickness (NWT)',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'corrosionAllowance',
                'number',
                $this->options(
                    [
                        'label' => 'Corrosion Allowance (CA)',
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'required' => false,
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'mAWT',
                'number',
                $this->options(
                    [
                        'label' => 'Minimum Allowable Wall Thickness (MAWT)',
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'required' => false,
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            );
    }

    protected function buildAnomaly(FormBuilderInterface &$builder)
    {
        $builder->add(
            'raisedOn',
            new DatePickerType(),
            $this->options(
                [
                    'label' => 'Raised On Date',
                    'data' => ($this->entity->getRaisedOn()) ? $this->entity->getRaisedOn()->format(
                        'd F Y'
                    ) : '',
                ],
                'date_picker'
            )
        )
            ->add(
                'defectDescription',
                'text',
                $this->options(
                    [
                        'label' => 'Defect Description',
                        'required' => false,
                    ]
                )
            )

            ->add(
                'locationPoint',
                'text',
                $this->options(
                    [
                        'label' => 'Test Point Location (KP, TP, FP etc)',
                        'required' => false
                    ]
                )
            )
            ->add(
                'threat',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:AnomalyThreat',
                        'choice_label' => function ($o) {
                            return $o->getCode().' - '.$o->getDescription();
                        },
                        'label' => 'Anomaly Threat',
                        'required' => false,
                        'choices' => $this->entity->getProject()->getThreats(),
                    ]
                )
            )
            ->add(
                'classification',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\AnomalyClass',
                        'choice_label' => function ($o) {
                            return $o->getCode().' - '.$o->getDescription();
                        },
                        'label' => 'Anomaly Class',
                        'required' => false,
                        'choices' => $this->entity->getProject()->getClasses(),
                    ]
                )
            );

        $defectOrientation = $builder->create(
            'defect_orientation_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Defect Orientation',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9']
            ]
        );


        $defectOrientation
            ->add(
                'defectOrientationHours',
                'integer',
                $this->options(
                    [
                        'label' => false,
                        'mapped' => false,
                        'required' => false,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0, 'max' => 12])],
                        'attr' => [
                            'container_class' => '',
                            'form_class' => 'col-md-2',
                            'next_group_addon' => 'hr',
                            'class' => 'number'
                        ]
                    ]
                )
            )
            ->add(
                'defectOrientationMinutes',
                'integer',
                $this->options(
                    [
                        'label' => ':',
                        'required' => false,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0, 'max' => 59])],
                        'mapped' => false,
                        'attr' => ['form_class' => 'col-md-3', 'next_group_addon' => 'min', 'class' => 'number']
                    ]
                )
            );

        $builder
            ->add($defectOrientation)
            ->add(
                'corrosionRate',
                'number', //show only if threat was CR-Corrosion
                $this->options(
                    [
                        'label' => 'Corrosion Rate',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm/year', 'class' => 'number cr-ml'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'remainingWallThickness',
                'number',
                $this->options(
                    [
                        'label' => 'Remaining Wall Thickness',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'retiralValue',
                'number',
                $this->options(
                    [
                        'label' => 'Retiral Value',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'length',
                'number',
                $this->options(
                    [
                        'label' => 'Length',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'width',
                'number',
                $this->options(
                    [
                        'label' => 'Width',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            );

        $builder
            ->add(
                'trackingCriteria',
                CollectionType::class,
                array(
                    'entry_type' => TrackingCriteriaType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'label' => false
                )
            )
            ->add(
                'add_tracking_criteria',
                'button',
                $this->options(
                    [
                        'label' => '+ Add Tracking Criteria',
                        'attr' => ['class' => 'btn btn-link', 'container_class' => 'button-container col-md-offset-3'],

                    ]
                    ,
                    'button'
                )
            );

        $builder
            ->add(
                'comments',
                'textarea',
                $this->options(['label' => 'Comments', 'required' => false])
            )
            ->add(
                'summary',
                'textarea',
                $this->options(['label' => 'Anomaly Summary Details', 'required' => false])
            )
            ->add(
                'recommendations',
                'textarea',
                $this->options(['label' => 'Recommendations', 'required' => false])
            );

    }

    protected function buildAssessment(FormBuilderInterface &$builder)
    {
        if ($this->entity->getProject() instanceof RiskProject) {
            $builder
                ->add(
                    'probability',
                    'choice',
                    $this->options(
                        [
                            'label' => 'Probability',
                            'choices' => AnomalyRegistrar::getRiskChoices(),
                            'required' => false,
                        ]
                    )
                )//from 1 - 5
                ->add('criticality', 'hidden');
            $builder->add(
                'consequence','choice',
                $this->options(
                    [
                        'label' => 'Consequence',
                        'choices' => AnomalyRegistrar::getRiskChoices(),
                        'required' => false,
                    ]
                )
            )//from 1 - 5
            ;
        } else {
            $builder
                ->add(
                    'criticality',
                    'choice',
                    $this->options(
                        [
                            'label' => 'Criticality',
                            'choices' => $this->entity->getProject()->getEnabledCriticalityChoices(),
                            'required' => false,
                        ]
                    )
                )
                ->add('probability', 'hidden')
                ->add('consequence', 'hidden');
        }
    }

    protected function buildRemedial(FormBuilderInterface &$builder)
    {
        $actionsHelp = [
            'Inspection' => 'For reporting & management of anomalies requiring future inspection (i.e. UT Wall thickness monitoring etc.)',
            'Repair Order' => 'For reporting and management of repairs (i.e. Pipework Replacement etc.)',
            'Temporary' => 'For reporting & management of temporary repairs (i.e. Composite Repairs, Box Clamps etc.)',
            'Fabric' => 'For reporting & management of fabric maintenance (Prevenatative Maintenance i.e. Painting, Insulation Repairs etc.)',
            'Assessment' => 'For further assessment and analysis to decide the status of the Anomaly'
        ];

        $actionChoices = $this->entity->getActionsChoices();
        $builder->add(
            'actionRequired',
            'choice',
            $this->options(
                [
                    'required' => false,
                    'label' => 'Action Required',
                    'choices' => $actionChoices,
                    'multiple' => true,
                    'expanded' => true,
                    'mapped' => true,
                    'attr' => ['class' => 'col-md-9'],
                ],
                'choice_expandable'
            )
        )
            ->add(
                'action_confirmation',
                'choice',
                $this->options(
                    [
                        'mapped' => false,
                        'required' => false,
                        'choices' => [
                            'Temporary Repair is for an External Defect' => 'Temporary Repair is for an External Defect',
                            'Temporary Repair is "Engineered" (on the contrary of "Bandag")' => 'Temporary Repair is "Engineered" (on the contrary of "Bandag")',
                            'Temporary Repair design life is > 10 years' => 'Temporary Repair design life is > 10 years',
                            'Temporary Repair has a detailed Inspection plan' => 'Temporary Repair has a detailed Inspection plan',
                        ],
                        'multiple' => true,
                        'expanded' => true,
                    ],
                    'choice_expandable'
                )
            );

    }

    protected function buildManagement(FormBuilderInterface &$builder)
    {
        $builder
            ->add(
                'inspectionTechnique',
                'text',
                $this->options(
                    [
                        'label' => 'Latest Inspection Technique',
                        'required' => false,
                    ]
                )
            )
            ->add(
                'inspectionDate',
                new DatePickerType(),
                $this->options(
                    [
                        'label' => 'Latest Inspection Date',
                        'required' => false,
                        'data' => ($this->entity->getInspectionDate()) ? $this->entity->getInspectionDate()->format(
                            'd F Y'
                        ) : '',
                        'attr' => ['class' => 'inspection-date']
                    ],
                    'date_picker'
                )
            )
            ->add(
                'inspectionFrequency',
                'number',
                $this->options(
                    [
                        'label' => 'Inspection Frequency',
                        'required' => false,
                        'attr' => ['class' => 'number', 'next_group_addon' => 'months']
                    ]
                )
            )
            ->add(
                'inspectionResults',
                'textarea',
                $this->options(
                    [
                        'label' => 'Inspection Results',
                        'required' => false,
                        'attr' => ['class' => '']
                    ]
                )
            )
            ->add(
                'bufferDays',
                'integer',
                $this->options(
                    [
                        'label' => 'Buffer Days',
                        'required' => false,
                        'data' => 0,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0])],
                        'attr' => ['class' => 'integer']
                    ]
                )
            )
            ->add(
                'closeOutDate',
                new DatePickerType(),
                $this->options(
                    [
                        'label' => 'Close Out Date',
                        'required' => false,
                        'data' => ($this->entity->getCloseOutDate()) ? $this->entity->getCloseOutDate()->format(
                            'd F Y'
                        ) : '',
                        'attr' => ['class' => 'close_out'],
                        'disabled' => ($this->entity->getStatus() !== null) ? $this->entity->getStatusChoices(
                            )[$this->entity->getStatus()] != 'Live' : true,
                    ],
                    'date_picker'
                )
            )
            ->add(
                'closeOutJustification',
                'textarea',
                $this->options(
                    [
                        'label' => 'Close Out Justification',
                        'attr' => ['class' => 'close_out'],
                        'disabled' => ($this->entity->getStatus() !== null) ? $this->entity->getStatusChoices(
                            )[$this->entity->getStatus()] != 'Live' : true,
                    ]
                )
            );

    }

    protected function buildFiles(FormBuilderInterface &$builder){}

    protected function buildRest(FormBuilderInterface &$builder)
    {
        $builder->add(
            'assetTagNumber',
            'text',
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'anomalyCustomId',
            'text',
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'location',
            'entity',
            $this->options(
                [
                    'class' => 'AIEAnomalyBundle:Locations',
                    'property' => 'name',
                    'label' => false,
                    'required' => false,
                    'choices' => $this->entity->getProject()->getLocations(),
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'threat',
            'entity',
            $this->options(
                [
                    'class' => 'AIEAnomalyBundle:AnomalyThreat',
                    'choice_label' => function ($o) {
                        return $o->getCode().' - '.$o->getDescription();
                    },
                    'label' => false,
                    'required' => false,
                    'choices' => $this->entity->getProject()->getThreats(),
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'classification',
            'entity',
            $this->options(
                [
                    'class' => 'AIE\Bundle\AnomalyBundle\Entity\AnomalyClass',
                    'choice_label' => function ($o) {
                        return $o->getCode().' - '.$o->getDescription();
                    },
                    'label' => false,
                    'required' => false,
                    'choices' => $this->entity->getProject()->getClasses(),
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'status',
            'choice',
            $this->options(
                [
                    'required' => false,
                    'choices' => [1=>'Live', 2=>'Closed', 3=>'Cancelled'],
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'statusTag',
            'choice',
            $this->options(
                [
                    'required' => false,
                    'label' => false,
                    'choices' => $this->entity->getStageChoices(),
                    "attr" => array("class" => "form-control")
                ]
            )
        )->add(
            'nextInspectionDate',
            new DatePickerType(),
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => ''
                ],
                'date_picker'
            )
        )
            ->add(
                'spa',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionOwners',
                        'property' => 'user.username',
                        'required' => false,
                        'label' => false,
                        'mapped' => true,
                        'choices' => $this->actionOwners,
                        "attr" => array("class" => "form-control")
                    ]
                )
            );
        $builder->add(
            'component',
            'text',
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    "attr" => array("class" => "form-control")
                ])
        );
        if($this->entity->getProject() instanceof RiskProject){
            $builder->add(
                'riskCriticality',
                'choice',
                $this->options(
                    [
                        'choices' => Registrar::getRiskChoices(false),
                        'required' => false,
                        'mapped' => false,
                        "attr" => array("class" => "form-control")
                    ]
                )
            );
        }else{
            $builder->add(
                'riskCriticality',
                'choice',
                $this->options(
                    [
                        'choices' => $this->entity->getProject()->getEnabledCriticalityChoices(),
                        'required' => false,
                        'mapped' => false,
                        "attr" => array("class" => "form-control")
                    ]
                )
            );
        }
        $builder->add(
            'raisedOn',
            new DatePickerType(),
            $this->options(
                [
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'data' => '',
                ],
                'date_picker'
            )
        );
        if($this->entity instanceof TemporaryRepairOrderRegistrar){
            $builder->add(
                'expiryDate',
                new DatePickerType(),
                $this->options(
                    [
                        'label' => false,
                        'required' => false,
                        'data' => ''
                    ],
                    'date_picker'
                )
            );
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'inherit_data' => false,
                'mapped' => true,
                'required' => false,
                'constraints' => null,
                "attr" => array("class" => "form-control")
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_anomalysearch';
    }
}
