<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 20/09/15
 * Time: 06:24
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\VeracityBundle\Form\FilesEditType as FileEditBase;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilesEditType extends FileEditBase {

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Files'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'aie_bundle_anomalybundle_files_edit';
	}
}