<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ProjectsType extends AbstractType
{
    use Helper\FormStyleHelper;
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $assessmentTypes = [
            1 => 'Risk', 2 => 'Criticality',
        ];
        $approvalLevels = [
            1 => 'One Level Approval', 2 => 'Two Levels Approval',
        ];
        $enable = [1 => "Enabled", 0=>"Disabled"];
        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description'), 'textarea'))
            ->add('referenceNo', 'text', $this->options(array('label' => 'Reference #')))
            ->add('client', 'text', $this->options(array('label' => 'Company')))
            ->add('type', 'choice', $this->options(array('label' => 'Assessment Type', 'choices' => $assessmentTypes, 'mapped'=>false, "attr"=>["class"=>"forCriticality"])))
            ->add('enableProductionCriticality', 'choice', $this->options(array('label' => 'Production Criticality', 'choices' => $enable, 'mapped'=>false)))
            ->add('enableFMCriticality', 'choice', $this->options(array('label' => 'Fabric Maintenance Criticality', 'choices' => $enable, 'mapped'=>false)))
            ->add('approvalLevel', 'choice', $this->options(array('label' => 'Approval Levels', 'choices' => $approvalLevels)))
            ->add('file', 'file', $this->options(array('label' => 'Logo'), 'file'));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Projects'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_projects';
    }
}
