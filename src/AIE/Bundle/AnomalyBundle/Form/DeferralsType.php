<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Deferrals;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class DeferralsType extends AbstractType {
	use FormStyleHelper;

	private $actionSpas;
    private $approvalLevels;
    private $isNew;
    private $username;

	public function __construct($actionSpas, $approvalLevels, $isNew=true, $username=null){
		$this->actionSpas = $actionSpas;
        $this->approvalLevels = $approvalLevels;
        $this->isNew = $isNew;
        $this->username=$username;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

        //$actionChoices = Deferrals::getActionsChoices();

        if($this->isNew){
            $builder->add('actionOwner', 'choice', $this->options([
                'required' => true,
                'label'    => 'SPA',
                'mapped'   => true,
                'choices'  => $this->actionSpas
            ]));
            $builder->add('anomalyCategory', 'choice', $this->options([
                'choices'     => AnomalyRegistrar::getAnomalyCategories(),
                'required'    => true,
                'label'       => 'Category',
                'attr'        => ['class' => 'required'],
                'constraints' => [new NotNull()],
            ]));
        }else{
            $builder->add('actionOwnerName', 'text', $this->options([
                'required' => false,
                'label'    => 'SPA',
                'mapped'   => false,
                'data' => $this->username,
                'attr'        => ['disabled' => 'disabled']
            ]));
        }
			$builder->add('notifyOn', 'integer',
				$this->options([
					'label'       => 'Gets Notified After',
					'required'    => true,
					'data'        => 0,
					'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0])],
					'attr'        => ['class' => 'required integer', 'next_group_addon' => 'deferrals',]]))
            ->add('approvalLevel', 'choice', $this->options([
                'label' => 'Approval Levels',
                'required'    => true,
                'choices' => $this->approvalLevels,
            ]));;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Deferrals'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_deferrals';
	}
}
