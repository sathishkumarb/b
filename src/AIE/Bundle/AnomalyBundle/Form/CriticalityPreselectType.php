<?php
/**
 * Created by AIE
 * Date: 01/04/2018
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class CriticalityPreselectType extends AbstractType
{

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(CriticalityProject $entity){
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_choices',
            "choice",
            $this->options(
                [
                    "label" => "* Ranks List",
                    'required' => true,
                    "choices" =>  $this->entity->getChoices(),
                    "attr" => array("class" => "form-control select2",'style' => 'width: 71%'),
                    "expanded" => true,
                    "multiple" => true,
                ]
            ))
            ->add('vhName', 'text',
            $this->options(['label'       => 'Very High Name',
                'required'    => false,
                'attr'        => ['class' => 'form-control','value'=>'Very High'],
                'constraints' => []]))
            ->add('hName', 'text',
                $this->options(['label'       => 'High Name',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control','value'=>'High'],
                    'constraints' => []]))
            ->add('mName', 'text',
                $this->options(['label'       => 'Medium Name',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control','value'=>'Medium'],
                    'constraints' => []]))
            ->add('lName', 'text',
                $this->options(['label'       => 'Low Name',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control','value'=>'Low'],
                    'constraints' => []]))
            ->add('vlName', 'text',
                $this->options(['label'       => 'Very Low Name',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control','value'=>'Very Low'],
                    'constraints' => []]))
            ->add('vhColor', 'text',
                $this->options(['label'       => 'Ver High Color',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control jscolor','value'=>'AB0000'],
                    'constraints' => [],
                    ]))
            ->add('hColor', 'text',
                $this->options(['label'       => 'High Color',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control jscolor','value'=>'F72200'],
                    'constraints' => [],
                   ]))
            ->add('mColor', 'text',
                $this->options(['label'       => 'Medium Color',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control jscolor','value'=>'EDB518'],
                    'constraints' => [],
                    ]))
            ->add('lColor', 'text',
                $this->options(['label'       => 'Low Color',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control jscolor','value'=>'68E34B'],
                    'constraints' => [],
                    ]))
            ->add('vlColor', 'text',
                $this->options(['label'       => 'Very Low Color',
                    'required'    => false,
                    'attr'        => ['class' => 'form-control jscolor','value'=>'56AB54'],
                    'constraints' => [],
                    ]));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\CriticalityProject',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_criticalityproject';
    }
}