<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PipelineAnomalyLocationsType extends AbstractType {
	use FormStyleHelper;

	private $_locations;

	public function __construct($locations){
		$this->_locations = $locations;
	}
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('location', 'entity',
				$this->options([
					'label' => 'Pipeline',
					'choices' => $this->_locations,
					'mapped' => true,
					'class' => 'AIE\Bundle\AnomalyBundle\Entity\Locations',
					'property' => 'name'
				]), 'entity')
			;

	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => null
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_pipelineanomaly_locations';
	}
}
