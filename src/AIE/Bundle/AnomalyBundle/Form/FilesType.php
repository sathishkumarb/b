<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 20/09/15
 * Time: 06:23
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\VeracityBundle\Form\FilesType as FileBase;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilesType extends FileBase {

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Files'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_files_edit';
	}

}