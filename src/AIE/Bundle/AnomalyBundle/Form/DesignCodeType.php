<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class DesignCodeType extends AbstractType
{
    use FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'type',
                'choice',
                $this->options(
                    [
                        'choices' => AnomalyRegistrar::getAnomalyCategories(),
                        'required' => true,
                        'label' => 'Category',
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'code',
                'text',
                $this->options(
                    [
                        'label' => 'Code',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotBlank()],
                    ]
                )
            )
            ->add(
                'description',
                'textarea',
                $this->options(['label' => 'Description', 'required' => false])
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\DesignCode'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_designcode';
    }
}
