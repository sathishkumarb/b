<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Projects;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class ActionRegistrarType extends AbstractType {
	use FormStyleHelper;

	/** @var ActionRegistrar */
	protected $entity;
	/** @var AnomalyRegistrar */
	protected $anomaly;
    /** @var  ArrayCollection */
    protected $actionOwners;

	protected $currDate;
	protected $assessment;
    protected $files;
	protected $action;

	public function __construct(ActionRegistrar $entity, AnomalyRegistrar $anomaly, $actionOwners) {
		$this->entity = $entity;
		$this->anomaly = $anomaly;
		$this->currDate = new \DateTime();
        $this->actionOwners = $actionOwners;
	}


	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$this->action = $builder->create('action_group', null,
			['compound'     => true,
			 'mapped'       => false,
			 'label'        => false,
			 'inherit_data' => true,
			 'attr'         => ['class' => 'col-lg-12']]);

		$this->buildAction($this->action);
		$builder->add($this->action);

        $this->assessment = $builder->create('assessment_group', null,
            ['compound'     => true,
                'mapped'       => false,
                'label'        => false,
                'inherit_data' => true,
                'attr'         => ['class' => 'col-lg-12']]);
        $this->files = $builder->create(
            'files_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );
        $this->buildFiles($this->files);
        $builder->add($this->files);

        $this->buildRest($builder);

	}

	protected function buildAction(FormBuilderInterface &$builder) {
		$builder
			->add(
                $builder->create('expiryDate', new DatePickerType(),
				$this->options(['label'       => 'Technical Expiry Date',
				                'required'    => true,
				                'error_bubbling' => false,
				                'constraints' => [new NotNull()],
				                'attr'        => ['class' => 'inspection-date required']], 'date_picker')
                )->addModelTransformer(new DateTimePickerTransformer()))
//			->add('deferred', 'integer',
//				$this->options(['label'       => 'Deferred',
//				                'required'    => true,
//				                'constraints' => [new NotBlank(), new Type(['type' => 'integer'])],
//				                'attr'        => ['class' => 'required integer']]))
			->add('recommendations', 'textarea',
				$this->options(['label' => 'Recommendations', 'required' => false]));
	}

    protected function buildFiles(FormBuilderInterface &$builder){
        $builder->add(
            'files',
            CollectionType::class,
            array(
                'entry_type' => AnomalyFilesType::class,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label' => false,
                'data' => null,
                'mapped' => false,
            )
        )->add(
            'add_files',
            'button',
            $this->options(
                [
                    'label' => '+ Add Attachment',
                    'attr' => ['class' => 'btn btn-link', 'container_class' => 'button-container col-md-offset-3'],

                ]
                ,
                'button'
            )
        );
    }
    
	protected function buildRest(FormBuilderInterface &$builder) {
		$builder
			->add('status', 'choice', $this->options([
					'required' => true, 'choices' => $this->entity->getStatusChoices()
				]
			))
            ->add(
                'spa',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionOwners',
                        'property' => 'user.username',
                        'required' => true,
                        'label' => 'Action Owner',
                        'mapped' => true,
                        'choices' => $this->actionOwners
                    ]
                )
            )
			;
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_actionregistrar';
	}
}
