<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class LocationsType extends AbstractType
{
    use Helper\FormStyleHelper;
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false, 'attr' => ['placeholder' => '']), 'textarea'))
            ->add('geometry', 'hidden', array('attr' => array('id' => 'geometryMap')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Locations'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_locations';
    }
}
