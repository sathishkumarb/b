<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ThreatType extends AbstractType
{
    use FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'text', $this->options(array('label' => 'Code')))
            ->add(
                'description',
                'textarea',
                $this->options(
                    array('label' => 'Description', 'required' => false, 'attr' => ['placeholder' => '']),
                    'textarea'
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Threat'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_threat';
    }
}
