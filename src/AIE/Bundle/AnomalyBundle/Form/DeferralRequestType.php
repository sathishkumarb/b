<?php
/**
 * Created by Sathish Kumar
 * Date: 19/09/16
 * Time: 20:38
 */

namespace AIE\Bundle\AnomalyBundle\Form;


use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class DeferralRequestType extends AbstractType {
	use FormStyleHelper;

	/** @var Registrar */
	protected $entity;
	protected $currDate;
	protected $nextInspectionDate;

	public function __construct(Registrar $entity, $nextInspectionDate) {
		$this->entity = $entity;
		$this->currDate = new \DateTime();
		$this->nextInspectionDate = $nextInspectionDate;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
		 	->add('id', 'text',
                $this->options(
                    ['label'       => 'Anomaly/TRO/RO/FM ID',
                    	'data'     => $this->entity->getFullId(),
                        'disabled'    => true,
                        ]))
			->add('currentDeferral', 'text', $this->options([
				'label'    => 'Deferral (No. of Times)',
				'data'     => $this->entity->getDeferred(),
				'mapped'   => false,
				'disabled' => true,
			]));

			if ($this->entity instanceof AnomalyRegistrar ||
				($this->entity instanceof TemporaryRepairOrderRegistrar && $this->entity->getInstallationDate() !== null)
			) {
			
		        $builder->add('currentNextInspectionDate', 'text',
		                $this->options(['label'    => 'Current Next Inspection Date',
		                    'attr'     => ['class' => ''],
		                    'data'     => ($this->entity->getNextInspectionDate()) ? $this->entity->getNextInspectionDate()->format('d F Y') : '',
		                    'mapped'   => false,
		                    'disabled' => true,
		                ]))
		    			->add('nextInspectionDate', 'text',
		                $this->options(
		                    ['label'       => 'DefferedTo',
		                    	'data'     => ($this->nextInspectionDate) ? $this->nextInspectionDate->format('d F Y') : '',
		                        'disabled'    => true,
		                        ]));
		    }
	    	else
	    	{

				//RO, TRO (before installation date is specified in the TRO register) and FM
				$builder->add('currentExpiryDate', 'text',
						$this->options(['label'    => 'Old Technical Expiry Date',
					                'attr'     => ['class' => ''],
					                'data'     => ($this->entity->getExpiryDate()) ? $this->entity->getExpiryDate()->format('d F Y') : '',
					                'mapped'   => false,
					                'disabled' => true,]))
						->add('expiryDate', 'text',
						$this->options(['label'       => 'DefferedTo',
							            'data'        => ($this->nextInspectionDate) ? $this->nextInspectionDate->format('d F Y') : '',
						                'disabled'    => true,
						          		]));
			}

	        $builder->add('deferralJustification', 'textarea',
	                $this->options(
	                    ['label'       => 'Reason for Deferral',
	                        'disabled'    => true,
	                        'data'     => $this->entity->getDeferralSafeJustification()
	                        ]))
		            ->add('deferralSafeJustification', 'textarea',
		                $this->options(
		                    ['label'       => 'Technical Justification for Safe Deferral',
		                        'disabled'    => true,
		                        'data'     => $this->entity->getDeferralJustification()
		                        ]))
		         	->add('status', ChoiceType::class,  
		         		$this->options(
						    ['choices'  => array(
						        '1' => 'Approve',
						        '0' => 'Reject',
						    ),
						    'placeholder' => 'Choose an option',
								]))
		            ->add('deferralRejectJustification', 'textarea',
		                $this->options(
		                    ['label'       => 'Justification for Action',
		                        'disabled'    => false,
		                        'data'     => NULL
		                        ]));
	}
	

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Registrar'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_deferral_request';
	}
}