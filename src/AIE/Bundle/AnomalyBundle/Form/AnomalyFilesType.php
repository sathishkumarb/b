<?php
/**
 * User: mokha
 * Date: 2/27/16
 * Time: 1:22 PM
 */

namespace AIE\Bundle\AnomalyBundle\Form;


use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;


class AnomalyFilesType extends AbstractType
{
    use FormStyleHelper;


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $files = $builder->create(
            'files_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Attachment:',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9'],
            ]
        );

        $files
            ->add('filename', 'text', $this->options(['label' => '*Name', 'required' => true]))
            ->add('caption', 'text', $this->options(['label' => 'Caption', 'required' => false]))
            ->add('filereportcategory', 'choice', [
                'required' => false,
                'label' => '*Category',
                'mapped' => true,
                'position'    => ['after' => 'caption'],
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'form-control'],
                'choices' => ['P&ID' => 'P&ID', 'Isometric' => 'Isometric', 'Picture' => 'Picture','Inspection Report' => 'Inspection', 'Others' => 'Others'],
            ])
            ->add('company', 'text', $this->options(['label' => 'Company', 'required' => true]))
            ->add('document_number', 'text', $this->options(['label' => 'Document Number', 'required' => true]))
            ->add(
                'document_date',
                new DatePickerType(),
                $this->options(
                    [
                        'mapped' => true,
                        'disabled' => false,
                        'label' => 'Document Date',
                        'attr' => ['class' => 'col-md-9'],
                        'label_attr' => ['class' => 'col-md-3 control-label']
                    ],
                    ''
                )
            )
            ->add('file', 'file', $this->options(['label' => '*Upload','required' => true, 'attr' => ['class' => 'fileInputValidate'],], 'file'));

        $files->get('document_date')
            ->addModelTransformer(new DateTimePickerTransformer());

        $builder->add($files);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Files'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_files';
    }
}