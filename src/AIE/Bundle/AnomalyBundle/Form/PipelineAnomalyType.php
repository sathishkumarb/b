<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\Pipelines;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\NotNull;

class PipelineAnomalyType extends AnomalyRegistrarType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);

	}

	protected function buildEquipment(FormBuilderInterface &$builder) {
		parent::buildEquipment($builder);
		$builder
			->add('nominalWT', 'number',
				$this->options(['label'       => 'Nominal Wall Thickness (NWT)',
				                'attr'        => ['next_group_addon' => 'mm', 'class' => 'required number'],
				                'required'    => true,
				                'position'    => ['before' => 'corrosionAllowance'],
				                'constraints' => [new NotBlank(), new Type(['type' => 'double'])]
				]))
            ->add('outerDiameter', 'number',
                $this->options(['label'       => 'Outer Diameter',
                    'attr'        => ['next_group_addon' => 'mm', 'class' => 'required number'],
                    'required'    => true,
                    'position'    => ['after' => 'designCode'],
                    'constraints' => [new NotBlank(), new Type(['type' => 'double'])]
                ]))
			->add('corrosionAllowance', 'number',
				$this->options(['label'       => 'Corrosion Allowance (CA)',
				                'attr'        => ['next_group_addon' => 'mm', 'class' => 'required number'],
				                'required'    => true,
				                'position'    => ['before' => 'mAWT'],
				                'constraints' => [new NotBlank(), new Type(['type' => 'double'])]
				]))
			->add('sMYS', 'number',
				$this->options(['label'       => 'Specified Minimum Yield Strength (SMYS)',
				                'attr'        => ['next_group_addon' => 'ksi', 'class' => 'required number'],
				                'required'    => true,
				                'constraints' => [new NotBlank(), new Type(['type' => 'double'])]
				]))->add('sMTS', 'number',
				$this->options(['label'       => 'Specified Minimum Tensile Strength (SMTS)',
				                'attr'        => ['next_group_addon' => 'ksi', 'class' => 'required number'],
				                'required'    => true,
				                'constraints' => [new NotBlank(), new Type(['type' => 'double'])]]
				));
	}

	protected function buildAnomaly(FormBuilderInterface &$builder) {
		parent::buildAnomaly($builder);
		$builder->add('location', 'entity', $this->options([
			'class'       => 'AIEAnomalyBundle:Pipelines',
			'property'    => 'name',
			'label'       => 'Location',
			'constraints' => [new NotNull()],
			'required'    => true,
			'attr'        => ['class' => 'required'],
			'choices'     => array_filter($this->entity->getProject()->getLocations()->toArray(), function ($location) {
				// return only pipeline locations
				return $location instanceof Pipelines;
			})
		]))->add('locationPoint', 'text', $this->options(
			[
				'label'       => 'Test Point Location (KP, TP, FP etc)',
				'attr'        => ['next_group_addon' => 'km'],
				'required'    => false
			]
		))
			->add('tolerance', 'number', $this->options(['label'       => 'Inspection Tool Tolerance',
			                                             'required'    => false,
			                                             'attr'        => ['next_group_addon' => '%', 'class' => 'number'],
			                                             'constraints' => [new Type(['type' => 'double'])],
			                                             'position'    => ['before' => 'comments']]));


		$gps = $builder->create('gps_group', null, ['compound'   => true,
		                                            'mapped'     => false,
		                                            'inherit_data' => true,
		                                            'label'      => 'GPS Coordinates',
		                                            'label_attr' => ['class' => 'col-md-3 control-label'],
		                                            'attr'       => ['class' => 'col-md-9'],
		                                            'position'   => ['before' => 'threat'],

		]);
		$gps->add('gpsE', 'text', $this->options(
			['label' => false,
			 'attr'  => ['container_class' => '', 'form_class' => 'col-md-6', 'next_group_addon' => 'easting', 'class' => '']]))
			->add('gpsN', 'text', $this->options(
				['label' => false,
				 'attr'  => ['container_class' => 'col-md-offset-1', 'form_class' => 'col-md-6', 'next_group_addon' => 'northing', 'class' => '']]));
		$builder->add($gps);

	}

	protected function buildAssessment(FormBuilderInterface &$builder) {
		parent::buildAssessment($builder);
	}


	protected function buildRemedial(FormBuilderInterface &$builder) {
		parent::buildRemedial($builder);

	}

	protected function buildManagement(FormBuilderInterface &$builder) {
		parent::buildManagement($builder);

	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_pipelineanomaly';
	}
}
