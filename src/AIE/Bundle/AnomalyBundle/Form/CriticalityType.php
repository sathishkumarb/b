<?php
/**
 * Created by AIE
 * Date: 28/06/15
 * Time: 23:03
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityProject;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class CriticalityType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(CriticalityProject $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('criticality', new CriticalityRanksType($this->entity->getCriticality()))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\CriticalityProject',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_anomalybundle_criticalityproject';
    }
}