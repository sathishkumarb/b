<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 23:10
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class CriticalityRanksType extends AbstractType
{

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(CriticalityRanks $entity){
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vhDescription', 'textarea', $this->options(array(
                'data' => $this->entity->getVhDescription(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('vhAction', 'textarea', $this->options(array(
                'data' => $this->entity->getVhAction(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('hDescription', 'textarea', $this->options(array(
                'data' => $this->entity->getHDescription(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('hAction', 'textarea', $this->options(array(
                'data' => $this->entity->getHAction(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('mDescription', 'textarea', $this->options(array(
                'data' => $this->entity->getMDescription(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('mAction', 'textarea', $this->options(array(
                'data' => $this->entity->getMAction(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('lDescription', 'textarea', $this->options(array(
                'data' => $this->entity->getLDescription(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('lAction', 'textarea', $this->options(array(
                'data' => $this->entity->getLAction(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('vlDescription', 'textarea', $this->options(array(
                'data' => $this->entity->getVlDescription(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            )
            ->add('vlAction', 'textarea', $this->options(array(
                'data' => $this->entity->getVlAction(),
                'attr' => array('class' => '','rows'=>5)
            ), 'textarea')
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\CriticalityRanks',
            'property' => 'id'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_criticalityranks';
    }
}