<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class PipelinesType extends LocationsType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
		$builder
			->add('length', 'number',
				$this->options(['label'       => 'Length',
				                'constraints' => [new Type(['type' => 'double']), new Range(['min' => 0])],
				                'attr'        => ['class' => 'number', 'next_group_addon' => 'km']]))
			->add('outerDiameter', 'number',
				$this->options(['label'       => 'Outer Diameter',
				                'constraints' => [new Type(['type' => 'double']), new Range(['min' => 0])],
				                'attr'        => ['class' => 'number', 'next_group_addon' => 'inch']]))
			->add('mAOP', 'number',
				$this->options(['label'       => 'MAOP',
				                'constraints' => [new Type(['type' => 'double']), new Range(['min' => 0])],
				                'attr'        => ['class' => 'number', 'next_group_addon' => 'psi']]))
			->add('designFactor', 'number',
				$this->options(['label'       => 'Design Factor',
				                'constraints' => [new Type(['type' => 'double']), new Range(['min' => 0])],
				                'attr'        => ['class' => 'number']]));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Pipelines'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_pipelines';
	}
}
