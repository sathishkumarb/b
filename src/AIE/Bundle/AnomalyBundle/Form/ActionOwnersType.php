<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 11:28
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ActionOwnersType extends AbstractType {

    use Helper\FormStyleHelper;

    private $actionSpas;

    public function __construct($actionSpas){
        $this->actionSpas = $actionSpas;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('user', 'entity', $this->options([
			    'class'    => 'AIE\Bundle\UserBundle\Entity\User',
			    'property' => 'username',
			    'label'    => 'User',
	            'mapped' => false,
		    ]))*/
            ->add('user', 'choice', $this->options([
                'required' => true,
                'label'    => 'SPA',
                'mapped'   => true,
                'choices'  => $this->actionSpas
            ]))
            ->add('position', 'text', $this->options(array('label' => 'Position')))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionOwners'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_actionowners';
    }

}
