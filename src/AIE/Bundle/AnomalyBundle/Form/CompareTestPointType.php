<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 29/06/15
 * Time: 11:28
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class CompareTestPointType extends AbstractType {

    use Helper\FormStyleHelper;

    /** @var  Projects */
    protected $project;

    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'location',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:Locations',
                        'property' => 'name',
                        'label' => false,
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'choices' => $this->project->getLocations(),
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add('locationPoint', 'text',
                $this->options([
                    'label'       => false,
                    'required'    => true,
                    'attr'        => ['class' => 'required form-control'],
                    'constraints' => [new NotBlank()]
                    ]));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_comparetp';
    }

}
