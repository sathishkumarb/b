<?php
/**
 * User: mokha
 * Date: 3/5/16
 * Time: 11:51 PM
 */

namespace AIE\Bundle\AnomalyBundle\Form;


use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class PipelineSectionSegmentType extends AbstractType
{
    use FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $segment = $builder->create(
            'segment_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Start KP',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9 not-cr-ml']
            ]
        );

        $segment
            ->add(
                'x',
                'number',
                $this->options(
                    [
                        'label' => false,
                        'required' => true,
                        'attr' => ['container_class' => '', 'form_class' => 'col-md-5', 'class' => 'required number x'],
                        'constraints' => [new NotNull(), new Type(['type' => 'double'])],
                    ]
                )
            )
            ->add(
                'y',
                'number',
                $this->options(
                    [
                        'label' => 'End KP',
                        'required' => true,
                        'attr' => ['form_class' => 'col-md-4', 'class' => 'number required y'],
                        'constraints' => [new NotNull(), new Type(['type' => 'double'])],
                    ]
                )
            );


        $builder->add($segment);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\PipelineSectionSegment'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_pipelinesectionsegment';
    }
}