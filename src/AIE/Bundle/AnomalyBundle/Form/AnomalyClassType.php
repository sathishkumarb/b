<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AnomalyClassType extends AbstractType {
	use FormStyleHelper;

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('code', 'text', $this->options(['label' => 'Code']))
			->add('description', 'textarea', $this->options(['label' => 'Description', 'required' => false, 'attr' => ['placeholder' => '']], 'textarea'))
            ->add('isMetalLoss', 'choice', $this->options([
                    'required' => true, 'choices' => [0=>'No',1=>'Yes'],
                    'label'=>'Metal Loss Class?'
                ]
            ));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\AnomalyClass'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_anomalyclass';
	}
}
