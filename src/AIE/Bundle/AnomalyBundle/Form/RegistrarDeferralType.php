<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 04/01/16
 * Time: 20:38
 */

namespace AIE\Bundle\AnomalyBundle\Form;


use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class RegistrarDeferralType extends AbstractType {
	use FormStyleHelper;

	/** @var Registrar */
	protected $entity;
	protected $currDate;

	public function __construct(Registrar $entity) {
		$this->entity = $entity;
		$this->currDate = new \DateTime();
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
			->add('currentDeferral', 'text', $this->options([
				'label'    => 'Deferral (No. of Times)',
				'data'     => $this->entity->getDeferred(),
				'mapped'   => false,
				'disabled' => true,
			]))
        ;

		if ($this->entity instanceof AnomalyRegistrar ||
			($this->entity instanceof TemporaryRepairOrderRegistrar && $this->entity->getInstallationDate() !== null)
		) {
			//Anomaly Inspections and TRO Inspections (once installation date is specified in the TRO register)

            $builder
        		->add('currentNextInspectionDate', 'text',
                $this->options(['label'    => 'Current Next Inspection Date',
                    'attr'     => ['class' => ''],
                    'data'     => ($this->entity->getNextInspectionDate()) ? $this->entity->getNextInspectionDate()->format('d F Y') : '',
                    'mapped'   => false,
                    'disabled' => true,
                ]))
                ->add('nextInspectionDate', new DatePickerType(),
                    $this->options(['label'       => 'Deferred to',
                        'required'    => true,
                        'constraints' => [new NotNull()],
                        'data'        => ($this->entity->getNextInspectionDate()) ? $this->entity->getNextInspectionDate()->format('d F Y') : '',
                        'attr'        => ['class' => 'inspection-date required']], 'date_picker'));
		} else {
			//RO, TRO (before installation date is specified in the TRO register) and FM
			$builder->add('currentExpiryDate', new DatePickerType(),
				$this->options(['label'    => 'Old Technical Expiry Date',
				                'attr'     => ['class' => ''],
				                'data'     => ($this->entity->getExpiryDate()) ? $this->entity->getExpiryDate()->format('d F Y') : '',
				                'mapped'   => false,
				                'disabled' => true,
				], 'date_picker'))
				->add('expiryDate', new DatePickerType(),
					$this->options(['label'       => 'New Technical Expiry Date',
					                'required'    => true,
					                'constraints' => [new NotNull()],
					                'data'        => ($this->entity->getExpiryDate()) ? $this->entity->getExpiryDate()->format('d F Y') : '',
					                'attr'        => ['class' => 'inspection-date required']], 'date_picker'));
		}

        $builder
            ->add('deferralJustification', 'textarea',
                $this->options(
                    ['label'       => 'Reason for Deferral',
                        'required'    => true,
                        'constraints' => [new NotBlank()],
                        'attr'        => ['class' => 'required']]))
            ->add('deferralSafeJustification', 'textarea',
                $this->options(
                    ['label'       => 'Technical Justification for Safe Deferral',
                        'required'    => true,
                        'constraints' => [new NotBlank()],
                        'attr'        => ['class' => 'required']]));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\Registrar'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_deferral_registrar';
	}
}