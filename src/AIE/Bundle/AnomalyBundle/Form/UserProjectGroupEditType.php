<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class UserProjectGroupEditType extends AbstractType {

    use Helper\FormStyleHelper;

    protected $em;
    protected $reflection_id;

    public function __construct($em, $rId)
    {
        $this->em = $em;
        $this->reflection_id = $rId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
                /*
                  ->add('user', 'entity', array(
                  'class' => 'UserBundle:User',
                  'property' => 'username',
                  )) */
                ->add('group', 'entity', $this->options(array(
                            'class' => 'AIEAnomalyBundle:ReflectionAnomalyGroup',
                            'property' => 'name',
                            'label' => 'Position',
                            'data' => $this->em->getReference('AIEAnomalyBundle:ReflectionAnomalyGroup', $this->reflection_id )
                )));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\UserProjectGroup'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_anomalybundle_userprojectgroup';
    }

}
