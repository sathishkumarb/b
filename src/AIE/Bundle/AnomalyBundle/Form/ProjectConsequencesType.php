<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 28/06/15
 * Time: 22:30
 */

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ProjectConsequencesType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(ProjectConsequences $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('desc1', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc1(),
                'attr' => array('class' => 'consequence')
            ), 'textarea')
            )
            ->add('desc2', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc2(),
                'attr' => array('class' => 'consequence')
            ), 'textarea')
            )
            ->add('desc3', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc3(),
                'attr' => array('class' => 'consequence')
            ), 'textarea')
            )
            ->add('desc4', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc4(),
                'attr' => array('class' => 'consequence')
            ), 'textarea')
            )
            ->add('desc5', 'textarea', $this->options(array(
                'data' => $this->entity->getDesc5(),
                'attr' => array('class' => 'consequence')
            ), 'textarea')
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\ProjectConsequences'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_anomalybundle_projectconsequences';
    }

}
