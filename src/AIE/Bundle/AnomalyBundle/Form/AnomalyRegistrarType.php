<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyClass;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\PipelineAnomaly;
use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\AnomalyBundle\Entity\TemporaryRepairOrderRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class AnomalyRegistrarType extends AbstractType
{
    use FormStyleHelper;

    /** @var  AnomalyRegistrar */
    protected $entity;
    /** @var  EntityManager */
    protected $em;
    /** @var  ArrayCollection */
    protected $actionOwners;

    // These variables are used for storing fields under groups
    protected $equipment;
    protected $anomaly;
    protected $assessment;
    protected $remedial;
    protected $management;
    protected $files;
    protected $currDate;
    protected $spaUserId;
    protected $classesList;


    public function __construct($entity, $em, $actionOwners, $actionOwner)
    {
        $this->entity = $entity;
        $this->em = $em;
        $this->currDate = new \DateTime();
        $this->actionOwners = $actionOwners;
        $this->actionOwner = $actionOwner;
        $this->classesList = [];
        $classes = $this->em->getRepository('AIEAnomalyBundle:AnomalyClass')->findBy([
            'isMetalLoss' => 1,
            'project'=>$this->entity->getProject()->getId()
        ]);
        foreach ($classes as $c){
            array_push($this->classesList, $c->getId());
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->equipment = $builder->create(
            'equipment_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );
        $this->anomaly = $builder->create(
            'anomaly_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );
        $this->assessment = $builder->create(
            'assessment_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );
        $this->remedial = $builder->create(
            'remedial_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );
        $this->management = $builder->create(
            'management_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );

        $this->files = $builder->create(
            'files_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'attr' => ['class' => 'col-lg-12']
            ]
        );

        $this->buildEquipment($this->equipment);
        $this->buildAnomaly($this->anomaly);
        $this->buildAssessment($this->assessment);
        $this->buildRemedial($this->remedial);
        $this->buildManagement($this->management);
        $this->buildFiles($this->files);
        $builder->add($this->equipment)
        ->add($this->anomaly)
        ->add($this->assessment)
        ->add($this->remedial)
        ->add($this->management)

        ->add($this->files);

        $this->buildRest($builder);

    }

    protected function buildEquipment(FormBuilderInterface &$builder)
    {
        $design_codes = $this->em->getRepository('AIEAnomalyBundle:AnomalyDesignCode')
            ->findBy(
                [
                    'project' => $this->entity->getProject(),
                    'type' => $this->entity->getCode()
                ]
            );

        $builder
            ->add(
                    'assetTagNumber',
                    'text',
                    $this->options(
                        [
                            'label' => 'Asset Tag Number',
                            'position' => 'first',
                            'required' => true,
                            'attr' => ['class' => 'required'],
                            'constraints' => [new NotBlank()],
                        ]
                    )
            )
            ->add(
                'component',
                'text',
                $this->options(
                    [
                        'label' => 'Component',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotBlank()],
                    ]
                )
            )
            ->add(
                'componentDescription',
                'textarea',
                $this->options(
                    [
                        'label' => 'Description',
                        'required' => true,
                        'position'    => ((!$this->entity instanceof PipelineAnomaly) ? ['after' => 'system'] : ['after' => 'component']),
                        'attr' => ['rows'=>4, 'class'=>'required'],
                        'constraints' => [],
                    ]
                )
            )
            ->add(
                'designCode',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:AnomalyDesignCode',
                        'property' => 'code',
                        'label' => 'Design Code',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'choices' => $design_codes,
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'nominalWT',
                'number',
                $this->options(
                    [
                        'label' => 'Nominal Wall Thickness (NWT)',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'corrosionAllowance',
                'number',
                $this->options(
                    [
                        'label' => 'Corrosion Allowance (CA)',
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'required' => false,
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add('drawingNumber', 'text',
                $this->options(['label'       => 'Drawing Number',
                    'required'    => false,
                    'attr'        => ['class' => ''],
                    'constraints' => []]))
            ->add(
                'mAWT',
                'number',
                $this->options(
                    [
                        'label' => 'Minimum Allowable Wall Thickness (MAWT)',
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number'],
                        'required' => false,
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            );
    }

    protected function buildAnomaly(FormBuilderInterface &$builder)
    {
        $builder
            ->add(
                $builder->create('InspDate',
                    new DatePickerType(),
                    $this->options(
                        [
                            'label' => 'Inspection Date',
                            'required' => false,
                            'attr' => ['class' => ''],
                        ],
                        'date_picker'
                    )
                )->addModelTransformer(new DateTimePickerTransformer()))
            ->add(
            $builder->create('raisedOn',
            new DatePickerType(),
            $this->options(
                [
                    'label' => 'Raised On Date',
                    'required' => true,
                    'attr' => ['class' => 'required'],
                    'constraints' => [new NotNull()],
                ],
                'date_picker'
            )
            )->addModelTransformer(new DateTimePickerTransformer()))
            ->add(
                'anomalyCustomId',
                'text',
                $this->options(
                    [
                        'label' => 'Anomaly ID',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotBlank()],
                    ]
                )
            )

            ->add(
                'defectDescription',
                'text',
                $this->options(
                    [
                        'label' => 'Defect Description',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotBlank()]
                    ]
                )
            )
            ->add(
                'location',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:Locations',
                        'property' => 'name',
                        'label' => 'Location',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'choices' => $this->entity->getProject()->getLocations(),
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'locationPoint',
                'text',
                $this->options(
                    [
                        'label' => 'Test Point Location (KP, TP, FP etc)',
                        'required' => false,
                    ]
                )
            )
            ->add(
                'threat',
                'entity',
                $this->options(
                    [
                        'class' => 'AIEAnomalyBundle:AnomalyThreat',
                        'choice_label' => function ($o) {
                            return $o->getCode().' - '.$o->getDescription();
                        },
                        'attr' => ['class' => 'required'],
                        'label' => 'Anomaly Threat',
                        'required' => true,
                        'choices' => $this->entity->getProject()->getThreats(),
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'classification',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\AnomalyClass',
                        'choice_label' => function ($o) {
                            return $o->getCode().' - '.$o->getDescription();
                        },
                        'label' => 'Anomaly Class',
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'choices' => $this->entity->getProject()->getClasses(),
                        'constraints' => [new NotNull()],
                    ]
                )
            )
            ->add(
                'metalLoss',
                'hidden',
                $this->options(
                    [
                        'data' =>json_encode($this->classesList),
                        'label' => 'Anomaly Class',
                        'attr'=>['class' =>'checkClassification'],
                        'required' => false,
                        'mapped'=>false
                    ]
                )
            );

        $defectOrientation = $builder->create(
            'defect_orientation_group',
            null,
            [
                'compound' => true,
                'mapped' => false,
                'inherit_data' => true,
                'label' => 'Defect Orientation',
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr' => ['class' => 'col-md-9']
            ]
        );


        $defectOrientation
            ->add(
                'defectOrientationHours',
                'integer',
                $this->options(
                    [
                        'label' => false,
                        'mapped' => false,
                        'required' => false,
                        'data' => ($this->entity->getDefectOrientation()) ? intval(
                            $this->entity->getDefectOrientation()->format('h')
                        ) : null,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0, 'max' => 12])],
                        'attr' => [
                            'container_class' => '',
                            'form_class' => 'col-md-2',
                            'next_group_addon' => 'hr',
                            'class' => 'number'
                        ]
                    ]
                )
            )
            ->add(
                'defectOrientationMinutes',
                'integer',
                $this->options(
                    [
                        'label' => ':',
                        'required' => false,
                        'data' => ($this->entity->getDefectOrientation()) ? intval(
                            $this->entity->getDefectOrientation()->format('m')
                        ) : null,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0, 'max' => 59])],
                        'mapped' => false,
                        'attr' => ['form_class' => 'col-md-3', 'next_group_addon' => 'min', 'class' => 'number']
                    ]
                )
            );

        $builder
            ->add($defectOrientation)
            ->add(
                'corrosionRate',
                'number', //show only if threat was CR-Corrosion
                $this->options(
                    [
                        'label' => 'Corrosion Rate',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm/year', 'class' => 'number cr-ml'],
                        'constraints' => [new Type(['type' => 'double'])]
                    ]
                )
            )
            ->add(
                'remainingWallThickness',
                'number',
                $this->options(
                    [
                        'label' => 'Remaining Wall Thickness',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'retiralValue',
                'number',
                $this->options(
                    [
                        'label' => 'Retiral Value',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'length',
                'number',
                $this->options(
                    [
                        'label' => 'Length',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            )
            ->add(
                'width',
                'number',
                $this->options(
                    [
                        'label' => 'Width',
                        'required' => false,
                        'attr' => ['next_group_addon' => 'mm', 'class' => 'number cr-ml']
                    ]
                )
            );

        $builder
            ->add(
                'trackingCriteria',
                CollectionType::class,
                array(
                    'entry_type' => TrackingCriteriaType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'label' => false,
                    'attr' => ['class' => 'not-cr-ml']
                )
            )
            ->add(
                'add_tracking_criteria',
                'button',
                $this->options(
                    [
                        'label' => '+ Add Tracking Criteria',
                        'attr' => ['class' => 'btn btn-link not-cr-ml', 'container_class' => 'button-container col-md-offset-3'],

                    ]
                    ,
                    'button'
                )
            );

        $builder
            ->add(
                'comments',
                'textarea',
                $this->options(['label' => 'Comments', 'required' => false])
            )
            ->add(
                'summary',
                'textarea',
                $this->options(['label' => 'Anomaly Summary Details', 'required' => false])
            )
            ->add('failureSummary', 'textarea',
                $this->options(['label' => 'Failure Summary Details', 'required' => false]))
            ->add(
                'recommendations',
                'textarea',
                $this->options(['label' => 'Recommendations', 'required' => false])
            );

    }

    protected function buildAssessment(FormBuilderInterface &$builder)
    {
        if ($this->entity->getProject() instanceof RiskProject) {
            $builder
                ->add(
                    'probability',
                    'choice',
                    $this->options(
                        [
                            'label' => 'Probability',
                            'choices' => AnomalyRegistrar::getRiskChoices(),
                            'required' => true,
                            'attr' => ['class' => 'required'],
                            'constraints' => [new NotNull()],
                        ]
                    )
                )//from 1 - 5
                ->add('criticality', 'hidden');
            $builder->add(
                'consequence','choice',
                $this->options(
                    [
                        'label' => 'Consequence',
                        'choices' => AnomalyRegistrar::getRiskChoices(),
                        'required' => true,
                        'attr' => ['class' => 'required'],
                        'constraints' => [new NotNull()],
                    ]
                )
            )//from 1 - 5
            ;
        } else {
            if($this->entity->getProject()->hasFMCriticality()){
                $builder
                    ->add('isFabricMaintenanceCriticality', 'choice',
                        $this->options(['label'       => 'Critcality Type',
                            'choices'=>[0=>'Anomaly Criticality',1=>'Fabric Maintenance Criticality'],
                            'required'    => true,
                            'attr'        => ['class' => 'required isFmCriticality']
                        ]));
            }
            $builder
                ->add(
                    'criticality',
                    'choice',
                    $this->options(
                        [
                            'label' => 'Criticality',
                            'choices' => $this->entity->getProject()->getEnabledCriticalityChoices(),
                            'required' => true,
                            'attr' => ['class' => 'required'],
                            'constraints' => [new NotNull()],
                        ]
                    )
                );
            if($this->entity->getProject()->hasFMCriticality()){
                $builder
                    ->add('fabricMaintenanceCriticality', 'choice',
                        $this->options(['label'       => 'Criticality',
                            'choices'     => $this->entity->getProject()->getFMCriticalityArray(),
                            'required'    => true,
                            'attr'        => ['class' => 'required'],
                            'constraints' => [new NotNull()],
                        ]));
            }
            if($this->entity->getProject()->hasProductionCriticality()){
                $builder->add(
                    'productionCriticality',
                    'choice',
                    $this->options(
                        [
                            'label' => 'Production Criticality',
                            'choices' => $this->entity->getProject()->getProductionCriticalityArray(),
                            'required' => true,
                            'attr' => ['class' => 'required'],
                            'constraints' => [new NotNull()],
                        ]
                    )
                );
            }

                $builder->add('probability', 'hidden')
                ->add('consequence', 'hidden');
        }
    }

    protected function buildRemedial(FormBuilderInterface &$builder)
    {
        $actionsHelp = [
            'Inspection' => 'For reporting & management of anomalies requiring future routine inspection (i.e. UT Wall thickness monitoring etc.)',
            'Repair Order' => 'For reporting and management of repairs (i.e. Pipework Replacement etc.)',
            'Temporary' => 'For reporting & management of temporary repairs (i.e. Composite Repairs, Box Clamps etc.)',
            'Fabric' => 'For reporting & management of fabric maintenance (Prevenatative Maintenance i.e. Painting, Insulation Repairs etc.)',
            'Assessment' => 'For further assessment and analysis to decide the status of the Anomaly (including non-routine inspection)'
            ];

        $actionChoices = $this->entity->getActionsChoices();

        $actionConfirmationArr = array(0=>'AN', 1=>'RO', 2=>'TRO', 3=>'FM', 4=>'AS');
        $arrayIntersect = [];
        $actionConfirmation = $this->entity->getActionRequired();

        if (null !== $this->entity->getActionRequired() && array_key_exists(2,array_flip($actionConfirmation))) {
            $arrayIntersect = array(0=>'Temporary Repair is for an External Defect',1=>'Temporary Repair is "Engineered" (on the contrary of "Bandag")',2=>'Temporary Repair design life is > 10 years',3=>'Temporary Repair has a detailed Inspection plan');
        }
        $builder->add(
            'remediationRequireShutdown',
            'choice',
            $this->options(
                [
                    'label' => 'Shutdown Required',
                    'choices' => [0=>'No',1=>'Yes'],
                    'required' => true,
                    'attr' => ['class' => 'required'],
                    'constraints' => [new NotNull()],
                ]
            )
        );
        $builder->add(
            'actionRequired',
            'choice',
            $this->options(
                [
                    'required' => true,
                    'label' => '*Action Required',
                    'choices' => $actionChoices,
                    'multiple' => true,
                    'expanded' => true,
                    'mapped' => true,
                    'attr' => ['class' => 'col-md-9'],
                    'constraints' => [
                        new NotNull(),
                        new Count(['min' => 1, 'minMessage' => "At least one action must be selected"])
                    ],
                    'choice_attr' => function ($val, $key, $index) use ($actionsHelp, $actionChoices) {
                        $helpMessage = '';
                        foreach ($actionsHelp as $k => $v) {
                            $type = $actionChoices[$key];
                            if (strpos($type, $k) !== false) {
                                $helpMessage = $v;
                                break;
                            }
                        }

                        return ['help' => $helpMessage];
                    },
                ],
                'choice_expandable'
            )
        )
            ->add(
                'action_confirmation',
                'choice',
                $this->options(
                    [
                        'mapped' => false,
                        'required' => false,
                        'choices' => [
                            'Temporary Repair is for an External Defect' => 'Temporary Repair is for an External Defect',
                            'Temporary Repair is "Engineered" (on the contrary of "Bandag")' => 'Temporary Repair is "Engineered" (on the contrary of "Bandag")',
                            'Temporary Repair design life is > 10 years' => 'Temporary Repair design life is > 10 years',
                            'Temporary Repair has a detailed Inspection plan' => 'Temporary Repair has a detailed Inspection plan',
                        ],
                        'multiple' => true,
                        'expanded' => true,
                        'data' => $arrayIntersect,
                        'constraints' => new Callback(
                            [
                                'callback' => function ($object, $context) {
                                    $form = $context->getRoot();
                                    $data = $form->getData();
                                    // If $data is null then the field was blank, do nothing more
                                    if (is_null($data)) {
                                        return;
                                    }

                                    $requiredActions = $form->get('remedial_group')->get('actionRequired')->getData();
                                    $actionConfirmation = $object;

                                    //if TRO without RO, make sure all the options are selected
                                    if (in_array(2, $requiredActions) && !in_array(1, $requiredActions) //TRO but not RO
                                        && count($actionConfirmation) == 0
                                    ) //didn't check any of them
                                    {
                                        $form->addError(new FormError('You should select RO as well!'));
                                    }
                                }
                            ]
                        )
                    ],
                    'choice_expandable'
                )
            );

    }

    protected function buildManagement(FormBuilderInterface &$builder)
    {
        $builder
            ->add(
                'inspectionTechnique',
                'text',
                $this->options(
                    [
                        'label' => 'Latest Inspection Technique',
                        'attr' => ['class' => 'required'],
                        'required' => true,
                        'constraints' => new Callback(
                            [
                                'callback' => function ($object, $context) {
                                    $form = $context->getRoot();
                                    $requiredActions = $form->get('remedial_group')->get('actionRequired')->getData();

                                    if (in_array(0, $requiredActions) //if inspection
                                    ) {
                                        if (!$object || empty($object)) {
                                            $form->addError(new FormError('Inspection technique is required!'));
                                        }

                                    }
                                }
                            ]
                        ),
                    ]
                )
            )
            ->add(
                $builder->create('inspectionDate',
                new DatePickerType(),
                $this->options(
                    [
                        'label' => 'Latest Inspection Date',
                        'required' => true,
                        'constraints' => new Callback(
                            [
                                'callback' => function ($object, $context) {
                                    $form = $context->getRoot();
                                    $requiredActions = $form->get('remedial_group')->get('actionRequired')->getData();

                                    if (in_array(0, $requiredActions) //if inspection
                                    ) {
                                        if (!$object) {
                                            $form->addError(new FormError('Inspection date is required!'));
                                        }

                                    }
                                }
                            ]
                        ),
                        'attr' => ['class' => 'inspection-date required']
                    ],
                    'date_picker'
                )
            )->addModelTransformer(new DateTimePickerTransformer()))
            ->add(
                'inspectionFrequency',
                'number',
                $this->options(
                    [
                        'label' => 'Inspection Frequency',
                        'required' => true,
                        'constraints' => new Callback(
                            [
                                'callback' => function ($object, $context) {
                                    $form = $context->getRoot();
                                    $requiredActions = $form->get('remedial_group')->get('actionRequired')->getData();

                                    if (in_array(0, $requiredActions) //if inspection
                                    ) {
                                        if (!$object || empty($object) || !is_numeric($object)) {
                                            $form->addError(new FormError('Inspection frequency is required!'));
                                        }

                                    }
                                }
                            ]
                        ),
                        'attr' => ['class' => 'required number', 'next_group_addon' => 'months']
                    ]
                )
            )
            ->add(
                'inspectionResults',
                'textarea',
                $this->options(
                    [
                        'label' => 'Inspection Results',
                        'required' => false,
                        'attr' => ['class' => '']
                    ]
                )
            )
            ->add(
                'inspectionRecommendations',
                'textarea',
                $this->options(
                    [
                        'label' => 'Next Inspection Requirements',
                        'required' => false,
                        'attr' => ['class' => '']
                    ]
                )
            )
            ->add(
                'bufferDays',
                'integer',
                $this->options(
                    [
                        'label' => 'Buffer Days',
                        'required' => false,
                        'data' => ($this->entity->getBufferDays())?$this->entity->getBufferDays():0,
                        'constraints' => [new Type(['type' => 'integer']), new Range(['min' => 0])],
                        'attr' => ['class' => 'integer']
                    ]
                )
            )
            ->add(
                $builder->create('closeOutDate',
                new DatePickerType(),
                $this->options(
                    [
                        'label' => 'Close Out Date',
                        'attr' => ['class' => 'close_out'],
                        'constraints' => new Callback(                            [
                                'callback' => function ($value, $context) {
                                    $form = $context->getRoot();
                                    $data = $form->get('status');
                                    // If $data is null then the field was blank, do nothing more
                                    if (is_null($data)) {
                                        return;
                                    }

                                    $status = $data->getData();
                                    if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                        $form->addError(new FormError('You should fill close out date!'));
                                    }
                                }
                            ]
                        ),

                    ],
                    'date_picker'
                )
                )->addModelTransformer(new DateTimePickerTransformer()))
            ->add(
                'closeOutJustification',
                'textarea',
                $this->options(
                    [
                        'label' => 'Close Out Justification',
                        'attr' => ['class' => 'close_out'],
                        'constraints' => new Callback(                            [
                                'callback' => function ($value, $context) {
                                    $form = $context->getRoot();
                                    $data = $form->get('status');

                                    // If $data is null then the field was blank, do nothing more
                                    if (is_null($data)) {
                                        return;
                                    }

                                    $status = $data->getData();

                                    if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                        $form->addError(new FormError('You should fill close out justification!'));
                                    }

                                }
                            ]
                        ),
                    ]
                )
            );

    }



    protected function buildFiles(FormBuilderInterface &$builder){
        $builder->add(
            'files',
            CollectionType::class,
            array(
                'entry_type' => AnomalyFilesType::class,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label' => false,
                'data' => null
            ))
            ->add(
            'add_files',
            'button',
            $this->options(
                [
                    'label' => '+ Add Attachment',
                    'attr' => ['class' => 'btn btn-link', 'container_class' => 'button-container col-md-offset-3'],

                ]
                ,
                'button'
            )
        );
    }

    protected function buildRest(FormBuilderInterface &$builder)
    {

        //$this->spaUserId = (!empty($this->entity->getSpaUser()) ? $this->entity->getSpaUser() : $this->entity->getSpaId());

        $builder->add(
                'status',
                'choice',
                $this->options(
                    [
                        'required' => true,
                        'choices' => $this->entity->getStatusChoices(),
                        'attr'=>['class'=>'anomaly-status']
                    ]
                )
            )
            ->add(
                'spa',
                'entity',
                $this->options(
                    [
                        'class' => 'AIE\Bundle\AnomalyBundle\Entity\ActionOwners',
                        'property' => 'user.username',
                        'required' => true,
                        'label' => 'Action Owner',
                        'mapped' => true,
                        'choices' => $this->actionOwners,
                        'data' => $this->em->getReference("AIEAnomalyBundle:ActionOwners", $this->actionOwner)
                    ]
                )
            )
//			->add('registrarFiles', 'file', $this->options([
//				'label'    => '',
//				'data'     => '',
//				'multiple' => true,
//				'mapped'   => false,
//				'position' => ['before' => 'submit']
//			], 'file'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar',
                'inherit_data' => true,
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_anomalybundle_anomalyregistrar';
    }
}
