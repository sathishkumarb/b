<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\RiskProject;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SelectableFileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Form\FormError;


class AssessmentRegistrarType extends ActionRegistrarType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
    }

    protected function buildAction(FormBuilderInterface &$builder) {
        parent::buildAction($builder);
        $builder->add('assessmentType', 'text',
            $this->options([
                'label'       => 'Assessment Type',
                'attr'        => [],
                'required'    => false,
                'constraints' => [],]))
            ->add('assessmentCode', 'text',
                $this->options([
                    'label'       => 'Assessment Code',
                    'attr'        => [],
                    'required'    => false,
                    'constraints' => [],]))
            ->add('assessmentBufferDays', 'number',
                $this->options([
                    'label'       => 'Buffer Days',
                    'required'    => false,
                    'constraints' => [new Type(['type' => 'double'])],
                    'attr'        => ['class' => 'number', 'next_group_addon' => 'days']]))
            ->add(
                $builder->create('assessmentDueDate', new DatePickerType(),
                    $this->options(['label'       => 'Assessment Completion Date',
                        'required'    => false,
                        'constraints' => new Callback(                            [
                                'callback' => function ($value, $context) {
                                    $form = $context->getRoot();
                                    $data = $form->get('status');
                                    // If $data is null then the field was blank, do nothing more
                                    if (is_null($data)) {
                                        return;
                                    }

                                    $status = $data->getData();
                                    if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                        $form->addError(new FormError('Assessment Completion Date is mandatory'));
                                    }
                                }
                            ]
                        ),
                        'attr'        => ['class' => 'inspection-date']], 'date_picker')
                )->addModelTransformer(new DateTimePickerTransformer()))
            ->add('assessmentResult', 'text',
                $this->options([
                    'label'       => 'Assessment Result',
                    'attr'        => [],
                    'required'    => false,
                    'error_bubbling' => true,
                    'constraints' => new Callback(                            [
                            'callback' => function ($value, $context) {
                                $form = $context->getRoot();
                                $data = $form->get('status');
                                // If $data is null then the field was blank, do nothing more
                                if (is_null($data)) {
                                    return;
                                }

                                $status = $data->getData();
                                if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                    $form->addError(new FormError('Assessment Result is mandatory'));
                                }
                            }
                        ]
                    ),]));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults([
            'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\AssessmentRegistrar'
        ]);
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_anomalybundle_assessmentregistrar';
    }
}
