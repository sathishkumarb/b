<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\VeracityBundle\Form\DataTransformer\DateTimePickerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Form\FormError;

class FabricMaintenanceRegistrarType extends ActionRegistrarType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
	}

	protected function buildAction(FormBuilderInterface &$builder) {
		parent::buildAction($builder);
		$builder->add('description', 'text',
			$this->options([
				'label'       => 'Fabric Maintenance Description',
				'attr'        => ['class' => 'required'],
				'required'    => true,
				'constraints' => [new NotBlank()],]))
			->add('immediateRepair', 'choice', $this->options([
				'label'    => 'Immediate Repair',
				'choices'  => [0 => 'No', 1 => 'Yes'],
				'required' => true,
			]))
			->add('longIsolation', 'choice', $this->options([
				'label'    => 'Long Isolation',
				'choices'  => [0 => 'No', 1 => 'Yes'],
				'required' => true,
			]))
			->add('inService', 'number',
				$this->options([
					'label'       => 'In-Service',
					'required'    => false,
					'constraints' => [new Type(['type' => 'double'])],
					'attr'        => ['class' => 'number', 'next_group_addon' => 'months']]))
			->add($builder->create('completionDate', new DatePickerType(),
				$this->options(['label'       => 'Completion Date',
				                'required'    => false,
				                'error_bubbling' => true,
                                'constraints' => new Callback(                            [
                                        'callback' => function ($value, $context) {
                                            $form = $context->getRoot();
                                            $data = $form->get('status');
                                            // If $data is null then the field was blank, do nothing more
                                            if (is_null($data)) {
                                                return;
                                            }

                                            $status = $data->getData();
                                            if ($this->entity->getStatusChoices()[$status] == 'Closed' && empty($value) ) {
                                                $form->addError(new FormError('Completion Date is mandatory'));
                                            }
                                        }
                                    ]
                                ),
				                'attr'        => ['class' => 'inspection-date']], 'date_picker'))->addModelTransformer(new DateTimePickerTransformer()));

	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_fabricmaintenanceregistrar';
	}
}
