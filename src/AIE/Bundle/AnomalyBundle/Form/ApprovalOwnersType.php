<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class ApprovalOwnersType extends AbstractType {
	use FormStyleHelper;

	private $actionSpas;
	private $approvalLevels;
    private $isNew;
    private $username;

	public function __construct($actionSpas, $approvalLevels, $isNew=true, $username=null){
		$this->actionSpas = $actionSpas;
        $this->approvalLevels = $approvalLevels;
        $this->isNew = $isNew;
        $this->username=$username;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

       // $actionChoices = ApprovalOwners::getActionsChoices();
        if($this->isNew){
            $builder->add('actionOwner', 'choice', $this->options([
                'required' => true,
                'label'    => 'SPA',
                'mapped'   => true,
                'choices'  => $this->actionSpas
            ]));
        }else{
            $builder->add('actionOwnerName', 'text', $this->options([
                'required' => false,
                'label'    => 'SPA',
                'mapped'   => false,
                'data' => $this->username,
                'attr'        => ['disabled' => 'disabled']
            ]));
        }

        $builder
			->add('anomalyCategory', 'choice', $this->options([
				'choices'     => AnomalyRegistrar::getAnomalyCategories(),
				'required'    => true,
				'label'       => 'Category',
				'attr'        => ['class' => 'required'],
				'constraints' => [new NotNull()],
			]))
            ->add('approvalLevel', 'choice', $this->options([
                'label' => 'Approval Levels',
                'required'    => true,
                'choices' => $this->approvalLevels,
            ]));
            // ->add('actionCategory', 'choice', $this->options([
            //     'choices'     => $actionChoices,
            //     'required'    => true,
            //     'label'       => 'Sub-Category',
            //     'attr'        => ['class' => 'required'],
            //     'constraints' => [new NotNull()],
            // ]));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\ApprovalOwners'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_approvalowners';
	}
}
