<?php

namespace AIE\Bundle\AnomalyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class StaticEquipmentAnomalyType extends AnomalyRegistrarType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		parent::buildForm($builder, $options);
	}


	protected function buildEquipment(FormBuilderInterface &$builder) {
		parent::buildEquipment($builder);
		$builder
			->add('system', 'text',
				$this->options(['label'       => 'System',
				                'required'    => true,
				                'attr'        => ['class' => 'required'],
				                'position'    => ['after' => 'component'],
				                'constraints' => [new NotBlank()],]))
			->add('nominalWT', 'number',
				$this->options(['label'       => 'Nominal Wall Thickness (NWT)',
				                'attr'        => ['next_group_addon' => 'mm', 'class' => 'number'],
				                'required'    => false,
				                'position'    => ['before' => 'corrosionAllowance'],
				                'constraints' => [new Type(['type' => 'double'])]
				]))
			->add('sMYS', 'number',
				$this->options(['label'       => 'Specified Minimum Yield Strength (SMYS)',
				                'attr'        => ['next_group_addon' => 'ksi', 'class' => 'number'],
				                'position'    => ['after' => 'mAWT'],
				                'constraints' => [new Type(['type' => 'double'])]
				]))->add('sMTS', 'number',
				$this->options(['label'       => 'Specified Minimum Tensile Strength (SMTS)',
				                'attr'        => ['next_group_addon' => 'ksi', 'class' => 'number'],
				                'position'    => ['after' => 'sMYS'],
				                'constraints' => [new Type(['type' => 'double'])]]
				));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults([
			'data_class' => 'AIE\Bundle\AnomalyBundle\Entity\StaticEquipmentAnomaly'
		]);
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'aie_bundle_anomalybundle_staticequipmentanomaly';
	}
}
