<?php
namespace AIE\Bundle\AnomalyBundle\Command;

use AIE\Bundle\AnomalyBundle\Entity\RepairOrderRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\FabricMaintenanceRegistrar;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class UpdateActionRegisterCommand extends ContainerAwareCommand {

    //php app/console aie:update_actionregister
    protected function configure()
    {
        $this
            ->setName('aie:update_actionregister')
            ->setDescription('Update Action Items')
            ->addOption(
                'closeoutdate',
                null,
                InputOption::VALUE_NONE,
                'If close out date reaches the current day update the status of action item registers to closed'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $container->enterScope('request');
        $container->set('request', new Request(), 'request');

        $em = $container->get('doctrine')->getEntityManager('anomaly');
        $currentNow = new \DateTime('now');

        $currentNow = $currentNow->format('Y-m-d');
        if ($input->getOption('closeoutdate'))
        {
            $currentNow = new \DateTime($input->getOption('closeoutdate'));

            $updateCloseoutActions = $em->getRepository('AIEAnomalyBundle:RepairOrderRegistrar')->findByCloseOutDate($currentNow);
            $updateCloseoutActions = $em->getRepository('AIEAnomalyBundle:RepairOrderRegistrar')->findByCompletionDate($currentNow);

        } 
        else
        {
            $updateCloseoutActions = $em->getRepository('AIEAnomalyBundle:RepairOrderRegistrar')->findByCloseOutDate($currentNow);
            $updateCloseoutActions = $em->getRepository('AIEAnomalyBundle:FabricMaintenanceRegistrar')->findByCompletionDate($currentNow);
        }

        $output->writeln('RO / FM action item updated');

    }
} 