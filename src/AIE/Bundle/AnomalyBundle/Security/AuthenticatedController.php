<?php
namespace AIE\Bundle\AnomalyBundle\Security;

use AIE\Bundle\AnomalyBundle\Security\SecureObjectInterface;

interface AuthenticatedController {

    public function getSecureObject();

    /*
     * [
     * 'ROUTE' => 'ROLE_NAME'
     * ]
     */
    public function getRoutesRoles();
} 