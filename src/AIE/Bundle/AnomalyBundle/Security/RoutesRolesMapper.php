<?php
/**
 * User: Sathish
 * Date: 04/10/2016
 */

namespace AIE\Bundle\AnomalyBundle\Security;


trait RoutesRolesMapper {

    protected $_routesRoles = [
        'admin_dashboard'                           => 'ROLE_ADMIN',
        /* Project */
        'pipeline_index'                            => 'ROLE_VERACITY_PIPELINE',
        'anomaly_index'                             => 'ROLE_VERACITY_ANOMALY',
        'ccm_index'                                 => 'ROLE_VERACITY_CCM',

        /* Project */
        'anomaly_projects_create'                           => 'ROLE_PROJECT_LIST',
        'anomaly_projects_create'                           => 'ROLE_PROJECT_ADD',
        'anomaly_projects_new'                              => 'ROLE_PROJECT_ADD',
        'anomaly_projects_show'                             => 'ROLE_PROJECT_SHOW',
        'anomaly_projects_edit'                             => 'ROLE_PROJECT_EDIT',
        'anomaly_projects_update'                           => 'ROLE_PROJECT_EDIT',
        'anomaly_projects_delete'                           => 'ROLE_PROJECT_DELETE',
        'anomaly_projects_post_delete'                      => 'ROLE_PROJECT_DELETE',

        /* Pipeline */
        'anomaly_pipelines'                                 => 'ROLE_PIPELINE_LIST',
        'anomaly_pipelines_create'                          => 'ROLE_PIPELINE_ADD',
        'anomaly_pipelines_new'                             => 'ROLE_PIPELINE_ADD',
        'anomaly_pipelines_show'                            => 'ROLE_PIPELINE_SHOW',
        'anomaly_pipelines_edit'                            => 'ROLE_PIPELINE_EDIT',
        'anomaly_pipelines_update'                          => 'ROLE_PIPELINE_EDIT',
        'anomaly_pipelines_delete'                          => 'ROLE_PIPELINE_DELETE',
        'anomaly_pipelines_post_delete'                     => 'ROLE_PIPELINE_DELETE',
        
        /* Pipeline Sections */
        'anomaly_pipelinesections'                          => 'ROLE_PIPELINE_SECTIONS_LIST',
        'anomaly_pipelinesections_create'                   => 'ROLE_PIPELINE_SECTIONS_ADD',
        'anomaly_pipelinesections_new'                      => 'ROLE_PIPELINE_SECTIONS_ADD',
        'anomaly_pipelinesections_edit'                     => 'ROLE_PIPELINE_SECTIONS_EDIT',
        'anomaly_pipelinesections_update'                   => 'ROLE_PIPELINE_SECTIONS_EDIT',
        'anomaly_pipelinesections_delete'                   => 'ROLE_PIPELINE_SECTIONS_DELETE',
        'anomaly_pipelinesections_post_delete'              => 'ROLE_PIPELINE_SECTIONS_DELETE',

         /* Location */
        'anomaly_locations_show'                    => 'ROLE_LOCATION_SHOW',
        'anomaly_locations_create'                  => 'ROLE_LOCATION_ADD',
        'anomaly_locations_new'                     => 'ROLE_LOCATION_ADD',
        'anomaly_locations_edit'                    => 'ROLE_LOCATION_EDIT',
        'anomaly_locations_update'                  => 'ROLE_LOCATION_EDIT',
        'anomaly_locations_delete'                  => 'ROLE_LOCATION_DELETE',

        /* Risk Matrix */
        'anomaly_riskmatrix_create'                 => 'ROLE_RISKMATRIX_EDIT',
        'anomaly_riskmatrix_new'                    => 'ROLE_RISKMATRIX_SHOW',
        'anomaly_projectsconsequences'              => 'ROLE_RISKMATRIX_CONSEQUENCE',

         /* Criticality */
        'anomaly_criticality_create'                 => 'ROLE_CRITICALITY_EDIT',
        'anomaly_criticality_preselect'                    => 'ROLE_CRITICALITY_SHOW',

        /* Threats */
        'anomaly_a_threats'                                   => 'ROLE_THREATS',
        'anomaly_a_threats_create'                            => 'ROLE_THREATS_ADD',
        'anomaly_a_threats_new'                               => 'ROLE_THREATS_ADD',
        'anomaly_a_threats_show'                              => 'ROLE_THREATS_SHOW',
        'anomaly_a_threats_update'                            => 'ROLE_THREATS_EDIT',
        'anomaly_a_threats_edit'                              => 'ROLE_THREATS_EDIT',
        'anomaly_a_threats_delete'                            => 'ROLE_THREATS_DELETE',

        /* Class */
        'anomaly_classes'                                   => 'ROLE_CLASS_SHOW',
        'anomaly_classes_create'                            => 'ROLE_CLASS_ADD',
        'anomaly_classes_new'                               => 'ROLE_CLASS_ADD',
        'anomaly_classes_edit'                              => 'ROLE_CLASS_EDIT',
        'anomaly_classes_update'                            => 'ROLE_CLASS_EDIT',
        'anomaly_classes_update'                            => 'ROLE_CLASS_DELETE',

        /* Codes */
        'anomaly_codes'                                   => 'ROLE_CODES_SHOW',
        'anomaly_codes_create'                            => 'ROLE_CODES_ADD',
        'anomaly_codes_new'                               => 'ROLE_CODES_ADD',
        'anomaly_codes_edit'                              => 'ROLE_CODES_EDIT',
        'anomaly_codes_update'                            => 'ROLE_CODES_EDIT',
        'anomaly_codes_udelete'                           => 'ROLE_CODES_DELETE',

        /* Action Owners */
        'anomaly_ao'                                      => 'ROLE_ACTION_OWNERS_SHOW',
        'anomaly_ao_create'                               => 'ROLE_ACTION_OWNERS_ADD',
        'anomaly_ao_new'                                  => 'ROLE_ACTION_OWNERS_ADD',
        'anomaly_ao_delete'                               => 'ROLE_ACTION_OWNERS',

        /* Deferral Owners */
        'anomaly_deferrals'                            => 'ROLE_DEFERRAL_OWNERS',
        'anomaly_deferrals_create'                     => 'ROLE_DEFERRAL_OWNERS',
        'anomaly_deferrals_new'                        => 'ROLE_DEFERRAL_OWNERS',
        'anomaly_deferrals_delete'                     => 'ROLE_DEFERRAL_OWNERS',

        /* Pipeline Document */
        'anomaly_files'                             => 'ROLE_DOCUMENT',
        'anomaly_files_create'                      => 'ROLE_DOCUMENT',
        'anomaly_files_new'                         => 'ROLE_DOCUMENT',
        'anomaly_files_show'                        => 'ROLE_DOCUMENT',
        'anomaly_files_edit'                        => 'ROLE_DOCUMENT',
        'anomaly_files_update'                      => 'ROLE_DOCUMENT',
        'anomaly_files_delete'                      => 'ROLE_DOCUMENT',
        'anomaly_files_categories'                  => 'ROLE_DOCUMENT',
        'anomaly_files_categories_create'           => 'ROLE_DOCUMENT',
        'anomaly_files_categories_update'           => 'ROLE_DOCUMENT',
        'anomaly_files_categories_delete'           => 'ROLE_DOCUMENT',
        'anomaly_files_categories_move'             => 'ROLE_DOCUMENT',
        'anomaly_files_move'                        => 'ROLE_DOCUMENT',
        'ajax_pipeline_files'                       => 'ROLE_DOCUMENT',

         /* Plannig Anomaly */
        'anomaly_planning_master'               => 'ROLE_PLANNING',
        'anomaly_planning'                      => 'ROLE_PLANNING',

        'anomaly_reminders_master'              => 'ROLE_REMINDERS',
        'anomaly_reminders'                     => 'ROLE_REMINDERS',

        /* Pipeline Anomaly */
        'anomaly_pl'                            => 'ROLE_PIPELINE_ANOMALY_LIST',
        'anomaly_pl_create'                     => 'ROLE_PIPELINE_ANOMALY_ADD',
        'anomaly_pl_new'                        => 'ROLE_PIPELINE_ANOMALY_ADD',
        'anomaly_pl_vs'                         => 'ROLE_PIPELINE_ANOMALY_SHOW',
        'anomaly_pl_show'                       => 'ROLE_PIPELINE_ANOMALY_SHOW',
        'anomaly_pl_edit'                       => 'ROLE_PIPELINE_ANOMALY_EDIT',
        'anomaly_pl_update'                     => 'ROLE_PIPELINE_ANOMALY_EDIT',
        'anomaly_pl_delete'                     => 'ROLE_PIPELINE_ANOMALY_DELETE',
        'anomaly_pl_clone'                      => 'ROLE_PIPELINE_ANOMALY_CLONE',
        'anomaly_pl_locations'                  => 'ROLE_PIPELINE_ANOMALY_LOCATION',
        'anomaly_pl_duplicate'                  => 'ROLE_PIPELINE_ANOMALY_DUPLICATE',

        /* Piping Anomaly */
        'anomaly_pi'                            => 'ROLE_PIPING_ANOMALY_LIST',
        'anomaly_pi_create'                     => 'ROLE_PIPING_ANOMALY_ADD',
        'anomaly_pi_new'                        => 'ROLE_PIPING_ANOMALY_ADD',
        'anomaly_pi_vs'                         => 'ROLE_PIPING_ANOMALY_SHOW',
        'anomaly_pi_show'                       => 'ROLE_PIPING_ANOMALY_SHOW',
        'anomaly_pi_edit'                       => 'ROLE_PIPING_ANOMALY_EDIT',
        'anomaly_pi_update'                     => 'ROLE_PIPING_ANOMALY_EDIT',
        'anomaly_pi_delete'                     => 'ROLE_PIPING_ANOMALY_DELETE',
        'anomaly_pi_clone'                      => 'ROLE_PIPING_ANOMALY_CLONE',
        'anomaly_pi_duplicate'                  => 'ROLE_PIPING_ANOMALY_DUPLICATE',

        /* Static Equipment Anomaly */
        'anomaly_se'                            => 'ROLE_STATICEQUIPMENT_ANOMALY_LIST',
        'anomaly_se_create'                     => 'ROLE_STATICEQUIPMENT_ANOMALY_NEW',
        'anomaly_se_new'                        => 'ROLE_STATICEQUIPMENT_ANOMALY_NEW',
        'anomaly_se_vs'                         => 'ROLE_STATICEQUIPMENT_ANOMALY_SHOW',
        'anomaly_se_show'                       => 'ROLE_STATICEQUIPMENT_ANOMALY_SHOW',
        'anomaly_se_edit'                       => 'ROLE_STATICEQUIPMENT_ANOMALY_EDIT',
        'anomaly_se_update'                     => 'ROLE_STATICEQUIPMENT_ANOMALY_EDIT',
        'anomaly_se_delete'                     => 'ROLE_STATICEQUIPMENT_ANOMALY_DELETE',
        'anomaly_se_clone'                      => 'ROLE_STATICEQUIPMENT_ANOMALY_CLONE',
        'anomaly_se_duplicate'                  => 'ROLE_STATICEQUIPMENT_ANOMALY_DUPLICATE',

        /* Structure Anomaly */
        'anomaly_st'                            => 'ROLE_STRUCTURE_ANOMALY_SHOW',
        'anomaly_st_create'                     => 'ROLE_STRUCTURE_ANOMALY_NEW',
        'anomaly_st_new'                        => 'ROLE_STRUCTURE_ANOMALY_NEW',
        'anomaly_st_vs'                         => 'ROLE_STRUCTURE_ANOMALY_SHOW',
        'anomaly_st_show'                       => 'ROLE_STRUCTURE_ANOMALY_SHOW',
        'anomaly_st_edit'                       => 'ROLE_STRUCTURE_ANOMALY_EDIT',
        'anomaly_st_update'                     => 'ROLE_STRUCTURE_ANOMALY_EDIT',
        'anomaly_st_delete'                     => 'ROLE_STRUCTURE_ANOMALY_DELETE',
        'anomaly_st_clone'                      => 'ROLE_STRUCTURE_ANOMALY_CLONE',
        'anomaly_st_duplicate'                  => 'ROLE_STRUCTURE_ANOMALY_DUPLICATE',

        /* Marine Anomaly */
        'anomaly_mr'                            => 'ROLE_MARINE_ANOMALY_LIST',
        'anomaly_mr_create'                     => 'ROLE_MARINE_ANOMALY_NEW',
        'anomaly_mr_new'                        => 'ROLE_MARINE_ANOMALY_NEW',
        'anomaly_mr_vs'                         => 'ROLE_MARINE_ANOMALY_SHOW',
        'anomaly_mr_show'                       => 'ROLE_MARINE_ANOMALY_SHOW',
        'anomaly_mr_edit'                       => 'ROLE_MARINE_ANOMALY_EDIT',
        'anomaly_mr_update'                     => 'ROLE_MARINE_ANOMALY_EDIT',
        'anomaly_mr_delete'                     => 'ROLE_MARINE_ANOMALY_DELETE',
        'anomaly_mr_clone'                      => 'ROLE_MARINE_ANOMALY_CLONE',
        'anomaly_mr_duplicate'                  => 'ROLE_MARINE_ANOMALY_DUPLICATE',

        /* Anomaly Deferrals */
        'anomaly_deferral_anomaly'              => 'ROLE_ANOMALY_DEFERRAL',
        'anomaly_deferral_action'               => 'ROLE_ANOMALY_DEFERRAL',
        'anomaly_deferral_request'              => 'ROLE_ANOMALY_DEFERRAL',

        'anomaly_master'                        => 'ROLE_ANOMALY_MASTER',
        'anomaly_search_master'                 => 'ROLE_ANOMALY_SEARCH_MASTER',

        /* Repair Orders */
        'anomaly_ro'                            => 'ROLE_REPAIR_ORDER_LIST',
        'anomaly_ro_master'                     => 'ROLE_REPAIR_ORDER_MASTER_SHOW',
        'anomaly_ro_show'                       => 'ROLE_REPAIR_ORDER_SHOW',
        'anomaly_ro_update'                     => 'ROLE_REPAIR_ORDER_EDIT',
        'anomaly_ro_edit'                       => 'ROLE_REPAIR_ORDER_EDIT',

        /* Temporary Repair Orders */
        'anomaly_tro'                            => 'ROLE_TEMPORARY_REPAIR_ORDER_LIST',
        'anomaly_tro_master'                     => 'ROLE_TEMPORARY_REPAIR_ORDER_MASTER_SHOW',
        'anomaly_tro_show'                       => 'ROLE_TEMPORARY_REPAIR_ORDER_SHOW',
        'anomaly_tro_update'                     => 'ROLE_TEMPORARY_REPAIR_ORDER_EDIT',
        'anomaly_tro_edit'                       => 'ROLE_TEMPORARY_REPAIR_ORDER_EDIT',

        /* Temporary Repair Orders */
        'anomaly_fm'                            => 'ROLE_FABRIC_MAINTENANCE_ORDER_LIST',
        'anomaly_fm_master'                     => 'ROLE_FABRIC_MAINTENANCE_ORDER_MASTER_SHOW',
        'anomaly_fm_show'                       => 'ROLE_FABRIC_MAINTENANCE_ORDER_SHOW',
        'anomaly_fm_update'                     => 'ROLE_FABRIC_MAINTENANCE_ORDER_EDIT',
        'anomaly_fm_edit'                       => 'ROLE_FABRIC_MAINTENANCE_ORDER_EDIT',

        /* Monitors */
        'anomaly_monitor'                          => 'ROLE_MONITOR',
        'anomaly_monitor_edit'                     => 'ROLE_MONITOR',
        'anomaly_monitor_update'                   => 'ROLE_MONITOR',

        /* Requests */
        'anomaly_requests'                        => 'ROLE_REQUEST',
        'anomaly_requests_show'                   => 'ROLE_REQUEST',
        'anomaly_requests_edit'                   => 'ROLE_REQUEST',
        'anomaly_requests_update'                 => 'ROLE_REQUEST',
        'anomaly_requests_history'                => 'ROLE_REQUEST',
      
        /* Positions */
        'anomaly_userprojectgroup'                          => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_create'                   => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_new'                      => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_show'                     => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_edit'                     => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_update'                   => 'ROLE_POSITIONS',
        'anomaly_userprojectgroup_delete'                   => 'ROLE_POSITIONS',

        /* Users */
        'anomaly_users'                             => 'ROLE_USERS',
        'anomaly_user_doadmin'                      => 'ROLE_USERS',
    ];


    public function detectSecureObject($request, $em)
    {

        //routes that will contain object
        $projectRoutes = [
            /* Project */
            'anomaly_projects_show',
            'anomaly_projects_edit',
            'anomaly_projects_update',
            'anomaly_projects_delete',

            /* Pipeline */
            'anomaly_pipelines',
            'anomaly_pipelines_create',
            'anomaly_pipelines_new',

            /* Project locations */
            'anomaly_locations_show' ,
            'anomaly_locations_create',
            'anomaly_locations_new',
            'anomaly_locations_edit',
            'anomaly_locations_update',
            'anomaly_locations_delete',

            /* Project Risk Matrix */
            'anomaly_riskmatrix_create',
            'anomaly_riskmatrix_new',
            'anomaly_projectsconsequences',

            /* Project criticality */
            'anomaly_criticality_create',
            'anomaly_criticality_preselect',
            'anomaly_criticality_new',

            /* Threats */
            'anomaly_a_threats',
            'anomaly_a_threats_create',
            'anomaly_a_threats_new',
            'anomaly_a_threats_show',
            'anomaly_a_threats_update',
            'anomaly_a_threats_edit',
            'anomaly_a_threats_delete',

            /* Class */
            'anomaly_classes',
            'anomaly_classes_create',
            'anomaly_classes_new',
            'anomaly_classes_edit',
            'anomaly_classes_update',
            'anomaly_classes_update',

            /* Codes */
            'anomaly_codes',
            'anomaly_codes_create',
            'anomaly_codes_new',
            'anomaly_codes_edit',
            'anomaly_codes_update',
            'anomaly_codes_udelete',

            /* Action Owners */
            'anomaly_ao',
            'anomaly_ao_create',
            'anomaly_ao_new',
            'anomaly_ao_delete',

            /* Deferral Owners */
            'anomaly_deferrals',
            'anomaly_deferrals_create',
            'anomaly_deferrals_new',
            'anomaly_deferrals_delete',

            /* Plannig Anomaly */
            'anomaly_planning_master',
            'anomaly_planning',
            'anomaly_reminders_master',
            'anomaly_reminders',

             /* Anomaly Master */
            'anomaly_master',

            /* Pipeline Anomaly */
            'anomaly_pl',
            'anomaly_pl_create',
            'anomaly_pl_new',
            'anomaly_pl_vs',
            'anomaly_pl_show',
            'anomaly_pl_edit',
            'anomaly_pl_update',
            'anomaly_pl_delete',
            'anomaly_pl_clone',
            'anomaly_pl_duplicate',
            'anomaly_pl_location',

            /* Piping Anomaly */
            'anomaly_pi',
            'anomaly_pi_create',
            'anomaly_pi_new',
            'anomaly_pi_vs',
            'anomaly_pi_show',
            'anomaly_pi_edit',
            'anomaly_pi_update',
            'anomaly_pi_delete',
            'anomaly_pi_clone',
            'anomaly_pi_duplicate',

             /* Static Equipment Anomaly */
            'anomaly_se',
            'anomaly_se_create',
            'anomaly_se_new',
            'anomaly_se_vs',
            'anomaly_se_show',
            'anomaly_se_edit',
            'anomaly_se_update',
            'anomaly_se_delete',
            'anomaly_se_clone',
            'anomaly_se_duplicate',

             /* Structure Anomaly */
            'anomaly_st',
            'anomaly_st_create',
            'anomaly_st_new',
            'anomaly_st_vs',
            'anomaly_st_show',
            'anomaly_st_edit',
            'anomaly_st_update',
            'anomaly_st_delete',
            'anomaly_st_clone',
            'anomaly_st_duplicate',

            /* Marine Anomaly */
            'anomaly_mr',
            'anomaly_mr_create',
            'anomaly_mr_new',
            'anomaly_mr_vs',
            'anomaly_mr_show',
            'anomaly_mr_edit',
            'anomaly_mr_update',
            'anomaly_mr_delete',
            'anomaly_mr_clone',
            'anomaly_mr_duplicate',

            /* Anomaly Deferrals */
            'anomaly_deferral_anomaly',
            'anomaly_deferral_action',
            'anomaly_deferral_request',

            /* Repair Orders */
            'anomaly_ro',
            'anomaly_ro_master',
            'anomaly_ro_show',
            'anomaly_ro_update',
            'anomaly_ro_edit',

            /* Temporary Repair Orders */
            'anomaly_tro',
            'anomaly_tro_master',
            'anomaly_tro_show',
            'anomaly_tro_update',
            'anomaly_tro_edit',

            /* Temporary Repair Orders */
            'anomaly_fm',
            'anomaly_fm_master',
            'anomaly_fm_show',
            'anomaly_fm_update',
            'anomaly_fm_edit',

        ];

        $projectPipelineRoutes = [
            /* Pipeline */
            'anomaly_pipelines_show',
            'anomaly_pipelines_edit',
            'anomaly_pipelines_update',
            'anomaly_pipelines_delete',

            /* Pipeline Files */
            'anomaly_files',
            'anomaly_files_create',
            'anomaly_files_new',
            'anomaly_files_show',
            'anomaly_files_edit',
            'anomaly_files_update',
            'anomaly_files_delete',
            'anomaly_files_categories',
            'anomaly_files_categories_create',
            'anomaly_files_categories_update',
            'anomaly_files_categories_delete',
            'anomaly_files_categories_move',
            'anomaly_files_move',

            /* Pipeline Sections */
            'anomaly_pipeline_sections',
            'anomaly_pipeline_sections_create',
            'anomaly_pipeline_sections_new',
            'anomaly_pipeline_sections_show',
            'anomaly_pipeline_sections_edit',
            'anomaly_pipeline_sections_update',
            'anomaly_pipeline_sections_delete',
            'anomaly_pipeline_sections_post_delete',

        ];

        $projectId = null;
        //choose first parameter if project


        //if pipeline, get pipeline then get project
        $route = $request->get('_route');
        if (in_array($route, $projectRoutes))
        {
            $projectId = $request->get('projectId', $request->get('id'));
        } else if (in_array($route, $projectPipelineRoutes))
        {
            $pipelineId = $request->get('pipelineId', $request->get('id'));
            $pipeline = $em->getRepository('AIEAnomalyBundle:Pipelines')->find($pipelineId);
            $projectId = $pipeline->getProject()->getId();
        } else
        {
            return null;
        }

        //return project else null
        return $em->getRepository('AIEAnomalyBundle:Projects')->find($projectId);;

    }
} 