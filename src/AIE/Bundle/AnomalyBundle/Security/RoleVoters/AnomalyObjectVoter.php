<?php

namespace AIE\Bundle\AnomalyBundle\Security\RoleVoters;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\ArrayHelper;
use Symfony\Component\Security\Core\User\UserInterface;
use AIE\Bundle\AnomalyBundle\Security\SecureObjectInterface;

#http://kriswallsmith.net/post/15994931191/symfony2-security-voters
#https://github.com/KnpLabs/KnpRadBundle/blob/develop/Security/Voter/IsOwnerVoter.php#L35-L45
#http://slid.es/marieminasyan/drop-ace-use-role-voters

class AnomalyObjectVoter extends RoleVoter {

    private $supportedRoles = array();
    private $entityManager;
    private $userRoles = array();

    public function __construct($supportedRoles, $em)
    {

        //$this->supportedRoles = ArrayHelper::array_flatten($supportedRoles);

        $this->supportedRoles = $supportedRoles;
        $this->entityManager = $em;
    }

    public function supportsAttribute($attribute)
    {

        return ArrayHelper::in_array_r($attribute, $this->supportedRoles);
    }

    public function supportsClass($class)
    {
        if ($class === null)
        {
            return true;
        }

        if (is_object($class))
        {
            $refl = new \ReflectionObject($class);

            return $refl->implementsInterface('AIE\Bundle\AnomalyBundle\Security\SecureObjectInterface');
        }

        return false;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {

        $result = VoterInterface::ACCESS_ABSTAIN;

        $user = $token->getUser();

        //for each requested permission/role/attribute
        foreach ($attributes as $attribute)
        {

            if (! $this->supportsAttribute($attribute))
            {
                continue;
            }

            if (! $this->supportsClass($object))
            {
                return self::ACCESS_ABSTAIN;
            }

            $result = VoterInterface::ACCESS_DENIED;

            if ($this->isUserGranted($user, $object, $attribute))
            {

                return VoterInterface::ACCESS_GRANTED;
            }
            /*
             * if ($this->isOwner($token->getUser(), $object)) {
              return self::ACCESS_GRANTED;
              }
             * OR
             * 
              if ($object->getAuthor() === $token->getUser()
              || in_array('ROLE_ADMIN', $token->getRoles())) {
              return VoterInterface::ACCESS_GRANTED;
              } */
        }


        return $result;
    }

    private function initUserRolesOn(UserInterface $user, $object)
    {

        $userProjGroups = [];
        if ($object !== null)
        {
            $userProjGroups = $this->entityManager->getRepository('AIEAnomalyBundle:UserProjectGroup')->findBy(array('user' => $user->getId(), 'project' => $object->getId()));
        }

        $roles = array();

        foreach ($userProjGroups as $upg)
        {
            $roles = array_merge($roles, $this->getGroupRoles($upg->getGroup()->getId()));
        }

        $roles = array_merge($roles, $user->getRoles());

        return array_unique($roles);
    }

    private function isUserGranted(UserInterface $user, $object, $attribute)
    {
        /*
          if ($ownable->getOwner() instanceof UserInterface && $owner instanceof EquatableInterface) {
          return $owner->isEqualTo($ownable->getOwner());
          }

          return $ownable->getOwner() === $owner; */

        $roles = $this->initUserRolesOn($user, $object);

        $matched_hierarchy = array_intersect_key($this->supportedRoles, array_flip($roles));

        return in_array($attribute, $roles) || ArrayHelper::in_array_r($attribute, $matched_hierarchy);
    }

}