<?php
/**
 * Created by Khalid Alnajjar (mokha)
 * Date: 06/01/16
 * Time: 22:57
 */

namespace AIE\Bundle\AnomalyBundle\Service;


use AIE\Bundle\AnomalyBundle\Entity\ActionRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\AnomalyRegistrar;
use AIE\Bundle\AnomalyBundle\Entity\Files;
use AIE\Bundle\AnomalyBundle\Entity\Registrar;
use AIE\Bundle\AnomalyBundle\Entity\Request as AnomalyRequest;
use AIE\Bundle\AnomalyBundle\Helper\AnomalyHelper;
use AIE\Bundle\ReportingBundle\Helper\PhpDocx\TemplateManager;
use Doctrine\ORM\Query\ResultSetMapping;

class ReportGenerationService
{


    private $em;
    private $defem;
    private $kernel;
    private $fileuploader;

    private $html_settings = [
        'isFile' => false,
        'parseDivsAsPs' => true,
        'downloadImages' => true,
        'parseFloats' => false,
        'strictWordStyles' => true,
        'wordStyles' => [
            '.table' => 'AIETable',
            '.appendix-title' => 'AppendixTitle',
            '.appendix-desc' => 'AppendixDesc',
            '.caption' => 'Caption',
            '' => 'Normal'
        ]
    ];

    private $table_settings = ['header' => true];

    public function __construct($em, $defem, $kernel, $fileuploader)
    {
        $this->em = $em;
        $this->defem = $defem;
        $this->kernel = $kernel;
        $this->fileuploader = $fileuploader;
    }

    private function getReportVariables($entity, $requestData)
    {
        $registrarVariables = $this->kernel->getContainer()->get('aie_anomaly.anomaly.helper')->formatAnomaly(
            $entity,
            $requestData
        );
        $variables = [];
        foreach ($registrarVariables as $key => $value) {
            $newKey = $key;
            $newKey = strtoupper($newKey);
            $newKey = str_replace(' ', '_', $newKey);
            $variables[$newKey] = $value;
        }

        if ($entity instanceof ActionRegistrar) {
            $anomalyVariables = $this->kernel->getContainer()->get('aie_anomaly.anomaly.helper')->formatAnomaly(
                $entity->getAnomaly()
            );
            foreach ($anomalyVariables as $key => $value) {
                $newKey = $key;
                $newKey = strtoupper($newKey);
                $newKey = str_replace(' ', '_', $newKey);
                if (!array_key_exists($newKey, $variables)) {
                    $variables[$newKey] = $value;
                }
            }

        }

        return $variables;

    }

    public function generateReport(Registrar $entity, AnomalyRequest $request, $name)
    {
        $code = $entity->getCode();
        if ($entity instanceof AnomalyRegistrar) {
            $code = 'AN';
        }

        $template = $this->kernel->locateResource("@AIEAnomalyBundle/Reports/Templates/AIE-{$code}-TEMPLATE.docx");

        $templateManager = new TemplateManager($template);
        $templateManager->setHtmlSettings($this->html_settings);
        $templateManager->setTableSettings($this->table_settings);
        $docx = &$templateManager->getDocument();

        $textVariables = $this->getReportVariables($entity, json_decode($request->getData(), true));

        $textVariables['DEFECT_ORIENTATION'] = (!empty($textVariables['DEFECT_ORIENTATION']) ? $textVariables['DEFECT_ORIENTATION'] : "-");
        $textVariables['GPSE'] = (!empty($textVariables['GPSE']) ? $textVariables['GPSE'] : "-");
        $textVariables['GPSN'] = (!empty($textVariables['GPSN']) ? $textVariables['GPSN'] : "-");
        $textVariables['TOLERANCE'] = (!empty($textVariables['TOLERANCE']) ? $textVariables['TOLERANCE'] : "-");
        $textVariables['SMYS'] = (!empty($textVariables['SMYS']) ? $textVariables['SMYS'] : "-");
        $textVariables['SMTS'] = (!empty($textVariables['SMTS']) ? $textVariables['SMTS'] : "-");
        $textVariables['OUTER_DIAMETER'] = (!empty($textVariables['OUTER_DIAMETER']) ? $textVariables['OUTER_DIAMETER'] : "-");


        /** Add Metal Loss block or Tracking Criteria? */
        $isML = false;
        $anomaly = ($entity instanceof AnomalyRegistrar)? $entity: $entity->getAnomaly();
        $threat = $anomaly->getThreat()->getCode();
        $classification = $anomaly->getClassification()->getCode();

        if($threat == 'CR' && ($classification == 'IML' || $classification == 'EML')){
            $isML = true; 
        }

        if ($isML) {
            $templateManager->deleteBlock('TRACKING_CRITERIA'); //delete tracking criteria block
        } else {
            /** adding tracking criteria section */
            $trackingCriteria = ($entity instanceof AnomalyRegistrar) ? $entity->getTrackingCriteria(
            ) : $entity->getAnomaly()->getTrackingCriteria();
            if(count($trackingCriteria) > 0) {
                $trackingCriteriaText = [];
                foreach ($trackingCriteria as $i => $tc) {
                    array_push(
                        $trackingCriteriaText,
                        [
                            'TRACKING_NUM' => $i + 1,
                            'TRACKING_CRITERIA' => $tc->getTrackingCriteriaString(),
                            'TRACKING_CONC' => $tc->getTrackingConcString(),
                        ]
                    );
                }
                $docx->addTemplateVariable($trackingCriteriaText, 'table', ['header' => true]);
                //delete ml block
                $templateManager->deleteBlock('METAL_LOSS');
            }else{
                $templateManager->deleteBlock('TRACKING_CRITERIA'); //delete tracking criteria block
            }
           
        }

        switch($entity->getCriticality()){
            case 0:
                $risk = "Low";
            break;
            case 1:
                $risk = "Medium";
            break;
            case 2:
                $risk = "High";
            break;
            default:
            break;
        }

        $textVariables['RISK'] = $risk;
        
        /**
         * Update the logo
         */
        $project = $entity->getProject();
        $path = $this->fileuploader->getAbsoluteFilePath($project->getFilepath());
        $logoContent = $this->fileuploader->read($project->getFilepath());

        $tempLogo="";
  
        if ($project && $logoContent)
        {

            $tempLogo = tempnam(sys_get_temp_dir(), 'COMPANY_LOGO_' . uniqid());

            $logoHandle = fopen($tempLogo, "w");
            fwrite($logoHandle, $logoContent);
            fclose($logoHandle);

            $templateManager->replaceHeaderImage('COMPANY_LOGO',$tempLogo); //fixed to replace company logo
            
        }

        $getReportattachments = $request->getAttachedreportanomalys();
        if (!empty($getReportattachments)) 
        {

            $strReportAttach = json_decode($getReportattachments);

            $qb = $this->em->createQueryBuilder();
            if ($entity instanceof AnomalyRegistrar) {
                $passId = $entity->getId();
            }
            else{
                $passId = $entity->getAnomaly()->getId();
            }
            
            $query = $qb->select('partial r.{id,anomaly,filename,caption,document_number,company,date,document_date,size,filepath}')
                        ->from('AIEAnomalyBundle:Files','r')
                        ->Where('r.id IN (:ids)')
                        ->andWhere('r.anomaly = :id')
                        ->setParameter('ids',$strReportAttach)
                        ->setParameter('id',$passId);

            $result = $query->getQuery()->getResult();

            $request->setRequestFiles($result);

        }

        /***
         * This section is responsible of adding the attachments
         */
        $attachments = $request->getRequestFiles();

        if (count($attachments) > 0) {
            $acceptableExtensions = [
                'jpg',
                'jpeg',
                'png',
                'bmp'
            ];

            $attachments = $attachments->filter(
                function (Files $attachment) use ($acceptableExtensions) {
                    $filepath = $attachment->getFilepath();
                    foreach ($acceptableExtensions as $ext) {
                        if (substr($filepath, -strlen($ext)) === $ext) { // ends with? image extensions only
                            return true;
                        }
                    }

                    return false;
                }
            );

            $attachments = array_map(
                function ($attachment) {
                    $content = $this->fileuploader->read($attachment->getFilepath());
                    $tempPath = sys_get_temp_dir().DIRECTORY_SEPARATOR.md5(uniqid(rand(), true)).'.jpg';
                    $fh = fopen($tempPath, 'w');
                    fwrite($fh, $content);
                    fclose($fh);

                    return $tempPath;
                },
                $attachments->toArray()
            );
            $attachments_labels = [];
            $attachments_images = [];

            $maxWidth = 650;
            foreach ($attachments as $key => $attachment) {
                $id = "ATTACHMENTS_{$key}";
                array_push($attachments_labels, '$'.$id.'$');
                list($width, $height, $type, $attr) = getimagesize($attachment);
                $width = ($width <= $maxWidth) ? $width : $maxWidth;
                $attachments_images[$id] = [
                    'html' => "<img src='file://$attachment' width='{$width}'>",
                    'img_path' => $attachment
                ];
            }
            $templateManager->addMultipleTexts(['ATTACHMENTS' => $attachments_labels]);
            $templateManager->replaceVariables();
            foreach ($attachments_images as $id => $data) {
                $docx->replaceTemplateVariableByHTML($id, 'inline', $data['html'], ['downloadImages' => true]);
                @unlink($data['img_path']);
            }
        } else {
            $templateManager->addText('ATTACHMENTS', $templateManager->getClearVariable());
        }


        $actionOwner = $this->em->getRepository('AIEAnomalyBundle:ActionOwners')->findOneBy(
            array('project' => $entity->getProject()->getId())
        );

        $actionOwnerInfo = $this->defem->getRepository('UserBundle:User')->find($actionOwner->getUserId());

        if ($entity instanceof AnomalyRegistrar) {

            $approvalOwner = $this->em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy(
                array('project' => $entity->getProject()->getId(),'anomalyCategory'=>$entity->getCode())
            );

        }
        else
        {
             $approvalOwner = $this->em->getRepository('AIEAnomalyBundle:ApprovalOwners')->findOneBy(
                array('project' => $entity->getProject()->getId())
            );

        }
       
       
        $approvalOwnerInfo = $this->defem->getRepository('UserBundle:User')->find($approvalOwner->getActionOwner()->getId());

        $revisionNos = [];
        $countRevision = 0;

        if ($entity instanceof AnomalyRegistrar){
            foreach($entity->getApprovedRequests() as $index => $request){
                if ( $request->getRequestStatus() == 'update' || $request->getRequestStatus() == 'new' ){
                    $revisionNos[$index+1]= $request->getId();
                    $countRevision++;
                }
            }
        }
        else {
            foreach($entity->getApprovedRequests() as $index => $request){
                if ( $request->getRequestStatus() == 'update' || $request->getRequestStatus() == 'new' || $request->getRequestStatus() == 'deferral'){
                    $revisionNos[$index+1]= $request->getId();
                    $countRevision++;
                }
            }
        }

        $revisionId = array_search($request->getId(),$revisionNos);

        if (!$revisionId) $revisionId = $countRevision;

        $textVariables['REQUESTS_COUNTS'] = $revisionId;
        $textVariables['APPROVAL_OWNER'] = $approvalOwnerInfo->getUsername();
        $textVariables['ACTION_OWNER'] = $actionOwnerInfo->getUsername();
        $templateManager->addMultipleTexts($textVariables);
        $templateManager->replaceVariables();
        $templateManager->clearDocument();

        $report = $templateManager->generateDocument();

        //Sending the file to be downloaded

        $fullName = sys_get_temp_dir().DIRECTORY_SEPARATOR.$name;
 
        $report->createDocx($fullName);
      
        if ($tempLogo) unlink($tempLogo);

        return $report;

    }
    
}