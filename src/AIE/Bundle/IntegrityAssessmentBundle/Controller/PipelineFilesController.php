<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineFilesType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineEditFilesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * PipelineFiles controller.
 *
 * @Route("pipeline/{pipelineId}/pipeline_files")
 */
class PipelineFilesController extends BaseController {

    /**
     * Lists all PipelineFiles entities.
     *
     * @Route("/", name="pipeline_files")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->findByPipeline($pipelineId);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'   => $entities,
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a new PipelineFiles entity.
     *
     * @Route("/", name="pipeline_files_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:PipelineFiles:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId)
    {
        $entity = new PipelineFiles();
        $form = $this->createCreateForm($entity, $pipelineId);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isValid())
        {
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
            $entity->setPipeline($pipeline);
            $data = $form->getData();
            /* Handle Uploaded File */
            $file = $data->getFile();
            $uploadedPath = $this->uploadFile($file, $entity);
            $entity->setFilepath($uploadedPath);
            $entity->setSize($file->getSize());

            $date = $data->getDocumentDate();
            if ($date)
            {
                $entity->setDocumentDate(new \DateTime($date));
            }
            $entity->setDate(new \DateTime());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pipeline_files', array('pipelineId' => $pipelineId)));
            //return $this->redirect($this->generateUrl('pipeline_files_show', array('id' => $entity->getId(), 'pipelineId' => $pipelineId)));
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a form to create a PipelineFiles entity.
     *
     * @param PipelineFiles $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PipelineFiles $entity, $pipelineId)
    {
        $form = $this->createForm(new PipelineFilesType(), $entity, array(
            'action' => $this->generateUrl('pipeline_files_create', array('pipelineId' => $pipelineId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new PipelineFiles entity.
     *
     * @Route("/new", name="pipeline_files_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($pipelineId)
    {
        $entity = new PipelineFiles();
        $form = $this->createCreateForm($entity, $pipelineId);

        $em = $this->getDoctrine()->getManager();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/{id}", name="pipeline_files_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineFiles entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'     => $entity,
            'pipelineId' => $pipelineId
            //'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PipelineFiles entity.
     *
     * @Route("/{id}/edit", name="pipeline_files_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineFiles entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $editForm = $this->createEditForm($entity, $pipelineId);
        $deleteForm = $this->createDeleteForm($id, $pipelineId);

        return array(
            'entity'      => $entity,
            'pipelineId'  => $pipelineId,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a PipelineFiles entity.
     *
     * @param PipelineFiles $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PipelineFiles $entity, $pipelineId)
    {
        $form = $this->createForm(new PipelineEditFilesType($entity), $entity, array(
            'action' => $this->generateUrl('pipeline_files_update', array('id' => $entity->getId(), 'pipelineId' => $pipelineId)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing PipelineFiles entity.
     *
     * @Route("/{id}", name="pipeline_files_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:PipelineFiles:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineFiles entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $deleteForm = $this->createDeleteForm($id, $pipelineId);
        $editForm = $this->createEditForm($entity, $pipelineId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {

            /* Handle Uploaded File */
            $data = $editForm->getData();
            /*$file = $data->getFile();
            $uploadedPath = $this->replaceFile($file, $entity);
            $entity->setFilepath($uploadedPath);
            $entity->setSize($file->getSize());*/

            $date = $data->getDocumentDate();
            if ($date)
            {
                $entity->setDocumentDate(new \DateTime($date));
            }

            $em->flush();
            return $this->redirect($this->generateUrl('pipeline_files', array('pipelineId' => $pipelineId)));
            //return $this->redirect($this->generateUrl('pipeline_files_edit', array('id' => $id, 'pipelineId' => $pipelineId)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a PipelineFiles entity.
     *
     * @Route("/{id}", name="pipeline_files_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $pipelineId)
    {
        $form = $this->createDeleteForm($id, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find PipelineFiles entity.');
            }

            $this->deleteFile($entity);
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipeline_files', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to delete a PipelineFiles entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pipeline_files_delete', array('id' => $id, 'pipelineId' => $pipelineId)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm();
    }

}
