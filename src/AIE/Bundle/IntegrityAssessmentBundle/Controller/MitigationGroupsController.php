<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\MitigationGroups;
use AIE\Bundle\IntegrityAssessmentBundle\Form\MitigationGroupsType;

/**
 * MitigationGroups controller.
 *
 * @Route("/mitigationgroups")
 */
class MitigationGroupsController extends BaseController
{

    /**
     * Lists all MitigationGroups entities.
     *
     * @Route("/", name="mitigationgroups")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationGroups')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new MitigationGroups entity.
     *
     * @Route("/", name="mitigationgroups_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:MitigationGroups:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MitigationGroups();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mitigationgroups_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a MitigationGroups entity.
     *
     * @param MitigationGroups $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MitigationGroups $entity)
    {
        $form = $this->createForm(new MitigationGroupsType(), $entity, array(
            'action' => $this->generateUrl('mitigationgroups_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MitigationGroups entity.
     *
     * @Route("/new", name="mitigationgroups_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new MitigationGroups();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a MitigationGroups entity.
     *
     * @Route("/{id}", name="mitigationgroups_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationGroups')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationGroups entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MitigationGroups entity.
     *
     * @Route("/{id}/edit", name="mitigationgroups_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationGroups')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationGroups entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a MitigationGroups entity.
    *
    * @param MitigationGroups $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MitigationGroups $entity)
    {
        $form = $this->createForm(new MitigationGroupsType(), $entity, array(
            'action' => $this->generateUrl('mitigationgroups_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing MitigationGroups entity.
     *
     * @Route("/{id}", name="mitigationgroups_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:MitigationGroups:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationGroups')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationGroups entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mitigationgroups_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a MitigationGroups entity.
     *
     * @Route("/{id}", name="mitigationgroups_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationGroups')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MitigationGroups entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mitigationgroups'));
    }

    /**
     * Creates a form to delete a MitigationGroups entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mitigationgroups_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
