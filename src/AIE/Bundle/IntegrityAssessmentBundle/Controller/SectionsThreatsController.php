<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment;
use AIE\Bundle\IntegrityAssessmentBundle\Manager\MitigationFrequenciesManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SectionsThreats controller.
 *
 * @Route("pipeline/{pipelineId}/sectionsthreats")
 */
class SectionsThreatsController extends BaseController
{

    /**
     * Lists all Threats entities.
     *
     * @Route("/list", name="sectionsthreats")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);


        $sections_count = array();
        if ($entities) {
            foreach ($entities as $entity) {

                $id = $entity->getPipelineSection()->getSection()->getId();
                if (isSet($sections_count[$id])) {
                    $sections_count[$id]++;
                } else {
                    $sections_count[$id] = 1;
                }
            }
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities' => $entities,
            'pipelineId' => $pipelineId,
            'sections_count' => $sections_count
        );
    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/", name="sectionsthreats_list")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreats:list.html.twig")
     */
    public function newAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $sections = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findByPipeline($pipelineId);
        $threats = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();

        $form = $this->createCreateForm($pipelineId, $sections, $threats);

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'sections' => $sections,
            'threats' => $threats,
            'pipelineId' => $pipelineId,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new SectionsThreats entity.
     *
     * @Route("/", name="sectionsthreats_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreats:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $sections = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findByPipeline($pipelineId);
        $threats = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);

        $project = $pipeline->getProject();
        $form = $this->createCreateForm($pipelineId, $sections, $threats);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();

            $projectConsequences = $project->getPc();

            foreach ($data as $k => $d) {
                $attr = explode('_', $k); //1=>section, 2=>threat

                $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findOneBy(array('pipelineSections' => $attr[1], 'threats' => $attr[2]));


                if (!$sectionThreat && $d === true) {
                    $section = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($attr[1]);
                    $threat = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->find($attr[2]);

                    $sectionThreat = new SectionsThreats();
                    $sectionThreat->setPipelineSections($section);
                    $sectionThreat->setThreats($threat);
                    $sectionThreat->setProbabilityOfFailer(1);
                    $sectionThreat->setAdjProbabilityOfFailer(1);
                    $em->persist($sectionThreat);

                    foreach ($projectConsequences as $consequence) {
                        $c = new ConsequenceAssessment();
                        $c->setSectionsThreats($sectionThreat);
                        $c->setConsequence($consequence);
                        $c->setServerity(1);

                        $em->persist($c);
                    }
                } else if ($sectionThreat && $d === false) {
                    $em->persist($sectionThreat);
                    $em->remove($sectionThreat);
                }
            }

            $em->flush();
        }

        $mitigationFreqManager = new MitigationFrequenciesManager($em);
        $mitigationFreqManager->updateMitigationFrequencies($pipeline);

        return $this->redirect($this->generateUrl('sectionsthreats_list', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a SectionsThreats entity.
     *
     * @param int $pipelineId The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($pipelineId, $sections, $threats)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('sectionsthreats_create', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        foreach ($sections as $section) {
            foreach ($threats as $threat) {
                $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findOneBy(array('pipelineSections' => $section->getId(), 'threats' => $threat->getId()));
                $attr = array();
                if ($sectionThreat) {
                    $attr = array('checked' => 'checked');
                }
                $form->add('sectionThreat_' . $section->getId() . '_' . $threat->getId(), 'checkbox', array('label' => '', 'required' => false, 'attr' => $attr));
            }
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save', 'attr' => array('class' => 'right')), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/liklyhood", name="sectionsthreats_liklyhood")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreats:liklyhood.html.twig")
     */
    public function LiklyhoodAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($pipeline->getProject()->getId());
        $riskMatrix = $project->getRiskMatrix();


        $sections = $pipeline->getPse();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        foreach ($sections as $section) {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $mitigationAction[$section->getId()] = $stm;
            foreach ($stm as $s) {
                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()])) {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]++;
                } else {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }
            }
        }

        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);

        $sectionThreats = array();
        foreach ($sectionsThreats as $st) {
            if (!isSet($sectionThreats[$st->getPipelineSections()->getId()])) {
                $sectionThreats[$st->getPipelineSections()->getId()] = array();
            }
            $sectionThreats[$st->getPipelineSections()->getId()][] = $st;
        }

        $form = $this->createLiklyhoodForm($pipelineId, $sections, $sectionsThreats);


        // $threats = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();
        // $form = $this->createLiklyhoodForm($pipelineId, $sections, $threats);

        return array(
            'riskMatrix' => $riskMatrix,
            'sections' => $sections,
            'mitigationAction' => $mitigationAction,
            'sectionsThreatsCounter' => $sectionsThreatsCounter,
            'sectionsThreats' => $sectionsThreats,
            'form' => $form->createView(),
            'sectionThreats' => $sectionThreats,
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a new SectionsThreats entity.
     *
     * @Route("/liklyhood", name="sectionsthreats_liklyhood_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreats:liklyhood.html.twig")
     */
    public function createLiklyhoodAction(Request $request, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $sections = $pipeline->getPse();
        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);


        $form = $this->createLiklyhoodForm($pipelineId, $sections, $sectionsThreats);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();


            foreach ($data as $k => $d) {
                $attr = explode('_', $k); //1=>section, 2=>threat
                $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($attr[2]);

                if ($sectionThreat) {
                    $sectionThreat->setProbabilityOfFailer($d->getProbabilityOfFailer());
                    $sectionThreat->setConfidence($d->getConfidence());
                    $adj = $d->getProbabilityOfFailer();
                    if (!$d->getConfidence() && $adj < 5)
                        $adj++;
                    $sectionThreat->setAdjProbabilityOfFailer($adj);
                }
            }

            $em->flush();
        }

        $mitigationFreqManager = new MitigationFrequenciesManager($em);
        $mitigationFreqManager->updateMitigationFrequencies($pipeline);

        return $this->redirect($this->generateUrl('sectionsthreats_liklyhood', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a SectionsThreats entity.
     *
     * @param int $pipelineId The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createLiklyhoodForm($pipelineId, $sections, $threats)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('sectionsthreats_liklyhood_create', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        /*
          foreach ($sections as $section) {
          foreach ($threats as $threat) {
          if ($threat->getPipelineSections()->getId() != $section->getId())
          continue;

          if(empty($threat->getStm()))
          continue;

          $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($threat->getId());
          $form->add('sectionThreat_' . $section->getId() . '_' . $threat->getId(), new SectionsThreatsType($sectionThreat));
          }
          } */

        foreach ($threats as $threat) {
            $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($threat->getId());
            if (count($sectionThreat->getStm()) == 0) {
                //continue;
            }
            $form->add('sectionThreat_' . $threat->getPipelineSections()->getId() . '_' . $threat->getId(), new SectionsThreatsType($sectionThreat));
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/risks", name="threats_risks")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreats:threats_risks.html.twig")
     */
    public function FreqAssesmentAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $pipeline->getProject()->getId()));

        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);
        $sectionsThreatsConsequences = array();
        foreach ($sectionsThreats as $st) {
            $sectionsThreatsConsequences[$st->getId()] = array();
            $sectionThreatsconsequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findBy(array('sectionsThreats' => $st->getId()));

            foreach ($sectionThreatsconsequences as $stc) {
                $sectionsThreatsConsequences[$st->getId()][$stc->getConsequence()->getId()] = $stc->getServerity();
            }
        }

        $sections = $pipeline->getPse();

        $riskmatrix = $pipeline->getProject()->getRiskMatrix()->getColorMatrix();


        // $threats = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();
        // $form = $this->createAssesmentFreqForm($pipelineId, $sections, $threats);

        return array(
            'sections' => $sections,
            'pipelineId' => $pipelineId,
            'consequences' => $consequences,
            'sectionsThreatsConsequences' => $sectionsThreatsConsequences,
            'riskmatrix' => $riskmatrix,

        );
    }

}
