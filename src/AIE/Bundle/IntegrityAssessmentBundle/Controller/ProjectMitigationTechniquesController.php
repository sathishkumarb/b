<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ProjectMitigationTechniquesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * ProjectMitigationTechniques controller.
 *
 * @Route("/project/{projectId}/mitigationtechniques")
 */
class ProjectMitigationTechniquesController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all ProjectMitigationTechniques entities.
     *
     * @Route("/", name="projectmitigationtechniques")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->findBy(array('project' => $projectId));
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);

        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'project' => $project,
        );
    }

    /**
     * Creates a new ProjectMitigationTechniques entity.
     *
     * @Route("/", name="projectmitigationtechniques_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:ProjectMitigationTechniques:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {
        $entity = new ProjectMitigationTechniques();
        $form = $this->createCreateForm($entity, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
            $entity->setProject($project);
            $entity->setDate(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('projectmitigationtechniques', array('projectId' => $projectId)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Creates a form to create a ProjectMitigationTechniques entity.
     *
     * @param ProjectMitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProjectMitigationTechniques $entity, $projectId) {
        $form = $this->createForm(new ProjectMitigationTechniquesType(), $entity, array(
            'action' => $this->generateUrl('projectmitigationtechniques_create', array('projectId'=>$projectId)),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));
        return $form;
    }

    /**
     * Displays a form to create a new ProjectMitigationTechniques entity.
     *
     * @Route("/new", name="projectmitigationtechniques_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId) {
        $entity = new ProjectMitigationTechniques();
        $form = $this->createCreateForm($entity, $projectId);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Finds and displays a ProjectMitigationTechniques entity.
     *
     * @Route("/{id}", name="projectmitigationtechniques_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $projectId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectMitigationTechniques entity.');
        }

       // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'projectId' => $projectId
            //'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ProjectMitigationTechniques entity.
     *
     * @Route("/{id}/edit", name="projectmitigationtechniques_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $projectId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectMitigationTechniques entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId,
        );
    }

    /**
     * Creates a form to edit a ProjectMitigationTechniques entity.
     *
     * @param ProjectMitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ProjectMitigationTechniques $entity, $projectId) {
        $form = $this->createForm(new ProjectMitigationTechniquesType(), $entity, array(
            'action' => $this->generateUrl('projectmitigationtechniques_update', array('id' => $entity->getId(), 'projectId' => $projectId)),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing ProjectMitigationTechniques entity.
     *
     * @Route("/{id}", name="projectmitigationtechniques_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:ProjectMitigationTechniques:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $projectId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectMitigationTechniques entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('projectmitigationtechniques_edit', array('id' => $id, 'projectId' => $projectId)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Deletes a ProjectMitigationTechniques entity.
     *
     * @Route("/{id}", name="projectmitigationtechniques_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProjectMitigationTechniques entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('projectmitigationtechniques'));
    }

    /**
     * Creates a form to delete a ProjectMitigationTechniques entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('projectmitigationtechniques_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
