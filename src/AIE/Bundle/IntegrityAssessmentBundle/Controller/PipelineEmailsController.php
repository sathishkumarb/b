<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineEmailsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * PipelineEmails controller.
 *
 * @Route("/pipeline/{pipelineId}/emails")
 */
class PipelineEmailsController extends BaseController {

    /**
     * Lists all PipelineEmails entities.
     *
     * @Route("/", name="pipelineemails")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineEmails')->findByPipeline($pipelineId);

        foreach ($entities as $entity)
        {
            $deleteForm = $this->createDeleteForm($entity->getId(), $pipelineId);
            $entity->delete_form = $deleteForm->createView();
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'   => $entities,
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a new PipelineEmails entity.
     *
     * @Route("/", name="pipelineemails_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:PipelineEmails:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId)
    {
        $entity = new PipelineEmails();
        $form = $this->createCreateForm($entity, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
            $entity->setPipeline($pipeline);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pipelineemails', array('pipelineId' => $pipelineId)));
        }

        return $this->redirect($this->generateUrl('pipelineemails_new', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a PipelineEmails entity.
     *
     * @param PipelineEmails $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PipelineEmails $entity, $pipelineId)
    {
        $form = $this->createForm(new PipelineEmailsType(), $entity, array(
            'action' => $this->generateUrl('pipelineemails_create', array('pipelineId' => $pipelineId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new PipelineEmails entity.
     *
     * @Route("/new", name="pipelineemails_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($pipelineId)
    {
        $entity = new PipelineEmails();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createCreateForm($entity, $pipelineId);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'pipelineId' => $pipelineId,
            'projectId'  => $pipeline->getProject()->getId()
        );
    }

    /**
     * Finds and displays a PipelineEmails entity.
     *
     * @Route("/{id}", name="pipelineemails_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineEmails')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineEmails entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $deleteForm = $this->createDeleteForm($id, $pipelineId);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'pipelineId'  => $pipelineId,
        );
    }

    /**
     * Displays a form to edit an existing PipelineEmails entity.
     *
     * @Route("/{id}/edit", name="pipelineemails_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineEmails')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineEmails entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $editForm = $this->createEditForm($entity, $pipelineId);
        $deleteForm = $this->createDeleteForm($id, $pipelineId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'pipelineId'  => $pipelineId,
            'projectId'   => $pipeline->getProject()->getId()
        );
    }

    /**
     * Creates a form to edit a PipelineEmails entity.
     *
     * @param PipelineEmails $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PipelineEmails $entity, $pipelineId)
    {
        $form = $this->createForm(new PipelineEmailsType(), $entity, array(
            'action' => $this->generateUrl('pipelineemails_update', array('pipelineId' => $pipelineId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing PipelineEmails entity.
     *
     * @Route("/{id}", name="pipelineemails_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:PipelineEmails:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineEmails')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineEmails entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $deleteForm = $this->createDeleteForm($id, $pipelineId);
        $editForm = $this->createEditForm($entity, $pipelineId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);

            return $this->redirect($this->generateUrl('pipelineemails', array('pipelineId' => $pipelineId)));
        }

        return $this->redirect($this->generateUrl('pipelineemails_edit', array('pipelineId' => $pipelineId, 'id' => $id)));
    }

    /**
     * Deletes a PipelineEmails entity.
     *
     * @Route("/{id}", name="pipelineemails_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $pipelineId)
    {
        $form = $this->createDeleteForm($id, $pipelineId);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        if ($form->isValid())
        {
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineEmails')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find PipelineEmails entity.');
            }

            $assignedMitigations = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBy(['action_owner' => $id ]);
            foreach($assignedMitigations as $mitigation){
                $mitigation->setActionOwner(null);
                $mitigation->setRemindedOn(null);
                $em->persist($mitigation);
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipelineemails', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to delete a PipelineEmails entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pipelineemails_delete', array('pipelineId' => $pipelineId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => [ 'onClick' => 'return confirm(\'Are you sure you want to delete this item?\');']))
            ->getForm();
    }

}
