<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesEditType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesSpecificationsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * PipelinesSpecifications controller.
 *
 * @Route("pipeline/{pipelineId}/details")
 */
class PipelinesSpecificationsController extends BaseController {

    /**
     * Lists all PipelinesSpecifications entities.
     *
     * @Route("/", name="pipelinesspecifications")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findByPipeline($pipelineId);

        $form = $this->createCreateForm($pipelineId, $entities);
        $pipeline = $entities[0]->getPipeline();
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'   => $entities,
            'pipelineId' => $pipelineId,
            'form'       => $form->createView(),
            'pipeline'   => $pipeline
        );
    }

    /**
     * Lists all PipelinesSpecifications entities.
     *
     * @Route("/", name="pipelinesspecifications_update")
     * @Method("POST")
     * @Template()
     */
    public function updateAction(Request $request, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findByPipeline($pipelineId);

        $form = $this->createCreateForm($pipelineId, $entities);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $data = $form->getData();


            $pipeline = $data['pipeline'];
            $em->persist($pipeline);

            foreach ($entities as $e)
            {
                $id = $e->getId();

                $e->setComment($data["specification_{$id}"]->getComment());
                $e->setValue($data["specification_{$id}"]->getValue());
                $e->setUnit($data["specification_{$id}"]->getUnit());
                $em->persist($e);
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipelinesspecifications_update', array('pipelineId' => $pipelineId)));
    }

    private function createCreateForm($pipelineId, $specifications)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('pipelinesspecifications_update', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        $pipeline = $specifications[0]->getPipeline();
        $files = $pipeline->getPf();
        $form->add('pipeline', new PipelinesEditType($pipeline, $files), ['data' => $pipeline, 'mapped' => true, 'label' => false]);

        foreach ($specifications as $s)
        {
            $form->add('specification_' . $s->getId(), new PipelinesSpecificationsType($s));
        }
        //$deleteForm = $this->createDeleteForm($id);

        $form->add('submit', 'submit', $this->options(array('label' => 'Save', 'attr' => array('class' => 'right')), 'btn'));

        $form = $form->getForm();

        return $form;
    }

}
