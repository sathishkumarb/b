<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ProjectConsequencesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\FormError;

/**
 * ProjectConsequences controller.
 *
 * @Route("/projects/{projectId}/consequences")
 */
class ProjectConsequencesController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all ProjectConsequences entities.
     *
     * @Route("/", name="projectsconsequences")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:ProjectConsequences:new.html.twig")
     */
    public function indexAction($projectId) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $projectId));
        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->findAll();


        $form = $this->createCreateForm($projectId, $consequences);
        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'form' => $form->createView(),
            'consequences' => $consequences,
        );
    }

    /**
     * Creates a new ProjectConsequences entity.
     *
     * @Route("/", name="projectsconsequences_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:ProjectConsequences:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $projectId));
        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->findAll();

        $form = $this->createCreateForm($projectId, $consequences);
        $form->handleRequest($request);

        if ($form->isValid()) {


            $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
            $data = $form->getData();


            $count = count(array_filter($data));

            if ($count < 1) {
                $this->get('session')->getFlashBag()->add('error', 'A minimum of 1 consequences should be selected!');
            } else if ($count > 6) {
                $this->get('session')->getFlashBag()->add('error', 'A maximum of 6 consequences can be selected!');
            } else {
                foreach ($data as $k => $d) {

                    $projectConsequence = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findOneBy(array('project' => $projectId, 'consequence' => $k));

                    if (!$projectConsequence && $d === true) {
                        $projectConsequence = new ProjectConsequences();
                        $projectConsequence->setProject($project);
                        $consequence = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->find($k);
                        $projectConsequence->setConsequence($consequence);
                        $projectConsequence->setDesc1($consequence->getDesc1());
                        $projectConsequence->setDesc2($consequence->getDesc2());
                        $projectConsequence->setDesc3($consequence->getDesc3());
                        $projectConsequence->setDesc4($consequence->getDesc4());
                        $projectConsequence->setDesc5($consequence->getDesc5());
                        $em->persist($projectConsequence);
                    } else if ($projectConsequence && $d === false) {
                        $em->persist($projectConsequence);
                        $em->remove($projectConsequence);
                    }
                    $em->flush();
                }
                $this->get('session')->getFlashBag()->add('success', 'Consequences successfully updated!');
            }
        }
        return $this->redirect($this->generateUrl('projectsconsequences', array('projectId' => $projectId)));
    }

    /**
     * Creates a form to create a ProjectConsequences entity.
     *
     * @param ProjectConsequences $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($projectId, $consequences) {
        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('projectsconsequences_create', array('projectId' => $projectId)))
                ->setMethod('POST');

        $em = $this->getDoctrine()->getManager();

        foreach ($consequences as $consequence) {
            $projectConsequence = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findOneBy(array('project' => $projectId, 'consequence' => $consequence->getId()));
            $attr = array();
            if ($projectConsequence) {
                $attr = array('checked' => 'checked');
            }
            $form->add($consequence->getId(), 'checkbox', array('label' => '', 'required' => false, 'attr' => $attr));
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
                ->getForm();


        return $form;
    }

    /**
     * Displays a form to create a new ProjectConsequences entity.
     *
     * @Route("/new", name="projectsconsequences_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new ProjectConsequences();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ProjectConsequences entity.
     *
     * @Route("/{id}", name="projectsconsequences_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectConsequences entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ProjectConsequences entity.
     *
     * @Route("/{id}/edit", name="projectsconsequences_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectConsequences entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ProjectConsequences entity.
     *
     * @param ProjectConsequences $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ProjectConsequences $entity) {
        $form = $this->createForm(new ProjectConsequencesType(), $entity, array(
            'action' => $this->generateUrl('projectsconsequences_update', array('id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing ProjectConsequences entity.
     *
     * @Route("/{id}", name="projectsconsequences_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:ProjectConsequences:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectConsequences entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('projectsconsequences_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ProjectConsequences entity.
     *
     * @Route("/{id}", name="projectsconsequences_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProjectConsequences entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('projectsconsequences'));
    }

    /**
     * Creates a form to delete a ProjectConsequences entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('projectsconsequences_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
