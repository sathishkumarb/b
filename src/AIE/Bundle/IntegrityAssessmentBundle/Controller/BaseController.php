<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Security\AuthenticatedController;
use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ProjectConsequencesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AIE\Bundle\IntegrityAssessmentBundle\Security;
use Symfony\Component\HttpFoundation\File\File;
use AIE\Bundle\VeracityBundle\Controller\BaseController as VeracityBaseController;

class BaseController extends VeracityBaseController {

	/**
	 * @param $em
	 * @return array
	 */
	public function getGrantedProjects() {
		$em = $this->getManager();
		$securityContext = $this->container->get('security.authorization_checker');
		$projects = [];
		if ($securityContext->isGranted('ROLE_PROJECTS')) {
			$projects = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->findAll();
		} else {
			$user = $this->getUser();
			$projects = [];

			$assignedProjects = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->findBy(['user' => $user->getId()]);
			foreach ($assignedProjects as $p) {
				$projects[ ] = $p->getProject();
			}
		}

		return $projects;
	}
}