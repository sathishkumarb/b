<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Manager\MitigationFrequenciesManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ConsequenceAssessmentType;
use Doctrine\Common\Collections\ArrayCollection;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * ConsequenceAssessment controller.
 *
 * @Route("pipeline/{pipelineId}/consequenceassessment")
 */
class ConsequenceAssessmentController extends BaseController {
    /**
     * Lists all ConsequenceAssessment entities.
     *
     * @Route("/", name="consequenceassessment")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId) {
        $em = $this->getDoctrine()->getManager();

        //$consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findByPipelineId($pipelineId);

        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $pipeline->getProject()->getId()));

        $this->setLeftTitleFromPipeline($pipeline);

        $form = $this->createCreateForm($pipelineId, $consequences, $sectionsThreats);

        //Count how many Threats in each Section
        $sections_count = array();

        if ($sectionsThreats) {
            foreach ($sectionsThreats as $st) {

                $id = $st->getPipelineSections()->getId();
                if (isSet($sections_count[$id])) {
                    $sections_count[$id]++;
                } else {
                    $sections_count[$id] = 1;
                }
            }
        }

        return array(
            'consequences' => $consequences,
            'sectionsThreats' => $sectionsThreats,
            'form' => $form->createView(),
            'sections_count' => $sections_count
        );
    }

    /**
     * Creates a new ConsequenceAssessment entity.
     *
     * @Route("/", name="consequenceassessment_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:ConsequenceAssessment:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId) {
        $em = $this->getDoctrine()->getManager();

        //$consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findByPipelineId($pipelineId);

        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $pipeline->getProject()->getId()));

        $form = $this->createCreateForm($pipelineId, $consequences, $sectionsThreats);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            foreach ($data as $k => $d) {
                $attr = explode('_', $k); //1=>sectionThreat, 2=>Consequence
                $c = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findOneBy(array('sectionsThreats' => $attr[1], 'consequence' => $attr[2]));
                if(!$c){
                    $c = new ConsequenceAssessment();
                    $st = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($attr[1]);
                    $cc = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->find($attr[2]);
                    $c->setSectionsThreats($st);
                    $c->setConsequence($cc);
                }
                $c->setServerity($d->getServerity());
                $em->persist($c);
            }

            $em->flush();
        }

        $mitigationFreqManager = new MitigationFrequenciesManager($em);
        $mitigationFreqManager->updateMitigationFrequencies($pipeline);

        return $this->redirect($this->generateUrl('consequenceassessment', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a ConsequenceAssessment entity.
     *
     * @param ConsequenceAssessment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($pipelineId, $consequences, $sectionsThreats) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('consequenceassessment_create', array('pipelineId' => $pipelineId)))
                ->setMethod('POST');


        foreach ($sectionsThreats as $st) {
            foreach ($consequences as $consequence) {

                $c = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findOneBy(array('sectionsThreats' => $st->getId(), 'consequence' => $consequence->getId()));
                if (!$c) {
                    $c = new ConsequenceAssessment();
                    $c->setSectionsThreats($st);
                    $c->setConsequence($consequence);
                }
                $form->add('sc_' . $st->getId() . '_' . $consequence->getId(), new ConsequenceAssessmentType($c));
            }
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save', 'attr' => array('class' => 'right')), 'btn'))
                ->getForm();


        return $form;
    }

}
