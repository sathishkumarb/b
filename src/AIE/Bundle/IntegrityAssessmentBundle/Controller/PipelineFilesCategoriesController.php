<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\StorageBundle\Upload\FileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineFilesCategoriesType;

/**
 * PipelineFilesCategories controller.
 *
 * @Route("pipeline/{pipelineId}/files/categories")
 */
class PipelineFilesCategoriesController extends BaseController {

    private $fileuploader;

    private function generateTree($categories, $files)
    {

        $tree = [];
        $_catFiles = $files;

        foreach ($_catFiles as $key => $file)
        {
            if ($file->getCategory() === null)
            {
                $tree[] = $file->getAsTreeNode($this->fileuploader);
                unset($_catFiles[$key]);
            }
        }

        foreach ($categories as $category)
        {
            $categoryNode = $category->getAsTreeNode($_catFiles, $this->fileuploader);
            $tree[] = $categoryNode;
        }

        return $tree;
    }

    /**
     * Lists all PipelineFilesCategories entities.
     *
     * @Route("/", name="pipeline_files_categories", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function getFilesTreeAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();
        $folders = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->findBy(['pipeline' => $pipelineId, 'parent' => null]);
        $files = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->findBy(['pipeline' => $pipelineId]);

        $this->fileuploader = $this->get('storage.file_uploader');

        $tree = $this->generateTree($folders, $files);

        return new JsonResponse($tree);
    }

    /**
     * Creates a new PipelineFilesCategories entity.
     *
     * @Route("/", name="pipeline_files_categories_create", options={"expose"=true})
     * @Method("POST")
     */
    public function createAction(Request $request, $pipelineId)
    {
        $entity = new PipelineFilesCategories();
        $name = $request->get('name');
        $parentId = $request->get('parent');
        $response = false;

        if ($name)
        {
            $em = $this->getDoctrine()->getManager();
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);

            if ($pipeline)
            {
                $entity->setName($name);
                $entity->setPipeline($pipeline);
                if ($parentId)
                {
                    $parent = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($parentId);
                    $entity->setParent($parent);
                }

                $em->persist($entity);
                $em->flush();

                $response = true;
            }
        }

        return new JsonResponse(['success' => $response, 'id' => $entity->getId()]);
    }

    /**
     * Edits an existing PipelineFilesCategories entity.
     *
     * @Route("/{id}", name="pipeline_files_categories_update", options={"expose"=true})
     * @Method("PUT")
     */
    public function updateAction(Request $request, $pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($id);

        if (! $category)
        {
            throw $this->createNotFoundException('Unable to find FilesCategories entity.');
        }

        $name = $request->get('name');
        $response = false;

        if ($name)
        {
            $category->setName($name);
            $em->flush();
            $response = true;
        }

        return new JsonResponse(['success' => $response]);
    }

    /**
     * Deletes a PipelineFilesCategories entity.
     *
     * @Route("/{id}", name="pipeline_files_categories_delete", options={"expose"=true})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $response = false;
        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineFilesCategories entity.');
        }

        if ($em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->deleteCategory($id))
        {
            $response = true;
        }

        return new JsonResponse(['success' => $response]);
    }


    /**
     * Creates a new PipelineFilesCategories entity.
     *
     * @Route("/move/{id}", name="pipeline_files_categories_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveAction(Request $request, $pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $response = false;
        $category = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($id);

        if ($category)
        {
            $parentId = $request->get('parent');
            if ($parentId)
            {
                $parent = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($parentId);
                if ($parent)
                {
                    $category->setParent($parent);
                    $response = true;
                }
            } else
            {
                $category->setParent(NULL);
                $response = true;
            }

            $em->flush();
        }

        return new JsonResponse(['success' => $response]);
    }


	/**
     * Creates a new PipelineFilesCategories entity.
     *
     * @Route("/move_file/{id}", name="pipeline_files_move", options={"expose"=true})
     * @Method("POST")
     */
    public function moveFileAction(Request $request, $pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $response = false;
        $file = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->find($id);

        if ($file)
        {
            $parentId = $request->get('parent');
            if ($parentId)
            {
                $parent = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFilesCategories')->find($parentId);
                if ($parent)
                {
                    $file->setCategory($parent);
                    $response = true;
                }
            } else
            {
                $file->setCategory(NULL);
                $response = true;
            }

            $em->flush();
        }

        return new JsonResponse(['success' => $response]);
    }
}
