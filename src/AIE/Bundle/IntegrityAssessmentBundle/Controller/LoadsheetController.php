<?php
namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesType;
//use AIE\Bundle\IntegrityAssessmentBundle\Helper;
//use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesSpecificationsType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Form\MasterLoadSheetType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\LoadsheetHelper;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\LoadsheetAssessmentHelper;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\LoadsheetRiskAssessmentHelper;
use Doctrine\ORM\ORMException;
use PDOException;
use Exception;

/**
 * Loadsheet controller.
 *
 * @Route("projects/{projectId}/loadsheet", name="loadsheet")
 */
class LoadsheetController extends BaseController {

//    use Helper\FormStyleHelper;

    /**
     * Loadsheet upload/ download
     *
     * @Route("/", name="loadsheet")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function loadsheetAction(Request $request, $projectId){

        return $this->uploadLoadSheetHandler($request, $projectId, $request->get('_route'));
    }


    public function uploadLoadSheetHandler(Request $request, $projectId, $submitForm) {
        $isReadFile =0;
        $dataList=[];
        $excelHeader=null;
        $isValidLoadSheetFormat=true;
        $fileInfo=['name'=>'','ext'=>'' ,'size'=>'','rowsCount'=>0,'status'=>0];
        $em = $this->getManager();
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
        if (! $project) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Data Missing','message' => "Unable to find Projects entity."));

//            throw $this->createNotFoundException('Unable to find Projects entity.');
        }
        $form = $this->createMasterLoadSheetForm($projectId, $project, $submitForm);
        $form->handleRequest($request);
        $downloadform = $this->createDownloadForm($projectId);
        $insertform = $this->createInsertLoadSheetForm($projectId, $submitForm, null, null,null, null);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $file = $data['loadsheetfile'];
                if ($file->getMimeType() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    $this->addFlash('warning', 'File format must be xlsx (Excel 2007 or later)');
                    return $this->redirect(
                        $this->generateUrl('loadsheet', ['projectId' => $projectId])
                    );
                }else{
                    $fileInfo['name']=$file->getClientOriginalName();
                    $fileInfo['ext']=$file->guessExtension();
                    $fileInfo['size']=$this->formatSizeUnits($file->getClientSize());
                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file);
                    $lsHelper = new LoadsheetHelper($phpExcelObject, $em,$this->getDoctrine()->getManager(), $projectId);
                    if ($lsHelper->isValidLoadSheet()){

                        $excelHeader = $lsHelper->getHeader();

                        $dataList= $lsHelper->readLoadSheetDataRows($lsHelper->getDataSheetHeaderRowsCount() + 1, $lsHelper->getDataSheetHeaderColumnsCount());

                        $fileInfo['rowsCount']=count($dataList['data']);
                        $fileInfo['status']=$dataList['dataStatus'];

                        $validationArray=$lsHelper->getValidationArray();

                        $insertform = $this->createInsertLoadSheetForm($projectId, $submitForm, $dataList, $validationArray,  $excelHeader, $fileInfo);
                    }else{
                        $isValidLoadSheetFormat=false;
                    }
                    $isReadFile=1;
                }
            } catch (\Exception $e) {
                $this->addFlash('error', 'Unexpected error' . $e->getMessage());

            }
        }

        $insertform->handleRequest($request);
        if ($insertform->isSubmitted() && $insertform->isValid()) {

            $data = $insertform->getData();
            $fileInfo = json_decode($data['fileInfo'],true);
            $excelHeader= json_decode($data['headersObject'],true);
            $dataList= json_decode($data['dataObject'],true);
            $validationData= json_decode($data['validationObject'],true);
            $isValidLoadSheetFormat=true;
            $isReadFile=1;


            $dataList = $this->InsertPipeline($dataList,$validationData,$project,$em,$this->getDoctrine()->getManager());

            $fileInfo['status'] = $dataList['dataStatus'];
            if($dataList['dataStatus'] == 3)
                $this->addFlash('error', 'The loadsheet has not been submitted');
            else
                $this->addFlash('success', 'The loadsheet has been submitted.');
        }
        return [
            'fileInfo'=>$fileInfo,
            'excelHeaderData'=>$excelHeader,
            'excelData'=>$dataList,
            'hasFile'=>$isReadFile,
            'validExcelFormat'=>$isValidLoadSheetFormat,
            'project' =>$project,
            'form'=>$form->createView(),
            'download_form'=>$downloadform->createView(),
            'insert_form'=>$insertform->createView()
        ];
    }

    /**
     * @param $projectId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createDownloadForm($projectId){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('pplDownload_loadsheet', array('projectId' => $projectId)))
            ->setMethod('GET')
            ->add('download', 'submit', $this->options(['label' => 'Download Master Sheet', 'attr' => ['class' => 'btn btn-success btn-block']], 'btn'))
            ->getForm();
        return $downloadform;

    }

    /**
     * download loadsheet.
     *
     * @Route("/downloadloadsheet", name="pplDownload_loadsheet")
     * @Method("GET")
     * @Template()
     */
    public function downloadloadsheetAction($projectId) {
        $excelObj = $this->get('phpexcel')->createPHPExcelObject();
        $lsHelper = new LoadsheetHelper($excelObj, $this->getManager(),$this->getDoctrine()->getManager() , $projectId);
        $excelObj = $lsHelper->downloadExcelLoadsheet();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="PipelineDetailsLoadsheet_' . $projectId .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = $this->get('phpexcel')->createWriter($excelObj, 'Excel2007');
        $writer->save('php://output');
        exit;
    }


    /**
     * @param $projectId
     * @param $submitForm
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInsertLoadSheetForm($projectId, $submitForm, $dataObject, $validationObject, $headersObject, $fileInfo){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl($submitForm, array('projectId' => $projectId)))
            ->setMethod('POST')
            ->add('insert', 'submit', $this->options(['label' => 'Submit Pipeline Details Sheet', 'attr' => ['class' => 'btn btn-success btn-block call-loader']], 'btn'))
            ->add('dataObject', 'hidden', array('data' => json_encode($dataObject)))
            ->add('validationObject', 'hidden', array('data' => json_encode($validationObject)))
            ->add('headersObject', 'hidden', array('data' => json_encode($headersObject)))
            ->add('fileInfo', 'hidden', array('data' => json_encode($fileInfo)))
            ->getForm();
        return $downloadform;

    }


    private function createMasterLoadSheetForm($projectId, $project, $submitForm) {
        $requestParameters = ['projectId' => $projectId,];

        $form = $this->createForm(new MasterLoadSheetType($project),null, [
            'action' => $this->generateUrl($submitForm,
                $requestParameters),
            'method' => 'POST',
        ]);
        $form = $form
            ->add('submit', 'submit', $this->options(['label' => 'Upload', 'attr' => ['class' => 'btn btn-primary btn-block call-loader']], 'btn'));
        return $form;
    }

    public function InsertPipeline($items, $validationObj, $project, $em, $defaultEm){

            $hasFailure=false;
            $dataIndex=0;

            foreach($items['data'] as $data){
                $values=[];
                $index=0;
                foreach ($data['item'] as $item){
//                    $pipeline = new Pipelines();
//                    $pipeline->setProject($project);
//                    if($validationObj[$index]['type']=='date' && !empty($item['value'])) {
//                        $val=\DateTime::createFromFormat("Y-m-d",$item['value']);
//                        if ($val instanceof \DateTime) {
//                            $values[$validationObj[$index]['name']]=$val;
//                        }else{
//                            $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);
//                        }
//                    }else{
                        $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);

//                    }
                    $index++;
                }

                    $entity= new Pipelines();
            try{

                $entity->setProject($project);

                $entity->setName($values['pipelineName']);
                $entity->setDescription($values['description']);
                $entity->setBatteryLimits($values['batteryLimits']);
                $entity->setFolder( md5(uniqid(rand(), true)) );

                $specifications = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSpecifications')->findAll();
                $entity->setPipelineSpecifications($specifications);

                $em->persist($entity);
                $em->flush();

                $pipelineId = $entity->getId();

                $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findByPipeline($pipelineId);
                $specCount = 0; // total 21 specs
                foreach ($entities as $e)
                {
                    $id = $specifications[$specCount]->getId();

                    $e->setValue($values["specValue" . $id]);
                    if ($id !== 9) //Year of commissioning does not have unit or comment
                    {
                    $e->setComment($values["specComment" . $id]);
                    if ($id == 6 || $id == 8 || $id == 15 || $id == 16 || $id == 17 || $id == 18 || $id == 20) //only these spec ids have units
                    $e->setUnit($values["specUnit" . $id]);
                    }

                    $em->persist($e);

                    $specCount++;
                }




                $em->flush();

                $hasFailure=false;
                $items['data'][$dataIndex]['itemStatus']=2;
                }



//                }else{
//                    $hasFailure=true;
//                    $items['data'][$dataIndex]['itemStatus']=3;
//                }
//                catch (Exception $e) {
//            $this->get('session')->setFlash('flash_key',"Add not done: " . $e->getMessage());
//        }
                catch (\Exception $e) {
                    $this->addFlash('error', 'Unexpected error' . $e->getMessage());

                    $hasFailure=true;
                    $items['data'][$dataIndex]['itemStatus']=3;
                    $items['dataStatus']=3;
                    $em->clear();
                    return $items;
                }
                $dataIndex++;
            }

            if ($hasFailure){
            $items['dataStatus']=3;
        }else{
            $items['dataStatus']=2;
        }
        $em->clear();
        return $items;
    }

//    public function getArrayItemByKey($value, $array,$key, $returnKey,$parentKey=null,$iskeyValue=false){
//        if (!$iskeyValue){
//            if ($parentKey==null){
//                foreach ($array as $obj){
//                    if (strtolower($obj[$key])==strtolower($value)){
//                        return $obj[$returnKey];
//                    }
//                }
//            }else{
//                foreach ($array[$parentKey] as $obj){
//                    if (strtolower($obj[$key])==strtolower($value)){
//                        return $obj[$returnKey];
//                    }
//                }
//            }
//        }else{
//            if ($parentKey==null){
//                foreach ($array as $k => $obj){
//                    if (strtolower($obj)==strtolower($value)){
//                        return $k;
//                    }
//                }
//            }else{
//                foreach ($array[$parentKey] as $k => $obj){
//                    if (strtolower($obj)==strtolower($value)){
//                        return $k;
//                    }
//                }
//            }
//        }
//
//
//        return null;
//    }

    public function returnValue($field){
        if ($field === 0 || $field === 0.0)
            return $field;
        elseif ($field==null || $field=="" || empty($field)){
            return null;
        }
        else return $field;
    }

    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

// Start of second loadsheet code - Mitigation status assessment

    /**
     * Loadsheet upload/ download for mitigation status details
     *
     * @Route("/assessmentdetails", name="loadsheet_assessment")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function loadsheetAssessmentAction(Request $request, $projectId){

        return $this->uploadAssessLoadSheetHandler($request, $projectId, $request->get('_route'));
    }

    public function uploadAssessLoadSheetHandler(Request $request, $projectId, $submitForm) {
        $isReadFile =0;
        $dataList=[];
        $excelHeader=null;
        $isValidLoadSheetFormat=true;
        $fileInfo=['name'=>'','ext'=>'' ,'size'=>'','rowsCount'=>0,'status'=>0];
        $em = $this->getManager();
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
        if (! $project) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Data Missing','message' => "Unable to find Projects entity."));

//            throw $this->createNotFoundException('Unable to find Projects entity.');
        }
        $form = $this->createMasterLoadSheetForm($projectId, $project, $submitForm);
        $form->handleRequest($request);
        $downloadform = $this->createAssessmentDownloadForm($projectId);
        $insertform = $this->createInsertAssessmentLoadSheetForm($projectId, $submitForm, null, null,null, null);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $file = $data['loadsheetfile'];
                if ($file->getMimeType() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    $this->addFlash('warning', 'File format must be xlsx (Excel 2007 or later)');
                    return $this->redirect(
                        $this->generateUrl('loadsheet_assessment', ['projectId' => $projectId])
                    );
                }else{
                    $fileInfo['name']=$file->getClientOriginalName();
                    $fileInfo['ext']=$file->guessExtension();
                    $fileInfo['size']=$this->formatSizeUnits($file->getClientSize());
                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file);
                    $lsHelper = new LoadsheetAssessmentHelper($phpExcelObject, $em,$this->getDoctrine()->getManager(), $projectId);
                    if ($lsHelper->isValidLoadSheet()){

                        $excelHeader = $lsHelper->getHeader();

                        $dataList= $lsHelper->readLoadSheetDataRows($lsHelper->getDataSheetHeaderRowsCount() + 1, $lsHelper->getDataSheetHeaderColumnsCount());

                        $fileInfo['rowsCount']=count($dataList['data']);
                        $fileInfo['status']=$dataList['dataStatus'];

                        $validationArray=$lsHelper->getValidationArray();

                        $insertform = $this->createInsertAssessmentLoadSheetForm($projectId, $submitForm, $dataList, $validationArray,  $excelHeader, $fileInfo);
                    }else{
                        $isValidLoadSheetFormat=false;

                    }
                    $isReadFile=1;
                }
            } catch (\Exception $e) {
                $this->addFlash('error', 'Unexpected error' . $e->getMessage());

            }
        }

        $insertform->handleRequest($request);
        if ($insertform->isSubmitted() && $insertform->isValid()) {

            $data = $insertform->getData();
            $fileInfo = json_decode($data['fileInfo'],true);
            $excelHeader= json_decode($data['headersObject'],true);
            $dataList= json_decode($data['dataObject'],true);
            $validationData= json_decode($data['validationObject'],true);
            $isValidLoadSheetFormat=true;
            $isReadFile=1;


            $dataList = $this->InsertPipelineAssessment($dataList,$validationData,$project,$em,$this->getDoctrine()->getManager());

            $fileInfo['status'] = $dataList['dataStatus'];
            if($dataList['dataStatus'] == 3)
                $this->addFlash('error', 'The loadsheet has not been submitted');
            else
                $this->addFlash('success', 'The loadsheet has been submitted.');
        }

        return [
            'fileInfo'=>$fileInfo,
            'excelHeaderData'=>$excelHeader,
            'excelData'=>$dataList,
            'hasFile'=>$isReadFile,
            'validExcelFormat'=>$isValidLoadSheetFormat,
            'project' =>$project,
            'form'=>$form->createView(),
            'download_form'=>$downloadform->createView(),
            'insert_form'=>$insertform->createView()
        ];
    }

    public function createAssessmentDownloadForm($projectId){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('Download_assess_loadsheet', array('projectId' => $projectId)))
            ->setMethod('GET')
            ->add('download', 'submit', $this->options(['label' => 'Download Mitigation Status Load Sheet', 'attr' => ['class' => 'btn btn-success btn-block']], 'btn'))
            ->getForm();
        return $downloadform;

    }

    /**
     * download loadsheet.
     *
     * @Route("/downloadAssessmentLoadsheet", name="Download_assess_loadsheet")
     * @Method("GET")
     * @Template()
     */
    public function downloadAssessmentloadsheetAction($projectId) {
        $excelObj = $this->get('phpexcel')->createPHPExcelObject();

        $lsHelper = new LoadsheetAssessmentHelper($excelObj, $this->getManager(),$this->getDoctrine()->getManager() , $projectId);
        $excelObj = $lsHelper->downloadExcelLoadsheet();

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="PipelineMitigationStatusLoadsheet_' . $projectId .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = $this->get('phpexcel')->createWriter($excelObj, 'Excel2007');
        $writer->save('php://output');
        exit;
    }


    /**
     * @param $projectId
     * @param $submitForm
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInsertAssessmentLoadSheetForm($projectId, $submitForm, $dataObject, $validationObject, $headersObject, $fileInfo){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl($submitForm, array('projectId' => $projectId)))
            ->setMethod('POST')
            ->add('insert', 'submit', $this->options(['label' => 'Submit Mitigation Status Sheet', 'attr' => ['class' => 'btn btn-success btn-block call-loader']], 'btn'))
            ->add('dataObject', 'hidden', array('data' => json_encode($dataObject)))
            ->add('validationObject', 'hidden', array('data' => json_encode($validationObject)))
            ->add('headersObject', 'hidden', array('data' => json_encode($headersObject)))
            ->add('fileInfo', 'hidden', array('data' => json_encode($fileInfo)))
            ->getForm();
        return $downloadform;

    }

    public function InsertPipelineAssessment($items, $validationObj, $project, $em, $defaultEm){

        $hasFailure=false;
        $dataIndex=0;

        foreach($items['data'] as $data){
            $values=[];
            $index=0;
            foreach ($data['item'] as $item){
//                    $pipeline = new Pipelines();
//                    $pipeline->setProject($project);
                    if($validationObj[$index]['type']=='date' && !empty($item['value'])) {
                        $val=\DateTime::createFromFormat("Y-m-d",$item['value']);
                        if ($val instanceof \DateTime) {
                            $values[$validationObj[$index]['name']]=$val;
                        }else{
                            $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);
                        }
                    }else{
                $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);

                    }
                $index++;
            }

            try{
                $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->findOneByName($values['pipelineName']);

                If (isSet($pipeline) and !empty($pipeline)){
                $section = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findOneBy(array('pipeline'=>$pipeline,'section'=>$values['sectionName']));
                    If (isSet($section) == False or empty($section)) { //If section is already defined, don't update it. It caters to both cases, if a previous excel row had the same section name as well as if software already had section created.

                        $section = new PipelineSections();

                        $section->setPipeline($pipeline);

                        $section->setSection($values['sectionName']);
                        $section->setX($values['startKP']);
                        $section->setY($values['endKP']);

                        $em->persist($section);
                        $em->flush();

                        }
                    $sectionId = $section->getId();

                    $threat = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findOneByName($values['threat']);
                    If (isSet($threat) == False or empty($threat)) {
                        $this->addFlash('error', 'Threat does not exist.');
                    }
                    else {


                    $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findOneBy(array('pipelineSections'=>$sectionId, 'threats'=>$threat));
                    If (isSet($sectionThreat) == False or empty($sectionThreat)) { //if threat not already assigned, then do it
                        $sectionThreat = new SectionsThreats();

                        $sectionThreat->setPipelineSections($section);
                        $sectionThreat->setThreats($threat);

                        $em->persist($sectionThreat);
                        $em->flush();

                    }

                    }
                    $sectionThreatId = $sectionThreat->getId();

                    $mitigationTechnique = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->findOneBy(array('mitigationTechnique'=>$values['mitigationTechnique'],'project'=>$project));
                    If (isSet($mitigationTechnique) == False or empty($mitigationTechnique)) {
                        $this->addFlash('error', "Mitigation Technique has not been defined for this project.");
                    }
                    else
                    {
                        $mitigationTechniqueId = $mitigationTechnique->getId();
                        $sectionsThreatsMitigation = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findOneBy(array('sectionsThreats'=>$sectionThreatId,'mitigationTechnique'=>$mitigationTechniqueId));
                        If (isSet($sectionsThreatsMitigation) == False or empty($sectionsThreatsMitigation)) { // to check if the mitigation technique is already defined, then only update the other parameters such as Last insp date, etc.
                            $sectionsThreatsMitigation = new SectionsThreatsMitigations();

                            $sectionsThreatsMitigation->setSectionsThreats($sectionThreat);
                            $sectionsThreatsMitigation->setMitigationTechnique($mitigationTechnique);

                            }

                        $sectionsThreatsMitigation->setLastInspectionDate($values['lastInspectionDate']);
                        $sectionsThreatsMitigation->setPerformanceStandard($values['performanceStandard']);

//                        if (is_array($values['mitigationStatus'])){
//                            $values['mitigationStatus'] = implode("\r\n" ,$values['mitigationStatus']);
//                        }

                        $sectionsThreatsMitigation->setMitigationStatus($values['mitigationStatus']);
                        $sectionsThreatsMitigation->setComments($values['comments']);
                        $sectionsThreatsMitigation->setRecommendations($values['recommendations']);
                        $sectionsThreatsMitigation->setDate(new \DateTime("now"));

                        $em->persist($sectionsThreatsMitigation);
                        $em->flush();
                    }


                $hasFailure=false;
                $items['data'][$dataIndex]['itemStatus']=2;
            }
              else{
                  $this->addFlash('error', "The pipeline does not exist. Please create the pipeline first.");
                }
            }
//                catch (Exception $e) {
//            $this->get('session')->setFlash('flash_key',"Add not done: " . $e->getMessage());
//        }
            catch (\Exception $e) {
                $this->addFlash('error', 'Unexpected error' . $e->getMessage());
                $hasFailure=true;
                $items['data'][$dataIndex]['itemStatus']=3;
                $items['dataStatus']=3;
                $em->clear();
                return $items;
            }
            $dataIndex++;
        }

        if ($hasFailure){
            $items['dataStatus']=3;
        }else{
            $items['dataStatus']=2;
        }
        $em->clear();
        return $items;
    }

// Start of third loadsheet code - Risk assessment

    /**
     * Loadsheet upload/ download for risk assessment details
     *
     * @Route("/pof_cof_details", name="loadsheet_risk")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function loadsheetRiskAssessmentAction(Request $request, $projectId){

        return $this->uploadRiskAssessLoadSheetHandler($request, $projectId, $request->get('_route'));
    }

    public function uploadRiskAssessLoadSheetHandler(Request $request, $projectId, $submitForm) {
        $isReadFile =0;
        $dataList=[];
        $excelHeader=null;
        $isValidLoadSheetFormat=true;
        $fileInfo=['name'=>'','ext'=>'' ,'size'=>'','rowsCount'=>0,'status'=>0];
        $em = $this->getManager();
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
        if (! $project) {
            return $this->render('AIEVeracityBundle:Default:customMessage.html.twig', array( 'type' => ' - Data Missing','message' => "Unable to find Projects entity."));

//            throw $this->createNotFoundException('Unable to find Projects entity.');
        }



        $form = $this->createMasterLoadSheetForm($projectId, $project, $submitForm);
        $form->handleRequest($request);
        $downloadform = $this->createRiskAssessmentDownloadForm($projectId);
        $insertform = $this->createInsertRiskAssessmentLoadSheetForm($projectId, $submitForm, null, null,null, null);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            try {
                $file = $data['loadsheetfile'];
                if ($file->getMimeType() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    $this->addFlash('warning', 'File format must be xlsx (Excel 2007 or later)');
                    return $this->redirect(
                        $this->generateUrl('loadsheet_assessment', ['projectId' => $projectId])
                    );
                }else{

                    $this->dynamicGenerationOfJsonFile($projectId);

                    $fileInfo['name']=$file->getClientOriginalName();
                    $fileInfo['ext']=$file->guessExtension();
                    $fileInfo['size']=$this->formatSizeUnits($file->getClientSize());
                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($file);
                    $lsHelper = new LoadsheetRiskAssessmentHelper($phpExcelObject, $em,$this->getDoctrine()->getManager(), $projectId);
//                echo("is valid: " . $lsHelper->isValidLoadSheet() . "?");
                    if ($lsHelper->isValidLoadSheet()){

                        $excelHeader = $lsHelper->getHeader();

                        $dataList= $lsHelper->readLoadSheetDataRows($lsHelper->getDataSheetHeaderRowsCount() + 1, $lsHelper->getDataSheetHeaderColumnsCount());

                        $fileInfo['rowsCount']=count($dataList['data']);
                        $fileInfo['status']=$dataList['dataStatus'];

                        $validationArray=$lsHelper->getValidationArray();

                        $insertform = $this->createInsertRiskAssessmentLoadSheetForm($projectId, $submitForm, $dataList, $validationArray,  $excelHeader, $fileInfo);
                    }else{
                        $isValidLoadSheetFormat=false;

                    }
                    $isReadFile=1;
                }
            } catch (\Exception $e) {
                $this->addFlash('error', 'Unexpected error' . $e->getMessage());

            }
        }

        $insertform->handleRequest($request);
        if ($insertform->isSubmitted() && $insertform->isValid()) {

            $data = $insertform->getData();
            $fileInfo = json_decode($data['fileInfo'],true);
            $excelHeader= json_decode($data['headersObject'],true);
            $dataList= json_decode($data['dataObject'],true);
            $validationData= json_decode($data['validationObject'],true);
            $isValidLoadSheetFormat=true;
            $isReadFile=1;


            $dataList = $this->InsertPipelineRiskAssessment($dataList,$validationData,$project,$em,$this->getDoctrine()->getManager());

            $fileInfo['status'] = $dataList['dataStatus'];
            if($dataList['dataStatus'] == 3)
                $this->addFlash('error', 'The loadsheet has not been submitted');
            else
                $this->addFlash('success', 'The loadsheet has been submitted.');
        }
        return [
            'fileInfo'=>$fileInfo,
            'excelHeaderData'=>$excelHeader,
            'excelData'=>$dataList,
            'hasFile'=>$isReadFile,
            'validExcelFormat'=>$isValidLoadSheetFormat,
            'project' =>$project,
            'form'=>$form->createView(),
            'download_form'=>$downloadform->createView(),
            'insert_form'=>$insertform->createView()
        ];
    }

    public function createRiskAssessmentDownloadForm($projectId){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl('Download_riskAssess_loadsheet', array('projectId' => $projectId)))
            ->setMethod('GET')
            ->add('download', 'submit', $this->options(['label' => 'Download POF/ COF Details Load Sheet', 'attr' => ['class' => 'btn btn-success btn-block']], 'btn'))
            ->getForm();
        return $downloadform;

    }

    public function dynamicGenerationOfJsonFile($projectId)
    {
        //        //Code to add the relevant consequence categories to the json file (PipelineAssessLoadsheetSource.json) dynamically. So that only the consequences selected in the project will appear. If new consequences are added, they will automatically come to load sheet template.
//        // Get the consequence categories for the current project
        $em = $this->getManager();
        $conseqCategoryArray = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findByProject($projectId);
        If (isSet($conseqCategoryArray) and !empty($conseqCategoryArray)) {

            // open the json file and decode it so that we can push the conseq categories
            $inp = file_get_contents('../web/assets/js/pipelineRiskAssessLoadsheetSource.json');
            $tempArray = json_decode($inp,true);


            // to avoid duplication, first remove all elements with consequence of failure
            if (sizeof($tempArray['columns']) > 5) {

                $total_conseqCats = sizeof($tempArray['columns']) - 5;
                for ($index = 0 ; $index < $total_conseqCats ; $index++){
                     array_pop($tempArray['columns']);

                }
            }


            // Add in the conseq categories
            $column =[];
            $count = 0;
            foreach ($conseqCategoryArray as $cat){

                $column[$count]["id"] = $cat->getConsequence()->getName();
                $column[$count]["name"] = "Consequence of Failure - " . $cat->getConsequence()->getName();
                $column[$count]["hasChild"] = false;
                $column[$count]["type"] = "list";
                $column[$count]["mandatory"] = "1";
                $column[$count]["source"] = [1, 2, 3, 4, 5];

                $column[$count]["tableInfo"]["rowNumber"] = 1;
                $column[$count]["tableInfo"]["colNumber"] = 6 + $count; // 6 is the static column number
                $column[$count]["tableInfo"]["rowSpan"] = 1;
                $column[$count]["tableInfo"]["colSpan"] = 1;



                array_push($tempArray['columns'], $column[$count]);

                $count++;
            }

            $tempArray['sheets']['dataSheet']['style']['header']['headersCount'] = $count + 5; // this is for the borders dynamically

            // re encode and write to the json file
            $jsonData = json_encode($tempArray);
            file_put_contents('../web/assets/js/pipelineRiskAssessLoadsheetSource.json', $jsonData);

        }
        else
        {
            $this->addFlash('error', 'Consequences are not assigned to this Project.');
            exit;
        }

        // end of change
    }

    /**
     * download loadsheet.
     *
     * @Route("/downloadRiskAssessmentLoadsheet", name="Download_riskAssess_loadsheet")
     * @Method("GET")
     * @Template()
     */
    public function downloadRiskAssessmentloadsheetAction($projectId) {


        $excelObj = $this->get('phpexcel')->createPHPExcelObject();
        $this->dynamicGenerationOfJsonFile($projectId);
        $lsHelper = new LoadsheetRiskAssessmentHelper($excelObj, $this->getManager(),$this->getDoctrine()->getManager() , $projectId);
        $excelObj = $lsHelper->downloadExcelLoadsheet();

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="POF_COFAssessmentLoadsheet_' . $projectId .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = $this->get('phpexcel')->createWriter($excelObj, 'Excel2007');
        $writer->save('php://output');
        exit;


    }


    /**
     * @param $projectId
     * @param $submitForm
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInsertRiskAssessmentLoadSheetForm($projectId, $submitForm, $dataObject, $validationObject, $headersObject, $fileInfo){
        $downloadform = $this->createFormBuilder()
            ->setAction($this->generateUrl($submitForm, array('projectId' => $projectId)))
            ->setMethod('POST')
            ->add('insert', 'submit', $this->options(['label' => 'Submit Assessment Details Sheet', 'attr' => ['class' => 'btn btn-success btn-block call-loader']], 'btn'))
            ->add('dataObject', 'hidden', array('data' => json_encode($dataObject)))
            ->add('validationObject', 'hidden', array('data' => json_encode($validationObject)))
            ->add('headersObject', 'hidden', array('data' => json_encode($headersObject)))
            ->add('fileInfo', 'hidden', array('data' => json_encode($fileInfo)))
            ->getForm();
        return $downloadform;

    }

    public function InsertPipelineRiskAssessment($items, $validationObj, $project, $em, $defaultEm){

        $hasFailure=false;
        $dataIndex=0;

        foreach($items['data'] as $data){
            $values=[];
            $index=0;
            foreach ($data['item'] as $item){
//                    $pipeline = new Pipelines();
//                    $pipeline->setProject($project);
                if($validationObj[$index]['type']=='date' && !empty($item['value'])) {
                    $val=\DateTime::createFromFormat("Y-m-d",$item['value']);
                    if ($val instanceof \DateTime) {
                        $values[$validationObj[$index]['name']]=$val;
                    }else{
                        $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);
                    }
                }else{
                    $values[$validationObj[$index]['name']]=$this->returnValue($item['value']);

                }
                $index++;
            }

            try{

                $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->findOneBy(array('name'=>$values['pipelineName'],'project'=>$project));
                If (isSet($pipeline) == False or empty($pipeline)) {
                    $this->addFlash('error', 'Pipeline does not exist.');
                    exit;
                }

                $section = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findOneBy(array('section'=>$values['sectionName'],'pipeline'=>$pipeline));
                If (isSet($section) == False or empty($section)) {
                    $this->addFlash('error', 'Section does not exist.');
                    exit;
                }

                $sectionId = $section->getId();

                $threat = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findOneByName($values['threatName']);

               $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findOneBy(array('pipelineSections'=>$sectionId, 'threats'=>$threat));
               If (isSet($sectionThreat) == False or empty($sectionThreat)) {
                   $this->addFlash('error', 'Threat does not exist.');
                   exit;
                   }

                $sectionThreat->setProbabilityOfFailer($values['pof']);

                if ($values['confidence'] == 'Low')
                $sectionThreat->setConfidence(0);
                elseif ($values['confidence'] == 'Medium')
                    $sectionThreat->setConfidence(1);
                elseif ($values['confidence'] == 'High')
                    $sectionThreat->setConfidence(2);
                else {
                    $this->addFlash('error', 'Confidence is invalid.');
                    exit;
                }

                //calculate adjusted pof
                $adj = $values['pof'];
                if ($values['confidence'] == 'Low' && $adj < 5)
                    $adj++;
                $sectionThreat->setAdjProbabilityOfFailer($adj);

                $em->persist($sectionThreat);
                $em->flush();

                $sectionThreatId = $sectionThreat->getId();

                $projectConseq = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project'=>$project));
                If (isSet($sectionThreat) == False or empty($sectionThreat)) {
                    $this->addFlash('error', 'Consequences are not assigned to this Project.');
                    exit;
                }


                foreach ($projectConseq as $cat){

                $conseqAssess = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findOneBy(array('sectionsThreats'=>$sectionThreat,'consequence'=>$cat));
                If (isSet($conseqAssess) == False or empty($conseqAssess)) { //Insert case
                    $conseqAssess = new ConsequenceAssessment();
                    $conseqAssess->setSectionsThreats($sectionThreat);
                }
                    // Whether Insert or Update, set the consequences and severities
                    $conseqAssess->setConsequence($cat);
                    $conseqAssess->setServerity($values[$cat->getConsequence()->getName()]);

                    $em->persist($conseqAssess);
                }


                $em->flush();


                    $hasFailure=false;
                    $items['data'][$dataIndex]['itemStatus']=2;
                }


//                catch (Exception $e) {
//            $this->get('session')->setFlash('flash_key',"Add not done: " . $e->getMessage());
//        }
            catch (\Exception $e) {
                $this->addFlash('error', 'Unexpected error' . $e->getMessage());

                $hasFailure=true;
                $items['data'][$dataIndex]['itemStatus']=3;
                $items['dataStatus']=3;
                $em->clear();
                return $items;
            }
            $dataIndex++;
        }

        if ($hasFailure){
            $items['dataStatus']=3;
        }else{
            $items['dataStatus']=2;
        }
        $em->clear();
        return $items;
    }


}