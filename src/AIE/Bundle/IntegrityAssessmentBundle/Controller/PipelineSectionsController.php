<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineSectionsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * PipelineSections controller.
 *
 * @Route("pipelines/{pipelineId}/sections")
 */
class PipelineSectionsController extends BaseController {

    /**
     * Lists all PipelineSections entities.
     *
     * @Route("/", name="pipelinesections")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findByPipeline($pipelineId);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        return array(
            'entities' => $entities,
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline
        );
    }

    /**
     * Creates a new PipelineSections entity.
     *
     * @Route("/", name="pipelinesections_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:PipelineSections:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId) {

        $em = $this->getDoctrine()->getManager();

        $sections = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findByPipeline($pipelineId);


        $entity = new PipelineSections();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $pipelineSpecification = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findOneBy(array('pipeline' => $pipelineId, 'specification' => 1));
        if (!$pipelineSpecification || !$pipelineSpecification->getValue()) {
            return $this->redirect($this->generateUrl('pipelinesspecifications', array('pipelineId' => $pipelineId)));
        }

        $length = $pipelineSpecification->getValue();

        $entity->setPipeline($pipeline);

        $form = $this->createCreateForm($entity, $pipelineId, $length);

        $form->handleRequest($request);

        if ($form->isValid()) {
            
            //exit();
            /*
              $range = $request->request->get('sectionRange');
              $range = explode(';', $range);
              if ($range) {
              $entity->setX($range[0]);
              $entity->setY($range[1]);
              } */
            $em = $this->getDoctrine()->getManager();

            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);

            $entity->setPipeline($pipeline);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('pipelinesections_new', array('pipelineId' => $pipelineId)));
        }

        return array(
            'sections' => $sections,
            'entity' => $entity,
            'form' => $form->createView(),
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline,
            'length' => $length
        );
    }

    /**
     * Creates a form to create a PipelineSections entity.
     *
     * @param PipelineSections $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PipelineSections $entity, $pipelineId, $length) {

        $em = $this->getManager();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $entity->setPipeline($pipeline);

        $form = $this->createForm(new PipelineSectionsType($entity, $length), $entity, array(
            'action' => $this->generateUrl('pipelinesections_create', array('pipelineId' => $pipelineId)),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new PipelineSections entity.
     *
     * @Route("/new", name="pipelinesections_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($pipelineId) {
        $em = $this->getDoctrine()->getManager();

        $sections = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findByPipeline($pipelineId);


        $entity = new PipelineSections();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $pipelineSpecification = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findOneBy(array('pipeline' => $pipelineId, 'specification' => 1));
        if (!$pipelineSpecification || !$pipelineSpecification->getValue()) {
            return $this->redirect($this->generateUrl('pipelinesspecifications', array('pipelineId' => $pipelineId)));
        }

        $length = $pipelineSpecification->getValue();
        
        $form = $this->createCreateForm($entity, $pipelineId, $length);



        return array(
            'sections' => $sections,
            'entity' => $entity,
            'form' => $form->createView(),
            'pipelineId' => $pipelineId,
            'pipeline' => $pipeline,
            'length' => $length
        );
    }

    /**
     * Finds and displays a PipelineSections entity.
     *
     * @Route("/{id}", name="pipelinesections_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($pipelineId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            //  'delete_form' => $deleteForm->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Displays a form to edit an existing PipelineSections entity.
     *
     * @Route("/{id}/edit", name="pipelinesections_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($pipelineId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($id);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $pipelineSpecification = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findOneBy(array('pipeline' => $pipelineId, 'specification' => 1));
        if (!$pipelineSpecification || !$pipelineSpecification->getValue()) {
            return $this->redirect($this->generateUrl('pipelinesspecifications', array('pipelineId' => $pipelineId)));
        }

        $length = $pipelineSpecification->getValue();
        $entity->setPipeline($pipeline);
        
        $editForm = $this->createEditForm($entity, $pipelineId, $length);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'pipelineId' => $pipelineId,
            'length' => $length
        );
    }

    /**
     * Creates a form to edit a PipelineSections entity.
     *
     * @param PipelineSections $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PipelineSections $entity, $pipelineId, $length) {
        $form = $this->createForm(new PipelineSectionsType($entity, $length), $entity, array(
            'action' => $this->generateUrl('pipelinesections_update', array('id' => $entity->getId(), 'pipelineId' => $pipelineId)),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing PipelineSections entity.
     *
     * @Route("/{id}", name="pipelinesections_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:PipelineSections:edit.html.twig")
     */
    public function updateAction(Request $request, $pipelineId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($id);
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        //$this->setLeftTitleFromPipeline($pipeline);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }


        $pipelineSpecification = $em->getRepository('AIEIntegrityAssessmentBundle:PipelinesSpecifications')->findOneBy(array('pipeline' => $pipelineId, 'specification' => 1));
        if (!$pipelineSpecification || !$pipelineSpecification->getValue()) {
            return $this->redirect($this->generateUrl('pipelinesspecifications', array('pipelineId' => $pipelineId)));
        }

        $length = $pipelineSpecification->getValue();
        $entity->setPipeline($pipeline);

        $editForm = $this->createEditForm($entity, $pipelineId, $length);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pipelinesections', array('id' => $id, 'pipelineId' => $pipelineId)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'pipelineId' => $pipelineId,
            'length' => $length
        );

    }

    /**
     * Displays a form to edit an existing PipelineSections entity.
     *
     * @Route("/{id}/delete", name="pipelinesections_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction($pipelineId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PipelineSections entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $deleteForm = $this->createDeleteForm($entity->getId(), $pipelineId);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Deletes a PipelineSections entity.
     *
     * @Route("/{id}", name="pipelinesections_post_delete")
     * @Method("DELETE")
     */
    public function postDeleteAction(Request $request, $id, $pipelineId) {
        $form = $this->createDeleteForm($id, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PipelineSections entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipelinesections', array('id' => $id, 'pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to delete a PipelineSections entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pipelinesections_post_delete', array('id' => $id,'pipelineId'=>$pipelineId)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger right hide')), 'btn'))
                        ->getForm()
        ;
    }

}
