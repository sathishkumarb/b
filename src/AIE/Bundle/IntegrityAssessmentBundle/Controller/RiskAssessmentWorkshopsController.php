<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops;
use AIE\Bundle\IntegrityAssessmentBundle\Form\RiskAssessmentWorkshopsType;

/**
 * RiskAssessmentWorkshops controller.
 *
 * @Route("/pipeline/{pipelineId}/workshops")
 */
class RiskAssessmentWorkshopsController extends BaseController {

    /**
     * Lists all RiskAssessmentWorkshops entities.
     *
     * @Route("/", name="workshops")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->findByPipeline($pipelineId);

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'   => $entities,
            'pipelineId' => $pipelineId,
            'project' => $pipeline->getProject(),
        );
    }

    /**
     * Creates a new RiskAssessmentWorkshops entity.
     *
     * @Route("/", name="workshops_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId)
    {
        $entity = new RiskAssessmentWorkshops();
        $form = $this->createCreateForm($entity, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
            $entity->setPipeline($pipeline);
            $data = $form->getData();
            $date = $data->getDate();
            if ($date)
            {
                $entity->setDate(new \DateTime($date));
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a form to create a RiskAssessmentWorkshops entity.
     *
     * @param RiskAssessmentWorkshops $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RiskAssessmentWorkshops $entity, $pipelineId)
    {
        $form = $this->createForm(new RiskAssessmentWorkshopsType($entity), $entity, array(
            'action' => $this->generateUrl('workshops_create', ['pipelineId' => $pipelineId]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new RiskAssessmentWorkshops entity.
     *
     * @Route("/new", name="workshops_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($pipelineId)
    {
        $entity = new RiskAssessmentWorkshops();
        $form = $this->createCreateForm($entity, $pipelineId);

        $em = $this->getDoctrine()->getManager();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Finds and displays a RiskAssessmentWorkshops entity.
     *
     * @Route("/{id}", name="workshops_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find RiskAssessmentWorkshops entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $deleteForm = $this->createDeleteForm($id, $pipelineId);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing RiskAssessmentWorkshops entity.
     *
     * @Route("/{id}/edit", name="workshops_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find RiskAssessmentWorkshops entity.');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $editForm = $this->createEditForm($entity, $pipelineId);
        $deleteForm = $this->createDeleteForm($id, $pipelineId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'pipelineId'  => $pipelineId,
        );
    }

    /**
     * Creates a form to edit a RiskAssessmentWorkshops entity.
     *
     * @param RiskAssessmentWorkshops $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(RiskAssessmentWorkshops $entity, $pipelineId)
    {
        $form = $this->createForm(new RiskAssessmentWorkshopsType($entity), $entity, array(
            'action' => $this->generateUrl('workshops_update', array('pipelineId' => $pipelineId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing RiskAssessmentWorkshops entity.
     *
     * @Route("/{id}", name="workshops_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find RiskAssessmentWorkshops entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $pipelineId);
        $editForm = $this->createEditForm($entity, $pipelineId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {

            $data = $editForm->getData();
            $date = $data->getDate();
            if ($date)
            {
                $entity->setDate(new \DateTime($date));
            }
            $em->flush();

            return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a RiskAssessmentWorkshops entity.
     *
     * @Route("/{id}", name="workshops_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $pipelineId)
    {
        $form = $this->createDeleteForm($id, $pipelineId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find RiskAssessmentWorkshops entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to delete a RiskAssessmentWorkshops entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('workshops_delete', array('pipelineId' => $pipelineId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm();
    }
}
