<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Sections controller.
 *
 * @Route("/ajax")
 */
class AjaxController extends BaseController {

    /**
     * Lists all Sections entities.
     *
     * @Route("/sections", name="ajax_sections")
     * @Method("GET")
     */
    public function listSectionsAction(Request $request) {

        if ($request->isXmlHttpRequest()) { // is it an Ajax request?
            $em = $this->getDoctrine()->getManager();

            $entities = $em->getRepository('AIEIntegrityAssessmentBundle:Sections')->findAll();
            $response = array();
            foreach ($entities as $entity) {
                $response[] = $entity->getName();
            }
            return new Response(json_encode($response));
        }

        return $this->redirect($this->generateUrl('sections'));
    }

    
    /**
     * Lists all Sections entities.
     *
     * @Route("/pipeline_files", name="ajax_pipeline_files")
     * @Method("POST")
     */
    public function listFilesActions(Request $request) {
        if ($request->isXmlHttpRequest()) { // is it an Ajax request?
            $em = $this->getDoctrine()->getManager();

            $pipelineId = $request->request->get('pipelineId');

            $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineFiles')->findByPipeline($pipelineId);
            $response = array();
            foreach ($entities as $entity) {
                $response[] = [
                    'id' => $entity->getId(),
                    'name' => $entity->getName()
                ];
            }
            return new Response(json_encode($response));
        }

        return $this->redirect($this->generateUrl('index'));
    }

}
