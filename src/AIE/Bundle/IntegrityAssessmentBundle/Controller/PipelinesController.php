<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelinesSpecificationsType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Pipelines controller.
 *
 * @Route("projects/{projectId}/pipelines")
 */
class PipelinesController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all Pipelines entities.
     *
     * @Route("/", name="pipelines")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($projectId) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->findByProject($projectId);
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);

        return array(
            'entities' => $entities,
            'projectId' => $projectId,
            'project' => $project,
        );
    }

    /**
     * Creates a new Pipelines entity.
     *
     * @Route("/", name="pipelines_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:Pipelines:new.html.twig")
     */
    public function createAction(Request $request, $projectId) {
        $pipeline = new Pipelines();

        $form = $this->createCreateForm($pipeline, $projectId);
        $form->handleRequest($request);

        if ($form->isValid()) {

            /*
              $fs = new Filesystem();

              try {
              $fs->mkdir( mt_rand(), 0700);
              } catch (IOExceptionInterface $e) {
              echo "An error occurred while creating your directory at " . $e->getPath();
              }
             */



            $em = $this->getDoctrine()->getManager();
            $specifications = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineSpecifications')->findAll();
            $pipeline->setPipelineSpecifications($specifications);

            $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
            $pipeline->setProject($project);
            
            $pipeline->setFolder( md5(uniqid(rand(), true)) );

            $em->persist($pipeline);
            $em->flush();

            return $this->redirect($this->generateUrl('pipelines_show', array('projectId' => $projectId, 'id' => $pipeline->getId())));
        }

        return array(
            'entity' => $pipeline,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a form to create a Pipelines entity.
     *
     * @param Pipelines $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pipelines $entity, $projectId) {

        $em = $this->getManager();
        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);
        $entity->setProject($project);

        $form = $this->createForm(new PipelinesType($entity), $entity, array(
            'action' => $this->generateUrl('pipelines_create', array('projectId' => $projectId)),
            'method' => 'POST'
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Pipelines entity.
     *
     * @Route("/new", name="pipelines_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($projectId) {

        $pipeline = new Pipelines();

        $form = $this->createCreateForm($pipeline, $projectId);

        return array(
            'entity' => $pipeline,
            'form' => $form->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Finds and displays a Pipelines entity.
     *
     * @Route("/{id}", name="pipelines_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($projectId, $id) {

        //return $this->redirect($this->generateUrl('pipelinesspecifications', array('pipelineId' => $id)));

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pipelines entity.');
        }

        $this->setLeftTitleFromPipeline($entity);

        $deleteForm = $this->createDeleteForm($projectId, $id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Displays a form to edit an existing Pipelines entity.
     *
     * @Route("/{id}/edit", name="pipelines_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($projectId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pipelines entity.');
        }

        $this->setLeftTitleFromPipeline($entity);

        $editForm = $this->createEditForm($entity, $projectId);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Creates a form to edit a Pipelines entity.
     *
     * @param Pipelines $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pipelines $entity, $projectId) {
        $form = $this->createForm(new PipelinesType($entity, $projectId), $entity, array(
            'action' => $this->generateUrl('pipelines_update', array('id' => $entity->getId(),'projectId'=>$projectId)),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing Pipelines entity.
     *
     * @Route("/{id}", name="pipelines_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:Pipelines:edit.html.twig")
     */
    public function updateAction(Request $request, $projectId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pipelines entity.');
        }

        $editForm = $this->createEditForm($entity, $projectId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pipelines_show', array('id' => $id,'projectId'=>$projectId)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Pipelines entity.
     *
     * @Route("/{id}/delete", name="pipelines_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction($projectId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pipelines entity.');
        }

        $deleteForm = $this->createDeleteForm($projectId, $id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'projectId' => $projectId
        );
    }

    /**
     * Deletes a Pipelines entity.
     *
     * @Route("/{id}", name="pipelines_post_delete")
     * @Method("DELETE")
     */
    public function postDeleteAction(Request $request, $projectId, $id) {
        $form = $this->createDeleteForm($projectId, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pipelines entity.');
            }

            $uploader = $this->get('storage.file_uploader');

            $uploader->delete($entity->getFolder());

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipelines', array('projectId' => $projectId)));
    }

    /**
     * Creates a form to delete a Pipelines entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($projectId, $id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pipelines_post_delete', array('id' => $id, 'projectId' => $projectId)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger right hide')), 'btn'))
                        ->getForm()
        ;
    }

}
