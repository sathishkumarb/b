<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequencesCategories;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ConsequencesCategoriesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * ConsequencesCategories controller.
 *
 * @Route("/consequencescategories")
 */
class ConsequencesCategoriesController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all ConsequencesCategories entities.
     *
     * @Route("/", name="consequencescategories")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new ConsequencesCategories entity.
     *
     * @Route("/", name="consequencescategories_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:ConsequencesCategories:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new ConsequencesCategories();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setDate(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('consequencescategories'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ConsequencesCategories entity.
     *
     * @param ConsequencesCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ConsequencesCategories $entity) {
        $form = $this->createForm(new ConsequencesCategoriesType(), $entity, array(
            'action' => $this->generateUrl('consequencescategories_create'),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new ConsequencesCategories entity.
     *
     * @Route("/new", name="consequencescategories_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new ConsequencesCategories();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ConsequencesCategories entity.
     *
     * @Route("/{id}", name="consequencescategories_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ConsequencesCategories entity.
     *
     * @Route("/{id}/edit", name="consequencescategories_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ConsequencesCategories entity.
     *
     * @param ConsequencesCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ConsequencesCategories $entity) {
        $form = $this->createForm(new ConsequencesCategoriesType(), $entity, array(
            'action' => $this->generateUrl('consequencescategories_update', array('id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing ConsequencesCategories entity.
     *
     * @Route("/{id}", name="consequencescategories_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:ConsequencesCategories:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('consequencescategories'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ConsequencesCategories entity.
     *
     * @Route("/{id}", name="consequencescategories_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequencesCategories')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ConsequencesCategories entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consequencescategories'));
    }

    /**
     * Creates a form to delete a ConsequencesCategories entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('consequencescategories_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
