<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup;
use AIE\Bundle\IntegrityAssessmentBundle\Form\UserProjectGroupType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\UserProjectGroupEditType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * UserProjectGroup controller.
 *
 * @Route("user/{userId}/projects")
 */
class UserProjectGroupController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all UserProjectGroup entities.
     *
     * @Route("/", name="userprojectgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($userId) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->findByUser($userId);

        $user = $em->getRepository('UserBundle:User')->find($userId);

        foreach ($entities as $entity) {
            $entity->form = $this->createDeleteForm($entity->getId(), $userId)->createView();
        }

        return array(
            'entities' => $entities,
            'userId' => $userId,
            'user' => $user
        );
    }

    /**
     * Creates a new UserProjectGroup entity.
     *
     * @Route("/", name="userprojectgroup_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:UserProjectGroup:new.html.twig")
     */
    public function createAction(Request $request, $userId) {
        $entity = new UserProjectGroup();
        $form = $this->createCreateForm($entity, $userId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('UserBundle:User')->find($userId);

            $entity->setUser($user);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('userprojectgroup', array('userId' => $userId)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a UserProjectGroup entity.
     *
     * @param UserProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UserProjectGroup $entity, $userId) {
        $form = $this->createForm(new UserProjectGroupType(), $entity, array(
            'action' => $this->generateUrl('userprojectgroup_create', array('userId' => $userId)),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new UserProjectGroup entity.
     *
     * @Route("/new", name="userprojectgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($userId) {
        $entity = new UserProjectGroup();
        $form = $this->createCreateForm($entity, $userId);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'userId' => $userId
        );
    }

    /**
     * Finds and displays a UserProjectGroup entity.
     *
     * @Route("/{id}", name="userprojectgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $userId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $userId);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UserProjectGroup entity.
     *
     * @Route("/{id}/edit", name="userprojectgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $userId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $editForm = $this->createEditForm($entity, $userId);
        $deleteForm = $this->createDeleteForm($id, $userId);
        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'userId' => $userId
        );
    }

    /**
     * Creates a form to edit a UserProjectGroup entity.
     *
     * @param UserProjectGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(UserProjectGroup $entity, $userId) {
        $form = $this->createForm(new UserProjectGroupEditType(), $entity, array(
            'action' => $this->generateUrl('userprojectgroup_update', array('userId' => $userId, 'id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing UserProjectGroup entity.
     *
     * @Route("/{id}", name="userprojectgroup_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:UserProjectGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $userId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $userId);
        $editForm = $this->createEditForm($entity, $userId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('userprojectgroup_edit', array('userId' => $userId, 'id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a UserProjectGroup entity.
     *
     * @Route("/{id}", name="userprojectgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $userId) {
        $form = $this->createDeleteForm($id, $userId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:UserProjectGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserProjectGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('userprojectgroup', array('userId' => $userId)));
    }

    /**
     * Creates a form to delete a UserProjectGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $userId) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('userprojectgroup_delete', array('userId' => $userId, 'id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger')), 'btn'))
                        ->getForm()
        ;
    }

}
