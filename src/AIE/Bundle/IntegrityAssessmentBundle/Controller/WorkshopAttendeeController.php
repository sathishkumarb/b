<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\WorkshopAttendee;
use AIE\Bundle\IntegrityAssessmentBundle\Form\WorkshopAttendeeType;

/**
 * WorkshopAttendee controller.
 *
 * @Route("/pipeline/{pipelineId}/workshops/{workshopId}/attendee")
 */
class WorkshopAttendeeController extends BaseController {

    /**
     * Creates a new WorkshopAttendee entity.
     *
     * @Route("/", name="workshops_attendee_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:WorkshopAttendee:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId, $workshopId)
    {
        $entity = new WorkshopAttendee();
        $form = $this->createCreateForm($entity, $pipelineId, $workshopId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $workshop = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->find($workshopId);
            $entity->setWorkshop($workshop);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a WorkshopAttendee entity.
     *
     * @param WorkshopAttendee $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(WorkshopAttendee $entity, $pipelineId, $workshopId)
    {
        $form = $this->createForm(new WorkshopAttendeeType(), $entity, array(
            'action' => $this->generateUrl('workshops_attendee_create', ['pipelineId' => $pipelineId, 'workshopId' => $workshopId]),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new WorkshopAttendee entity.
     *
     * @Route("/new", name="workshops_attendee_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($pipelineId, $workshopId)
    {
        $entity = new WorkshopAttendee();
        $form = $this->createCreateForm($entity, $pipelineId, $workshopId);

        return array(
            'entity'     => $entity,
            'form'       => $form->createView(),
            'pipelineId' => $pipelineId,
            'workshopId' => $workshopId,
        );
    }

    /**
     * Displays a form to edit an existing WorkshopAttendee entity.
     *
     * @Route("/{id}/edit", name="workshops_attendee_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $pipelineId, $workshopId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:WorkshopAttendee')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
        }

        $editForm = $this->createEditForm($entity, $pipelineId, $workshopId);
        $deleteForm = $this->createDeleteForm($id, $pipelineId, $workshopId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'pipelineId' => $pipelineId,
            'workshopId' => $workshopId,
        );
    }

    /**
     * Creates a form to edit a WorkshopAttendee entity.
     *
     * @param WorkshopAttendee $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(WorkshopAttendee $entity, $pipelineId, $workshopId)
    {
        $form = $this->createForm(new WorkshopAttendeeType(), $entity, array(
            'action' => $this->generateUrl('workshops_attendee_update', array('pipelineId' => $pipelineId, 'workshopId' => $workshopId, 'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing WorkshopAttendee entity.
     *
     * @Route("/{id}", name="workshops_attendee_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:WorkshopAttendee:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $pipelineId, $workshopId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:WorkshopAttendee')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $pipelineId, $workshopId);
        $editForm = $this->createEditForm($entity, $pipelineId, $workshopId);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();

            return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a WorkshopAttendee entity.
     *
     * @Route("/{id}", name="workshops_attendee_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $pipelineId, $workshopId)
    {
        $form = $this->createDeleteForm($id, $pipelineId, $workshopId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:WorkshopAttendee')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find WorkshopAttendee entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('workshops', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to delete a WorkshopAttendee entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $pipelineId, $workshopId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('workshops_attendee_delete', array('pipelineId' => $pipelineId, 'workshopId' => $workshopId, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'right btn-danger')), 'btn'))
            ->getForm();
    }
}
