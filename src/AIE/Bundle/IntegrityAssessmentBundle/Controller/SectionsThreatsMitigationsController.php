<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Manager\MitigationFrequenciesManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsMitigationsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * SectionsThreatsMitigations controller.
 * @Route("pipeline/{pipelineId}/threatsmitigations")
 */
class SectionsThreatsMitigationsController extends BaseController {

    /**
     * Lists all SectionsThreatsMitigations entities.
     *
     * @Route("", name="sectionsthreatsmitigations")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($pipelineId) {
        $em = $this->getDoctrine()->getManager();
        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $mitigations = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->findBy(array('project' => $pipeline->getProject()->getId()));
        $sections = $pipeline->getPse();
        $form = $this->createCreateForm($pipelineId, $sections, $mitigations);
        /*
          foreach($sections as $section){
          $threats = $section->getThreats(); //sectionsThreats
          foreach($threats as $threat){
          $threatMitigation = $threat->getStm();
          }
          }
         */

        //$entities = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findAll();

        return array(
            // 'entities' => $entities,
            'sections' => $sections,
            'wizard_title' => 'Mitigation Techniques',
            'form' => $form->createView(),
            'mitigations' => $mitigations,
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a new SectionsThreatsMitigations entity.
     *
     * @Route("/", name="sectionsthreatsmitigations_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreatsMitigations:new.html.twig")
     */
    public function createAction(Request $request, $pipelineId) {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $mitigations = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->findBy(array('project' => $pipeline->getProject()->getId()));
        $sections = $pipeline->getPse();
        $form = $this->createCreateForm($pipelineId, $sections, $mitigations);
        $form->handleRequest($request);

        if ($form->isValid()) {



            $data = $form->getData();

            foreach ($data as $k => $d) {
                $attr = explode('_', $k); //1=>section, 2=>sectionThreat, 3=>projectMitigation

                $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findOneBy(array('sectionsThreats' => $attr[2], 'mitigationTechnique' => $attr[3]));

                if (!$stm && $d === true) {
                    $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($attr[2]);
                    $mitigation = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->find($attr[3]);


                    $stm = new SectionsThreatsMitigations();
                    $stm->setSectionsThreats($sectionThreat);
                    $stm->setMitigationTechnique($mitigation);
                    $stm->setLastInspectionDate(new \DateTime());
                    $stm->setDate(new \DateTime());
                    $stm->setFrequency(-1);
                    $em->persist($stm);
                } else if ($stm) {
                    $em->persist($stm);

	                if($d === false){
                        $em->remove($stm);
	                }
                }
            }

            $em->flush();
        }

        $mitigationFreqManager = new MitigationFrequenciesManager($em);
        $mitigationFreqManager->updateMitigationFrequencies($pipeline);

        return $this->redirect($this->generateUrl('sectionsthreatsmitigations', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a SectionsThreatsMitigations entity.
     *
     * @param SectionsThreatsMitigations $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($pipelineId, $sections, $mitigations) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('sectionsthreatsmitigations_create', array('pipelineId' => $pipelineId)))
                ->setMethod('POST');

        $stm_form = array();
        $stms = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findByPipelineId($pipelineId);
        if ($stms) {
            foreach ($stms as $stm) {
                $stm_form[$stm->getSectionsThreats()->getPipelineSections()->getId() . '_' . $stm->getSectionsThreats()->getId() . '_' . $stm->getMitigationTechnique()->getId()] = true;
            }
        }

        foreach ($sections as $section) {
            $threats = $section->getSte(); //sectionsThreats
            foreach ($threats as $threat) {
                foreach ($mitigations as $mitigation) {
                    $attr = array();
                    if (isSet($stm_form[$section->getId() . '_' . $threat->getId() . '_' . $mitigation->getId()])) {
                        $attr = array('checked' => 'checked');
                    }
                    $form->add('stm_' . $section->getId() . '_' . $threat->getId() . '_' . $mitigation->getId(), 'checkbox', array('label' => '', 'required' => false, 'attr' => $attr));
                }
            }
        }



        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
                ->getForm();


        return $form;
    }

    /**
     * Lists all SectionsThreatsMitigations entities.
     *
     * @Route("/details", name="sectionsthreatsmitigations_details")
     * @Method("GET")
     * @Template()
     */
    public function detailsAction($pipelineId) {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $files = $pipeline->getPf();        
        $sections = $pipeline->getPse();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        foreach ($sections as $section) {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $mitigationAction[$section->getId()] = $stm;
            //sma.sectionsThreats.threats.name
            foreach ($stm as $s) {
                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()])) {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]++;
                } else {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }
            }
        }
        $form = $this->createDetailsForm($pipelineId, $sections, $mitigationAction, $files);
        /*
          foreach($sections as $section){
          $threats = $section->getThreats(); //sectionsThreats
          foreach($threats as $threat){
          $threatMitigation = $threat->getStm();
          }
          }
         */

        //$entities = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findAll();

        return array(
            // 'entities' => $entities,
            'sections' => $sections,
            'wizard_title' => 'Mitigation Status',
            'form' => $form->createView(),
            'mitigationAction' => $mitigationAction,
            'sectionsThreatsCounter' => $sectionsThreatsCounter,
            'pipelineId' => $pipelineId
        );
    }

    /**
     * Creates a new SectionsThreatsMitigations entity.
     *
     * @Route("/details", name="sectionsthreatsmitigations_details_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreatsMitigations:details.html.twig")
     */
    public function updateDetailsAction(Request $request, $pipelineId) {
        //ini_set('max_input_vars', 3000);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        $files = $pipeline->getPf();
        $sections = $pipeline->getPse();
        $mitigationAction = array();
        foreach ($sections as $section) {
            $mitigationAction[$section->getId()] = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
        }
        $form = $this->createDetailsForm($pipelineId, $sections, $mitigationAction, $files);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            foreach ($data as $k => $d) {
                $attr = explode('_', $k); //1=>sectionsThreatsMitigation

                $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($attr[1]);

                if ($stm) {

                    $stm->setMitigationStatus($d->getMitigationStatus());
                    $stm->setRecommendations($d->getRecommendations());
                    $stm->setPerformanceStandard($d->getPerformanceStandard());
                    $stm->setComments($d->getComments());
                    $date = new \DateTime();
                    if ($d->getLastInspectionDate()) {
                        $date = \DateTime::createFromFormat('d F Y', $d->getLastInspectionDate());
                        $date->setTime(0, 0, 0);
                    }
                    $stm->setLastInspectionDate($date);
                }
                else{
                    $this->addFlash('error', 'Did not save'.$attr[1]);
                }
            }
            $em->flush();
            $em->clear();
        }

        return $this->redirect($this->generateUrl('sectionsthreatsmitigations_details', array('pipelineId' => $pipelineId)));
    }

    public function createDetailsForm($pipelineId, $sections, $mitigationAction, $files) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('sectionsthreatsmitigations_details_create', array('pipelineId' => $pipelineId)))
                ->setMethod('POST');

        foreach ($sections as $section) {
            foreach ($mitigationAction[$section->getId()] as $sma) {
                $form->add('sma_' . $sma->getId(), new SectionsThreatsMitigationsType($sma, $files));
            }
        }

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
                ->getForm();

        return $form;
    }

    /**
     * Displays a form to create a new SectionsThreatsMitigations entity.
     *
     * @Route("/new", name="sectionsthreatsmitigations_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new SectionsThreatsMitigations();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a SectionsThreatsMitigations entity.
     *
     * @Route("/{id}", name="sectionsthreatsmitigations_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectionsThreatsMitigations entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SectionsThreatsMitigations entity.
     *
     * @Route("/{id}/edit", name="sectionsthreatsmitigations_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectionsThreatsMitigations entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a SectionsThreatsMitigations entity.
     *
     * @param SectionsThreatsMitigations $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SectionsThreatsMitigations $entity) {
        $form = $this->createForm(new SectionsThreatsMitigationsType(), $entity, array(
            'action' => $this->generateUrl('sectionsthreatsmitigations_update', array('id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing SectionsThreatsMitigations entity.
     *
     * @Route("/{id}", name="sectionsthreatsmitigations_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:SectionsThreatsMitigations:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SectionsThreatsMitigations entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sectionsthreatsmitigations_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a SectionsThreatsMitigations entity.
     *
     * @Route("/{id}", name="sectionsthreatsmitigations_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SectionsThreatsMitigations entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sectionsthreatsmitigations'));
    }

    /**
     * Creates a form to delete a SectionsThreatsMitigations entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('sectionsthreatsmitigations_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
