<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\MitigationTechniques;
use AIE\Bundle\IntegrityAssessmentBundle\Form\MitigationTechniquesType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * MitigationTechniques controller.
 *
 * @Route("/mitigationtechniques")
 */
class MitigationTechniquesController extends BaseController {

    /**
     * Lists all MitigationTechniques entities.
     *
     * @Route("/", name="mitigationtechniques")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new MitigationTechniques entity.
     *
     * @Route("/", name="mitigationtechniques_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:MitigationTechniques:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new MitigationTechniques();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setDate(new \DateTime());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mitigationtechniques'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a MitigationTechniques entity.
     *
     * @param MitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MitigationTechniques $entity) {
        $form = $this->createForm(new MitigationTechniquesType(), $entity, array(
            'action' => $this->generateUrl('mitigationtechniques_create'),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new MitigationTechniques entity.
     *
     * @Route("/new", name="mitigationtechniques_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new MitigationTechniques();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a MitigationTechniques entity.
     *
     * @Route("/{id}", name="mitigationtechniques_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationTechniques entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MitigationTechniques entity.
     *
     * @Route("/{id}/edit", name="mitigationtechniques_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationTechniques entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a MitigationTechniques entity.
     *
     * @param MitigationTechniques $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MitigationTechniques $entity) {
        $form = $this->createForm(new MitigationTechniquesType(), $entity, array(
            'action' => $this->generateUrl('mitigationtechniques_update', array('id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing MitigationTechniques entity.
     *
     * @Route("/{id}", name="mitigationtechniques_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:MitigationTechniques:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MitigationTechniques entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mitigationtechniques'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a MitigationTechniques entity.
     *
     * @Route("/{id}", name="mitigationtechniques_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MitigationTechniques entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mitigationtechniques'));
    }

    /**
     * Creates a form to delete a MitigationTechniques entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('mitigationtechniques_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
