<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ThreatsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

/**
 * Threats controller.
 *
 * @Route("/threats")
 */
class ThreatsController extends BaseController {

    use Helper\FormStyleHelper;

    /**
     * Lists all Threats entities.
     *
     * @Route("/", name="threats")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Threats entity.
     *
     * @Route("/", name="threats_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:Threats:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Threats();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('threats'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Threats entity.
     *
     * @param Threats $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Threats $entity) {
        $form = $this->createForm(new ThreatsType(), $entity, array(
            'action' => $this->generateUrl('threats_create'),
            'method' => 'POST',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Threats entity.
     *
     * @Route("/new", name="threats_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Threats();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Threats entity.
     *
     * @Route("/{id}", name="threats_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threats entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Threats entity.
     *
     * @Route("/{id}/edit", name="threats_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threats entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Threats entity.
     *
     * @param Threats $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Threats $entity) {
        $form = $this->createForm(new ThreatsType(), $entity, array(
            'action' => $this->generateUrl('threats_update', array('id' => $entity->getId())),
            'method' => 'PUT',
                ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing Threats entity.
     *
     * @Route("/{id}", name="threats_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:Threats:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Threats entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('threats'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Threats entity.
     *
     * @Route("/{id}", name="threats_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Threats entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('threats'));
    }

    /**
     * Creates a form to delete a Threats entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('threats_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
