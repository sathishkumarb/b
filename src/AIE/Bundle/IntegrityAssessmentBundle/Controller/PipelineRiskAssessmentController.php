<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigationsActionOwners;
use AIE\Bundle\IntegrityAssessmentBundle\Form\AssessmentReportType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PlanningInspectionsType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsMitigationsOwnerType;
use AIE\Bundle\IntegrityAssessmentBundle\Manager\MitigationFrequenciesManager;
use AIE\Bundle\ReportingBundle\Helper\PhpDocx\TemplateManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\PipelineRiskAssessmentType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\SectionsThreatsMitigationsFrequencyType;
use Symfony\Component\HttpFoundation\Response;
use AIE\Bundle\IntegrityAssessmentBundle\Form\FormCreators\AssessmentReportCreator;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * PipelineRiskAssessment controller.
 *
 * @Route("pipeline/{pipelineId}/pipelineriskassessment")
 */
class PipelineRiskAssessmentController extends BaseController {

    use AssessmentReportCreator;

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/", name="pipelineriskassessment")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:riskassessment.html.twig")
     */
    public function FreqAssesmentAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);


        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($pipeline->getProject()->getId());
        $riskMatrix = $project->getRiskMatrix();
        $consequences = $em->getRepository('AIEIntegrityAssessmentBundle:ProjectConsequences')->findBy(array('project' => $pipeline->getProject()->getId()));

        $matrix = json_decode($riskMatrix->getMatrix(), true);
        //$freq = BaseData where mitigationTechnique = mt and freq = $matrix[$consequence][$prob];
        $matrixValues = array(); //stores risk matrix value of sections threats

        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);
        $sectionsThreatsConsequences = array();

        $mitigationFreqManager = new MitigationFrequenciesManager($em);
        $defaultFrequencies = $mitigationFreqManager->getDefaultMitigationFrequencies($pipeline);

        foreach ($sectionsThreats as $st)
        {
            $sectionsThreatsConsequences[$st->getId()] = array();
            $sectionThreatsconsequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findBy(array('sectionsThreats' => $st->getId()));

            $maxServerity = - 999;
            foreach ($sectionThreatsconsequences as $stc)
            {
                $sectionsThreatsConsequences[$st->getId()][$stc->getConsequence()->getId()] = $stc->getServerity();

                if ($maxServerity < $stc->getServerity())
                {
                    $maxServerity = $stc->getServerity();
                }
            }

            if(isset($matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1]))
                $matrixValues[$st->getId()] = $matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1];
        }

        $sections = $pipeline->getPse();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        $mitigationFrequency = array();
        foreach ($sections as $section)
        {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $mitigationAction[$section->getId()] = $stm;
            foreach ($stm as $s)
            {
                /* Calculate Frequency */
                if(isset($matrixValues[$s->getSectionsThreats()->getId()]))
                {
                    $value = $matrixValues[$s->getSectionsThreats()->getId()];
                    $function = "get{$value}Freq";
                    $freq = $s->getMitigationTechnique()->$function();
                }


                if ($s->getFrequency() == - 1) //if not seted yet, show default frequency
                    $s->setFrequency($freq);

                /* End of Calculation */
                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]))
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] ++;
                } else
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }
            }
        }


        $form = $this->createAssesmentFreqForm($pipelineId, $sectionsThreats);


        // $threats = $em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();
        // $form = $this->createAssesmentFreqForm($pipelineId, $sections, $threats);

        return array(
            'riskmatrix'                  => $riskMatrix->getColorMatrix(),
            'sections'                    => $sections,
            'mitigationAction'            => $mitigationAction,
            'sectionsThreatsCounter'      => $sectionsThreatsCounter,
            'form'                        => $form->createView(),
            'pipelineId'                  => $pipelineId,
            'consequences'                => $consequences,
            'sectionsThreatsConsequences' => $sectionsThreatsConsequences,
            'defaultFrequencies'          => $defaultFrequencies,
            'wizard_title'                => 'Mitigation Frequency'
        );
    }

    /**
     * Creates a new SectionsThreats entity.
     *
     * @Route("/", name="pipelineriskassessment_create")
     * @Method("POST")
     */
    public function createAssesmentAction(Request $request, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipelineId);

        $form = $this->createAssesmentFreqForm($pipelineId, $sectionsThreats);
        $form->handleRequest($request);

        if ($form->isValid())
        {

            $data = $form->getData();

            $mitigationFreqManager = new MitigationFrequenciesManager($em);
            $defaultFrequencies = $mitigationFreqManager->getDefaultMitigationFrequencies($pipeline);


            foreach ($data as $k => $d)
            {
                $attr = explode('_', $k); // 1=>sectionThreatMitigation
                $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($attr[1]);

                if ($stm)
                {
                    $stm->setFrequency($d->getFrequency());
                    $stm->setFrequencyJustification($d->getFrequencyJustification());

                    if ($defaultFrequencies[$stm->getId()] != $d->getFrequency())
                    {
                        $stm->setLastModifiedFrequency($d->getFrequency());
                    }
                }
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('pipelineriskassessment', array('pipelineId' => $pipelineId)));
    }

    /**
     * Creates a form to create a SectionsThreats entity.
     *
     * @param int $pipelineId The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createAssesmentFreqForm($pipelineId, $sectionsThreats)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('pipelineriskassessment_create', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');
        /*
          foreach ($sections as $section) {
          foreach ($threats as $threat) {
          if ($threat->getPipelineSections()->getId() != $section->getId())
          continue;

          $sectionThreat = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->find($threat->getId());
          $form->add('sectionThreat_' . $section->getId() . '_' . $threat->getId(), new SectionsThreatsType($sectionThreat));
          }
          }
         */
        foreach ($sectionsThreats as $st)
        {
            $mitigationTechniques = $st->getStm(); //get sections threats mitigations
            foreach ($mitigationTechniques as $stm)
            {
                $form->add('stm_' . $stm->getId(), new SectionsThreatsMitigationsFrequencyType($stm));
            }
        }
        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/assessments", name="assessments")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:assessments.html.twig")
     */
    public function AssesmentsAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->findBy(array('pipeline' => $pipelineId, 'isApproved' => false), array('date' => 'DESC'));
        $workshops = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->findByPipeline($pipelineId);
        $form = $this->createAssesmentForm($pipelineId);

        $assessment_report_forms = array();
        foreach ($entities as $entity)
        {
            $assessment_form = $this->createAssesmentReportForm($pipelineId, $entity->getId(), $workshops);
            $assessment_report_forms[$entity->getId()] = $assessment_form->createView();
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'                => $entities,
            'pipelineId'              => $pipelineId,
            'pipeline'                => $pipeline,
            'form'                    => $form->createView(),
            'assessment_report_forms' => $assessment_report_forms,
        );
    }

    /**
     * Lists all SectionsThreats entities.
     *
     * @Route("/approved_assessments", name="approved_assessments")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:assessments.html.twig")
     */
    public function approvedAssesmentsAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->findBy(array('pipeline' => $pipelineId, 'isApproved' => true), array('date' => 'DESC'));
        $workshops = $em->getRepository('AIEIntegrityAssessmentBundle:RiskAssessmentWorkshops')->findByPipeline($pipelineId);

        $assessment_report_forms = array();
        foreach ($entities as $entity)
        {
            $assessment_form = $this->createAssesmentReportForm($pipelineId, $entity->getId(), $workshops);
            $assessment_report_forms[$entity->getId()] = $assessment_form->createView();
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);

        return array(
            'entities'                => $entities,
            'pipelineId'              => $pipelineId,
            'pipeline'                => $pipeline,
            'assessment_report_forms' => $assessment_report_forms,
        );
    }

    /**
     * Creates a new SectionsThreats entity.
     *
     * @Route("/assessments", name="assessments_create")
     * @Method("POST")
     */
    public function AssesstAction(Request $request, $pipelineId)
    {
        $entity = new PipelineRiskAssessment();
        $form = $this->createAssesmentForm($pipelineId);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
            if ($pipeline)
            {
                $entity->setPipeline($pipeline);
                $entity->setIsApproved(false);
                $entity->setDate(new \DateTime());

                //initilize assessment Data
                $assessmentData = array();

                //pipeline Data
                $pipelineData = array();
                $pipelineData['id'] = $pipeline->getId();
                $pipelineData['name'] = $pipeline->getName();
                $pipelineData['description'] = $pipeline->getDescription();
                $pipelineData['geometry'] = $pipeline->getGeometry();
                $pipelineData['battery_limits'] = $pipeline->getBatteryLimits();

                //design Data
                $specifications = $pipeline->getPs();
                $designData = array();
                foreach ($specifications as $specification)
                {
                    $id = $specification->getId();
                    $designData[$id] = array();
                    $designData[$id]['id'] = $id;
                    $designSpecification = $specification->getSpecification();
                    $designData[$id]['specificationId'] = $designSpecification->getId();
                    $designData[$id]['parameter'] = $designSpecification->getParameter();

                    $unit = $specification->getUnit();
                    $designData[$id]['unit'] = $specification->getUnit();
                    $designData[$id]['value'] = $specification->getValue();
                    $designData[$id]['comment'] = $specification->getComment();
                }
                $pipelineData['design_data'] = $designData;

                //documents
                $documentsData = array();

                $documents = $pipeline->getFiles();
                foreach ($documents as $document)
                {
                    $id = $document->getId();
                    if (isSet($documentsData[$id]))
                    {
                        continue;
                    }

                    $documentsData[$id] = array();
                    $documentsData[$id]['id'] = $id;
                    $documentsData[$id]['filename'] = $document->getFilename();
                    $documentsData[$id]['document_number'] = $document->getDocumentNumber();
                    $documentsData[$id]['caption'] = $document->getCaption();
                    $documentsData[$id]['company'] = $document->getCompany();
                    $documentsData[$id]['document_date'] = $document->getDocumentDate();
                    $documentsData[$id]['date'] = $document->getDate();
                }


                $sections = $pipeline->getPse();
                $sectionsData = array();
                $projectThreats = array();
                foreach ($sections as $section)
                {
                    $id = $section->getId(); //PipelineSections
                    $sectionsData[$id] = array();
                    $sectionsData[$id]['id'] = $id;
                    $sectionsData[$id]['name'] = $section->getSection();
                    $sectionsData[$id]['x'] = $section->getX();
                    $sectionsData[$id]['y'] = $section->getY();

                    $threats = $section->getSte();
                    $mitigationsCounter = 0;

                    $threatsData = array();
                    foreach ($threats as $threat)
                    {
                        $id = $threat->getId();
                        $threatsData[$id] = array();
                        $threatsData[$id]['id'] = $id;
                        $threatInfo = $threat->getThreats();
                        if (! isSet($projectThreats[$threatInfo->getId()]))
                        {
                            $projectThreats[$threatInfo->getId()] = array();
                            $projectThreats[$threatInfo->getId()]['name'] = $threatInfo->getName();
                            $projectThreats[$threatInfo->getId()]['description'] = $threatInfo->getDescription();
                            $projectThreats[$threatInfo->getId()]['mitigations'] = array();
                        }
                        $threatsData[$id]['threatId'] = $threatInfo->getId();
                        $threatsData[$id]['name'] = $threatInfo->getName();
                        $threatsData[$id]['confidence'] = $threat->getConfidence();
                        $threatsData[$id]['probabilityOfFailer'] = $threat->getProbabilityOfFailer();
                        $threatsData[$id]['AdjProbabilityOfFailer'] = $threat->getAdjProbabilityOfFailer();

                        //consequence assessment
                        $consequences = $threat->getStc();
                        $consequencesData = array();
                        foreach ($consequences as $consequence)
                        {
                            //$id = $consequence->getId();
                            //$consequencesData[$id] = array();
                            //$consequencesData[$id]['id'] = $id;
                            $projectConsequence = $consequence->getConsequence(); //ProjectConsequences
                            $consequencesData[$projectConsequence->getId()] = $consequence->getServerity();
                            //$consequencesData[$id]['serverity'] = $consequence->getServerity();
                        }
                        $threatsData[$threat->getId()]['consequence_assessments'] = $consequencesData;

                        //mitigations
                        $mitigations = $threat->getStm();
                        $mitigationsData = array();
                        foreach ($mitigations as $mitigation)
                        {
                            $id = $mitigation->getId();
                            $mitigationsData[$id] = array();
                            $mitigationInfo = $mitigation->getMitigationTechnique();
                            $projectThreats[$threatInfo->getId()]['mitigations'][$mitigationInfo->getId()] = $mitigationInfo->getId();
                            $mitigationsData[$id]['mitigationId'] = $mitigationInfo->getId();
                            $mitigationsData[$id]['performanceStandard'] = $mitigation->getPerformanceStandard();
                            $mitigationsData[$id]['frequency'] = $mitigation->getFrequency();
                            $mitigationsData[$id]['date'] = $mitigation->getDate();
                            $mitigationsData[$id]['lastInspection'] = $mitigation->getLastInspectionDate();
                            $mitigationsData[$id]['nextInspection'] = $mitigation->getNextInspectionDate();
                            $mitigationsData[$id]['recommendations'] = $mitigation->getRecommendations();
                            $mitigationsData[$id]['comments'] = $mitigation->getComments();
                            $mitigationsData[$id]['status'] = $mitigation->getMitigationStatus();

                            $documents = $mitigation->getFiles();
                            foreach ($documents as $document)
                            {
                                $id = $document->getId();
                                if (isSet($documentsData[$id]))
                                {
                                    continue;
                                }

                                $documentsData[$id] = array();
                                $documentsData[$id]['id'] = $id;
                                $documentsData[$id]['filename'] = $document->getFilename();
                                $documentsData[$id]['document_number'] = $document->getDocumentNumber();
                                $documentsData[$id]['caption'] = $document->getCaption();
                                $documentsData[$id]['company'] = $document->getCompany();
                                $documentsData[$id]['document_date'] = $document->getDocumentDate();
                                $documentsData[$id]['date'] = $document->getDate();
                            }

                            $mitigationsCounter ++;
                        }
                        $threatsData[$threat->getId()]['mitigations'] = $mitigationsData;
                    }

                    $sectionsData[$section->getId()]['threats'] = $threatsData;
                    $sectionsData[$section->getId()]['mitigationsCounter'] = $mitigationsCounter;
                }
                $pipelineData['sections'] = $sectionsData;
                $pipelineData['documents'] = $documentsData;
                $assessmentData['pipeline'] = $pipelineData;


                //projectData
                $project = $pipeline->getProject();
                $projectData = array();
                $projectData['id'] = $project->getId();
                $projectData['name'] = $project->getName();
                $projectData['description'] = $project->getDescription();
                $projectData['referenceNo'] = $project->getReferenceNo();
                $projectData['client'] = $project->getClient();
                $projectData['location'] = $project->getLocation();

                $consequences = $project->getPc(); //Project Concequences
                $consequencesData = array();
                foreach ($consequences as $consequence)
                {
                    $id = $consequence->getId();
                    $consequencesData[$id] = array();
                    $consequencesData[$id]['id'] = $id;
                    $consequenceInfo = $consequence->getConsequence();
                    $consequencesData[$id]['consequenceId'] = $consequenceInfo->getId();
                    $consequencesData[$id]['name'] = $consequenceInfo->getName();
                    $consequencesData[$id]['desc_1'] = $consequence->getDesc1();
                    $consequencesData[$id]['desc_2'] = $consequence->getDesc2();
                    $consequencesData[$id]['desc_3'] = $consequence->getDesc3();
                    $consequencesData[$id]['desc_4'] = $consequence->getDesc4();
                    $consequencesData[$id]['desc_5'] = $consequence->getDesc5();
                }
                $projectData['consequences'] = $consequencesData;

                $riskmatrix = $project->getRiskMatrix();
                $riskmatrixData = array();
                $riskmatrixData['matrix'] = $riskmatrix->getMatrix();
                $riskmatrixData['desc_1'] = $riskmatrix->getDesc1();
                $riskmatrixData['desc_2'] = $riskmatrix->getDesc2();
                $riskmatrixData['desc_3'] = $riskmatrix->getDesc3();
                $riskmatrixData['desc_4'] = $riskmatrix->getDesc4();
                $riskmatrixData['desc_5'] = $riskmatrix->getDesc5();
                $projectData['riskmatrix'] = $riskmatrixData;

                $mitigations = $project->getPmt();
                $mitigationsData = array();
                foreach ($mitigations as $mitigation)
                {
                    $id = $mitigation->getId();
                    $mitigationsData[$id] = array();
                    $mitigationsData[$id]['id'] = $id;
                    $mitigationsData[$id]['name'] = $mitigation->getMitigationTechnique();
                    $mitigationsData[$id]['description'] = $mitigation->getDescription();
                    $mitigationsData[$id]['l_freq'] = $mitigation->getLFreq();
                    $mitigationsData[$id]['m_freq'] = $mitigation->getMFreq();
                    $mitigationsData[$id]['h_freq'] = $mitigation->getHFreq();
                    $mitigationsData[$id]['v_freq'] = $mitigation->getVFreq();
                    $mitigationsData[$id]['vl_freq'] = $mitigation->getVlFreq();
                }
                $projectData['mitigations'] = $mitigationsData;
                $projectData['threats'] = $projectThreats;

                $assessmentData['project'] = $projectData;


                $entity->setAssessmentData($assessmentData);


                $em->persist($entity);
                $em->flush();

                //call update report
                $this->updateAssessment($pipelineId, $entity);

            }
        }

        return $this->redirect($this->generateUrl('assessments', array('pipelineId' => $pipelineId)));
    }

    private function createAssesmentForm($pipelineId)
    {


        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('assessments_create', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate Report'), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/report/{id}", name="assessment_report")
     * @Method("GET")
     */
    public function reportAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineRiskAssessment entity.');
        }

        return new Response($entity->getHtmlReport());
    }

    private function renderReport($entity, $pipelineId)
    {

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        //echo '<pre>';print_r($assessment);echo '</pre>';exit();
        $matrix = json_decode($assessment['project']['riskmatrix']['matrix'], true);

        return array(
            'assessment' => $entity,
            'pipelineId' => $pipelineId,
            'pipeline'   => $assessment['pipeline'],
            'project'    => $assessment['project'],
            'matrix'     => $matrix
        );
    }


    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/report/{id}/section_gmap", name="assessment_section_gmap")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:Reports:section_gmap.html.twig")
     */
    public function sectionGmapAction($pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineRiskAssessment entity.');
        }

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        $matrix = json_decode($assessment['project']['riskmatrix']['matrix'], true);

        return array(
            'assessment' => $entity,
            'pipelineId' => $pipelineId,
            'pipeline'   => $assessment['pipeline'],
            'project'    => $assessment['project'],
            'matrix'     => $matrix
        );
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/report/{id}/chart_img", name="assessment_chart")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:Reports:chart_img.html.twig")
     */
    public function chartAction($pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineRiskAssessment entity.');
        }

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        $matrix = json_decode($assessment['project']['riskmatrix']['matrix'], true);

        return array(
            'assessment' => $entity,
            'pipelineId' => $pipelineId,
            'pipeline'   => $assessment['pipeline'],
            'project'    => $assessment['project'],
            'matrix'     => $matrix
        );
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/approve/{id}", name="riskassessment_approve")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:Reports:assessment.html.twig")
     */
    public function approveAction($pipelineId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find PipelineRiskAssessment entity.');
        }

        $em->getRepository('AIEIntegrityAssessmentBundle:PipelineRiskAssessment')->updateApproved($pipelineId);

        $entity->setIsApproved(true);
        $em->flush();

        return $this->redirect($this->generateUrl('assessments', array('pipelineId' => $pipelineId)));
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/inspections", name="inspection_dates")
     * @Method("GET")
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:inspections.html.twig")
     */
    public function inspectionsAction($pipelineId)
    {
        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $sections = $pipeline->getPse();
        $emails = $pipeline->getPe();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        foreach ($sections as $section)
        {
            $mitigationAction[$section->getId()] = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            //sma.sectionsThreats.threats.name
            foreach ($mitigationAction[$section->getId()] as $s)
            {
                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]))
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] ++;
                } else
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }
            }
        }

        $form = $this->createInspectionOwnerForm($pipelineId, $sections, $mitigationAction, $emails);

        return array(
            // 'entities' => $entities,
            'sections'               => $sections,
            'wizard_title'           => 'Activities',
            'mitigationAction'       => $mitigationAction,
            'sectionsThreatsCounter' => $sectionsThreatsCounter,
            'pipelineId'             => $pipelineId,
            'form'                   => $form->createView()
        );
    }


    /**
     *
     * @Route("/inspections", name="update_inspection_dates")
     * @Method("POST")
     */
    public function updateInspections(Request $request, $pipelineId)
    {

        $em = $this->getDoctrine()->getManager();

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $sections = $pipeline->getPse();
        $emails = $pipeline->getPe();
        $mitigationAction = array();
        foreach ($sections as $section)
        {
            $mitigationAction[$section->getId()] = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
        }

        $form = $this->createInspectionOwnerForm($pipelineId, $sections, $mitigationAction, $emails);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $data = $form->getData();
            foreach ($data as $k => $d)
            {
                $attr = explode('_', $k); //1=>sectionsThreatsMitigation

                $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($attr[1]);
                if ($stm)
                {
                    $stm->setActionOwner($d->getActionOwner());
                }
            }
            $em->flush();
        }

        return $this->redirect($this->generateUrl('inspection_dates', array('pipelineId' => $pipelineId)));
    }

    /**
     * @param $mitigationAction
     * @param $pipelineId
     * @param $sections
     * @param $mitigationAction
     * @param $emails
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInspectionOwnerForm($pipelineId, $sections, $mitigationAction, $emails)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('update_inspection_dates', array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        foreach ($sections as $section)
        {
            foreach ($mitigationAction[$section->getId()] as $sma)
            {
                $form->add('sma_' . $sma->getId(), new SectionsThreatsMitigationsOwnerType($sma, $pipelineId, $emails));
            }
        }
        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Save'), 'btn'))
            ->getForm();

        return $form;
    }

    public function filterActions(Request $request, $pipelineId, $submitForm)
    {

        $em = $this->getDoctrine()->getManager();
        $from_date = new \DateTime('now');
        $to_date = clone $from_date;
        $to_date->modify('+1 month');

        $filter_form = $this->createInspectionFilterForm($pipelineId, $from_date, $to_date, $submitForm);

        $filter_form->handleRequest($request);

        if ($filter_form->isValid())
        {
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];

            $from_date = new \DateTime($filter_data['from_date']);
            $to_date = new \DateTime($filter_data['to_date']);
            $to_date->modify('+1 day');
        }


        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $sections = $pipeline->getPse();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        $sectionsCounter = array();
        foreach ($sections as $section)
        {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $sectionsCounter[$section->getId()] = 0;
            $sectionActions = [];
            foreach ($stm as $key => $s)
            {
                $next_inspection_date_U = (int)$s->getNextInspectionDate()->format('U');
                if (
                    (! $s->getIsInspectionDue() || $next_inspection_date_U > $to_date->format('U')) //overdue activities up to "to_date"
                    &&
                    ($next_inspection_date_U < (int)$from_date->format('U') || $next_inspection_date_U > (int)$to_date->format('U')) //filter between from and to dates
                )
                {
                    unset($stm[$key]);
                    continue;
                }

                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]))
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] ++;
                } else
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }


                $action = $s->getMitigationTechnique()->getMitigationTechnique();
                if (isSet($sectionActions[$action])) //if mitigation action is duplicated in same section
                    //take lowest freq
                {

                    $index = $sectionActions[$action];
                    if (isset($stm[$key]) && $stm[$key]->getFrequency() < $s->getFrequency())
                    {
                        $sectionsThreatsCounter[$section->getId() . '_' . $stm[$key]->getSectionsThreats()->getId()] --;
                        unset($stm[$key]);
                    } 
                    else if(isset($stm[$index]))
                    {
                        $sectionsThreatsCounter[$section->getId() . '_' . $stm[$index]->getSectionsThreats()->getId()] --;
                        unset($stm[$index]);
                    }

                    continue;

                } else
                {
                    $sectionActions[$action] = $key;
                }


                $sectionsCounter[$section->getId()] ++;
            }

            $mitigationAction[$section->getId()] = $stm;
        }

        $to_date->modify('-1 day');
        $filter_form = $this->createInspectionFilterForm($pipelineId, $from_date, $to_date, $submitForm);

        return array(
            // 'entities' => $entities,
            'sections'               => $sections,
            'mitigationAction'       => $mitigationAction,
            'sectionsThreatsCounter' => $sectionsThreatsCounter,
            'sectionsCounter'        => $sectionsCounter,
            'pipelineId'             => $pipelineId,
            'filter_form'            => $filter_form->createView()
        );
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/planning", name="inspections_planning")
     * @Method({"GET", "POST"})
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:inspections_planning.html.twig")
     */
    public function inspectionsFilterAction(Request $request, $pipelineId)
    {
        $em = $this->getDoctrine()->getManager();
        $from_date = new \DateTime('now');
        $to_date = clone $from_date;
        $to_date->modify('+1 month');
        $to_date->modify('+1 day');

        $filter_form = $this->createInspectionFilterForm($pipelineId, $from_date, $to_date, 'inspections_planning');

        $filter_form->handleRequest($request);

        if ($filter_form->isValid())
        {
            $data = $filter_form->getData();
            $filter_data = $data['filter_form'];

            $from_date = new \DateTime($filter_data['from_date']);
            $to_date = new \DateTime($filter_data['to_date']);
            $to_date->modify('+1 day');
        }

        $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
        $this->setLeftTitleFromPipeline($pipeline);
        $sections = $pipeline->getPse();
        $mitigationAction = array();
        $sectionsThreatsCounter = array();
        $sectionsCounter = array();

        foreach ($sections as $section)
        {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $sectionsCounter[$section->getId()] = 0;

            $sectionActions = [];
            foreach ($stm as $key => $s)
            {
                $next_inspection_date_U = (int)$s->getNextInspectionDate()->format('U');

                if($s->getFrequency() == 0){
                    //if continues action
                    unset($stm[$key]);
                    continue;
                }

                if (
                    ((int)$next_inspection_date_U < $from_date->format('U') || $next_inspection_date_U > (int)$to_date->format('U')) //filter between from and to dates
                    &&
                    (! $s->getIsInspectionDue() || (int)$next_inspection_date_U > $to_date->format('U')) //overdue activities up to "to_date"
                )
                {
                    unset($stm[$key]);
                    continue;
                }

                $action = $s->getMitigationTechnique()->getMitigationTechnique();
                if (isSet($sectionActions[$action])) //if mitigation action is duplicated in same section
                    //take lowest freq
                {

                    $index = $sectionActions[$action];
                    if (isset($stm[$index]) && $stm[$index]->getFrequency() < $s->getFrequency())
                    {
                        unset($stm[$key]);
                    } else
                    {
                        unset($stm[$index]);
                    }

                    continue;

                } else
                {
                    $sectionActions[$action] = $key;
                }


                if (isSet($sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()]))
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] ++;
                } else
                {
                    $sectionsThreatsCounter[$section->getId() . '_' . $s->getSectionsThreats()->getId()] = 1;
                }
                $sectionsCounter[$section->getId()] ++;
            }

            $mitigationAction[$section->getId()] = $stm;
        }

        $repeatedActions = [];
        foreach ($mitigationAction as $sectionId => $actions)
        {
            foreach ($actions as $action)
            {
                $repeatedAction = clone $action;

                if($repeatedAction->getFrequency() == 0){
                    continue;
                }

                $nextDate = $repeatedAction->getNextInspectionDate(true);
                $repeatedAction->setLastInspectionDate($nextDate);
                $nextDate = $repeatedAction->getNextInspectionDate(true);
                $nextDate_U = (int)$nextDate->format('U');

                while ($nextDate_U >= (int)$from_date->format('U') && $nextDate_U <= (int)$to_date->format('U'))
                {
                    //add the repeated action to actions list
                    if (! isSet($repeatedActions[$sectionId]))
                    {
                        $repeatedActions[$sectionId] = [];
                    }
                    $repeatedActions[$sectionId][] = clone $repeatedAction;

                    //set repeated action last time to next date
                    $repeatedAction->setLastInspectionDate($nextDate);
                    //set next date from repeated action
                    $nextDate = $repeatedAction->getNextInspectionDate(true);
                    $nextDate_U = (int)$nextDate->format('U');
                }
            }
        }


        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($from_date, $interval, $to_date);

        $sortedActions = Helper\ArrayHelper::array_flatten($mitigationAction);
        $repeatedActions = Helper\ArrayHelper::array_flatten($repeatedActions);

        //echo count($sortedActions) . '<br>' . count($repeatedActions) . '<br>';
        //var_dump($repeatedActions);exit();
        $sortedActions = array_merge($sortedActions, $repeatedActions);
        //echo count($sortedActions);exit();
        usort($sortedActions, array('AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations', 'nextDateSort'));

        $actions = [];

        /*foreach ($period as $dt)
        {   $actions[$dt->format('d F Y')] = [];
        }*/

        foreach ($sortedActions as $action)
        {

            $nextDate = $action->getNextInspectionDate();
            if ((int)$nextDate->format('U') > (int)$to_date->format('U'))
            {
                continue;
            }

            $date = $nextDate->format('d F Y');

            if (! isSet($actions[$date]))
            {
                $actions[$date] = [];
            }

            $actions[$date][] = $action;
        }

        $to_date->modify('-1 day');
        $filter_form = $this->createInspectionFilterForm($pipelineId, $from_date, $to_date, 'inspections_planning');

        return array(
            // 'entities' => $entities,
            'sections'               => $sections,
            'mitigationAction'       => $mitigationAction,
            'sectionsThreatsCounter' => $sectionsThreatsCounter,
            'sectionsCounter'        => $sectionsCounter,
            'pipelineId'             => $pipelineId,
            'period'                 => $period,
            'actions'                => $actions,
            'filter_form'            => $filter_form->createView()
        );
    }

    /**
     * @param $pipelineId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createInspectionFilterForm($pipelineId, \DateTime $from_date, \DateTime $to_date, $submit_form)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl($submit_form, array('pipelineId' => $pipelineId)))
            ->setMethod('POST');

        $form->add('filter_form', new PlanningInspectionsType($from_date, $to_date), ['label' => ' ']);

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate', 'attr' => ['class' => 'right', 'style' => 'margin-right: 30px;']), 'btn'))
            ->getForm();

        return $form;
    }

    /**
     * Finds and displays a PipelineFiles entity.
     *
     * @Route("/reminders", name="inspections_reminders")
     * @Method({"GET", "POST"})
     * @Template("AIEIntegrityAssessmentBundle:PipelineRiskAssessment:inspections_reminders.html.twig")
     */
    public function inspectionsReminderAction(Request $request, $pipelineId)
    {
        return $this->filterActions($request, $pipelineId, 'inspections_reminders') + ['wizard_title' => 'Reminders'];
    }


    public function updateAssessment($pipelineId, $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $assessment = $entity->getAssessmentData(); //json_decode(json_encode($entity->getAssessmentData()), FALSE);
        $matrix = json_decode($assessment['project']['riskmatrix']['matrix'], true);
        $pipeline = $assessment['pipeline'];
        $project = $assessment['project'];
        $projectEntity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($project['id']);

        $sections = $pipeline['sections'];
        $sections_names = array();
        foreach ($sections as $section)
        {
            $sections_names[] = $section['name'];
        }
        $sections_string = join(' and ', array_filter(array_merge(array(join(', ', array_slice($sections_names, 0, - 1))), array_slice($sections_names, - 1))));

        $designData = array();
        foreach ($pipeline['design_data'] as $design)
        {
            $designData[] = array(
                'DESIGN_PARAMETER' => $design['parameter'],
                'DESIGN_VALUE'     => $design['value'] . ' ' . $design['unit']
            );
        }

        $references_list = array();
        foreach ($pipeline['documents'] as $document)
        {

            $reference = "{$document['filename']}, {$document['company']}, Document Number {$document['document_number']}";

            if ($document['document_date'])
            {
                $date = $document['document_date']; //new \DateTime($document['document_date']['date']);
                $date = $date->format('d F Y');
                $reference .= ", $date";
            }
            $references_list[] = $reference;
        }


        $html = $this->renderView('AIEIntegrityAssessmentBundle:Reports:assessment.html.twig', array(
            'assessment' => $entity,
            'pipelineId' => $pipelineId,
            'pipeline'   => $pipeline,
            'project'    => $project,
            'matrix'     => $matrix
        ));

        //$html = mb_convert_encoding($html, 'UTF-8', mb_detect_encoding($html, 'UTF-8, ISO-8859-1', true));
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
        //iconv(mb_detect_encoding($html, mb_detect_order(), true), "UTF-8", $html);

        $html_settings = array(
            'isFile'           => false,
            'parseDivsAsPs'    => true,
            'downloadImages'   => true,
            'parseFloats'      => false,
            'strictWordStyles' => true,
            'wordStyles'       => array(
                '.table'          => 'AIETable',
                '.appendix-title' => 'AppendixTitle',
                '.appendix-desc'  => 'AppendixDesc',
                '.caption'        => 'Caption',
                ''                => 'Normal'
            )
        );

        $table_settings = array('header' => true);


        $template = $this->get('kernel')->locateResource("@AIEIntegrityAssessmentBundle/Reports/Templates/AIE-RA-TEMPLATE.docx");

        $threats = $project['threats'];
        $threats_names = array();
        foreach ($threats as $key => $threat)
        {
            $threats_names[] = $threat['name'];
            $threats_names[] = '$THREAT_DESC_' . $key . '$';
        }

        $threats_mitigations_names = array();
        foreach ($threats as $key => $threat)
        {
            $threats_mitigations_names[] = 'Inspection Interval - ' . ucfirst($threat['name']);
            $threats_mitigations_names[] = '$THREAT_MITIGATION_TABLE_' . $key . '$';
        }

        $consequences = $project['consequences'];
        $consequences_names = array();
        foreach ($consequences as $key => $consequence)
        {
            $consequences_names[] = $consequence['name'];
        }

        $low_mid_recommendations = array();

        foreach ($sections as $section)
        {
            $maxThreats = [];
            $lastLevel = 'L';
            foreach ($section['threats'] as $threat)
            {
                $maxConsequence = - 1;
                foreach ($project['consequences'] as $consequence)
                {
                    $s = $threat['consequence_assessments'][$consequence['id']] - 1;

                    if ($s > $maxConsequence)
                    {
                        $maxConsequence = $s;
                    }
                }

                $p = $threat['AdjProbabilityOfFailer'] - 1;
                $level = $matrix[$maxConsequence][$p];

                if ($level == 'L' || $level == 'M')
                {
                    //was L and now M
                    if ($level != $lastLevel && $level == 'M')
                    {
                        $lastLevel = 'M';
                        $maxThreats = [];
                    }

                    $maxThreats[] = $threat;
                }
            }

            foreach ($maxThreats as $threat)
            {
                foreach ($threat['mitigations'] as $mitigation)
                {
                    if (! empty($mitigation['recommendations']))
                    {
                        $low_mid_recommendations[] = $section['name'] . ' - ' . $threat['name'] . ': ' . $mitigation['recommendations'];
                    }
                }
            }
        }


        $templateManage = new TemplateManager($template);
        $templateManage->setHtmlSettings($html_settings);
        $templateManage->setTableSettings($table_settings);

        $parser = $this->container->get('simple_html_dom');
        $parser->load($html);


        $colors = array(
            'lightgreen' => '#ddffa6',
            'green'  => '#66ff33',
            'yellow' => '#ffff00',
            'orange' => '#ff0000',
            'red'    => 'rgb(192, 0, 0)',
        );

        foreach ($parser->find('.risk-color') as $element)
        {
            foreach ($colors as $color => $rgb)
            {
                if (strpos($element->class, $color) !== false)
                {
                    $element->style = 'background: ' . $rgb;

                    $inner = $element->first_child();
                    if ($inner)
                    {
                        $inner->style = 'background: transparent; font-weight: bold; text-align: center;';
                    }
                    break;
                }
            }
        }

        foreach ($parser->find('table thead th') as $element)
        {
            $element->style = 'background: #DDDDDD;';
            $inner = $element->first_child();
            if ($inner)
            {
                $inner->style = 'background: transparent; font-weight: bold; text-align: center;';
            }
        }

        foreach ($parser->find('.text-center') as $element)
        {
            $element->style = 'text-align: center;';
        }

        $total_risks_counter = $parser->find('#total-risks-counter', 0)->plaintext;

        $textVariables = array(
            'PIPELINE_NAME'                 => $pipeline['name'],
            'CLIENT_NAME'                   => $project['client'],
            'REPORT_ID'                     => $entity->getId(),
            'REFERENCE_NUMBER'              => $project['referenceNo'],
            'CREATED_DATE'                  => $entity->getDate()->format('jS F Y'),
            'INTRODUCTION'                  => $pipeline['description'],
            'BATTERY_LIMITS'                => $pipeline['battery_limits'],
            'SECTIONS_STRING'               => $sections_string,
            'TOTAL_RISKS'                   => $total_risks_counter,
            'LOW_MID_RISKS_RECOMMENDATIONS' => $low_mid_recommendations,
            'THREATS_COUNTER'               => count($threats),
        );
	
	$headerVariables = array('PIPELINE_NAME' => $pipeline['name']);
	$listVariables = array(
           'CONSEQUENCE_LIST'              => array_map(function ($val)
            {
                return $val;
            }, $consequences_names),
	    'REFERENCES_LIST'               => $references_list,
            'SECTIONS_LIST'                 => $sections_names,
            'THREAT_SECTION'                => $threats_names,
            'THREAT_MITIGATION_ACTIONS'     => $threats_mitigations_names,
        );

        $htmlVariables = array(
            'SECTIONAL_THREAT_ANALYSIS'         => array('html' => $parser->find("#sectional-threat-analysis", 0)),
            'THREAT_MITIGATION_TECHNIQUES'      => array('html' => $parser->find("#threat-mitigations", 0)),
            'PROBABILITY_OF_FAILURE_ASSESSMENT' => array('html' => $parser->find("#probability-of-failure-assessment", 0)),
            'RISK_LEVEL_RESULTS'                => array('html' => $parser->find("#risk-level-results", 0)),
        );

        $htmlColoredVariables = [
            'CONSEQUENCE_ASSESSMENT_CRITERIA' => array('html' => $parser->find("#consequences", 0), 'settings' => array('strictWordStyles' => false) + $html_settings),
            'PROBABILITY_OF_FAILURE'          => array('html' => $parser->find("#probabilities", 0), 'settings' => array('strictWordStyles' => false) + $html_settings),
            'HIGH_VERY_HIGH_RISKS'            => array('html' => $parser->find("#risks-high-very-high", 0), 'settings' => array('strictWordStyles' => false) + $html_settings),
            'CONSEQUENCES_ASSESSMENT'         => array('html' => $parser->find("#consequences-assessment", 0), 'settings' => array('strictWordStyles' => false) + $html_settings),
        ];

        $tableVariables = array(
            'DESIGN_SPECIFICATIONS' => array('data' => $designData)
        );


        foreach ($threats as $key => $threat)
        {
            $description = '<p>' . $threat['description'] . '</p>';
            $description = mb_convert_encoding($description, "HTML-ENTITIES", "UTF-8");
            $htmlVariables['THREAT_DESC_' . $key] = array('html' => $description);
        }

        foreach ($threats as $key => $threat)
        {
            $table = $parser->find("#threat-mitigations-$key", 0);
            $table->width = '100%';
            $table->style = 'border-collapse: collapse;border: 1px solid black; width: 100%; font-size: 13px; text-align: center;';

            $td = $table->find('tbody td');

            $td[1]->style = $td[2]->style = $td[3]->style = $td[4]->style = 'width: 55px;';

            $caption = '<p class="caption" style="text-align:center;  font-size: 13px;">Inspection Interval (months) - ' . ucfirst($threat['name']) . '</p>';
            $htmlVariables['THREAT_MITIGATION_TABLE_' . $key] = array('html' => "{$table}{$caption}", 'settings' => array('strictWordStyles' => false) + $html_settings);
        }


        foreach ($htmlColoredVariables as $key => $htmlTable)
        {
            $table = $htmlTable['html'];
            $table->style = 'border-collapse: collapse;border: 1px solid black; width: 100%; font-size: 13px;';
        }

        $riskMatrixTable = $parser->find("#risk-matrix-table", 0);
        $highRisksRecommendations = $parser->find("#high-risks-recommendations", 0);


        $riskMatrixTable->style = 'border-collapse: collapse;border: 1px solid black; width: 725px; margin-left:-70px; font-size: 12px; font-weight: bold; text-align: center;';
        $highRisksRecommendations->style = 'border-collapse: collapse;border: 1px solid black; width: 725px; margin-left:-70px; font-size: 13px; text-align: left;';
        $htmlVariables['RISK_MATRIX_TABLE'] = array('html' => $riskMatrixTable, 'settings' => array('strictWordStyles' => false) + $html_settings);
        $htmlVariables['HIGH_RISKS_RECOMMENDATIONS'] = array('html' => $highRisksRecommendations, 'settings' => array('strictWordStyles' => false) + $html_settings);


        $appendix = array();
        foreach ($sections as $key => $section)
        {
            $id = $section['id'];
            $appendix[] = '$APPENDIX_TITLE_' . $id . '$';
            $appendix[] = '$APPENDIX_TABLE_' . $id . '$';
            $appendix[] = "\r\n";
        }
        $listVariables['APPENDIX'] = $appendix;


        //Generate IMAGES
        $knpImage = $this->get('knp_snappy.image');
//        $knpImage->getInternalGenerator()->setTimeout($this->_timeoutGeneration);

        $session = $this->get('session');
        $session->save();
        session_write_close();
        $imageVariables = [];

        $chartURL = $this->generateUrl('assessment_chart', array('pipelineId' => $pipelineId, 'id' => $entity->getId()), true);
        $chartPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . md5(uniqid(rand(), true)) . '.jpg';
        try
        {
            $knpImage->generate($chartURL, $chartPath, array('cookie' => array($session->getName() => $session->getId())));

            $imageVariables = [
                'THREATS_IMAGE' => $chartPath,
            ];
        } catch (\Exception $e)
        {

        }

        $templateManage->addMultipleTexts($textVariables);
        $templateManage->addMultipleHTML($htmlVariables);
        $templateManage->addMultipleHTML($htmlColoredVariables);
        $templateManage->addMultipleTables($tableVariables);
        $templateManage->addMultipleImages($imageVariables);
	$templateManage->addMultipleLists($listVariables);
	$templateManage->replaceHeaderText($headerVariables);	

        $templateManage->replaceVariables();

        $logoContent = $this->readFile($projectEntity);
        if ($projectEntity && $logoContent)
        {
            $tempLogo = tempnam(sys_get_temp_dir(), 'COMPANY_LOGO' . uniqid());
            $logoHandle = fopen($tempLogo, "w");
            fwrite($logoHandle, $logoContent);
            fclose($logoHandle);


            $imageVariables['COMPANY_LOGO'] = $tempLogo;
            $templateManage->addMultipleImages($imageVariables);
            $templateManage->replaceVariables();
        }

        $alphas = array_merge(range('A', 'Z'), range('a', 'z'));
        $appendix_index = 0;
        foreach ($sections as $key => $section)
        {
            $id = $section['id'];
            $table = $parser->find("#section-$id", 0)->find('table', 0);
            $table->style = 'border-collapse: collapse;border: 1px solid black; width: 980px; margin-left:-50px; font-size: 12px; text-align: left; page-break-after: always;page-break-before:always;';

            $thead = $table->find('thead', 0);
            $thead->style = 'font-size: 10px;';

            $tr = $thead->find('tr');
            $columns = $tr[0]->find('th');

//            $columns[0]->style = ''; //threat group
//            $columns[1]->style = ''; //mitigation action
//            $columns[2]->style = ''; //performance standard
            $columns[3]->width = '30'; //mitigation status
//            $columns[4]->width = ''; //prob of failure
//            $columns[5]->style = ''; //confidence
//            $columns[6]->style = '';//Individual Risk
//            $columns[7]->style = ''; //freq
//            $columns[8]->style = ''; //next inspection
//            $columns[9]->style = ''; //inspection status
//            $columns[10]->style = ''; //recommendation


            foreach ($tr[1]->find('th') as $consequenceTh)
            {
                //$consequenceTh->style = 'width: 400px';
            }

            $htmlVariables['APPENDIX_TITLE_' . $id] = array('html' => '<h1 class="appendix-title">APPENDIX ' . $alphas[$appendix_index] . '</h1><p class="appendix-desc">Detailed Risk Assessment of the ' . $section['name'] . ' Section of the ' . $project['client'] . '\'s ' . $pipeline['name'] . '</p>');
            $htmlVariables['APPENDIX_TABLE_' . $id] = array('html' => "<div/>$table<div/>", 'settings' => array('strictWordStyles' => false) + $html_settings);

            $appendix_index ++;
        }
        $templateManage->addMultipleHTML($htmlVariables);
        $report = $templateManage->generateDocument();

        //Sending the file to be downloaded
        $bad = array_merge(
            array_map('chr', range(0, 31)), array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));

        $name = $project['client'] . ' - ' . $pipeline['name'];
        $name = str_replace($bad, "", $name);
        $fullName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $name;
        //$report->createDocx($fullName);


        $fileName = $fullName . '.' . $report->getExtension();
        $report->createDocx($fullName);
        $file = new File($fileName);
        $filePath = $this->uploadFile($file, $entity);

        $entity->setFilepath($filePath);
        $entity->setHtmlReport($html);

        $em->persist($entity);
        $em->flush();

        foreach ($imageVariables as $image)
        {
            unlink($image);
        }
        @unlink($fileName);
    }
}
