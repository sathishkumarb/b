<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects;
use AIE\Bundle\IntegrityAssessmentBundle\Form\ProjectsType;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskMatrix;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Projects controller.
 *
 * @Route("/projects")
 */
class ProjectsController extends BaseController {

    /**
     * Lists all Projects entities.
     *
     * @Route("/", name="projects")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $this->getGrantedProjects();

        $pagination_config = $this->container->getParameter('pagination');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities, $this->get('request')->query->get('page', 1) /* page number */, $pagination_config['limit_per_page'] /* limit per page */
        );

        return array(
            'entities'   => $entities,
            'pagination' => $pagination
        );
    }

    /**
     * Creates a new Projects entity.
     *
     * @Route("/", name="projects_create")
     * @Method("POST")
     * @Template("AIEIntegrityAssessmentBundle:Projects:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Projects();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {


            /* Handle Uploaded File */
            $data = $form->getData();
            $file = $data->getFile();
            $uploadedPath = $this->uploadFile($file, $entity);
            $entity->setFilepath($uploadedPath);


            /* */
            $entity->setDateCreate(new \DateTime("now"));

            $securityContext = $this->get('security.context');
            $token = $securityContext->getToken();
            $user = $token->getUser();
            $userId = $user->getId();
            $entity->setCreatedBy($userId);

            /*  */
            $em = $this->getDoctrine()->getManager();

            $mitigationTechniques = $em->getRepository('AIEIntegrityAssessmentBundle:MitigationTechniques')->findAll();

            foreach ($mitigationTechniques as $mt)
            {
                $pmt = new ProjectMitigationTechniques();
                $pmt->setMitigationTechnique($mt->getName());
                $pmt->setDescription($mt->getDescription());
                $pmt->setDate(new \DateTime());
                $pmt->setProject($entity);
                $pmt->setLFreq($mt->getLFreq());
                $pmt->setMFreq($mt->getMFreq());
                $pmt->setHFreq($mt->getHFreq());
                $pmt->setVFreq($mt->getVFreq());

                $entity->addPmt($pmt);
            }

            /* Create Risk Matrix */

            $entity->setRiskMatrix(new RiskMatrix());

            $em->persist($entity);


            /* Give Admin permissions to the created Project */
            $adminGroup = $em->getRepository('UserBundle:Group')->find(1);
            $userProjects = new UserProjectGroup();
            $userProjects->setProject($entity);
            $userProjects->setUser($user);
            $userProjects->setGroup($adminGroup);


            $em->persist($userProjects);
            $em->flush();

            return $this->redirect($this->generateUrl('projects_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Projects $entity)
    {
        $form = $this->createForm(new ProjectsType(), $entity, array(
            'action' => $this->generateUrl('projects_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Create', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Displays a form to create a new Projects entity.
     *
     * @Route("/new", name="projects_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Projects();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Projects entity.
     *
     * @Route("/{id}", name="projects_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }


        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('ROLE_PROJECT_SHOW', $entity) === false)
        {
            throw new AccessDeniedException();
        }


        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/edit", name="projects_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Projects entity.
     *
     * @param Projects $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Projects $entity)
    {
        $form = $this->createForm(new ProjectsType(), $entity, array(
            'action' => $this->generateUrl('projects_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', $this->options(array('label' => 'Update', 'attr' => array('class' => 'right')), 'btn'));

        return $form;
    }

    /**
     * Edits an existing Projects entity.
     *
     * @Route("/{id}", name="projects_update")
     * @Method("PUT")
     * @Template("AIEIntegrityAssessmentBundle:Projects:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {

            /* Handle Uploaded File */
            $data = $editForm->getData();
            $file = $data->getFile();
            $uploadedPath = $this->replaceFile($file, $entity);
            $entity->setFilepath($uploadedPath);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('projects'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Projects entity.
     *
     * @Route("/{id}/delete", name="projects_delete")
     * @Method("GET")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($id);

        if (! $entity)
        {
            throw $this->createNotFoundException('Unable to find Projects entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Projects entity.
     *
     * @Route("/{id}", name="projects_post_delete")
     * @Method("DELETE")
     */
    public function postDeleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($id);

            if (! $entity)
            {
                throw $this->createNotFoundException('Unable to find Projects entity.');
            }

            $this->deleteFile($entity);
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('projects'));
    }

    /**
     * Creates a form to delete a Projects entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projects_post_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', $this->options(array('label' => 'Delete', 'attr' => array('class' => 'btn-danger right hide')), 'btn'))
            ->getForm();
    }

}
