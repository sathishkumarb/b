<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Controller;

use AIE\Bundle\IntegrityAssessmentBundle\Helper\FileHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends BaseController {

    /*
      public function templateAction() {
      return $this -> render('AIEIntegrityAssessmentBundle:Default:template.html.twig');
      }
     */

    /**
     *
     *
     * @Route("/", name="pipeline_index")
     * @Method("get")
     * @Template("AIEIntegrityAssessmentBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
        error_reporting(0);
        $securityContext = $this->container->get('security.authorization_checker');

        $projectsCounter = 0;
        $pipelinesCounter = 0;
        $totalPipelinesLength = 0;
        $documentsCounter = 0;
        $documentsSize = 0;
        $overdueActions = 0;

        $risksCounter = [
            'L' => 0,
            'M' => 0,
            'H' => 0,
            'V' => 0
        ];

        $sectionsThreatsTotal = 0;
        $sectionsThreatsNotAssigned = 0;

        $em = $this->getDoctrine()->getManager();
        $projects = $this->getGrantedProjects();

        if (!$projects && !$securityContext->isGranted('ROLE_SUPER_ADMIN') && !$securityContext->isGranted('ROLE_ADMIN')){
            return $this->render(
                'AIEVeracityBundle:Default:accessdenied.html.twig'
            );
        }

        $projectsCounter = count($projects);
        foreach ($projects as $project)
        {
            $matrix = json_decode($project->getRiskMatrix()->getMatrix(), true);

            $pipelines = $project->getP();
            $pipelinesCounter += count($pipelines);
            foreach ($pipelines as $pipeline)
            {
                $specifications = $pipeline->getPs();
                if (isset($specifications[0]))
                {
                    $totalPipelinesLength += $specifications[0]->getValue();
                }

                $documents = $pipeline->getPf();
                $documentsCounter += count($documents);
                foreach ($documents as $document)
                {
                    $path = $this->getAbsolutePath($document);
                    $documentsSize += $document->getSize();
                }
                $documentsSize = FileHelper::fileSizeConvert($documentsSize);


                $sectionsThreatsMitigations = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findByPipelineId($pipeline->getId());

                //calculating activity management
                $sectionsThreatsTotal += count($sectionsThreatsMitigations);

                $sectionsThreats = array();
                foreach ($sectionsThreatsMitigations as $stm)
                {
                    $sectionThreat = $stm->getSectionsThreats();
                    if (! isSet($sectionsThreats[$sectionThreat->getId()]))
                    {
                        $sectionsThreats[$sectionThreat->getId()] = $sectionThreat;
                    }
                    if (! $stm->getActionOwner())
                    {
                        $sectionsThreatsNotAssigned ++;
                    }

                    if ($stm->getLastInspectionDate() && $stm->getIsInspectionDue())
                    {
                        $overdueActions ++;
                    }
                }

                foreach ($sectionsThreats as $sectionThreat)
                {
                    //calculating risks
                    $sectionThreatConsequences = $sectionThreat->getStc();
                    $maxSeverity = 0;
                    foreach ($sectionThreatConsequences as $consequence)
                    {
                        $severity = $consequence->getServerity() - 1;
                        if ($maxSeverity < $severity)
                        {
                            $maxSeverity = $severity;
                        }
                    }

                    $probability = $sectionThreat->getAdjProbabilityOfFailer() - 1;
                    $level = $matrix[$maxSeverity][$probability];

                    $risksCounter[$level[0]] ++; // = (isset($severitiesCounter[$level]))? $severitiesCounter[$level] + 1: 0;
                }
            }
        }

        return array(
            'projectsCounter'            => $projectsCounter,
            'pipelinesCounter'           => $pipelinesCounter,
            'totalPipelinesLength'       => $totalPipelinesLength,
            'documentsCounter'           => $documentsCounter,
            'documentsSize'              => $documentsSize,
            'risksCounter'               => $risksCounter,
            'sectionsThreatsTotal'       => $sectionsThreatsTotal,
            'sectionsThreatsNotAssigned' => $sectionsThreatsNotAssigned,
            'overdueActions'             => $overdueActions,

        );
    }

}
