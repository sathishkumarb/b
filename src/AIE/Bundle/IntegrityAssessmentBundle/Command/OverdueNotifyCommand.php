<?php
namespace AIE\Bundle\IntegrityAssessmentBundle\Command;


use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class OverdueNotifyCommand extends ContainerAwareCommand {

    //php app/console aie:notify_overdue
    protected function configure()
    {
        $this
            ->setName('aie:notify_overdue')
            ->setDescription('Greet someone')
            ->addOption(
                'month',
                null,
                InputOption::VALUE_NONE,
                'If set, the action owner will get notified for all the actions coming in the next 30 days'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $container->enterScope('request');
        $container->set('request', new Request(), 'request');

        $mailer = $container->get('mailer');
        $templating = $container->get('templating');
        $em = $container->get('doctrine')->getEntityManager();

        $from_address = [$container->getParameter('mailer_email') => $container->getParameter('mailer_name')];

        //GET ALL OVERDUE ACTIONS
        $overdueActions = null;

        if ($input->getOption('month'))
        {
            $overdueActions = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findOverDue(true);

        } else
        {
            $overdueActions = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findOverDue();
        }

        $pipelineOwnersActions = [];

        $sectionActions = [];
        foreach ($overdueActions as $key => $action)
        {
            $action = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->find($action['id']);

            if (! $action)
                continue;

            $mitigation = $action->getMitigationTechnique();
            $sectionThreat = $action->getSectionsThreats();
            $pipelineSection = $sectionThreat->getPipelineSections();
            $pipeline = $pipelineSection->getPipeline();
            $dueDate = $action->getNextInspectionDate();
            $actionOwner = $action->getActionOwner();


            if (! $actionOwner)
                continue;

            if (! isSet($pipelineOwnersActions[$pipeline->getId()]))
            {
                $pipelineOwnersActions[$pipeline->getId()] = [
                    'pipeline' => $pipeline,
                    'owners'   => []
                ];
            }


            if (! isSet($pipelineOwnersActions[$pipeline->getId()]['owners'][$actionOwner->getId()]))
            {
                $pipelineOwnersActions[$pipeline->getId()]['owners'][$actionOwner->getId()] = [
                    'owner'   => $actionOwner,
                    'actions' => []
                ];
            }

            //$sectionActions[SECTION][MITIGATION]
            $actionName = $mitigation->getMitigationTechnique();
            if (isSet($sectionActions[$pipelineSection->getId()]))
            {
                if (isSet($sectionActions[$pipelineSection->getId()][$actionName]))
                {
                    $index = $sectionActions[$actionName];
                    if ($sectionActions[$index]->getFrequency() > $action->getFrequency())
                    {
                        unset($pipelineOwnersActions[$pipeline->getId()]['owners'][$actionOwner->getId()]['actions'][$index]);
                    } else
                    {
                        continue;
                    }
                } else
                {
                    $sectionActions[$pipelineSection->getId()][$actionName] = $key;
                }
            } else
            {
                $sectionActions[$pipelineSection->getId()] = [];
                $sectionActions[$pipelineSection->getId()][$actionName] = $key;
            }


            $pipelineOwnersActions[$pipeline->getId()]['owners'][$actionOwner->getId()]['actions'][$key] = $action;
        }

        $today = new \DateTime('now');
        $monthYear = $today->format('F Y');
        foreach ($pipelineOwnersActions as $poa)
        {

            $pipeline = $poa['pipeline'];

            foreach ($poa['owners'] as $owner)
            {

                $actionOwner = $owner['owner'];
                $actions = $owner['actions'];

                if (empty($actions))
                    continue;

                $message = new \Swift_Message();
                $message->setContentType("text/html");
                $message->setSubject('REMINDER: ' . $pipeline->getName() . ' - ' . $monthYear . ' Planned and Overdue Activities')
                    ->setFrom($from_address)
                    ->setTo($actionOwner->getEmail())
                    ->setBody(
                        $templating->render(
                            'AIEIntegrityAssessmentBundle:Email:overdue.html.twig',
                            [
                                'owner'   => $actionOwner,
                                'actions' => $actions,
                            ]
                        )
                    );

                $mailer->send($message);

                foreach ($actions as $action)
                {
                    $remindedOn = $action->getRemindedOn();
                    if (! is_array($remindedOn) || empty($remindedOn))
                    {
                        $remindedOn = array();
                    }
                    $remindedOn = $remindedOn + [new \DateTime('now')];
                    $action->setRemindedOn($remindedOn);
                    $em->persist($action);
                }

            }
        }

        // now manually flush the queue
        $spool = $mailer->getTransport()->getSpool();
        $transport = $container->get('swiftmailer.transport.real');

        $spool->flushQueue($transport);
        $em->flush();
        $output->writeln('Sent!');
    }
} 