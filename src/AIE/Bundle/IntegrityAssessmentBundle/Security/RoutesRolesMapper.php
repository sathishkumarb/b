<?php
/**
 * User: Mokha
 * Date: 6/30/14
 * Time: 7:10 PM
 */

namespace AIE\Bundle\IntegrityAssessmentBundle\Security;


trait RoutesRolesMapper {

    protected $_routesRoles = [
        'admin_dashboard'                           => 'ROLE_ADMIN',
        /* Project */
        'projects_create'                           => 'ROLE_PROJECT_ADD',
        'projects_new'                              => 'ROLE_PROJECT_ADD',
        'projects_show'                             => 'ROLE_PROJECT_SHOW',
        'projects_edit'                             => 'ROLE_PROJECT_EDIT',
        'projects_update'                           => 'ROLE_PROJECT_EDIT',
        'projects_delete'                           => 'ROLE_PROJECT_DELETE',
        'projects_post_delete'                      => 'ROLE_PROJECT_DELETE',

        /* Project Consequences */
        'projectsconsequences'                      => 'ROLE_PROJECT_CONSEQUENCES',
        'projectsconsequences_create'               => 'ROLE_PROJECT_CONSEQUENCES',

        /* Project Risk Matrix */
        'riskmatrix_create'                         => 'ROLE_PROJECT_RISKMATRIX',
        'riskmatrix_new'                            => 'ROLE_PROJECT_RISKMATRIX',

        /* Project Base Data */
        'projectmitigationtechniques'               => 'ROLE_PROJECT_BASEDATA_LIST',
        'projectmitigationtechniques_create'        => 'ROLE_PROJECT_BASEDATA_ADD',
        'projectmitigationtechniques_new'           => 'ROLE_PROJECT_BASEDATA_ADD',
        'projectmitigationtechniques_show'          => 'ROLE_PROJECT_BASEDATA_LIST',
        'projectmitigationtechniques_edit'          => 'ROLE_PROJECT_BASEDATA_EDIT',
        'projectmitigationtechniques_update'        => 'ROLE_PROJECT_BASEDATA_EDIT',
        'projectmitigationtechniques_delete'        => 'ROLE_PROJECT_BASEDATA_EDIT',


        /* Pipeline */
        'pipelines'                                 => 'ROLE_PIPELINES_LIST',
        'pipelines_create'                          => 'ROLE_PIPELINE_ADD',
        'pipelines_new'                             => 'ROLE_PIPELINE_ADD',
        'pipelines_show'                            => 'ROLE_PIPELINE_SHOW',
        'pipelines_edit'                            => 'ROLE_PIPELINE_EDIT',
        'pipelines_update'                          => 'ROLE_PIPELINE_EDIT',
        'pipelines_delete'                          => 'ROLE_PIPELINE_DELETE',
        'pipelines_post_delete'                     => 'ROLE_PIPELINE_DELETE',

        /* Loadsheet */
        'loadsheet'                          => 'ROLE_LOADSHEET_LIST',
        'pplDownload_loadsheet'                 => 'ROLE_LOADSHEET_LIST',
        'loadsheet_assessment'                    => 'ROLE_LOADSHEET_LIST',
        'Download_assess_loadsheet'                 => 'ROLE_LOADSHEET_LIST',
        'loadsheet_risk'                            => 'ROLE_LOADSHEET_LIST',
        'Download_riskAssess_loadsheet'                 => 'ROLE_LOADSHEET_LIST',


        /* Pipeline Design Data */
        'pipelinesspecifications'                   => 'ROLE_PIPELINE_DESIGN_DATA',
        'pipelinesspecifications_update'            => 'ROLE_PIPELINE_DESIGN_DATA',

        /* Pipeline Files */
        'pipelinefiles'                             => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_create'                      => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_new'                         => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_show'                        => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_edit'                        => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_update'                      => 'ROLE_PIPELINE_FILES',
        'pipelinefiles_delete'                      => 'ROLE_PIPELINE_FILES',
        'files_categories'                          => 'ROLE_PIPELINE_FILES',
        'files_categories_create'                   => 'ROLE_PIPELINE_FILES',
        'files_categories_update'                   => 'ROLE_PIPELINE_FILES',
        'files_categories_delete'                   => 'ROLE_PIPELINE_FILES',
        'files_categories_move'                     => 'ROLE_PIPELINE_FILES',
        'files_move'                                => 'ROLE_PIPELINE_FILES',
        'ajax_pipeline_files'                       => 'ROLE_PIPELINE_FILES',

        /* Pipeline Sections */
        'pipelinesections'                          => 'ROLE_PIPELINE_SECTIONS_LIST',
        'pipelinesections_create'                   => 'ROLE_PIPELINE_SECTIONS_ADD',
        'pipelinesections_new'                      => 'ROLE_PIPELINE_SECTIONS_ADD',
        'pipelinesections_edit'                     => 'ROLE_PIPELINE_SECTIONS_EDIT',
        'pipelinesections_update'                   => 'ROLE_PIPELINE_SECTIONS_EDIT',
        'pipelinesections_delete'                   => 'ROLE_PIPELINE_SECTIONS_DELETE',
        'pipelinesections_post_delete'              => 'ROLE_PIPELINE_SECTIONS_DELETE',

        /* Pipeline Sections Threats */
        'sectionsthreats_list'                      => 'ROLE_SECTIONS_THREATS',
        'sectionsthreats_create'                    => 'ROLE_SECTIONS_THREATS',

        /* Sections Threats Mitigations */
        'sectionsthreatsmitigations'                => 'ROLE_THREATS_MITIGATIONS',
        'sectionsthreatsmitigations_create'         => 'ROLE_THREATS_MITIGATIONS',

        /* Sections THreats Mitigation Status */
        'sectionsthreatsmitigations_details'        => 'ROLE_MITIGATION_STATUS',
        'sectionsthreatsmitigations_details_create' => 'ROLE_MITIGATION_STATUS',

        /* Sections Likelyhoods */
        'sectionsthreats_liklyhood'                 => 'ROLE_THREATS_LIKELYHOOD',
        'sectionsthreats_liklyhood_create'          => 'ROLE_THREATS_LIKELYHOOD',

        /* Sections Consequences Assessment */
        'consequenceassessment'                     => 'ROLE_CONSEQUENCES_ASSESSMENT',
        'consequenceassessment_create'              => 'ROLE_CONSEQUENCES_ASSESSMENT',
        /* Risks */
        'threats_risks'                             => 'ROLE_THREATS_RISKS',
        /* Mitigation Freq */
        'pipelineriskassessment'                    => 'ROLE_MITIGATIONS_FREQUENCIES',
        'pipelineriskassessment_create'             => 'ROLE_MITIGATIONS_FREQUENCIES',
        /* Workshops */
        'workshops'                                 => 'ROLE_WORKSHOPS',
        'workshops_create'                          => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_new'                             => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_show'                            => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_edit'                            => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_update'                          => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_delete'                          => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_attendee_create'                 => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_attendee_new'                    => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_attendee_edit'                   => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_attendee_update'                 => 'ROLE_WORKSHOPS_MANAGE',
        'workshops_attendee_delete'                 => 'ROLE_WORKSHOPS_MANAGE',
        /* Action Owners */
        'pipelineemails'                            => 'ROLE_ACTION_OWNERS',
        'pipelineemails_create'                     => 'ROLE_ACTION_OWNERS',
        'pipelineemails_new'                        => 'ROLE_ACTION_OWNERS',
        'pipelineemails_show'                       => 'ROLE_ACTION_OWNERS',
        'pipelineemails_edit'                       => 'ROLE_ACTION_OWNERS',
        'pipelineemails_update'                     => 'ROLE_ACTION_OWNERS',
        'pipelineemails_delete'                     => 'ROLE_ACTION_OWNERS',
        /* Activities */
        'inspection_dates'                          => 'ROLE_ACTIVITIES',
        'update_inspection_dates'                   => 'ROLE_ACTIVITIES',
        /* Planning */
        'inspections_planning'                      => 'ROLE_PLANNING',
        /* Reminders */
        'inspections_reminders'                     => 'ROLE_REMINDERS',
        /* Generate Report */
        'assessments_create'                        => 'ROLE_GENERATE_ASSESSMENT',
        'assessment_report_pdf'                     => 'ROLE_GENERATE_ASSESSMENT',
        'assessment_chart'                          => 'ROLE_GENERATE_ASSESSMENT',
        /* Archived Reports */
        'assessments'                               => 'ROLE_SHOW_ARCHIVE_REPORTS',
        /* Approved Reports */
        'approved_assessments'                      => 'ROLE_SHOW_APPROVED_REPORTS',
        /* Approve Report */
        'riskassessment_approve'                    => 'ROLE_APPROVE_ASSESSMENT',
        /* Show Reports */
        'assessment_report'                         => 'ROLE_REPORT_SHOW',
        'assessment_report_docx'                    => 'ROLE_REPORT_SHOW',
        'assessment_section_gmap'                   => 'ROLE_REPORT_SHOW',



        'sections'                                  => 'ROLE_SECTIONS_LIST',
        'sections_create'                           => 'ROLE_SECTIONS_ADD',
        'sections_new'                              => 'ROLE_SECTIONS_ADD',
        'sections_show'                             => 'ROLE_SECTIONS_LIST',
        'sections_edit'                             => 'ROLE_SECTIONS_EDIT',
        'sections_update'                           => 'ROLE_SECTIONS_EDIT',
        'sections_delete'                           => '',
        'threats'                                   => 'ROLE_THREATS_LIST',
        'threats_create'                            => 'ROLE_THREATS_ADD',
        'threats_new'                               => 'ROLE_THREATS_ADD',
        'threats_show'                              => 'ROLE_THREATS_LIST',
        'threats_edit'                              => 'ROLE_THREATS_EDIT',
        'threats_update'                            => 'ROLE_THREATS_EDIT',
        'threats_delete'                            => '',
        'consequencescategories'                    => 'ROLE_CONSEQUENCES_LIST',
        'consequencescategories_create'             => 'ROLE_CONSEQUENCES_ADD',
        'consequencescategories_new'                => 'ROLE_CONSEQUENCES_ADD',
        'consequencescategories_show'               => 'ROLE_CONSEQUENCES_LIST',
        'consequencescategories_edit'               => 'ROLE_CONSEQUENCES_EDIT',
        'consequencescategories_update'             => 'ROLE_CONSEQUENCES_EDIT',
        'consequencescategories_delete'             => '',
        'mitigationtechniques'                      => 'ROLE_MITIGATIONS_LIST',
        'mitigationtechniques_create'               => 'ROLE_MITIGATIONS_ADD',
        'mitigationtechniques_new'                  => 'ROLE_MITIGATIONS_ADD',
        'mitigationtechniques_show'                 => 'ROLE_MITIGATIONS_LIST',
        'mitigationtechniques_edit'                 => 'ROLE_MITIGATIONS_EDIT',
        'mitigationtechniques_update'               => 'ROLE_MITIGATIONS_EDIT',
        'mitigationtechniques_delete'               => '',
        /* Positions */
        'userprojectgroup'                          => 'ROLE_POSITIONS',
        'userprojectgroup_create'                   => 'ROLE_POSITIONS',
        'userprojectgroup_new'                      => 'ROLE_POSITIONS',
        'userprojectgroup_show'                     => 'ROLE_POSITIONS',
        'userprojectgroup_edit'                     => 'ROLE_POSITIONS',
        'userprojectgroup_update'                   => 'ROLE_POSITIONS',
        'userprojectgroup_delete'                   => 'ROLE_POSITIONS',
        /* Users */
        'fos_user_registration_register'            => 'ROLE_USERS',
        'fos_user_registration_check_email'         => 'ROLE_USERS',
        'fos_user_registration_confirm'             => 'ROLE_USERS',
        'fos_user_registration_confirmed'           => 'ROLE_USERS',
        'users'                                     => 'ROLE_USERS',
        'user_edit'                                 => 'ROLE_USERS',
        'user_update'                               => 'ROLE_USERS',
    ];


    public function detectSecureObject($request, $em)
    {

        //routes that will contain object
        $projectRoutes = [
            /* Project */
            'projects_show',
            'projects_edit',
            'projects_update',
            'projects_delete',
            'projects_post_delete',
            /* Project Consequences */
            'projectsconsequences',
            'projectsconsequences_create',
            /* Project Risk Matrix */
            'riskmatrix_create',
            'riskmatrix_new',
            /* Project Base Data */
            'projectmitigationtechniques',
            'projectmitigationtechniques_create',
            'projectmitigationtechniques_new',
            'projectmitigationtechniques_show',
            'projectmitigationtechniques_edit',
            'projectmitigationtechniques_update',
            'projectmitigationtechniques_delete',
            /* Project Risk Matrix */
            'riskmatrix_create',
            'riskmatrix_new',
            /* Pipeline */
            'pipelines',
            'pipelines_create',
            'pipelines_new',
            'pipelines_delete',
            'pipelines_post_delete',
            'loadsheet',
            'loadsheet_assessment',
        ];

        $projectPipelineRoutes = [
            /* Pipeline */
            'pipelines_show',
            'pipelines_edit',
            'pipelines_update',
            /* Pipeline Design Data */
            'pipelinesspecifications',
            'pipelinesspecifications_update',
            /* Pipeline Files */
            'pipelinefiles',
            'pipelinefiles_create',
            'pipelinefiles_new',
            'pipelinefiles_show',
            'pipelinefiles_edit',
            'pipelinefiles_update',
            'pipelinefiles_delete',
            'files_categories',
            'files_categories_create',
            'files_categories_update',
            'files_categories_delete',
            'files_categories_move',
            'files_move',
            'ajax_pipeline_files',
            /* Pipeline Sections */
            'pipelinesections',
            'pipelinesections_create',
            'pipelinesections_new',
            'pipelinesections_show',
            'pipelinesections_edit',
            'pipelinesections_update',
            'pipelinesections_delete',
            'pipelinesections_post_delete',
            /* Sections Threats */
            'sectionsthreats_list',
            'sectionsthreats_create',
            /* Sections Threats Mitigations */
            'sectionsthreatsmitigations',
            'sectionsthreatsmitigations_create',
            /* Sections THreats Mitigation Status */
            'sectionsthreatsmitigations_details',
            'sectionsthreatsmitigations_details_create',
            /* Sections Likelyhood */
            'sectionsthreats_liklyhood',
            'sectionsthreats_liklyhood_create',
            /* Sections Consequences Assessment */
            'consequenceassessment',
            'consequenceassessment_create',
            /* Risks */
            'threats_risks',
            /* Mitigation Freq */
            'pipelineriskassessment',
            'pipelineriskassessment_create',
            /* Mitigation Freq */
            'pipelineriskassessment',
            'pipelineriskassessment_create',
            /* Workshops */
            'workshops',
            'workshops_create',
            'workshops_new',
            'workshops_show',
            'workshops_edit',
            'workshops_update',
            'workshops_delete',
            'workshops_attendee_create',
            'workshops_attendee_new',
            'workshops_attendee_edit',
            'workshops_attendee_update',
            'workshops_attendee_delete',
            /* Action Owners */
            'pipelineemails',
            'pipelineemails_create',
            'pipelineemails_new',
            'pipelineemails_show',
            'pipelineemails_edit',
            'pipelineemails_update',
            'pipelineemails_delete',
            /* Activities */
            'inspection_dates',
            'update_inspection_dates',
            /* Planning */
            'inspections_planning',
            /* Reminders */
            'inspections_reminders',
            /* Generate Report */
            'assessments_create',
            'assessment_report_pdf',
            'assessment_chart',
            /* Archived Reports */
            'assessments',
            /* Approved Reports */
            'approved_assessments',
            /* Approve Report */
            'riskassessment_approve',
            /* Show Reports */
            'assessment_report',
            'assessment_report_docx',
            'assessment_section_gmap',




            'pipelineriskassessment',
        ];

        $projectId = null;
        //choose first parameter if project


        //if pipeline, get pipeline then get project
        $route = $request->get('_route');
        if (in_array($route, $projectRoutes))
        {
            $projectId = $request->get('projectId', $request->get('id'));
        } else if (in_array($route, $projectPipelineRoutes))
        {
            $pipelineId = $request->get('pipelineId', $request->get('id'));
            $pipeline = $em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
            $projectId = $pipeline->getProject()->getId();
        } else
        {
            return null;
        }

        //return project else null
        return $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);;

    }
} 
