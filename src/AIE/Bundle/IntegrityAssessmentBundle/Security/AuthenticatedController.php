<?php
namespace AIE\Bundle\IntegrityAssessmentBundle\Security;

use AIE\Bundle\IntegrityAssessmentBundle\Security\SecureObjectInterface;

interface AuthenticatedController {

    public function getSecureObject();

    /*
     * [
     * 'ROUTE' => 'ROLE_NAME'
     * ]
     */
    public function getRoutesRoles();
} 