<?php

// src/Acme/MainBundle/Menu/MenuBuilder.php

namespace AIE\Bundle\IntegrityAssessmentBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilder {

    private $factory;
    private $securityContext;
    private $entityManager;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, $em, $securityContext)
    {
        $this->factory = $factory;
        $this->securityContext = $securityContext;

        $this->entityManager = $em;
    }

    public function createMainLeftMenu(Request $request)
    {

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu->setChildrenAttribute('id', 'top-nav');


        //$menu->addChild('Lable', array('label'=>'hello'));
        $menu->addChild('Dashboard', array('route' => "index"));
        $menu->addChild('Projects', array('route' => 'projects'));

        switch ($request->get('_route'))
        {


            case 'projects_edit':
            case 'projects_show':
            case 'projects_delete':
            case 'projectsconsequences':
            case 'riskmatrix_new':
            case 'pipelines':
            case 'pipelines_delete':
            case 'pipelines_show':
            case 'pipelines_new':
            case 'loadsheet':
            case 'loadsheet_assessment':
            case 'loadsheet_risk':
            case 'projectmitigationtechniques':
            case 'projectmitigationtechniques_edit':
            case 'projectmitigationtechniques_new':
                $projectId = $request->get('projectId', $request->get('id'));
                break;


            case 'pipelines_show':
            case 'pipelines_edit':
            case 'pipeline_files':
            case 'pipeline_files_new':
            case 'pipeline_files_edit':
            case 'pipelineemails':
            case 'pipelineemails_new':
            case 'pipelineemails_edit':
            case 'pipelinesections':
            case 'pipelinesections_new':
            case 'pipelinesections_edit':
            case 'pipelinesections_delete':
            case 'pipelinesspecifications_new':
            case 'pipelinesspecifications_edit':
            case 'pipelinesspecifications':
            case 'sectionsthreats_list':
            case 'sectionsthreatsmitigations':
            case 'sectionsthreatsmitigations_details':
            case 'sectionsthreats_liklyhood':
            case 'consequenceassessment':
            case 'pipelineriskassessment':
            case 'threats_risks':
            case 'inspections_planning':
            case 'inspections_reminders':
            case 'inspection_dates':
            case 'workshops':
            case 'workshops_new':
            case 'workshops_edit':
            case 'workshops_attendee_new':
            case 'workshops_attendee_edit':
            case 'assessments':
            case 'approved_assessments':
                $pipelineId = $request->get('pipelineId', $request->get('id'));
                $pipeline = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
                $projectId = $pipeline->getProject()->getId();
                break;

            default:
                $projectId = false;
                break;
        }

        if (! $projectId)
            return $menu;

        $project = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);


        $menu->addChild('Project', array('label' => $project->getName()))
            ->setAttribute('dropdown', true)
            ->setAttribute('class', 'dropdown');


        if ($this->securityContext->isGranted('ROLE_PROJECT_CONSEQUENCES', $project))
            $menu['Project']->addChild('Consequences', array('route' => 'projectsconsequences', 'routeParameters' => array('projectId' => $projectId)));
        if ($this->securityContext->isGranted('ROLE_PROJECT_RISKMATRIX', $project))
            $menu['Project']->addChild('Risk Matrix', array('route' => 'riskmatrix_new', 'routeParameters' => array('projectId' => $projectId)));
        if ($this->securityContext->isGranted('ROLE_PROJECT_BASEDATA_LIST', $project))
            $menu['Project']->addChild('Mitigation Techniques', array('route' => 'projectmitigationtechniques', 'routeParameters' => array('projectId' => $projectId)));
        if ($this->securityContext->isGranted('ROLE_PIPELINES_LIST', $project))
            $menu['Project']->addChild('Pipelines', array('route' => 'pipelines', 'routeParameters' => array('projectId' => $projectId)));



        /*
          $menu->addChild('User')
          ->setAttribute('dropdown', true)
          ->setAttribute('class', 'dropdown');

          $menu['User']->addChild('Profile', array('uri' => '#'));
          $menu['User']->addChild('Logout', array('uri' => '#'));
         */


        //for security
        /*      $securityContext = $this->container->get('security.context');
          if (!$securityContext->isGranted('ROLE_ADMIN')) {
          $menu->addChild('About Me', array(
          'route' => 'page_show',
          'routeParameters' => array('id' => 42)
          ));
          }
         */

        return $menu;
    }

    public function createMainRightMenu(Request $request)
    {

        $user = $this->securityContext->getToken()->getUser();

        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');


        /* User Security */
        if ($this->securityContext->isGranted('ROLE_ADMIN'))
            $menu->addChild('Admin', array('label' => 'Admin Panel', 'route' => 'admin_dashboard'));


        $menu->addChild('User', array('label' => $user->getUsername()))
            ->setAttribute('dropdown', true)
            ->setAttribute('class', 'dropdown');

        $menu['User']->addChild('Profile', array('route' => 'fos_user_profile_show'));
        $menu['User']->addChild('Change Password', array('route' => 'fos_user_change_password'))->setAttribute('divider_append', true);

        $menu['User']->addChild('Logout', array('route' => 'fos_user_security_logout'));


        //for security
        /*      $securityContext = $this->container->get('security.context');
          if (!$securityContext->isGranted('ROLE_ADMIN')) {
          $menu->addChild('About Me', array(
          'route' => 'page_show',
          'routeParameters' => array('id' => 42)
          ));
          }
         */

        return $menu;
    }

    public function createLeftNav(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');


        //$menu->addChild('User', array('route' => 'index', 'extras' => array('icon' => 'home')))->setLinkAttribute('class', 'sideicon');
        switch ($request->get('_route'))
        {
            case 'index':
                break;
            case 'projects':
            case 'projects_new':
            case 'projects_delete':
                $menu->addChild('List All', array('route' => 'projects'));
                if ($this->securityContext->isGranted('ROLE_PROJECT_ADD'))
                    $menu->addChild('Add New', array('route' => 'projects_new'));
                break;
            case 'projects_edit':
            case 'projects_show':
            case 'projectsconsequences':
            case 'riskmatrix_new':
            case 'pipelines':
            case 'pipelines_delete':
            case 'projectmitigationtechniques':
            case 'projectmitigationtechniques_edit':
            case 'projectmitigationtechniques_new':
            case 'pipelines_new':
            case 'loadsheet':
            case 'loadsheet_assessment':
            case 'loadsheet_risk':
                $projectId = $request->get('projectId', $request->get('id'));

                $project = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);

                $basedata = $menu->addChild('Project Setup', array('uri' => '#'))
                    ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                    $basedata->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
                if ($this->securityContext->isGranted('ROLE_PROJECT_CONSEQUENCES', $project))
                    $basedata->addChild('Consequences', array('route' => 'projectsconsequences', 'routeParameters' => array('projectId' => $projectId)));

                if ($this->securityContext->isGranted('ROLE_PROJECT_RISKMATRIX', $project))
                    $basedata->addChild('Risk Matrix', array('route' => 'riskmatrix_new', 'routeParameters' => array('projectId' => $projectId)));

                if ($this->securityContext->isGranted('ROLE_PROJECT_BASEDATA_LIST', $project))
                    $basedata->addChild('Mitigation Techniques', array('route' => 'projectmitigationtechniques', 'routeParameters' => array('projectId' => $projectId)));

                if ($this->securityContext->isGranted('ROLE_PIPELINES_LIST', $project))
                    $menu->addChild('Pipelines', array('route' => 'pipelines', 'routeParameters' => array('projectId' => $projectId)));


            if ($this->securityContext->isGranted('ROLE_LOADSHEET_LIST', $project))
            {
                $loadsheet = $menu->addChild('Load Sheet', array('uri' => '#'))
                    ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                $loadsheet->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
//        if ($this->securityContext->isGranted('ROLE_LOADSHEET_PIPELINE_DETAILS', $project))
            $loadsheet->addChild('Pipeline Details', array('route' => 'loadsheet', 'routeParameters' => array('projectId' => $projectId)));
//        if ($this->securityContext->isGranted('ROLE_LOADSHEET_ASSESSMENT_DETAILS', $project))
            $loadsheet->addChild('Mitigation Status', array('route' => 'loadsheet_assessment', 'routeParameters' => array('projectId' => $projectId)));
            $loadsheet->addChild('POF/COF Details', array('route' => 'loadsheet_risk', 'routeParameters' => array('projectId' => $projectId)));
            }
//            if ($this->securityContext->isGranted('ROLE_LOADSHEET_LIST', $project))
//                    $menu->addChild('Load Sheet', array('route' => 'loadsheet', 'routeParameters' => array('projectId' => $projectId)));
                /*$pipelines = $menu->addChild('Pipelines', array('uri' => '#', 'extras' => array('icon' => 'filter')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
                $pipelines->setChildrenAttribute('class', 'subnav')->setAttribute('dropdown', true);
                $pipelines->addChild('List All', array('route' => 'pipelines', 'routeParameters' => array('projectId' => $projectId), 'extras' => array('icon' => 'list')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
                $pipelines->addChild('Add', array('route' => 'pipelines_new', 'routeParameters' => array('projectId' => $projectId), 'extras' => array('icon' => 'plus')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');*/
                break;
            case 'pipelines_show':
            case 'pipelines_edit':
            case 'pipeline_files':
            case 'pipeline_files_new':
            case 'pipeline_files_edit':
            case 'pipelinesections':
            case 'pipelinesections_new':
            case 'pipelinesections_edit':
            case 'pipelinesections_delete':
            case 'pipelineemails':
            case 'pipelineemails_new':
            case 'pipelineemails_edit':
            case 'pipelinesspecifications_new':
            case 'pipelinesspecifications_edit':
            case 'pipelinesspecifications':
            case 'sectionsthreats_list':
            case 'sectionsthreatsmitigations':
            case 'sectionsthreatsmitigations_details':
            case 'consequenceassessment':
            case 'sectionsthreats_liklyhood':
            case 'pipelineriskassessment':
            case 'threats_risks':
            case 'inspections_planning':
            case 'inspections_reminders':
            case 'inspection_dates':
            case 'workshops':
            case 'workshops_new':
            case 'workshops_edit':
            case 'workshops_attendee_new':
            case 'workshops_attendee_edit':
            case 'assessments':
            case 'approved_assessments':
                $pipelineId = $request->get('pipelineId', $request->get('id'));
                $pipeline = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
                $projectId = $pipeline->getProject()->getId();
                $project = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);

                if ($this->securityContext->isGranted('ROLE_PIPELINE_DESIGN_DATA', $project))
                    $menu->addChild('Pipeline Details', array('route' => 'pipelinesspecifications', 'routeParameters' => array('pipelineId' => $pipelineId)));

                if ($this->securityContext->isGranted('ROLE_PIPELINE_FILES', $project))
                    $menu->addChild('Documents', array('route' => 'pipeline_files', 'routeParameters' => array('pipelineId' => $pipelineId)));

                //$menu->addChild('Design Data', array('route' => 'pipelinesspecifications', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'list-alt')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');

                $assessmentSetup = $menu->addChild('Assessment Setup', array('uri' => '#'))->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                $assessmentSetup->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
                if ($this->securityContext->isGranted('ROLE_PIPELINE_SECTIONS_LIST', $project))
                    $assessmentSetup->addChild('Sections', array('route' => 'pipelinesections', 'routeParameters' => array('pipelineId' => $pipelineId)));

                if ($this->securityContext->isGranted('ROLE_SECTIONS_THREATS', $project))
                    $assessmentSetup->addChild('Threat Matrix', array('route' => 'sectionsthreats_list', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_THREATS_MITIGATIONS', $project))
                    $assessmentSetup->addChild('Mitigation Techniques', array('route' => 'sectionsthreatsmitigations', 'routeParameters' => array('pipelineId' => $pipelineId)));
                /*$sections = $menu->addChild('Sections', array('uri' => '#', 'extras' => array('icon' => 'road')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
                $sections->setChildrenAttribute('class', 'subnav')->setAttribute('dropdown', true);

                $sections->addChild('List All', array('route' => 'pipelinesections', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'list')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
                $sections->addChild('Add New', array('route' => 'pipelinesections_new', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'plus')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');*/

                //$menu->addChild('Threats', array('route' => 'sectionsthreats_list', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'list')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');
                //$menu->addChild('List Threats', array('route' => 'sectionsthreats', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'list')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');

                if ($this->securityContext->isGranted('ROLE_MITIGATION_STATUS', $project))
                    $menu->addChild('Mitigation Status', array('route' => 'sectionsthreatsmitigations_details', 'routeParameters' => array('pipelineId' => $pipelineId)));

                $ra = $menu->addChild('Risk Assessment', array('uri' => '#'))
                    ->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                //$ra->setChildrenAttribute('style', 'display: block;');
            $ra->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
                if ($this->securityContext->isGranted('ROLE_THREATS_LIKELYHOOD', $project))
                    $ra->addChild('Likelihoods', array('route' => 'sectionsthreats_liklyhood', 'routeParameters' => array('pipelineId' => $pipelineId)));

                if ($this->securityContext->isGranted('ROLE_CONSEQUENCES_ASSESSMENT', $project))
                    $ra->addChild('Consequences', array('route' => 'consequenceassessment', 'routeParameters' => array('pipelineId' => $pipelineId)));

                if ($this->securityContext->isGranted('ROLE_THREATS_RISKS', $project))
                    $ra->addChild('Risks', array('route' => 'threats_risks', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_MITIGATIONS_FREQUENCIES', $project))
                    $ra->addChild('Mitigation Frequency', array('route' => 'pipelineriskassessment', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_WORKSHOPS', $project))
                    $ra->addChild('Workshops', array('route' => 'workshops', 'routeParameters' => array('pipelineId' => $pipelineId)));

                $activityManagement = $menu->addChild('Activity Management', array('uri' => '#', ))->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
            $activityManagement->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);

                if ($this->securityContext->isGranted('ROLE_ACTION_OWNERS', $project))
                    $activityManagement->addChild('Action Owners', array('route' => 'pipelineemails', 'routeParameters' => array('pipelineId' => $pipelineId)));

                if ($this->securityContext->isGranted('ROLE_ACTIVITIES', $project))
                    $activityManagement->addChild('Activities', array('route' => 'inspection_dates', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_PLANNING', $project))
                    $activityManagement->addChild('Planning', array('route' => 'inspections_planning', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_REMINDERS', $project))
                    $activityManagement->addChild('Reminders', array('route' => 'inspections_reminders', 'routeParameters' => array('pipelineId' => $pipelineId)));


                $reports = $menu->addChild('Reports', array('uri' => '#'))->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
            $reports->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
                if ($this->securityContext->isGranted('ROLE_SHOW_ARCHIVE_REPORTS', $project))
                    $reports->addChild('Archive', array('route' => 'assessments', 'routeParameters' => array('pipelineId' => $pipelineId)));
                if ($this->securityContext->isGranted('ROLE_SHOW_APPROVED_REPORTS', $project))
                    $reports->addChild('Approved', array('route' => 'approved_assessments', 'routeParameters' => array('pipelineId' => $pipelineId)));
                //$ra->addChild('Assesst', array('route' => 'sectionsthreatsmitigations', 'routeParameters' => array('pipelineId' => $pipelineId), 'extras' => array('icon' => 'list')))->setLinkAttribute('class', 'sideicon')->setLabelAttribute('class', 'nav-item hidden-xs hidden-sm');

                break;


            case 'admin_dashboard':
            case 'sections':
            case 'sections_new':
            case 'sections_edit':
            case 'threats':
            case 'threats_new':
            case 'threats_edit':
            case 'mitigationtechniques':
            case 'mitigationtechniques_new':
            case 'mitigationtechniques_edit':
            case 'consequencescategories':
            case 'consequencescategories_new':
            case 'consequencescategories_edit':
            case 'fos_user_registration_register':
            case 'users':
            case 'fos_user_group_list':
            case 'fos_user_group_new':
            case 'fos_user_group_edit':
            case 'fos_user_group_show':
            case 'userprojectgroup_edit':
            case 'userprojectgroup_new':
            case 'userprojectgroup':
            case 'user_edit':
                $basedata = $menu->addChild('Base Data', array('uri' => '#'))->setLinkAttribute('class', 'dropdown-toggle')->setLinkAttribute('data-toggle','dropdown')
                    ->setLinkAttribute('role','button')->setLinkAttribute('aria-haspopup','true')
                    ->setLinkAttribute('aria-expanded','false')
                    ->setLabelAttribute('class', 'caret');
                $basedata->setChildrenAttribute('class', 'dropdown-menu')->setAttribute('dropdown', true);
                if ($this->securityContext->isGranted('ROLE_SECTIONS_LIST'))
                    $basedata->addChild('Sections', array('route' => 'sections'));
                if ($this->securityContext->isGranted('ROLE_THREATS_LIST'))
                    $basedata->addChild('Threats', array('route' => 'threats'));
                //$menu['BaseData']->addChild('Mitigation Groups', array('route' => 'mitigationgroups'));
                if ($this->securityContext->isGranted('ROLE_MITIGATIONS_LIST'))
                    $basedata->addChild('Mitigation Techniques', array('route' => 'mitigationtechniques'));
                if ($this->securityContext->isGranted('ROLE_CONSEQUENCES_LIST'))
                    $basedata->addChild('Consequences', array('route' => "consequencescategories"));


                if ($this->securityContext->isGranted('ROLE_POSITIONS'))
                    $menu->addChild('Positions', array('route' => 'fos_user_group_list'));
                if ($this->securityContext->isGranted('ROLE_USERS'))
                    $menu->addChild('Users', array('route' => 'users'));

            //$menu->addChild('Admin', array('route' => 'index', 'extras' => array('icon' => 'home')))->setLinkAttribute('class', 'sideicon')->setCurrent(true);
        }

        //for security
        /*      $securityContext = $this->container->get('security.context');
          if (!$securityContext->isGranted('ROLE_ADMIN')) {
          $menu->addChild('About Me', array(
          'route' => 'page_show',
          'routeParameters' => array('id' => 42)
          ));
          }
         */

        return $menu;
    }

    public function createBreadcrumbMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'breadcrumb');


        // this item will always be displayed

        switch ($request->get('_route'))
        {

            case 'projects_edit':
            case 'projects_delete':
            case 'projects_show':
            case 'projectsconsequences':
            case 'riskmatrix_new':
            case 'pipelines':
            case 'pipelines_delete':
            case 'pipelines_show':
            case 'pipelines_new':
            case 'loadsheet':
            case 'loadsheet_assessment':
            case 'loadsheet_risk':
            case 'projectmitigationtechniques':
            case 'projectmitigationtechniques_edit':
            case 'projectmitigationtechniques_new':
                $projectId = $request->get('projectId', $request->get('id'));
                break;

            case 'pipelines_show':
            case 'pipelines_edit':
            case 'pipeline_files':
            case 'pipeline_files_new':
            case 'pipeline_files_edit':
            case 'pipelineemails':
            case 'pipelineemails_new':
            case 'pipelineemails_edit':
            case 'pipelinesections':
            case 'pipelinesections_new':
            case 'pipelinesections_edit':
            case 'pipelinesections_delete':
            case 'pipelinesspecifications_new':
            case 'pipelinesspecifications_edit':
            case 'pipelinesspecifications':
            case 'sectionsthreats_list':
            case 'sectionsthreatsmitigations':
            case 'sectionsthreatsmitigations_details':
            case 'sectionsthreats_liklyhood':
            case 'consequenceassessment':
            case 'pipelineriskassessment':
            case 'threats_risks':
            case 'inspections_planning':
            case 'inspections_reminders':
            case 'inspection_dates':
            case 'workshops':
            case 'workshops_new':
            case 'workshops_edit':
            case 'workshops_attendee_new':
            case 'workshops_attendee_edit':
            case 'assessments':
            case 'approved_assessments':
                $pipelineId = $request->get('pipelineId', $request->get('id'));
                $pipeline = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->find($pipelineId);
                $projectId = $pipeline->getProject()->getId();
                break;

            default:
                $projectId = false;
                break;
        }

        //if (!$projectId)
        return $menu;

        $menu->addChild('Home', array('route' => 'index'));

        $menu->addChild('A', array('uri' => '#'));
        $menu->addChild('B', array('uri' => '#'));
        $menu->addChild('C', array('uri' => '#'))->setCurrent(true);


        $project = $this->entityManager->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($projectId);


        $menu->addChild('Project', array('label' => $project->getName()));

        $menu->addChild('Consequences', array('route' => 'projectsconsequences', 'routeParameters' => array('projectId' => $projectId)));
        $menu->addChild('Risk Matrix', array('route' => 'riskmatrix_new', 'routeParameters' => array('projectId' => $projectId)));
        $menu->addChild('Base Data', array('route' => 'projectmitigationtechniques', 'routeParameters' => array('projectId' => $projectId)));
        $menu->addChild('Pipelines', array('route' => 'pipelines', 'routeParameters' => array('projectId' => $projectId)));


        return $menu;
    }

}
