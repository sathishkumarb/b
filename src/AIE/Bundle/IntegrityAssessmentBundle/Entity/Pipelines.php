<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Pipelines
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"project","name"},
 *     errorPath="name"
 * )
 */
class Pipelines {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="p")
     * @ORM\JoinColumn(name="projectId", referencedColumnName="id")
     * */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="battery_limits", type="text", nullable=true)
     */
    private $batteryLimits;

    /**
     * @var string
     *
     * @ORM\Column(name="folder", type="string", length=255)
     */
    private $folder;


    //http://stackoverflow.com/questions/19614805/how-to-save-a-google-maps-overlay-shape-in-the-database
    //http://jsfiddle.net/doktormolle/EdZk4/
    //http://www.geocodezip.com/blitz-gmap-editor/test4.html
    //http://geocodezip.com/v3_GoogleEx_drawing-tools_single.html

    /**
     * @var string
     *
     * @ORM\Column(name="geometry", type="text", nullable=true)
     */
    private $geometry;

    /**
     * @ORM\OneToMany(targetEntity="PipelinesSpecifications" , mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ps;

    /**
     * @ORM\OneToMany(targetEntity="PipelineSections", mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pse;

    /**
     * @ORM\OneToMany(targetEntity="PipelineRiskAssessment", mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $pra;

    /**
     * @ORM\OneToMany(targetEntity="PipelineFiles" , mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pf;

    /**
     * @ORM\OneToMany(targetEntity="PipelineFilesCategories" , mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pfc;

    /**
     * @ORM\OneToMany(targetEntity="PipelineEmails" , mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pe;

    /**
     * @ORM\OneToMany(targetEntity="RiskAssessmentWorkshops" , mappedBy="pipeline" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pw;

    /**
     * @ORM\ManyToMany(targetEntity="PipelineFiles", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="pipeline_specifications_files",
     *      joinColumns={@ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    protected $files;

    /**
     * Constructor
     */
    public function __construct() {
        $this->ps = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pse = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Pipelines
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Pipelines
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set folder
     *
     * @param string $folder
     * @return Pipelines
     */
    public function setFolder($folder) {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return string 
     */
    public function getFolder() {
        return $this->folder;
    }

    /**
     * Set geometry
     *
     * @param string $geometry
     * @return Pipelines
     */
    public function setGeometry($geometry) {
        $this->geometry = $geometry;

        return $this;
    }

    /**
     * Get geometry
     *
     * @return string 
     */
    public function getGeometry() {
        return $this->geometry;
    }

    /**
     * Add ps
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps
     * @return Pipelines
     */
    public function addP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps) {
        $this->ps[] = $ps;

        return $this;
    }

    /**
     * Remove ps
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps
     */
    public function removeP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps) {
        $this->ps->removeElement($ps);
    }

    /**
     * Get ps
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPs() {
        return $this->ps;
    }

    /**
     * Add pse
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse
     * @return Pipelines
     */
    public function addPse(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse) {
        $this->pse[] = $pse;

        return $this;
    }

    /**
     * Remove pse
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse
     */
    public function removePse(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse) {
        $this->pse->removeElement($pse);
    }

    /**
     * Get pse
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPse() {
        return $this->pse;
    }

    /**
     * Set pra
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra
     * @return Pipelines
     */
    public function setPra(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra = null) {
        $this->pra = $pra;

        return $this;
    }

    /**
     * Get pra
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment 
     */
    public function getPra() {
        return $this->pra;
    }

    public function getPipelineSpecifications() {
        $specifications = new ArrayCollection();

        foreach ($this->ps as $s) {
            $specifications[] = $s->getSpecification();
        }

        return $specifications;
    }

    public function setPipelineSpecifications($specifications) {
        foreach ($specifications as $s) {
            $ps = new PipelinesSpecifications();

            $ps->setPipeline($this);
            $ps->setSpecification($s);

            $this->addP($ps);
        }
    }

    /**
     * Add pf
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pf
     * @return Pipelines
     */
    public function addPf(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pf) {
        $this->pf[] = $pf;

        return $this;
    }

    /**
     * Remove pf
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pf
     */
    public function removePf(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pf) {
        $this->pf->removeElement($pf);
    }

    /**
     * Get pf
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPf() {
        return $this->pf;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return Pipelines
     */
    public function setProject(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects 
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * Set batteryLimits
     *
     * @param string $batteryLimits
     * @return Pipelines
     */
    public function setBatteryLimits($batteryLimits) {
        $this->batteryLimits = $batteryLimits;

        return $this;
    }

    /**
     * Get batteryLimits
     *
     * @return string 
     */
    public function getBatteryLimits() {
        return $this->batteryLimits;
    }

    /**
     * Add pra
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra
     * @return Pipelines
     */
    public function addPra(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra) {
        $this->pra[] = $pra;

        return $this;
    }

    /**
     * Remove pra
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra
     */
    public function removePra(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment $pra) {
        $this->pra->removeElement($pra);
    }


    /**
     * Add pe
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $pe
     * @return Pipelines
     */
    public function addPe(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $pe)
    {
        $this->pe[] = $pe;
    
        return $this;
    }

    /**
     * Remove pe
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $pe
     */
    public function removePe(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $pe)
    {
        $this->pe->removeElement($pe);
    }

    /**
     * Get pe
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPe()
    {
        return $this->pe;
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files
     * @return Pipelines
     */
    public function addFile(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files)
    {
        $this->files[] = $files;
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files
     */
    public function removeFile(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add pfc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $pfc
     * @return Pipelines
     */
    public function addPfc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $pfc)
    {
        $this->pfc[] = $pfc;
    
        return $this;
    }

    /**
     * Remove pfc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $pfc
     */
    public function removePfc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $pfc)
    {
        $this->pfc->removeElement($pfc);
    }

    /**
     * Get pfc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPfc()
    {
        return $this->pfc;
    }

    /**
     * Add pw
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $pw
     * @return Pipelines
     */
    public function addPw(\AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $pw)
    {
        $this->pw[] = $pw;
    
        return $this;
    }

    /**
     * Remove pw
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $pw
     */
    public function removePw(\AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $pw)
    {
        $this->pw->removeElement($pw);
    }

    /**
     * Get pw
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPw()
    {
        return $this->pw;
    }
}