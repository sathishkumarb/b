<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use AIE\Bundle\VeracityBundle\Component\Core\Projects as ProjectsBase;
use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\IntegrityAssessmentBundle\Security\SecureObjectInterface;
use AIE\Bundle\UserBundle\Entity\User;

/**
 * Projects
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity
 */
class Projects extends ProjectsBase {

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", nullable=true)
     */
    private $location;


    /**
     * @ORM\OneToMany(targetEntity="ProjectConsequences" , mappedBy="project" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pc;

    /**
     * @ORM\OneToOne(targetEntity="RiskMatrix", cascade={"all"})
     * @ORM\JoinColumn(name="risk_matrix_id", referencedColumnName="id")
     */
    protected $riskMatrix;

    /**
     * @ORM\OneToMany(targetEntity="ProjectMitigationTechniques", mappedBy="project" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pmt;

    /**
     * @ORM\OneToMany(targetEntity="Pipelines" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $p;

    /**
     * @ORM\OneToMany(targetEntity="UserProjectGroup" , mappedBy="project", cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ug;

    /**
     * Set location
     *
     * @param string $location
     * @return Projects
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences $pc
     * @return Projects
     */
    public function addPc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences $pc)
    {
        $this->pc[] = $pc;

        return $this;
    }

    /**
     * Remove pc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences $pc
     */
    public function removePc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectConsequences $pc)
    {
        $this->pc->removeElement($pc);
    }

    /**
     * Get pc
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPc()
    {
        return $this->pc;
    }

    /**
     * Add pmt
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $pmt
     * @return Projects
     */
    public function addPmt(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $pmt)
    {
        $this->pmt[] = $pmt;

        return $this;
    }

    /**
     * Remove pmt
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $pmt
     */
    public function removePmt(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $pmt)
    {
        $this->pmt->removeElement($pmt);
    }

    /**
     * Get pmt
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPmt()
    {
        return $this->pmt;
    }

    /**
     * Set riskMatrix
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskMatrix $riskMatrix
     * @return Projects
     */
    public function setRiskMatrix(\AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskMatrix $riskMatrix = null)
    {
        $this->riskMatrix = $riskMatrix;

        return $this;
    }

    /**
     * Get riskMatrix
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskMatrix
     */
    public function getRiskMatrix()
    {
        return $this->riskMatrix;
    }


    /**
     * Add p
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $p
     * @return Projects
     */
    public function addP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $p)
    {
        $this->p[] = $p;

        return $this;
    }

    /**
     * Remove p
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $p
     */
    public function removeP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $p)
    {
        $this->p->removeElement($p);
    }

    /**
     * Get p
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getP()
    {
        return $this->p;
    }


    /**
     * Add ug
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup $ug
     * @return Projects
     */
    public function addUg(\AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug[] = $ug;
    
        return $this;
    }

    /**
     * Remove ug
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup $ug
     */
    public function removeUg(\AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup $ug)
    {
        $this->ug->removeElement($ug);
    }

    /**
     * Get ug
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUg()
    {
        return $this->ug;
    }
}