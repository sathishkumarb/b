<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AIE\Bundle\VeracityBundle\Component\Core\ActionOwners as AO;

/**
 * PipelineEmails
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PipelineEmails extends AO {



    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pe")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;


    /**
     * @ORM\OneToMany(targetEntity="SectionsThreatsMitigations" , mappedBy="action_owner", fetch="EXTRA_LAZY")
     * */
    protected $pao;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pao = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelineEmails
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null)
    {
        $this->pipeline = $pipeline;
    
        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines 
     */
    public function getPipeline()
    {
        return $this->pipeline;
    }

    
    /**
     * Add pao
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $pao
     * @return PipelineEmails
     */
    public function addPao(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $pao)
    {
        $this->pao[] = $pao;
    
        return $this;
    }

    /**
     * Remove pao
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $pao
     */
    public function removePao(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $pao)
    {
        $this->pao->removeElement($pao);
    }

    /**
     * Get pao
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPao()
    {
        return $this->pao;
    }

}