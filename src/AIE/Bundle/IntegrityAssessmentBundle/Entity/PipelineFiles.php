<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * PipelineFiles
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PipelineFiles extends AbstractFile {


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $filename;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pf")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;


    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="document_number", type="string", length=2555, nullable=true)
     */
    private $document_number;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=2555, nullable=true)
     */
    private $company;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="document_date", type="datetime", nullable=true)
     */
    private $document_date;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineFilesCategories", inversedBy="pfc")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=true)
     * */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set caption
     *
     * @param string $caption
     * @return PipelineFiles
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return PipelineFiles
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelineFiles
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null)
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines
     */
    public function getPipeline()
    {
        return $this->pipeline;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PipelineFiles
     */
    public function setFilename($name)
    {
        $this->filename = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/' . $this->pipeline->getFolder();
    }

    /**
     * Set document_number
     *
     * @param string $documentNumber
     * @return PipelineFiles
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->document_number = $documentNumber;

        return $this;
    }

    /**
     * Get document_number
     *
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->document_number;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return PipelineFiles
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set document_date
     *
     * @param \DateTime $documentDate
     * @return PipelineFiles
     */
    public function setDocumentDate($documentDate)
    {
        $this->document_date = $documentDate;

        return $this;
    }

    /**
     * Get document_date
     *
     * @return \DateTime
     */
    public function getDocumentDate()
    {
        return $this->document_date;
    }


    /***
     * @return string
     */
    function getUniqueName()
    {
        return '<a class="download-attachment" target="_blank" href="#"><span class="glyphicon glyphicon-cloud-download"></span></a><span>' . $this->caption . '</span>';
    }

    function getObject(){
        return $this;
    }
    /**
     * Set category
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $category
     * @return PipelineFiles
     */
    public function setCategory(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function getAsTreeNode($fileuploader)
    {
        $filePath = $fileuploader->getAbsoluteFilePath($this->getFilepath());
        $documentDate = $this->getDocumentDate();
        if($documentDate)
        {
            $documentDate = $documentDate->format('d F Y');
        }

        return [
            'id'             => $this->getId(),
            'key'            => $this->getId(),
            'title'          => $this->getFilename(),
            'caption'        => $this->getCaption(),
            'company'        => $this->getCompany(),
            'documentNumber' => $this->getDocumentNumber(),
            'documentDate'   => $documentDate,
            'date'           => $this->getDate()->format('d F Y'),
            'filepath'       => $this->getFilepath(),
            'absolutepath'   => $filePath,
        ];
    }


    /**
     * Set size
     *
     * @param integer $size
     * @return PipelineFiles
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    public function __toString(){
        return $this->filename;
    }
}