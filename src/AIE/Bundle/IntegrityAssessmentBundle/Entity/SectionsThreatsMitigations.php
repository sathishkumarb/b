<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use AIE\Bundle\VeracityBundle\Helper\InspectionHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * SectionsThreatsMitigations
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\IntegrityAssessmentBundle\Repository\SectionsThreatsMitigationsRepository")
 */
class SectionsThreatsMitigations {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SectionsThreats", inversedBy="stm")
     * @ORM\JoinColumn(name="sections_threats_id", referencedColumnName="id")
     * */
    private $sectionsThreats;

    /**
     * @ORM\ManyToOne(targetEntity="MitigationGroups", inversedBy="stm")
     * @ORM\JoinColumn(name="mitigation_group_id", referencedColumnName="id")
     * */
    //private $mitigationGroup;

    /**
     * @ORM\ManyToOne(targetEntity="ProjectMitigationTechniques", inversedBy="stm")
     * @ORM\JoinColumn(name="mitigation_technique_id", referencedColumnName="id")
     * */
    private $mitigationTechnique;

    /**
     * @var string
     *
     * @ORM\Column(name="mitigation_status", type="text", nullable=true)
     */
    private $mitigationStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    private $recommendations;

    /**
     * @var string
     *
     * @ORM\Column(name="performance_standard", type="text", nullable=true)
     */
    private $performanceStandard;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_inspection_date", type="datetime", nullable=true)
     */
    private $last_inspection_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     *
     * @ORM\Column(name="frequency", type="integer")
     */
    private $frequency = - 1; //-1 not defined yet

    /**
     *
     * @ORM\Column(name="last_modified_frequency", type="integer", nullable=true)
     */
    private $last_modified_frequency; //-1 not defined yet

    /**
     * @var string
     *
     * @ORM\Column(name="frequency_justification", type="text", nullable=true)
     */
    private $frequency_justification;
    private $next_inspection_date = null;

    /**
 * @ORM\ManyToMany(targetEntity="PipelineFiles", fetch="EXTRA_LAZY")
 * @ORM\JoinTable(name="section_threat_mitigation_files",
 *      joinColumns={@ORM\JoinColumn(name="stm_id", referencedColumnName="id")},
 *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
 * )
 */
    protected $files;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineEmails", inversedBy="pao")
     * @ORM\JoinColumn(name="action_owner", referencedColumnName="id", nullable=true)
     * */
    private $action_owner;

    /**
     * @var array
     *
     * @ORM\Column(name="reminded_on", type="json_array", nullable=true)
     */
    private $remindedOn;

    public function __construct()
    {
        $this->files = [];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mitigationStatus
     *
     * @param string $mitigationStatus
     * @return SectionsThreatsMitigations
     */
    public function setMitigationStatus($mitigationStatus)
    {
        $this->mitigationStatus = $mitigationStatus;

        return $this;
    }

    /**
     * Get mitigationStatus
     *
     * @return string
     */
    public function getMitigationStatus()
    {
        return $this->mitigationStatus;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return SectionsThreatsMitigations
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     * @return SectionsThreatsMitigations
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set last_inspection_date
     *
     * @param \DateTime $lastInspectionDate
     * @return SectionsThreatsMitigations
     */
    public function setLastInspectionDate($lastInspectionDate)
    {
        $this->last_inspection_date = $lastInspectionDate;

        return $this;
    }

    /**
     * Get last_inspection_date
     *
     * @return \DateTime
     */
    public function getLastInspectionDate()
    {
        return $this->last_inspection_date;
    }


    public function getNextInspectionDate($force = false)
    {
        if ($force === true || $this->next_inspection_date == null)
        {
            $this->next_inspection_date = InspectionHelper::calculateNextInspectionDate($this->last_inspection_date, $this->frequency);
        }

        return $this->next_inspection_date;
    }

    public function getIsInspectionDue()
    {
        //if today is the next inspection or after that 
        $now = new \DateTime('now');

        if($this->getNextInspectionDate())
           return ($this->getNextInspectionDate()->format('U') <= $now->format('U'));
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return SectionsThreatsMitigations
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sectionsThreats
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $sectionsThreats
     * @return SectionsThreatsMitigations
     */
    public function setSectionsThreats(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $sectionsThreats = null)
    {
        $this->sectionsThreats = $sectionsThreats;

        return $this;
    }

    /**
     * Get sectionsThreats
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats
     */
    public function getSectionsThreats()
    {
        return $this->sectionsThreats;
    }

    /**
     * Set mitigationTechnique
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $mitigationTechnique
     * @return SectionsThreatsMitigations
     */
    public function setMitigationTechnique(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques $mitigationTechnique = null)
    {
        $this->mitigationTechnique = $mitigationTechnique;

        return $this;
    }

    /**
     * Get mitigationTechnique
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques
     */
    public function getMitigationTechnique()
    {
        return $this->mitigationTechnique;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     * @return SectionsThreatsMitigations
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set performanceStandard
     *
     * @param string $performanceStandard
     * @return SectionsThreatsMitigations
     */
    public function setPerformanceStandard($performanceStandard)
    {
        $this->performanceStandard = $performanceStandard;

        return $this;
    }

    /**
     * Get performanceStandard
     *
     * @return string
     */
    public function getPerformanceStandard()
    {
        return $this->performanceStandard;
    }

    /**
     * Set frequency_justification
     *
     * @param string $frequencyJustification
     * @return SectionsThreatsMitigations
     */
    public function setFrequencyJustification($frequencyJustification)
    {
        $this->frequency_justification = $frequencyJustification;

        return $this;
    }

    /**
     * Get frequency_justification
     *
     * @return string
     */
    public function getFrequencyJustification()
    {
        return $this->frequency_justification;
    }

    /**
     * Add files
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files
     * @return SectionsThreatsMitigations
     */
    public function addFile(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files
     */
    public function removeFile(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set remindedOn
     *
     * @param array $remindedOn
     * @return SectionsThreatsMitigations
     */
    public function setRemindedOn($remindedOn)
    {
        $this->remindedOn = $remindedOn;

        return $this;
    }

    /**
     * Get remindedOn
     *
     * @return array
     */
    public function getRemindedOn()
    {
        return $this->remindedOn;
    }

    /**
     * Set action_owner
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $actionOwner
     * @return SectionsThreatsMitigations
     */
    public function setActionOwner(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails $actionOwner = null)
    {
        $this->action_owner = $actionOwner;

        return $this;
    }

    /**
     * Get action_owner
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails
     */
    public function getActionOwner()
    {
        return $this->action_owner;
    }

    public static function nextDateSort($a, $b)
    {
        return $a->getNextInspectionDate() > $b->getNextInspectionDate();
    }

    /**
     * Set last_modified_frequency
     *
     * @param integer $lastModifiedFrequency
     * @return SectionsThreatsMitigations
     */
    public function setLastModifiedFrequency($lastModifiedFrequency)
    {
        $this->last_modified_frequency = $lastModifiedFrequency;

        return $this;
    }

    /**
     * Get last_modified_frequency
     *
     * @return integer
     */
    public function getLastModifiedFrequency()
    {
        return $this->last_modified_frequency;
    }
}