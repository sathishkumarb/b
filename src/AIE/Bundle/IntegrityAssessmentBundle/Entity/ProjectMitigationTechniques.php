<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectMitigationTechniques
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ProjectMitigationTechniques {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="pmt")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="mitigation_technique", type="string", length=255)
     */
    private $mitigationTechnique;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     *
     * @ORM\Column(name="l_freq", type="integer")
     */
    private $l_freq;

    /**
     *
     * @ORM\Column(name="m_freq", type="integer")
     */
    private $m_freq;

    /**
     *
     * @ORM\Column(name="h_freq", type="integer")
     */
    private $h_freq;

    /**
     *
     * @ORM\Column(name="v_freq", type="integer")
     */
    private $v_freq;


    /**
     * @ORM\Column(name="vl_freq", type="integer", columnDefinition="INT(11) DEFAULT 0 NOT NULL AFTER v_freq")
     */
    private $vl_freq;

    /**
     * @ORM\OneToMany(targetEntity="SectionsThreatsMitigations" , mappedBy="mitigationTechnique" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $stm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set l_freq
     *
     * @param integer $lFreq
     * @return ProjectMitigationTechniques
     */
    public function setLFreq($lFreq) {
        $this->l_freq = $lFreq;

        return $this;
    }

    /**
     * Get l_freq
     *
     * @return integer 
     */
    public function getLFreq() {
        return $this->l_freq;
    }

    /**
     * Set m_freq
     *
     * @param integer $mFreq
     * @return ProjectMitigationTechniques
     */
    public function setMFreq($mFreq) {
        $this->m_freq = $mFreq;

        return $this;
    }

    /**
     * Get m_freq
     *
     * @return integer 
     */
    public function getMFreq() {
        return $this->m_freq;
    }

    /**
     * Set h_freq
     *
     * @param integer $hFreq
     * @return ProjectMitigationTechniques
     */
    public function setHFreq($hFreq) {
        $this->h_freq = $hFreq;

        return $this;
    }

    /**
     * Get h_freq
     *
     * @return integer 
     */
    public function getHFreq() {
        return $this->h_freq;
    }

    /**
     * Set v_freq
     *
     * @param integer $vFreq
     * @return ProjectMitigationTechniques
     */
    public function setVFreq($vFreq) {
        $this->v_freq = $vFreq;

        return $this;
    }

    //Duplicate Get and setter method for Very High
    /**
     * Get v_freq
     *
     * @return integer 
     */
    public function getVHFreq() {
        return $this->v_freq;
    }

    /**
     * Set v_freq
     *
     * @param integer $vFreq
     * @return ProjectMitigationTechniques
     */
    public function setVHFreq($vFreq) {
        $this->v_freq = $vFreq;

        return $this;
    }

    /**
     * Get v_freq
     *
     * @return integer
     */
    public function getVFreq() {
        return $this->v_freq;
    }


    /**
     * Get vl_freq
     *
     * @return integer
     */
    public function getVlfreq()
    {
        return $this->vl_freq;
    }

    /**
     * Set vl_freq
     *
     * @param integer $vlFreq
     * @return ProjectMitigationTechniques
     */
    public function setVlfreq($vl_freq)
    {
        $this->vl_freq = $vl_freq;
    }


    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return ProjectMitigationTechniques
     */
    public function setProject(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects 
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->stm = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     * @return ProjectMitigationTechniques
     */
    public function addStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm) {
        $this->stm[] = $stm;

        return $this;
    }

    /**
     * Remove stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     */
    public function removeStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm) {
        $this->stm->removeElement($stm);
    }

    /**
     * Get stm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStm() {
        return $this->stm;
    }

    /**
     * Set mitigationTechnique
     *
     * @param string $mitigationTechnique
     * @return ProjectMitigationTechniques
     */
    public function setMitigationTechnique($mitigationTechnique) {
        $this->mitigationTechnique = $mitigationTechnique;

        return $this;
    }

    /**
     * Get mitigationTechnique
     *
     * @return string 
     */
    public function getMitigationTechnique() {
        return $this->mitigationTechnique;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return ProjectMitigationTechniques
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ProjectMitigationTechniques
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}