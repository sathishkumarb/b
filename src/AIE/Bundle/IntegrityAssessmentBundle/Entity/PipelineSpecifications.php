<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PipelineSpecifications
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PipelineSpecifications {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="parameter", type="string", length=255)
     */
    private $parameter;

    /**
     * @var array
     *
     * @ORM\Column(name="unit", type="json_array")
     */
    private $unit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRequired", type="boolean")
     */
    private $isRequired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDouble", type="boolean")
     */
    private $isDouble;

    /**
     * @ORM\OneToMany(targetEntity="PipelinesSpecifications" , mappedBy="specification" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ps;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set parameter
     *
     * @param string $parameter
     * @return PipelineSpecifications
     */
    public function setParameter($parameter) {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return string 
     */
    public function getParameter() {
        return $this->parameter;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     * @return PipelineSpecifications
     */
    public function setIsRequired($isRequired) {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean 
     */
    public function getIsRequired() {
        return $this->isRequired;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->ps = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ps
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps
     * @return PipelineSpecifications
     */
    public function addP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps) {
        $this->ps[] = $ps;

        return $this;
    }

    /**
     * Remove ps
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps
     */
    public function removeP(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications $ps) {
        $this->ps->removeElement($ps);
    }

    /**
     * Get ps
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPs() {
        return $this->ps;
    }

    /**
     * Set isDouble
     *
     * @param boolean $isDouble
     * @return PipelineSpecifications
     */
    public function setIsDouble($isDouble) {
        $this->isDouble = $isDouble;

        return $this;
    }

    /**
     * Get isDouble
     *
     * @return boolean 
     */
    public function getIsDouble() {
        return $this->isDouble;
    }


    /**
     * Set unit
     *
     * @param array $unit
     * @return PipelineSpecifications
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    
        return $this;
    }

    /**
     * Get unit
     *
     * @return array 
     */
    public function getUnit()
    {
        return $this->unit;
    }
}