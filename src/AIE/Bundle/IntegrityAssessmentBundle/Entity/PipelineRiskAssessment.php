<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use AIE\Bundle\StorageBundle\Entity\AbstractFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * PipelineRiskAssessment
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\IntegrityAssessmentBundle\Repository\PipelineRiskAssessmentRepository")
 */
class PipelineRiskAssessment extends AbstractFile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isApproved", type="boolean")
     */
    private $isApproved;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pra")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;

    /**
     * @var json_array
     *
     * @ORM\Column(name="assessment_data", type="json_array")
     */
    private $assessmentData;

    /**
     * @var text
     *
     * @ORM\Column(name="html_report", type="text", nullable=true)
     */
    private $htmlReport;

    protected $allowedMimeTypes = [];

    public function __construct() {
        $this->isApproved = false;
        $this->sections = array();
    }

    //ARRAY FOR MITIGATIONTECHNIQUES

    //
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return PipelineRiskAssessment
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     * @return PipelineRiskAssessment
     */
    public function setIsApproved($isApproved) {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean 
     */
    public function getIsApproved() {
        return $this->isApproved;
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelineRiskAssessment
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null) {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines 
     */
    public function getPipeline() {
        return $this->pipeline;
    }

    /**
     * Set assessmentData
     *
     * @param array $assessmentData
     * @return PipelineRiskAssessment
     */
    public function setAssessmentData($assessmentData)
    {
        $this->assessmentData = $assessmentData;
    
        return $this;
    }

    /**
     * Get assessmentData
     *
     * @return array 
     */
    public function getAssessmentData()
    {
        return $this->assessmentData;
    }

    public function getUploadDir()
    {
        return 'assessments';
    }


    /**
     * Set htmlReport
     *
     * @param string $htmlReport
     * @return PipelineRiskAssessment
     */
    public function setHtmlReport($htmlReport)
    {
        $this->htmlReport = $htmlReport;
    
        return $this;
    }

    /**
     * Get htmlReport
     *
     * @return string 
     */
    public function getHtmlReport()
    {
        return $this->htmlReport;
    }
}