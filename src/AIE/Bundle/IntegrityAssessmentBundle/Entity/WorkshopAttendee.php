<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkshopAttendee
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class WorkshopAttendee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="RiskAssessmentWorkshops", inversedBy="wa")
     * @ORM\JoinColumn(name="workshop_id", referencedColumnName="id")
     * */
    private $workshop;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return WorkshopAttendee
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return WorkshopAttendee
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return WorkshopAttendee
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set workshop
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $workshop
     * @return WorkshopAttendee
     */
    public function setWorkshop(\AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops $workshop = null)
    {
        $this->workshop = $workshop;
    
        return $this;
    }

    /**
     * Get workshop
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }
}