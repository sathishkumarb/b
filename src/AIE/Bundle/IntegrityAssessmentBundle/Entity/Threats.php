<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Threats
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Threats {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="added_on", type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="SectionsThreats" , mappedBy="threats" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ste;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Threats
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Threats
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Threats
     */
    public function setNotes($notes) {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes() {
        return $this->notes;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Threats
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->ste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ste
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste
     * @return Threats
     */
    public function addSte(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste) {
        $this->ste[] = $ste;

        return $this;
    }

    /**
     * Remove ste
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste
     */
    public function removeSte(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste) {
        $this->ste->removeElement($ste);
    }

    /**
     * Get ste
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSte() {
        return $this->ste;
    }


}