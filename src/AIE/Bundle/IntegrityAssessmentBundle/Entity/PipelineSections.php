<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PipelineSections
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\IntegrityAssessmentBundle\Repository\PipelineSectionsRepository")
 * @UniqueEntity(
 *     fields={"pipeline","section"},
 *     errorPath="section"
 * )
 */
class PipelineSections {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pse")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;

    /**
     * @var string
     *
     * @ORM\Column(name="section", type="string", length=255)
     */
    private $section;

    /**
     * @ORM\Column(name="x", type="float", scale=2)
     */
    private $x;

    /**
     * @ORM\Column(name="y", type="float", scale=2)
     */
    private $y;

    
    /**
     * @ORM\OneToMany(targetEntity="SectionsThreats" , mappedBy="pipelineSections" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $ste;
    

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelineSections
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null) {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines 
     */
    public function getPipeline() {
        return $this->pipeline;
    }

    /**
     * Set x
     *
     * @param string $x
     * @return PipelineSections
     */
    public function setX($x) {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return string 
     */
    public function getX() {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param string $y
     * @return PipelineSections
     */
    public function setY($y) {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return string 
     */
    public function getY() {
        return $this->y;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->ste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getThreats() {
        $threats = new ArrayCollection();

        foreach ($this->ste as $s) {
            $threats[] = $s->getThreats();
        }

        return $threats;
    }

    public function setThreats($threats) {
        foreach ($threats as $t) {
            $st = new SectionsThreats();

            $st->setPipelineSection($this);
            $st->setThreats($t);

            $this->addSte($st);
        }
    }

    /**
     * Add ste
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste
     * @return PipelineSections
     */
    public function addSte(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste) {
        $this->ste[] = $ste;

        return $this;
    }

    /**
     * Remove ste
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste
     */
    public function removeSte(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats $ste) {
        $this->ste->removeElement($ste);
    }

    /**
     * Get ste
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSte() {
        return $this->ste;
    }


    /**
     * Set section
     *
     * @param string $section
     * @return PipelineSections
     */
    public function setSection($section)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }
}