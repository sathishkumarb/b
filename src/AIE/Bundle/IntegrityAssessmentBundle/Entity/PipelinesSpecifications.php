<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PipelinesSpecifications
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PipelinesSpecifications {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="ps")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineSpecifications", inversedBy="ps")
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id")
     * */
    private $specification;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit", type="string",  nullable=true)
     */
    private $unit;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", nullable=true, length=255)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", nullable=true, type="string", nullable=true)
     */
    private $comment;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return PipelinesSpecifications
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return PipelinesSpecifications
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelinesSpecifications
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null) {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines 
     */
    public function getPipeline() {
        return $this->pipeline;
    }

    /**
     * Set specification
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSpecifications $specification
     * @return PipelinesSpecifications
     */
    public function setSpecification(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSpecifications $specification = null) {
        $this->specification = $specification;

        return $this;
    }

    /**
     * Get specification
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSpecifications 
     */
    public function getSpecification() {
        return $this->specification;
    }

    /**
     * Set unit
     *
     * @param string $unit
     * @return PipelinesSpecifications
     */
    public function setUnit($unit) {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit() {
        return $this->unit;
    }

}