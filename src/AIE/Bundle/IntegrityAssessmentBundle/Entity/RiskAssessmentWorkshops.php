<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RiskAssessmentWorkshops
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RiskAssessmentWorkshops
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="initiallyAssessedBy", type="string", length=255)
     */
    private $initiallyAssessedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pw")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;

    /**
     * @ORM\OneToMany(targetEntity="WorkshopAttendee" , mappedBy="workshop" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $wa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initiallyAssessedBy
     *
     * @param string $initiallyAssessedBy
     * @return RiskAssessmentWorkshops
     */
    public function setInitiallyAssessedBy($initiallyAssessedBy)
    {
        $this->initiallyAssessedBy = $initiallyAssessedBy;
    
        return $this;
    }

    /**
     * Get initiallyAssesstedBy
     *
     * @return string 
     */
    public function getInitiallyAssessedBy()
    {
        return $this->initiallyAssessedBy;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return RiskAssessmentWorkshops
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->wa = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add wa
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\WorkshopAttendee $wa
     * @return RiskAssessmentWorkshops
     */
    public function addWa(\AIE\Bundle\IntegrityAssessmentBundle\Entity\WorkshopAttendee $wa)
    {
        $this->wa[] = $wa;
    
        return $this;
    }

    /**
     * Remove wa
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\WorkshopAttendee $wa
     */
    public function removeWa(\AIE\Bundle\IntegrityAssessmentBundle\Entity\WorkshopAttendee $wa)
    {
        $this->wa->removeElement($wa);
    }

    /**
     * Get wa
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWa()
    {
        return $this->wa;
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return RiskAssessmentWorkshops
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null)
    {
        $this->pipeline = $pipeline;
    
        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines 
     */
    public function getPipeline()
    {
        return $this->pipeline;
    }
}