<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PipelineFilesCategories
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\IntegrityAssessmentBundle\Repository\PipelineFilesCategoriesRepository")
 */
class PipelineFilesCategories {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pipelines", inversedBy="pfc")
     * @ORM\JoinColumn(name="pipeline_id", referencedColumnName="id")
     * */
    private $pipeline;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineFilesCategories", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="PipelineFilesCategories", mappedBy="parent", fetch="EXTRA_LAZY")
     */
    protected $children;

    /**
     * @ORM\OneToMany(targetEntity="PipelineFiles" , mappedBy="category" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $pfc;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PipelineFilesCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pfc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pipeline
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline
     * @return PipelineFilesCategories
     */
    public function setPipeline(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $pipeline = null)
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    /**
     * Get pipeline
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines
     */
    public function getPipeline()
    {
        return $this->pipeline;
    }

    /**
     * Set parent
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $parent
     * @return PipelineFilesCategories
     */
    public function setParent(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $children
     * @return PipelineFilesCategories
     */
    public function addChildren(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $children
     */
    public function removeChildren(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFilesCategories $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add pfc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pfc
     * @return PipelineFilesCategories
     */
    public function addPfc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pfc)
    {
        $this->pfc[] = $pfc;

        return $this;
    }

    /**
     * Remove pfc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pfc
     */
    public function removePfc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles $pfc)
    {
        $this->pfc->removeElement($pfc);
    }

    /**
     * Get pfc
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPfc()
    {
        return $this->pfc;
    }


    public function getAsTreeNode(&$files, $fileuploader)
    {

        $node = [
            'id'       => $this->getId(),
            'title'    => $this->getName(),
            'key'      => $this->getId(),
            'folder'   => true,
            'children' => []
        ];

        $children = $this->getChildren();
        $childrenArr = [];

        foreach ($children as $child)
        {
            $childrenArr[] = $child->getAsTreeNode($files, $fileuploader);
        }

        foreach ($files as $key => $file)
        {
            if ($file->getCategory() && $file->getCategory()->getId() == $this->getId())
            {
                $childrenArr[] = $file->getAsTreeNode($fileuploader);
                unset($files[$key]);
            }
        }

        $node['children'] = $childrenArr;

        return $node;
    }
}