<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectConsequences
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ProjectConsequences {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="pc")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="ConsequencesCategories", inversedBy="pc")
     * @ORM\JoinColumn(name="consequence_id", referencedColumnName="id")
     * */
    protected $consequence;

    /**
     * @ORM\OneToMany(targetEntity="ConsequenceAssessment" , mappedBy="consequence" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $stc;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_1", type="text", nullable=true)
     */
    protected $desc_1;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_2", type="text", nullable=true)
     */
    protected $desc_2;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_3", type="text", nullable=true)
     */
    protected $desc_3;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_4", type="text", nullable=true)
     */
    protected $desc_4;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_5", type="text", nullable=true)
     */
    protected $desc_5;

    /**
     * Constructor
     */
    public function __construct() {
        $this->stc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return ProjectConsequences
     */
    public function setProject(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project = null) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects 
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * Set consequence
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequencesCategories $consequence
     * @return ProjectConsequences
     */
    public function setConsequence(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequencesCategories $consequence = null) {
        $this->consequence = $consequence;

        return $this;
    }

    /**
     * Get consequence
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequencesCategories 
     */
    public function getConsequence() {
        return $this->consequence;
    }

    /**
     * Add stc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc
     * @return ProjectConsequences
     */
    public function addStc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc) {
        $this->stc[] = $stc;

        return $this;
    }

    /**
     * Remove stc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc
     */
    public function removeStc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc) {
        $this->stc->removeElement($stc);
    }

    /**
     * Get stc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStc() {
        return $this->stc;
    }


    /**
     * Set desc_1
     *
     * @param string $desc1
     * @return ProjectConsequences
     */
    public function setDesc1($desc1)
    {
        $this->desc_1 = $desc1;
    
        return $this;
    }

    /**
     * Get desc_1
     *
     * @return string 
     */
    public function getDesc1()
    {
        return $this->desc_1;
    }

    /**
     * Set desc_2
     *
     * @param string $desc2
     * @return ProjectConsequences
     */
    public function setDesc2($desc2)
    {
        $this->desc_2 = $desc2;
    
        return $this;
    }

    /**
     * Get desc_2
     *
     * @return string 
     */
    public function getDesc2()
    {
        return $this->desc_2;
    }

    /**
     * Set desc_3
     *
     * @param string $desc3
     * @return ProjectConsequences
     */
    public function setDesc3($desc3)
    {
        $this->desc_3 = $desc3;
    
        return $this;
    }

    /**
     * Get desc_3
     *
     * @return string 
     */
    public function getDesc3()
    {
        return $this->desc_3;
    }

    /**
     * Set desc_4
     *
     * @param string $desc4
     * @return ProjectConsequences
     */
    public function setDesc4($desc4)
    {
        $this->desc_4 = $desc4;
    
        return $this;
    }

    /**
     * Get desc_4
     *
     * @return string 
     */
    public function getDesc4()
    {
        return $this->desc_4;
    }

    /**
     * Set desc_5
     *
     * @param string $desc5
     * @return ProjectConsequences
     */
    public function setDesc5($desc5)
    {
        $this->desc_5 = $desc5;
    
        return $this;
    }

    /**
     * Get desc_5
     *
     * @return string 
     */
    public function getDesc5()
    {
        return $this->desc_5;
    }
}