<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MitigationGroups
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MitigationGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * @ORM\OneToMany(targetEntity="SectionsThreatsMitigations" , mappedBy="mitigationgroups" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $stm;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MitigationGroups
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MitigationGroups
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     * @return MitigationGroups
     */
    public function addStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm)
    {
        $this->stm[] = $stm;
    
        return $this;
    }

    /**
     * Remove stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     */
    public function removeStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm)
    {
        $this->stm->removeElement($stm);
    }

    /**
     * Get stm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStm()
    {
        return $this->stm;
    }
}