<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MitigationTechniques
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MitigationTechniques {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     *
     * @ORM\Column(name="l_freq", type="integer")
     */
    private $l_freq = 0;

    /**
     *
     * @ORM\Column(name="m_freq", type="integer")
     */
    private $m_freq = 0;

    /**
     *
     * @ORM\Column(name="h_freq", type="integer")
     */
    private $h_freq = 0;

    /**
     *
     * @ORM\Column(name="v_freq", type="integer")
     */
    private $v_freq = 0;


    /**
     *
     * @ORM\Column(name="vl_freq", type="integer")
     */
    private $vl_freq = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MitigationTechniques
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MitigationTechniques
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set performanceStandard
     *
     * @param string $performanceStandard
     * @return MitigationTechniques
     */
    public function setPerformanceStandard($performanceStandard) {
        $this->performanceStandard = $performanceStandard;

        return $this;
    }

    /**
     * Get performanceStandard
     *
     * @return string 
     */
    public function getPerformanceStandard() {
        return $this->performanceStandard;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return MitigationTechniques
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }


    /**
     * Set l_freq
     *
     * @param integer $lFreq
     * @return MitigationTechniques
     */
    public function setLFreq($lFreq) {
        $this->l_freq = $lFreq;

        return $this;
    }

    /**
     * Get l_freq
     *
     * @return integer 
     */
    public function getLFreq() {
        return $this->l_freq;
    }

    /**
     * Set m_freq
     *
     * @param integer $mFreq
     * @return MitigationTechniques
     */
    public function setMFreq($mFreq) {
        $this->m_freq = $mFreq;

        return $this;
    }

    /**
     * Get m_freq
     *
     * @return integer 
     */
    public function getMFreq() {
        return $this->m_freq;
    }

    /**
     * Set h_freq
     *
     * @param integer $hFreq
     * @return MitigationTechniques
     */
    public function setHFreq($hFreq) {
        $this->h_freq = $hFreq;

        return $this;
    }

    /**
     * Get h_freq
     *
     * @return integer 
     */
    public function getHFreq() {
        return $this->h_freq;
    }

    /**
     * Set v_freq
     *
     * @param integer $vFreq
     * @return MitigationTechniques
     */
    public function setVFreq($vFreq) {
        $this->v_freq = $vFreq;

        return $this;
    }

    /**
     * Get v_freq
     *
     * @return integer 
     */
    public function getVFreq() {
        return $this->v_freq;
    }

    /**
     * Get vl_freq
     *
     * @return integer
     */
    public function getVlFreq()
    {
        return $this->vl_freq;
    }

    /**
     * Set vl_freq
     *
     * @param integer $vl_Freq
     * @return MitigationTechniques
     */
    public function setVlFreq($vl_freq)
    {
        $this->vl_freq = $vl_freq;
    }



}