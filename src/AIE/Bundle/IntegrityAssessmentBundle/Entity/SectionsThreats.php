<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SectionsThreats
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AIE\Bundle\IntegrityAssessmentBundle\Repository\SectionsThreatsRepository")
 */
class SectionsThreats {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PipelineSections", inversedBy="ste")
     * @ORM\JoinColumn(name="pipeline_section_id", referencedColumnName="id")
     * */
    private $pipelineSections;

    /**
     * @ORM\ManyToOne(targetEntity="Threats", inversedBy="ste")
     * @ORM\JoinColumn(name="threat_id", referencedColumnName="id")
     * */
    private $threats;

    /**
     *
     * @ORM\Column(name="probability_of_failer", type="integer")
     */
    private $probabilityOfFailer = 0;
    
    /**
     *
     * @ORM\Column(name="confidence", type="integer")
     */
    private $confidence = 1; //0 low, 1 mid, 2 high
    
    /**
     *
     * @ORM\Column(name="adj_probability_of_failer", type="integer")
     */
    private $AdjProbabilityOfFailer = 0;
    
    
    /**
     * @ORM\OneToMany(targetEntity="SectionsThreatsMitigations" , mappedBy="sectionsThreats" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $stm;
    
    
    /**
     * @ORM\OneToMany(targetEntity="ConsequenceAssessment" , mappedBy="sectionsThreats" , cascade={"all"}, fetch="EXTRA_LAZY")
     * */
    protected $stc;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stm = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     * @return SectionsThreats
     */
    public function addStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm)
    {
        $this->stm[] = $stm;
    
        return $this;
    }

    /**
     * Remove stm
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm
     */
    public function removeStm(\AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations $stm)
    {
        $this->stm->removeElement($stm);
    }

    /**
     * Get stm
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStm()
    {
        return $this->stm;
    }

    /**
     * Set probabilityOfFailer
     *
     * @param integer $probabilityOfFailer
     * @return SectionsThreats
     */
    public function setProbabilityOfFailer($probabilityOfFailer)
    {
        $this->probabilityOfFailer = $probabilityOfFailer;
    
        return $this;
    }

    /**
     * Get probabilityOfFailer
     *
     * @return integer 
     */
    public function getProbabilityOfFailer()
    {
        return $this->probabilityOfFailer;
    }

    /**
     * Set AdjProbabilityOfFailer
     *
     * @param integer $adjProbabilityOfFailer
     * @return SectionsThreats
     */
    public function setAdjProbabilityOfFailer($adjProbabilityOfFailer)
    {
        $this->AdjProbabilityOfFailer = $adjProbabilityOfFailer;
    
        return $this;
    }

    /**
     * Get AdjProbabilityOfFailer
     *
     * @return integer 
     */
    public function getAdjProbabilityOfFailer()
    {
        return $this->AdjProbabilityOfFailer;
    }

    /**
     * Add stc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc
     * @return SectionsThreats
     */
    public function addStc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc)
    {
        $this->stc[] = $stc;
    
        return $this;
    }

    /**
     * Remove stc
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc
     */
    public function removeStc(\AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment $stc)
    {
        $this->stc->removeElement($stc);
    }

    /**
     * Get stc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStc()
    {
        return $this->stc;
    }

    /**
     * Set threats
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats $threats
     * @return SectionsThreats
     */
    public function setThreats(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats $threats = null)
    {
        $this->threats = $threats;
    
        return $this;
    }

    /**
     * Get threats
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats 
     */
    public function getThreats()
    {
        return $this->threats;
    }

    /**
     * Set pipelineSections
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pipelineSections
     * @return SectionsThreats
     */
    public function setPipelineSections(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pipelineSections = null)
    {
        $this->pipelineSections = $pipelineSections;
    
        return $this;
    }

    /**
     * Get pipelineSections
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections 
     */
    public function getPipelineSections()
    {
        return $this->pipelineSections;
    }

    /**
     * Set confidence
     *
     * @param integer $confidence
     * @return SectionsThreats
     */
    public function setConfidence($confidence)
    {
        $this->confidence = $confidence;
    
        return $this;
    }

    /**
     * Get confidence
     *
     * @return integer 
     */
    public function getConfidence()
    {
        return $this->confidence;
    }
}