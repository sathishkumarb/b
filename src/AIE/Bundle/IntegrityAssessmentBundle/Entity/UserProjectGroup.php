<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserProjectGroup
 *
 * @ORM\Table(name="user_projects_group")
 * @ORM\Entity
 */
class UserProjectGroup {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Bundle\UserBundle\Entity\Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="Projects", inversedBy="ug")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    //json_array for limiting pipelines

    /**
     * Set user
     *
     * @param \AIE\Bundle\UserBundle\Entity\User $user
     * @return UserProjectGroup
     */
    public function setUser(\AIE\Bundle\UserBundle\Entity\User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AIE\Bundle\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \AIE\Bundle\UserBundle\Entity\Group $group
     * @return UserProjectGroup
     */
    public function setGroup(\AIE\Bundle\UserBundle\Entity\Group $group = null) {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AIE\Bundle\UserBundle\Entity\Group 
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * Set project
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project
     * @return UserProjectGroup
     */
    public function setProject(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects $project) {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects 
     */
    public function getProject() {
        return $this->project;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function getGroupRoles() {
        return $this->getGroup()->getRoles();
    }
    
    public function getUserRoles(){
        return $this->getUser()->getRoles();
    }

}