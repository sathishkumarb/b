<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sections
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Sections {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="PipelineSections" , mappedBy="sections" , cascade={"all"})
     * */
    protected $pse;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pse = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Sections
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add pse
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse
     * @return Sections
     */
    public function addPse(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse)
    {
        $this->pse[] = $pse;
    
        return $this;
    }

    /**
     * Remove pse
     *
     * @param \AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse
     */
    public function removePse(\AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections $pse)
    {
        $this->pse->removeElement($pse);
    }

    /**
     * Get pse
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPse()
    {
        return $this->pse;
    }

}