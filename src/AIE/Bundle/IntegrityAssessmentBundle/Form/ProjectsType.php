<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ProjectsType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description'), 'textarea'))
            ->add('referenceNo', 'text', $this->options(array('label' => 'Reference #')))
            ->add('client', 'text', $this->options(array('label' => 'Company')))
            ->add('location', 'text', $this->options(array('label' => 'Location')))
            ->add('file', 'file', $this->options(array('label' => 'Logo'), 'file'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_projects';
    }

}
