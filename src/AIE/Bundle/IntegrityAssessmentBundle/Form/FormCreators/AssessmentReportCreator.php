<?php
namespace AIE\Bundle\IntegrityAssessmentBundle\Form\FormCreators;


use AIE\Bundle\IntegrityAssessmentBundle\Form\AssessmentReportType;
use AIE\Bundle\IntegrityAssessmentBundle\Form\WorkshopReportType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;


trait AssessmentReportCreator {

    //use Helper\FormStyleHelper;

    private $_blocks = [
        'INTRODUCTION'                 => 'Introduction',
        'BATTERY_LIMITS'               => 'Battery Limits',
        'ABBREVIATIONS'                => 'Abbreviations',
        'DESIGN_SPECIFICATIONS'        => 'Design Specifications',
        'RISK_ASSESSMENT_METHODOLOGY'  => 'Risk Assessment Methodology',
        'PIPELINE_PHYSICAL_SECTIONING' => 'Pipeline Physical Sectioning',
        'DESCRIPTION_OF_THREATS'       => 'Description of Threats',
        'SECTIONAL_THREAT_ANALYSIS'    => 'Sectional Threat Analysis',
        'RISK_ASSESSMENT'              => 'Risk Assessment',
        'WORKSHOP'                     => 'Risk Assessment Workshop',
        'ASSESSMENT_OF_CONSEQUENCES'   => 'Assessment of the Consequences',
        'ASSESSMENT_OF_LIKELIHOOD'     => 'Assessment of Likelihood of Failure',
        'CONFIDENCE_IN_ASSESSMENT'     => 'Confidence in the Assessment',
        'MITIGATION_TECHNIQUES'        => 'Mitigation Techniques',
        'MITIGATION_SCHEDULING'        => 'Mitigation Scheduling',
        'THREAT_PRIORITISATION'        => 'Threat Prioritisation',
        'CONSEQUENCES_ASSESSMENT'      => 'Consequences Assessment Results',
        'PROBABILITY_ASSESSMENT'       => 'Probability of Failure Assessment Results',
        'ASSESSMENT_OF_RISK_LEVEL'     => 'Assessment of Risk Level Results',
        'APPENDIX'                     => 'APPENDICES',
        'REFERENCES_LIST'              => 'References',
    ];

    /**
     * @param $pipelineId
     * @param $assessmentId
     * @return $this|\Symfony\Component\Form\Form|\Symfony\Component\Form\FormConfigBuilderInterface
     */
    public function createAssesmentReportForm($pipelineId, $assessmentId, $workshops)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('assessment_report_docx', array('pipelineId' => $pipelineId, 'id' => $assessmentId)))
            ->setMethod('POST');

        $form->add('assessment_report', new AssessmentReportType($this->_blocks), ['label' => ' ']);

        $form->add('workshop_report', new WorkshopReportType($workshops), ['label' => ' ']);

        $form = $form->add('submit', 'submit', $this->options(array('label' => 'Generate', 'attr' => ['class' => 'right', 'style' => 'margin-right: 30px;']), 'btn'))
            ->getForm();

        return $form;
    }

    public function getBlocks()
    {
        return $this->_blocks;
    }
} 