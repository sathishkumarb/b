<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ThreatsType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', $this->options(array('label' => 'Name')))
                ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false), 'textarea'))
                ->add('notes', 'textarea', $this->options(array('label' => 'Notes', 'required' => false), 'textarea'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_threats';
    }

}
