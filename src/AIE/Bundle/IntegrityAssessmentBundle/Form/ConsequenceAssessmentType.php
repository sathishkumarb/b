<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment;

class ConsequenceAssessmentType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(ConsequenceAssessment $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $choices = array(1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5');

        $builder
        ->add('serverity', 'choice', $this->options(array('label' => false, 'choices' => $choices, 'data' => $this->entity->getServerity())));
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_consequenceassessment';
    }

}
