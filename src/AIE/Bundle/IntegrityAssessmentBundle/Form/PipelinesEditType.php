<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class PipelinesEditType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;
    private $files;

    public function __construct(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $entity, $files)
    {
        $this->entity = $entity;
        $this->files = $files;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $batteryLimits = ($this->entity->getBatteryLimits() == null) ?
            'From the valve downstream of the pig launcher to the valve upstream the pig receiver and all branch piping up to the first flange' : $this->entity->getBatteryLimits();
        $builder
            ->add('name', 'text', $this->options(array('label' => 'Name')))
            ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false, 'attr' => ['placeholder' => 'Intorduction about the pipeline and the operator']), 'textarea'))
            ->add('batteryLimits', 'textarea', $this->options(array('label' => 'Battery Limits', 'required' => false, 'data' => $batteryLimits), 'textarea'))
            ->add('geometry', 'hidden', array('attr' => array('id' => 'geometryMap')))
            ->add('files', new SelectableFileType(), $this->options(
                    array(
                        'class'    => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles',
                        'property' => 'caption',
                        'choices'  => $this->files,
                        'expanded' => false,
                        'multiple' => true,
                        'data'     => $this->entity->getFiles(),
                        'attr'     => array('class' => 'files')
                    ), 'entity'
                )
            )
            /* ->add('PipelineSpecifications', 'entity', array(
              'class' => 'AIEIntegrityAssessmentBundle:PipelineSpecifications',
              'property' => 'parameter',
              'expanded' => true,
              'multiple' => true,)) */
            // ->add('Ps', 'collection', array('type' => new PipelinesSpecificationsType(), 'allow_add' => true));
        ;
        /*
          $specifications = $this->entity->getPipelineSpecifications();

          var_dump($specifications);
          //exit();
          foreach($specifications as $s){
          $builder->add('Ps','text', array('label'=>'hi'));
          } */
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines',
            'em'         => '',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_pipelines_edit';
    }

}
