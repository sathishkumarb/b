<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class PipelinesType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(\AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {


        $batteryLimits = $this->entity->getBatteryLimits() ? $this->entity->getBatteryLimits() :
                'From the valve downstream of the pig launcher to the valve upstream the pig receiver and all branch piping up to the first flange';
        $builder
                ->add('name', 'text', $this->options(array('label' => 'Name')))
                ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false, 'attr' => ['placeholder' => 'Introduction about the pipeline and the operator']), 'textarea'))
                ->add('batteryLimits', 'textarea', $this->options(array('label' => 'Battery Limits', 'required' => false, 'data' => $batteryLimits), 'textarea'))
                ->add('geometry', 'hidden', array('attr' => array('id' => 'geometryMap')))
        /* ->add('PipelineSpecifications', 'entity', array(
          'class' => 'AIEIntegrityAssessmentBundle:PipelineSpecifications',
          'property' => 'parameter',
          'expanded' => true,
          'multiple' => true,)) */
        // ->add('Ps', 'collection', array('type' => new PipelinesSpecificationsType(), 'allow_add' => true));
        ;
        /*
          $specifications = $this->entity->getPipelineSpecifications();

          var_dump($specifications);
          //exit();
          foreach($specifications as $s){
          $builder->add('Ps','text', array('label'=>'hi'));
          } */
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines',
            'em' => '',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_pipelines';
    }

}
