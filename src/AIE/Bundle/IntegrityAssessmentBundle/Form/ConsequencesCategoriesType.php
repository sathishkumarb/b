<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ConsequencesCategoriesType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', $this->options(array('label'=>'Name')))
                ->add('desc_1', 'textarea', $this->options(array('label' => 'Description of Very Low / 1'), 'textarea'))
                ->add('desc_2', 'textarea', $this->options(array('label' => 'Description of Low / 2'), 'textarea'))
                ->add('desc_3', 'textarea', $this->options(array('label' => 'Description of Medium / 3'), 'textarea'))
                ->add('desc_4', 'textarea', $this->options(array('label' => 'Description of High / 4'), 'textarea'))
                ->add('desc_5', 'textarea', $this->options(array('label' => 'Description of Very High / 5'), 'textarea'))
                ;
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequencesCategories'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_consequencescategories';
    }

}
