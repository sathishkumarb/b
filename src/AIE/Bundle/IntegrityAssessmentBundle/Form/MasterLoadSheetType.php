<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AIE\Bundle\IntegrityAssessmentBundle\Helper\FormStyleHelper;


class MasterLoadSheetType extends AbstractType {

    use FormStyleHelper;

    /** @var  Projects */
    protected $project;

    public function __construct($project)
    {
        $this->project = $project;
    }

    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('loadsheetfile', FileType::class,
                $this->options([
                    'label'       => false,
                    'required'    => false,
                    'attr'        => ['class' => 'required form-control'],
                    'constraints' => [new NotBlank()]
                ]));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_masterloadsheet';
    }

}
