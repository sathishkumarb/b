<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ProjectMitigationTechniquesType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('mitigationTechnique', 'text', $this->options(array('label' => 'Name')))
                ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false), 'textarea'))
                ->add('vlfreq', 'integer', $this->options(array('label' => 'Frequency for Very Low Risk')))
                ->add('l_freq', 'integer', $this->options(array('label' => 'Frequency for Low Risk')))
                ->add('m_freq', 'integer', $this->options(array('label' => 'Frequency for Medium Risk')))
                ->add('h_freq', 'integer', $this->options(array('label' => 'Frequency for High Risk')))
                ->add('v_freq', 'integer', $this->options(array('label' => 'Frequency for Very High Risk')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_projectmitigationtechniques';
    }

}
