<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class PipelineSectionsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $length;
    private $entity;

    public function __construct($entity, $length) {
        $this->length = $length;
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $x = (!$this->entity->getX()) ? 0 : $this->entity->getX();
        $y = (!$this->entity->getY()) ? $this->length : $this->entity->getY();
        $builder
                ->add('section', 'text', $this->options(array(
                            'label' => 'Section',
                            'attr' => array(
                                'class' => 'section',
                                'autocomplete' => 'off',
                                'data-provide' => 'typeahead'
                            )
                )))
                ->add('x', 'text', $this->options(array('label' => 'Start KP', 'attr' => array('class' => 'x'), 'data' => $x)))
                ->add('y', 'text', $this->options(array('label' => 'End KP', 'attr' => array('class' => 'y'), 'data' => $y)))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_pipelinesections';
    }

}
