<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class AssessmentReportType extends AbstractType {

    use Helper\FormStyleHelper;

    public $_blocks;

    public function __construct(array $blocks)
    {
        $this->_blocks = $blocks;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->_blocks as $block_key => $block_label)
        {
            $builder
                /*->add($block_key, 'checkbox', array(
                    'label'    => $block_label,
                    'required' => false,
                    'mapped'   => false,
                    'attr'     => [
                        'checked' => 'checked',
                    ]
                ))*/
                ->add($block_key, 'hidden', array(
                    'label'    => $block_label,
                    'required' => false,
                    'mapped'   => false,
                ))
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'empty_data' => function (FormInterface $form)
                {
                    $blocks = [];
                    foreach ($this->_blocks as $block_key => $block_label)
                    {
                        $blocks[$block_key] = $form->get($block_key)->getData();
                    }

                    return $blocks;
                },
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_assessment_report';
    }

}
