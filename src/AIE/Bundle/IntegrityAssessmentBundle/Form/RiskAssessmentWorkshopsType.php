<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class RiskAssessmentWorkshopsType extends AbstractType
{
    use Helper\FormStyleHelper;

    private $entity;
    public function __construct(RiskAssessmentWorkshops $entity) {
        $this->entity = $entity;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = ($this->entity->getDate()) ? $this->entity->getDate() : new \DateTime();
        if ($date)
        {
            $date = $date->format('d F Y');
        }
        $builder
            ->add('initiallyAssessedBy', 'text', $this->options(array('label' => 'Initially Assessed by (Company)')))
            ->add('date', new DatePickerType(), $this->options(array('mapped' => true, 'label' => 'Date', 'data' => $date,  'attr' => ['class' => 'col-md-9'], 'label_attr' => ['class' => 'col-md-3 control-label']), ''))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\RiskAssessmentWorkshops'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_riskassessmentworkshops';
    }
}
