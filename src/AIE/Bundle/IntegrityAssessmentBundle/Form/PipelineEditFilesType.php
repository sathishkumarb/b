<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class PipelineEditFilesType extends AbstractType {

    use Helper\FormStyleHelper;


    private $entity;

    public function __construct(PipelineFiles $pipelineFile)
    {
        $this->entity = $pipelineFile;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = $this->entity->getDocumentDate();
        if ($date)
        {
            $date = $date->format('d F Y');
        }
        $builder
            ->add('filename', 'text', $this->options(array('label' => 'Name')))
            ->add('caption', 'text', $this->options(array('label' => 'Caption', 'required' => false)))
            ->add('company', 'text', $this->options(array('label' => 'Company', 'required' => true)))
            ->add('document_number', 'text', $this->options(array('label' => 'Document Number', 'required' => true)))
            ->add('document_date', new DatePickerType(), $this->options(array('mapped' => true, 'label' => 'Document Date', 'data' => $date, 'attr' => ['class' => 'col-md-9'], 'label_attr' => ['class' => 'col-md-3 control-label']), ''));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_pipelineeditfiles';
    }

}
