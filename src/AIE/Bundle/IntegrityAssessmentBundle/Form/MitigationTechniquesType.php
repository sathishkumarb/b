<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class MitigationTechniquesType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', $this->options(array('label' => 'Name')))
                ->add('description', 'textarea', $this->options(array('label' => 'Description', 'required' => false), 'textarea'))
                ->add('l_freq', 'integer', $this->options(array('label' => 'Frequency for Low Risk'), 'integer'))
                ->add('m_freq', 'integer', $this->options(array('label' => 'Frequency for Medium Risk'), 'integer'))
                ->add('h_freq', 'integer', $this->options(array('label' => 'Frequency for High Risk'), 'integer'))
                ->add('v_freq', 'integer', $this->options(array('label' => 'Frequency for Very High Risk'), 'integer'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\MitigationTechniques'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_mitigationtechniques';
    }

}
