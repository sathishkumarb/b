<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;

class PlanningInspectionsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $from_date;
    private $to_date;

    public function __construct(\DateTime $from_date, \DateTime $to_date)
    {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('from_date', new DatePickerType(), array('label' => false, 'mapped' => false, 'data' => $this->from_date->format('d F Y'), 'attr' => ['class' => '']))
            ->add('to_date', new DatePickerType(), array('label' => false, 'mapped' => false, 'data' => $this->to_date->format('d F Y'), 'attr' => ['class' => '']));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'empty_data' => function (FormInterface $form)
                {
                    return [
                        'from_date' => $form->get('from_date')->getData(),
                        'to_date'   => $form->get('to_date')->getData()
                    ];
                },
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_planning_inspections';
    }

}
