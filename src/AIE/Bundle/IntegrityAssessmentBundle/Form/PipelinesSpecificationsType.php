<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications;

class PipelinesSpecificationsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(PipelinesSpecifications $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $specification = $this->entity->getSpecification();

        if ($specification->getIsDouble()) {
            $builder->add('value', 'text', $this->options(array('label' => false, 'required' => $specification->getIsRequired(), 'data' => $this->entity->getValue(), 'attr' => array('class' => 'double'))));
        } else {
            $builder->add('value', 'text', $this->options(array('label' => false, 'required' => $specification->getIsRequired(), 'data' => $this->entity->getValue())));
        }

        $builder->add('comment', 'textarea', $this->options(array('label' => false, 'required' => false, 'data' => $this->entity->getComment()), 'textarea'));

        $unites = $specification->getUnit();


        if (empty($unites)) {
            $builder->add('unit', 'hidden', array('label' => false));
        } else if (count($unites) == 1) {
            $builder->add('unit', 'hidden', array('label' => false, 'data' => $unites[0]));
        } else {

            $choices = array();
            foreach($unites as $unit){
                $choices[$unit] = $unit;
            }
            $builder->add('unit', 'choice', $this->options(array(
                        'label' => false,
                        'multiple' => false,
                        'choices' => $choices,
                        'data' => $this->entity->getUnit(),
                            )
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelinesSpecifications'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_pipelinesspecifications';
    }

}
