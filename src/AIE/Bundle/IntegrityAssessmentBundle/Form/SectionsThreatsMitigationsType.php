<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;

class SectionsThreatsMitigationsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;
    private $files;

    public function __construct(SectionsThreatsMitigations $entity, $files) {
        $this->entity = $entity;
        $this->files = $files;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $date = ($this->entity->getLastInspectionDate()) ? $this->entity->getLastInspectionDate() : new \DateTime();
        
        $builder
                ->add('performanceStandard', 'textarea', $this->options(array('required' => false, 'data' => $this->entity->getPerformanceStandard())))
                ->add('comments', 'textarea', $this->options(array('required' => false, 'data' => $this->entity->getComments())))
                ->add('recommendations', 'textarea', $this->options(array('required' => false, 'data' => $this->entity->getRecommendations())))
                ->add('mitigationStatus', 'textarea', $this->options(array('required' => false, 'data' => $this->entity->getMitigationStatus())))
                ->add('last_inspection_date', new DatePickerType(), array('label'=>false, 'data' => $date->format('d F Y')))
                ->add('files', new SelectableFileType(), $this->options(
                                array(
                            'class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles',
                            'property' => 'object',
                            'choices' => $this->files,
                            'expanded' => false,
                            'multiple' => true,
                            'data' => $this->entity->getFiles(),
                            'attr' => array('class' => 'files')
                                ), 'entity'
                        )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_sectionsthreatsmitigations';
    }

}
