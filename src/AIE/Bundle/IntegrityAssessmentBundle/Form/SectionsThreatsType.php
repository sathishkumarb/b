<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class SectionsThreatsType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct($entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $choices = array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5');

        $builder
                ->add('probabilityOfFailer', 'choice', $this->options(array('label' => false, 'choices' => $choices, 'attr' => array('class' => 'probF'), 'data' => $this->entity->getProbabilityOfFailer())))
                ->add('confidence', 'choice', $this->options(array('label' => false, 'choices' => array(0 => 'Low', 1 => 'Medium', 2 => 'High'), 'attr' => array('class' => 'confidence'), 'data' => $this->entity->getConfidence())))
        //->add('AdjProbabilityOfFailer','choice', $this->options( array('label'=>false,'disabled'=>true, 'choices' => $choices,'data'=>$this->entity->getAdjProbabilityOfFailer())))
        ;
        //->add('id','checkbox')
        /*
          ->add('pipeline_section','entity', array(
          'class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections',
          'property' => 'id'
          ))
          ->add('threat','entity', array(
          'class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats',
          'property' => 'name'
          ))


         */
        //->add('','checkbox',array('class' => 'TrackerMembersBundle:Category'))


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_sectionsthreats';
    }

}
