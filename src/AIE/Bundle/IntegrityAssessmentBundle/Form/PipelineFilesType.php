<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class PipelineFilesType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('filename', 'text', $this->options(array('label' => 'Name')))
            ->add('caption', 'text', $this->options(array('label' => 'Caption', 'required' => false)))
            ->add('company', 'text', $this->options(array('label' => 'Company', 'required' => true)))
            ->add('document_number', 'text', $this->options(array('label' => 'Document Number', 'required' => true)))
            ->add('document_date', new DatePickerType(), $this->options(array('mapped' => true, 'label' => 'Document Date', 'attr' => ['class' => 'col-md-9'], 'label_attr' => ['class' => 'col-md-3 control-label']), ''))
            ->add('file', 'file', $this->options(array('label' => ' '), 'file'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineFiles'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_pipelinefiles';
    }

}
