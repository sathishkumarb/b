<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class WorkshopReportType extends AbstractType {

    use Helper\FormStyleHelper;

    public $_workshops;

    public function __construct($workshops)
    {
        $this->_workshops = $workshops;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->_workshops as $workshop)
        {
            $builder
                ->add('WORKSHOP_' . $workshop->getId(), 'hidden', array(
                    'label'    => 'Workshop #' . $workshop->getId(),
                    'required' => false,
                    'mapped'   => false,
                ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'empty_data' => function (FormInterface $form)
                {
                    $workshops = [];
                    foreach ($this->_workshops as $workshop)
                    {
                        $workshops['WORKSHOP_' . $workshop->getId()] = $form->get('WORKSHOP_' . $workshop->getId())->getData();
                    }

                    return $workshops;
                },
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_workshop_report';
    }

}
