<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;
use Doctrine\ORM\EntityRepository;

class SectionsThreatsMitigationsOwnerType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;
    private $pipelineId;
    private $emails;

    public function __construct(SectionsThreatsMitigations $entity, $pipelineId, $emails)
    {
        $this->entity = $entity;
        $this->pipelineId = $pipelineId;
        $this->emails = $emails;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('action_owner', 'entity', $this->options(
                    array(
                        'class'    => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineEmails',
                        'property' => 'name',
                        'choices'  => $this->emails,
                        'label'       => false,
                        'expanded' => false,
                        'required' => false,
                        'multiple' => false,
                        'data'     => $this->entity->getActionOwner(),
                        'attr'     => array('class' => ''),
                        'empty_value' => 'Choose an option',
                    ), 'entity'
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations',
            /*'empty_data' => function (FormInterface $form)
                {
                    $actionOwner = new SectionsThreatsMitigationsActionOwners();
                    $actionOwner->setActionOwner($form->get('action_owner')->getData());

                    return $actionOwner;
                },*/
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'aie_bundle_integrityassessmentbundle_sectionsthreatsmitigationsowners';
    }

} 