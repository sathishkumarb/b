<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PipelineRiskAssessmentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /*->add('date', 'choice', array(
                    'required' => true, 'choices' => $this->buildYearChoices()
                ));*/
        ;
    }

    public function buildYearChoices() {
        $distance = 5;
        //$yearsBefore = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") - $distance));
        $yearsCurrent = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y")));
        $yearsAfter = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") + $distance));
        //return array_combine(range($yearsBefore, $yearsAfter), range($yearsBefore, $yearsAfter));
        return array_combine(range($yearsCurrent, $yearsAfter), range($yearsCurrent, $yearsAfter));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_pipelineriskassessment';
    }

}
