<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;
use AIE\Bundle\IntegrityAssessmentBundle\Form\DatePickerType;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations;

class SectionsThreatsMitigationsFrequencyType extends AbstractType {

    use Helper\FormStyleHelper;

    private $entity;

    public function __construct(SectionsThreatsMitigations $entity) {
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('frequency', 'text', $this->options(array('label' => false, 'data' => $this->entity->getFrequency(), 'attr' => array('class' => 'double manual_freq'))))
                ->add('frequency_justification', 'textarea', $this->options(array('label' => false, 'attr' => array('class' => 'justification'), 'required' => false, 'data' => $this->entity->getFrequencyJustification())))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_sectionsthreatsmitigations';
    }

}
