<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AIE\Bundle\IntegrityAssessmentBundle\Helper;

class UserProjectGroupType extends AbstractType {

    use Helper\FormStyleHelper;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /*
                  ->add('user', 'entity', array(
                  'class' => 'UserBundle:User',
                  'property' => 'username',
                  )) */
                ->add('project', 'entity', $this->options(array(
                            'class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\Projects',
                            'property' => 'name',
                )))
                ->add('group', 'entity', $this->options(array(
                            'class' => 'UserBundle:Group',
                            'property' => 'name',
                            'label' => 'Position'
                )))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AIE\Bundle\IntegrityAssessmentBundle\Entity\UserProjectGroup'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'aie_bundle_integrityassessmentbundle_userprojectgroup';
    }

}
