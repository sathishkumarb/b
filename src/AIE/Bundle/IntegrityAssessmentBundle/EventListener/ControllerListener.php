<?php
/**
 * User: Mokha
 * Date: 6/30/14
 * Time: 1:17 AM
 */

namespace AIE\Bundle\IntegrityAssessmentBundle\EventListener;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use AIE\Bundle\IntegrityAssessmentBundle\Security\AuthenticatedController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ControllerListener {

    private $securityContext;

    public function __construct($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (! is_array($controller))
        {
            return;
        }

        if ($controller[0] instanceof AuthenticatedController)
        {
            /*
            $token = $event->getRequest()->query->get('token');
            if (! in_array($token, $this->tokens))
            {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }*/

            $request = $controller[0]->get('request');
            $routeName = $request->get('_route');
            $em = $controller[0]->getDoctrine()->getManager();

            $roles = $controller[0]->getRoutesRoles();
            $object = $controller[0]->detectSecureObject($request, $em);

            //detect if passed is a project object
            //set object to project
            //else return false

            if (! isSet($roles[$routeName]))
            {
                return;
            }

            $role = $roles[$routeName];

            if (!$this->securityContext->isGranted($role, $object))
            {
                throw new AccessDeniedException();
            }

            // mark the request as having passed token authentication
            //$event->getRequest()->attributes->set('auth_token', $token);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        // check to see if onKernelController marked this as a token "auth'ed" request
        /*if (!$token = $event->getRequest()->attributes->get('auth_token')) {
            return;
        }

        $response = $event->getResponse();

        // create a hash and set it as a response header
        $hash = sha1($response->getContent().$token);
        $response->headers->set('X-CONTENT-HASH', $hash);*/
    }
} 