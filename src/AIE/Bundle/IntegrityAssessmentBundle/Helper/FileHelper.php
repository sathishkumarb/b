<?php
/**
 * Created by PhpStorm.
 * User: Mokha
 * Date: 6/11/14
 * Time: 2:27 AM
 */

namespace AIE\Bundle\IntegrityAssessmentBundle\Helper;


class FileHelper {

	/**
	 * Converts bytes into human readable file size.
	 *
	 * @param string $bytes
	 * @return string human readable file size (2,87 Мб)
	 * @author Mogilev Arseny
	 */
	public static function fileSizeConvert($bytes) {
		$result = 0;
		$bytes = floatval($bytes);
		$arBytes = [
			0 => [
				"UNIT"  => "TB",
				"VALUE" => pow(1024, 4)
			],
			1 => [
				"UNIT"  => "GB",
				"VALUE" => pow(1024, 3)
			],
			2 => [
				"UNIT"  => "MB",
				"VALUE" => pow(1024, 2)
			],
			3 => [
				"UNIT"  => "KB",
				"VALUE" => 1024
			],
			4 => [
				"UNIT"  => "B",
				"VALUE" => 1
			],
		];

		foreach ($arBytes as $arItem) {
			if ($bytes >= $arItem[ "VALUE" ]) {
				$result = $bytes / $arItem[ "VALUE" ];
				$result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem[ "UNIT" ];
				break;
			}
		}

		return $result;
	}

	/**
	 * A function that cleans file names to remove non-allowable characters
	 *
	 * @param $filename
	 * @return mixed
	 */
	public static function cleanFileName($filename) {
		$bad = array_merge(
			array_map('chr', range(0, 31)), ["<", ">", ":", '"', "/", "\\", "|", "?", "*"]);

		$filename = str_replace($bad, "", $filename);
		$filename = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $filename); #remove non-ascii characters

		return $filename;
	}
} 