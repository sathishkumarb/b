<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Helper;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class LoadsheetAssessmentHelper
{
    private $em;
    private $excelobj;
    private $projectId;
    private $defaultem;
    private $sourceJSON;
    private $sourceArray;
    private $tableHeaders;
    private $validationArray;
    private $documentInfo;
    private $sheetsInfo;
    private $sourcesHelper;
    private $sourcesSheetArray;

    
    public function __construct($excelobj, $em, $defaultEm, $projectId)
    {
        $this->excelobj = $excelobj;
        $this->em = $em;
        $this->defaultem = $defaultEm;
        $this->projectId = $projectId;
        $this->sourceJSON=file_get_contents('../web/assets/js/pipelineAssessLoadsheetSource.json');

        $this->sourceArray = json_decode($this->sourceJSON, true);
//        echo("sourceArray:" .  $this->sourceArray);
//        $this->sourceArray = $this->jsonDecode($this->sourceJSON);


        $this->documentInfo =  $this->sourceArray['document'];
        $this->sheetsInfo =  $this->sourceArray['sheets'];
        //create object of the configurable loadsheet sources helper class
        $this->sourcesHelper=new LoadsheetAssessmentSourcesHelper($em,$defaultEm,$projectId);
        //create the headers array
        $this->tableHeaders=[];

        $this->parseJsonIntoColumns($this->sourceArray['columns'],false,$this->tableHeaders);
        //create the validation array
        $this->validationArray=[];
        $this->parseJsonIntoValidationArray($this->sourceArray['columns'],$this->validationArray);
        //create sources sheet array from the sources helper object
        $this->sourcesSheetArray=$this->sourcesHelper->defineSourcesSheetArray($this->sheetsInfo['sourcesSheet']['title'], $this->validationArray);
    }




    private function getColumnFullName($colName, $isMand){
        if ($isMand=="1"){
            return $colName ."*";
        }elseif ($isMand=="c"){
            return $colName ."**";
        }else{
            return $colName;
        }
    }

    public function isValidLoadSheet(){
        $objWorksheet = $this->excelobj->getActiveSheet();
        $isValid=true;
        // Check if all the columns of the uploaded excel file are matching those in the master load sheet.
        $headerRows=count($this->tableHeaders);

        for ($i=1;$i<=$headerRows;$i++){
            foreach ($this->tableHeaders['row' . $i] as $row){

                if ($objWorksheet->getCell($row['cell'])->getValue() != $this->getColumnFullName($row['name'],$row['mandatory'])){
                    $isValid=false;
//                    echo("lhs is : " . $objWorksheet->getCell($row['cell'])->getValue() . " ");
//                    echo("rhs is : " . $this->getColumnFullName($row['name'],$row['mandatory']) . " ");
                }
            }
        }

        if ($isValid){
            //Check if the creator, title, subject, description, keywords of the uploaded excel file match those of the master load sheet
            if($this->excelobj->getProperties()->getCreator()!=$this->documentInfo['creator']){
                $isValid=false;


            }elseif($this->excelobj->getProperties()->getTitle()!=$this->documentInfo['title']) {
                $isValid = false;


            }elseif($this->excelobj->getProperties()->getSubject()!=$this->documentInfo['subject']) {
                $isValid = false;


            }elseif($this->excelobj->getProperties()->getDescription()!=$this->documentInfo['description']) {
                $isValid = false;

            }elseif($this->excelobj->getProperties()->getKeywords()!=$this->documentInfo['keywords']) {
                $isValid = false;

            }else{
                $isValid = true;
            }
        }

        return $isValid;
    }

    public function getHeader(){
        return $this->tableHeaders;
    }

    public function getValidationArray(){
        return $this->validationArray;
    }

    public function getDataSheetHeaderColumnsCount(){
        return $this->sheetsInfo['dataSheet']['style']['header']['headersCount'];
    }

    public function getDataSheetHeaderRowsCount(){
        return $this->sheetsInfo['dataSheet']['style']['header']['headerRows'];
    }

    private function isMerged($colSpan,$rowSpan){
        if($colSpan>1 or $rowSpan>1)
            return true;
        return false;
    }

    private function getCell($colNumber,$rowNumber){
        return $this->getColumnNameByColumnNumber($colNumber) . $rowNumber;
    }

    private function parseJsonIntoColumns(array $array, $headerIsDefined, &$headersArray) {
        if (!$headerIsDefined){
            for ($i=1;$i<=$this->sheetsInfo['dataSheet']['style']['header']['headerRows'];$i++){
                $headersArray['row'.$i]=[];
            }
        }

        foreach ($array as $value) { // for each column of the associative array (decoded from the json source of empty master load sheet)
            array_push($headersArray['row' . $value['tableInfo']['rowNumber']],
                ['name'=>$value['name'],
                    'mandatory'=>(isset($value['mandatory']) ? $value['mandatory'] : null),
                    'mandatoryFor'=> (isset($value['mandatoryFor']) ? ($value['mandatoryFor'] == null? null : implode(", ", $value['mandatoryFor'])) : null),
                    'rowspan'=>$value['tableInfo']['rowSpan'],
                    'colspan'=>$value['tableInfo']['colSpan'],
                    'cell'=>$this->getCell($value['tableInfo']['colNumber'],$value['tableInfo']['rowNumber']),
                    'merge'=>(($this->isMerged($value['tableInfo']['colSpan'],$value['tableInfo']['rowSpan'])) ? $this->getMergeRange($value['tableInfo']['colNumber'],$value['tableInfo']['rowNumber'],$value['tableInfo']['colSpan'],$value['tableInfo']['rowSpan']): null),
                    'col'=>$this->getColumnNameByColumnNumber($value['tableInfo']['colNumber'])]);
            if ($value['hasChild']){
                if (is_array($value['columns'])) {
                    $this->parseJsonIntoColumns($value['columns'],true,$headersArray);
                }
            }
        }
    }

    private function parseJsonIntoValidationArray(array $array, &$validationArray) {
        foreach ($array as $value) {
            if ($value['hasChild']){

                if (is_array($value['columns'])) {
                    $this->parseJsonIntoValidationArray($value['columns'],$validationArray);
                }
            }else{
                array_push($validationArray,['name'=>$value['id'],
                    'type'=>$value['type'],
                    'mandatory'=>$value['mandatory'],
//                    'mandatoryFor'=>$value['mandatoryFor'],
                    'source'=>$value['source'],
                    'check'=>$this->sourcesHelper->getCheckSourceArray($value['source'])
//                    'key'=>$value['key']
                ]);
            }
        }
    }

    public function readLoadSheetDataRows($startrow, $maxcolNumber = 0){
        $objWorksheet = $this->excelobj->getActiveSheet();
        $retArray=[];
        $loopVal=true;
        $rownumber =1;
        if ($maxcolNumber==0){ // If max column number is not provided, determine the max column number by iterating through the cells of the loaded excel file and stopping when an empty cell is encountered. Even if sheet is empty, it will return the number of columns across which header spans
            while ($loopVal){
                if ($objWorksheet->getCellByColumnAndRow($maxcolNumber, $rownumber)->getValue()=== NULL || $objWorksheet->getCellByColumnAndRow($maxcolNumber, $rownumber)->getValue() === ''){
                    $loopVal=false;
                }else{
                    $maxcolNumber = $maxcolNumber+1;
                }
            }
        } // end of if for maxcolNumber is 0

        $loopVal=true;
        $rownumber=$startrow;
        $colnumber=0;
        while ($loopVal){ // loop until you encounter an empty cell; push each cell value into the 2 dimensional $retArray.. SO, for 4th row, we will have $retArray[1] = an array holding cells values of fourth excel row
            if ($objWorksheet->getCellByColumnAndRow($colnumber, $rownumber)->getValue()=== NULL || $objWorksheet->getCellByColumnAndRow($colnumber, $rownumber)->getValue() === ''){
                $loopVal=false;
            }else{
                $retArray[$rownumber-$startrow]=[];
                while ($colnumber<$maxcolNumber){
                    if(!empty($objWorksheet->getCellByColumnAndRow($colnumber, $rownumber)->getValue()) && $this->validationArray[$colnumber]['type']=='date' && PHPExcel_Shared_Date::isDateTime($objWorksheet->getCellByColumnAndRow($colnumber, $rownumber))) { //to check the date
                        $field = PHPExcel_Shared_Date::ExcelToPHPObject($objWorksheet->getCellByColumnAndRow($colnumber, $rownumber)->getValue());
                    }else{
                        $field = $objWorksheet->getCellByColumnAndRow($colnumber, $rownumber)->getValue();
                    }
                    array_push($retArray[$rownumber-$startrow], $field);
                    $colnumber=$colnumber+1;
                }
                $rownumber=$rownumber+1;
                $colnumber=0;
            }
        }


        $checkArray=$this->validationArray;


        $finalArray=['dataStatus'=>1,'data'=>[]];
        for ($i=0;$i<count($retArray);$i++){ // iterates over all the excel rows
            $dataRow=$this->getVerifiedDataItem($retArray,$i,$checkArray);

            if($dataRow['itemStatus']==0){
                $finalArray['dataStatus']=0;
            }
            array_push($finalArray['data'],$dataRow);
        }

        if (count($retArray) == 0){ // if no pipeline has been specified or empty excel has been uploaded.
            $finalArray['dataStatus']=0;
        }

        return $finalArray;
    }

    private function getVerifiedDataItem($inputArray, $index, $checkArray){
        $outputItem=['itemStatus'=>1,'item'=>[]]; // itemStatus will contain cumulative status for the row and item shall contain an array of all individual fields/ columns
        for ($j=0;$j<count($inputArray[$index]);$j++) { // iterates over each column of the excel row
            $type=$checkArray[$j]['type'];
            $mandatory=$checkArray[$j]['mandatory'];
//            $mandatoryFor=$checkArray[$j]['mandatoryFor'];
//            $check=$checkArray[$j]['source']; // array of LIST parameters visible on the excel (from the sources)
            $check=$checkArray[$j]['check']; // array of LIST parameters visible on the excel (from the sources)
//            $key=$checkArray[$j]['key']; // For ex: in designCode array - it is array with key: assetcategory and value:designcode. So, for each row, we need to validate that the design code is within the values specified for the assetcategory of that row
//            $keyval=null;
//            if ($key!=null){ //If key exists, fetch the 0 based index (in the excel) of the key col
//                $indexofKey=0;
//                for ($z=0;$z<count($inputArray[$index]);$z++) {
//                    if ($checkArray[$z]['name']==$key){
//                        $indexofKey=$z;
//                    }
//                }
//                $keyval=$inputArray[$index][$indexofKey]; // get the value of the key column for the excel row. For ex in case of designCode, the value of asset category could be 'Pipelines'.
//            }


            $dataItem=$this->verifyItemEntity($inputArray[$index][$j],$type,$mandatory,$check); // function to verify each cell of the excel row in question

          if ($dataItem['status']=='error'){
                $outputItem['itemStatus']=0;
            }
            array_push($outputItem['item'], $dataItem);
        }

        return $outputItem;
    }

    private function is_empty_field($value){
        if ($value === 0 || $value === 0.0)
            return false;

        elseif($value=="" || empty($value) || $value==null || ctype_space($value)){
            return true;
        }
        else
            return false;
    }

    private function verifyItemEntity($field, $vType, $vMand,  $vCheck){
        $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];

        //First, check if it is a mandatory column and left empty
        if ($vMand=='1' && ($this->is_empty_field($field))) {
            $resulsArray = ['status' => 'error', 'message' => 'Mandatory field', 'value' => $field];
        }
        // Then, if that is not so, whether mandatory or not, check if the cell is filled. if so, do type specific checks including list.
        elseif (!$this->is_empty_field($field))
        {
           if (($vType=='float' || $vType=='float') && !is_numeric($field)) // if a numeric cell has non - numeric data
                $resulsArray=['status' => 'error', 'message'=>'Numeric field' , 'value'=>$field];
//           elseif ($vType=='integer'  && !is_integer($field))
//                $resulsArray=['status' => 'error', 'message'=>'Integer field' , 'value'=>$field];
           elseif ($vType=='list' && !in_array($field,$vCheck, true)) // if a list type of cell does not have data from the list
               $resulsArray=['status' => 'error', 'message'=>'Should be part of ' . implode(", ",$vCheck) , 'value'=>$field];
           elseif ($vType=='boolean' && $field!="Yes" && $field!="No") //if a boolean type cell has data other than yes, no
               $resulsArray=['status' => 'error', 'message'=>'This field should be Yes/ No' , 'value'=>$field];
           elseif ($vType == 'date' || $vType=='time' || $vType == 'comma'){
               if($vType=='date'){
                   if ($field instanceof \DateTime) {
                       $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>date_format($field, 'Y-m-d')];
                   }else{
                       $resulsArray=['status' => 'error', 'message'=>'Cell value must be datetime' , 'value'=>$field];
                   }
               }elseif ($vType=='time'){
                   $timeObj=explode (":",$field);
                   if(!is_array($timeObj) || count($timeObj)!=2){
                       $resulsArray=['status' => 'error', 'message'=>'This field is time formatted as hh:mm' , 'value'=>$field];
                   }else{
                       if ($timeObj[0]!="" && $timeObj[1] != "" && is_numeric($timeObj[0]) & is_numeric($timeObj[1])){
                           $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
                       }else{
                           $resulsArray=['status' => 'error', 'message'=>'Incorrect time format' , 'value'=>$field];
                       }
                   }
               }elseif ($vType=='comma'){
                   $timeObj=explode (",",$field);
                   if(!is_array($timeObj) || count($timeObj)!=2){
                       $resulsArray=['status' => 'error', 'message'=>'This field is time formatted as (value, value)' , 'value'=>$field];
                   }else{
                       if ($timeObj[0]!="" && $timeObj[1] != ""){
                           $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
                       }else{
                           $resulsArray=['status' => 'error', 'message'=>'Incorrect comma format' , 'value'=>$field];
                       }
                   }
               }
           }

        else
            $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
        }
        else
            $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];


        return $resulsArray;
    }

    private function verifyItemEntityGeneral($field, $regName, $vType, $vMand, $vMandFor, $vCheck, $vKey, $keyValue=null){
        $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
        //if (column is mandatory but field is empty) or (col is conditional mandatory and of type LIST and is mandatory for the fields' register (Pipeline, piping etc) , but field is empty)
        if (($vMand=='1' && $vMandFor==null && ($this->is_empty_field($field))) || ($vMand=='c' && $vMandFor!=null && $this->is_empty_field($field) && $vType=='list' && in_array($regName, $vMandFor, true))){
            if ($vKey==null && $keyValue==null){
                $resulsArray=['status' => 'error', 'message'=>'Mandatory field' , 'value'=>$field];
            }else{
                if($keyValue=="Yes" || $this->is_empty_field($keyValue) || ($vType=='list' && $regName==$keyValue)){
                    $resulsArray=['status' => 'error', 'message'=>'Mandatory field' , 'value'=>$field];
                }
                else{
                    $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
                }
            }
        }elseif(!empty($field) && $vType=='float' && !is_numeric($field)){ // if a numeric column has non - numeric data
            $resulsArray=['status' => 'error', 'message'=>'Numeric field' , 'value'=>$field];
        }elseif($vMand=='1' && $vMandFor==null && !empty($field)& $vType=='list'){ // if a list type of column does not have data from the list
            if ($vKey==null){
                if(!in_array($field,$vCheck, true)){
                    $resulsArray=['status' => 'error', 'message'=>'Should be part of ' . implode(", ",$vCheck) , 'value'=>$field];
                }
            }else{
                if (isset($vCheck[$keyValue])) {
                    if (!in_array($field, $vCheck[$keyValue], true)) {
                        $resulsArray = ['status' => 'error', 'message' => 'Should be part of ' . implode(", ", $vCheck[$keyValue]), 'value' => $field];
                    }
                }elseif ($keyValue == "Yes"){
                    if(!in_array($field,$vCheck, true)){
                        $resulsArray=['status' => 'error', 'message'=>'Should be part of ' . implode(", ",$vCheck) , 'value'=>$field];
                    }
                }else{
                    $resulsArray=['status' => 'error', 'message'=>'No register with name ' . $regName , 'value'=>$field];
                }
            }
        }elseif($vMand=='1' && $vMandFor==null && !empty($field)& $vType=='boolean') { //if boolean type columns has data other than 'Yes/ No'
            if ($field!="Yes" && $field!="No"){
                $resulsArray=['status' => 'error', 'message'=>'This field should be Yes/No' , 'value'=>$field];
            }
        }elseif($vMand=='c' && $vMandFor!=null){
            if (in_array($regName,$vMandFor, true) && (empty($field) || ctype_space($field) || $field =="")){
                $resulsArray=['status' => 'error', 'message'=>'Mandatory field for ' . $regName . ' register' , 'value'=>$field];
            }elseif(!empty($field) && $vType=='float' && !is_numeric($field)){
                $resulsArray=['status' => 'error', 'message'=>'Numeric field' , 'value'=>$field];
            }else{
                $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
            }
        }elseif ($vType == 'date' || $vType=='time' || $vType == 'comma'){
            if($vType=='date' && !empty($field)){
                if ($field instanceof \DateTime) {
                    $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>date_format($field, 'Y-m-d')];
                }else{
                    $resulsArray=['status' => 'error', 'message'=>'Field must be datetime' , 'value'=>$field];
                }
            }elseif ($vType=='time' && !empty($field)){
                $timeObj=explode (":",$field);
                if(!is_array($timeObj) || count($timeObj)!=2){
                    $resulsArray=['status' => 'error', 'message'=>'This field is time formatted as hh:mm' , 'value'=>$field];
                }else{
                    if ($timeObj[0]!="" && $timeObj[1] != "" && is_numeric($timeObj[0]) & is_numeric($timeObj[1])){
                        $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
                    }else{
                        $resulsArray=['status' => 'error', 'message'=>'Incorrect time format' , 'value'=>$field];
                    }
                }
            }elseif ($vType=='comma' && !empty($field)){
                $timeObj=explode (",",$field);
                if(!is_array($timeObj) || count($timeObj)!=2){
                    $resulsArray=['status' => 'error', 'message'=>'This field is time formatted as (value, value)' , 'value'=>$field];
                }else{
                    if ($timeObj[0]!="" && $timeObj[1] != ""){
                        $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
                    }else{
                        $resulsArray=['status' => 'error', 'message'=>'Incorrect comma format' , 'value'=>$field];
                    }
                }
            }
        }else{
            $resulsArray=['status' => 'success', 'message'=>'' , 'value'=>$field];
        }
        return $resulsArray;
    }

    private function createSheets($objPHPExcel){
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle($this->sheetsInfo['dataSheet']['title']);
        //for sources
        $objWorkSheet = $objPHPExcel->createSheet(1);
        $objWorkSheet->setTitle($this->sheetsInfo['sourcesSheet']['title']);
        foreach ($this->validationArray as $chkData){
            if ($chkData['type']=='list') {
                if (!is_array($chkData['source'])) {
                    $objWorkSheet->fromArray(array_map(function($value) { return [$value]; }, $this->sourcesSheetArray[$chkData['source']]['data']), NULL, $this->sourcesSheetArray[$chkData['source']]['cell']);
                }
            }
        }

        $objPHPExcel->setActiveSheetIndex(0);
    }

    private function fillSourcesSheet($objPHPExcel){
        $objPHPExcel->setActiveSheetIndex(1);
        foreach ($this->validationArray as $chkData){
            if ($chkData['type']=='list') {
                if (!is_array($chkData['source']) && count($this->sourcesSheetArray[$chkData['source']]['data']) > 0) {
                    $objPHPExcel->getActiveSheet()->fromArray(array_map(function($value) { return [$value]; }, $this->sourcesSheetArray[$chkData['source']]['data']), NULL, $this->sourcesSheetArray[$chkData['source']]['cell']);

                }
            }
        }
        $objPHPExcel->setActiveSheetIndex(0);
    }

    private function protectDocumentAndSheets($objPHPExcel){
        if($this->documentInfo['protection']['isProtected']){
            $objPHPExcel->getSecurity()->setLockWindows($this->documentInfo['protection']['lockWindow']);
            $objPHPExcel->getSecurity()->setLockStructure($this->documentInfo['protection']['lockStructure']);
            $objPHPExcel->getSecurity()->setWorkbookPassword($this->documentInfo['protection']['password']);
        }
        $objPHPExcel->setActiveSheetIndex(0);
        if ($this->sheetsInfo['dataSheet']['isHidden']){
            $objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
        }
        if($this->sheetsInfo['dataSheet']['protection']['isProtected']){
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setSort($this->sheetsInfo['dataSheet']['protection']['sortProtected']);
            $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows($this->sheetsInfo['dataSheet']['protection']['insertProtect']);
            $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells($this->sheetsInfo['dataSheet']['protection']['formatProtected']);
            $objPHPExcel->getActiveSheet()->getProtection()->setPassword($this->sheetsInfo['dataSheet']['protection']['password']);
        }
        // for sources
        $objPHPExcel->setActiveSheetIndex(1);
        if ($this->sheetsInfo['sourcesSheet']['isHidden']){
            $objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
        }
        if($this->sheetsInfo['sourcesSheet']['protection']['isProtected']){
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
            $objPHPExcel->getActiveSheet()->getProtection()->setSort($this->sheetsInfo['sourcesSheet']['protection']['sortProtected']);
            $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows($this->sheetsInfo['sourcesSheet']['protection']['insertProtect']);
            $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells($this->sheetsInfo['sourcesSheet']['protection']['formatProtected']);
            $objPHPExcel->getActiveSheet()->getProtection()->setPassword($this->sheetsInfo['sourcesSheet']['protection']['password']);
        }
        $objPHPExcel->setActiveSheetIndex(0);
    }

    public function downloadExcelLoadsheet($isProtected=true)
    {
        $headerCols = $this->sheetsInfo['dataSheet']['style']['header']['headersCount'];
        $allowedRowsCount = $this->sheetsInfo['dataSheet']['style']['allowedRowsCount'];
        $headerRows = $this->sheetsInfo['dataSheet']['style']['header']['headerRows'];

        try{
        $objPHPExcel =$this->excelobj;
        $objPHPExcel->getProperties()
            ->setCreator($this->documentInfo['creator'])
            ->setLastModifiedBy($this->documentInfo['lastModifiedBy'])
            ->setTitle($this->documentInfo['title'])
            ->setSubject($this->documentInfo['subject'])
            ->setDescription($this->documentInfo['description'])
            ->setKeywords($this->documentInfo['keywords']);
        $this->createSheets($objPHPExcel);
        $this->fillSourcesSheet($objPHPExcel);

        $headerStyle = new PHPExcel_Style();
        $headerStyle->applyFromArray(
            array('fill' 	=> array(
                'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
                'color'		=> array('rgb' => $this->sheetsInfo['dataSheet']['style']['header']['headerColor'])
            ),
                'borders' => array(
                    'allborders'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
                )
            ));
        $cellStyle = new PHPExcel_Style();
        $cellStyle->applyFromArray(
            array('borders' => array(
                'allborders'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
            )
            ));

        for ($i=1;$i<=$headerRows;$i++){
            foreach ($this->tableHeaders['row' . $i] as $row){
                if ($row['rowspan']>1){
                    $objPHPExcel->getActiveSheet()->mergeCells($row['merge']);
                }else{
                    if ($row['colspan']>1){
                        $objPHPExcel->getActiveSheet()->mergeCells($row['merge']);
                    }
                }
                $objPHPExcel->getActiveSheet()->SetCellValue($row['cell'], $this->getColumnFullName($row['name'],$row['mandatory']))->getColumnDimension($row['col'])->setWidth($this->sheetsInfo['dataSheet']['style']['cellWidth']);
            }
        }
        $headerRange=$this->getColumnNameByColumnNumber($this->sheetsInfo['dataSheet']['style']['header']['colNumber']) . $this->sheetsInfo['dataSheet']['style']['header']['rowNumber'] .
            ":" . $this->getColumnNameByColumnNumber($this->sheetsInfo['dataSheet']['style']['header']['headersCount']) . $this->sheetsInfo['dataSheet']['style']['header']['headerRows'];
        $objPHPExcel->getActiveSheet()->setSharedStyle($headerStyle, $headerRange);

            //            'wrap text'
            $objPHPExcel->getDefaultStyle()->getAlignment()->setWrapText(true);

        $lastColumnName = 'A';
        for ($i=2;$i<=$headerCols;$i++){
            $lastColumnName++;
        }

        $objPHPExcel->getActiveSheet()->setSharedStyle($cellStyle, "A" . ($headerRows + 1) . ":". $lastColumnName . ($headerRows + $allowedRowsCount));
        $objPHPExcel->getActiveSheet()
            ->getStyle($headerRange)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle("A" . ($headerRows + 1) . ":". $lastColumnName .($headerRows + $allowedRowsCount))->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $booleanArray=["Yes","No"];
        for ($ind=$headerRows + 1;$ind<=($headerRows + $allowedRowsCount);$ind++){
            $Letter = "A";
            foreach ($this->validationArray as $chkData){
                if ($chkData['type']=='list'){
//                    if (is_array($chkData['source']) && $chkData['source'] != null ){
//                        $this->createDropDownInExcelCell($chkData['source'],$objPHPExcel,$Letter.$ind);
//                        $this->createDropDownInExcelCell($this->sourcesSheetArray[$chkData['source']]['range'],$objPHPExcel,$Letter.$ind);
//                    }
//                    else{
//                        if (is_array($chkData['check'])){
//                            $this->createDropDownInExcelCell($this->sourcesHelper->getCheckSourceArray($chkData['check']),$objPHPExcel,$Letter.$ind);
//                        }
//                    }
                   if (!is_array($chkData['source']) && $chkData['source'] != null and isset($this->sourcesSheetArray[$chkData['source']])){
                        $this->createDropDownInExcelCell($this->sourcesSheetArray[$chkData['source']]['range'],$objPHPExcel,$Letter.$ind);
                    }else{
                        if (is_array($chkData['check'])){
                            $this->createDropDownInExcelCell($this->sourcesHelper->getCheckSourceArray($chkData['check']),$objPHPExcel,$Letter.$ind);
                        }
                    }
                }elseif ($chkData['type']=='boolean'){
                    $this->createDropDownInExcelCell($booleanArray,$objPHPExcel,$Letter.$ind);
                }elseif ($chkData['type']=='float'){
                    $this->createNumericalInExcelCell($objPHPExcel,$Letter.$ind);
                }elseif ($chkData['type']=='date'){
                    $this->createDateFormatInExcelCell($objPHPExcel,$Letter.$ind);
                }elseif ($chkData['type']=='time' || $chkData['type']=='comma'){
                    $this->createSplitInExcelCell($objPHPExcel,$Letter.$ind,$chkData['type']);
                }
                $Letter++;
            }
        }

        if ($isProtected){
            $this->protectDocumentAndSheets($objPHPExcel);
            }
        }
        catch (\Exception $e) {
            echo($e->getMessage());
            }
        return $objPHPExcel;
    }


    private function createDropDownInExcelCell($verificationList,&$excelObj, $cellName){
        $isvaild = true;
        if (!is_array($verificationList)){
            $configs = '='.$verificationList;
        }else{
            if ($verificationList == null){
                $isvaild = false;
            }else {
                $configs = str_replace('"', '""', implode(",", $verificationList));
                $configs = preg_replace('/[^\00-\255]+/u', '', $configs);
            }
        }
        if($isvaild){
            $objValidation = $excelObj->getActiveSheet()->getCell($cellName)->getDataValidation();
            $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
            $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in part of the list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            if (!is_array($verificationList) && $verificationList!=null){
                $objValidation->setFormula1($configs);
            }else{
                $objValidation->setFormula1('"'.$configs.'"');
            }
        }else{

        }

    }

    private function createDateFormatInExcelCell(& $excelObj, $cellName){
        $objValidation = $excelObj->getActiveSheet()->getCell($cellName)->getDataValidation();
        $objValidation->setType( \PHPExcel_Cell_DataValidation::TYPE_DATE ); //'or whole'
        $objValidation->setAllowBlank(true);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP ); //'stop'
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value must be Date');
        $objValidation->setPromptTitle('Date field');
        $objValidation->setPrompt('The value of the field must be date');
        $objValidation->setAllowBlank(true);
        $format = 'yyyy-mm-dd';
        $excelObj->getActiveSheet()->getStyle($cellName)->getNumberFormat()->setFormatCode($format);

    }

    private function createNumericalInExcelCell(& $excelObj, $cellName,$min=0,$max=999999){
        $objValidation = $excelObj->getActiveSheet()->getCell($cellName)->getDataValidation();
        $objValidation->setType( \PHPExcel_Cell_DataValidation::TYPE_DECIMAL ); //'or whole'
        $objValidation->setOperator(\PHPExcel_Cell_DataValidation::OPERATOR_BETWEEN);
        $objValidation->setAllowBlank(true);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP ); //'stop'
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value must be numerical');
        $objValidation->setPromptTitle('Numerical Value');
        $objValidation->setPrompt('The value of the field must be numerical');
        $objValidation->setFormula1($min);
        $objValidation->setFormula2($max);
        $objValidation->setAllowBlank(true);
        $excelObj->getActiveSheet()->getStyle($cellName)->getNumberFormat()->setFormatCode('00.00');
    }

    private function createSplitInExcelCell(& $excelObj, $cellName, $type){
        $objValidation = $excelObj->getActiveSheet()->getCell($cellName)->getDataValidation();
        $objValidation->setType( \PHPExcel_Cell_DataValidation::TYPE_TEXTLENGTH ); //'or whole'
        $objValidation->setAllowBlank(true);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP ); //'stop'
        $objValidation->setErrorTitle('Input error');
        if ($type=="comma"){
            $objValidation->setError('Value must be formatted as value,value');
            $objValidation->setPromptTitle('Comma separated Value');
            $objValidation->setPrompt('The value of the field must be formatted as value,value');
        }else{
            $objValidation->setError('Value must be formated as hh:mm');
            $objValidation->setPromptTitle('Time Value');
            $objValidation->setPrompt('The value of the field must be formatted as hh:mm, and not exceeding 5 digits');
            //$objValidation->setFormula1(5);
        }

        $objValidation->setAllowBlank(true);
        $excelObj->getActiveSheet()->getStyle($cellName)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
    }


    private function getExcelColumnNameByCellName($cell){
        $strLength = strlen($cell);
        if ($strLength==0){
            return null;
        }elseif ($strLength==2){
            return substr($cell,0,1);
        }elseif ($strLength==3){
            return substr($cell,0,2);
        }else{
            return null;
        }
    }
    private function getExcelRowNumberByCellName($cell){
        $strLength = strlen($cell);
        if ($strLength==0){
            return null;
        }elseif ($strLength==2){
            return substr($cell,1,1);
        }elseif ($strLength==3){
            return substr($cell,2,1);
        }else{
            return null;
        }
    }

    private function getColSpanBetweenTwoColumns($col1, $col2){
        if ($col2==$col1){
            return 1;
        }elseif ($col2<$col1){
            $tempCol2 = $col2;
            $col2=$col1;
            $col1=$tempCol2;
        }
        $counter=1;
        while ($col1!=$col2){
            $col1++;
            $counter++;
        }
        return $counter;
    }

    private function getDataSheetHeaderRowsFromArray(array $array) {
        $max_depth = 1;
        foreach ($array as $value) {
            if ($value['hasChild']){
                if (is_array($value['columns'])) {
                    $depth = $this->getDataSheetHeaderRowsFromArray($value['columns']) + 1;
                    if ($depth > $max_depth) {
                        $max_depth = $depth;
                    }
                }
            }
        }
        return $max_depth;
    }

    public function getColumnNameByColumnNumber($colNumber){
        $letter = 'A';
        if($colNumber==1)
            return $letter;
        for ($i=2;$i<=$colNumber;$i++){
            $letter++;
        }
        return $letter;
    }
    public function getMergeRange($colNumber,$currentRow,$colSpan,$rowSpan){
        $columnName= $this->getColumnNameByColumnNumber($colNumber);
        $secondCol = $columnName;
        for ($i=$colNumber;$i<($colNumber + $colSpan - 1);$i++)
            $secondCol++;
        return ($columnName . $currentRow . ":" . $secondCol . ($rowSpan + $currentRow - 1));
    }
}