<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Helper;

trait FormStyleHelper {

    public function textClass() {
        return 'form-control input-md ';
    }

    public function LabelClass() {
        return 'col-md-3 control-label ';
    }

    public function textareaClass() {
        return 'form-control ';
    }

    public function buttonClass() {
        return 'btn btn-primary ';
    }

    public function fileClass() {
        return 'input-file ';
    }

    public function choiceExpandableClass() {
        return 'col-md-9 ';
    }

    public function multipleChoiceClass() {
        return 'form-control ';
    }

    public function datePickerClass(){
        return ' ';
    }

    public function options(array $options, $type = 'text') {

        $style_options = array();

        $attr_class = isSet($options['attr']['class']) ? $options['attr']['class'] : '';
        $label_class = isSet($options['label_attr']['class']) ? $options['label_attr']['class'] : '';


        switch ($type) {
            case 'entity':
            case 'text':
            case 'number':
            case 'integer':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->textClass() . $attr_class
                    )
                );
                break;
            case 'textarea':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->textareaClass() . $attr_class
                    )
                );
                break;

            case 'file':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->fileClass() . $attr_class
                    )
                );
                break;

            case 'btn':
                $style_options = array(
                    'attr' => array(
                        'class' => $this->ButtonClass() . $attr_class
                    )
                );
                break;
            case 'choice_expandable':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->choiceExpandableClass() . $attr_class
                    )
                );
                break;
            case 'multiple_choice':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->multipleChoiceClass() . $attr_class
                    )
                );
                break;

            case 'date_picker':
                $style_options = array(
                    'label_attr' => array(
                        'class' => $this->labelClass() . $label_class
                    ),
                    'attr' => array(
                        'class' => $this->datePickerClass() . $attr_class
                    )
                );
                break;
            default:
                break;
        }

        return array_replace_recursive($options, $style_options);
    }

}

?>
