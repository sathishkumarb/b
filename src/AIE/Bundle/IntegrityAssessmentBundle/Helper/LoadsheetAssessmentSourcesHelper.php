<?php
/**
 * Created by AIE
 * Date: 29/06/18
 */

namespace AIE\Bundle\IntegrityAssessmentBundle\Helper;

use AIE\Bundle\IntegrityAssessmentBundle\Entity\Pipelines;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\Threats;
use AIE\Bundle\IntegrityAssessmentBundle\Entity\ProjectMitigationTechniques;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use PHPExcel_Style;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_Cell_DataValidation;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Protection;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;

class LoadsheetAssessmentSourcesHelper
{
    private $em;
    private $projectId;
    private $defaultem;
    private $pipelinesArray;
    private $threatsArray;
    private $mitigationTechniquesArray;
//    private $sectionsArray;

    public function __construct($em, $defaultEm, $projectId)
    {
        $this->em = $em;
        $this->defaultem = $defaultEm;
        $this->projectId = $projectId;
        $this->pipelinesArray = $this->getPipelinesArray();
        $this->threatsArray = $this->getThreatsArray();
        $this->mitigationTechniquesArray = $this->getMitigationTechniquesArray();
//        $this->sectionsArray = $this->getSectionsArray();
    }

    public function getCheckSourceArray($arg){

        if (is_array ($arg)){
            return $arg;
        }else{
            if ($arg=='pipelinesArray'){
                return $this->pipelinesArray;
            }elseif ($arg=='threatsArray'){
                return $this->threatsArray;
            }elseif ($arg=='mitigationTechniquesArray'){
                return $this->mitigationTechniquesArray;
//            }elseif ($arg=='sectionsArray'){
//                return $this->sectionsArray;
            }else{
                return null;
            }
        }
    }

    public function defineSourcesSheetArray($refSheetName, $validationArray){
        $arr=[];
        $letter = "A";
        foreach ($validationArray as $item){
            if ($item['type']=="list" && !is_array($item['source']) && $item['source'] != null){
                $arr[$item['source']]=["cell"=>$letter . "1",
                    "range"=>$refSheetName."!" . $letter . "1:" . $letter . (is_array($this->{$item['source']}) && count($this->{$item['source']}) > 0 ? count($this->{$item['source']}) : "1"),
                    'data'=>$this->{$item['source']}];
                ++$letter;

            }
        }
        return $arr;
    }

    private function getPipelinesArray(){
        $entities = $this->em->getRepository('AIEIntegrityAssessmentBundle:Pipelines')->findByProject($this->projectId);
        $pipelines=[];
        foreach ($entities as $entity){
            array_push($pipelines,$entity->getName());
        }
        return $pipelines;
    }

    private function getThreatsArray(){
        $entities = $this->em->getRepository('AIEIntegrityAssessmentBundle:Threats')->findAll();
        $threats=[];
        foreach ($entities as $entity){
            array_push($threats,$entity->getName());
        }
        return $threats;
    }


    private function getMitigationTechniquesArray(){

        $entities = $this->em->getRepository('AIEIntegrityAssessmentBundle:ProjectMitigationTechniques')->findByProject($this->projectId);
        $mitigationTechniques=[];
        foreach ($entities as $entity){
            array_push($mitigationTechniques,$entity->getMitigationTechnique());
        }
        return $mitigationTechniques;
    }

//    private function getSectionsArray(){
//        $entities = $this->em->getRepository('AIEIntegrityAssessmentBundle:PipelineSections')->findAll();
//        $threats=[];
//        foreach ($entities as $entity){
//            array_push($threats,$entity->getName());
//        }
//        return $threats;
//    }

}