<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Helper;

class ArrayHelper {

    public static function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && static::in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

    public static function array_flatten($array) {

        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $return = array_merge($return, static::array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;
    }

}