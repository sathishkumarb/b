<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PipelineSectionsRepository extends EntityRepository
{
    public function findTest()
    {
        return $this->getEntityManager()
                    ->createQuery('SELECT ps FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps')
                    ->getResult();
    }
}
