<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PipelineRiskAssessmentRepository extends EntityRepository {

    public function updateApproved($pipelineId) {
        
        $now = new \DateTime("now");
        $currYear = $now->format('Y');
        
        $start = new \DateTime();
        $start->setDate($currYear, 1, 1);
        $end = new \DateTime();
        $end->setDate($currYear, 12, 31);
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->update('AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineRiskAssessment', 'pra')
                ->set('pra.isApproved', 0)
                ->where('pra.pipeline = :id')->setParameter('id', $pipelineId)
                ->andWhere('pra.date BETWEEN :start AND :end')->setParameter('start', $start)->setParameter('end', $end)
                ;
        return $qb->getQuery()
                        ->execute();
    }

}
