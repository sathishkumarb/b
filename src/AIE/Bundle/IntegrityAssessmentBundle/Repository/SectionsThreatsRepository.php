<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SectionsThreatsRepository extends EntityRepository {

    public function findByPipeline($pipelineId) {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('st')
                ->from('AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats', 'st')
                ->where("st.pipelineSections IN 
                    (
                        SELECT ps.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps WHERE ps.pipeline = :pipelineId 
                    )")->setParameter('pipelineId', $pipelineId)
                    ->orderBy('st.pipelineSections');
                ;

        return $qb->getQuery()->getResult();
    }

}
