<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class SectionsThreatsMitigationsRepository extends EntityRepository {

    public function findByPipelineId($pipelineId)
    {
        $em = $this->getEntityManager();

        /*  $qb = $this->createQueryBuilder('c');
          $qb->addSelect('p')
          ->leftJoin('c.cities', 'p')
          //               ^^^^^^^^
          ->where('p.county = :id')
          ->setParameter('id', $pipelineId)
          ;

         */
        // First get the EM handle
        // and call the query builder on it
        $qb = $em->createQueryBuilder();
        $qb->select('stm')
            ->from('AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations', 'stm')
            ->where("stm.sectionsThreats IN (
                    SELECT st.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats st WHERE st.pipelineSections IN (
                        SELECT ps.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps WHERE ps.pipeline = :pipelineId )   
                    )")->setParameter('pipelineId', $pipelineId);

        return $qb->getQuery()->getResult();


        /* return $this->getEntityManager()
          ->createQuery('SELECT ps FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps')
          ->getResult();
         * 
         */
    }

    public function findBySectionId($pipelineSection)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('stm')
            ->from('AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations', 'stm')
            ->where("stm.sectionsThreats IN (
                    SELECT st.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats st WHERE st.pipelineSections IN (
                        SELECT ps.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps WHERE ps.id = :pipelineSectionId)   
                    ORDER BY st.threats )")->setParameter('pipelineSectionId', $pipelineSection)
            ->orderBy('stm.sectionsThreats');

        return $qb->getQuery()->getResult();
    }


    public function findOverDue($month = false)
    {
        $em = $this->getEntityManager();
        /*$qb = $em->createQueryBuilder();
        $qb->select('stm')
            ->from('AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreatsMitigations', 'stm')
            ->where(" :now >= DATE_ADD( stm.LastInspectionDate , stm.frequency , 'MONTH')")->setParameter('now', new \DateTime('now'));

        if($pipelineId !== null)
            $qb->andWhere("stm.sectionsThreats IN (
                    SELECT st.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats st WHERE st.pipelineSections IN (
                        SELECT ps.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps WHERE ps.pipeline = :pipelineId )
                    )")->setParameter('pipelineId', $pipelineId);*/


        if ($month)
        {
            $sql = "SELECT * FROM `sectionsthreatsmitigations` WHERE now() >= DATE_ADD( `sectionsthreatsmitigations`.`last_inspection_date`, INTERVAL `sectionsthreatsmitigations`.`frequency` MONTH)";
        } else
        {
            $sql = "SELECT * FROM `sectionsthreatsmitigations` WHERE (now() - INTERVAL 2 DAY) = DATE_ADD( `sectionsthreatsmitigations`.`last_inspection_date`, INTERVAL `sectionsthreatsmitigations`.`frequency` MONTH)";
        }
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
