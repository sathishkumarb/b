<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ConsequenceAssessmentRepository extends EntityRepository {

    public function findByPipelineId($pipelineId) {
        $em = $this->getEntityManager();

        /*  $qb = $this->createQueryBuilder('c');
          $qb->addSelect('p')
          ->leftJoin('c.cities', 'p')
          //               ^^^^^^^^
          ->where('p.county = :id')
          ->setParameter('id', $pipelineId)
          ;

         */
        // First get the EM handle
        // and call the query builder on it
        $qb = $em->createQueryBuilder();
        $qb->select('c')
                ->from('AIE\Bundle\IntegrityAssessmentBundle\Entity\ConsequenceAssessment', 'c')
                ->where("c.sectionsThreats IN ( 
                    SELECT st.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\SectionsThreats st WHERE st.pipelineSections IN (
                        SELECT ps.id FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps WHERE ps.pipeline = :pipelineId )   
                    )")->setParameter('pipelineId', $pipelineId);

        return $qb->getQuery()->getResult();


        /* return $this->getEntityManager()
          ->createQuery('SELECT ps FROM AIE\Bundle\IntegrityAssessmentBundle\Entity\PipelineSections ps')
          ->getResult();
         * 
         */
    }

}
