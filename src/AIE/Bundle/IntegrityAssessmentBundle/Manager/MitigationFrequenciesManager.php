<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Manager;


class MitigationFrequenciesManager {

    private $entityManager;

    public function __construct($em){
        $this->entityManager = $em;
    }


    public function getDefaultMitigationFrequencies($pipeline){
        $em = $this->entityManager;

        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($pipeline->getProject()->getId());
        $riskMatrix = $project->getRiskMatrix();

        $matrix = json_decode($riskMatrix->getMatrix(), true);
        //$freq = BaseData where mitigationTechnique = mt and freq = $matrix[$consequence][$prob];
        $matrixValues = array(); //stores risk matrix value of sections threats

        //get all section threats of pipeline
        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipeline->getId());
        $sectionsThreatsConsequences = array();
        foreach ($sectionsThreats as $st)
        {
            $sectionsThreatsConsequences[$st->getId()] = array();
            $sectionThreatsconsequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findBy(array('sectionsThreats' => $st->getId()));

            $maxServerity = - 999;
            foreach ($sectionThreatsconsequences as $stc)
            {
                $sectionsThreatsConsequences[$st->getId()][$stc->getConsequence()->getId()] = $stc->getServerity();

                if ($maxServerity < $stc->getServerity())
                {
                    $maxServerity = $stc->getServerity();
                }
            }

            if(isset($matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1]))
                $matrixValues[$st->getId()] = $matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1];

        }

        $sections = $pipeline->getPse();
        $mitigationAction = array();

        $defaultFrequencies = [];
        foreach ($sections as $section)
        {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $mitigationAction[$section->getId()] = $stm;
            foreach ($stm as $s)
            {
                /* Calculate Frequency */
                if(isset($matrixValues[$s->getSectionsThreats()->getId()]))
                {
                    $value = $matrixValues[$s->getSectionsThreats()->getId()];
                    $function = "get{$value}Freq";
                    $freq = $s->getMitigationTechnique()->$function();
                    $defaultFrequencies[$s->getId()] = $freq;
                }

            }
        }


        return $defaultFrequencies;
    }

    public function updateMitigationFrequencies($pipeline){

        $em = $this->entityManager;

        $project = $em->getRepository('AIEIntegrityAssessmentBundle:Projects')->find($pipeline->getProject()->getId());
        $riskMatrix = $project->getRiskMatrix();

        $matrix = json_decode($riskMatrix->getMatrix(), true);
        //$freq = BaseData where mitigationTechnique = mt and freq = $matrix[$consequence][$prob];
        $matrixValues = array(); //stores risk matrix value of sections threats

        //get all section threats of pipeline
        $sectionsThreats = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreats')->findByPipeline($pipeline->getId());
        $sectionsThreatsConsequences = array();
        foreach ($sectionsThreats as $st)
        {
            $sectionsThreatsConsequences[$st->getId()] = array();
            $sectionThreatsconsequences = $em->getRepository('AIEIntegrityAssessmentBundle:ConsequenceAssessment')->findBy(array('sectionsThreats' => $st->getId()));

            $maxServerity = 1;
            foreach ($sectionThreatsconsequences as $stc)
            {
                $sectionsThreatsConsequences[$st->getId()][$stc->getConsequence()->getId()] = $stc->getServerity();

                if ($maxServerity < $stc->getServerity())
                {
                    $maxServerity = $stc->getServerity();
                }
            }

            if(isset($matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1]))
                $matrixValues[$st->getId()] = $matrix[$maxServerity - 1][$st->getAdjProbabilityOfFailer() - 1];
        }

        $sections = $pipeline->getPse();
        $mitigationAction = array();
        foreach ($sections as $section)
        {
            $stm = $em->getRepository('AIEIntegrityAssessmentBundle:SectionsThreatsMitigations')->findBySectionId($section->getId());
            $mitigationAction[$section->getId()] = $stm;
            foreach ($stm as $s)
            {
                /* Calculate Frequency */
                if(isset($matrixValues[$s->getSectionsThreats()->getId()]))
                {
                    $value = $matrixValues[$s->getSectionsThreats()->getId()];
                    $function = "get{$value}Freq";
                    $freq = $s->getMitigationTechnique()->$function();
                    $s->setFrequency($freq);
                    $em->persist($s);
                }
            }
        }

        $em->flush();
    }
} 