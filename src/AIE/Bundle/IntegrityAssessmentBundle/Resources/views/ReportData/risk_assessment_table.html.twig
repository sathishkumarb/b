<h2>Risk Assessment</h2>
<p>The qualitative risk model assesses the risk associated with pipelines by ranking them using a pre-defined scale.</p>
<p>The condition of the pipeline is ranked according to the consequence of its failure as an impact
    on {% for consequence in project.consequences %}{% if loop.index > 1 and loop.index < project.consequences|length %}, {% elseif loop.index != 1 %} and {% endif %}{{ consequence.name }}{% endfor %}
    (scale from 1 to 5) and according to its probability of failure (scale from 1 to 5). By applying both the ranks for
    the consequence and the probability of failure, the level of risk associated with each threat group posed by the
    condition of the pipeline is determined using the 5x5 matrix below. The risk assessment will help prioritize
    repairs, inspections, monitoring and other risk management activities.</p>


<table class="table table-bordered" id="risk-matrix-table">
    <thead>
    <tr>
        <th rowspan="4"></th>
        <th colspan="{{ project.consequences|length }}"><span>Consequence Categories</span></th>
        <th colspan="5"><span>Increasing Probability</span></th>
    </tr>
    <tr>
        {% for consequence in project.consequences %}
            <th rowspan="3"><span>{{ consequence.name }}</span></th>
        {% endfor %}
        <th><span>1</span></th>
        <th><span>2</span></th>
        <th><span>3</span></th>
        <th><span>4</span></th>
        <th><span>5</span></th>
    </tr>
    <tr>
        {% for i in 1..5 %}
            <th id="probability-{{ i }}">
                <span>{{ attribute(project.riskmatrix, 'desc_' ~ i)|nl2br }}</span>
            </th>
        {% endfor %}
    </tr>
    <tr>
        <th><span>Not likely</span></th>
        <th><span>Unlikely</span></th>
        <th><span>Possible</span></th>
        <th><span>Likely</span></th>
        <th><span>Very Likely</span></th>
    </tr>
    </thead>
    <tbody>
    {% for i in 5..1 %}
        <tr class="consequence">
            <td>{{ i }}</td>
            {% for consequence in project.consequences %}
                <td id="consequence-{{ consequence.consequenceId ~ '-' ~ i }}">
                    {{ attribute(consequence, 'desc_' ~ i ) }}
                </td>
            {% endfor %}

            {% for j in 0..4 %}
                {% set risk =  matrix[i-1][j] %}
                <td class="risk-color
                        {% if risk == 'L' %}green
                        {% elseif risk == 'M' %}yellow
                        {% elseif risk == 'H' %}orange
                        {% elseif risk == 'VL' %}lightgreen
                        {% else %}red
                        {% endif %}">
                    <span>{{ risk }}</span>
                </td>
            {% endfor %}
        </tr>
    {% endfor %}
    </tbody>
</table>

<span class="table-caption">Table 3. Risk Assessment Matrix</span>


<table class="table-bordered">
    <thead>
    <tr>
        <th>Risk</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="risk-color red">Very High</td>
        <td>Intolerable - Immediate action to be taken</td>
    </tr>
    <tr>
        <td class="risk-color orange">High</td>
        <td>Intolerable - Action to be taken</td>
    </tr>
    <tr>
        <td class="risk-color yellow">Medium</td>
        <td>Tolerable – Action to reduce the risk may be evaluated and implemented as required</td>
    </tr>
    <tr>
        <td class="risk-color green">Low</td>
        <td>Acceptable - Monitoring is required to ensure that the controls are maintained</td>
    </tr>
    <tr>
        <td class="risk-color lightgreen">Very Low</td>
        <td>Acceptable – No action required</td>
    </tr>
    </tbody>

</table>
<span class="table-caption">Table 4. Level of Risk Description</span>


<h3>Assessment of the Consequences</h3>
<p>For each section of the pipeline, the effect of failure due the various degradation mechanisms was assessed as
    follows:</p>
<ul>
    {% for consequence in project.consequences %}
        <li>Consequence on {{ consequence.name }}</li>
    {% endfor %}
</ul>

<p>The following table details the criteria for assessing each consequence for the various pipeline sections:</p>

<table id="consequences" class="table table-bordered">
    <thead>
    <tr>
        <th rowspan="2"><span>Consequence</span></th>
        <th colspan="{{ project.consequences|length }}"><span>Consequence Categories</span></th>
    </tr>
    <tr>
        {% for consequence in project.consequences %}
            <th><span>{{ consequence.name }}</span></th>
        {% endfor %}
    </tr>
    </thead>
    <tbody>
    {% for i in 1..5 %}
        <tr class="consequence-container">
            <td>{{ i }}</td>
            {% for consequence in project.consequences %}
                <td class="desc" id="{{ i }}">
                    {{ attribute(consequence, 'desc_' ~ i ) }}
                </td>
            {% endfor %}
        </tr>
    {% endfor %}

    </tbody>
</table>


<span class="table-caption">Table 5. Consequence Assessment Criteria</span>


<h3>Assessment of Likelihood of Failure</h3>
<p>The probability of failure was assessed to each section of the pipeline from five different severity levels. The
    level of severity reflects the likelihood of failure of the pipeline section being assessed due to the different
    degradation mechanisms. The description of each severity level is provided in the risk matrix above.</p>

<table id="probabilities" class="table table-bordered">
    <thead>
    <tr>
        <th colspan="5"><span>Increasing Probability</span></th>
    </tr>
    <tr>
        <th><span>1</span></th>
        <th><span>2</span></th>
        <th><span>3</span></th>
        <th><span>4</span></th>
        <th><span>5</span></th>
    </tr>
    </thead>
    <tbody>
    <tr class="probability">
        {% for i in 1..5 %}
            <td>{{ attribute(project.riskmatrix, 'desc_' ~ i)|nl2br }}</td>
        {% endfor %}
    </tr>
    <tr>
        <td>Not likely</td>
        <td>Unlikely</td>
        <td>Possible</td>
        <td>Likely</td>
        <td>Very Likely</td>
    </tr>
    </tbody>
</table>

<span class="table-caption">Table 6. Probability of Failure Assessment Criteria</span>

<h3>Confidence in the Assessment</h3>
<p>The assessment of the probability of failure is done based on the availability of the sufficient data required for
    the assessment and based on whether the data are historical or up to date. The uncertainty that can be caused due to
    those factors is used in determining the level of confidence in the assessment of the probability of failure as low,
    medium, or high confidence. For medium and high levels of confidence in the assessments, the probability remains as
    assessed. Whereas when the level of confidence in the assessment is low, the probability of failure is increased by
    one in the calculation of the risk using the risk matrix above.</p>