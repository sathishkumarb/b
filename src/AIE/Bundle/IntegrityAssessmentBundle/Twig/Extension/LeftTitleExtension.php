<?php

namespace AIE\Bundle\IntegrityAssessmentBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\Request;

class LeftTitleExtension  extends \Twig_Extension
{
    /**
     * @var Request 
     */
    protected $request;

   /**
    * @var \Twig_Environment
    */
    protected $environment;

    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFunctions()
    {
        return array(
            'get_controller_name' => new \Twig_Function_Method($this, 'getControllerName'),
            'get_action_name' => new \Twig_Function_Method($this, 'getActionName'),
            'get_left_title' => new \Twig_Function_Method($this, 'getLeftTitle'),
        );
    }

    /**
    * Get current controller name
    */
    public function getControllerName()
    {
        if(null !== $this->request)
        {
            $pattern = "#Controller\\\([a-zA-Z]*)Controller#";
            $matches = array();
            preg_match($pattern, $this->request->get('_controller'), $matches);

            return strtolower($matches[1]);
        }

    }

    /**
    * Get current action name
    */
    public function getActionName()
    {
        if(null !== $this->request)
        {
            $pattern = "#::([a-zA-Z]*)Action#";
            $matches = array();
            preg_match($pattern, $this->request->get('_controller'), $matches);

            return $matches[1];
        }
    }
    
    public function getLeftTitle(){
        if(null !== $this->request)
        {
            //return $this->request->get('_controller');
            $pattern = "#([a-zA-Z\/\\\]*)::#";
            $matches = array();
            preg_match($pattern, $this->request->get('_controller'), $matches);
            
            if(method_exists($matches[1],'getLeftTitle')){
                return $matches[1]::getLeftTitle();
            }
            
        }
    }

    public function getName()
    {
        return 'aie_left_title_twig_extension';
    }
}