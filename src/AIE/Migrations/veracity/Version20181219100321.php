<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181219100321 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cof_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, is_common TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE risk_matrix_pof (id INT AUTO_INCREMENT NOT NULL, risk_matrix_id INT NOT NULL, name VARCHAR(4) NOT NULL, identifier VARCHAR(4) NOT NULL, description VARCHAR(255) DEFAULT NULL, from_operator VARCHAR(4) DEFAULT NULL, from_year SMALLINT DEFAULT NULL, to_operator VARCHAR(4) DEFAULT NULL, to_year SMALLINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B5A49C6F77EB5E6D (risk_matrix_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE risk_matrix_category (id INT AUTO_INCREMENT NOT NULL, risk_matrix_id INT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(4) NOT NULL, color VARCHAR(7) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_17861AF77EB5E6D (risk_matrix_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE risk_matrix_cof_mapping (id INT AUTO_INCREMENT NOT NULL, risk_matrix_cof_id INT NOT NULL, cof_category_id INT NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_71B315FEACC79B4A (risk_matrix_cof_id), INDEX IDX_71B315FEC7E1FB52 (cof_category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE risk_matrix_cof (id INT AUTO_INCREMENT NOT NULL, risk_matrix_id INT NOT NULL, name VARCHAR(4) NOT NULL, identifier VARCHAR(4) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_ABC4814677EB5E6D (risk_matrix_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE risk_matrix_pof ADD CONSTRAINT FK_B5A49C6F77EB5E6D FOREIGN KEY (risk_matrix_id) REFERENCES risk_matrix (id)');
        $this->addSql('ALTER TABLE risk_matrix_category ADD CONSTRAINT FK_17861AF77EB5E6D FOREIGN KEY (risk_matrix_id) REFERENCES risk_matrix (id)');
        $this->addSql('ALTER TABLE risk_matrix_cof_mapping ADD CONSTRAINT FK_71B315FEACC79B4A FOREIGN KEY (risk_matrix_cof_id) REFERENCES risk_matrix_cof (id)');
        $this->addSql('ALTER TABLE risk_matrix_cof_mapping ADD CONSTRAINT FK_71B315FEC7E1FB52 FOREIGN KEY (cof_category_id) REFERENCES cof_category (id)');
        $this->addSql('ALTER TABLE risk_matrix_cof ADD CONSTRAINT FK_ABC4814677EB5E6D FOREIGN KEY (risk_matrix_id) REFERENCES risk_matrix (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE risk_matrix_cof_mapping DROP FOREIGN KEY FK_71B315FEC7E1FB52');
        $this->addSql('ALTER TABLE risk_matrix_cof_mapping DROP FOREIGN KEY FK_71B315FEACC79B4A');
        $this->addSql('DROP TABLE cof_category');
        $this->addSql('DROP TABLE risk_matrix_pof');
        $this->addSql('DROP TABLE risk_matrix_category');
        $this->addSql('DROP TABLE risk_matrix_cof_mapping');
        $this->addSql('DROP TABLE risk_matrix_cof');
    }
}
