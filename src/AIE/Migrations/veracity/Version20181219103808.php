<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181219103808 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE risk_matrix_mapping (id INT AUTO_INCREMENT NOT NULL, risk_matrix_id INT NOT NULL, risk_matrix_cof_id INT NOT NULL, risk_matrix_pof_id INT NOT NULL, risk_matrix_category_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_105A5B8E77EB5E6D (risk_matrix_id), INDEX IDX_105A5B8EACC79B4A (risk_matrix_cof_id), INDEX IDX_105A5B8E2985EB7F (risk_matrix_pof_id), INDEX IDX_105A5B8E965D7C84 (risk_matrix_category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE risk_matrix_mapping ADD CONSTRAINT FK_105A5B8E77EB5E6D FOREIGN KEY (risk_matrix_id) REFERENCES risk_matrix (id)');
        $this->addSql('ALTER TABLE risk_matrix_mapping ADD CONSTRAINT FK_105A5B8EACC79B4A FOREIGN KEY (risk_matrix_cof_id) REFERENCES risk_matrix_cof (id)');
        $this->addSql('ALTER TABLE risk_matrix_mapping ADD CONSTRAINT FK_105A5B8E2985EB7F FOREIGN KEY (risk_matrix_pof_id) REFERENCES risk_matrix_pof (id)');
        $this->addSql('ALTER TABLE risk_matrix_mapping ADD CONSTRAINT FK_105A5B8E965D7C84 FOREIGN KEY (risk_matrix_category_id) REFERENCES risk_matrix_category (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE risk_matrix_mapping');
    }
}
