<?php
/**
 * Created by PhpStorm.
 * User: aie
 * Date: 12/19/18
 * Time: 9:31 AM
 */

namespace AIE\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="test")
     */
    public function list()
    {
        echo 'menu';
    }
}