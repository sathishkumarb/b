<?php
/**
 * Created by PhpStorm.
 * User: aie
 * Date: 12/19/18
 * Time: 3:05 PM
 */

namespace AIE\Entity\RBI;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="rbi_inspection_level", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="project_assets_level_idx", columns={"project_id", "level_key", "asset_category_id"})
 *     }))
 */
class InspectionLevel
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\RBI\AssetCategory")
     * @ORM\JoinColumn(nullable=false)
     */
    private $assetCategory;

    /**
     * @ORM\Column(type="smallint")
     */
    private $levelKey;

    /**
     * @ORM\Column(type="string")
     */
    private $levelValue;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $remainingLifeFactor;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $maxInspectionFrequency;

    public function getId()
    {
        return $this->id;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject($project)
    {
        $this->project = $project;
    }

    public function getAssetCategory()
    {
        return $this->assetCategory;
    }

    public function setAssetCategory($assetCategory)
    {
        $this->assetCategory = $assetCategory;
    }

    public function getLevelKey()
    {
        return $this->levelKey;
    }

    public function setLevelKey($levelKey)
    {
        $this->levelKey = $levelKey;
    }

    public function getLevelValue()
    {
        return $this->levelValue;
    }

    public function setLevelValue($levelValue)
    {
        $this->levelValue = $levelValue;
    }

    public function getRemainingLifeFactor()
    {
        return $this->remainingLifeFactor;
    }

    public function setRemainingLifeFactor($remainingLifeFactor)
    {
        $this->remainingLifeFactor = $remainingLifeFactor;
    }

    public function getMaxInspectionFrequency()
    {
        return $this->maxInspectionFrequency;
    }

    public function setMaxInspectionFrequency($maxInspectionFrequency)
    {
        $this->maxInspectionFrequency = $maxInspectionFrequency;
    }
}