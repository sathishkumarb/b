<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/19/18
 * Time: 11:29 AM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix_pof", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="matrix_pof_idx", columns={"risk_matrix_id", "identifier"})
 *     })
 */
class RiskMatrixPOF
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=4)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=4)
     */
    private $identifier;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;
    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $fromOperator;
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $fromYear;
    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $toOperator;
    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $toYear;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrix;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getFromOperator()
    {
        return $this->fromOperator;
    }

    public function setFromOperator($fromOperator)
    {
        $this->fromOperator = $fromOperator;
    }

    public function getFromYear()
    {
        return $this->fromYear;
    }

    public function setFromYear($fromYear)
    {
        $this->fromYear = $fromYear;
    }

    public function getToOperator()
    {
        return $this->toOperator;
    }

    public function setToOperator($toOperator)
    {
        $this->toOperator = $toOperator;
    }

    public function getToYear()
    {
        return $this->toYear;
    }

    public function setToYear($toYear)
    {
        $this->toYear = $toYear;
    }

    public function getRiskMatrix()
    {
        return $this->riskMatrix;
    }

    public function setRiskMatrix(RiskMatrix $riskMatrix)
    {
        $this->riskMatrix = $riskMatrix;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }
}