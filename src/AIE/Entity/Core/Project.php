<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/9/18
 * Time: 3:45 PM
 */

namespace AIE\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Guzzle\Common\Collection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="project")
 */
class Project
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $description;
    /**
     * @ORM\Column(type="string")
     */
    private $client;
    /**
     * @ORM\Column(type="string")
     */
    private $location;
    /**
     * @ORM\Column(type="string")
     */
    private $referenceNo;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $folder;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $filepath;

    /**
     * @ORM\OneToMany(targetEntity="SubProject", mappedBy="project")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $subproject;

    public function __construct()
    {
        $this->subproject = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient($client)
    {
        $this->client = $client;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getFolder()
    {
        return $this->folder;
    }

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getFilepath()
    {
        return $this->filepath;
    }

    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    public function getReferenceNo()
    {
        return $this->referenceNo;
    }

    public function setReferenceNo($referenceNo)
    {
        $this->referenceNo = $referenceNo;
    }

    /**
     * @return Collection|SubProject[]
     */
    public function getSubproject()
    {
        return $this->subproject;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }
}