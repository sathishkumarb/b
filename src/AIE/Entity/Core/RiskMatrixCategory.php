<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/19/18
 * Time: 1:49 PM
 */

namespace AIE\Entity\Core;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix_category", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="matrix_color_idx", columns={"risk_matrix_id", "color"})
 *     })
 */
class RiskMatrixCategory
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=4)
     */
    private $shortName;
    /**
     * @ORM\Column(type="string", length=7)
     */
    private $color;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrix;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getShortName()
    {
        return $this->shortName;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getRiskMatrix()
    {
        return $this->riskMatrix;
    }

    public function setRiskMatrix(RiskMatrix $riskMatrix)
    {
        $this->riskMatrix = $riskMatrix;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

}