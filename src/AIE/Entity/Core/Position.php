<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/12/18
 * Time: 12:22 PM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="position")
 */
class Position
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    /**
     * @ORM\ManyToOne(targetEntity="Permission")
     * @ORM\JoinColumn(nullable=false)
     */
    private $permission;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function getPermission()
    {
        return $this->permission;
    }

    public function setPermission(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }
}