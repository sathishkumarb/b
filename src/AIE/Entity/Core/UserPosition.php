<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/12/18
 * Time: 12:30 PM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="user_position")
 */
class UserPosition
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="SubProject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subproject;

    /**
     * @ORM\ManyToOne(targetEntity="Position")
     * @ORM\JoinColumn(nullable=false)
     */
    private $position;

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getProject()
    {
        return $this->project;
    }

     public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getSubproject()
    {
        return $this->subproject;
    }

    public function setSubproject(SubProject $subproject)
    {
        $this->subproject = $subproject;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

}