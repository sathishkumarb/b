<?php
/**
 * Created by PhpStorm.
 * User: aie
 * Date: 12/19/18
 * Time: 2:13 PM
 */

namespace AIE\Entity\Core;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix_mapping", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="matrix_cof_pof_idx",
 *                      columns={"risk_matrix_id", "risk_matrix_cof_id", "risk_matrix_pof_id" }
 *     )})
 */
class RiskMatrixMapping
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrix;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrixCOF")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrixCOF;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrixPOF")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrixPOF;

    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrixCategory")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrixCategory;


    public function getId()
    {
        return $this->id;
    }

    public function getRiskMatrix()
    {
        return $this->riskMatrix;
    }

    public function setRiskMatrix(RiskMatrix $riskMatrix)
    {
        $this->riskMatrix = $riskMatrix;
    }

    public function getRiskMatrixCOF()
    {
        return $this->riskMatrixCOF;
    }

    public function setRiskMatrixCOF(RiskMatrixCOF $riskMatrixCOF)
    {
        $this->riskMatrixCOF = $riskMatrixCOF;
    }

    public function getRiskMatrixPOF()
    {
        return $this->riskMatrixPOF;
    }

    public function setRiskMatrixPOF(RiskMatrixPOF $riskMatrixPOF)
    {
        $this->riskMatrixPOF = $riskMatrixPOF;
    }

    public function getRiskMatrixCategory()
    {
        return $this->riskMatrixCategory;
    }

    public function setRiskMatrixCategory(RiskMatrixCategory $riskMatrixCategory)
    {
        $this->riskMatrixCategory = $riskMatrixCategory;
    }
}