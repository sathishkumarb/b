<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/19/18
 * Time: 12:08 PM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix_cof_mapping", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="matrixcof_category_idx", columns={"risk_matrix_cof_id", "cof_category_id"})
 *    })
 */
class RiskMatrixCOFMapping
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrixCOF")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrixCof;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\COFCategory")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cofCategory;

    public function getId()
    {
        return $this->id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getRiskMatrixCof()
    {
        return $this->riskMatrixCof;
    }

    public function setRiskMatrixCof(RiskMatrixCOF $riskMatrixCof)
    {
        $this->riskMatrixCof = $riskMatrixCof;
    }

    public function getCofCategory()
    {
        return $this->cofCategory;
    }

    public function setCofCategory(COFCategory $cofCategory)
    {
        $this->cofCategory = $cofCategory;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

}