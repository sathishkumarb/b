<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/19/18
 * Time: 10:21 AM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix")
 */
class RiskMatrix
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $pofLevel;
    /**
     * @ORM\Column(type="string")
     */
    private $cofLevel;

    /**
     * @ORM\ManyToOne(targetEntity="project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="module")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPofLevel()
    {
        return $this->pofLevel;
    }

    public function setPofLevel($pofLevel)
    {
        $this->pofLevel = $pofLevel;
    }

    public function getCofLevel()
    {
        return $this->cofLevel;
    }

    public function setCofLevel($cofLevel)
    {
        $this->cofLevel = $cofLevel;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function setModule(Module $module)
    {
        $this->module = $module;
    }
    public function __toString()
    {
        return (string) $this->getName();
    }
}