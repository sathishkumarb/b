<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/11/18
 * Time: 3:49 PM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * @ORM\Entity()
 * @ORM\Table(name="sub_project")
 */
class SubProject
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $location;
    /**
     * @ORM\Column(type="string")
     */
    private $referenceNo;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $folder;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $filepath;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reportingPeriod;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="subproject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getReferenceNo()
    {
        return $this->referenceNo;
    }

    public function setReferenceNo($referenceNo)
    {
        $this->referenceNo = $referenceNo;
    }

    public function getFolder()
    {
        return $this->folder;
    }

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getFilepath()
    {
        return $this->filepath;
    }

    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    public function getReportingPeriod()
    {
        return $this->reportingPeriod;
    }

    public function setReportingPeriod($reportingPeriod)
    {
        $this->reportingPeriod = $reportingPeriod;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }
}