<?php
/**
 * Created by AIE.
 * User: aie
 * Date: 12/19/18
 * Time: 12:01 PM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="risk_matrix_cof", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="matrix_cof_idx", columns={"risk_matrix_id", "identifier"})
 *     })
 */
class RiskMatrixCOF
{
    use TimestampableEntity;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=4)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=4)
     */
    private $identifier;
    /**
     * @ORM\ManyToOne(targetEntity="AIE\Entity\Core\RiskMatrix")
     * @ORM\JoinColumn(nullable=false)
     */
    private $riskMatrix;


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    public function getRiskmatrix()
    {
        return $this->riskMatrix;
    }

    public function setRiskmatrix(RiskMatirx $riskMatrix)
    {
        $this->riskMatrix = $riskMatrix;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

}