<?php
/**
 * Created by PhpStorm.
 * User: aie
 * Date: 12/12/18
 * Time: 11:54 AM
 */

namespace AIE\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="permission", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="project_module_idx", columns={"project_id", "module_id"})})
 * @UniqueEntity(
 *     fields={"project", "module"},
 *     message="Permission for given module for the project exists in database"
 * )
 */
class Permission
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;
    /**
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;

    public function getId()
    {
        return $this->id;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }
}