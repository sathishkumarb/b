<?php
/**
 * Created by PhpStorm.
 * User: aie
 * Date: 12/19/18
 * Time: 12:20 PM
 */

namespace AIE\Entity\Core;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="cof_category")
 */
class COFCategory
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isCommon = false;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getIsCommon()
    {
        return $this->isCommon;
    }

    public function setIsCommon($isCommon)
    {
        $this->isCommon = $isCommon;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }
}