<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel {

    public function __construct($environment, $debug) {
        date_default_timezone_set( 'UTC' );
        parent::__construct($environment, $debug);
    }
    
    public function registerBundles() {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            /* FOSUserBundle */
            new FOS\UserBundle\FOSUserBundle(),
            /* FOSJS*/
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            /* MenuBundle */
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            /* PaginationBundle */
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            /* HTML TO PDF/JPG */
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            
            /* HTML DOM Parser */
            new Erivello\SimpleHtmlDomBundle\ErivelloSimpleHtmlDomBundle(),
            
            /* AWS */
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),

            /* Serializer of objects */
            new JMS\SerializerBundle\JMSSerializerBundle(),

            /* Sorting Form Fields */
            new Ivory\OrderedFormBundle\IvoryOrderedFormBundle(),

	        /* For Extra Security */
	        new Nelmio\SecurityBundle\NelmioSecurityBundle(),

            new Liuggio\ExcelBundle\LiuggioExcelBundle(),
            
            /* AIE */
            new AIE\Bundle\UserBundle\UserBundle(),
            new AIE\Bundle\StorageBundle\StorageBundle(),
            new AIE\Bundle\IntegrityAssessmentBundle\AIEIntegrityAssessmentBundle(),
            new AIE\Bundle\ReportingBundle\ReportingBundle(),
            new AIE\Bundle\AnomalyBundle\AIEAnomalyBundle(),
            new AIE\Bundle\CmsBundle\AIECmsBundle(),
            new AIE\Bundle\VeracityBundle\AIEVeracityBundle(),

            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle(),

            new AIE\Bundle\AdminBundle\AIEAdminBundle(),

            new AIE\Bundle\RBIBundle\AIERBIBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader) {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
