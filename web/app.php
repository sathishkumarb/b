<?php

use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../app/bootstrap.php.cache';

// Enable APC for autoloading to improve performance.
// You should change the ApcClassLoader first argument to a unique prefix
// in order to prevent cache key conflicts with other applications
// also using APC.
/*
$apcLoader = new Symfony\Component\ClassLoader\ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();

/* Because we are using Amazon Elastic Beanstalk
And amazon doesn't use the same proxies protocols to define the connection as symfony
we must add it manually, which took a lot alot of time to figure out
http://symfony.com/doc/current/cookbook/request/load_balancer_reverse_proxy.html
*/
Request::setTrustedProxies([$request->server->get('REMOTE_ADDR')]);


$response = $kernel->handle($request);
//if ($request->getScheme() === 'http') {
  //  $urlRedirect = str_replace($request->getScheme(), 'https', $request->getUri());
    //$response = new RedirectResponse($urlRedirect);
//} else {
  //  $response = $kernel->handle($request);
//}
$response->send();
$kernel->terminate($request, $response);
