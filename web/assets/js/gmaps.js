var colorIndex = 0;
function getRandomColor() {

    var pipelineColors = [
        'FF0000',
        '000066',
        '00CC00',
        'FFFF66', //'#CC0066',
        '663300',
        'FFCC00',
        '990099',
    ];
    if (colorIndex < pipelineColors.length) {
        color = pipelineColors[colorIndex];
        colorIndex++;
    } else {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
    }
    return color;
}

google.maps.Polygon.prototype.Contains = function (point) {
    // ray casting alogrithm http://rosettacode.org/wiki/Ray-casting_algorithm
    var crossings = 0,
        path = this.getPath();

    // for each edge
    for (var i = 0; i < path.getLength(); i++) {
        var a = path.getAt(i),
            j = i + 1;
        if (j >= path.getLength()) {
            j = 0;
        }
        var b = path.getAt(j);
        if (rayCrossesSegment(point, a, b)) {
            crossings++;
        }
    }

    // odd number of crossings?
    return (crossings % 2 == 1);

    function rayCrossesSegment(point, a, b) {
        var px = point.lng(),
            py = point.lat(),
            ax = a.lng(),
            ay = a.lat(),
            bx = b.lng(),
            by = b.lat();
        if (ay > by) {
            ax = b.lng();
            ay = b.lat();
            bx = a.lng();
            by = a.lat();
        }
        if (py == ay || py == by)
            py += 0.00000001;
        if ((py > by || py < ay) || (px > Math.max(ax, bx)))
            return false;
        if (px < Math.min(ax, bx))
            return true;

        var red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
        var blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
        return (blue >= red);
    }
};

//https://jsfiddle.net/doktormolle/EdZk4/show/
var IO = {
//returns array with storable google.maps.Overlay-definitions
    IN: function (arr, //array with google.maps.Overlays
                  encoded//boolean indicating if pathes should be stored encoded
        ) {
        var shapes = [],
            goo = google.maps,
            shape, tmp, geometry;
        for (var i = 0; i < arr.length; i++) {
            shape = arr[i];
            tmp = {
                type: this.t_(shape.type),
                id: shape.id || null
            };
            switch (tmp.type) {
                case 'CIRCLE':
                    tmp.radius = shape.getRadius();
                    tmp.geometry = this.p_(shape.getCenter());
                    break;
                case 'MARKER':
                    tmp.geometry = this.p_(shape.getPosition());
                    break;
                case 'RECTANGLE':
                    tmp.geometry = this.b_(shape.getBounds());
                    break;
                case 'POLYLINE':
                    tmp.geometry = this.l_(shape.getPath(), encoded);
                    break;
                case 'POLYGON':
                    tmp.geometry = this.m_(shape.getPaths(), encoded);
                    break;
            }

            shapes.push(tmp);
        }

        return shapes;
    },
    //returns array with google.maps.Overlays
    OUT: function (geo, //array containg the stored shape-definitions
                   map//map where to draw the shapes
        ) {




        var shapes = [],
            goo = google.maps,
            map = map || null, shape,
            tmp;

        var arr = [];

        try {
            arr = JSON.parse(geo);
            for(var i = 0; i < arr.length; i++) {
                shape = arr[i];
                switch(shape.type){
                    case 'CIRCLE':
                        tmp=new goo.Circle({radius:Number(shape.radius),center:this.pp_.apply(this,shape.geometry)});
                        break;
                    case 'MARKER':
                        tmp=new goo.Marker({position:this.pp_.apply(this,shape.geometry)});
                        break;
                    case 'RECTANGLE':
                        tmp=new goo.Rectangle({bounds:this.bb_.apply(this,shape.geometry)});
                        break;
                    case 'POLYLINE':
                        tmp=new goo.Polyline({path:this.ll_(shape.geometry)});
                        break;
                    case 'POLYGON':
                        tmp=new goo.Polygon({paths:this.mm_(shape.geometry)});

                        break;
                }
                tmp.setValues({map:map,id:shape.id})

                shapes.push(tmp);
            }
        } catch (e) {
            //for pipeline as it only accepts polylines
            tmp = new goo.Polyline({
                path: this.ll_(geo)
            });
            tmp.setValues({
                map: map
            });
            shapes.push(tmp);
        }


        return shapes;
    },
    l_: function (path, e) {
        path = (path.getArray) ? path.getArray() : path;
        if (e) {
            return google.maps.geometry.encoding.encodePath(path);
        } else {
            var r = [];
            for (var i = 0; i < path.length; ++i) {
                r.push(this.p_(path[i]));
            }
            return r;
        }
    },
    ll_: function (path) {
        if (typeof path === 'string') {
            return google.maps.geometry.encoding.decodePath(path);
        }
        else {
            var r = [];
            for (var i = 0; i < path.length; ++i) {
                r.push(this.pp_.apply(this, path[i]));
            }
            return r;
        }
    },
    m_: function (paths, e) {
        var r = [];
        paths = (paths.getArray) ? paths.getArray() : paths;
        for (var i = 0; i < paths.length; ++i) {
            r.push(this.l_(paths[i], e));
        }
        return r;
    },
    mm_: function (paths) {
        var r = [];
        for (var i = 0; i < paths.length; ++i) {
            r.push(this.ll_.call(this, paths[i]));
        }
        return r;
    },
    p_: function (latLng) {
        return([latLng.lat(), latLng.lng()]);
    },
    pp_: function (lat, lng) {
        return new google.maps.LatLng(lat, lng);
    },
    b_: function (bounds) {
        return([this.p_(bounds.getSouthWest()),
            this.p_(bounds.getNorthEast())]);
    },
    bb_: function (sw, ne) {
        return new google.maps.LatLngBounds(this.pp_.apply(this, sw),
            this.pp_.apply(this, ne));
    },
    t_: function (s) {
        var t = ['CIRCLE', 'MARKER', 'RECTANGLE', 'POLYLINE', 'POLYGON'];
        for (var i = 0; i < t.length; ++i) {
            if (s === google.maps.drawing.OverlayType[t[i]]) {
                return t[i];
            }
        }
    },
    getZoomByBounds: function (map, bounds) {
        var MAX_ZOOM = map.mapTypes.get(map.getMapTypeId()).maxZoom || 21;
        var MIN_ZOOM = map.mapTypes.get(map.getMapTypeId()).minZoom || 0;
        var ne = map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
        var sw = map.getProjection().fromLatLngToPoint(bounds.getSouthWest());
        var worldCoordWidth = Math.abs(ne.x - sw.x);
        var worldCoordHeight = Math.abs(ne.y - sw.y);
        //Fit padding in pixels 
        var FIT_PAD = 40;
        for (var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom) {
            if (worldCoordWidth * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).width() &&
                worldCoordHeight * (1 << zoom) + 2 * FIT_PAD < $(map.getDiv()).height())
                return zoom;
        }
        return 0;
    }
};

function initializeIn() {
    var goo = google.maps,
        map_in = new goo.Map(document.getElementById('map_in'),
            {
                zoom: 12,
                center: new goo.LatLng(32.344, 51.048),
                mapTypeId: goo.MapTypeId.SATELLITE
            }),
    /*map_out = new goo.Map(document.getElementById('map_out'),
     {zoom: 12,
     center: new goo.LatLng(32.344, 51.048)
     }),*/
        shapes = [],
        selected_shape = null,
        drawman = new goo.drawing.DrawingManager({
            map: map_in,
            drawingControlOptions: {
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYLINE
                ]
            }
        }),
        byId = function (s) {
            return document.getElementById(s)
        },
        clearSelection = function () {
            if (selected_shape) {
                selected_shape.set((selected_shape.type
                    ===
                    google.maps.drawing.OverlayType.MARKER
                    ) ? 'draggable' : 'editable', false);
                selected_shape = null;
            }
        },
        setSelection = function (shape) {
            clearSelection();
            selected_shape = shape;
            selected_shape.set((selected_shape.type
                ===
                google.maps.drawing.OverlayType.MARKER
                ) ? 'draggable' : 'editable', true);
        },
        clearShapes = function () {
            for (var i = 0; i < shapes.length; ++i) {
                shapes[i].setMap(null);
            }
            shapes = [];
        };
    fitShapeBounds = function () {
        if (shapes) {
            var latlngbounds = new google.maps.LatLngBounds();
            for (var i = 0; i < shapes.length; i++) {
                shapes[i].getPath().forEach(function (e) {
                    latlngbounds.extend(e);
                });
            }
            map_in.fitBounds(latlngbounds);
        }
    };
    //map_in.bindTo('center', map_out, 'center');
    //map_in.bindTo('zoom', map_out, 'zoom');

    goo.event.addListener(drawman, 'overlaycomplete', function (e) {
        var shape = e.overlay;
        shape.type = e.type;
        goo.event.addListener(shape, 'click', function () {
            setSelection(this);
        });
        setSelection(shape);
        shapes.push(shape);
    });
    goo.event.addListener(map_in, 'click', clearSelection);
    goo.event.addDomListener(byId('clear_shape'), 'click', function () {
        clearShapes();
        drawman.setOptions({
            drawingControl: true
        });
        byId('geometryMap').value = "";
    });
    goo.event.addDomListener(byId('center_shape'), 'click', function () {
        fitShapeBounds();
    });
    goo.event.addDomListener(byId('draw_shape'), 'click', function () {
        clearShapes();
        drawman.setOptions({
            drawingControl: false
        });
        var latlngs = $('#lat_lng').val().split('\n');
        var lineArr = new Array();
        var ll = new Array();
        $.each(latlngs, function () {
            var sLatLng = this;
            var LatLng = sLatLng.split(',');
            var newLocation = new google.maps.LatLng(LatLng[0], LatLng[1]);
            ll.push(newLocation);
            lineArr.push(LatLng);
        });
        //var geometry = JSON.stringify( lineArr ) ;
        //this.shapes = IO.OUT(JSON.parse(byId('data').value), map_out);
        //http://salman-w.blogspot.ae/2011/03/zoom-to-fit-all-markers-on-google-map.html
        shapes = IO.OUT(lineArr, map_in);
        fitShapeBounds();
        //[[32.32601629134226,50.98651885986328],[32.349512498847915,51.020851135253906],[32.3271767410611,51.026344299316406],[32.34922245940497,51.097412109375]]
        byId('geometryMap').value = google.maps.geometry.encoding.encodePath(ll);
    });
    /*goo.event.addDomListener(byId('save_encoded'), 'click', function() {
     var data = IO.IN(shapes, true);
     var geo = data[0].geometry;
     byId('data').value = geo;
     byId('geometryMap').value = geo;
     });
     goo.event.addDomListener(byId('save_raw'), 'click', function() {
     var data = IO.IN(shapes, false);

     var geo = data[0].geometry;
     byId('data').value = JSON.stringify(geo);
     byId('geometryMap').value = JSON.stringify(geo);
     });
     goo.event.addDomListener(byId('restore'), 'click', function() {
     if (this.shapes) {
     for (var i = 0; i < this.shapes.length; ++i) {
     this.shapes[i].setMap(null);
     }
     }
     //this.shapes = IO.OUT(JSON.parse(byId('data').value), map_out);
     });*/

    goo.event.addListener(drawman, 'overlaycomplete', function (e) {
        if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawman.setDrawingMode(null);
            // To hide:
            if($('body').hasClass('pipeline')) {
                drawman.setOptions({
                    drawingControl: false
                });
            }
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            google.maps.event.addListener(newShape, 'click', function () {
                setSelection(newShape);
            });
            setSelection(newShape);
            var data = IO.IN(shapes, true);

            var geo = ($('body').hasClass('pipeline'))? data[0].geometry: JSON.stringify(data);
            byId('geometryMap').value = geo;
        }
    });
    if (byId('geometryMap').value.length > 0) {
        shapes = IO.OUT((byId('geometryMap').value), map_in);
        drawman.setOptions({
            drawingControl: false
        });
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            //console.log(lat + " " + lng);
            map_in.setCenter(new google.maps.LatLng(lat, lng), 13);
        });
    }
}

function initializeOut(map_element) {
    var map_settings = {
        zoom: 24,
        mapTypeId: google.maps.MapTypeId.SATELLITE
        //draggable: false,
        //zoomControl: false,
        //scrollwheel: false,
        //disableDoubleClickZoom: true
    };

    if (map_element.hasClass('hide_panels')) {
        $.extend(map_settings, {
            draggable: false,
            zoomControl: false,
            scrollwheel: false,
            scaleControl: false,
            panControl: false,
            streetViewControl: false,
            disableDoubleClickZoom: true,
            mapTypeControl: false
        });
    }

    var goo = google.maps,
        map_out = new goo.Map(map_element[0], map_settings),
        shapes = [],
        clearShapes = function () {
            for (var i = 0; i < shapes.length; ++i) {
                shapes[i].setMap(null);
            }
        },
        lineLength = function (line) {

            //get length in KM
            if (line) {
                var length, radius = 6371;
                var g = goo.geometry.spherical;
                var pathref = line.getPath().getArray();
                length = g.computeLength(pathref, radius);
                return length;
            }
            return false;
        },
        polygon_path_from_bounds = function (bounds) {
            var paths = new google.maps.MVCArray();
            var path = new google.maps.MVCArray();
            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();
            path.push(ne);
            path.push(new google.maps.LatLng(sw.lat(), ne.lng()));
            path.push(sw);
            path.push(new google.maps.LatLng(ne.lat(), sw.lng()));
            paths.push(path);
            return paths;
        },
        polyContains = function (poly, point) {
            return (poly.Contains(point) || goo.geometry.poly.containsLocation(point, poly)
                || goo.geometry.poly.isLocationOnEdge(point, poly));
        },
        getSectionCoordinates = function (line, x, y, length, pipeline_length, goo) {

            var from_point = null;
            if (x == 0) {
                from_point = line.GetPointAtDistance(0);
            } else {
                var from_distance = (x * length) / pipeline_length; //in Km
                from_point = line.GetPointAtDistance(from_distance * 1000);
            }

            var to_distance = ((y * length) / pipeline_length) * 1000; //in Meters
            var to_point = line.GetPointAtDistance(to_distance);

            var from_setted = false;
            var linePath = line.getPath();
            var lineLength = linePath.length;
            var sectionCoordinates = [];
            var sub_path = [linePath.getAt(0)];
            for (var k = 1; k < lineLength; k++) {
                var point = (!from_setted) ? from_point : to_point;
                sub_path.push(linePath.getAt(k));

                var sub_line = new goo.Polyline({
                    path: sub_path
                });

                var sub_bounds = new google.maps.LatLngBounds();
                sub_line.getPath().forEach(function (e) {
                    sub_bounds.extend(e);
                });

                subPolygon = new google.maps.Polygon({
                    paths: polygon_path_from_bounds(sub_bounds),
                    /*geodesic: true,
                     strokeColor: 'red',
                     strokeOpacity: 0.6,
                     map: map_out*/
                });


                if (polyContains(

                    subPolygon, point)) {

                    if (!from_setted) {
                        from_setted = true;
                        sectionCoordinates.push(point);
                        sectionCoordinates.push(linePath.getAt(k));
                        sub_line =
                            sectionCoordinates;
                    } else {
                        sectionCoordinates.pop();
                        sectionCoordinates.push(point);
                        break;
                    }
                } else {
                    if (from_setted) {
                        sectionCoordinates.push(

                            linePath.getAt(k));
                    }
                }

            }
            return sectionCoordinates;
        }
        ,
        byId = function (s) {
            return document.getElementById(s)
        };
    //http://salman-w.blogspot.ae/2011/03/zoom-to-fit-all-markers-on-google-map.html
    var data = byId('geometryMap').value;
    shapes = IO.OUT((data), map_out);


    var latlngbounds = new google.maps.LatLngBounds();
    for (var i = 0; i < shapes.length; i++) {
        shapes[i].getPath().forEach(function (e) {
            latlngbounds.extend(e);
        });
    }


    var infowindow = new google.maps.InfoWindow({
        // map: map_out
    });

    var showRisks = map_element.hasClass('risks');

    if (map_element.hasClass('section')) {
        for (var i = 0; i < shapes.length; i++) {
            var line = shapes[i];
            var length = lineLength(line);
            /*console.log("entered lng = " + pipeline.length);
             console.log("Length = " + length);
             console.log("Sections = " + pipeline.sections.length);*/

            var stroke = pipeline.sections.length * 2;
            for (var j = 0; j < pipeline.sections.length; j++) {
                var section = pipeline.sections[j];

                /*var from_marker = new google.maps.Marker({
                 position: from_point,
                 map: map_out,
                 title: 'To Point!'
                 });

                 var to_marker = new google.maps.Marker({
                 position: to_point,
                 map: map_out,
                 title: 'From Point!'
                 });*/

                /*console.log("section.name:" + section.name);
                 console.log("section.x:" + section.x);
                 console.log("section.y:" + section.y);
                 console.log("length:" + length);
                 console.log("pipeline.length:" + pipeline.length);
                 console.log("to_distance: " + to_distance);
                 console.log("to_point:" + to_point);
                 console.log("----------------------------");
                 */


                //bermudaTriangle.setPaths(polygon_path_from_bounds(latlngbounds));
                //bermudaTriangle.setMap(map_out);

                /*
                 line.GetPointsAtDistance(from_distance * 1000).forEach(function(point){
                 if(goo.geometry.poly.containsLocation(point, line)){
                 from_point = point;
                 console.log("contained from ");
                 return false;
                 }
                 });

                 line.GetPointsAtDistance(to_distance * 1000).forEach(function(point){
                 if(goo.geometry.poly.containsLocation(point, line)){
                 to_point = point;
                 console.log("contained to ");
                 return false;
                 }
                 });

                 console.log("FROM in Line " + lineContains(line, from_point));
                 console.log("TO in Line " + lineContains(line, to_point));

                 console.log("Section: " + section.name + " Risk: " + section.risk);

                 */

                var sectionCoordinates = getSectionCoordinates(line, section.x, section.y, length, pipeline.length, goo);
                var color = '000000';

                if (showRisks) {
                    if (section.risk == 1) {
                        //Green
                        color = '008000';
                    } else if (section.risk == 2) {
                        //Yellow
                        color = 'FFFF00';
                    } else if (section.risk == 3) {
                        //Orange
                        color = 'FFA500';
                    } else if (section.risk == 4) {
                        //Red
                        color = 'FF0000';
                    }
                } else {
                    color = getRandomColor();
                }


                /*var sectionPath = new google.maps.Polyline({
                 path: sectionCoordinates,
                 geodesic: true,
                 section: section,
                 strokeColor: color,
                 strokeOpacity: 0.6,
                 strokeWeight: stroke,
                 map: map_out
                 });*/

                stroke -= 2;

                //add markers
                for (var m = 0; m < 2; m++) {

                    var start = (m == 0);

                    var position = (start) ? sectionCoordinates[0] : sectionCoordinates[sectionCoordinates.length - 1];

                    var iconUrl = (start) ?
                        'https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|+|' + color + '|000000' :
                        'https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_sright|+|' + color + '|000000';
                    var iconPoint = (start) ?
                        new google.maps.Point(23, 33) :
                        new google.maps.Point(0, 33);

                    var marker = new google.maps.Marker({
                        position: position,
                        map: map_out,
                        section: section,
                        icon: new google.maps.MarkerImage(
                            iconUrl,
                            null, null, iconPoint),
                    });

                    google.maps.event.addListener(marker, 'click', function (event) {

                        var toolTip = '<div id="map-box">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h1 id="firstHeading" class="firstHeading">' + this.section.name + '</h1>';

                        if (showRisks) {
                            toolTip += '<ul>';
                            var risks = this.section.risks;
                            for (var l = 0; l < risks.length; l++) {
                                toolTip += '<li>' + risks[l] + '</li>';
                            }
                            toolTip += '</ul>';
                        }

                        toolTip += '</div>';

                        infowindow.setContent(toolTip);
                        infowindow.setPosition(this.getPosition());
                        infowindow.open(map_out);
                    });
                }
            }
        }
    } else {
        if (map_element.hasClass('live_sectioning')) {
            var color = 'red';
            var line = shapes[0];
            var length = lineLength(line);

            var markerStart, markerEnd;
            markerStart = new google.maps.Marker({
                map: map_out,
                icon: new google.maps.MarkerImage(
                    'https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|+|FF0000|000000'
                    , null, null, new google.maps.Point(23, 33)
                ),
            });

            markerEnd = new goo.Marker({
                map: map_out,
                icon: new goo.MarkerImage(
                    'https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_sright|+|FF0000|000000'
                    , null, null, new google.maps.Point(0, 33)
                ),
            });

            var updating = false;
            $('#sectionX').on('change', function () {
                if (!updating) {
                    updating = true;
                    setTimeout(function () {
                        var x = parseFloat($('#sectionX').val()).toFixed(2);
                        var y = parseFloat($('#sectionY').val()).toFixed(2);

                        var sectionCoordinates = getSectionCoordinates(line, x, y, length, pipeline.length, goo);

                        var from_point = sectionCoordinates[0];
                        var to_point = sectionCoordinates[sectionCoordinates.length - 1];

                        markerStart.setPosition(from_point);
                        markerEnd.setPosition(to_point);

                        updating = false;
                        console.log('done');
                    }, 1500);
                }
            });
        }
    }

    map_out.fitBounds(latlngbounds);
    goo.LatLng.prototype.kmTo = function (a) {
        var e = Math, ra = e.PI / 180;
        var b = this.lat() * ra, c = a.lat() * ra, d = b - c;
        var g = this.lng() * ra - a.lng() * ra;
        var f = 2 * e.asin(e.sqrt(e.pow(e.sin(d / 2), 2) + e.cos(b) * e.cos
            (c) * e.pow(e.sin(g / 2), 2)));
        return f * 6378.137;
    };
    goo.Polyline.prototype.inKm = function (n) {
        var a = this.getPath(n), len = a.getLength(), dist = 0;
        for (var i = 0; i < len - 1; i++) {
            dist += a.getAt(i).kmTo(a.getAt(i + 1));
        }
        return dist;
    };
}

$(document).ready(function () {
    if ($('.map').length > 0) {

        $('.map').each(function () {
            var map = $(this);
            if (map.hasClass('map_out')) {
                google.maps.event.addDomListener(window, 'load', function () {
                    initializeOut(map);
                });
            }
            if (map.is('#map_in')) {
                google.maps.event.addDomListener(window, 'load', initializeIn);
            }
        });
    }
});