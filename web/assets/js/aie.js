/*


 //Google Charts
 google.load("visualization", "1", {packages: ["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
 var data = google.visualization.arrayToDataTable([
 ['Year', 'Sales', 'Expenses'],
 ['2004', 1000, 400],
 ['2005', 1170, 460],
 ['2006', 660, 1120],
 ['2007', 1030, 540]
 ]);

 var options = {
 title: 'Company Performance'
 };

 var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
 chart.draw(data, options);
 }


 function drawVisualization() {
 // Some raw data (not necessarily accurate)
 var data = google.visualization.arrayToDataTable([
 ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
 ['2004/05', 165, 938, 522, 998, 450, 614.6],
 ['2005/06', 135, 1120, 599, 1268, 288, 682],
 ['2006/07', 157, 1167, 587, 807, 397, 623],
 ['2007/08', 139, 1110, 615, 968, 215, 609.4],
 ['2008/09', 136, 691, 629, 1026, 366, 569.6]
 ]);

 var options = {
 title: 'Monthly Coffee Production by Country',
 vAxis: {title: "Cups"},
 hAxis: {title: "Month"},
 seriesType: "bars",
 series: {5: {type: "line"}}
 };

 var chart = new google.visualization.ComboChart(document.getElementById('chart_div_2'));
 chart.draw(data, options);
 }
 google.setOnLoadCallback(drawVisualization);


 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart2);
 function drawChart2() {
 var data = google.visualization.arrayToDataTable([
 ['Year', 'Sales', 'Expenses'],
 ['2004',  1000,      400],
 ['2005',  1170,      460],
 ['2006',  660,       1120],
 ['2007',  1030,      540]
 ]);

 var options = {
 title: 'Company Performance',
 hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
 };

 var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_3'));
 chart.draw(data, options);
 }


 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChartD);
 function drawChartD() {
 var data = google.visualization.arrayToDataTable([
 ['Task', 'Hours per Day'],
 ['Work',     11],
 ['Eat',      2],
 ['Commute',  2],
 ['Watch TV', 2],
 ['Sleep',    7]
 ]);

 var options = {
 title: 'My Daily Activities',
 pieHole: 0.4,
 };

 var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
 chart.draw(data, options);
 }

 */
$(function () {
    $('[data-toggle="tooltip"]').tooltip({html: true})
})

$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 40;  // How many characters are shown by default
    var ellipsestext = "";
    var moretext = "more..";
    var lesstext = "less..";


    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '</span><span class="morecontent"><span>' + h + '</span>&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});

window.onscroll = function() {FixSecondTopBar()};
var sticky = $("#seconderyNavbar").offsetTop;
function FixSecondTopBar() {
    $(window).scroll(function(){
        var scrollPos = $(document).scrollTop();
        if (scrollPos >= 50) {
            $("#seconderyNavbar").addClass("sticky-navbar")
            if ($("#seconderyNavbar").length > 0){
                $("#contentHolder").css("margin-top","50px");
            }

        } else {
            $("#seconderyNavbar").removeClass("sticky-navbar");
            if ($("#seconderyNavbar").length > 0) {
                $("#contentHolder").css("margin-top","");
            }
        }
    });

}

function colorRisks() {
    var red = '#FC1501';
    var orange = '#FF7722';
    var yellow = '#FFFF00';
    var green = '#39D221';
    var lightgreen = '#ddffa6';

    $('.color.risk').each(function () {
        var field = $(this);

        var value_str = field.text().trim();
        var value = parseInt(value_str);

        if(field.hasClass("L") || field.hasClass("M") || field.hasClass("H") || field.hasClass("VH") || field.hasClass("VL")){
            if (field.hasClass("L")) {
                //low
                field.css("background-color", green);
            } else if (field.hasClass("M")) {
                //medium
                field.css("background-color", yellow);
            } else if (field.hasClass("H")) {
                //high
                field.css("background-color", orange);
            } else if (field.hasClass("VL")) {
                //very low
                field.css("background-color", lightgreen);
            } else {
                //very high
                field.css("background-color", red);
            }
        }else{

            if (value < 4 || value_str == "L") {
                //low
                field.css("background-color", green);
            } else if (value < 10 || value_str == "M") {
                //medium
                field.css("background-color", yellow);
            } else if (value < 20 || value_str == "H") {
                //high
                field.css("background-color", orange);
            } else if(value <= 25 || value_str.slice(0, 1) == "V"){
                //very high
                field.css("background-color", red);
            }
        }

    });
}

$(document).ready(function () {

    $(".records_list:visible").resizableColumns();
    if ($("#seconderyNavbar").length > 0){
        $('.records_list:not(.not-sticky)').stickyTableHeaders({fixedOffset: 50});
    }else{
        $('.records_list:not(.not-sticky)').stickyTableHeaders();
    }

    $('.riskmatrix td').each(function () {
        var self = $(this);

        switch (self.text()) {
            case "L":
                self.css('background', 'green');
                break;
            case "M":
                self.css('background', 'yellow');
                break;
            case "H":
                self.css('background', 'orange');
                break;
            case "V":
                self.css('background', 'red');
                break;
        }
    });

    var datetimepicker_conf = {
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        pickerPosition: "bottom-left"
    };

    $(document).on('focusin', '.form_date', function(e) {
        $(this).datetimepicker(datetimepicker_conf);
    });

    /* Javascript Validation */
    var validator = $('form:not(#search-form)').validate(
        {
            ignore:  ":hidden:not(.validate)",
            rules: {
                firstname: {
                    minlength: 3,
                    maxlength: 15,
                    required: true
                },
                lastname: {
                    minlength: 3,
                    maxlength: 15,
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            onfocusout: false,
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    var elm = validator.errorList[0].element;

                    elm.focus();
                    if($(elm).is(':hidden')){
                        alert('A required field is not filled');
                        return;
                    }

                }
            }
        });

    $.validator.addMethod('integer', function (value, element, param) {
        return (value == parseInt(value, 10));
    }, 'Please enter a non zero integer value!');


    jQuery.validator.addClassRules("double", {
        number: true
    });

    colorRisks();

    $('th').click(function(){
        var table = $(this).parents('table').eq(0)
        var rows = table.find("tr:not(:has('th'))").toArray().sort(comparer($(this).index()))
        this.asc = !this.asc
        if (!this.asc){rows = rows.reverse()}
        for (var i = 0; i < rows.length; i++){table.append(rows[i])}
    })

    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index), valB = getCellValue(b, index)
            return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
        }
    }

    function getCellValue(row, index){
        if (Date.parse($(row).children('td').eq(index).html())) {
            return Date.parse($(row).children('td').eq(index).html())
        } else {
            return $(row).children('td').eq(index).html()
        }
    }
    
    var referrerstr = document.referrer;
    var containsaFoo = referrerstr.indexOf('/a/') >= 0;

    //var containscFoo = referrerstr.indexOf('/c/') >= 0;

    if (containsaFoo){
        $('#logo').attr("class", "brand left anomaly-logo");
     }
    //
    // if (containscFoo){
    //     $('#logo').attr("class", "brand left cms-logo");
    // }

});


/**
 * A function do create dynamic symfony forms
 * @param buttonLink
 * @param collectionHolder
 */
function dynamic_form(buttonLink, collectionHolder) {
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    collectionHolder.data('index', collectionHolder.find(':input').length);

    buttonLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm(collectionHolder);
    });

    function addForm(collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = collectionHolder.data('prototype');

        // get the new index
        var index = collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);

        // increase the index with one for the next item
        collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<li></li>').append(newForm);
        collectionHolder.append($newFormLi);
    }

}
