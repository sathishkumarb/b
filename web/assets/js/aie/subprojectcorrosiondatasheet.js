$(document).ready(function() {
    var rows_selected = [];

    $('.filters').hide();
    $('.average-filters').hide();

    if (corrosionType == "ca-cd-ch")
    {

        var rows_inh_selected = [];

        // DataTable

        var table_obj_average = $('.table-grid-head-average').DataTable({
            'columnDefs': [{
                'targets': 0,
                "bSortable": false,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox">';
                }
            }],
            'order': [1, 'asc'],
            'rowCallback': function (row, data, dataIndex) {
                // Get row ID
                var rowId = data[0];

                // If row ID is in the list of selected row IDs
                if ($.inArray(rowId, rows_selected) !== -1) {
                    $(row).find('input[type="checkbox"]').prop('checked', true);
                    $(row).addClass('selected');
                }
            }
        });

        var current = 0;
        // Iterate over all checkboxes in the table
        table_obj_average.$('input[type="checkbox"]').each(function(){
            this.checked = true;
            var data = table_obj_average.row(current).data();

            var rowId = data[0];
            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_inh_selected);
            // If checkbox is checked and row ID is not in list of selected row IDs
            if (this.checked && index === -1) {
                rows_inh_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1) {
                rows_inh_selected.splice(index, 1);
            }

            current++;

        });

        updateDataTableSelectAllCtrlAverage(table_obj_average);

        function updateDataTableSelectAllCtrlAverage(table)
        {
            var $table = table_obj_average.table().node();
            var $chkbox_all = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if ($chkbox_checked.length === 0)
            {
                chkbox_select_all.checked = false;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length)
            {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }

                // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all)
                {
                    chkbox_select_all.indeterminate = true;
                }
            }
        }

        $('.table-grid-head-average-row th').on("click.DT", function (e)
        {

            if (!$(e.target).hasClass('sortMask'))
            {
                if ($('.average-filters:visible').length)
                    $('.average-filters').hide();
                else {
                    $('.average-filters .clsSelectAll').html('');
                    $('.average-filters').show();
                }
            }

            //stop Propagation if clicked outsidemask
            if (!$(e.target).hasClass('sortMask')) {
                e.stopImmediatePropagation();
            }
        });

        // Setup - add a text input to each footer cell
        $('.table-grid-head-average .average-filters th').each(function ()
        {
            var id = $('.table-grid-head-average thead th').attr('id');
            //if (id !== "selectall") {
            var title = $('.table-grid-head-average thead th').eq($(this).index()).text();
            $(this).html('<input id="' + title + '" style="width:150px;" type="text" placeholder="" />');
            // }
        });

        //var ele = $('.clsSelectAll').find('*').filter(':input:visible:first');

        // Apply the search
        table_obj_average.columns().eq(0).each(function (colIdx)
        {
            $('input', $('.average-filters th')[colIdx]).on('keyup change', function ()
            {
                table_obj_average
                    .column(colIdx)
                    .search(this.value)
                    .draw();
            });
        });

        // Handle click on checkbox
        $('#average tbody').on('click', 'input[type="checkbox"]', function (e)
        {
            var $row = $(this).closest('tr');

            // Get row data
            var data = table_obj_average.row($row).data();

            // Get row ID
            var rowId = data[0];

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if (this.checked && index === -1) {
                rows_inh_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1) {
                rows_inh_selected.splice(index, 1);
            }

            if (this.checked) {
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            // Update state of "Select all" control
            updateDataTableSelectAllCtrlAverage(table_obj_average);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        // Handle click on table cells with checkboxes
        $('#average').on('click', 'tbody td, thead th:first-child', function (e) {
            $(this).parent().find('input[type="checkbox"]').trigger('click');
        });

        // Handle click on "Select all" control
        $('thead input[name="select_all"]', table_obj_average.table().container()).on('click', function (e) {
            if (this.checked) {
                $('#average tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('#average tbody input[type="checkbox"]:checked').trigger('click');
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();

        });

        // Handle table draw event
        table_obj_average.on('draw', function () {
            // Update state of "Select all" control
            updateDataTableSelectAllCtrlAverage(table_obj_average);
        });

    }

    if (corrosionType == "pr")
    {
        // DataTable
        var table_obj = $('.table-grid-head').DataTable({
            'columnDefs': [{
                'targets': 0,
                "bSortable": false,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return data;
                }
            }],
            'order': [1, 'desc'],
            'rowCallback': function (row, data, dataIndex) {
                // Get row ID
                var rowId = data[0];

                // If row ID is in the list of selected row IDs
                if ($.inArray(rowId, rows_selected) !== -1) {
                    if (rowId) {
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
            }
        });
    }
    else
    {
        var table_obj = $('.table-grid-head').DataTable({
            'columnDefs': [{
                'targets': 0,
                "bSortable": false,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return data;
                }
            }],
            'order': [1, 'asc'],
            'rowCallback': function (row, data, dataIndex) {
                // Get row ID
                var rowId = data[0];

                // If row ID is in the list of selected row IDs
                if ($.inArray(rowId, rows_selected) !== -1) {
                    if (rowId) {
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
            }
        });

    }

    function updateDataTableSelectAllCtrl(table) {

        var $table = table_obj.table().node();
        var $chkbox_all = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

        // If none of the checkboxes are checked
        if ($chkbox_checked.length === 0) {
            chkbox_select_all.checked = false;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = false;
            }

            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length) {
            chkbox_select_all.checked = true;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = false;
            }

            // If some of the checkboxes are checked
        } else {
            chkbox_select_all.checked = true;
            if ('indeterminate' in chkbox_select_all) {
                chkbox_select_all.indeterminate = true;
            }
        }
    }

    $('.table-grid-head-row th').on("click.DT", function (e) {

        if (!$(e.target).hasClass('sortMask')) {
            if ($('.filters:visible').length)
                $('.filters').hide();
            else {
                $('.filters .clsSelectAll').html('');
                $('.filters').show();
            }
        }

        //stop Propagation if clicked outsidemask
        if (!$(e.target).hasClass('sortMask')) {
            e.stopImmediatePropagation();
        }
    });

    // Setup - add a text input to each footer cell
    $('.table-grid-head .filters th').each(function () {
        var id = $('.table-grid-head thead th').attr('id');
        //if (id !== "selectall") {
        var title = $('.table-grid-head thead th').eq($(this).index()).text();
        $(this).html('<input id="' + title + '" style="width:150px;" type="text" placeholder="" />');
        // }
    });

    //var ele = $('.clsSelectAll').find('*').filter(':input:visible:first');

    // Apply the search
    table_obj.columns().eq(0).each(function (colIdx) {
        $('input', $('.filters th')[colIdx]).on('keyup change', function () {
            table_obj
                .column(colIdx)
                .search(this.value)
                .draw();
        });
    });

    var current = 0;
    // Iterate over all checkboxes in the table
    table_obj.$('input[type="checkbox"]').each(function(){
        this.checked = true;
        var data = table_obj.row(current).data();

        var rowId = data[0];

        var string = rowId,
            expr = /span/;

        if (expr.test(string)==true) {

            var res = string.replace('<span style="display: none;">', "");

            rowId = res.replace('</span>', "");

        }

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);
        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        current++;

    });

    updateDataTableSelectAllCtrl(table_obj);

    // Handle click on checkbox
    $('#example tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table_obj.row($row).data();

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table_obj);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#example').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table_obj.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#example tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();

    });

    // Handle table draw event
    table_obj.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table_obj);
    });

    $('.addCorrosionDatasheet').on("click", function (e)
    {

        var currentID = $(this).attr('id');
        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionLocationsLn > 0)
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }
        else if (data.length > 1  && corrosionType === "pr")
        {
            $.notify({
                // options
                message: 'Please select one location at a time to add production datasheet',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        // Remove added elements

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[1]) + "/" + res[2] + "/new" + "?dataRetrieve=" + data + '&systemId='+systemIDpass;
        //window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');

        $(".addCorrosionDatasheet").attr("href", url);

    });

    $('.editCorrosionDatasheetHistory').on("click", function (e)
    {
        var currentID = $(this).attr('id');

        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionType != "pr")
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        // Remove added elements

        if (corrosionType != "pr")
        {
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/edit" + "?dataRetrieve=" + data+ '&systemId='+systemIDpass;
            //window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');

            $(".editCorrosionDatasheetHistory").attr("href", url);
        }
        else{
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/productionedit"+ '?systemId='+systemIDpass;
            $("#hidGroupId").val(data);
            $("#frmDatasheet").attr('action', url);
            $("#frmDatasheet").submit();
        }

    });

    $('.editCorrosionDatasheetHistoryAverage').on("click", function (e)
    {
        var currentID = $(this).attr('id');

        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_inh_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length)
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        // Remove added elements

        var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/edit" + "?dataRetrieve=" + data+ '&systemId='+systemIDpass;

        $(".editCorrosionDatasheetHistoryAverage").attr("href", url);

    });

    $('.exportCorrosionDatasheetHistory').on("click", function (e)
    {
        var currentID = $(this).attr('id');

        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionType != "pr")
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        // Remove added elements

        if (corrosionType != "pr")
        {
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/exportexcel" + "?dataRetrieve=" + data+ '&systemId='+systemIDpass;
            //window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');
            //$(".editCorrosionDatasheetHistory").attr("href", url);
        }
        else{
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/productionexcel"+ '?systemId='+systemIDpass;
            $("#hidGroupId").val(data);
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                "hidGroupId" :data.join(),
            },
            success: function (s) {
                $('#hiddenTblToExcel').html(s);
                exportThis();
            },
            error: function (e) {
                notify();
                return;
            }
        });

    });

    var exportThis = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel"  xmlns="http://www.w3.org/TR/REC-html40"><head> <!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets> <x:ExcelWorksheet><x:Name>{worksheet}</x:Name> <x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions> </x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook> </xml><![endif]--></head><body> <table>{table}</table></body></html>',
            base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            },
            format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; })
            }
        return function () {
            var ctx = { worksheet: 'AIE - Veracity Datasheet' || 'Worksheet', table: document.getElementById("hiddenTblToExcel").innerHTML }
            document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = 'AIE-VeracityCCM'+filenameTodownlaod;
            document.getElementById("dlink").click();

        }
    })()

    $('.exportCorrosionDatasheetHistoryAverage').on("click", function (e)
    {

        var currentID = $(this).attr('id');

        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionType != "pr")
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        // Remove added elements
        var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/exportexcel" + "?dataRetrieve=" + data+ '&systemId='+systemIDpass;

        $.ajax({
            url: url,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                "hidGroupId" :data.join(),
            },
            success: function (s) {

                $('#hiddenTblToExcel').html(s);

                exportThis();

            },
            error: function (e) {
                notify();
                return;
            }
        });

    });

    $('.graphLocationCorrosionDatasheet').on("click", function (e)
    {
        var currentID = $(this).attr('id');

        var res = currentID.split('_');
        //console.log(res);
        var result = [];

        // Iterate over all selected checkboxes
        $.each(rows_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });

        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionType != "pr")
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        dataPost = JSON.stringify(data);
        dataPost = dataPost.replace(/\"/g, "");

        if (corrosionType == "ca-cd-ch")
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/" + dataPost + "/graphcorrosioninhibitor"+ '?systemId='+systemIDpass;
        else if (corrosionType == "pr")
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/graphproduction"+ '?systemId='+systemIDpass;
        else
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/" + dataPost + "/graph"+ '?systemId='+systemIDpass;

        if (corrosionType != "pr")
        {
            //window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');

            $(".graphLocationCorrosionDatasheet").attr("href", url);
        }
        else{

            $("#hidGroupId").val(dataPost);
            $("#frmDatasheet").attr('action', url);
            $("#frmDatasheet").submit();
        }

    });

    $('.addCorrosionDatasheetAverage').on("click", function (e)
    {

        var currentID = $(this).attr('id');
        var res = currentID.split('_');

        var result = [];

        // Iterate over all selected checkboxes

        $.each(rows_inh_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });
        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length && corrosionInhibitorLocationsLn > 0 )
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        // Remove added elements

        var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[1]) + "/" + res[2] + "/new" + "?dataRetrieve=" + data+ '&systemId='+systemIDpass;
        //window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');

        $(".addCorrosionDatasheetAverage").attr("href", url);

    });

    $('.graphLocationCorrosionDatasheetAverage').on("click", function (e)
    {
        var currentID = $(this).attr('id');

        var res = currentID.split('_');
        //console.log(res);
        var result = [];

        // Iterate over all selected checkboxes
        $.each(rows_inh_selected, function (index, rowId) {

            var res = rowId.replace('<input type="checkbox" id="id[]" name="id" value="', "");

            res = res.replace('">', "");

            result.push(res);
        });

        //sort the ids.
        result = result.sort(function (a, b) {
            return (a - b);
        })

        data = result;

        if (!data.length)
        {
            $.notify({
                // options
                message: 'Please select location(s)',
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
            return;
        }

        if ($('#systemId').val()) var systemIDpass = $('#systemId').val(); var systemIDpass = 0;

        dataPost = JSON.stringify(data);

        if (corrosionType == "ca-cd-ch")
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/" + dataPost + "/graphcorrosioninhibitor"+ '?systemId='+systemIDpass;
        else if (corrosionType == "pr")
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/" + dataPost + "/graphproduction"+ '?systemId='+systemIDpass;
        else
            var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + parseInt(res[0]) + "/" + res[1] + "/" + dataPost + "/graph"+ '?systemId='+systemIDpass;

//        window.open(url, 'popUpWindow', 'height=600, width=750, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes', '_blank');
        $(".graphLocationCorrosionDatasheetAverage").attr("href", url);
//
    });

    $(document).on('click', '.saveLocationCorrosionDatasheet', function (e)
    {
        var currentID = $(this).attr('id');
        var data = $("#grid_json_paste").pqGrid("option", "dataModel.data");
        var url = "/c/" + projectId + "/subprojectcorrosiondatasheet/" + subprojectId + "/" + currentID + "/create";
        $.ajax({
            url: url,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                dataStore: data,
                activityId: currentID,
                corrosionType: corrosionType
            },
            success: function (s) {

                try {
                    obj = JSON.parse(s);
                } catch (e) {

                    // not json
                }

                if (obj.result == 'notok') {
                    notify();
                    return;
                }
            },
            error: function (e) {
                notify();
                return;
            }
        });
    });

    function notify() {
        $.notify({
            // options
            message: 'Error processing your request!',
        }, {
            // settings
            type: 'danger',
            placement: {
                from: "top",
                align: "center"
            }
        });
    }

    $(document).on('click', '.submitcorrosion', function (e)
    {
        $('#frmCorrosionData #frmCorrosionActivityD').submit();
    });

});