var deleteId ;
var notify;

$(document).ready(function() {

    // DataTable and code for index column
    var table = $('.table-grid-head').DataTable({
        /* Disable initial sort */
        "aaSorting": [],
    });

    $('.table-grid-head-row th').on("click.DT", function (e) {

        if (!$(e.target).hasClass('sortMask')) {
            if ($('.filters:visible').length)
                $('.filters').hide();
            else {
                $('.filters').show();
            }
        }

    });

    $('.table-grid-head .filters th').each( function () {
        var title = $('.table-grid-head thead th').eq( $(this).index() ).text();
        $(this).html( '<input style="width:80px;" type="text" placeholder="'+title+'" />' );

    } );

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', $('.filters th')[colIdx] ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
    $('#linkHeader').children().hide();
    $('#indexHeader').children().hide();
    $('.filters').hide();

    $(document).on('click','#deleteSubmit_',function(e){

        var linkId = '#delete_'+deleteId;
        var url = $(linkId).prop("href");
        window.location.href = url;
        deleteId = null;
    });

    $(document).on('click','.closeNotification',function(e){
        notify.close();
    });

    //Use notify.js to show delete confirmation message
    $('.deleteLink').click(function(e) {
        e.preventDefault();
        var id_str = $(this).attr('id');
        index = id_str.lastIndexOf("_")+1;
        id = id_str.slice(index);
        deleteId = id;
        $('.alert').hide();
        if(notify != null)
            notify.close();
        notify = $.notify({
            title: 'Are you sure to delete?',
            message: 'Confirm Deletion?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div id='message' style='width:79%; height:70px;' class='alert alert-danger'>" +
                            "<div class='row pad'>" +
                                "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
                                    "<div class='col-md-1' style='text-align:right;'>" +
                                    "<button class='yes btn btn-primary right btn-danger' id=\"deleteSubmit_\">Delete</button></div>" +
                                    "<div class='col-md-1' style='text-align:left; padding-right:40px;'>" +
                                    "<button class='no btn right btn-default closeNotification'>Cancel</button>" +
                                "</div>" +
                            "</div>" +
                        "</div>"
        });

        console.log($(window).width())

        var string = window.location.href,
            substring = "subprojectactiontrackeritems",
            substringfirst = "subprojectactiontracker";

        if ($(window).width() > 1500 && $(window).width() <= 1650 && string.indexOf(substring) !== -1)
        {
            //$('.sidenav').width('300');

            $('#message').css(
                "width" , "96%"
            );

        }
        else if ($(window).width() <=1350 && string.indexOf(substringfirst) !== -1)
        {
            //$('.sidenav').width('300');

            $('#message').css(
                "width" , "90%"
            );

        }
        else if ($(window).width() > 1900)
        {
            //$('.sidenav').width('300');

            $('#message').css(
                "width" , "82%"
            );

        }
    });

});

