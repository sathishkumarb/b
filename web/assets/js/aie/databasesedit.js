var notify;
$(document).ready(function() {

    $('.table-grid-head-row th').on("click.DT", function (e) {

        if (!$(e.target).hasClass('sortMask')) {
            if ($('.filters:visible').length)
                $('.filters').hide();
            else
                $('.filters').show();
        }

        //stop Propagation if clciked outsidemask
        //becasue we want to sort locally here
        if (!$(e.target).hasClass('sortMask')) {
            e.stopImmediatePropagation();
        }
    });
    // Setup - add a text input to each footer cell
    $('.table-grid-head .filters th').each( function () {
        var title = $('.table-grid-head thead th').eq( $(this).index() ).text();
        $(this).html( '<input id="'+title+'" style="width:80px;" type="text" placeholder="'+title+'" />' );
    } );

    $.fn.dataTable.ext.errMode = 'none';

    if (excelShow)
    {
        var messageTopExcel = projectName + " - " + subprojectName + " - " +  systemname;
        var table = $('.table-grid-head').DataTable({
            "dom": '<"top"Blf>rt<"bottom"ip><"clear">',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Export to Excel',
                    className: 'grey',
                    titleAttr: 'Export to Excel',
                    exportOptions: {
                        columns: ':visible',
                    },
                    messageTop: messageTopExcel ,

                },
                {
                    extend: 'colvis',
                    text: 'Select Column(s)',
                    className: 'orange',
                    collectionLayout: 'fixed four-column'
                },
            ],
        });
    }
    else
    {
        var table = $('.table-grid-head').DataTable();
    }
   // $('.downloadExcel').html("");

  //  $('.dt-buttons').appendTo($('.downloadExcel'));

    // Apply the search

    if (table.columns().eq( 0 )) {
        table.columns().eq(0).each(function (colIdx) {
            $('input', $('.filters th')[colIdx]).on('keyup change', function () {
                table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
            });
        });
    }

    $('#Photo').hide();
    $('#Actions').hide();
    $('.filters').hide();

    var isModelDialogOpen = false;

    $(document).on('click','.close',function(e) {
        can(e);
    });

    $(document).on('click','.subcancelbutton',function(e) {
        can(e);
    });

    $(document).on('click','.editsamplingdatabase',function(e) {
            e.preventDefault();
            var stepID = $("#wizard").wizard('selectedItem')['step'];
            var currentID = $(this).attr('id');
            var res = currentID.split("_");
            var url = "/c/" + projectId + "/subprojectsamplingdatabase/" + subprojectId + "/" + stepID + "/" + res[0] + "/" + res[1] + "/edit";

        callajax(url);

    });

    $(document).on('click','.editcathodicprotectiondatabase',function(e) {
        var stepID = $("#wizard").wizard('selectedItem')['step'];
        var currentID = $(this).attr('id');
        var res = currentID.split("_");
        var url = "/c/"+projectId+"/subprojectcathodicprotectiondatabase/"+subprojectId+"/"+stepID+"/"+res[0]+"/"+res[1]+"/edit";
        //var modal_id = $(this).attr('data-target');
        callajax(url);
    });

    $(document).on('click','.editchemicaldosagedatabase',function(e) {
        var stepID = $("#wizard").wizard('selectedItem')['step'];
        var currentID = $(this).attr('id');
        var res = currentID.split("_");
        var url = "/c/"+projectId+"/subprojectchemicaldosagedatabase/"+subprojectId+"/"+stepID+"/"+res[0]+"/"+res[1]+"/edit";
        //var modal_id = $(this).attr('data-target');
        callajax(url);
    });

    $(document).on('click','.editcorrosionmonitoringdatabase',function(e) {
        var stepID = $("#wizard").wizard('selectedItem')['step'];
        var currentID = $(this).attr('id');
        var res = currentID.split("_");
        var url = "/c/"+projectId+"/subprojectcorrosionmonitoringdatabase/"+subprojectId+"/"+stepID+"/"+res[0]+"/"+res[1]+"/edit";
        //var modal_id = $(this).attr('data-target');
        callajax(url);
    });

    $(document).on('click','.editproductiondatabase',function(e) {
        var stepID = $("#wizard").wizard('selectedItem')['step'];
        var currentID = $(this).attr('id');
        var res = currentID.split("_");
        var url = "/c/"+projectId+"/subprojectproductiondatabase/"+subprojectId+"/"+stepID+"/"+res[0]+"/"+res[1]+"/edit";
        //var modal_id = $(this).attr('data-target');
        callajax(url);
    });

    function callajax(url) {
        var modal_id = $(this).attr('data-target');
        // $('[data-toggle="modal"]').modal('show');
        $.blockUI({ message: "<h4>Page Loading...</h4>", overlayCSS: { backgroundColor: '#747474' }  });
        $.post(url, function (data) {
            $('.modal').remove();
            $('.modal-body').empty();
            $(modal_id).removeData();
            $(data).modal({
                backdrop: 'static',
                keyboard: false
            });
        }).success(function (d) {

            $(modal_id).on('hide.bs.modal', function(e) {
                $(this).removeData('bs.modal');
            });
            $.unblockUI();
        });
    }

    function can(e) {

        $('[data-toggle="modal"]').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $('.modal').not($(this)).each(function () {
            $(this).modal('hide');
        });

        if ($('.editsamplingdatabase').is(":visible")) {
            var idd = $('.editsamplingdatabase').attr('id');
            idd = idd.split("_");
            var esv = '#editsamplingdatabasemodal' + idd[1] + ':visible'
            var es = '#editsamplingdatabasemodal' + idd[1];
            if ($(esv).length) $(es).remove();
        }

        if ($('.editcathodicprotectiondatabase').is(":visible")) {
            var idsd = $('.editcathodicprotectiondatabase').attr('id');
            idsd = idsd.split("_");
            var ecpv = '#editcathodicprotectiondatabasemodal' + idsd[1] + ':visible'
            var ecp = '#editcathodicprotectiondatabasemodal' + idsd[1];
            if($(ecpv).length) $(ecp).remove();
        }

        if ($('.editchemicaldosagedatabase').is(":visible")) {
            var idds = $('.editchemicaldosagedatabase').attr('id');
            idds = idds.split("_");
            var ecdv = '#editchemicaldosagedatabasemodal' + idds[1] + ':visible'
            var ecd = '#editchemicaldosagedatabasemodal' + idds[1];
            if($(ecdv).length) $(ecd).remove();
        }

        if ($('.editcorrosionmonitoringdatabase').is(":visible")) {
            var idsdd = $('.editcorrosionmonitoringdatabase').attr('id');
            idsdd = idsdd.split("_");
            var ecmv = '#editcorrosionmonitoringdatabasemodal' + idsdd[1] + ':visible'
            var ecm = '#editcorrosionmonitoringdatabasemodal' + idsdd[1];
            if($(ecmv).length) $(ecm).remove();
        }

        if ($('.editproductiondatabase').is(":visible")) {
            var idsdd = $('.editproductiondatabase').attr('id');
            idsdd = idsdd.split("_");
            var ecmv = '#editproductiondatabasemodal' + idsdd[1] + ':visible'
            var ecm = '#editproductiondatabasemodal' + idsdd[1];
            if($(ecmv).length) $(ecm).remove();
        }

        if($('#samplingdatabasemodal:visible').length) $('#samplingdatabasemodal').remove();
        if($('#cathodicprotectiondatabasemodal:visible').length) $('#cathodicprotectiondatabasemodal').remove();
        if($('#chemicaldosagedatabasemodal:visible').length) $('#chemicaldosagedatabasemodal').remove();
        if($('#corrosionmonitoringdatabasemodal:visible').length) $('#corrosionmonitoringdatabasemodal').remove();
        if($('#productiondatabasemodal:visible').length) $('#productiondatabasemodal').remove();

        isModelDialogOpen = false;

    }

    $(document).on('click','#deleteSubmit_',function(e){

        var linkId = '#delete_'+deleteId;
        var url = $(linkId).prop("href");

        $.ajax({
            url: url,
            type: 'GET',
            data: {   // Data Sending With Request To Server

            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result === 'ok'){
                    location.reload(true);
                }
                else
                {
                    alert('Error Processing your Request!');
                    return;
                }
            },

        });
        deleteId = null;
    });

    $(document).on('click','.closeNotification',function(e){
        notify.close();
    });

    //Use notify.js to show delete confirmation message
    $("table").on('click','.deleteDatabase',function(e){
        e.preventDefault();
        var id_str = $(this).attr('id');
        index = id_str.lastIndexOf("_")+1;
        id = id_str.slice(index);
        deleteId = id;
        $('.alert').hide();
        notify = $.notify({
            title: 'Are you sure to delete?',
            message: 'Confirm Deletion?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165

            },
            type: 'danger',
            position: 'fixed',
            delay: 0,
            template: "<div id='message' style='position: fixed; top: 0; width: 98%;'>" +
                            "<div><div style='width:100%; height:70px;' class='alert alert-danger'>" +
                                "<div class='row pad'>" +
                                    "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
                                    "<div class='col-md-1' style='text-align:right;'>" +
                                        "<button class='yes btn btn-primary btn-danger' id=\"deleteSubmit_\">Delete</button></div>" +
                                    "<div class='col-md-1' style='text-align:left; padding:0;'>"+
                                        "<button class='no btn btn-default closeNotification'>Cancel</button>"+
                                    "</div>"+
                                "</div>" +
                            "</div>"+
                       "</div>"
        });

        if ($(window).width() <= 1650)
        {
            //$('.sidenav').width('300');

            $('#message').css(
                "width", "98%"
            );

        }

    });
});

