jQuery(document).ready(function() {
    var notify;
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.emptytext= '',
    $.fn.editable.defaults.emptyclass= '',
    $('.xedit').editable();
    $(document).on('click','.editable-submit',function(e) {
        var key = $(this).closest('.editable-container').prev().attr('key');
        var x = $(this).closest('.editable-container').prev().attr('id');
        var y = $('.input-sm').val();
        var expVal=null;

        if (key == "systems") {
            var currentID = x;
            currentID = currentID.replace('systems', '');
            var ur = "/c/basematrix/basesystems/" + currentID + "/update";
            var passId = currentID;
        }
        else if (key == "threats") {
            var currentID = x;
            currentID = currentID.replace('threats', '');
            var res = currentID.split('_');
            var ur = "/c/basematrix/" + res[0] + "/basethreats/" + res[1] + "/update";
            var passId = res[1];
        }
        else if (key == "mitigationtechniques") {
            var currentID = x;
            currentID = currentID.replace('mitigationtechniques', '');
            var res = currentID.split('_');
            var ur = "/c/basematrix/" + res[0] + "/basemitigationtechniques/" + res[1] + "/update";
            var passId = res[1];
        }
        else if (key.indexOf("threshold") >= 0) {
            var currentID = x;
            currentID = currentID.replace('threshold', '');
            var res = currentID.split('_');
            var ur = "/c/basematrix/" + res[0] + "/baseactivity/" + res[1] + "/updatefields";
            var passId = res[1];
        }
        if (!y){
            $(this).closest('.form-group').addClass('has-error');
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                title:y,
                titleexpression:expVal,
                passid:passId,
                key:key
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true);
                }
                else if (obj.result == 'notok'){
                    alert('Error Processing your Request!!');
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });
    });

    $("#add_new_systems").click(function () {
        $("#systemsData").show();
    });

    $(".add_new_threats").click(function () {
        var currentId = $(this).attr('id');
        currentId = currentId.replace('threats', '');
        $("#threatData"+currentId).html('<b>Loading....</b>');
        var ur = "/c/basematrix/"+currentId+"/basethreatscreate";

        $.ajax({
            url: ur,
            type: 'POST',
            success: function(s){
                $("#threatData"+currentId).html(s);
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });

    });

    $(".add_new_mitigationtechniques").click(function () {

        var currentId = $(this).attr('id');
        currentId = currentId.replace('mitigationtechnique', '');
        $("#mitigationtechniqueData"+currentId).html('<b>Loading....</b>');
        var ur = "/c/basematrix/"+currentId+"/basemitigationtechniquescreate";

        $.ajax({
            url: ur,
            type: 'POST',
            success: function(s){
                $("#mitigationtechniqueData"+currentId).html(s);
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });

    });
    var isModelDialogOpen = false;

    $('[data-toggle="modal"]').click(function(e) {
        if (isModelDialogOpen === false) {
            isModelDialogOpen = true;
            e.preventDefault();
            var url = $(this).attr('href');
            //var modal_id = $(this).attr('data-target');
            $.post(url, function (data) {
                $(data).modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }
    });

    $(document).on('click','.close',function(e){
        $('[data-toggle="modal"]').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    });

    $(".edit_db_metric").click(function () {

        var currentId = $(this).attr('id');
        currentId = currentId.replace('editmetric', '');

        var res = currentId.split('_');
        var ur = "/c/basematrix/"+res[0]+"/baseactivity/"+res[1]+"/edit";
        var el = "#editMetricData"+res[0]+"_"+res[1];
        $(el).html('<b>Loading....</b>');

        $.ajax({
            url: ur,
            type: 'GET',
            success: function(s){
                $(el).html(s);
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });

    });

    $(".greenlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('greenlink', '');

        $(".titlegreenexpression"+currentID).hide();
        $(".titlegreenexpressionrange"+currentID).hide();

        $(".titlegreen"+currentID).hide();
        $(".titlegreenrange"+currentID).hide();

        $(".fieldgreentitleexpression"+currentID).show();
        $(".fieldgreentitleexpressionrange"+currentID).show();

        $(".fieldgreen"+currentID).show();
        $(".fieldgreenrange"+currentID).show();
        $("#subeditgreenbuttons"+currentID).show();

    });

    $(".amberlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('amberlink', '');

        $(".titleamberexpression"+currentID).hide();
        $(".titleamberexpressionrange"+currentID).hide();

        $(".titleamber"+currentID).hide();
        $(".titleamberrange"+currentID).hide();

        $(".fieldambertitleexpression"+currentID).show();
        $(".fieldambertitleexpressionrange"+currentID).show();

        $(".fieldamber"+currentID).show();
        $(".fieldamberrange"+currentID).show();
        $("#subeditamberbuttons"+currentID).show();

    });

    $(".redlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('redlink', '');

        $(".titleredexpression"+currentID).hide();
        $(".titleredexpressionrange"+currentID).hide();

        $(".titlered"+currentID).hide();
        $(".titleredrange"+currentID).hide();

        $(".fieldredtitleexpression"+currentID).show();
        $(".fieldredtitleexpressionrange"+currentID).show();

        $(".fieldred"+currentID).show();
        $(".fieldredrange"+currentID).show();
        $("#subeditredbuttons"+currentID).show();

    });

    $(document).on('click','.closeNotification',function(e){
        notify.close();
    });

    $(document).on('click','.deleteSystem',function(e){
        var currentID = $(this).attr('id');
        currentID = currentID.replace('systems', '');
        var ur = "/c/basematrix/" + currentID + "/basesystems/delete";
        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                id: currentID,
            },
            success: function (s) {
                obj = JSON.parse(s);

                if (obj.result == 'ok') {
                    location.reload(true)
                }
                else if (obj.result == 'notok') {
                    alert('Error Processing your Request');
                    return;
                }
            },
            error: function (e) {
                alert('Error Processing your Request!!');
                return;
            }
        });
    });

    $(".delete_systems").click(function(evt) {
        var currentID = $(this).attr('id');
        notify = $.notify({
            title: 'Are you sure to delete system?',
            message: 'Do you really want to delete the system?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div style='width:82%;height:70px;' class='alert alert-danger'>" +
                            "<div class='row pad'>" +
                                "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
                                "<div class='col-md-1' style='text-align:right;'>" +
                                    "<button class='yes btn btn-primary right btn-danger deleteSystem' id="+currentID+">Delete</button></div>" +
                                    "<div class='col-md-1' style='text-align:left; padding-right:40px;'>"+
                                        "<button class='no btn right btn-default closeNotification'>Cancel</button>"+
                                    "</div>"+
                            "</div>" +
                        "</div>"
        });

        return;
    });

    $(document).on('click','.deleteThreat',function(e){
        var currentID = $(this).attr('id');
        currentID = currentID.replace('threats', '');

        var res = currentID.split('_');
        var ur = "/c/basematrix/" + res[1] + "/basethreats/delete";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                systemid: res[0],
                threatid: res[1],
            },
            success: function (s) {
                obj = JSON.parse(s);

                if (obj.result == 'ok') {
                    location.reload(true)
                }
                else if (obj.result == 'notok') {
                    alert('Error Processing your Request');
                    return;
                }
            },
            error: function (e) {
                alert('Error Processing your Request!!');
                return;
            }
        });
    });

    $(".delete_threats").click(function(evt) {
        var currentID = $(this).attr('id');

        notify = $.notify({
            title: 'Are you sure to delete threat?',
            message: 'Do you really want to delete the threat?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div style='width:82%;height:70px;' class='alert  alert-danger'>" +
                            "<div class='row pad'>" +
                            "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
                            "<div class='col-md-1' style='text-align:right;'>" +
                                "<button class='yes btn btn-primary right btn-danger deleteThreat' id="+currentID+">Delete</button></div>" +
                                "<div class='col-md-1' style='text-align:left; padding-right:40px;'>"+
                                    "<button class='no btn right btn-default closeNotification'>Cancel</button>"+
                                "</div>"+
                            "</div>" +
                        "</div>"
        });

        return;

    });

     $(document).on('click','.deleteMitigation',function(e){
        var currentID = $(this).attr('id');
        currentID = currentID.replace('mitigationtechniques', '');

        var res = currentID.split('_');
        var ur = "/c/basematrix/" + res[1] + "/basemitigationtechniques/delete";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                threatid: res[0],
                mitigationtechniqueid: res[1],
            },
            success: function (s) {
                obj = JSON.parse(s);

                if (obj.result == 'ok') {
                    location.reload(true)
                }
                else if (obj.result == 'notok') {
                    alert('Error Processing your Request');
                    return;
                }
            },
            error: function (e) {
                alert('Error Processing your Request!!');
                return;
            }
        });
    });

    $(".delete_mitigationtechniques").click(function(evt) {
        var currentID = $(this).attr('id');

        notify = $.notify({
            title: 'Are you sure to delete mitigation technique?',
            message: 'Do you really want to delete the mitigation technique?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div style='width:82%;height:70px;' class='alert  alert-danger'>" +
                            "<div class='row pad'>" +
                                "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
                                "<div class='col-md-1' style='text-align:right;'>" +
                                    "<button class='yes btn btn-primary right btn-danger deleteMitigation' id="+currentID+">Delete</button></div>" +
                                "<div class='col-md-1' style='text-align:left; padding-right:70px;'>" +
                                    "<button class='no btn right btn-default closeNotification'>Cancel</button>" +
                                "</div>" +
                            "</div>" +
                        "</div>"
        });

        return;

    });

    $(document).on('click','.edit-submit-green',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");
        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditgreenbutton', '');
        var res = newValue.split('_');

        var key ="controlgreen";
        var pr = '#selgreenexpression'+res[1];
        var dt = '#txtgreen'+res[1];

        var pri = '#selgreenexpressionrange'+res[1];
        var dti = '#txtgreenrange'+res[1];

        splitS = splitS.replace('subeditgreenbutton', '');
        var res = splitS.split('_');
        var ur = "/c/basematrix/"+res[0]+"/baseactivity/"+res[1]+"/updateexpressionfields";

        if (!$(dt).val()){
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        if (isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true)
                }
                else if (obj.result == 'notok')
                {
                    alert('Control activity is "N/A", only text "N/A" accepted as control threshold[s]!');
                    return;
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
                return;
            }
        });

    });

    $(document).on('click','.edit-submit-red',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");
        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditredbutton', '');
        var res = newValue.split('_');

        var key ="controlred";
        var pr = '#selredexpression'+res[1];
        var dt = '#txtred'+res[1];

        var pri = '#selredexpressionrange'+res[1];
        var dti = '#txtredrange'+res[1];

        splitS = splitS.replace('subeditredbutton', '');
        var res = splitS.split('_');
        var ur = "/c/basematrix/"+res[0]+"/baseactivity/"+res[1]+"/updateexpressionfields";

        if (!$(dt).val()){
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        if (isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true)
                }
                else if (obj.result == 'notok')
                {
                    alert('Control activity is "N/A", only text "N/A" accepted as control threshold[s]!');
                    return;
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
                return;
            }
        });

    });

    $(document).on('click','.edit-submit-amber',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");
        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditamberbutton', '');
        var res = newValue.split('_');

        var key ="controlamber";
        var pr = '#selamberexpression'+res[1];
        var dt = '#txtamber'+res[1];

        var pri = '#selamberexpressionrange'+res[1];
        var dti = '#txtamberrange'+res[1];

        splitS = splitS.replace('subeditamberbutton', '');
        var res = splitS.split('_');
        var ur = "/c/basematrix/"+res[0]+"/baseactivity/"+res[1]+"/updateexpressionfields";

        if ($(dt).val() && isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true)
                }
                else if (obj.result == 'notok')
                {
                    alert('Control activity is "N/A", only text "N/A" accepted as control threshold[s]!');
                    return;
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
                return;
            }
        });

    });

    $(document).on('click','.subcancelbutton',function(e){
        window.location.reload();
    });

});

$(document).on('click',".systemssave", function() {
    if (!$("#aie_bundle_cmsbundle_basesystems_systemstitle").val()) {
        $('input[required]')
            .closest(".form-group")
            .addClass('has-error');
        event.preventDefault();
        return;
    }
});

$(document).on('click',".threatssave", function() {
    if (!$("#aie_bundle_cmsbundle_basethreats_threatstitle").val()) {
        $('input[required]')
            .closest(".form-group")
            .addClass('has-error');
        event.preventDefault();
        return;
    }
});

$(document).on('click',".activitysave", function(){
    var fl = false;

    if (!$("#aie_bundle_cmsbundle_baseactivtiy_mitigationtechniquestitle").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_baseactivtiy_mitigationtechniquestitle').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_managementtitle").val()) {
        $('#aie_bundle_cmsbundle_baseactivtiy_managementtitle').closest('.form-group').addClass('has-error');
        fl = true;
    }
    if (isNaN($("#aie_bundle_cmsbundle_baseactivtiy_frequency").val()) || !$("#aie_bundle_cmsbundle_baseactivtiy_frequency").val() || $("#aie_bundle_cmsbundle_baseactivtiy_frequency").val() == 0) {
        fl = true;
        $('#aie_bundle_cmsbundle_baseactivtiy_frequency').closest('.form-group').addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_frequency').closest('.form-group').removeClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_activityperson").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_baseactivtiy_activityperson').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_weightage").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_baseactivtiy_weightage').closest('.form-group').addClass('has-error');
    }
    if (isNaN($("#aie_bundle_cmsbundle_baseactivtiy_weightage").val())) {
        fl = true;
        $('#aie_bundle_cmsbundle_baseactivtiy_weightage').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_managementtitle").val()) {
        fl = true;
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_managementgreen").val()) {
        fl = true;
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_managementred").val()) {
        fl = true;
    }
    if (!$("#aie_bundle_cmsbundle_baseactivtiy_controltitle").val()) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').prev().addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').removeClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').prev().removeClass('has-error');
    }


    if ($("#aie_bundle_cmsbundle_baseactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_baseactivtiy_controlgreen").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controlgreen').closest('.form-group').addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_controlgreen').closest('.form-group').removeClass('has-error');
    }
    if ($("#aie_bundle_cmsbundle_baseactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_baseactivtiy_controlamber").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controlamber').closest('.form-group').addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_controlamber').closest('.form-group').removeClass('has-error');
    }
    if ($("#aie_bundle_cmsbundle_baseactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_baseactivtiy_controlred").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controlred').closest('.form-group').addClass('has-error');
    }
    else if (!$("#aie_bundle_cmsbundle_baseactivtiy_controlred").val()) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controlred').closest('.form-group').addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_controlred').closest('.form-group').removeClass('has-error');
    }
    if (($("#aie_bundle_cmsbundle_baseactivtiy_controlgreen").val() == "N/A" || $("#aie_bundle_cmsbundle_baseactivtiy_controlamber").val() == "N/A" || $("#aie_bundle_cmsbundle_baseactivtiy_controlred").val() == "N/A") && $("#aie_bundle_cmsbundle_baseactivtiy_controltitle").val() != "N/A") {
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').addClass('has-error');
    }
    else if (!$("#aie_bundle_cmsbundle_baseactivtiy_controlgreen").val()) {
        $('#aie_bundle_cmsbundle_baseactivtiy_controlgreen').closest('.form-group').addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_baseactivtiy_controltitle').closest('.form-group').removeClass('has-error');
    }

    if (fl == true){

        $('#aie_bundle_cmsbundle_baseactivtiy_managementtitle').closest('.form-group').prev().addClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_managementgreen').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_managementred').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_basemetrics').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_baseactivtiy_basedatasheet').closest('.form-group').addClass('has-error');

        $('.modal-footer').html('<label><font color="#8b0000">* Please check all mandatory and valid inputs (numbers)</font></label>');
        event.preventDefault();
        return;

    }
    else{
        $(this).val($('#aie_bundle_cmsbundle_baseactivtiy_mitigationtechniquestitle').val());
        return;
    }

});

$(document).on('change',".elementMetrics", function(){

    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "/c/basematrix/ajaxloaddatasheet?metrics_id=" + val,
        success: function(data) {
            // Remove current options
            $('.elementDatasheet').html('');
            $.each(data, function(k, v) {
                $('.elementDatasheet').append('<option value="' + v + '">' + k + '</option>');
            });
        }
    });
    return false;

});
