jQuery(document).ready(function() {
    $.fn.editable.defaults.mode = 'inline';
    $('.xedit').editable();
    $(document).on('click','.editable-submit',function() {
        var key = $(this).closest('.editable-container').prev().attr('key');
        var x = $(this).closest('.editable-container').prev().attr('id');
        var y = $('.input-sm').val();

        if (x)
        {
            x = x.replace('ed', '');
            var res = x.split('_');
            var pli = $(".pLinkEditData").attr("id");
            pli = pli.replace('pli', '');
            var ur = "/c/"+res[0]+"/subprojectdatabaseconfig/"+res[1]+"/"+pli+"/create";
            var idval = res[2];
        }
        else{
            x = $('.input-sm').attr('id');
            var res = x.split('_');
            var ur = "/c/"+res[0]+"/subprojectdatabaseconfig/"+res[1]+"/"+res[2]+"/create";
            var idval = 0;
        }
        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                title:y,
                id:idval,
                key:key
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true);
                }
                else if (obj.result == 'error::empty'){
                    alert('Error Processing your Request, Please enter the text!');
                }
                else if (obj.result == 'error::noentity'){
                    alert('Error Processing your Request, No Sub Project Found!');
                }
                else if (obj.result == 'error::duplicate'){
                    alert('Error Processing your Request, Duplicate entries not allowed!');
                }
                else{
                    alert('Error Processing your Request!');
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });
    });

    $(document).on('click','.editable-cancel',function() {
        location.reload(true)
    });

    $("#service").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {

        });

    });

    $("#sampling").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {

        });

    });

    $("#chemical").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {

        });

    });

    $("#corrosion").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {

        });

    });

    $("#cathodic").click(function () {

        $header = $(this);
        //getting the next element
        $content = $header.next();
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        $content.slideToggle(500, function () {

        });

    });

    $(".linkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('link', '');
        $(".titlecolone"+currentID).hide();
        $(".fieldcolone"+currentID).show();
        $(".titlecoltwo"+currentID).hide();
        $(".fieldcoltwo"+currentID).show();
        $("#subeditbuttons"+currentID).show();

    });
    var clickDel = false;

    $(document).on('click','.deleteDB',function(e){

        var currentID = $(this).attr('id');
        currentID = currentID.replace('del', '');
        currentID = currentID.split('_');

        var pli = $(".pLinkEditData").attr("id");
        pli = pli.replace('pli', '');

        var ur = "/c/"+currentID[0]+"/subprojectdatabaseconfig/"+currentID[1]+"/"+pli+"/delete";
        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                id:currentID[2],
            },
            success: function(s){
                obj = JSON.parse(s);
                if (obj.result == 'ok'){
                    location.reload(true);
                }
                else if (obj.result == 'error::empty'){
                    alert('Error processing your request, please enter the text!');
                }
                else if (obj.result == 'error::duplicate'){
                    alert('Error processing your request, duplicate entries not allowed!');
                }
                else if (obj.result == 'error::denied'){
                    alert('Error Processing your Request, You are not permitted to perform this action!');
                    return;
                }
                else{
                    alert('Error processing your request!');
                }
            },
            error: function(e){
                alert('Error processing your request!!');
            }
        });

    });

    $(".deletespan").click(function(evt) {

        window.scrollTo(0, 0);

        var currentID = $(this).attr('id');
        notify = $.notify({
            title: 'Are you sure you want to delete?',
            message: 'Do you really want to delete the database properties?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div style='width:82%;height:70px;' class='alert  alert-danger'>" +
            "<div class='row pad'>" +
            "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
            "<div class='col-md-1' style='text-align:right;'>" +
            "<button class='yes btn btn-primary right btn-danger deleteDB' id="+currentID+">Delete</button></div>" +
            "<div class='col-md-1' style='text-align:left; padding-right:70px;'>" +
            "<button class='no btn right btn-default closeNotification'>Cancel</button>" +
            "</div>" +
            "</div>" +
            "</div>"
        });

        return;

    });

    $(document).on('click','.closeNotification',function(e){
        notify.close();
    });

    $(document).on('click','.deleteLinkDB',function(e){
        var currentID = $(this).attr('id');
        currentID = currentID.replace('dellink', '');
        var res = currentID.split('_');

        var ur = "/c/"+res[0]+"/subprojectdatabaseconfig/"+res[1]+"/"+res[2]+"/"+res[3]+"/linkeddelete";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                id:res[4],
            },
            success: function(s){
                obj = JSON.parse(s);
                if (obj.result == 'ok'){
                    window.location.reload();
                }
                else if (obj.result == 'error::denied'){
                    alert('Error Processing your Request, You are not permitted to perform this action!');
                    return;
                }else{
                    alert('Error Processing your Request!');
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
            }
        });
    });

    $(".deletelinkspan").click(function(evt) {

        window.scrollTo(0, 0);

        var currentID = $(this).attr('id');
        notify = $.notify({
            title: 'Are you sure you want to delete?',
            message: 'Do you really want to delete the database linked properties?',
            target: '_self',
        },{
            offset: {
                x: 15,
                y: 165
            },
            type: 'danger',
            delay: 0,
            template:  "<div style='width:82%;height:70px;' class='alert  alert-danger'>" +
            "<div class='row pad'>" +
            "<div class='col-md-10 left' style='padding-top: 5px;'><span data-notify='message'>{2}</span></div>" +
            "<div class='col-md-1' style='text-align:right;'>" +
            "<button class='yes btn btn-primary right btn-danger deleteLinkDB' id="+currentID+">Delete</button></div>" +
            "<div class='col-md-1' style='text-align:left; padding-right:70px;'>" +
            "<button class='no btn right btn-default closeNotification'>Cancel</button>" +
            "</div>" +
            "</div>" +
            "</div>"
        });

        return;

    });

    $(document).on('click','#subaddbutton',function(e){
        var splitS = $(e.target).closest("div").attr("id");
        var res = splitS.split('_');
        var ur = "/c/"+res[0]+"/subprojectdatabaseconfig/"+res[1]+"/"+res[2]+"/"+res[3]+"/linkedcreate";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titlecolone:$('#colidone').val(),
                titlecoltwo:$('#colidtwo').val(),
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok')
                {
                    location.reload(true);
                }
                else if (obj.result == 'error::nointeger')
                {
                    alert('Error Processing your Request, Only decimal[s] allowed for density / area!');
                    return;
                }
                else if (obj.result == 'error::duplicate'){
                    alert('Error Processing your Request, Duplicate entries not allowed!');
                    return;
                }
                else if (obj.result == 'error::denied'){
                    alert('Error Processing your Request, You are not permitted to perform this action!');
                    return;
                }
                else
                {
                    alert('Error Processing your Request!');
                    return;
                }
            },
            error: function(e){
                alert('Error Processing your Request!!');
                return;
            }
        });
    });

    $(document).on('click','.subeditbutton',function(e){

        var splitS = $(this).attr("id");

        splitS = splitS.replace("pli",'');
        var res = splitS.split('_');

        var ur = "/c/"+res[0]+"/subprojectdatabaseconfig/"+res[1]+"/"+res[2]+"/"+res[3]+"/linkedcreate";

        var newValue = $(e.target).closest("div").attr("id");

        newValue = newValue.replace('subeditbuttons', '');
        var pr = '#colidone'+newValue;
        var dt = '#colidtwo'+newValue;

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titlecolone:$(pr).val(),
                titlecoltwo:$(dt).val(),
                id:newValue
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    location.reload(true)
                }
                else if (obj.result == 'error::nointeger')
                {
                    alert('Error Processing your Request, Only decimal[s] allowed for density / area!');
                    return;
                }
                else if (obj.result == 'error::duplicate'){
                    alert('Error Processing your Request, Duplicate entries not allowed!');
                    return;
                }
                else if (obj.result == 'error::denied'){
                    alert('Error Processing your Request, You are not permitted to perform this action!');
                    return;
                }
                else
                {
                    alert('Error Processing your Request!');
                    return;
                }
            },
            error: function(e){
                alert('Error Processing your Request!');
                return;
            }
        });

    });

    $(document).on('click','#subcancelbutton',function(e){
        window.location.reload();
    });

});

var x=1;
function appendRow(obj)
{

    var objRepl = obj.replace('Btn', '');
    var d = document.getElementById(objRepl+'Bdy');

    d.innerHTML+='<span class="editable-container editable-inline"><div><div class="editableform-loading" style="display: none;"></div><form class="form-inline editableform" style=""><div class="control-group form-group"><div><div class="editable-input" style="position: relative;"><input id="'+objRepl+'" type="text" class="form-control input-sm" style="padding-right: 24px;"><span class="editable-clear-x"></span></div><div class="editable-buttons"><button id="btnsub'+objRepl+'" type="submit" class="btn btn-primary btn-sm editable-submit"><i class="glyphicon glyphicon-ok"></i> </button><button id="btncan'+objRepl+'" type="button" class="btn btn-default btn-sm editable-cancel"><i class="glyphicon glyphicon-remove"></i> </button></div></div><div class="editable-error-block help-block" style="display: none;"></div></div></form></div></span>';

    var obj = "#"+obj;

    $(obj).prop('disabled', true);

}

function appendLinkRow(obj)
{

    var objRepl = obj.replace('Btn', '');
    var d = document.getElementById(objRepl+'Bdy');
    var htmlLinkStr= '<tr>';
    htmlLinkStr+=       '<td>';
    htmlLinkStr+=             '<div>';
    htmlLinkStr+=                       '<form class="form-inline editableform" style="" novalidate="novalidate">';
    htmlLinkStr+=                           '<div class="control-group form-group">';
    htmlLinkStr+=                                 '<span class="field"><input id="colidone" value="" class="form-control input-sm" style="padding-right: 24px;" /></span>';
    htmlLinkStr+=                                      '&nbsp;<span><b>and</b></span>&nbsp;';
    htmlLinkStr+=                                 '<span class="field"><input id="colidtwo" value="" class="form-control input-sm" style="padding-right: 24px;" /></span>';
    htmlLinkStr+=                            '</div>';

    htmlLinkStr+=                           '<div id="'+objRepl+'" class="editable-buttons">'+
                                                '<button id="subaddbutton" type="submit" class="btn btn-primary btn-sm">'+
                                                '<i class="glyphicon glyphicon-ok"></i> </button>'+
                                                '<button id="subcancelbutton" type="button" class="btn btn-default btn-sm">'+
                                                '<i class="glyphicon glyphicon-remove"></i> </button>'+
                                            '</div>'+
                                        '</form>';
    htmlLinkStr+=             '</div>';
    htmlLinkStr+= '</td>';
    htmlLinkStr+= '</tr>';
    d.innerHTML+= htmlLinkStr;
    $(obj).prop('disabled', true);

}