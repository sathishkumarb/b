jQuery(document).ready(function() {
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.emptytext= '',
    $.fn.editable.defaults.emptyclass= '',
    $('.xedit').editable();
    $(document).on('click','.editable-submit',function(e) {
        var key = $(this).closest('.editable-container').prev().attr('key');
        var x = $(this).closest('.editable-container').prev().attr('id');
        var y = $('.input-sm').val();
        var expVal=null;

        if (key == "systems") {
            var currentID = x;
            currentID = currentID.replace('systems', '');
            var ur = "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId+"/subprojectsystems/" + currentID + "/update";
            var passId = currentID;
        }
        else if (key == "threats") {
            var currentID = x;
            currentID = currentID.replace('threats', '');
            var res = currentID.split('_');
            var ur = "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId+"/" + res[0] + "/subprojectthreats/" + res[1] + "/update";
            var passId = res[1];
        }
        else if (key == "mitigationtechniques") {
            var currentID = x;
            currentID = currentID.replace('mitigationtechniques', '');
            var res = currentID.split('_');
            var ur = "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId+"/" + res[0] + "/subprojectmitigationtechniques/" + res[1] + "/update";
            var passId = res[1];
        }
        else if (key.indexOf("threshold") >= 0) {
            var currentID = x;
            currentID = currentID.replace('threshold', '');
            var res = currentID.split('_');
            var ur = "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId+"/" + res[0] + "/subprojectactivity/" + res[1] + "/updatefields";
            var passId = res[1];
        }
        if (!y){
            $(this).closest('.form-group').addClass('has-error');
            return false;
        }
        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                title:y,
                titleexpression:expVal,
                passid:passId,
                key:key
            },
            success: function(s){
                obj = JSON.parse(s);
                if (obj.result == 'ok'){
                    //location.reload(true);

                    $(this).closest('.editable-container').html(obj.text);
                    $.notify({
                        // options
                        message: 'Data saved',
                    },{
                        // settings
                        type: 'success',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                }
                else if (obj.result == 'notok'){
                    $.notify({
                        // options
                        message: 'Error Processing your Request!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                }
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            }
        });

    });

    $("#add_new_systems").click(function () {
        $("#systemsData").show();
    });

    $(".add_new_threats").click(function () {
        var currentId = $(this).attr('id');
        currentId = currentId.replace('threats', '');
        $("#threatData"+currentId).html('<b>Loading....</b>');
        var ur = "/c/" + projectId + "/subprojectmatrixconfig/" + subprojectId +"/"+currentId+"/subprojectthreatscreate";

        $.ajax({
            url: ur,
            type: 'POST',
            success: function(s){
                $("#threatData"+currentId).html(s);

                $.notify({
                    // options
                    message: 'Data saved!',
                },{
                    // settings
                    type: 'success',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            }
        });

    });

    $(".add_new_mitigationtechniques").click(function () {

        var currentId = $(this).attr('id');
        currentId = currentId.replace('mitigationtechnique', '');
        $("#mitigationtechniqueData"+currentId).html('<b>Loading....</b>');
        var ur = "/c/"+projectId+"/subprojectmatrixconfig/"+subprojectId+"/"+currentId+"/subprojectmitigationtechniquescreate";

        $.ajax({
            url: ur,
            type: 'POST',
            success: function(s){
                $("#mitigationtechniqueData"+currentId).html(s);

                $.notify({
                    // options
                    message: 'Data saved!',
                },{
                    // settings
                    type: 'success',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            }
        });

    });

    var isModelDialogOpen = false;

    $('[data-toggle="modal"]').click(function(e) {
        if (isModelDialogOpen === false) {
            isModelDialogOpen = true;
            e.preventDefault();
            var url = $(this).attr('href');
            //var modal_id = $(this).attr('data-target');
            $.post(url, function (data) {
                $(data).modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }

    });

    $(document).on('click','.close',function(e){
        $('[data-toggle="modal"]').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    });

    $(".edit_db_metric").click(function () {

        var currentId = $(this).attr('id');
        currentId = currentId.replace('editmetric', '');

        var res = currentId.split('_');
        var ur = "/c/" + projectId + "/subprojectmatrixconfig/" + subprojectId +"/"+res[0]+"/subprojectactivity/"+res[1]+"/edit";
        var el = "#editMetricData"+res[0]+"_"+res[1];
        $(el).html('<b>Loading....</b>');

        $.ajax({
            url: ur,
            type: 'GET',
            success: function(s){
                $(el).html(s);

                $.notify({
                    // options
                    message: 'Data saved!',
                },{
                    // settings
                    type: 'success',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
            }
        });

    });

    $(".greenlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('greenlink', '');

        $(".titlegreenexpression"+currentID).hide();
        $(".titlegreenexpressionrange"+currentID).hide();

        $(".titlegreen"+currentID).hide();
        $(".titlegreenrange"+currentID).hide();

        $(".fieldgreentitleexpression"+currentID).show();
        $(".fieldgreentitleexpressionrange"+currentID).show();

        $(".fieldgreen"+currentID).show();
        $(".fieldgreenrange"+currentID).show();
        $("#subeditgreenbuttons"+currentID).show();

    });

    $(".amberlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('amberlink', '');

        $(".titleamberexpression"+currentID).hide();
        $(".titleamberexpressionrange"+currentID).hide();

        $(".titleamber"+currentID).hide();
        $(".titleamberrange"+currentID).hide();

        $(".fieldambertitleexpression"+currentID).show();
        $(".fieldambertitleexpressionrange"+currentID).show();

        $(".fieldamber"+currentID).show();
        $(".fieldamberrange"+currentID).show();
        $("#subeditamberbuttons"+currentID).show();

    });

    $(".redlinkdatadiv").click(function(evt) {

        var currentID = $(this).attr('id');
        currentID = currentID.replace('redlink', '');

        $(".titleredexpression"+currentID).hide();
        $(".titleredexpressionrange"+currentID).hide();

        $(".titlered"+currentID).hide();
        $(".titleredrange"+currentID).hide();

        $(".fieldredtitleexpression"+currentID).show();
        $(".fieldredtitleexpressionrange"+currentID).show();

        $(".fieldred"+currentID).show();
        $(".fieldredrange"+currentID).show();
        $("#subeditredbuttons"+currentID).show();

    });

    $(".delete_systems").click(function(evt) {
        var currentID = $(this).attr('id');
        currentID = currentID.replace('systems', '');
        var ur = "/c/" + projectId + "/subprojectmatrixconfig/" + subprojectId + "/" + currentID + "/subprojectsystems/deleteconfirm";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                id: currentID,
            },
            success: function (s) {

                try {
                    obj = JSON.parse(s);
                } catch (e) {
                    $('.displayError').show();
                    $('.displayError').html(s);
                    return;
                    // not json
                }

                if (obj.result == 'notok') {
                    $.notify({
                        // options
                        message: 'Error Processing your Request!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function (e) {
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });

    });

    $(".delete_threats").click(function(evt) {
        var currentID = $(this).attr('id');
        currentID = currentID.replace('threats', '');
        var res = currentID.split('_');
        var ur = "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId + "/" + res[1] + "/subprojectthreats/deleteconfirm";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                systemid: res[0],
                threatid: res[1],
            },
            success: function (s) {

                try {
                    obj = JSON.parse(s);
                } catch (e) {
                    $('.displayError').show();
                    $('.displayError').html(s);
                    return;
                    // not json
                }
                if (obj.result == 'notok') {
                    $.notify({
                        // options
                        message: 'Error Processing your Request!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function (e) {
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });
    });

    $(".delete_mitigationtechniques").click(function(evt) {
        var currentID = $(this).attr('id');
        currentID = currentID.replace('mitigationtechniques', '');

        var res = currentID.split('_');
        var ur = "/c/" + projectId + "/subprojectmatrixconfig/" + subprojectId + "/" + res[1] + "/subprojectmitigationtechniques/deleteconfirm";

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                threatid: res[0],
                mitigationtechniqueid: res[1],
            },
            success: function (s) {

                try {
                    obj = JSON.parse(s);
                } catch (e) {
                    $('.displayError').show();
                    $('.displayError').html(s);
                    return;
                    // not json
                }

                if (obj.result == 'notok') {
                    $.notify({
                        // options
                        message: 'Error Processing your Request!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function (e) {
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });
    });

    $(document).on('click','.edit-submit-green',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");

        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditgreenbutton', '');
        var res = newValue.split('_');

        var key ="controlgreen";
        var pr = '#selgreenexpression'+res[1];
        var dt = '#txtgreen'+res[1];

        var pri = '#selgreenexpressionrange'+res[1];
        var dti = '#txtgreenrange'+res[1];

        splitS = splitS.replace('subeditgreenbutton', '');
        var res = splitS.split('_');
        var ur = "/c/"+projectId+"/subprojectmatrixconfig/"+subprojectId + "/"+res[0]+"/subprojectactivity/"+res[1]+"/updateexpressionfields";
        var loah = "#greenlink"+res[1];
        if (!$(dt).val()){
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        if (isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){

                    var currentID = obj.passid;

                    $(".titlegreenexpression"+currentID).html(obj.textExpression);
                    $(".titlegreenexpression"+currentID).show();
                    $(".titlegreenexpressionrange"+currentID).html(obj.textRangeExpression);
                    $(".titlegreenexpressionrange"+currentID).show();

                    $(".titlegreen"+currentID).html(obj.text);
                    $(".titlegreen"+currentID).show();
                    $(".titlegreenrange"+currentID).html(obj.textRange);
                    $(".titlegreenrange"+currentID).show();

                    $(".fieldgreentitleexpression"+currentID).hide();
                    $(".fieldgreentitleexpressionrange"+currentID).hide();

                    $(".fieldgreen"+currentID).hide();
                    $(".fieldgreenrange"+currentID).hide();
                    $("#subeditgreenbuttons"+currentID).hide();
                    $.notify({
                        // options
                        message: 'Data saved',
                    },{
                        // settings
                        type: 'success',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                }
                else if (obj.result == 'notok')
                {
                    $.notify({
                        // options
                        message: 'Control activity is "N/A", only text "N/A" accepted as control threshold[s]!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });

    });

    $(document).on('click','.edit-submit-red',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");
        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditredbutton', '');
        var res = newValue.split('_');

        var key ="controlred";
        var pr = '#selredexpression'+res[1];
        var dt = '#txtred'+res[1];

        var pri = '#selredexpressionrange'+res[1];
        var dti = '#txtredrange'+res[1];

        splitS = splitS.replace('subeditredbutton', '');
        var res = splitS.split('_');
        var ur = "/c/"+projectId+"/subprojectmatrixconfig/"+subprojectId + "/"+res[0]+"/subprojectactivity/"+res[1]+"/updateexpressionfields";

        if (!$(dt).val()){
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        if (isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    var currentID = obj.passid;
                    $(".titleredexpression"+currentID).html(obj.textExpression);
                    $(".titleredexpression"+currentID).show();
                    $(".titleredexpressionrange"+currentID).html(obj.textRangeExpression);
                    $(".titleredexpressionrange"+currentID).show();

                    $(".titlered"+currentID).html(obj.text);
                    $(".titlered"+currentID).show();
                    $(".titleredrange"+currentID).html(obj.textRange);
                    $(".titleredrange"+currentID).show();

                    $(".fieldredtitleexpression"+currentID).hide();
                    $(".fieldredtitleexpressionrange"+currentID).hide();

                    $(".fieldred"+currentID).hide();
                    $(".fieldredrange"+currentID).hide();
                    $("#subeditredbuttons"+currentID).hide();
                    $.notify({
                        // options
                        message: 'Data saved',
                    },{
                        // settings
                        type: 'success',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                }
                else if (obj.result == 'notok')
                {
                    $.notify({
                        // options
                        message: 'Control activity is "N/A", only text "N/A" accepted as control threshold[s]!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });

    });

    $(document).on('click','.edit-submit-amber',function(e){
        //var splitS = $(".pGreenLinkEditData").attr("id");
        var splitS= $(this).attr('id');
        var newValue = $(this).attr('id');
        newValue = newValue.replace('subeditamberbutton', '');
        var res = newValue.split('_');

        var key ="controlamber";
        var pr = '#selamberexpression'+res[1];
        var dt = '#txtamber'+res[1];

        var pri = '#selamberexpressionrange'+res[1];
        var dti = '#txtamberrange'+res[1];

        splitS = splitS.replace('subeditamberbutton', '');
        var res = splitS.split('_');
        var ur = "/c/"+projectId+"/subprojectmatrixconfig/"+subprojectId + "/"+res[0]+"/subprojectactivity/"+res[1]+"/updateexpressionfields";

        if ($(dt).val() && isNaN($(dt).val()) && $(dt).val() != "N/A") {
            $(this).closest('.form-group').addClass('has-error');
            e.preventDefault();
            return false;
        }

        $.ajax({
            url: ur,
            type: 'POST',
            data: {   // Data Sending With Request To Server
                titleexpression:$(pr).val(),
                title:$(dt).val(),
                titlerangeexpression:$(pri).val(),
                titlerange:$(dti).val(),
                passid:res[1],
                key: key,
            },
            success: function(s){
                obj = JSON.parse(s);

                if (obj.result == 'ok'){
                    var currentID = obj.passid;
                    $(".titleamberexpression"+currentID).html(obj.textExpression);
                    $(".titleamberexpression"+currentID).show();
                    $(".titleamberexpressionrange"+currentID).html(obj.textRangeExpression);
                    $(".titleamberexpressionrange"+currentID).show();

                    $(".titleamber"+currentID).html(obj.text);
                    $(".titleamber"+currentID).show();
                    $(".titleamberrange"+currentID).html(obj.textRange);
                    $(".titleamberrange"+currentID).show();

                    $(".fieldambertitleexpression"+currentID).hide();
                    $(".fieldambertitleexpressionrange"+currentID).hide();

                    $(".fieldamber"+currentID).hide();
                    $(".fieldamberrange"+currentID).hide();
                    $("#subeditamberbuttons"+currentID).hide();

                    $.notify({
                        // options
                        message: 'Data saved',
                    },{
                        // settings
                        type: 'success',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                }
                else if (obj.result == 'notok')
                {
                    $.notify({
                        // options
                        message: 'Control activity is "N/A", only text "N/A" accepted as control threshold[s]!',
                    },{
                        // settings
                        type: 'danger',

                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
            },
            error: function(e){
                $.notify({
                    // options
                    message: 'Error Processing your Request!',
                },{
                    // settings
                    type: 'danger',

                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                return;
            }
        });

    });

    $(document).on('click','.subcancelbutton',function(e){

        var currentID = $(this).attr('id');
        var newValue = $(this).attr('id');

        if (currentID.indexOf("subcancelredbutton") !== -1) {
            console.log('sa');
            newValue = newValue.replace('subcancelredbutton', '');
            var res = newValue.split('_');
            var currentID = res[1];
            $(".titleredexpression" + currentID).show();
            $(".titleredexpressionrange" + currentID).show();

            $(".titlered" + currentID).show();
            $(".titleredrange" + currentID).show();

            $(".fieldredtitleexpression" + currentID).hide();
            $(".fieldredtitleexpressionrange" + currentID).hide();

            $(".fieldred" + currentID).hide();
            $(".fieldredrange" + currentID).hide();
            $("#subeditredbuttons" + currentID).hide();
        }

        else if (currentID.indexOf("subcancelamberbutton") !== -1) {
            newValue = newValue.replace('subcancelamberbutton', '');
            var res = newValue.split('_');
            var currentID = res[1];
            $(".titleamberexpression" + currentID).show();
            $(".titleamberexpressionrange" + currentID).show();

            $(".titleamber" + currentID).show();
            $(".titleamberrange" + currentID).show();

            $(".fieldambertitleexpression" + currentID).hide();
            $(".fieldambertitleexpressionrange" + currentID).hide();

            $(".fieldamber" + currentID).hide();
            $(".fieldamberrange" + currentID).hide();
            $("#subeditamberbuttons" + currentID).hide();
        }

        else if (currentID.indexOf("subcancelgreenbutton") !== -1) {
            newValue = newValue.replace('subcancelgreenbutton', '');
            var res = newValue.split('_');
            var currentID = res[1];
            $(".titlegreenexpression" + currentID).show();
            $(".titlegreenexpressionrange" + currentID).show();

            $(".titlegreen" + currentID).show();
            $(".titlegreenrange" + currentID).show();

            $(".fieldgreentitleexpression" + currentID).hide();
            $(".fieldgreentitleexpressionrange" + currentID).hide();

            $(".fieldgreen" + currentID).hide();
            $(".fieldgreenrange" + currentID).hide();
            $("#subeditgreenbuttons" + currentID).hide();
        }
    });

});

$(document).on('click',".systemssave", function() {
    if (!$("#aie_bundle_cmsbundle_subprojectsystems_systemstitle").val()) {
        $('input[required]')
            .closest(".form-group")
            .addClass('has-error');
        event.preventDefault();
        return;
    }
});

$(document).on('click',".threatssave", function() {
    if (!$("#aie_bundle_cmsbundle_subprojectthreats_threatstitle").val()) {
        $('input[required]')
            .closest(".form-group")
            .addClass('has-error');
        event.preventDefault();
        return;
    }
});

$(document).on('click',".activitysave", function(){

    var fl = false;

    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_mitigationtechniquestitle").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_subprojectactivtiy_mitigationtechniquestitle').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_managementtitle").val()) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_managementtitle').closest('.form-group').addClass('has-error');
        fl = true;
    }
    if (isNaN($("#aie_bundle_cmsbundle_subprojectactivtiy_frequency").val()) || !$("#aie_bundle_cmsbundle_subprojectactivtiy_frequency").val() || $("#aie_bundle_cmsbundle_subprojectactivtiy_frequency").val() == 0) {
        fl = true;
        $('#aie_bundle_cmsbundle_subprojectactivtiy_frequency').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_activityperson").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_subprojectactivtiy_activityperson').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_weightage").val()) {
        fl = true;
        $('#aie_bundle_cmsbundle_subprojectactivtiy_weightage').closest('.form-group').addClass('has-error');
    }
    if (isNaN($("#aie_bundle_cmsbundle_subprojectactivtiy_weightage").val())) {
        fl = true;
        $('#aie_bundle_cmsbundle_subprojectactivtiy_weightage').closest('.form-group').addClass('has-error');
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_managementtitle").val()) {
        fl = true;
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_managementgreen").val()) {
        fl = true;
    }
    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_managementred").val()) {
        fl = true;
    }

    if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_controltitle").val()) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').prev().addClass('has-error');
    }
    else{
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').removeClass('has-error');
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').prev().removeClass('has-error');
    }


    if ($("#aie_bundle_cmsbundle_subprojectactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen').closest('.form-group').addClass('has-error');
    }
    else {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen').closest('.form-group').removeClass('has-error');
    }

    if ($("#aie_bundle_cmsbundle_subprojectactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_subprojectactivtiy_controlamber").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlamber').closest('.form-group').addClass('has-error');
    }
    else {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlamber').closest('.form-group').removeClass('has-error');
    }

    if ($("#aie_bundle_cmsbundle_subprojectactivtiy_controltitle").val() == "N/A" && $("#aie_bundle_cmsbundle_subprojectactivtiy_controlred").val() != "N/A" ) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlred').closest('.form-group').addClass('has-error');
    }
    else if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_controlred").val()) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlred').closest('.form-group').addClass('has-error');
    }
    else {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlred').closest('.form-group').removeClass('has-error');
    }

    if (($("#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen").val() == "N/A" || $("#aie_bundle_cmsbundle_subprojectactivtiy_controlamber").val() == "N/A" || $("#aie_bundle_cmsbundle_subprojectactivtiy_controlred").val() == "N/A") && $("#aie_bundle_cmsbundle_subprojectactivtiy_controltitle").val() != "N/A") {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').addClass('has-error');
    }
    else if (!$("#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen").val()) {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controlgreen').closest('.form-group').addClass('has-error');
    }
    else {
        $('#aie_bundle_cmsbundle_subprojectactivtiy_controltitle').closest('.form-group').removeClass('has-error');
    }

    if (fl == true)
    {

        $('#aie_bundle_cmsbundle_subprojectactivtiy_managementtitle').closest('.form-group').prev().addClass('has-error');
        $('#aie_bundle_cmsbundle_subprojectactivtiy_managementgreen').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_subprojectactivtiy_managementred').closest('.form-group').addClass('has-error');


        $('#aie_bundle_cmsbundle_subprojectactivtiy_subprojectmetrics').closest('.form-group').addClass('has-error');
        $('#aie_bundle_cmsbundle_subprojectactivtiy_subprojectdatasheet').closest('.form-group').addClass('has-error');

        $('.modal-footer').html('<label><font color="#8b0000"> * Please check all mandatory and valid inputs (numbers)</font></label>');
        event.preventDefault();
        return;

    }
    else
    {
        $(this).val($('#aie_bundle_cmsbundle_subprojectactivtiy_mitigationtechniquestitle').val());
        return;
    }

});

$(document).on('change',".elementMetrics", function(){

    var val = $(this).val();
    $.ajax({
        type: "POST",
        url: "/c/"+projectId+"/subprojectmatrixconfig/" +subprojectId + "/ajaxloaddatasheet?metrics_id=" + val,
        success: function(data) {
            // Remove current options
            $('.elementDatasheet').html('');
            $.each(data, function(k, v) {
                $('.elementDatasheet').append('<option value="' + v + '">' + k + '</option>');
            });
        }
    });
    return false;

});



