$(document).ready(function () {

    //get which software is used
    var documentTag = '#documents-explorer';
    var de = $(documentTag);
    var software = null;
    var p_id = null;


    var urls = {
        'files_categories': 'files_categories',
        'files_categories_delete': 'files_categories_delete',
        'files_categories_move': 'files_categories_move',
        'files_move': 'files_move',
        'files_delete': 'files_delete',
        'files_categories_update': 'files_categories_update',
        'files_edit': 'files_edit',
        'files_categories_create': 'files_categories_create'
    };

    var urlData = {};

    $.each($('body').attr('class').split(/\s+/), function (index, item) {
        switch (item) {
            case 'anomaly':
                software = 'anomaly';
                p_id = de.attr('ref-id');
                urlData = {refId: p_id};
                return false;
            case 'pipeline':
                software = 'pipeline';
                p_id = de.attr('pipeline-id');
                urlData = {pipelineId: p_id};
                return false;
            case 'cms':
                software = 'cms';
                p_id = de.attr('subproject-id');
                p_id = p_id.split("_");
                urlData = {projectId: p_id[0],subprojectId: p_id[1]};
                return false;
            default:
                return true; //continue
        }
    });

    $.each(urls, function (index, item) {
        urls[index] = software + '_' + item;
    });


    var filesTreeurl = Routing.generate(urls['files_categories'], urlData, true);

    var CLIPBOARD = null;

    function addFolder(node) {
        var folder = {
            title: "New Folder",
            isNew: true,
            folder: true
        };
        var refNode = node.addChildren(folder);
        node.setExpanded();
        refNode.editStart();
    }

    function deleteFolder(node) {
        var id = node.data.id;
        var deleteUrl = Routing.generate(urls['files_categories_delete'], $.extend({}, urlData, {id: id}), true);

        var result = confirm("Are you sure to delete the selected folder?");
        if (result) {
            //create
            $.ajax({
                url: deleteUrl,
                type: 'DELETE',
                dataType: 'json',
                success: function (response) {
                    if (response.success && response.status===200) {
                        node.remove();
                    }
                    location.reload();
                 },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status&&xhr.status===404){
                        alert("Error processing your request!");
                    }
                }
            });
        }
    }

    function moveFolder(node) {
        var id = node.data.id;
        var parent = node.getParent();
        var parent_id = (parent === de.fancytree("getRootNode")) ? null : parent.data.id;

        var moveUrl = Routing.generate(urls['files_categories_move'], $.extend({}, urlData, {id: id}), true);

        //create
        $.ajax({
            url: moveUrl,
            type: 'POST',
            data: {parent: parent_id},
            success: function (response) {
                if (response.success) {
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if(xhr.status&&xhr.status===404){
                    alert("Error processing your request!");
                }
            }
        });

    }

    function moveFile(node) {
        var id = node.data.id;
        var parent = node.getParent();
        var parent_id = (parent === de.fancytree("getRootNode")) ? null : parent.data.id;

        var moveUrl = Routing.generate(urls['files_move'], $.extend({}, urlData, {id: id}), true);

        //create
        $.ajax({
            url: moveUrl,
            type: 'POST',
            data: {parent: parent_id},
            success: function (response) {
                if (response.success) {
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if(xhr.status && xhr.status===404){
                    alert("Error processing your request!");
                }
            }
        });

    }

    function deleteFile(node) {

        var id = node.data.id;
        var parent = node.getParent();
        var parent_id = (parent === de.fancytree("getRootNode")) ? null : parent.data.id;

        var deleteUrl = Routing.generate(urls['files_delete'], $.extend({}, urlData, {id: id}), true);

        var result = confirm("Are you sure to delete, the selected file could have used in other place?");
        if (result) {
            //delete
            $.ajax({
                url: deleteUrl,
                type: 'DELETE',
                dataType: 'json',
                data: {parent: parent_id},
                success: function (response) {
                    if (response.success && response.status===200) {
                        node.remove();
                    }
                    location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if(xhr.status && xhr.status===404){
                        alert("Error processing your request!");
                    }
                }
            });
        }

    }

    de.fancytree({
        autoCollapse: true, // Automatically collapse all siblings, when a node is expanded.
        selectMode: 1, // 1:single, 2:multi, 3:multi-hier
        source: {
            url: filesTreeurl,
            cache: false
        },
        init: function (e, data) {
            //Sort it alphabetically after it is loaded
            $(documentTag).fancytree("getRootNode").sortChildren(null, true);
        },
        activate: function (event, data) {
            var node = data.node;
            // Use <a> href and target attributes to load the content:
            if (node.data.absolutepath) {
                // Open target
                //window.open(node.data.absolutepath, '_blank');
            }
        },
        click: function(event, data)
        {
            var node = data.node;
            if (node.data.absolutepath) {
                // Open target
                window.open(node.data.absolutepath, '_blank');
            }
        },
        titlesTabbable: true,     // Add all node titles to TAB chain
        extensions: ["edit", "dnd", "table", "gridnav", "filter"],
        filter: {
            mode: "hide"
        }
        ,
        dnd: {
            preventVoidMoves: true,
            preventRecursiveMoves: true,
            autoExpandMS: 400,
            dragStart: function (node, data) {
                return true;
            }

            ,
            dragEnter: function (node, data) {
                /** data.otherNode may be null for non-fancytree droppables.
                 *  Return false to disallow dropping on node. In this case
                 *  dragOver and dragLeave are not called.
                 *  Return 'over', 'before, or 'after' to force a hitMode.
                 *  Return ['before', 'after'] to restrict available hitModes.
                 *  Any other return value will calc the hitMode from the cursor position.
                 */
                // Prevent dropping a parent below another parent (only sort
                // nodes under the same parent)
                /*           if(node.parent !== data.otherNode.parent){
                 return false;
                 }
                 // Don't allow dropping *over* a node (would create a child)
                 return ["before", "after"];*/

                if (!node.isFolder()) {
                    //Allow dropping only on folders
                    return false;
                }
                return true;
            }
            ,
            dragDrop: function (node, data) {


                //node -> targeted Folder
                //otherNode -> dragged Item
                data.otherNode.moveTo(node, data.hitMode);
                if (data.otherNode.isFolder()) {
                    moveFolder(data.otherNode);
                } else {
                    moveFile(data.otherNode);
                }

            }
        }
        ,
        edit: {
            triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"],
            beforeClose: function (event, data) {
                // Return false to prevent cancel/save (data.input is available)
            }

            ,
            beforeEdit: function (event, data) {
                if (!data.node.isFolder()) {
                    return false;
                }
                // Return false to prevent edit mode
            }
            ,
            close: function (event, data) {
                // Editor was removed
            }
            ,
            edit: function (event, data) {
                // Editor was opened (available as data.input)
            }
            ,
            save: function (event, data) {
                var node = data.node;
                var name = data.input.val();

                if (node.isFolder()) {
                    if (node.data.id) {
                        //old folder
                        var updateUrl = Routing.generate(urls['files_categories_update'], $.extend({}, urlData, {id: node.data.id}), true);
                        //update
                        $.ajax({
                            url: updateUrl,
                            type: 'PUT',
                            data: {name: name},
                            success: function (response) {
                                //...
                            }
                        });

                    } else {
                        //new folder
                        var createUrl = Routing.generate(urls['files_categories_create'], urlData, true);
                        var parent = node.getParent();
                        var parent_id = null;

                        if (parent) {
                            parent_id = parent.data.id;
                        }
                        //create
                        $.ajax({
                            url: createUrl,
                            type: 'POST',
                            data: {name: name, parent: parent_id},
                            success: function (response) {
                                if (response.success) {
                                    var id = response.id;
                                    node.data.id = id;
                                }
                            }
                        });
                        //refresh
                    }
                }
            }
        }
        ,
        table: {
            indentation: 20,
            nodeColumnIdx: 1
        }
        ,
        gridnav: {
            autofocusInput: false,
            handleCursorKeys: true
        }
        ,

        renderColumns: function (event, data) {
            var node = data.node,
                $list = $("<ul />"),
                $tdList = $(node.tr).find(">td");

            // (index #0 is rendered by fancytree by adding the checkbox)
            if (node.isFolder()) {
                // make the title cell span the remaining columns, if it is a folder:
                $tdList.eq(2)
                    .prop("colspan", 6)
                    .nextAll().remove();
            }

            $tdList.eq(0).text(node.getIndexHier()).addClass("alignRight");
            // (index #2 is rendered by fancytree)
            // $tdList.eq(3).text(node.key);
            $tdList.eq(2).text(node.data.caption);
            $tdList.eq(3).text(node.data.company);
            $tdList.eq(4).text(node.data.documentNumber);
            var docDate = (node.data.documentDate) ? node.data.documentDate : '';
            $tdList.eq(5).text(docDate);
            $tdList.eq(6).text(node.data.date);
        }
    }).on("nodeCommand", function (event, data) {
        // Custom event handler that is triggered by keydown-handler and
        // context menu:

        var refNode, moveMode,
            tree = $(this).fancytree("getTree"),
            node = tree.getActiveNode();

        switch (data.cmd) {
            case 'view':
                window.open(node.data.absolutepath, '_blank');
                break;
            case 'edit':
                var editUrl = Routing.generate(urls['files_edit'], $.extend({}, urlData, {id: node.data.id}), true);
                window.location.href = editUrl;
                break;
            case "moveUp":
                node.moveTo(node.getPrevSibling(), "before");
                node.setActive();
                break;
            case "moveDown":
                node.moveTo(node.getNextSibling(), "after");
                node.setActive();
                break;
            case "indent":
                refNode = node.getPrevSibling();
                node.moveTo(refNode, "child");
                refNode.setExpanded();
                node.setActive();
                break;
            case "outdent":
                node.moveTo(node.getParent(), "after");
                node.setActive();
                break;
            case "rename":
                node.editStart();
                break;
            case "remove":
                deleteFolder(node);
                break;
            case "removefile":
                deleteFile(node);
                break;
            case "addChild":
                addFolder(node);
                break;
            default:
                alert("Unhandled command: " + data.cmd);
                return;
        }

    }).on("keydown", function (e) {
        var c = String.fromCharCode(e.which),
            cmd = null;

        if (c === "N" && e.ctrlKey && e.shiftKey) {
            cmd = "addChild";
        } else if (c === "C" && e.ctrlKey) {
            cmd = "copy";
        } else if (c === "V" && e.ctrlKey) {
            cmd = "paste";
        } else if (c === "X" && e.ctrlKey) {
            cmd = "cut";
        } else if (c === "N" && e.ctrlKey) {
            cmd = "addSibling";
        } else if (e.which === $.ui.keyCode.DELETE) {
            cmd = "remove";
        } else if (e.which === $.ui.keyCode.F2) {
            cmd = "rename";
        } else if (e.which === $.ui.keyCode.UP && e.ctrlKey) {
            cmd = "moveUp";
        } else if (e.which === $.ui.keyCode.DOWN && e.ctrlKey) {
            cmd = "moveDown";
        } else if (e.which === $.ui.keyCode.RIGHT && e.ctrlKey) {
            cmd = "indent";
        } else if (e.which === $.ui.keyCode.LEFT && e.ctrlKey) {
            cmd = "outdent";
        }
        if (cmd) {
            $(this).trigger("nodeCommand", {cmd: cmd});
            return false;
        }
    });
    var tree = $(documentTag).fancytree("getTree");

    $('#create-folder').click(function () {
        var node = de.fancytree("getRootNode");
        addFolder(node);
        return false;
    });

    /*
     * Context menu (https://github.com/mar10/jquery-ui-contextmenu)
     */
    var fileMenu = [
        {title: "View", cmd: "view", uiIcon: "ui-icon-trash"},
        {title: "Edit", cmd: "edit", uiIcon: "ui-icon-pencil"},
        {title: "Delete", cmd: "removefile", uiIcon: "ui-icon-trash"}
    ];
    var folderMenu = [
        {title: "Edit", cmd: "rename", uiIcon: "ui-icon-pencil"},
        {title: "Delete", cmd: "remove", uiIcon: "ui-icon-trash"},
        {title: "----"},
        {title: "New Sub-Folder", cmd: "addChild", uiIcon: "ui-icon-plus"}
    ];
    $(documentTag).contextmenu({
        delegate: "span.fancytree-title",
        menu: [],
        beforeOpen: function (event, ui) {
            var node = $.ui.fancytree.getNode(ui.target);

            if (!node.isFolder()) {
                $(this).contextmenu("replaceMenu", fileMenu);
            } else {
                $(this).contextmenu("replaceMenu", folderMenu);
            }
            node.setActive();
        },
        select: function (event, ui) {
            var that = this;
            // delay the event, so the menu can close and the click event does
            // not interfere with the edit control
            setTimeout(function () {
                $(that).trigger("nodeCommand", {cmd: ui.cmd});
            }, 100);
        }
    });


    $("#documents_form").submit(false);
    $("input[name=document_filter]").keyup(function (e) {
        var n,
            leavesOnly = false,
            match = $(this).val();

        if (e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === "") {
            $("input[name=search]").val("");
            $("span#matches").text("");
            tree.clearFilter();
            return;
        }
        n = tree.filterNodes(match, leavesOnly);
        $("span#matches").text("(" + n + " matches)");
    }).focus();

    $('#btnResetSearch').click(function () {
        $("#document_filter").val("");
        $("span#matches").text("");
        tree.clearFilter();
    });
})
;