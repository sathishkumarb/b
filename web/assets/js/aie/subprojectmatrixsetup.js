$(document).ready(function() {

    var projectId = '{{ projectId }}';
    var subprojectId = '{{ subprojectId }}';

    $(window).scroll(function () {

        if ($(this).scrollTop() > 100) {

            $('#fixedheaderFlyout').css('top',50);
            $('#fixedheaderFlyout').addClass('fixed');

        } else {
            $('#fixedheaderFlyout').css('top',0);
            $('#fixedheaderFlyout').removeClass('fixed');
        }
    });

    $(".systems").change(function (e) {

        var id = $(this).closest('.systems').find('p').attr('id');
        var ids = id.split("_");
        var systemStr = '#systems_' + ids[1] + "_" + ids[3];

        var pStr = '#psystems_' + ids[1] + "_" + ids[2] + "_" + ids[3];

        var iss= $(".spaThreat").attr('id');
        var isds = iss.split("_");

        for( i=0; i <= isds.length; i++ )
        {
            var threatStr = '#threats_' + ids[1] + "_" + isds[i]+ "_" + ids[3];
            if ($(threatStr).attr("checked")) {
                $(systemStr).attr('checked', true);
                e.preventDefault();
                $.notify({
                    // options
                    message: 'Please select/unselect in the order of systems, threats, mitigation and activities and vice versa!',
                },{
                    // settings
                    type: 'danger',
                    placement: {
                        from: "top",
                        align: "center"
                    }
                });
                // $('body').scrollTop(0);
                // $(".displayError").addClass('alert alert-danger');
                // $(".displayError").html('Please select / unselect in the order of systems,threats,mitigation and activities and vice versa!');
                return;
            }
        }
        $(".displayError").html('');

    });

    $(".threats").click(function (e) {
        var id = $(this).closest('.threats').find('p').attr('id');
        var ids = id.split("_");
        var systemStr = '#systems_' + ids[1] + "_" + ids[4];
        var threatStr = '#threats_' + ids[1] + "_" + ids[2]+ "_" + ids[4];
        var pStr = '#pthreats_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4];

        if (!$(systemStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick system');
            $.notify({
                // options
                message: 'Please tick system!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else
        {
            var iss = $(".spaMitigation").attr('id');
            var isds = iss.split("_");
            for( i=0; i <= isds.length; i++ ){
                var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + isds[i] + "_" + ids[4];
                if ($(mitigationStr).attr("checked")) {
                    $(threatStr).attr('checked', true);
                    e.preventDefault();
                    $.notify({
                        // options
                        message: 'Please select/unselect in the order of systems, threats, mitigation and activities and vice versa!',
                    },{
                        // settings
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    // $('body').scrollTop(0);
                    // $(".displayError").addClass('alert alert-danger');
                    // $(".displayError").html('Please select / unselect in the order of systems,threats,mitigation and activities and vice versa!');
                    return;
                }
            }
            //$(".displayError").removeClass('alert alert-danger');
            $(pStr).html('');
        }

    });

    $(".mitigationtechniques").click(function (e) {

        var id = $(this).closest('.mitigationtechniques').find('p').attr('id');
        var ids = id.split("_");
        var threatStr = '#threats_' + ids[1] + "_" + ids[2] + "_" + ids[5];
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var pStr = '#pmitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(threatStr).attr("checked"))
        {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick threat and system');

            $.notify({
                // options
                message: 'Please tick threat and system!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else
        {
            var iss = $(".spaActivity").attr('id');
            var isds = iss.split("_");
            for( i=0; i <= isds.length; i++ ){
                var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                var commentsStr = '#comments_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + isds[i] + "_" + ids[5];
                if ($(controlStr).attr("checked")) {
                    $(mitigationStr).attr('checked', true);
                    e.preventDefault();
                    $.notify({
                        // options
                        message: 'Please select/unselect in the order of systems, threats, mitigation techniques and activities and vice versa!',
                    },{
                        // settings
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    // $('body').scrollTop(0);
                    // $(".displayError").addClass('alert alert-danger');
                    // $(".displayError").html('Please select / unselect in the order of systems,threats,mitigation and activities and vice versa!');
                    return;
                }
                if ($(managementStr).attr("checked")) {
                    $(mitigationStr).attr('checked', true);
                    e.preventDefault();
                    $.notify({
                        // options
                        message: 'Please select/unselect in the order of systems, threats, mitigation techniques and activities and vice versa!',
                    },{
                        // settings
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    // $('body').scrollTop(0);
                    // $(".displayError").addClass('alert alert-danger');
                    // $(".displayError").html('Please select / unselect in the order of systems, threats, mitigation and activities and vice versa!');
                    return;
                }

                if ($(nonmonitoringkpiStr).attr("checked")) {
                    $(mitigationStr).attr('checked', true);
                    e.preventDefault();
                    // $('body').scrollTop(0);
                    // $(".displayError").addClass('alert alert-danger');
                    // $(".displayError").html('Please select / unselect in the order of systems,threats,mitigation and activities and vice versa!');
                    $.notify({
                        // options
                        message: 'Please select/unselect in the order of systems, threats, mitigation techniques and activities and vice versa!',
                    },{
                        // settings
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "center"
                        }
                    });
                    return;
                }
                $(commentsStr).val('');
            }
            // $(".displayError").removeClass('alert alert-danger');
            // $(".displayError").html('');
        }

    });

    $(".managementactivities").click(function (e) {

        var id = $(this).closest('.managementactivities').find('input:checkbox').attr('id');
        var ids = id.split("_");
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(mitigationStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick mitigation and threat');

            $.notify({
                // options
                message: 'Please tick mitigation and threat!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else if ($(nonmonitoringkpiStr).attr("checked"))
        {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Cannot select activity, since non-KPI selected!');
            $.notify({
                // options
                message: 'Cannot select activity, since non-KPI selected!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        // else
        // {
        //     $(".displayError").removeClass('alert alert-danger');
        //     $(".displayError").html('');
        // }

    });

    $(".controlactivities").click(function (e) {

        var id = $(this).closest('.controlactivities').find('input:checkbox').attr('id');
        var ids = id.split("_");
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(mitigationStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick mitigation and threat');

            $.notify({
                // options
                message: 'Please tick mitigation and threat!',

            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else if ($(nonmonitoringkpiStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Cannot select activity, since non-KPI selected!');
            $.notify({
                // options
                message: 'Cannot select activity, since non-KPI selected!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }

        // else
        // {
        //     $(".displayError").removeClass('alert alert-danger');
        //     $(".displayError").html('');
        // }

    });

    $(".nonmonitoringkpi").click(function (e) {

        var id = $(this).closest('.nonmonitoringkpi').find('input:checkbox').attr('id');
        var ids = id.split("_");
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(mitigationStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick mitigation and threat');

            $.notify({
                // options
                message: 'Please tick mitigation and threat!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else if ($(managementStr).attr("checked") || $(controlStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Cannot select non-KPI, since activity is selected!');

            $.notify({
                // options
                message: 'Cannot select non-KPI, since activity is selected!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        // else
        // {
        //     //$(".displayError").removeClass('alert alert-danger');
        //     //$(".displayError").html('');
        // }

    });

    $(".flowassurancemanagement").click(function (e) {

        var id = $(this).closest('.flowassurancemanagement').find('input:checkbox').attr('id');
        var ids = id.split("_");
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(mitigationStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick mitigation and threat');

            $.notify({
                // options
                message: 'Please tick mitigation and threat!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else if ($(nonmonitoringkpiStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Cannot select non-KPI, since activity is selected!');

            $.notify({
                // options
                message: 'Cannot select Flow Assurance Management, since non-KPI is selected!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }

        // else
        // {
        //     //$(".displayError").removeClass('alert alert-danger');
        //     //$(".displayError").html('');
        // }

    });

    $(".flowassurancecontrol").click(function (e) {

        var id = $(this).closest('.flowassurancecontrol').find('input:checkbox').attr('id');
        var ids = id.split("_");
        var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
        var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceManagementStr = '#flowassurancemanagement_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var flowassuranceControlStr = '#flowassurancecontrol_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
        var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

        if (!$(mitigationStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Please tick mitigation and threat');

            $.notify({
                // options
                message: 'Please tick mitigation and threat!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }
        else if ($(nonmonitoringkpiStr).attr("checked")) {
            e.preventDefault();
            // $('body').scrollTop(0);
            // $(".displayError").addClass('alert alert-danger');
            // $(".displayError").html('Cannot select non-KPI, since activity is selected!');

            $.notify({
                // options
                message: 'Cannot select Flow Assurance Control, since non-KPI is selected!',
            },{
                // settings
                type: 'danger',
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        }

        // else
        // {
        //     //$(".displayError").removeClass('alert alert-danger');
        //     //$(".displayError").html('');
        // }

    });

});

$(document).on('click','#subcancelbutton',function(e){
    window.location.reload();
});

$(document).on('click','.expand',function(e){
    var toggleElement = "." + $(this).attr('id');
    var toggleElementId = "#" + $(this).attr('id');

    var splitStr = toggleElementId.split("_");
    var matchFirst = "." + "di_" +splitStr[1];

    $( toggleElement ).slideToggle(function() {
        if ($(toggleElement).is(':visible')) {
            $(matchFirst).css("background-color","white");
            $(toggleElementId).toggleClass('glyphicon-minus-sign', true);
            $(toggleElementId).toggleClass('glyphicon-plus-sign', false);
        }
        else{
            $(matchFirst).css("background-color","#ccc");
            $(toggleElementId).toggleClass('glyphicon-minus-sign', false);
            $(toggleElementId).toggleClass('glyphicon-plus-sign', true);
        }
    });

});

$(document).on('keydown', "textarea",function (e) {
    var dsd = $(this).attr('id');
    var ids = dsd.split("_");

    var systemStr = '#systems_' + ids[1] + "_" + ids[5];
    var threatStr = '#threats_' + ids[1] + "_" + ids[2]+ "_" + ids[5];
    var mitigationStr = '#mitigationtechniques_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[5];
    var controlStr = '#controlactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
    var managementStr = '#managementactivities_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
    var nonmonitoringkpiStr = '#nonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];
    var pStr = '#pnonmonitoringkpi_' + ids[1] + "_" + ids[2] + "_" + ids[3] + "_" + ids[4] + "_" + ids[5];

    if (!$(threatStr).attr("checked") || !$(mitigationStr).attr("checked") || !$(systemStr).attr("checked")) {
        //$('body').scrollTop(0);
        //$(".displayError").addClass('alert alert-danger');
        //$(".displayError").html('Please tick all required systems, threats, mitigation and activity or non-KPI to enter comments');

        $.notify({
            // options
            message: 'Please tick all required systems, threats, mitigation and activity or non-KPI to enter comments',
        },{
            // settings
            type: 'danger',
            placement: {
                from: "top",
                align: "center"
            }
        });
        e.preventDefault()
        return;
    }
    else
    {
        //$(".displayError").removeClass('alert alert-danger');
        //$(".displayError").html('');
        $(this).val();
    }

});