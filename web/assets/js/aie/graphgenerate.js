function single( container, passdata, flag, xaxisstr, yaxisstr, con, activityname, minRate, maxRate, fromLoadDate, toLoadDate, reddata, greendata, amberdata)
{

	if(minRate == "N/A")
	{
		var minRate = 0;
	}

	if(maxRate == "N/A")
	{
		var minRate = 0;
	}

	if ( passdata.empty != 'true' && passdata != '[]' )
	{

		var startDate = new Date(passdata[passdata.length - 1][0]), // Get year of last data point
			minRate = parseFloat(minRate),
			maxRate = parseFloat(maxRate),
			startPeriod,
			date,
			rate,
			index;

		startDate.setMonth(startDate.getMonth() - 3); // a quarter of a year before last data point
		startPeriod = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

		for (index = passdata.length - 1; index >= 0; index = index - 1) {
			date = passdata[index][0]; // data[i][0] is date
			rate = passdata[index][1]; // data[i][1] is exchange rate
			if (date < startPeriod) {
				break; // stop measuring highs and lows
			}
		}

        if (amberdata.length > 0) var amberThresholdVisible = true; else var amberThresholdVisible = false;

        if (reddata.length > 0) var redThresholdVisible = true; else var redThresholdVisible = false;

        if (greendata.length > 0) var greenThresholdVisible = true; else var greenThresholdVisible = false;

        var frmonth = new Date(fromLoadDate).getMonth();
        var fryear = new Date(fromLoadDate).getFullYear();
        var tomonth = new Date(toLoadDate).getMonth();
        var toyear = new Date(toLoadDate).getFullYear();

        var frdate = new Date(fromLoadDate).getDate();
        var todate = new Date(toLoadDate).getDate();

        if (frmonth === tomonth && fryear === toyear){
            fromLoadDate = null;
            toLoadDate = null;
        }

		if (minRate && maxRate) {

			if ( fromLoadDate && toLoadDate){
			    
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load:function(){

                                if ( !this.series[0].data.length)
                                {
                                    //console.log(new Date(fromLoadDate).getMonth() + "==" + new Date(fromLoadDate).getFullYear() + "==" + new Date(toLoadDate).getMonth() + "==" + new Date(toLoadDate).getFullYear())

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        //minTickInterval: 28 * 24 * 3600 * 1000,
                        //
                        min: Date.UTC(fryear, frmonth-1, frdate),
                        max: Date.UTC(toyear, tomonth-1, todate),

                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },

                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        enabled: true,
                        title: {
                            text: activityname,
                            useHTML: true,
                        },
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: activityname,
                        data: passdata,
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata,
                        color: 'green',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata,
                        color: 'red',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata,
                        color: '#FFC200',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    tooltip: {
                        formatter: function () {
                            var s = '<b>' + Highcharts.dateFormat('%e-%b-%Y',
                                new Date(this.x)) + '</b>';

                            $.each(this.points, function () {
                                s += '<br/>' + this.series.name + ': ' +
                                    this.y;
                            });

                            return s;
                        },
                        shared: true
                    },

                    credits: {
                        enabled: false
                    },
                });
            }
            else {

                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load: function () {

                                if (!this.series[0].data.length) {
                                    //console.log(new Date(fromLoadDate).getMonth() + "==" + new Date(fromLoadDate).getFullYear() + "==" + new Date(toLoadDate).getMonth() + "==" + new Date(toLoadDate).getFullYear())

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },

                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        enabled: true,
                        title: {
                            text: activityname,
                            useHTML: true,
                        },
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: activityname,
                        data: passdata,
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata,
                        color: 'green',
                        zones: [{
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata,
                        color: 'red',
                        zones: [{
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata,
                        color: '#FFC200',
                        zones: [{
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    tooltip: {
                        formatter: function () {
                            var s = '<b>' + Highcharts.dateFormat('%e-%b-%Y',
                                new Date(this.x)) + '</b>';

                            $.each(this.points, function () {
                                s += '<br/>' + this.series.name + ': ' +
                                    this.y;
                            });

                            return s;
                        },
                        shared: true
                    },

                    credits: {
                        enabled: false
                    },
                });
            }

		}
		else
		{
            if ( fromLoadDate && toLoadDate)
            {

                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        min: Date.UTC(fryear, frmonth - 1, frdate),
                        max: Date.UTC(toyear, tomonth - 1, todate),
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: activityname,
                            useHTML: true,
                        },
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: activityname,
                        data: passdata,
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }],

                    tooltip: {
                        formatter: function () {
                            var s = '<b>' + Highcharts.dateFormat('%e-%b-%Y',
                                new Date(this.x)) + '</b>';

                            $.each(this.points, function () {
                                s += '<br/>' + this.series.name + ': ' +
                                    this.y;
                            });

                            return s;
                        },
                        shared: true
                    },

                    credits: {
                        enabled: false
                    },
                });
            }
            else
            {

                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: activityname,
                            useHTML: true,
                        },
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: activityname,
                        data: passdata,
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }],

                    tooltip: {
                        formatter: function () {
                            var s = '<b>' + Highcharts.dateFormat('%e-%b-%Y',
                                new Date(this.x)) + '</b>';

                            $.each(this.points, function () {
                                s += '<br/>' + this.series.name + ': ' +
                                    this.y;
                            });

                            return s;
                        },
                        shared: true
                    },

                    credits: {
                        enabled: false
                    },
                });
            }
		}

	}

}

function double( container, seriesFirstData, seriesSecondData, seriesFirstStr, seriesSecondStr, data, yaxisstr, xaxisstr, fromLoadDate, toLoadDate, minRate, maxRate, reddata, greendata, amberdata )
{

	if(minRate == "N/A")
	{
		var minRate = 0;
	}

	if(maxRate == "N/A")
	{
		var minRate = 0;
	}

    var frmonth = new Date(fromLoadDate).getMonth();
    var fryear = new Date(fromLoadDate).getFullYear();
    var tomonth = new Date(toLoadDate).getMonth();
    var toyear = new Date(toLoadDate).getFullYear();

    var frdate = new Date(fromLoadDate).getDate();
    var todate = new Date(toLoadDate).getDate();

    if (frmonth === tomonth && fryear === toyear){
        fromLoadDate = null;
        toLoadDate = null;
    }
    else{
        var fromLoadDate = parseInt(fromLoadDate);
        var toLoadDate = parseInt(toLoadDate);
    }

    if (frmonth === tomonth && fryear === toyear){
        fromLoadDate = null;
        toLoadDate = null;
    }

	if (seriesFirstData.empty != 'true' && seriesSecondData.empty != 'true' && data != '[]' && data.length > 0)
	{

		var startDate = new Date(data[data.length - 1][0]), // Get year of last data point
			minRate = parseFloat(minRate),
			maxRate = parseFloat(maxRate),
			startPeriod,
			date,
			rate,
			index;

		startDate.setMonth(startDate.getMonth() - 3); // a quarter of a year before last data point
		startPeriod = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

		for (index = data.length - 1; index >= 0; index = index - 1) {
			date = data[index][0]; // data[i][0] is date
			rate = data[index][1]; // data[i][1] is exchange rate
			if (date < startPeriod) {
				break; // stop measuring highs and lows
			}

		}

        if (amberdata.length > 0) var amberThresholdVisible = true; else var amberThresholdVisible = false;

        if (reddata.length > 0) var redThresholdVisible = true; else var redThresholdVisible = false;

        if (greendata.length > 0) var greenThresholdVisible = true; else var greenThresholdVisible = false;

        var frmonth = new Date(fromLoadDate).getMonth();
        var fryear = new Date(fromLoadDate).getFullYear();
        var tomonth = new Date(toLoadDate).getMonth();
        var toyear = new Date(toLoadDate).getFullYear();

        var frdate = new Date(fromLoadDate).getDate();
        var todate = new Date(toLoadDate).getDate();

        if (frmonth === tomonth && fryear === toyear){
            fromLoadDate = null;
            toLoadDate = null;
        }

		if (minRate && maxRate) {

			if (fromLoadDate && toLoadDate)
			{
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load:function(){

                                if ( !this.series[0].data.length)
                                {
                                    //console.log(new Date(fromLoadDate).getMonth() + "==" + new Date(fromLoadDate).getFullYear() + "==" + new Date(toLoadDate).getMonth() + "==" + new Date(toLoadDate).getFullYear())

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        min: Date.UTC(fryear, frmonth-1, frdate),
                        max: Date.UTC(toyear, tomonth-1, todate),
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        showEmpty: false,

                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata,
                        color: 'green',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata.sort(),
                        color: 'red',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata.sort(),
                        color: '#FFC200',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    credits: {
                        enabled: false
                    },
                });
            }
            else
            {
// Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load:function(){

                                if ( !this.series[0].data.length)
                                {
                                    //console.log(new Date(fromLoadDate).getMonth() + "==" + new Date(fromLoadDate).getFullYear() + "==" + new Date(toLoadDate).getMonth() + "==" + new Date(toLoadDate).getFullYear())

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        showEmpty: false,

                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata.sort(),
                        color: 'green',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata.sort(),
                        color: 'red',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata,
                        color: '#FFC200',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    credits: {
                        enabled: false
                    },
                });
			}

		}
		else
		{
			if(fromLoadDate && toLoadDate)
			{
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        min: Date.UTC(fryear, frmonth-1, frdate),
                        max: Date.UTC(toyear, tomonth-1, todate),
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        }
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }],
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML:true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    },],

                    credits: {
                        enabled: false
                    },
                });
			}
			else
			{
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        }
                    },

                    yAxis: {
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }],
                    },

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML:true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    },],

                    credits: {
                        enabled: false
                    },
                });
			}

		}
	}
}

function triple( container, seriesFirstData, seriesSecondData, seriesThirdData, seriesFirstStr, seriesSecondStr, seriesThirdStr, data, yaxisstr, xaxisstr, fromLoadDate, toLoadDate, minRate, maxRate, reddata, greendata, amberdata )
{

    if(minRate == "N/A")
    {
        var minRate = 0;
    }

    if(maxRate == "N/A")
    {
        var minRate = 0;
    }

    if (seriesFirstData.empty != 'true' && seriesSecondData.empty != 'true' && seriesThirdData.empty != 'true' && data != '[]' && data.length > 0)
    {

        var startDate = new Date(data[data.length - 1][0]), // Get year of last data point
            minRate = parseFloat(minRate),
            maxRate = parseFloat(maxRate),
            startPeriod,
            date,
            rate,
            index;

        startDate.setMonth(startDate.getMonth() - 3); // a quarter of a year before last data point
        startPeriod = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

        for (index = data.length - 1; index >= 0; index = index - 1) {
            date = data[index][0]; // data[i][0] is date
            rate = data[index][1]; // data[i][1] is exchange rate
            if (date < startPeriod) {
                break; // stop measuring highs and lows
            }

        }

        if (amberdata.length > 0) var amberThresholdVisible = true; else var amberThresholdVisible = false;

        if (reddata.length > 0) var redThresholdVisible = true; else var redThresholdVisible = false;

        if (greendata.length > 0) var greenThresholdVisible = true; else var greenThresholdVisible = false;

        var frmonth = new Date(fromLoadDate).getMonth();
        var fryear = new Date(fromLoadDate).getFullYear();
        var tomonth = new Date(toLoadDate).getMonth();
        var toyear = new Date(toLoadDate).getFullYear();

        var frdate = new Date(fromLoadDate).getDate();
        var todate = new Date(toLoadDate).getDate();

        if (frmonth === tomonth && fryear === toyear){
            fromLoadDate = null;
            toLoadDate = null;
        }

        if (minRate && maxRate)
        {

        	if (fromLoadDate && toLoadDate)
        	{
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load:function(){

                                if ( !this.series[0].data.length)
                                {

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        min: Date.UTC(fryear, frmonth-1, frdate),
                        max: Date.UTC(toyear, tomonth-1, todate),
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: [{
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        showEmpty: false,

                    },{
                        type: 'linear',
                        opposite: true,
                        gridLineWidth: 0,
                        title: {
                            text: seriesFirstStr,
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    }],

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                        yAxis: 1
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: seriesThirdStr,
                        data: seriesThirdData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata.sort(),
                        color: 'green',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata.sort(),
                        color: 'red',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata.sort(),
                        color: '#FFC200',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    credits: {
                        enabled: false
                    },
                });
			}
			else
			{
                // Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                        events: {
                            load:function(){

                                if ( !this.series[0].data.length)
                                {

                                    this.yAxis[0].setExtremes(null, null);
                                    this.xAxis[0].setExtremes(null, null);
                                    this.redraw();
                                }

                            },
                        },
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        },
                    },

                    yAxis: [{
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        showEmpty: false,

                    },{
                        type: 'linear',
                        opposite: true,
                        gridLineWidth: 0,
                        title: {
                            text: seriesFirstStr,
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    }],

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,

                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML: true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                        yAxis: 1
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: seriesThirdStr,
                        data: seriesThirdData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }, {
                        name: 'Green Threshold',
                        data: greendata.sort(),
                        color: 'green',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: greenThresholdVisible,
                    }, {
                        name: 'Red Threshold',
                        data: reddata.sort(),
                        color: 'red',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: redThresholdVisible,
                    }, {
                        name: 'Amber Threshold',
                        data: amberdata.sort(),
                        color: '#FFC200',
                        zones: [ {
                            dashStyle: 'shortdash'
                        }],
                        visible: amberThresholdVisible,
                    },],

                    credits: {
                        enabled: false
                    },
                });
			}

        }
        else
        {
            if (fromLoadDate && toLoadDate)
            {
// Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        min: Date.UTC(fryear, frmonth-1, frdate),
                        max: Date.UTC(toyear, tomonth-1, todate),
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        }
                    },

                    yAxis: [{
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }],
                    },{
                        type: 'linear',
                        opposite: true,
                        gridLineWidth: 0,
                        title: {
                            text: seriesFirstStr,
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    }],

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML:true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                        yAxis: 1
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    }, {
                        name: seriesThirdStr,
                        data: seriesThirdData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }],

                    credits: {
                        enabled: false
                    },
                });
            }
            else
            {
// Create the chart
                Highcharts.stockChart(container, {

                    chart: {
                        renderTo: container,
                        borderColor: '#d2d5d5',
                        borderWidth: 1,
                        type: 'line',
                    },

                    navigator: {
                        enabled: false,
                    },

                    scrollbar: {
                        enabled: false,
                    },

                    xAxis: {
                        type: 'datetime',
                        minTickInterval: 28 * 24 * 3600 * 1000,
                        dateTimeLabelFormats: {
                            day: '%e of %b'
                        }
                    },

                    yAxis: [{
                        type: 'linear',
                        opposite: false,
                        title: {
                            text: yaxisstr,
                            useHTML: true,
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }],
                    },{
                        type: 'linear',
                        opposite: true,
                        gridLineWidth: 0,
                        title: {
                            text: seriesFirstStr,
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    }],

                    rangeSelector: {
                        selected: 8,
                        inputEnabled: false,
                    },

                    legend: {
                        enabled: true,
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'top',
                        x: -20,
                        floating: false,
                        useHTML:true,
                    },

                    series: [{
                        name: seriesFirstStr,
                        data: seriesFirstData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                        yAxis: 1
                    }, {
                        name: seriesSecondStr,
                        data: seriesSecondData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML:true,
                    }, {
                        name: seriesThirdStr,
                        data: seriesThirdData.sort(),
                        marker: {
                            enabled: true,
                            radius: 4
                        },
                        useHTML: true,
                    }],

                    credits: {
                        enabled: false
                    },
                });
            }

        }
    }
}

