cd /var/app/current/;
sudo chmod -fR 777 /var/app/current/app/cache;
mkdir -p /var/app/current/web/uploads/files
sudo chmod -fR 777 /var/app/current/web/uploads/files;
sudo chmod -fR 777 /var/app/current/app/logs;
php app/console assetic:dump --env=prod --no-debug;
php app/console cache:clear --env=prod --no-debug --no-warmup;
php app/console assets:install --symlink web --env=prod;
